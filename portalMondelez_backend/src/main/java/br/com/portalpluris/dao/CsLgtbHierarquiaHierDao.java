package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbHierarquiaHierBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;

public class CsLgtbHierarquiaHierDao {

	public void insertHierarquia(CsLgtbHierarquiaHierBean arqRet, String nmArquivo, String dhProcessamento) {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		//DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsLgtbHierarquiaHierDao");
		logsBean.setLogsDsMetodo("insertHierarquia");
		logsBean.setLogsInErro("N");
		
		log.info(" ***** INICIO DO INSERT ***** ");				
		
		try{
			
		DAO dao = new DAO();
		Connection con = dao.getConexao();				
			
			PreparedStatement pst = con.prepareStatement("");			
		
			pst = con.prepareStatement(""
					+ "		INSERT INTO CS_LGTB_HIERARQUIA_HIER ("
						+ " HIER_CD_HIERARQUIACODE,"
						+ " HIER_DS_CONSTANT,"
						+ " HIER_DS_SALESHIERARCHY,"
						+ " HIER_DS_DESCRIPTION,"
						+ " HIER_DS_SALESHIERARCHYREGION,"
						+ " HIER_DS_DESCRIPTION2,"
						+ " HIER_DS_SALESAREAHIERARCHY,"
						+ " HIER_DS_DESCRIPTION3,"
						+ " HIER_DS_TERRITORYSALESHIERARCHY,"
						+ " HIER_DS_NAMEALPHA,"
						+ " HIER_DS_ADDRESSNUMBERSALESMAN,"
						+ " HIER_DS_CUSTOMERCODE,"
						+ " HIER_DS_STATUSPROCESSAMENTO,"
						+ " HIER_DH_PROCESSAMENTO,"
						+ " HIER_DS_NOMEARQUIVO"
					+ ") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )" // 15
					+ "" );
		
				pst.setString(1, arqRet.getHier_cd_hierarquiacode().trim());
				pst.setString(2, arqRet.getHier_ds_constant().trim());
				pst.setString(3, arqRet.getHier_ds_saleshierarchy().trim());
				pst.setString(4, arqRet.getHier_ds_description().trim());
				pst.setString(5, arqRet.getHier_ds_saleshierarchy().trim());
				pst.setString(6, arqRet.getHier_ds_description2().trim());
				pst.setString(7, arqRet.getHier_ds_salesareahierarchy().trim());
				pst.setString(8, arqRet.getHier_ds_description3().trim());
				pst.setString(9, arqRet.getHier_ds_territorysaleshierarchy().trim());
				pst.setString(10, arqRet.getHier_ds_namealpha().trim());
				pst.setString(11, arqRet.getHier_ds_addressnumbersalesman().trim());
				pst.setString(12, arqRet.getHier_ds_customercode().trim());
				pst.setLong(13, 'N');
				pst.setString(14, dhProcessamento);
				pst.setString(15, nmArquivo);
				
				pst.execute();
			
				log.info(" ***** FIM DO INSERT ***** ");	
			
		}catch(Exception e){
			
			log.info(" ***** ERRO NO INSERT NA BASE DE DADOS" + e  + " ***** ");	
			e.printStackTrace();
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
	
		}finally{
			
			daoLog.createLog(logsBean);
		}
	}
	
}
