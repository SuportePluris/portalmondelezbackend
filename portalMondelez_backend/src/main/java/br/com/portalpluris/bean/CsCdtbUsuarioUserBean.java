package br.com.portalpluris.bean;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CsCdtbUsuarioUserBean {
	
	String user_id_cd_usuario;
	String user_dh_registro;
	String user_ds_nomeusuario;
	String user_ds_login;
	String user_ds_senha;
	String user_ds_email;
	String user_ds_permissionamento;
	String user_in_inativo;
	String user_id_cd_usuarioatualizacao;
	String user_dh_atualizacao;

	List<RegiaoCanalBean> lstRegiaoCanal = new ArrayList<RegiaoCanalBean>();
			

	public List<RegiaoCanalBean> getLstRegiaoCanal() {
		return lstRegiaoCanal;
	}

	public void setLstRegiaoCanal(List<RegiaoCanalBean> lstRegiaoCanal) {
		this.lstRegiaoCanal = lstRegiaoCanal;
	}

	public String getUser_id_cd_usuario() {
		return user_id_cd_usuario;
	}
	
	public void setUser_id_cd_usuario(String user_id_cd_usuario) {
		this.user_id_cd_usuario = user_id_cd_usuario;
	}
	
	public String getUser_dh_registro() {
		return user_dh_registro;
	}
	
	public void setUser_dh_registro(String user_dh_registro) {
		this.user_dh_registro = user_dh_registro;
	}

	public String getUser_ds_nomeusuario() {
		return user_ds_nomeusuario;
	}

	public void setUser_ds_nomeusuario(String user_ds_nomeusuario) {
		this.user_ds_nomeusuario = user_ds_nomeusuario;
	}

	public String getUser_ds_login() {
		return user_ds_login;
	}

	public void setUser_ds_login(String user_ds_login) {
		this.user_ds_login = user_ds_login;
	}

	public String getUser_ds_senha() {
		return user_ds_senha;
	}

	public void setUser_ds_senha(String user_ds_senha) {
		this.user_ds_senha = user_ds_senha;
	}

	public String getUser_ds_email() {
		return user_ds_email;
	}

	public void setUser_ds_email(String user_ds_email) {
		this.user_ds_email = user_ds_email;
	}

	public String getUser_ds_permissionamento() {
		return user_ds_permissionamento;
	}

	public void setUser_ds_permissionamento(String user_ds_permissionamento) {
		this.user_ds_permissionamento = user_ds_permissionamento;
	}

	public String getUser_in_inativo() {
		return user_in_inativo;
	}

	public void setUser_in_inativo(String user_in_inativo) {
		this.user_in_inativo = user_in_inativo;
	}

	public String getUser_id_cd_usuarioatualizacao() {
		return user_id_cd_usuarioatualizacao;
	}

	public void setUser_id_cd_usuarioatualizacao(String user_id_cd_usuarioatualizacao) {
		this.user_id_cd_usuarioatualizacao = user_id_cd_usuarioatualizacao;
	}

	public String getUser_dh_atualizacao() {
		return user_dh_atualizacao;
	}

	public void setUser_dh_atualizacao(String user_dh_atualizacao) {
		this.user_dh_atualizacao = user_dh_atualizacao;
	}

}
