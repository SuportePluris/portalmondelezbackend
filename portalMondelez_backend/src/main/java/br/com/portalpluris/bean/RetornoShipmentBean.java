package br.com.portalpluris.bean;

import java.util.ArrayList;

public class RetornoShipmentBean {

	
	 private String skud_ds_pedidomondelez;
	 private String skud_ds_pedidocliente;
	 private String skud_id_cd_delivery;
	 private String clie_id_cd_codigocliente;
	 private String clie_ds_cnpj;
	 private String clie_ds_cidade;
	 private String clie_ds_uf;
	 private String emba_ds_planta;
	 private String daag_dh_agendamento;
	 private String daag_ds_senhaagendamento;
	 private String nofi_nr_notafiscal;
	 private String nofi_ds_serienota;
	 
	 
	 
		
	 private String skud_id_cd_shipment;
	 private String emba_id_cd_embarque;
	 private String skud_ds_pesobruto;
	 private String skud_ds_pesoliquido;
	 private String skud_ds_volumecubico;
	 private String skud_ds_valor;
	 private String skud_nr_quantidade;
	 private String emba_id_cd_shipment;
	 private String emba_ds_veiculo;
	 private String emba_ds_tipoveiculo;
	 private String emba_in_transportadoraconfirmada;
	 private String emba_ds_tipocarga;
	 private String emba_ds_cod_transportadora;
	 private String emba_dh_leadtime;
	 private String stat_ds_status;
	 private String emba_id_cd_codcliente;
	 private String clie_ds_nomecliente;
	 private String emba_id_cd_status;
	 private String skud_dh_registro;
	 private String cocl_ds_grupocliente;
	 private String cncl_ds_chanel;
	 
	 private String multistop;
	 
	 
	 
	 public String getMultistop() {
		return multistop;
	}
	public void setMultistop(String multistop) {
		this.multistop = multistop;
	}
	public String getCncl_ds_chanel() {
		return cncl_ds_chanel;
	}
	public void setCncl_ds_chanel(String cncl_ds_chanel) {
		this.cncl_ds_chanel = cncl_ds_chanel;
	}
	public String getClie_id_cd_codigocliente() {
		return clie_id_cd_codigocliente;
	}
	public void setClie_id_cd_codigocliente(String clie_id_cd_codigocliente) {
		this.clie_id_cd_codigocliente = clie_id_cd_codigocliente;
	}
	public String getNofi_nr_notafiscal() {
		return nofi_nr_notafiscal;
	}
	public void setNofi_nr_notafiscal(String nofi_nr_notafiscal) {
		this.nofi_nr_notafiscal = nofi_nr_notafiscal;
	}
	public String getNofi_ds_serienota() {
		return nofi_ds_serienota;
	}
	public void setNofi_ds_serienota(String nofi_ds_serienota) {
		this.nofi_ds_serienota = nofi_ds_serienota;
	}
	public String getCocl_ds_grupocliente() {
		return cocl_ds_grupocliente;
	}
	public void setCocl_ds_grupocliente(String cocl_ds_grupocliente) {
		this.cocl_ds_grupocliente = cocl_ds_grupocliente;
	}
	public String getDaag_ds_senhaagendamento() {
		return daag_ds_senhaagendamento;
	}
	public void setDaag_ds_senhaagendamento(String daag_ds_senhaagendamento) {
		this.daag_ds_senhaagendamento = daag_ds_senhaagendamento;
	}
	public String getDaag_dh_agendamento() {
		return daag_dh_agendamento;
	}
	public void setDaag_dh_agendamento(String daag_dh_agendamento) {
		this.daag_dh_agendamento = daag_dh_agendamento;
	}
	public String getEmba_ds_planta() {
		return emba_ds_planta;
	}
	public void setEmba_ds_planta(String emba_ds_planta) {
		this.emba_ds_planta = emba_ds_planta;
	}
	public String getClie_ds_cidade() {
		return clie_ds_cidade;
	}
	public void setClie_ds_cidade(String clie_ds_cidade) {
		this.clie_ds_cidade = clie_ds_cidade;
	}
	public String getClie_ds_uf() {
		return clie_ds_uf;
	}
	public void setClie_ds_uf(String clie_ds_uf) {
		this.clie_ds_uf = clie_ds_uf;
	}
	public String getClie_ds_cnpj() {
		return clie_ds_cnpj;
	}
	public void setClie_ds_cnpj(String clie_ds_cnpj) {
		this.clie_ds_cnpj = clie_ds_cnpj;
	}
	public String getSkud_ds_pedidomondelez() {
		return skud_ds_pedidomondelez;
	}
	public void setSkud_ds_pedidomondelez(String skud_ds_pedidomondelez) {
		this.skud_ds_pedidomondelez = skud_ds_pedidomondelez;
	}
	public String getSkud_ds_pedidocliente() {
		return skud_ds_pedidocliente;
	}
	public void setSkud_ds_pedidocliente(String skud_ds_pedidocliente) {
		this.skud_ds_pedidocliente = skud_ds_pedidocliente;
	}
	public String getSkud_id_cd_delivery() {
		return skud_id_cd_delivery;
	}
	public void setSkud_id_cd_delivery(String skud_id_cd_delivery) {
		this.skud_id_cd_delivery = skud_id_cd_delivery;
	}
	public String getSkud_dh_registro() {
		return skud_dh_registro;
	}
	public void setSkud_dh_registro(String skud_dh_registro) {
		this.skud_dh_registro = skud_dh_registro;
	}
	public String getEmba_id_cd_status() {
		return emba_id_cd_status;
	}
	public void setEmba_id_cd_status(String emba_id_cd_status) {
		this.emba_id_cd_status = emba_id_cd_status;
	}
	public String getEmba_id_cd_embarque() {
		return emba_id_cd_embarque;
	}
	public void setEmba_id_cd_embarque(String emba_id_cd_embarque) {
		this.emba_id_cd_embarque = emba_id_cd_embarque;
	}
	public String getEmba_id_cd_codcliente() {
		return emba_id_cd_codcliente;
	}
	public void setEmba_id_cd_codcliente(String emba_id_cd_codcliente) {
		this.emba_id_cd_codcliente = emba_id_cd_codcliente;
	}
	public String getClie_ds_nomecliente() {
		return clie_ds_nomecliente;
	}
	public void setClie_ds_nomecliente(String clie_ds_nomecliente) {
		this.clie_ds_nomecliente = clie_ds_nomecliente;
	}
	private ArrayList<RetornoDeliveryBean> retDelivery = new ArrayList<RetornoDeliveryBean>();
	 
	public String getSkud_nr_quantidade() {
		return skud_nr_quantidade;
	}
	public void setSkud_nr_quantidade(String skud_nr_quantidade) {
		this.skud_nr_quantidade = skud_nr_quantidade;
	} 
	 
	public String getStat_ds_status() {
		return stat_ds_status;
	}

	public void setStat_ds_status(String stat_ds_status) {
		this.stat_ds_status = stat_ds_status;
	}

	public ArrayList<RetornoDeliveryBean> getRetDelivery() {
		return retDelivery;
	}

	public void setRetDelivery(ArrayList<RetornoDeliveryBean> retDelivery) {
		this.retDelivery = retDelivery;
	}

	public String getEmba_dh_leadtime() {
		return emba_dh_leadtime;
	}

	public void setEmba_dh_leadtime(String emba_dh_leadtime) {
		this.emba_dh_leadtime = emba_dh_leadtime;
	}

	public String getEmba_ds_cod_transportadora() {
		return emba_ds_cod_transportadora;
	}

	public void setEmba_ds_cod_transportadora(String emba_ds_cod_transportadora) {
		this.emba_ds_cod_transportadora = emba_ds_cod_transportadora;
	}

	public String getSkud_id_cd_shipment() {
		return skud_id_cd_shipment;
	}

	public void setSkud_id_cd_shipment(String skud_id_cd_shipment) {
		this.skud_id_cd_shipment = skud_id_cd_shipment;
	}
	
	public String getSkud_ds_pesobruto() {
		return skud_ds_pesobruto;
	}

	public void setSkud_ds_pesobruto(String skud_ds_pesobruto) {
		this.skud_ds_pesobruto = skud_ds_pesobruto;
	}

	public String getSkud_ds_pesoliquido() {
		return skud_ds_pesoliquido;
	}

	public void setSkud_ds_pesoliquido(String skud_ds_pesoliquido) {
		this.skud_ds_pesoliquido = skud_ds_pesoliquido;
	}

	public String getSkud_ds_volumecubico() {
		return skud_ds_volumecubico;
	}

	public void setSkud_ds_volumecubico(String skud_ds_volumecubico) {
		this.skud_ds_volumecubico = skud_ds_volumecubico;
	}

	public String getSkud_ds_valor() {
		return skud_ds_valor;
	}

	public void setSkud_ds_valor(String skud_ds_valor) {
		this.skud_ds_valor = skud_ds_valor;
	}

	public String getEmba_id_cd_shipment() {
		return emba_id_cd_shipment;
	}

	public void setEmba_id_cd_shipment(String emba_id_cd_shipment) {
		this.emba_id_cd_shipment = emba_id_cd_shipment;
	}

	public String getEmba_ds_veiculo() {
		return emba_ds_veiculo;
	}

	public void setEmba_ds_veiculo(String emba_ds_veiculo) {
		this.emba_ds_veiculo = emba_ds_veiculo;
	}

	public String getEmba_ds_tipoveiculo() {
		return emba_ds_tipoveiculo;
	}

	public void setEmba_ds_tipoveiculo(String emba_ds_tipoveiculo) {
		this.emba_ds_tipoveiculo = emba_ds_tipoveiculo;
	}

	public String getEmba_in_transportadoraconfirmada() {
		return emba_in_transportadoraconfirmada;
	}

	public void setEmba_in_transportadoraconfirmada(String emba_in_transportadoraconfirmada) {
		this.emba_in_transportadoraconfirmada = emba_in_transportadoraconfirmada;
	}

	public String getEmba_ds_tipocarga() {
		return emba_ds_tipocarga;
	}

	public void setEmba_ds_tipocarga(String emba_ds_tipocarga) {
		this.emba_ds_tipocarga = emba_ds_tipocarga;
	}

}
