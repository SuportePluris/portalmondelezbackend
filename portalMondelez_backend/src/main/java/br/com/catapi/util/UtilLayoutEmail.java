package br.com.catapi.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import br.com.portalpluris.bean.CsCdtbSkudeliverySkudBean;
import br.com.portalpluris.bean.EmbarqueAgendamentoBean;
import br.com.portalpluris.bean.RetornoDeliveryBean;
import br.com.portalpluris.bean.RetornoShipmentBean;
import br.com.portalpluris.bean.RetornoSkudBean;
import br.com.portalpluris.bean.RetornoSkudXLSBean;
import br.com.portalpluris.bean.XlsCabecalhoBean;
import br.com.portalpluris.bean.DetalhesEmailCompletoBean;
import br.com.portalpluris.dao.CsCdtbSkudDeliveryDao;
import br.com.portalpluris.dao.CsNgtbEmbarqueEmbaDao;

public class UtilLayoutEmail {
	
	public byte[] getBytePDF(String id_embarque, String tipo) throws Exception {
		
		byte[] outputByte = null;
		
		StringWriter writer = getVelocityLayout(id_embarque, tipo);
		
		outputByte = gerarPDFFicha(writer);
		
		return outputByte;
	}

	
	public byte[] getByteXLS(String id_embarque, String tipo) throws Exception {
		
		byte[] outputByte = null;
		
		
		CsCdtbSkudDeliveryDao dao = new CsCdtbSkudDeliveryDao();
		XlsCabecalhoBean cabecalhoBean = dao.slcCabecalhoToXLS(id_embarque);
		List<RetornoSkudXLSBean> lstReturnSkuXLSBean = dao.slcSkudByDeliveryToXLS(id_embarque);
		
		outputByte = createfile(cabecalhoBean, lstReturnSkuXLSBean);
		
		return outputByte;
	}
	
	
	public byte[] createfile(XlsCabecalhoBean cabecalhoBean, List<RetornoSkudXLSBean> lstReturnSkuXLS) throws SQLException {
		
		byte[] bytesArquivo = null;
		
		try{
		
			//String urlTemplateXlsx = "C:\\Users\\rnunes\\Desktop\\Desenvolvimento\\mondelez - Envio de fichas de emails e dowload\\layoutClienteTemplate.xlsx";
			String urlTemplateXlsx = "/opt/wsMondelez/template_xls/layoutClienteTemplate.xlsx";
			//urlTemplateXlsx = "C:\\Users\\dcosta\\Desktop\\Desenvolvimento\\mondelez - Envio de fichas de emails e dowload\\layoutClienteTemplate.xlsx";
	// PEGAR ARQUIVO DE TEMPLATE E GERAR UMA CÓPIA NA MEMÓRIA COM NOME arquivoProcessado
			File arquivoProcessado = new File(urlTemplateXlsx);
	
	// TRANSFORMAR A COPIA DO ARQUIVO EM UM WORKBOOK 
			XSSFWorkbook hssfWorkbook = new XSSFWorkbook(new FileInputStream(arquivoProcessado));
			
	// PEGAR A PRIMEIRA SHEET APENAS
			XSSFSheet my_sheet = hssfWorkbook.getSheetAt(0);
			
			
			
	// PREENCHER CABEÇALHO		
		// CNPJ DESTINATARIO
			XSSFRow my_rowCabecalho = my_sheet.getRow(4); 							  
			my_rowCabecalho.getCell(3).setCellValue(cabecalhoBean.getCncl_ds_cnpj()); 
		// RAZÃO SOCIAL DESTINATARIO
			my_rowCabecalho = my_sheet.getRow(5); 							  		  		 
			my_rowCabecalho.getCell(3).setCellValue(cabecalhoBean.getCncl_nm_razaosocial()); 
		// NOME DESTINATARIO
			my_rowCabecalho = my_sheet.getRow(6); 							  		  
			my_rowCabecalho.getCell(3).setCellValue(cabecalhoBean.getClie_ds_nomecliente()); 
		// CIDADE - UF DESTINATARIO
			my_rowCabecalho = my_sheet.getRow(7); 							  		  
			my_rowCabecalho.getCell(3).setCellValue(cabecalhoBean.getClie_ds_cidade() + " - " + cabecalhoBean.getClie_ds_uf()); 
		// COD CLIENTE 
			my_rowCabecalho = my_sheet.getRow(8); 							  		  
			my_rowCabecalho.getCell(3).setCellValue(cabecalhoBean.getClie_id_cd_codigocliente()); 
		// CANAL 
			my_rowCabecalho = my_sheet.getRow(9); 							  		  
			my_rowCabecalho.getCell(3).setCellValue(cabecalhoBean.getCncl_ds_chanel());
		// CNPJ TRANSPORTADORA
			my_rowCabecalho = my_sheet.getRow(11); 							  		  
			my_rowCabecalho.getCell(3).setCellValue(cabecalhoBean.getTran_ds_cnpj());
		// NOME TRANSPORTADORA
			my_rowCabecalho = my_sheet.getRow(12); 							  		  
			my_rowCabecalho.getCell(3).setCellValue(cabecalhoBean.getTran_nm_nometransportadora());
			
		//LINHAS DO ARQUIVO TEMPLATE (COMEÇAR A PREENCHER LISTA DE SKUs)	
			int i = 15;   
			
			for(int x = 0; x < lstReturnSkuXLS.size(); x++) {  
			
				XSSFRow my_rowListaSku = my_sheet.getRow(i); //PEGAR APARTIR DA LINHA 16
				 
			// FINALIZAR O ARQUIVO QUANDO ENCONTRAR COLUNA NULA
				if(my_rowListaSku == null){  
					break;
				}
			
			//LISTA DE SKUS
				my_rowListaSku.getCell(1).setCellValue(lstReturnSkuXLS.get(x).getSkud_ds_pedidomondelez()); // PEDIDO MONDELEZ
				my_rowListaSku.getCell(2).setCellValue(lstReturnSkuXLS.get(x).getSkud_ds_pedidocliente()); // PEDIDO CLIENTE
				my_rowListaSku.getCell(3).setCellValue(lstReturnSkuXLS.get(x).getSkud_id_cd_delivery()); // DELIVERY 
				my_rowListaSku.getCell(4).setCellValue(lstReturnSkuXLS.get(x).getSkud_id_cd_shipment()); // SHIPMENT
				my_rowListaSku.getCell(5).setCellValue(lstReturnSkuXLS.get(x).getNofi_nr_notafiscal()); // NOTA FISCAL
				my_rowListaSku.getCell(6).setCellValue(lstReturnSkuXLS.get(x).getSkud_ds_codsku()); // SKU/CODIGO PRODUTO
				my_rowListaSku.getCell(7).setCellValue(lstReturnSkuXLS.get(x).getSkud_ds_itemsku()); // SKU / DESCRIÇÃO PRODUTO
				my_rowListaSku.getCell(8).setCellValue(""); // CATEGORIA
				my_rowListaSku.getCell(9).setCellValue(lstReturnSkuXLS.get(x).getSkud_nr_quantidade()); // QUANTIDADE
				my_rowListaSku.getCell(10).setCellValue(lstReturnSkuXLS.get(x).getEmba_ds_tipoveiculo()); // TIPO VEICULO
				//System.out.println(lstReturnSkuXLS.get(x).getEmba_ds_tipoveiculo());
				my_rowListaSku.getCell(11).setCellValue(""); // QTD PALLET
				my_rowListaSku.getCell(12).setCellValue(lstReturnSkuXLS.get(x).getEmba_ds_tipoveiculo().toUpperCase().indexOf("DRY") > -1 ? "SECA" : "REFRIGERADA"); // TIPO DE CARGA
			
				/*
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(1)  " + lstReturnSkuXLS.get(x).getSkud_ds_pedidomondelez()); // PEDIDO MONDELEZ
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(2)  " + lstReturnSkuXLS.get(x).getSkud_ds_pedidocliente()); // PEDIDO CLIENTE
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(3)  " + lstReturnSkuXLS.get(x).getSkud_id_cd_delivery()); // DELIVERY 
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(4)  " + lstReturnSkuXLS.get(x).getSkud_id_cd_shipment()); // SHIPMENT
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(5)  " + lstReturnSkuXLS.get(x).getNofi_nr_notafiscal()); // NOTA FISCAL
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(6)  " + lstReturnSkuXLS.get(x).getSkud_ds_codsku()); // SKU/CODIGO PRODUTO
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(7)  " + lstReturnSkuXLS.get(x).getSkud_ds_itemsku()); // SKU / DESCRIÇÃO PRODUTO
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(8)  " + ""); // CATEGORIA
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(9)  " + lstReturnSkuXLS.get(x).getSkud_nr_quantidade()); // QUANTIDADE
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(10) " + lstReturnSkuXLS.get(x).getEmba_ds_tipoveiculo()); // TIPO VEICULO
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(11) " + ""); // QTD PALLET
					System.out.println("linha :" + i + " coluna : my_rowListaSku.getCell(12) " + (lstReturnSkuXLS.get(x).getEmba_ds_tipoveiculo().toUpperCase().indexOf("DRY") > -1 ? "SECA" : "REFRIGERADA")); // TIPO DE CARGA
				*/			
				
				i++;			
				
			}
			
			//WORKBOOK TO BYTES
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			hssfWorkbook.write(baos);
			baos.close();
			
			bytesArquivo = baos.toByteArray();
			
	
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		
		
		return bytesArquivo;
		
	}
	
	
	


	public DetalhesEmailCompletoBean getHtmlBody(String id_embarque, String tipo) throws Exception {
		
		CsNgtbEmbarqueEmbaDao embaDao = new CsNgtbEmbarqueEmbaDao();
		
		RetornoShipmentBean shipBean = embaDao.slcEmbarqueShipment(id_embarque);
		List<EmbarqueAgendamentoBean> listAgem = embaDao.getLstAgendamento(Integer.parseInt(id_embarque));
		List<RetornoDeliveryBean> lstDelivery = embaDao.slcDelivery(shipBean.getEmba_id_cd_shipment(),id_embarque, tipo);
				
		StringBuilder sb = new StringBuilder();
				
		String html = " ";
		
		if(tipo.equalsIgnoreCase("download")){
			html = downloadLayout(shipBean, listAgem, lstDelivery);
			
		}else if(tipo.equalsIgnoreCase("emailTransportadora")){
			html = emailTransportadoraLayout(shipBean, listAgem, lstDelivery);
			
		}else if(tipo.equalsIgnoreCase("emailCliente")){
			html = emailClienteLayout(shipBean, listAgem, lstDelivery);
			
		}
		
		DetalhesEmailCompletoBean bean = new DetalhesEmailCompletoBean();
		bean.setLayoutEmail(html);
		bean.setDetalhesShip(shipBean);
		bean.setListDetalhesDoEmbarque(listAgem);
		bean.setListDetalhesDoDelivery(lstDelivery);
		
		
		return bean;
	
	}
	
	
	private byte[] gerarPDFFicha(StringWriter writer) throws Exception {
	       
		OutputStream os = new ByteArrayOutputStream();
        convertHTML(writer.toString(), os, "");
        byte[] resultado = ((ByteArrayOutputStream) os).toByteArray();
        os.close();
        
        return resultado;
	}
	
	public static void convertHTML(String input, OutputStream out, String baseDir){
		convert(new ByteArrayInputStream(input.getBytes()), out, baseDir);
	//	System.out.println(input);
	}
	
	public static void convert(InputStream input, OutputStream out, String baseDir) {

		try {
			Tidy tidy = new Tidy();

			Document doc = tidy.parseDOM(input, null);

			ITextRenderer renderer = new ITextRenderer();

			renderer.setDocument(doc, baseDir);
			renderer.layout();
			renderer.createPDF(out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public StringWriter getVelocityLayout(String id_embarque, String tipo) throws NumberFormatException, SQLException {
		
		StringWriter sw = new StringWriter();
		String template = new String("");
		VelocityContext context = new VelocityContext();
		
		template = getLayoutHtmlToPDF(id_embarque, tipo);
		
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
		ve.init();
		ve.evaluate(context, sw, "ISO-8859-1", template);
		
		return sw;
	}
	
	
	
	public String getLayoutHtmlToPDF(String id_embarque, String tipo ) throws NumberFormatException, SQLException {
		
		CsNgtbEmbarqueEmbaDao embaDao = new CsNgtbEmbarqueEmbaDao();
		
		RetornoShipmentBean shipBean = embaDao.slcEmbarqueShipment(id_embarque);
		List<EmbarqueAgendamentoBean> listAgem = embaDao.getLstAgendamento(Integer.parseInt(id_embarque));
		List<RetornoDeliveryBean> lstDelivery = embaDao.slcDelivery(shipBean.getEmba_id_cd_shipment(),id_embarque, tipo);
		
				
		StringBuilder sb = new StringBuilder();
		
		String html = "";
		String htmlInfoTransporte = "";
		
		if(tipo.equalsIgnoreCase("download")){
			html = downloadLayout(shipBean, listAgem, lstDelivery);
			
		}else if(tipo.equalsIgnoreCase("emailTransportadora")){
			html = emailTransportadoraLayout(shipBean, listAgem, lstDelivery);
			
		}else if(tipo.equalsIgnoreCase("emailCliente")){
			html = emailClienteLayout(shipBean, listAgem, lstDelivery);
			
		}
		
		sb.append(html);
		
		return sb.toString();
	}
	
	public String downloadLayout(RetornoShipmentBean shipBean, List<EmbarqueAgendamentoBean> listAgem, List<RetornoDeliveryBean> lstDelivery){
		
		List<RetornoSkudBean> lstSkudBean = new ArrayList<RetornoSkudBean>();
		CsCdtbSkudDeliveryDao skudDao = new CsCdtbSkudDeliveryDao();		
		
		String html = "";
		String htmlInfoTransporte = "";
		
		html+= "<div style=\"font-family: Verdana, Geneva, Tahoma, sans-serif; text-align: center; width: 100%;\">\r\n"                                                                                          ;
		
		html+= "    <table style=\"font-weight: bold; font-size: 12px; width: 100%; color: rgb(250, 245, 245); text-align: center; background-color: rgb(76, 76, 192); border-radius: 10px;\">\r\n" ;
		html+= "        <tr>\r\n"                                                                                                                                                                   ;
		html+= "            <td>Embarque: "+shipBean.getEmba_id_cd_embarque()+"</td>\r\n"                                                                                                                                    ;
		html+= "            <td>Status: "+shipBean.getStat_ds_status()+"</td>\r\n"                                                                                                                                        ;
		html+= "        </tr>\r\n"                                                                                                                                                                  ;
		html+= "    </table>\r\n"                                                                                                                                                                   ;
		html+= "    <br/>\r\n"                                                                                                                                                                      ;
		html+= "\r\n"                                                                                                                                                                               ;
		
		htmlInfoTransporte+= "    <div style=\"background-color: rgb(183, 183, 187); border-radius: 10px;\">\r\n"                                                                                                 ;
		htmlInfoTransporte+= "        <table style=\"font-weight: bold; font-size: 12px; width: 100%; color: rgb(250, 245, 245); text-align: left;\">\r\n"                                                        ;
		htmlInfoTransporte+= "            <tr>\r\n"                                                                                                                                                               ;
		htmlInfoTransporte+= "                <td>Info do transporte</td>\r\n"                                                                                                                                    ;
		htmlInfoTransporte+= "                <td>Transportadora confirmada? "+shipBean.getEmba_in_transportadoraconfirmada()+"</td>\r\n"                                                                                                            ;
		htmlInfoTransporte+= "            </tr>\r\n"                                                                                                                                                              ;
		htmlInfoTransporte+= "        </table>\r\n"                                                                                                                                                               ;
		htmlInfoTransporte+= "\r\n"                                                                                                                                                                               ;
		htmlInfoTransporte+= "        <table style=\"font-size: 12px; width: 100%; text-align: left;\">\r\n"                                                                                                      ;
		htmlInfoTransporte+= "            <tr>\r\n"                                                                                                                                                               ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Peso Bruto: "+shipBean.getSkud_ds_pesobruto()+"</td>\r\n"                                                                                            ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Peso Liquido: "+shipBean.getSkud_ds_pesoliquido()+"</td>\r\n"                                                                                       ;
		htmlInfoTransporte+= "            </tr>\r\n"                                                                                                                                                              ;
		htmlInfoTransporte+= "            <tr>\r\n"                                                                                                                                                               ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Cubagem: "+shipBean.getSkud_ds_volumecubico()+"</td>\r\n"                                                                                                  ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Valor: "+shipBean.getSkud_ds_valor()+"</td>\r\n"                                                                                                     ;
		htmlInfoTransporte+= "            </tr>\r\n"                                                                                                                                                              ;
		htmlInfoTransporte+= "            <tr>\r\n"                                                                                                                                                               ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Qtd: "+shipBean.getSkud_nr_quantidade()+"</td>\r\n"                                                                                                          ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Veiculo: "+shipBean.getEmba_ds_veiculo()+"</td>\r\n"                                                                                                        ;
		htmlInfoTransporte+= "            </tr>\r\n"                                                                                                                                                              ;
		htmlInfoTransporte+= "            <tr>\r\n"                                                                                                                                                               ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Tipo Carga: "+shipBean.getEmba_ds_tipocarga()+"</td>\r\n"                                                                                                   ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Transportadora: "+shipBean.getEmba_ds_cod_transportadora()+"</td>\r\n"                                                                                          ;
		htmlInfoTransporte+= "            </tr>\r\n"                                                                                                                                                              ;
		htmlInfoTransporte+= "            <tr>\r\n"                                                                                                                                                               ;
		htmlInfoTransporte+= "                <td style=\"text-align: left; width: 50%;\">Tipo veiculo: "+shipBean.getEmba_ds_tipoveiculo()+"</td>\r\n"                                                                                               ;
		htmlInfoTransporte+= "            </tr>\r\n"                                                                                                                                                              ;
		htmlInfoTransporte+= "        </table>\r\n"                                                                                                                                                               ;
		htmlInfoTransporte+= "    <br/>\r\n"                                                                                                                                                                      ;
		htmlInfoTransporte+= "    </div>\r\n"                                                                                                                                                                     ;
		htmlInfoTransporte+= "    <br/>\r\n"                                                                                                                                                                      ;
		htmlInfoTransporte+= "\r\n"                                                                                                                                                                               ;
		
		for (RetornoDeliveryBean retDelivery : lstDelivery) {
		
			html+= "    <div style=\"background-color: rgb(183, 183, 187); border-radius: 10px;\">\r\n"                                                                                                 ;
			html+= "        <table style=\"font-weight: bold; font-size: 12px; width: 100%; color: rgb(250, 245, 245); text-align: left;\">\r\n"                                                        ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			html+= "                <td>Detalhes do Shipment: "+shipBean.getEmba_id_cd_shipment()+"</td>\r\n"                                                                                                                       ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "        </table>\r\n"                                                                                                                                                               ;
			html+= "                \r\n"                                                                                                                                                               ;
			html+= "        <table style=\"font-size: 12px; width: 100%; text-align: left; border-radius: 10px;\">\r\n"                                                                                 ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Delivery: "+retDelivery.getSkud_id_cd_delivery()+"</td>\r\n"                                                                                                       ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Nota Fiscal: "+retDelivery.getNofi_nr_notafiscal()+"</td>\r\n"                                                                                                ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Peso Bruto: "+retDelivery.getSkud_ds_pesobruto()+"</td>\r\n"                                                                                               ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Peso Liquido: "+retDelivery.getSkud_ds_pesoliquido()+"</td>\r\n"                                                                                          ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Cubagem: "+retDelivery.getSkud_ds_volumecubico()+"</td>\r\n"                                                                                                    ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Valor: "+retDelivery.getSkud_ds_valor()+"</td>\r\n"                                                                                                       ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Qtd: "+retDelivery.getSkud_nr_quantidade()+"</td>\r\n"                                                                                                            ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			html+= "                <td>&nbsp;</td>\r\n"                                                                                                       ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			
			
			lstSkudBean = skudDao.slcSkudByDelivery(retDelivery.getSkud_id_cd_delivery());
			for (RetornoSkudBean skudBean : lstSkudBean) {

				html+= "            <tr>\r\n"                                                                                                                                                               ;
				html+= "                 <td style=\"text-align: left; width: 50%;\">Codigo: "+skudBean.getSkud_ds_codsku()+"</td>\r\n"                                                                                                                       ;
				html+= "                 <td style=\"text-align: left; width: 50%;\">Item: "+skudBean.getSkud_ds_itemsku()+"</td>\r\n"                                                                                                                       ;
				html+= "            </tr>\r\n"                                                                                                                                                              ;
				html+= "            <tr>\r\n"                                                                                                                                                               ;
				html+= "                <td style=\"text-align: left; width: 50%;\">Data delivery: "+skudBean.getSkud_dh_delivery()+"</td>\r\n"                                                                                                    ;
				html+= "                <td style=\"text-align: left; width: 50%;\">Tipo de venda: "+skudBean.getSkud_ds_tipovenda()+"</td>\r\n"                                                                                                       ;
				html+= "            </tr>\r\n"                                                                                                                                                              ;
				html+= "            <tr>\r\n"                                                                                                                                                               ;
				html+= "                <td style=\"text-align: left; width: 50%;\">Pedido Mondelez: "+skudBean.getSkud_ds_pedidomondelez()+"</td>\r\n"                                                                                                       ;
				html+= "                <td style=\"text-align: left; width: 50%;\">Pedido Cliente: "+skudBean.getSkud_ds_pedidocliente()+"</td>\r\n"                                                                                                       ;
				html+= "            </tr>\r\n"                                                                                                                                                              ;
				
				html+= "            <tr>\r\n"                                                                                                                                                               ;
				
				String skud_in_cancelado = "";
				if(retDelivery.getSkud_in_cancelado().equalsIgnoreCase("S")) {
					skud_in_cancelado = "Sim";
				}else {
					skud_in_cancelado = "Não";
				}
				
				html+= "                <td style=\"text-align: left; width: 50%;\">Cancelado: "+skud_in_cancelado+"</td>\r\n"                                                                                                       ;
				html+= "                <td style=\"text-align: left; width: 50%;\">&nbsp;</td>\r\n"                                                                                                       ;
				html+= "            </tr>\r\n"                                                                                                                                                              ;
				
				html+= "            <tr>\r\n"                                                                                                                                                               ;
				html+= "                <td>&nbsp;</td>\r\n"                                                                                                       ;
				html+= "            </tr>\r\n"                                                                                                                                                              ;
                                                                                                                                                                   ;
			}
			
			html+= "        </table>\r\n"                                                                                                                                                               ;
			html+= "    <br/>\r\n"                                                                                                                                                                      ;
			html+= "    </div>\r\n"                                                                                                                                                                     ;
			html+= "    <br/>\r\n"                                                                                                                                                                      ;
		}
		
		html += htmlInfoTransporte;
		
		html+= "\r\n"                                                                                                                                                                               ;
		html+= "    <br/>\r\n"                                                                                                                                                                      ;
		html+= "    \r\n"                                                                                                                                                                           ;
		html+= "    <div style=\"background-color: rgb(183, 183, 187); border-radius: 10px;\">\r\n"                                                                                                 ;
		html+= "        <table style=\"font-weight: bold; font-size: 12px; width: 100%; color: rgb(250, 245, 245); text-align: left;\">\r\n"                                                        ;
		html+= "            <tr>\r\n"                                                                                                                                                               ;
		html+= "                <td>Agenda</td>\r\n"                                                                                                                                                ;
		html+= "            </tr>\r\n"                                                                                                                                                              ;
		html+= "        </table>\r\n"                                                                                                                                                               ;
		html+= "\r\n"                                                                                                                                                                               ;

		for (EmbarqueAgendamentoBean agenBean : listAgem) {
		
			html+= "        <table style=\"font-size: 12px; width: 100%; text-align: left;\">\r\n"                                                                                                      ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Data: "+agenBean.getDaag_dh_agendamento()+"</td>\r\n"                                                                                                        ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Hora: "+agenBean.getDaag_ds_horaagendamento()+"</td>\r\n"                                                                                                        ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Tipo: "+agenBean.getDaag_ds_tipoagendamento()+"</td>\r\n"                                                                                                       ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			
			if(agenBean.getMoti_ds_motivo() == null) {
				html+= "            <td style=\"text-align: left; width: 50%;\">Motivo: </td>\r\n"                                                                                                    ;	
			}else {
				html+= "            <td style=\"text-align: left; width: 50%;\">Motivo: "+agenBean.getMoti_ds_motivo()+"</td>\r\n"                                                                                                    ;
			}
			
			html+= "                <td style=\"text-align: left; width: 50%;\">Responsavel: "+agenBean.getResp_nm_responsavel()+"</td>\r\n"                                                                                         ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Status: "+agenBean.getStat_ds_status()+"</td>\r\n"                                                                                                    ;
			html+= "                <td style=\"text-align: left; width: 50%;\">Senha da Transportadora: "+agenBean.getDaag_ds_senhaagendamento()+"</td>\r\n"                                                                                   ;
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "            <tr>\r\n"                                                                                                                                                               ;
			
			if(agenBean.getEmba_in_cutoff() == null || agenBean.getEmba_in_cutoff().equalsIgnoreCase("S")) {
				html+= "            <td style=\"text-align: left; width: 50%;\">Cutoff: Sim</td>\r\n"                                                                                                           ;	
			}else {
				html+= "            <td style=\"text-align: left; width: 50%;\">Cutoff: Nao</td>\r\n"                                                                                                           ;
			}
			
			html+= "            </tr>\r\n"                                                                                                                                                              ;
			html+= "        </table>\r\n"                                                                                                                                                               ;
			html+= "    <br/>\r\n"                                                                                                                                                                      ;
		
		}
		
		html+= "    </div>\r\n"                                                                                                                                                                     ;
		html+= "    <br/>\r\n"                                                                                                                                                                      ;
		html+= "</div>";
		
		return html;		
				
	}
	
	
	public String emailTransportadoraLayout(RetornoShipmentBean shipBean, List<EmbarqueAgendamentoBean> listAgem, List<RetornoDeliveryBean> lstDelivery){
		
		List<RetornoSkudBean> lstSkudBean = new ArrayList<RetornoSkudBean>();
		CsCdtbSkudDeliveryDao skudDao = new CsCdtbSkudDeliveryDao();		
		
		String html = "";
		
		
		html+= " <div style=\"font-family: Verdana, Geneva, Tahoma, sans-serif; text-align: left; width: 95%; font-size: 11px;\">";
		html+= " 	<br/>Caro Transportador, Segue relação de pedidos com agenda confirmada. ";
		html+= "     	<br/><br/> ";
		
		
	//*** FOR *** DELIVERYS
		
		for (RetornoDeliveryBean retDelivery : lstDelivery) {
			html+= " 	<!--FOR COM AS DELIVERYS--> ";
			
			html+= " 	<fieldset style=\"width: 100%;\"> ";
			html+= " 		<table style=\"width: 100%; text-align: left; font-size: 11px;\"> ";
			html+= "            <tr> ";
			html+= "                <td> ";
			html+= " 					<div style=\"font-size: 11px;\">Núm. Delivery: "+retDelivery.getSkud_id_cd_delivery()+"</div>	 ";
			
			html+= " 					<div style=\"font-size: 11px;\">Pedido MDLZ 557: "+retDelivery.getSkud_ds_pedidomondelez()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">Pedido Cliente: "+retDelivery.getSkud_ds_pedidocliente()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">Delivery Note 558: "+retDelivery.getSkud_id_cd_delivery()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">Nota fiscal: "+retDelivery.getNofi_ds_serienota()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">Série NF: "+retDelivery.getNofi_nr_notafiscal()+"</div>	 ";
			
			html+= " 					<div style=\"font-size: 11px;\">Cód. Embarque: "+shipBean.getEmba_id_cd_embarque()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">Pedido: "+retDelivery.getSkud_ds_pedidomondelez()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">CD: "+retDelivery.getEmba_ds_planta()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">Cliente: "+retDelivery.getClie_id_cd_codigocliente()+"-"+retDelivery.getEmba_ds_planta()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">Transportadora: "+retDelivery.getTran_ds_codigotransportadora()+"-"+retDelivery.getTran_nm_nometransportadora()+"</div>	 ";
			html+= " 					<div style=\"font-size: 11px;\">Data agendada: "+shipBean.getDaag_dh_agendamento()+"</div> ";
			html+= " 				</td> ";
			html+= "            </tr> ";
			html+= " 			<tr> ";
			html+= "                <td style=\"text-align:center;font-weight: bold\"> ";
			html+= " 					<div  style=\"font-size: 11px;\">Produtos da Delivery</div> "; 
			html+= " 				</td> ";
			html+= "            </tr> ";
			html+= " 			<tr> "; 
			html+= "                <td style=\"text-align:center;font-weight: bold\"> "; 
			html+= " 					<br/> ";
			html+= " 				</td> ";
			html+= "            </tr> ";
			html+= " 			<tr> ";
	html+= "                 		<td> ";
			html+= " 					<table style=\"width: 100%; text-align: center;\"> ";
			html+= "             				<tr> ";
			html+= " 								<td style=\"width: 70%;font-weight: bold;font-size: 11px;\">Produto</td> ";
			html+= " 								<td style=\"width: 10%;font-weight: bold;font-size: 11px;\">Qtd</td> ";
			html+= " 								<td style=\"width: 10%;font-weight: bold;font-size: 11px;\">Valor</td> ";
			html+= " 								<td style=\"width: 10%;font-weight: bold;font-size: 11px;\">Peso</td> ";
			//html+= " 								<td style=\"width: 10%;font-weight: bold;font-size: 11px;\">Lote</td> ";
			html+= "             				</tr> ";
			
			lstSkudBean = skudDao.slcSkudByDelivery(retDelivery.getSkud_id_cd_delivery());
		
		//*** FOR *** SKUS
			for (RetornoSkudBean skudBean : lstSkudBean) {
			
				html+= " 						<tr> ";
				html+= " 							<td style=\"width: 70%;font-size: 11px;\">" + skudBean.getSkud_ds_codsku() + " - " + skudBean.getSkud_ds_itemsku() +"</td> ";
				html+= " 							<td style=\"width: 10%;font-size: 11px;\">" + skudBean.getSkud_nr_quantidade() + "</td> ";
				html+= " 							<td style=\"width: 10%;font-size: 11px;\">" + skudBean.getSkud_ds_valor() + "</td> ";
				html+= " 							<td style=\"width: 10%;font-size: 11px;\">" + skudBean.getSkud_ds_pesoliquido() + "</td> ";
				//html+= " 							<td style=\"width: 10%;font-size: 11px;\"></td> ";
				html+= "             			</tr> ";
				
			}
				
			html+= " 					</table> ";
			html+= " 				</td> ";
			html+= "        	</tr> ";
			html+= "    	</table> ";
			html+= "   	</fieldset> ";
			html+= " 	<br/> ";
		
			
		}
		
		
		
		
	/*FOR COM AS DADOS DE AGENDA*/
		
		//for (EmbarqueAgendamentoBean agenBean : listAgem) {
			
			html+= " 	<fieldset style=\"width: 100%;\"> ";
			html+= " 		<table style=\"width: 100%; text-align: left; font-size: 11px;\"> ";
			html+= "            <tr> ";
			html+= "                <td style=\"text-align:center;font-size: 11px;font-weight: bold;\"> ";
			html+= " 					<div>[ Embarque "+shipBean.getEmba_id_cd_embarque()+" ]</div> ";
			html+= " 				</td> ";
			html+= "            </tr> ";
			html+= " 			<tr> ";
			html+= "                 <td> ";
			html+= " 					<table style=\"width: 100%; font-size: 11px;\"> ";
			html+= "             			<tr> ";
			//html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Pedido MDLZ 557 </td> ";
			//html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Pedido Cliente </td> ";
			//html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Delivery Note 558 </td> ";
			//html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Nota fiscal </td> ";
			//html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Série NF </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Carrier </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Transp confirmada </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Shipment </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Código Cliente </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> CNPJ Entrega </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Razão Social </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Cidade </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> UF </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> CD Origem </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Soma de Peso Líquido </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Soma de Cubagem Líquida </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Soma de Valor Líquido </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Qtd Volumes </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Tipo de veículo </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Agenda </td> " ;
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Senha </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> Grupo do cliente </td> ";
			html+= " 							 ";
			html+= "             			</tr> " ;
			html+= " 						<tr> ";
			//html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getSkud_ds_pedidomondelez() + " </td> "; 
			//html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getSkud_ds_pedidocliente() + " </td> ";
			//html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getSkud_id_cd_delivery() + " </td> ";
			//html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getNofi_nr_notafiscal() + " </td> ";
			//html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getNofi_ds_serienota() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getEmba_ds_cod_transportadora() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getEmba_in_transportadoraconfirmada() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getEmba_id_cd_shipment() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getEmba_id_cd_codcliente() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getClie_ds_cnpj() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getClie_ds_nomecliente() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getClie_ds_cidade() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getClie_ds_uf() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getEmba_ds_planta() + "  </td> ";
			
			
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getSkud_ds_pesoliquido() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getSkud_ds_volumecubico() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getSkud_ds_valor() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getSkud_nr_quantidade() + "  </td> ";
			
			
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getEmba_ds_tipoveiculo() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getDaag_dh_agendamento() + " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getDaag_ds_senhaagendamento() + " </td> ";
			//html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getCocl_ds_grupocliente() + "</td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> " + shipBean.getCncl_ds_chanel() + "</td> ";
			html+= "             			</tr> ";
			html+= " 					</table> ";
			html+= " 				</td> ";
			html+= "             </tr> ";
			html+= "         </table> ";
			html+= "   	</fieldset> ";
		
		//}
		
		html+= " 	Por favor, se atentar aos dias de entrega e qualquer problema, informar o time de Agendamento ou Tracking. <br/><br/>Obrigado, Time Agendamento PLURISMIDIA ";
		html+= " </div> ";
			
		
		return html;		
				
	}
	
	
	
	public String emailClienteLayout(RetornoShipmentBean shipBean, List<EmbarqueAgendamentoBean> listAgem, List<RetornoDeliveryBean> lstDelivery){
		
		List<RetornoSkudBean> lstSkudBean = new ArrayList<RetornoSkudBean>();
		CsCdtbSkudDeliveryDao skudDao = new CsCdtbSkudDeliveryDao();		
		
		String html = "";
		
		
		html+= " <div style=\"font-family: Verdana, Geneva, Tahoma, sans-serif; text-align: left; width: 95%; font-size: 11px;\">";
		html+= " 	<br/>Prezado cliente, segue solicitação de agendamento. ";
		html+= "    <br/><br/> ";
		
		
	/*FOR COM AS DADOS DE AGENDA*/
		
		//for (EmbarqueAgendamentoBean agenBean : listAgem) {
			
			html+= " 	<fieldset style=\"width: 100%;\"> ";
			html+= " 		<table style=\"width: 100%; text-align: left; font-size: 11px;\"> ";
			html+= "             		<tr> ";
			html+= "                 		<td style=\"text-align:center;font-size: 11px;font-weight: bold;\"> ";
			html+= " 							<div>[ Embarque " + shipBean.getEmba_id_cd_embarque() + " ]</div> ";
			html+= " 						</td> ";
			html+= "             		</tr> ";
			html+= " 			<tr> ";
			html+= "                 <td> ";
			html+= " 					<table style=\"width: 100%; font-size: 11px;\"> ";
			html+= "             			<tr> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> &nbsp;&nbsp; Shipment </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> &nbsp;&nbsp;Valor liquido </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> &nbsp;&nbsp;Cubagem liquida </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> &nbsp;&nbsp;Peso liquido </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> &nbsp;&nbsp;Qtd Vol </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> &nbsp;&nbsp;Tipo de veículo </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> &nbsp;&nbsp;CD Origem </td> ";
			html+= " 							<td style=\"text-align:center;font-weight: bold;font-size: 11px;\"> &nbsp;&nbsp;Data sugerida </td> ";
			html+= "             			</tr> " ;
			html+= " 						<tr> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> &nbsp;&nbsp;" + shipBean.getSkud_id_cd_shipment() +" </td> "; 
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> &nbsp;&nbsp;" + shipBean.getSkud_ds_valor() + " </td> "; 
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> &nbsp;&nbsp;" + shipBean.getSkud_ds_volumecubico() + " </td> "; 
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> &nbsp;&nbsp;" + shipBean.getSkud_ds_pesoliquido() + " </td> "; 
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> &nbsp;&nbsp;" + shipBean.getSkud_nr_quantidade() + " </td> "; 
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> &nbsp;&nbsp;" + shipBean.getEmba_ds_tipoveiculo() + " </td> "; 
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> &nbsp;&nbsp;" + shipBean.getEmba_ds_planta()+ " </td> ";
			html+= " 							<td style=\"text-align:center;font-size: 11px;\"> &nbsp;&nbsp;" + shipBean.getDaag_dh_agendamento() + " </td> ";
			html+= "             			</tr> ";
			html+= " 					</table> ";
			html+= " 				</td> ";
			html+= "             		</tr> ";
			html+= "         	</table> ";
			html+= "   	</fieldset> ";
		
		//}
		
		html+= " 	<br/> Atenção!!!<br/><br/>";
		
		html+= " 	Para que tenhamos tempo hábil de enviar as informações às transportadoras e possamos cumprir corretamente o Lead Time de entrega, solicitamos que as repostas de agendamento sejam enviadas até as 14:00hs do dia vigente (dia da solicitação).<br/>";
		html+= " 	Se por ventura não retornarem dentro deste prazo, por gentileza acrescentar 1 dia na data de agendamento sugerida. <br/><br/>";
		
		html+= " 	Cargas que não tiverem o agendamento confirmado no prazo de 48 horas, poderão seguir processo de desalocação/cancelamento.<br/>";
		
		html+= " </div> ";
			
		

		 

		
		return html;		
				
	}
	
	
	
}
