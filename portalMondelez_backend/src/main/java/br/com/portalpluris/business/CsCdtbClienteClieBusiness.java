package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.BaseClienteConsolidadaBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsCdtbClienteClieDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbClienteClieBusiness {

	CsCdtbClienteClieDao clieDao = new CsCdtbClienteClieDao();
	private DAOLog daoLog = new DAOLog();

	public String createClie(BaseClienteConsolidadaBean clieBean)
			throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogClie(clieBean, "createClie");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = clieDao.insertCliente(clieBean );

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String updateClie(BaseClienteConsolidadaBean clieBean) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogClie(clieBean, "updateClie");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = clieDao.updateCliente(clieBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListCliente(String clieInAtivo) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogClie(null, "getListClie");
		logsBean.setLogsTxInfoentrada(clieInAtivo);

		List<BaseClienteConsolidadaBean> listClieBean = new ArrayList<BaseClienteConsolidadaBean>();

		String jsonRetorno = "";

		try {

			listClieBean = clieDao.getListClie(clieInAtivo);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listClieBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListClienteFiltro(String criterio, String valor) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogClie(null, "getListClie");
		logsBean.setLogsTxInfoentrada(criterio + "/" + valor);

		List<BaseClienteConsolidadaBean> listClieBean = new ArrayList<BaseClienteConsolidadaBean>();

		String jsonRetorno = "";

		try {

			listClieBean = clieDao.getListClieFiltro(criterio, valor);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listClieBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}
	
	public String getCliente(String codCliente) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogClie(null, "getClie");
		logsBean.setLogsTxInfoentrada(codCliente + "");

		BaseClienteConsolidadaBean clieBean = new BaseClienteConsolidadaBean();

		String jsonRetorno = "";

		try {

			clieBean = clieDao.getClie(codCliente);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(clieBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;

	}
	
	
	
	public String getClienteByEmbarque(String idEmbarque) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogClie(null, "getClienteByEmbarque");
		logsBean.setLogsTxInfoentrada(idEmbarque + "");

		BaseClienteConsolidadaBean clieBean = new BaseClienteConsolidadaBean();

		String jsonRetorno = "";

		try {

			clieBean = clieDao.getClieByEmbarque(idEmbarque);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(clieBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;

	}

	private EsLgtbLogsLogsBean getLogClie(BaseClienteConsolidadaBean clieBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbClienteClieBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");
		logsBean.setLogsTxInfoentrada(getAllValues(clieBean));

		if (clieBean != null) {
			logsBean.setLogsDsChave(clieBean.getClie_id_cd_codigocliente());
		}

		return logsBean;
	}

	private String getAllValues(BaseClienteConsolidadaBean clieBean) {

		String all = "";

		if (clieBean != null) {
			all = "" + clieBean.getClie_id_cd_codigocliente() + ";" + clieBean.getClie_ds_nomecliente() + ";" + clieBean.getClie_ds_cnpj() + ";"
					+ clieBean.getClie_ds_cidade() + ";" + clieBean.getClie_ds_uf() + ";" + clieBean.getClie_ds_regiao() + ";"
					+ clieBean.getClie_ds_agrupamento() + ";" + clieBean.getClie_ds_endereco() + ";" + clieBean.getClie_ds_cep() + ";"
					+ clieBean.getClie_ds_temagendamento() + ";" + clieBean.getClie_ds_nfobrigatoria() + ";"
					+ clieBean.getClie_ds_agendamentoobrigatorio() + ";" + clieBean.getClie_ds_tipoagendamento()
					+ ";" + clieBean.getClie_ds_diasrecebimento() + ";" + clieBean.getClie_ds_horariorecebimento() + ";"
					+ clieBean.getClie_ds_esperacliente() + ";" + clieBean.getClie_ds_veiculodedicado() + ";"
					+ clieBean.getClie_ds_pagataxaveiculodedicado() + ";" + clieBean.getClie_ds_tipodescarga() + ";"
					+ clieBean.getClie_ds_taxadescarga() + ";" + clieBean.getClie_ds_fefoespecial() + ";"
					+ clieBean.getClie_ds_paletizacaoespecial() + ";" + clieBean.getClie_ds_clienteexigepalete() + ";"
					+ clieBean.getClie_ds_paletizacaomondelez() + ";" + clieBean.getClie_ds_tipopaletizacao() + ";"
					+ clieBean.getClie_ds_alturpalete() + ";" + clieBean.getClie_ds_codveiculomax() + ";"
					+ clieBean.getClie_ds_tipoveiculomax() + ";" + clieBean.getClie_ds_pesoveiculo() + ";" + clieBean.getClie_ds_datarevisao()
					+ ";" + clieBean.getClie_ds_resprevisao() + ";" + clieBean.getClie_ds_observacao() + ";"
					+ clieBean.getCli_ds_restricaocirculacao() + ";" + clieBean.getClie_ds_tiporestricao() + ";"
					+ clieBean.getClie_ds_horariorestricao() + ";" + "";
		}
		

		return all;
	}

}
