package br.com.portalpluris.business;

import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbResumoDnsRednBean;
import br.com.portalpluris.bean.CsLgtbShipmentShipBean;
import br.com.portalpluris.bean.RetornoCargaBean;
import br.com.portalpluris.bean.RetornoQuantidadeImportadosBean;
import br.com.portalpluris.bean.RetornoValidaCargaBean;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsLgtbResumoDnsRednDao;
import br.com.portalpluris.dao.CsLgtbShipmentShipDao;

public class CsLgtbShipmentShipBusiness {

	/**
	 * Metodo responsavel por realizar leitura e gravacao das informacoes disponibilizadas via planilha no  banco de dados
	 * @author Caio Fernandes
	 * */
	public void gravarShipment() {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		log.info("[gravarShipment] - INICIO ");

		try {

			List<CsLgtbShipmentShipBean> lstShipment = new ArrayList<CsLgtbShipmentShipBean>();
			List<CsLgtbResumoDnsRednBean> lstResumoDns = new ArrayList<CsLgtbResumoDnsRednBean>();
			
			CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
			UtilMoveArquivo utilMove = new UtilMoveArquivo();

//			String diretorioShipment = confDao.slctConf("conf.importacao.agendamento.shipment");
//			String diretorioShipmentBkp = confDao.slctConf("conf.importacao.agendamento.shipment.bkp");
//			String diretorioShipmentErro = confDao.slctConf("conf.importacao.agendamento.shipment.erro");
			
			String diretorioShipment = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\SHIPMENT";
			String diretorioShipmentBkp = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\SHIPMENT\\bkp";
			String diretorioShipmentErro = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\erro";
			
			// LEITURA DOS ARQUIVOS DO DIRETORIO
			File arquivos = new File(diretorioShipment);
			String[] arq = arquivos.list();
			
			if(arq != null) {
				
				for (int i = 0; i < arq.length; i++) {
					
					// ARQUIVO A SER PROCESSADO
					//File arquivoProcessado = new File(diretorioShipment + "/" +  arq[i]); // UTILIZAR NO LINUX
					File arquivoProcessado = new File(diretorioShipment + "\\" +  arq[i]); // UTILIZAR NO WINDOWNS
					
					try {

						if(arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

							String nmArquivo = arquivoProcessado.getName();

							// INSTANCIAR ARQUIVO
							FileInputStream file = new FileInputStream(arquivoProcessado);

							// INSTANCIAR PLANILHA
							Workbook wb = WorkbookFactory.create(file);								
							
							// FOR DAS ABAS DO ARQUIVO
							for(int j = 0; j < wb.getNumberOfSheets(); j++) {

								// CONFIRMAR NOME DA ABA
								if(wb.getSheetName(j).equalsIgnoreCase("Base Planejamento")) {
									
									// SETAR ABA "Base Planejamento"
									Sheet planilhaShip = wb.getSheetAt(j);

									// OBTER VALORES DA PLANILHA E PREENCHER A LISTA 
									lstShipment = setListaShipment(lstShipment, planilhaShip);
									
									// REALIZAR O INSERT DA LISTA NO BANCO
									CsLgtbShipmentShipDao shipmentShipDao = new CsLgtbShipmentShipDao();
									shipmentShipDao.insertShipment(lstShipment, nmArquivo);
									
								} else if (wb.getSheetName(j).equalsIgnoreCase("Resumo de DNs")) {
									
									// SETAR ABA "Resumo de DNs"
									Sheet planilhaResumo = wb.getSheetAt(j);
									
									// OBTER VALORES DA PLANILHA E PREENCHER A LISTA 
									lstResumoDns = setListaResumoDns(lstResumoDns, planilhaResumo);
									
									// REALIZAR O INSERT DA LISTA NO BANCO
									CsLgtbResumoDnsRednDao resumoDnsDao = new CsLgtbResumoDnsRednDao();
									resumoDnsDao.insertResumoDns(lstResumoDns, nmArquivo);
									
								}

							}
							
							// MOVER ARQUIVO PARA PASTA BKP
							File arquivoProcessadoBkp = new File(diretorioShipmentBkp + "/" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoBkp);
						}
						
					} catch (Exception e) {
						log.info("[gravarShipment] - ERRO AO LER AQUIVO SHIPMENT: " + e.getMessage());
						
						// MOVER ARQUIVO PARA PASTA DE ERRO
						if(arq != null) {
							File arquivoProcessadoErro = new File(diretorioShipmentErro + "/" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoErro);
						}
						
					}
				}
				
			} else {
				log.info("[gravarShipment] - NAO FOI POSSIVEL OBTER OS ARQUIVOS DO DIRETORIO ");
			}

		} catch (Exception e){
			log.info("[gravarShipment] - ERRO AO GRAVAR SHIPMENT: " + e.getMessage());
		}

	}

	/**
	 * Metodo responsavel por obter os valores da planilha e retornar a lista de CsLgtbResumoDnsRednBean preenchida.
	 * @author Caio Fernandes
	 * @param List<CsLgtbResumoDnsRednBean>, Sheet
	 * @return List<CsLgtbResumoDnsRednBean>
	 * */
	private List<CsLgtbResumoDnsRednBean> setListaResumoDns(List<CsLgtbResumoDnsRednBean> lstResumoDns, Sheet planilha) {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		UtilDate util = new UtilDate();

		// FOR DAS LINHAS DO ARQUIVO
		for (int i = 1; i < planilha.getLastRowNum()+1 ; i++){
			
			try {
				
				// POPULAR BEAN
				CsLgtbResumoDnsRednBean resumoDnsBean = new CsLgtbResumoDnsRednBean();
				
				if(planilha.getRow(i).getCell(0) != null){
					planilha.getRow(i).getCell(0).setCellType(CellType.STRING);
					resumoDnsBean.setRedn_ds_ordereleaseid(planilha.getRow(i).getCell(0).toString()); 
				}
				
				if(planilha.getRow(i).getCell(1) != null){
					planilha.getRow(i).getCell(1).setCellType(CellType.STRING);
					resumoDnsBean.setRedn_ds_dnn(planilha.getRow(i).getCell(1).toString()); 
				}
				
				if(planilha.getRow(i).getCell(2) != null){
					
					if(planilha.getRow(i).getCell(2).toString().equalsIgnoreCase("#N/A")) {
						resumoDnsBean.setRedn_dh_deliverydate(null);
					} else {
						resumoDnsBean.setRedn_dh_deliverydate(util.convertStringToDate(planilha.getRow(i).getCell(2).toString()));
					}
					
				}
				
				if(planilha.getRow(i).getCell(3) != null){
					planilha.getRow(i).getCell(3).setCellType(CellType.STRING);
					resumoDnsBean.setRedn_ds_shipmenttop(planilha.getRow(i).getCell(3).toString());
				}
				
				if(planilha.getRow(i).getCell(4) != null){
					planilha.getRow(i).getCell(4).setCellType(CellType.STRING);
					resumoDnsBean.setRedn_ds_shipmentsplit(planilha.getRow(i).getCell(4).toString());
				}
				
				if(planilha.getRow(i).getCell(5) != null){
					planilha.getRow(i).getCell(5).setCellType(CellType.STRING);
					resumoDnsBean.setRedn_ds_sourcelocationid(planilha.getRow(i).getCell(5).toString());
				}
				
				if(planilha.getRow(i).getCell(6) != null){
					resumoDnsBean.setRedn_ds_sourcelocationame(planilha.getRow(i).getCell(6).toString());
				}
				
				if(planilha.getRow(i).getCell(7) != null){
					planilha.getRow(i).getCell(7).setCellType(CellType.STRING);
					resumoDnsBean.setRedn_ds_destinationlocationid(planilha.getRow(i).getCell(7).toString());
				}
				
				if(planilha.getRow(i).getCell(8) != null){
					resumoDnsBean.setRedn_ds_destinationlocationname(planilha.getRow(i).getCell(8).toString());
				}
				
				if(planilha.getRow(i).getCell(9) != null){
					resumoDnsBean.setRedn_ds_destinationcity(planilha.getRow(i).getCell(9).toString());
				}
				
				if(planilha.getRow(i).getCell(10) != null){
					resumoDnsBean.setRedn_ds_destinationprovincecode(planilha.getRow(i).getCell(10).toString());
				}
				
				if(planilha.getRow(i).getCell(11) != null){
					resumoDnsBean.setRedn_ds_grossweight(planilha.getRow(i).getCell(11).toString());
				}
				
				if(planilha.getRow(i).getCell(12) != null){
					resumoDnsBean.setRedn_ds_grossvolume(planilha.getRow(i).getCell(12).toString());
				}
				
				if(planilha.getRow(i).getCell(13) != null){
					planilha.getRow(i).getCell(13).setCellType(CellType.STRING);
					resumoDnsBean.setRedn_ds_cases(planilha.getRow(i).getCell(13).toString());
				}
				
				if(planilha.getRow(i).getCell(14) != null){
					resumoDnsBean.setRedn_ds_projetos(planilha.getRow(i).getCell(14).toString());
				}
				
				if(planilha.getRow(i).getCell(15) != null){
					resumoDnsBean.setRedn_ds_modelplann(planilha.getRow(i).getCell(15).toString()); 
				}
				
				if(planilha.getRow(i).getCell(16) != null){
					resumoDnsBean.setRedn_ds_equipamentogroup(planilha.getRow(i).getCell(16).toString());
				}
								
				// VERIFICAR FINAL DO ARQUIVO
				if(planilha.getRow(i).getCell(0).toString().equalsIgnoreCase("") && planilha.getRow(i).getCell(1).toString().equalsIgnoreCase("") 
						&& planilha.getRow(i).getCell(2).toString().equalsIgnoreCase("") && planilha.getRow(i).getCell(4).toString().equalsIgnoreCase("")) {
					// FIM DO ARQUIVO IDENTIFICADO (NAO ADD NA LISTA)
					return lstResumoDns;
				}
				
				// ADICIONAR BEAN NA LISTA
				lstResumoDns.add(resumoDnsBean);
				
			} catch (Exception e) {
				log.info("[setListaResumoDns] ERRO AO EFETUAR LEITURA DA LINHA: " + i+1 + " " + e.getMessage());
			}

		}

		return lstResumoDns;
		
	}

	/**
	 * Metodo responsavel por obter os valores da planilha e retornar a lista de CsLgtbShipmentShipBean preenchida.
	 * @author Caio Fernandes
	 * @param List<CsLgtbShipmentShipBean>, Sheet
	 * @return List<CsLgtbShipmentShipBean>
	 * @throws ParseException 
	 * */
	private List<CsLgtbShipmentShipBean> setListaShipment(List<CsLgtbShipmentShipBean> lstShipmentBean, Sheet planilha) throws ParseException {
		
		UtilDate util = new UtilDate();
		Logger log = Logger.getLogger(this.getClass().getName());

		// FOR DAS LINHAS DO ARQUIVO
		for (int i = 1; i < planilha.getLastRowNum()+1 ; i++){
			
			// POPULAR BEAN
			CsLgtbShipmentShipBean shipmentBean = new CsLgtbShipmentShipBean();
			
			if(planilha.getRow(i).getCell(0) != null){
				// SHIP_CD_SHIPMENTID
				planilha.getRow(i).getCell(0).setCellType(CellType.STRING);
				shipmentBean.setShip_cd_shipmentid(planilha.getRow(i).getCell(0).toString());
			}
			
			if(planilha.getRow(i).getCell(1) != null){
				// SHIP_DS_STOPTYPE
				shipmentBean.setShip_ds_stoptype(planilha.getRow(i).getCell(1).toString());
			}
			
			if(planilha.getRow(i).getCell(2) != null){
				// SHIP_DS_STOPNUMBER
				planilha.getRow(i).getCell(2).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_stopnumber(planilha.getRow(i).getCell(2).toString());
			}
			
			if(planilha.getRow(i).getCell(3) != null){
				// SHIP_DS_TOTALSTOPS
				
				planilha.getRow(i).getCell(3).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_totalstops(planilha.getRow(i).getCell(3).toString());
			}
			
			if(planilha.getRow(i).getCell(4) != null){
				// SHIP_CD_SERVICEPROVIDERID
				shipmentBean.setShip_cd_serviceproviderid(planilha.getRow(i).getCell(4).toString());
			}
						
			if(planilha.getRow(i).getCell(5) != null){
				// SHIP_DS_SERVICEPROVIDERID
				shipmentBean.setShip_ds_serviceproviderid(planilha.getRow(i).getCell(5).toString());
			}
			
			if(planilha.getRow(i).getCell(6) != null){
				// SHIP_DS_EQUIPMENTGROUP
				shipmentBean.setShip_ds_equipmentgroup(planilha.getRow(i).getCell(6).toString());
			}
			
			if(planilha.getRow(i).getCell(7) != null){
				// SHIP_DS_SOURCELOCATIONID
				planilha.getRow(i).getCell(7).setCellType(CellType.STRING);
				
				String sourceLocationId = "";
				if(planilha.getRow(i).getCell(7).toString().contains(".")) {
					sourceLocationId = planilha.getRow(i).getCell(7).toString().replace(".", "").trim();
				} else {
					sourceLocationId = planilha.getRow(i).getCell(7).toString();
				}
				
				shipmentBean.setShip_ds_sourcelocationid(sourceLocationId); 
			}
			
			if(planilha.getRow(i).getCell(8) != null){
				// SHIP_DS_SOURCELOCATIONNAME
				shipmentBean.setShip_ds_sourcelocationname(planilha.getRow(i).getCell(8).toString());
			}
			
			if(planilha.getRow(i).getCell(9) != null){
				// SHIP_DS_SOURCECITY
				shipmentBean.setShip_ds_sourcecity(planilha.getRow(i).getCell(9).toString());
			}
			
			if(planilha.getRow(i).getCell(10) != null){
				// SHIP_DS_SOURCEPROVINCECODE
				shipmentBean.setShip_ds_sourceprovincecode(planilha.getRow(i).getCell(10).toString());
			}
			
			if(planilha.getRow(i).getCell(11) != null){
				// SHIP_DS_SOURCEZONE1
				shipmentBean.setShip_ds_sourcezone1(planilha.getRow(i).getCell(11).toString());
			}
			
			if(planilha.getRow(i).getCell(12) != null){
				// SHIP_DS_DESTLOCATIONID
				planilha.getRow(i).getCell(12).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_destlocationid(planilha.getRow(i).getCell(12).toString()); 
			}
			
			if(planilha.getRow(i).getCell(13) != null){
				// SHIP_DS_DESTLOCATIONNAME
				shipmentBean.setShip_ds_destlocationname(planilha.getRow(i).getCell(13).toString());
			}
			
			if(planilha.getRow(i).getCell(14) != null){
				// SHIP_DS_DESTCITY
				shipmentBean.setShip_ds_destcity(planilha.getRow(i).getCell(14).toString());
			}
			
			if(planilha.getRow(i).getCell(15) != null){
				// SHIP_DS_PROVINCECODE
				shipmentBean.setShip_ds_provincecode(planilha.getRow(i).getCell(15).toString());
			}
			
			if(planilha.getRow(i).getCell(16) != null){
				// SHIP_DS_DESTZONE1
				shipmentBean.setShip_ds_destzone1(planilha.getRow(i).getCell(16).toString());
			}
			
			if(planilha.getRow(i).getCell(17) != null){
				// SHIP_DS_STOPDISTANCE
				shipmentBean.setShip_ds_stopdistance(planilha.getRow(i).getCell(17).toString());
			}
			
			if(planilha.getRow(i).getCell(18) != null){
				// SHIP_DS_STOPDISTANCEUOM
				shipmentBean.setShip_ds_stopdistanceuom(planilha.getRow(i).getCell(18).toString());
			}
			
			if(planilha.getRow(i).getCell(19) != null){
				// SHIP_DS_STARTTIME				
				shipmentBean.setShip_ds_starttime(planilha.getRow(i).getCell(19).getDateCellValue());
			}
			
			if(planilha.getRow(i).getCell(20) != null){
				// SHIP_DS_WEEK
				planilha.getRow(i).getCell(20).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_week(planilha.getRow(i).getCell(20).toString());
			}
			
			if(planilha.getRow(i).getCell(21) != null){
				// SHIP_DS_MONTH
				shipmentBean.setShip_ds_month(planilha.getRow(i).getCell(21).toString());
			}
			
			if(planilha.getRow(i).getCell(22) != null){
				// SHIP_DS_DECLAREDVALUE
				shipmentBean.setShip_ds_declaredvalue(planilha.getRow(i).getCell(22).toString());
			}
			
			if(planilha.getRow(i).getCell(23) != null){
				// SHIP_DS_NETFREIGHTAMOUNT
				shipmentBean.setShip_ds_netfreightamount(planilha.getRow(i).getCell(23).toString());
			}

			if(planilha.getRow(i).getCell(24) != null){
				// SHIP_DS_NUMBERORDERRELEASES
				planilha.getRow(i).getCell(24).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_numberorderreleases(planilha.getRow(i).getCell(24).toString()); 
			}

			if(planilha.getRow(i).getCell(25) != null){
				// SHIP_DS_GROSSWEIGHT
				shipmentBean.setShip_ds_grossweight(planilha.getRow(i).getCell(25).toString());
			}
			
			if(planilha.getRow(i).getCell(26) != null){
				// SHIP_DS_TOTALGROSSWEIGHT
				shipmentBean.setShip_ds_totalgrossweight(planilha.getRow(i).getCell(26).toString());
			}
			
			if(planilha.getRow(i).getCell(27) != null){
				// SHIP_DS_GROSSVOLUME
				shipmentBean.setShip_ds_grossvolume(planilha.getRow(i).getCell(27).toString());
			}
			
			if(planilha.getRow(i).getCell(28) != null){
				// SHIP_DS_TOTALGROSSVOLUME
				shipmentBean.setShip_ds_totalgrossvolume(planilha.getRow(i).getCell(28).toString());
			}
			
			if(planilha.getRow(i).getCell(29) != null){
				// SHIP_DS_CUBEDWEIGHT
				shipmentBean.setShip_ds_cubedweight(planilha.getRow(i).getCell(29).toString());
			}
			
			if(planilha.getRow(i).getCell(30) != null){
				// SHIP_DS_CASES
				planilha.getRow(i).getCell(30).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_cases(planilha.getRow(i).getCell(30).toString()); 
			}
			
			if(planilha.getRow(i).getCell(31) != null){
				// SHIP_DS_VOLUMEVEHICLECAPACITY
				planilha.getRow(i).getCell(31).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_volumevehiclecapacity(planilha.getRow(i).getCell(31).toString()); 
			}
			
			if(planilha.getRow(i).getCell(32) != null){
				// SHIP_DS_VOLUMEVEHICLECAPCTYUOMCODE
				shipmentBean.setShip_ds_volumevehiclecapctyuomcode(planilha.getRow(32).getCell(32).toString());
			}
			
			if(planilha.getRow(i).getCell(33) != null){
				// SHIP_DS_WEIGHTVEHICLECAPACITY
				planilha.getRow(i).getCell(33).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_weightvehiclecapacity(planilha.getRow(i).getCell(33).toString()); 
			}
			
			if(planilha.getRow(i).getCell(34) != null){
				// SHIP_DS_WEIGHTVEHICLECAPCTYWOMCODE
				shipmentBean.setShip_ds_weightvehiclecapctywomcode(planilha.getRow(i).getCell(34).toString());
			}
			
			if(planilha.getRow(i).getCell(35) != null){
				// SHIP_DS_VOLUMEVEHICLEOCCUPANCY
				shipmentBean.setShip_ds_volumevehicleoccupancy(planilha.getRow(i).getCell(35).toString()); 
			}
			
			if(planilha.getRow(i).getCell(36) != null){
				// SHIP_DS_WEIGHTVEHICLEOCCUPANCY
				shipmentBean.setShip_ds_weightvehicleoccupancy(planilha.getRow(i).getCell(36).toString());
			}
			
			if(planilha.getRow(i).getCell(37) != null){
				// SHIP_DS_CORECARRIER
				shipmentBean.setShip_ds_corecarrier(planilha.getRow(i).getCell(37).toString());
			}
			
			if(planilha.getRow(i).getCell(38) != null){
				// SHIP_DS_PLANNEDSCAC
				shipmentBean.setShip_ds_plannedscac(planilha.getRow(i).getCell(38).toString());
			}
			
			if(planilha.getRow(i).getCell(39) != null){
				// SHIP_DS_PLANNEDSERVICEPROVIDERNAME
				shipmentBean.setShip_ds_plannedserviceprovidername(planilha.getRow(i).getCell(39).toString());
			}
			
			if(planilha.getRow(i).getCell(40) != null){
				// SHIP_DS_BULKPLANGID
				shipmentBean.setShip_ds_bulkplangid(planilha.getRow(i).getCell(40).toString());
			}
			
			if(planilha.getRow(i).getCell(41) != null){
				// SHIP_DS_STATUSOFERTA
				shipmentBean.setShip_ds_statusoferta(planilha.getRow(i).getCell(41).toString());
			}
			
			if(planilha.getRow(i).getCell(42) != null){
				// SHIP_DS_BUYSHIPMENT
				shipmentBean.setShip_ds_buyshipment(planilha.getRow(i).getCell(42).toString());
			}
			
			if(planilha.getRow(i).getCell(43) != null){
				// SHIP_DS_TRUCKOUCARRETA
				shipmentBean.setShip_ds_truckoucarreta(planilha.getRow(i).getCell(43).toString());
			}
			
			if(planilha.getRow(i).getCell(44) != null){
				// SHIP_DS_CLIENTEAGENDADO
				shipmentBean.setShip_ds_clienteagendado(planilha.getRow(i).getCell(44).toString());
			}
			
			if(planilha.getRow(i).getCell(45) != null){
				// SHIP_DS_AGENDAMENTOCOMNF
				shipmentBean.setShip_ds_agendamentocomnf(planilha.getRow(i).getCell(45).toString());
			}
			
			// PULAR A CELULA 46 POIS O ARQUIVO VEIO ASSIM
			
			if(planilha.getRow(i).getCell(47) != null){
				// SHIP_DS_TIPO 
				shipmentBean.setShip_ds_tipo(planilha.getRow(i).getCell(47).toString());
			}

			
			if(planilha.getRow(i).getCell(48) != null){
				// SHIP_DS_TRANSPORTADORA
				shipmentBean.setShip_ds_transportadora(planilha.getRow(i).getCell(48).toString());
			}

			
			if(planilha.getRow(i).getCell(49) != null){
				// SHIP_DS_CONTANDESHIPMENTS
				planilha.getRow(i).getCell(49).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_contandeshipments(planilha.getRow(i).getCell(49).toString()); 
			}

			
			if(planilha.getRow(i).getCell(50) != null){
				// SHIP_DS_FAIXA
				shipmentBean.setShip_ds_faixa(planilha.getRow(i).getCell(50).toString());
			}

			
			if(planilha.getRow(i).getCell(51) != null){
				// SHIP_DS_CHAVESHIPMENT
				shipmentBean.setShip_ds_chaveshipment(planilha.getRow(i).getCell(51).toString());
			}

			
			if(planilha.getRow(i).getCell(52) != null){
				// SHIP_DS_SHIPMENTS
				planilha.getRow(i).getCell(52).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_shipments(planilha.getRow(i).getCell(52).toString()); 
			}
			
			if(planilha.getRow(i).getCell(53) != null){
				// SHIP_DS_PLANTA
				planilha.getRow(i).getCell(53).setCellType(CellType.STRING);
				shipmentBean.setShip_ds_planta(planilha.getRow(i).getCell(53).toString()); 
			}
			
			if(planilha.getRow(i).getCell(54) != null){
				// SHIP_CD_CODCLIENTE
				planilha.getRow(i).getCell(54).setCellType(CellType.STRING);
				shipmentBean.setShip_cd_codcliente(planilha.getRow(i).getCell(54).toString()); 
			}
			
			if(planilha.getRow(i).getCell(55) != null){
				// SHIP_DS_CLIENTE
				shipmentBean.setShip_ds_cliente(planilha.getRow(i).getCell(55).toString());
			}
			
			if(planilha.getRow(i).getCell(56) != null){
				// SHIP_DS_PESO
				planilha.getRow(i).getCell(56).setCellType(CellType.NUMERIC);
				shipmentBean.setShip_ds_peso(planilha.getRow(i).getCell(56).toString()); 
			}
			
			if(planilha.getRow(i).getCell(57) != null){
				// SHIP_DS_LIBERARCROSSDOCKING
				shipmentBean.setShip_ds_liberarcrossdocking(planilha.getRow(i).getCell(57).toString());
			}
			
			// VERIFICAR FINAL DO ARQUIVO
			if(planilha.getRow(i).getCell(0).toString().equalsIgnoreCase("") && planilha.getRow(i).getCell(1).toString().equalsIgnoreCase("") 
					&& planilha.getRow(i).getCell(2).toString().equalsIgnoreCase("") && planilha.getRow(i).getCell(4).toString().equalsIgnoreCase("")) {
				// FIM DO ARQUIVO IDENTIFICADO (NAO ADD NA LISTA)
				return lstShipmentBean;
			}
			
			// ADICIONAR BEAN NA LISTA
			lstShipmentBean.add(shipmentBean);

		}

		return lstShipmentBean;
	}

	public String validaCarga() throws JsonProcessingException, SQLException {
	
		String retorno = null;
		
		RetornoValidaCargaBean retCarga = new RetornoValidaCargaBean();		
		CsLgtbShipmentShipDao shipDao = new CsLgtbShipmentShipDao();		
		
		// PREENCHIMENTO DE QUANTIDADE DE ARQUIVOS IMPORTADOS
		retCarga = shipDao.qntdEmba();				
		
		retCarga = shipDao.validaCarga(retCarga);			
		
		ObjectMapper mapper = new ObjectMapper();
		retorno = mapper.writeValueAsString(retCarga);
		
		
		return retorno;
	}
	

}
