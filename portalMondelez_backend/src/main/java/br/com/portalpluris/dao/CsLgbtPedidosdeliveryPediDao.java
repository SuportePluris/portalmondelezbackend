package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgbtPedidosdeliveryPediBean;

public class CsLgbtPedidosdeliveryPediDao {

	/**
	 * Metodo responsavel por realizar o insert das informacoes da planilha de pedidos delivery na tabela CS_LGTB_PEDIDOSDELIVERY_PEDI
	 * @author Caio Fernandes
	 * @param List<CsLgbtPedidosdeliveryPediBean> lstPedidoDeliveryBean
	 * */
	public void insertPedidosDelivery(List<CsLgbtPedidosdeliveryPediBean> lstPedidoDeliveryBean, String nmArquivo) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		UtilDate util = new UtilDate();
		
		log.info("[insertPedidosDelivery] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstPedidoDeliveryBean.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertPedidosDelivery] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" INSERT CS_LGTB_PEDIDOSDELIVERY_PEDI ( "
						+ "		PEDI_CD_SALESDOCUMENT, "
						+ "		PEDI_DS_NETVALUE, 	"
						+ "		PEDI_DS_PURCHASEORDERNUMBER, 	"
						+ " 	PEDI_DH_CREATEDON, 	"
						+ " 	PEDI_DS_CREATEDBY,	"
						+ " 	PEDI_DS_SOLDTOPARTY,	"
						+ " 	PEDI_DS_SALESDOCUMENTTYPE, "
						+ " 	PEDI_DH_DOCUMENTDATE, "
						+ " 	PEDI_DS_DISTRIBUTIONCHANNEL, "
						+ " 	PEDI_DS_DOCUMENTCURRENCY, "
						+ " 	PEDI_DS_SALESORGANIZATION, "
						+ " 	PEDI_DS_NOMEARQUIVO )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?,?,?,?)	"
						+ "");
				
				pst.setString(1, lstPedidoDeliveryBean.get(i).getPedi_cd_salesdocument());
				pst.setString(2, lstPedidoDeliveryBean.get(i).getPedi_ds_netvalue());
				pst.setString(3, lstPedidoDeliveryBean.get(i).getPedi_ds_purchaseordernumber());
				pst.setString(4, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstPedidoDeliveryBean.get(i).getPedi_dh_createdon()));
				pst.setString(5, lstPedidoDeliveryBean.get(i).getPedi_ds_createdby());
				pst.setString(6, lstPedidoDeliveryBean.get(i).getPedi_ds_soldtoparty());
				pst.setString(7, lstPedidoDeliveryBean.get(i).getPedi_ds_salesdocumenttype());
				pst.setString(8, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstPedidoDeliveryBean.get(i).getPedi_dh_documentdate()));
				pst.setString(9, lstPedidoDeliveryBean.get(i).getPedi_ds_distributionchannel());
				pst.setString(10, lstPedidoDeliveryBean.get(i).getPedi_ds_documentcurrency());
				pst.setString(11, lstPedidoDeliveryBean.get(i).getPedi_ds_salesorganization());
				pst.setString(12, nmArquivo);
				
				pst.execute();
				pst.close();
				
				log.info("[insertPedidosDelivery] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[insertPedidosDelivery] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		//conn.close();
		
		log.info("[insertPedidosDelivery] - FIM ");
		
	}

}
