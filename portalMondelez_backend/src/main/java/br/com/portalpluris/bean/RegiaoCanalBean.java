package br.com.portalpluris.bean;

public class RegiaoCanalBean {

	int indice;
	String urcs_id_regi_cd_regiao;
	String regi_ds_regiao;
	String urcs_id_cd_canal;
	String cana_ds_canal;
	public int getIndice() {
		return indice;
	}
	public void setIndice(int indice) {
		this.indice = indice;
	}
	public String getUrcs_id_regi_cd_regiao() {
		return urcs_id_regi_cd_regiao;
	}
	public void setUrcs_id_regi_cd_regiao(String urcs_id_regi_cd_regiao) {
		this.urcs_id_regi_cd_regiao = urcs_id_regi_cd_regiao;
	}
	public String getRegi_ds_regiao() {
		return regi_ds_regiao;
	}
	public void setRegi_ds_regiao(String regi_ds_regiao) {
		this.regi_ds_regiao = regi_ds_regiao;
	}
	public String getUrcs_id_cd_canal() {
		return urcs_id_cd_canal;
	}
	public void setUrcs_id_cd_canal(String urcs_id_cd_canal) {
		this.urcs_id_cd_canal = urcs_id_cd_canal;
	}
	public String getCana_ds_canal() {
		return cana_ds_canal;
	}
	public void setCana_ds_canal(String cana_ds_canal) {
		this.cana_ds_canal = cana_ds_canal;
	}
	
}
