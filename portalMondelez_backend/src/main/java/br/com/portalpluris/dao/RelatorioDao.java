package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.CsNgtbDataagendamentoDaagBean;
import br.com.portalpluris.bean.MotivoResponsavelCutoffBean;
import br.com.portalpluris.bean.MotivoResponsavelDesvioLTBean;
import br.com.portalpluris.bean.RelatorioBean;
import br.com.portalpluris.bean.relatorioOcrBean;

public class RelatorioDao {
	
	
	public List<RelatorioBean> getRelatorioDaoNovo(String periodo, String ano, String transp) throws SQLException, InterruptedException {
		List<RelatorioBean> retorno = new ArrayList<RelatorioBean>();
		Connection conn = DAO.getConexao();
		
		
		
		PreparedStatement pst;
		ResultSet rs = null;
		
		String cSQL = "";
		cSQL += " SELECT DISTINCT   ";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,   ";
		cSQL += "  ";
		cSQL += "  ";
		cSQL += " ISNULL((SELECT TOP 1  ";
		cSQL += " 	ISNULL(CONVERT(VARCHAR,FORMAT(DAAG_DH_AGENDAMENTO, 'dd/MM/yyyy')),'') DATA_AGENDAMENTO ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBAI ";
		cSQL += " 	ON EMBAI.EMBA_ID_CD_EMBARQUE = DAAG.DAAG_ID_CD_EMBARQUE ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Agendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	AND EMBAI.EMBA_ID_CD_STATUS IN (5,8) ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC),'') AS DAAG_DH_AGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	ISNULL((SELECT TOP 1  ";
		cSQL += " 	ISNULL(CONVERT(VARCHAR,FORMAT(DAAG_DH_REGISTRO, 'dd/MM/yyyy HH:mm')),'') DATA_INPUT_AGENDAMENTO  ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBAI ";
		cSQL += " 	ON EMBAI.EMBA_ID_CD_EMBARQUE = DAAG.DAAG_ID_CD_EMBARQUE ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Agendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	AND EMBAI.EMBA_ID_CD_STATUS IN (5,8) ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC),'') AS DAAG_DH_REGISTRO, ";
		cSQL += "  ";
		cSQL += "  ISNULL((SELECT TOP 1  ";
		cSQL += " 	DAAG_DS_HORAAGENDAMENTO ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBAI ";
		cSQL += " 	ON EMBAI.EMBA_ID_CD_EMBARQUE = DAAG.DAAG_ID_CD_EMBARQUE ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Agendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	AND EMBAI.EMBA_ID_CD_STATUS IN (5,8) ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC),'') AS HORA_AGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	ISNULL((SELECT TOP 1  ";
		cSQL += " 	DAAG_DS_SENHAAGENDAMENTO ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBAI ";
		cSQL += " 	ON EMBAI.EMBA_ID_CD_EMBARQUE = DAAG.DAAG_ID_CD_EMBARQUE ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Agendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	AND EMBAI.EMBA_ID_CD_STATUS IN (5,8) ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC),'') AS SENHA_AGENDAMENTO, ";
		cSQL += "  ";
		cSQL += "  ";
		cSQL += " 	ISNULL((SELECT TOP 1  ";
		cSQL += "  ";
		cSQL += " 	ISNULL(CONVERT(VARCHAR,FORMAT(DAAG_DH_AGENDAMENTO, 'dd/MM/yyyy')),'') DAAG_DH_AGENDAMENTO ";
		cSQL += "  ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = DAAG.DAAG_ID_CD_MOTIVO  ";
		cSQL += " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = DAAG.DAAG_ID_CD_RESPONSAVEL  ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Retificação', 'Reagendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC),'') AS DATA_REAGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	(SELECT TOP 1  ";
		cSQL += " 	ISNULL(CONVERT(VARCHAR,FORMAT(DAAG_DH_REGISTRO, 'dd/MM/yyyy HH:mm')),'') DAAG_DH_REGISTRO ";
		cSQL += " 	 ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = DAAG.DAAG_ID_CD_MOTIVO  ";
		cSQL += " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = DAAG.DAAG_ID_CD_RESPONSAVEL  ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Retificação', 'Reagendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC) AS DATA_INPUT_REAGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	ISNULL((SELECT TOP 1  ";
		cSQL += " 	ISNULL(DAAG_DS_HORAAGENDAMENTO,'') AS DAAG_DS_HORAAGENDAMENTO";
		cSQL += " 	 ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = DAAG.DAAG_ID_CD_MOTIVO  ";
		cSQL += " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = DAAG.DAAG_ID_CD_RESPONSAVEL  ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Retificação', 'Reagendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC),'') AS HORA_REAGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	(SELECT TOP 1  ";
		cSQL += " 	 ";
		cSQL += " 	DAAG_DS_SENHAAGENDAMENTO ";
		cSQL += " 	 ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = DAAG.DAAG_ID_CD_MOTIVO  ";
		cSQL += " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = DAAG.DAAG_ID_CD_RESPONSAVEL  ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Retificação', 'Reagendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC) AS SENHA_REAGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	(SELECT TOP 1  ";
		cSQL += " 	 ";
		cSQL += " 	 ";
		cSQL += " 	 ";
		cSQL += " 	MOTI.MOTI_DS_MOTIVO ";
		cSQL += " 	 ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = DAAG.DAAG_ID_CD_MOTIVO  ";
		cSQL += " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = DAAG.DAAG_ID_CD_RESPONSAVEL  ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Retificação', 'Reagendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC) AS MOTIVO_REAGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	(SELECT TOP 1  ";
		cSQL += " 	RESP.RESP_NM_RESPONSAVEL  ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = DAAG.DAAG_ID_CD_MOTIVO  ";
		cSQL += " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = DAAG.DAAG_ID_CD_RESPONSAVEL  ";
		cSQL += " 	WHERE   ";
		cSQL += " 	DAAG_DS_TIPOAGENDAMENTO IN ('Retificação', 'Reagendamento')  ";
		cSQL += " 	AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC) AS RESPONSAVEL_REAGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	(SELECT TOP 1  ";
		cSQL += " 	CONVERT(VARCHAR,FORMAT(EMFO.EMFO_DH_REGISTRO, 'dd/MM/yyyy HH:mm')) as DT_OCR_AGENDAMENTO ";
		cSQL += "  ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK) ";
		cSQL += " 	INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK)  ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = EMFO.EMFO_ID_CD_MOTIVO  ";
		cSQL += " 	INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK)  ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = EMFO.EMFO_ID_CD_MOTIVO  ";
		cSQL += " 	WHERE EMFO_ID_CD_FOLLOWUP IN  ";
		cSQL += " 	(11,12,13,14,15,17,18)  ";
		cSQL += " 	AND EMFO.EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE  ";
		cSQL += " 	ORDER BY EMFO_DH_REGISTRO DESC ) AS DT_OCR_AGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	(SELECT TOP 1  ";
		cSQL += "  ";
		cSQL += " 	MOTI.MOTI_DS_MOTIVO AS MOTIVO_OCR_AGEND ";
		cSQL += "  ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK) ";
		cSQL += " 	INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK)  ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = EMFO.EMFO_ID_CD_MOTIVO  ";
		cSQL += " 	INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK)  ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = EMFO.EMFO_ID_CD_MOTIVO  ";
		cSQL += " 	WHERE EMFO_ID_CD_FOLLOWUP IN  ";
		cSQL += " 	(11,12,13,14,15,17,18)  ";
		cSQL += " 	AND EMFO.EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE  ";
		cSQL += " 	ORDER BY EMFO_DH_REGISTRO DESC ) AS MOTIVO_OCR_AGEND, ";
		cSQL += "  ";
		cSQL += " 	(SELECT TOP 1  ";
		cSQL += "  ";
		cSQL += " 	RESP.RESP_NM_RESPONSAVEL AS RESPONSAVEL_PENDENCIA  ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK) ";
		cSQL += " 	INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK)  ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = EMFO.EMFO_ID_CD_MOTIVO  ";
		cSQL += " 	INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK)  ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = EMFO.EMFO_ID_CD_MOTIVO  ";
		cSQL += " 	WHERE EMFO_ID_CD_FOLLOWUP IN  ";
		cSQL += " 	(11,12,13,14,15,17,18)  ";
		cSQL += " 	AND EMFO.EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE  ";
		cSQL += " 	ORDER BY EMFO_DH_REGISTRO DESC ) AS RESPONSAVEL_PENDENCIA, ";
		cSQL += "  ";
		cSQL += " 	(SELECT  ";
		cSQL += " 	ISNULL(DATEDIFF(DAY,INI, FIM),0) AS AGING  ";
		cSQL += "   ";
		cSQL += " 	FROM  ";
		cSQL += " 	(  ";
		cSQL += " 	SELECT  ";
		cSQL += " 		(  ";
		cSQL += " 		SELECT TOP 1    ";
		cSQL += " 		EMFO_DH_REGISTRO  ";
		cSQL += " 		FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK) ";
		cSQL += " 		WHERE EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 		AND EMFO_ID_CD_FOLLOWUP IN (7,8,9)  ";
		cSQL += " 		ORDER BY EMFO_DH_REGISTRO ASC  ";
		cSQL += " 		) AS INI,  ";
		cSQL += " 		(  ";
		cSQL += " 		SELECT TOP 1    ";
		cSQL += " 		EMFO_DH_REGISTRO  ";
		cSQL += " 		FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK)  ";
		cSQL += " 		WHERE EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 		AND EMFO_ID_CD_FOLLOWUP IN (19)  ";
		cSQL += " 		ORDER BY EMFO_DH_REGISTRO ASC  ";
		cSQL += " 		) AS FIM  ";
		cSQL += " 	) AS T) AS AGING_AGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " 	(SELECT  ";
		cSQL += " 	ISNULL(DATEDIFF(DAY,INI, FIM),0) AS AGING  ";
		cSQL += "   ";
		cSQL += " 	FROM  ";
		cSQL += " 	(  ";
		cSQL += " 	SELECT  ";
		cSQL += " 		(  ";
		cSQL += " 		SELECT TOP 1    ";
		cSQL += " 		EMFO_DH_REGISTRO  ";
		cSQL += " 		FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK) ";
		cSQL += " 		WHERE EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 		AND EMFO_ID_CD_FOLLOWUP IN (7,8,9)  ";
		cSQL += " 		ORDER BY EMFO_DH_REGISTRO ASC  ";
		cSQL += " 		) AS INI,  ";
		cSQL += " 		(  ";
		cSQL += " 		SELECT TOP 1    ";
		cSQL += " 		EMFO_DH_REGISTRO  ";
		cSQL += " 		FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK)  ";
		cSQL += " 		WHERE EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 		AND EMFO_ID_CD_FOLLOWUP IN (20)  ";
		cSQL += " 		ORDER BY EMFO_DH_REGISTRO ASC  ";
		cSQL += " 		) AS FIM  ";
		cSQL += " 	) AS T) AS AGING_REAGENDAMENTO, ";
		cSQL += "  ";
		cSQL += " ( ";
		cSQL += " 	SELECT  ";
		cSQL += " 	ISNULL(DATEDIFF(DAY,(  ";
		cSQL += " 	SELECT TOP 1     ";
		cSQL += " 	EMFO_DH_REGISTRO   ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK)  ";
		cSQL += " 	WHERE EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	AND EMFO_ID_CD_FOLLOWUP IN (7,8,9)   ";
		cSQL += " 	ORDER BY EMFO_DH_REGISTRO ASC  ";
		cSQL += " 	),  ";
		cSQL += " 	(  ";
		cSQL += " 	SELECT TOP 1  ";
		cSQL += " 	DAAG_DH_REGISTRO  ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG (NOLOCK)  ";
		cSQL += " 	WHERE DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE  ";
		cSQL += " 	AND DAAG_DS_TIPOAGENDAMENTO = 'Agendamento'  ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC  ";
		cSQL += " 	)),0) AS TMCA ";
		cSQL += " ) AS TMCA, ";
		cSQL += "  ";
		cSQL += " ( ";
		cSQL += " 	SELECT  ";
		cSQL += " 		CASE WHEN ISNULL(DATEDIFF(DAY,INI,FIM),0) > 0 THEN 'Fora do prazo' ELSE 'Dentro do Prazo' END AS SCHEDULING ";
		cSQL += " 		FROM  ";
		cSQL += " 		(  ";
		cSQL += " 		SELECT  ";
		cSQL += " 		(  ";
		cSQL += " 		SELECT TOP 1  ";
		cSQL += " 		DAAG_DH_REGISTRO  ";
		cSQL += " 		FROM CS_NGTB_DATAAGENDAMENTO_DAAG (NOLOCK) ";
		cSQL += " 		WHERE DAAG_DS_TIPOAGENDAMENTO = 'Agendamento'  ";
		cSQL += " 		AND DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 		ORDER BY DAAG_DH_REGISTRO ASC  ";
		cSQL += " 		) AS FIM,  ";
		cSQL += " 		(SELECT   ";
		cSQL += " 		EMBA_DH_SHIPMENT  ";
		cSQL += " 		FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA (NOLOCK) ";
		cSQL += " 		WHERE EMBA_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE) AS INI) AS T ";
		cSQL += " ) AS ON_TIME_SCHEDULING, ";
		cSQL += "  ";
		cSQL += " ( ";
		cSQL += " 	SELECT TOP 1     ";
		cSQL += " 	CONVERT(VARCHAR(18),EMFO_DH_REGISTRO,103) AS DH_REGISTRO   ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK)  ";
		cSQL += " 	WHERE EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	AND EMFO_ID_CD_FOLLOWUP IN (7,8,9)   ";
		cSQL += " 	ORDER BY EMFO_DH_REGISTRO ASC  ";
		cSQL += " ) AS DT_PRIMEIRO_CONTATO, ";
		cSQL += "  ";
		cSQL += " ( ";
		cSQL += " 	SELECT  ";
		cSQL += " 	ISNULL(DATEDIFF(DAY,DH_LEADTIME, DH_AGENDA),0) AS DESVIO_LT  ";
		cSQL += " 	FROM  ";
		cSQL += " 	(  ";
		cSQL += " 	SELECT  ";
		cSQL += " 	(  ";
		cSQL += " 	SELECT TOP 1  ";
		cSQL += " 	DAAG_DH_AGENDAMENTO  ";
		cSQL += " 	FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) ";
		cSQL += " 	WHERE DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	ORDER BY DAAG_DH_REGISTRO DESC) AS DH_AGENDA,  ";
		cSQL += " 	(  ";
		cSQL += " 	SELECT  TOP 1 ";
		cSQL += " 	EMBA_DH_LEADTIME  ";
		cSQL += " 	FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA (NOLOCK) ";
		cSQL += " 	WHERE EMBA_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE) AS DH_LEADTIME) AS T  ";
		cSQL += " ) AS DESVIO_LEADTIME, ";
		cSQL += "  ";
		cSQL += " ( ";
		cSQL += " 	SELECT  ";
		cSQL += " 	MOTI.MOTI_DS_MOTIVO ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO  ";
		cSQL += " 	INNER JOIN CS_CDTB_FOLLOWUP_FOUP FOUP  ";
		cSQL += " 	ON FOUP.FOUP_ID_CD_FOLLOWUP = EMFO.EMFO_ID_CD_FOLLOWUP  ";
		cSQL += " 	INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = EMFO.EMFO_ID_CD_MOTIVO  ";
		cSQL += " 	INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = EMFO.EMFO_ID_CD_RESPONSAVEL  ";
		cSQL += " 	WHERE EMFO.EMFO_ID_CD_FOLLOWUP = 46  ";
		cSQL += " 	AND EMFO.EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " ) AS MOTIVO_DESVIO_LT, ";
		cSQL += "  ";
		cSQL += " ( ";
		cSQL += " 	SELECT  ";
		cSQL += " 	RESP.RESP_NM_RESPONSAVEL  ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO  ";
		cSQL += " 	INNER JOIN CS_CDTB_FOLLOWUP_FOUP FOUP  ";
		cSQL += " 	ON FOUP.FOUP_ID_CD_FOLLOWUP = EMFO.EMFO_ID_CD_FOLLOWUP  ";
		cSQL += " 	INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = EMFO.EMFO_ID_CD_MOTIVO  ";
		cSQL += " 	INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = EMFO.EMFO_ID_CD_RESPONSAVEL  ";
		cSQL += " 	WHERE EMFO.EMFO_ID_CD_FOLLOWUP = 46  ";
		cSQL += " 	AND EMFO.EMFO_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " ) AS RESPONSAVEL_DESVIO_LT, ";
		cSQL += " ( ";
		cSQL += " 	SELECT MOTI_DS_MOTIVO ";
		cSQL += " 	FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBAI ";
		cSQL += " 	INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = EMBAI.EMBA_ID_CD_MOTIVO ";
		cSQL += " 	INNER JOIN CS_ASTB_TIPOMOTIVO_TIMO TIMO ";
		cSQL += " 	ON MOTI.MOTI_ID_CD_MOTIVO = TIMO.MOTI_ID_CD_MOTIVO ";
		cSQL += " 	AND EMBAI.EMBA_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE ";
		cSQL += " 	AND TIMO.TIPO_ID_CD_TIPO = 3 ";
		cSQL += " ) AS MOTIVO_CUTOFF, ";
		
		cSQL += " (SELECT RESP_NM_RESPONSAVEL ";
		cSQL += " 	FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBAI ";
		cSQL += " 	INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP ";
		cSQL += " 	ON RESP.RESP_ID_CD_RESPONSAVEL = EMBAI.EMBA_ID_CD_RESPONSAVEL ";
		cSQL += " 	WHERE EMBAI.EMBA_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE) AS RESPONSAVEL_CUTOFF, ";
		cSQL += " EMBA_DS_TIPOAGENDAMENTO,   ";
		cSQL += " SKUD.SKUD_DS_PEDIDOMONDELEZ,   ";
		cSQL += " SKUD.SKUD_DS_PEDIDOCLIENTE,   ";
		cSQL += " SKUD.SKUD_ID_CD_DELIVERY,   ";
		cSQL += " SKUD.SKUD_DS_TIPOVENDA,   ";
		cSQL += " ISNULL(NOFI.NOFI_NR_NOTAFISCAL,'') AS NOFI_NR_NOTAFISCAL,   ";
		cSQL += " ISNULL(NOFI.NOFI_DS_SERIENOTA, '') AS NOFI_DS_SERIENOTA,   ";
		cSQL += " ISNULL(CONVERT(VARCHAR(10), CAST(NOFI.NOFI_DH_EMISSAONOTA AS DATETIME), 103),'') AS NOFI_DH_EMISSAONOTA,   ";
		cSQL += " EMBA.EMBA_DS_COD_TRANSPORTADORA,   ";
		cSQL += " TRANS.TRAN_NM_NOMETRANSPORTADORA,   ";
		cSQL += " EMBA.EMBA_IN_TRANSPORTADORACONFIRMADA,   ";
		cSQL += " EMBA.EMBA_DS_TIPOCARGA,   ";
		cSQL += " EMBA.EMBA_DS_VEICULO,   ";
		cSQL += " EMBA.EMBA_DS_TIPOVEICULO,   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,   ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMBA.EMBA_DH_SHIPMENT, 'dd/MM/yyyy HH:mm')) EMBA_DH_SHIPMENT,   ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE,   ";
		cSQL += " CLIE.CLIE_DS_CNPJ,   ";
		cSQL += " CLIE.CLIE_DS_NOMECLIENTE,   ";
		cSQL += " CLIE.CLIE_DS_CIDADE,   ";
		cSQL += " CLIE.CLIE_DS_UF,   ";
		cSQL += " EMBA.EMBA_DS_PLANTA,   ";
		cSQL += " CASE WHEN EMBA.EMBA_DS_TIPOCARGA = 'TL' THEN LDTM.LDTM_DS_TL + 1 ELSE LDTM.LDTM_DS_LTL + 1 END AS DIAS_LEADTIME,   ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMBA.EMBA_DH_LEADTIME, 'dd/MM/yyyy HH:mm')) EMBA_DH_LEADTIME,   ";
		cSQL += " STAT.STAT_DS_STATUS,   ";
		cSQL += " SUM(SKUD.SKUD_DS_PESOLIQUIDO) AS SKUD_DS_PESOLIQUIDO,   ";
		cSQL += " SUM(SKUD.SKUD_DS_VOLUMECUBICO) AS SKUD_DS_VOLUMECUBICO,   ";
		cSQL += " SUM(SKUD.SKUD_DS_VALOR) AS SKUD_DS_VALOR,   ";
		cSQL += " SUM(SKUD.SKUD_NR_QUANTIDADE) AS SKUD_NR_QUANTIDADE,   ";
		cSQL += " EMBA.EMBA_IN_CUTOFF,   ";
		cSQL += " EMBA.EMBA_ID_MOTIVOCUTOFF,   ";
		cSQL += " EMBA.EMBA_ID_RESPONSAVELCUTOFF,   ";
		cSQL += " MOTI.MOTI_DS_MOTIVO AS EMBA_DS_MOTIVOCUTOFF,  ";
		cSQL += " RESP.RESP_NM_RESPONSAVEL AS EMBA_DS_RESPONSAVELCUTOFF,  ";
		cSQL += " EMBA.EMBA_DS_PERIODO,   ";
		cSQL += " HQCL.HQCL_DS_GERENTENACIONAL,   ";
		cSQL += " HQCL.HQCL_DS_GERENTEREGIONAL,   ";
		cSQL += " HQCL.HQCL_DS_GERENTEAREA,   ";
		cSQL += " HQCL.HQCL_DS_VENDEDOR,   ";
		cSQL += " CONVERT(VARCHAR(10),SKUD.SKUD_DH_DELIVERY,103) AS SKUD_DH_DELIVERY,   ";
		cSQL += " CNCL.CNCL_DS_CHANEL,  ";
		cSQL += " CNCL.CNCL_DS_REGIAOCANAL  ";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA  (NOLOCK) ";
		cSQL += " INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD  (NOLOCK) ";
		cSQL += " ON SKUD.SKUD_ID_CD_SHIPMENT = EMBA.EMBA_ID_CD_SHIPMENT   ";
		cSQL += " AND SKUD.SKUD_ID_CD_CODCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE   ";
		cSQL += " LEFT JOIN CS_CDTB_NOTAFISCAL_NOFI NOFI  (NOLOCK) ";
		cSQL += " ON NOFI.NOFI_NR_DELIVERYDOCUMENT = SKUD.SKUD_ID_CD_DELIVERY   ";
		cSQL += " INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE  (NOLOCK) ";
		cSQL += " ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE   ";
		cSQL += " LEFT JOIN CS_CDTB_LEADTIME_LDTM LDTM  (NOLOCK) ";
		cSQL += " ON LDTM.LDTM_CD_SOURCELOCATION = EMBA.EMBA_DS_PLANTA AND LDTM.LDTM_DS_DESTINATIONCITY = CLIE.CLIE_DS_CIDADE   ";
		cSQL += " LEFT JOIN CS_CDTB_STATUSAGENDAMENTO_STAT STAT (NOLOCK)  ";
		cSQL += " ON STAT.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS   ";
		cSQL += " LEFT JOIN CS_CDTB_HIERARQUIACLIENTE_HQCL HQCL (NOLOCK)  ";
		cSQL += " ON HQCL.HQCL_ID_CLIENTE = CLIE.CLIE_ID_CD_CODIGOCLIENTE   ";
		cSQL += "   ";
		cSQL += " INNER JOIN CS_CDTB_TRANSPORTADORA_TRAN TRANS (NOLOCK)  ";
		cSQL += " ON TRANS.TRAN_DS_CODIGOTRANSPORTADORA = EMBA.EMBA_DS_COD_TRANSPORTADORA   ";
		cSQL += "   ";
		cSQL += " LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " ON MOTI.MOTI_ID_CD_MOTIVO = EMBA.EMBA_ID_CD_MOTIVO  ";
		cSQL += " LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " ON RESP.RESP_ID_CD_RESPONSAVEL = EMBA.EMBA_ID_CD_RESPONSAVEL  ";
		cSQL += "   ";
		cSQL += " LEFT JOIN CS_CDTB_CANALPORCLIENTE_CNCL CNCL (NOLOCK)  ";
		cSQL += " ON CNCL.CNCL_CD_CUSTUMER = CLIE.CLIE_ID_CD_CODIGOCLIENTE  ";
		cSQL += "  ";
		cSQL += " WHERE EMBA_DS_PERIODO = ? ";
		cSQL += " AND YEAR(EMBA_DH_REGISTRO) = ?  ";
		cSQL += " AND CLIE.CLIE_IN_INATIVO = 'N'   ";
		cSQL += " AND EMBA.EMBA_ID_CD_STATUS <> 15 ";
		if(transp.equalsIgnoreCase("S")) {
			cSQL += " AND EMBA_ID_CD_STATUS = 16 ";
		}
		
		cSQL += "  ";
		cSQL += " GROUP BY   ";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,   ";
		cSQL += " EMBA_DS_TIPOAGENDAMENTO,   ";
		cSQL += " SKUD.SKUD_DS_PEDIDOMONDELEZ,   ";
		cSQL += " SKUD.SKUD_DS_PEDIDOCLIENTE,   ";
		cSQL += " SKUD.SKUD_ID_CD_DELIVERY,   ";
		cSQL += " SKUD.SKUD_DS_TIPOVENDA,   ";
		cSQL += " NOFI.NOFI_NR_NOTAFISCAL,   ";
		cSQL += " NOFI.NOFI_DS_SERIENOTA,   ";
		cSQL += " CONVERT(VARCHAR(10),CAST(NOFI.NOFI_DH_EMISSAONOTA AS DATETIME),103),   ";
		cSQL += " EMBA.EMBA_DS_COD_TRANSPORTADORA,   ";
		cSQL += " TRANS.TRAN_NM_NOMETRANSPORTADORA,   ";
		cSQL += " EMBA.EMBA_IN_TRANSPORTADORACONFIRMADA,   ";
		cSQL += " EMBA.EMBA_DS_TIPOCARGA,   ";
		cSQL += " EMBA.EMBA_DS_VEICULO,   ";
		cSQL += " EMBA.EMBA_DS_TIPOVEICULO,   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,   ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMBA.EMBA_DH_SHIPMENT, 'dd/MM/yyyy HH:mm')),   ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE,   ";
		cSQL += " CLIE.CLIE_DS_CNPJ,   ";
		cSQL += " CLIE.CLIE_DS_NOMECLIENTE,   ";
		cSQL += " CLIE.CLIE_DS_CIDADE,   ";
		cSQL += " CLIE.CLIE_DS_UF,   ";
		cSQL += " EMBA.EMBA_DS_PLANTA,   ";
		cSQL += " EMBA.EMBA_ID_CD_MOTIVO, ";
		cSQL += " CASE WHEN EMBA.EMBA_DS_TIPOCARGA = 'TL' THEN LDTM.LDTM_DS_TL + 1 ELSE LDTM.LDTM_DS_LTL + 1 END,   ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMBA.EMBA_DH_LEADTIME, 'dd/MM/yyyy HH:mm')),   ";
		cSQL += " STAT.STAT_DS_STATUS,   ";
		cSQL += " EMBA.EMBA_IN_CUTOFF,   ";
		cSQL += " EMBA.EMBA_ID_MOTIVOCUTOFF,   ";
		cSQL += " EMBA.EMBA_ID_RESPONSAVELCUTOFF,   ";
		cSQL += " EMBA.EMBA_DS_PERIODO,   ";
		cSQL += " HQCL.HQCL_DS_GERENTENACIONAL,   ";
		cSQL += " HQCL.HQCL_DS_GERENTEREGIONAL,   ";
		cSQL += " HQCL.HQCL_DS_GERENTEAREA,   ";
		cSQL += " HQCL.HQCL_DS_VENDEDOR,   ";
		cSQL += " CONVERT(VARCHAR(10),SKUD.SKUD_DH_DELIVERY,103),  ";
		cSQL += " MOTI.MOTI_DS_MOTIVO,  ";
		cSQL += " RESP.RESP_NM_RESPONSAVEL,  ";
		cSQL += " CNCL.CNCL_DS_CHANEL,  ";
		cSQL += " CNCL.CNCL_DS_REGIAOCANAL  ";
		
		pst = conn.prepareStatement(cSQL);
		
		pst.setString(1, periodo);
		pst.setString(2, ano);
		
		rs = pst.executeQuery();
		
		while(rs.next()) {
			
			RelatorioBean ret = new RelatorioBean();
			
			ret.setEmba_id_cd_embarque(rs.getString("EMBA_ID_CD_EMBARQUE"));
			ret.setEmba_ds_tipoagendamento(rs.getString("EMBA_DS_TIPOAGENDAMENTO"));
			ret.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ"));
			ret.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE"));
			ret.setSkud_id_cd_delivery(rs.getString("SKUD_ID_CD_DELIVERY"));
			ret.setSkud_ds_tipovenda(rs.getString("SKUD_DS_TIPOVENDA"));
			ret.setNofi_nr_notafiscal(rs.getString("NOFI_NR_NOTAFISCAL"));
			ret.setNofi_ds_serienota(rs.getString("NOFI_DS_SERIENOTA"));
			ret.setNofi_dh_emissaonota(rs.getString("NOFI_DH_EMISSAONOTA"));
			ret.setEmba_ds_cod_transportadora(rs.getString("EMBA_DS_COD_TRANSPORTADORA"));
			ret.setTran_nm_nometransportadora(rs.getString("TRAN_NM_NOMETRANSPORTADORA"));
			ret.setEmba_in_transportadoraconfirmada(rs.getString("EMBA_IN_TRANSPORTADORACONFIRMADA"));
			ret.setEmba_ds_tipocarga(rs.getString("EMBA_DS_TIPOCARGA"));
			ret.setEmba_ds_veiculo(rs.getString("EMBA_DS_VEICULO"));
			ret.setEmba_ds_tipoveiculo(rs.getString("EMBA_DS_TIPOVEICULO"));
			ret.setEmba_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT"));
			ret.setEmba_dh_shipment(rs.getString("EMBA_DH_SHIPMENT"));
			ret.setEmba_id_cd_codcliente(rs.getString("EMBA_ID_CD_CODCLIENTE"));
			ret.setClie_ds_cnpj(rs.getString("CLIE_DS_CNPJ"));
			ret.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
			ret.setClie_ds_cidade(rs.getString("CLIE_DS_CIDADE"));
			ret.setClie_ds_uf(rs.getString("CLIE_DS_UF"));
			ret.setEmba_ds_planta(rs.getString("EMBA_DS_PLANTA"));
			ret.setCanal(rs.getString("CNCL_DS_CHANEL"));
			ret.setRegional(rs.getString("CNCL_DS_REGIAOCANAL"));
			ret.setSkud_ds_pesoliquido(rs.getString("SKUD_DS_PESOLIQUIDO"));
			ret.setSkud_ds_volumecubico(rs.getString("SKUD_DS_VOLUMECUBICO"));
			ret.setSkud_ds_valor(rs.getString("SKUD_DS_VALOR"));
			ret.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE"));
			ret.setDias_leadtime(rs.getString("DIAS_LEADTIME"));
			ret.setEmba_dh_leadtime(rs.getString("EMBA_DH_LEADTIME"));
			ret.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
			ret.setEmba_in_cutoff(rs.getString("EMBA_IN_CUTOFF"));
			ret.setEmba_ds_periodo(rs.getString("EMBA_DS_PERIODO"));
			ret.setHqcl_ds_gerentenacional(rs.getString("HQCL_DS_GERENTENACIONAL"));
			ret.setHqcl_ds_gerenteregional(rs.getString("HQCL_DS_GERENTEREGIONAL"));
			ret.setHqcl_ds_gerentearea(rs.getString("HQCL_DS_GERENTEAREA"));
			ret.setHqcl_ds_vendedor(rs.getString("HQCL_DS_VENDEDOR"));
			ret.setSkud_dh_delivery(rs.getString("SKUD_DH_DELIVERY"));
			ret.setCalc_aging_agendamento(rs.getInt("AGING_AGENDAMENTO"));
			ret.setCalc_ontime_scheduling(rs.getString("ON_TIME_SCHEDULING"));
			ret.setCalc_primeiro_contato(rs.getString("DT_PRIMEIRO_CONTATO"));
			ret.setCalc_input_dhl_agendamento(rs.getString("DAAG_DH_REGISTRO"));
			ret.setData_agendamento(rs.getString("DAAG_DH_AGENDAMENTO"));
			ret.setSenha_agendamento(rs.getString("SENHA_AGENDAMENTO"));
			ret.setData_retificacao_agenda(rs.getString("DATA_REAGENDAMENTO"));
			ret.setData_solicit_nova_agenda(rs.getString("DATA_INPUT_REAGENDAMENTO"));
			ret.setSenha_retificacao_agenda(rs.getString("SENHA_REAGENDAMENTO"));
			ret.setMotivo_retificacao_agenda(rs.getString("MOTIVO_REAGENDAMENTO"));
			ret.setResponsavel_retificacao_agenda(rs.getString("RESPONSAVEL_REAGENDAMENTO"));
			ret.setData_reagendamento(rs.getString("DATA_REAGENDAMENTO"));
			ret.setData_solicit_reagendamento(rs.getString("DATA_INPUT_REAGENDAMENTO"));
			ret.setSenha_reagendamento(rs.getString("SENHA_REAGENDAMENTO"));
			ret.setMotivo_reagendamento(rs.getString("MOTIVO_REAGENDAMENTO"));
			ret.setResponsavel_reagendamento(rs.getString("RESPONSAVEL_REAGENDAMENTO"));
			ret.setCalc_aging_reagendamento(rs.getInt("AGING_REAGENDAMENTO"));
			ret.setCalc_desvio_leadtime(rs.getString("DESVIO_LEADTIME"));
			ret.setFoup_motivo_desvio_leadtime(rs.getString("MOTIVO_DESVIO_LT"));
			ret.setFoup_responsavel_desvio_leadtime(rs.getString("RESPONSAVEL_DESVIO_LT"));
			ret.setEmba_ds_motivocutoff(rs.getString("MOTIVO_CUTOFF"));
			ret.setEmba_ds_responsavelcutoff(rs.getString("MOTIVO_CUTOFF"));
			ret.setDaag_ds_horaagendamento(rs.getString("HORA_AGENDAMENTO"));
			ret.setDaag_ds_horareagendamento(rs.getString("HORA_REAGENDAMENTO"));
			ret.setDaag_ds_horaretificacaoagenda(rs.getString("HORA_REAGENDAMENTO"));
			ret.setTmca(rs.getInt("TMCA"));
			ret.setDt_ocr_agendamento(rs.getString("DT_OCR_AGENDAMENTO"));
			ret.setMotivo_ocr_agend(rs.getString("MOTIVO_OCR_AGEND"));
			ret.setResponsavel_pendencia(rs.getString("RESPONSAVEL_PENDENCIA"));
			
			retorno.add(ret);
			
		}

		
		return retorno;
	}
	
	//METODO PARA BUSCAR OS CAMPOS DO RELATORIO QUE TEMOS NO BANCO E NAO SAO CALCULADOS
	//**RAFA**
	public List<RelatorioBean> getRelatorioDao(String periodo, String ano, String transp) throws SQLException, InterruptedException {
		List<RelatorioBean> retorno = new ArrayList<RelatorioBean>();
		Connection conn = DAO.getConexao();
		
		
		
		PreparedStatement pst;
		ResultSet rs = null;
		
		String cSQL = "";
		cSQL += " SELECT DISTINCT  ";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,  ";
		cSQL += " EMBA_DS_TIPOAGENDAMENTO,  ";
		cSQL += " SKUD.SKUD_DS_PEDIDOMONDELEZ,  ";
		cSQL += " SKUD.SKUD_DS_PEDIDOCLIENTE,  ";
		cSQL += " SKUD.SKUD_ID_CD_DELIVERY,  ";
		cSQL += " SKUD.SKUD_DS_TIPOVENDA,  ";
		cSQL += " ISNULL(NOFI.NOFI_NR_NOTAFISCAL,'') AS NOFI_NR_NOTAFISCAL,  ";
		cSQL += " ISNULL(NOFI.NOFI_DS_SERIENOTA, '') AS NOFI_DS_SERIENOTA,  ";
		cSQL += " ISNULL(CONVERT(VARCHAR(10), CAST(NOFI.NOFI_DH_EMISSAONOTA AS DATETIME), 103),'') AS NOFI_DH_EMISSAONOTA,  ";
		cSQL += " EMBA.EMBA_DS_COD_TRANSPORTADORA,  ";
		cSQL += " TRANS.TRAN_NM_NOMETRANSPORTADORA,  ";
		cSQL += " EMBA.EMBA_IN_TRANSPORTADORACONFIRMADA,  ";
		cSQL += " EMBA.EMBA_DS_TIPOCARGA,  ";
		cSQL += " EMBA.EMBA_DS_VEICULO,  ";
		cSQL += " EMBA.EMBA_DS_TIPOVEICULO,  ";
		cSQL += " EMBA_ID_CD_SHIPMENT,  ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMBA.EMBA_DH_SHIPMENT, 'dd/MM/yyyy HH:mm')) EMBA_DH_SHIPMENT,  ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE,  ";
		cSQL += " CLIE.CLIE_DS_CNPJ,  ";
		cSQL += " CLIE.CLIE_DS_NOMECLIENTE,  ";
		cSQL += " CLIE.CLIE_DS_CIDADE,  ";
		cSQL += " CLIE.CLIE_DS_UF,  ";
		cSQL += " EMBA.EMBA_DS_PLANTA,  ";
		cSQL += " CASE WHEN EMBA.EMBA_DS_TIPOCARGA = 'TL' THEN LDTM.LDTM_DS_TL + 1 ELSE LDTM.LDTM_DS_LTL + 1 END AS DIAS_LEADTIME,  ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMBA.EMBA_DH_LEADTIME, 'dd/MM/yyyy HH:mm')) EMBA_DH_LEADTIME,  ";
		cSQL += " STAT.STAT_DS_STATUS,  ";
		cSQL += " SUM(SKUD.SKUD_DS_PESOLIQUIDO) AS SKUD_DS_PESOLIQUIDO,  ";
		cSQL += " SUM(SKUD.SKUD_DS_VOLUMECUBICO) AS SKUD_DS_VOLUMECUBICO,  ";
		cSQL += " SUM(SKUD.SKUD_DS_VALOR) AS SKUD_DS_VALOR,  ";
		cSQL += " SUM(SKUD.SKUD_NR_QUANTIDADE) AS SKUD_NR_QUANTIDADE,  ";
		cSQL += " EMBA.EMBA_IN_CUTOFF,  ";
		cSQL += " EMBA.EMBA_ID_MOTIVOCUTOFF,  ";
		cSQL += " EMBA.EMBA_ID_RESPONSAVELCUTOFF,  ";
		cSQL += " MOTI.MOTI_DS_MOTIVO AS EMBA_DS_MOTIVOCUTOFF, ";
		cSQL += " RESP.RESP_NM_RESPONSAVEL AS EMBA_DS_RESPONSAVELCUTOFF, ";
		cSQL += " EMBA.EMBA_DS_PERIODO,  ";
		cSQL += " HQCL.HQCL_DS_GERENTENACIONAL,  ";
		cSQL += " HQCL.HQCL_DS_GERENTEREGIONAL,  ";
		cSQL += " HQCL.HQCL_DS_GERENTEAREA,  ";
		cSQL += " HQCL.HQCL_DS_VENDEDOR,  ";
		cSQL += " CONVERT(VARCHAR(10),SKUD.SKUD_DH_DELIVERY,103) AS SKUD_DH_DELIVERY,  ";
		cSQL += " CNCL.CNCL_DS_CHANEL, ";
		cSQL += " CNCL.CNCL_DS_REGIAOCANAL ";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA  (NOLOCK)";
		cSQL += " INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD  (NOLOCK)";
		cSQL += " ON SKUD.SKUD_ID_CD_SHIPMENT = EMBA.EMBA_ID_CD_SHIPMENT  ";
		cSQL += " LEFT JOIN CS_CDTB_NOTAFISCAL_NOFI NOFI  (NOLOCK)";
		cSQL += " ON NOFI.NOFI_NR_DELIVERYDOCUMENT = SKUD.SKUD_ID_CD_DELIVERY  ";
		cSQL += " INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE  (NOLOCK)";
		cSQL += " ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE  ";
		cSQL += " LEFT JOIN CS_CDTB_LEADTIME_LDTM LDTM  (NOLOCK)";
		cSQL += " ON LDTM.LDTM_CD_SOURCELOCATION = EMBA.EMBA_DS_PLANTA AND LDTM.LDTM_DS_DESTINATIONCITY = CLIE.CLIE_DS_CIDADE  ";
		cSQL += " LEFT JOIN CS_CDTB_STATUSAGENDAMENTO_STAT STAT (NOLOCK) ";
		cSQL += " ON STAT.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS  ";
		cSQL += " LEFT JOIN CS_CDTB_HIERARQUIACLIENTE_HQCL HQCL (NOLOCK) ";
		cSQL += " ON HQCL.HQCL_ID_CLIENTE = CLIE.CLIE_ID_CD_CODIGOCLIENTE  ";
		cSQL += "  ";
		cSQL += " INNER JOIN CS_CDTB_TRANSPORTADORA_TRAN TRANS (NOLOCK) ";
		cSQL += " ON TRANS.TRAN_DS_CODIGOTRANSPORTADORA = EMBA.EMBA_DS_COD_TRANSPORTADORA  ";
		cSQL += "  ";
		cSQL += " LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK)";
		cSQL += " ON MOTI.MOTI_ID_CD_MOTIVO = EMBA.EMBA_ID_CD_MOTIVO ";
		cSQL += " LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK)";
		cSQL += " ON RESP.RESP_ID_CD_RESPONSAVEL = EMBA.EMBA_ID_CD_RESPONSAVEL ";
		cSQL += "  ";
		cSQL += " INNER JOIN CS_CDTB_CANALPORCLIENTE_CNCL CNCL (NOLOCK) ";
		cSQL += " ON CNCL.CNCL_CD_CUSTUMER = CLIE.CLIE_ID_CD_CODIGOCLIENTE ";

		cSQL += " WHERE EMBA_DS_PERIODO = ? ";
		cSQL += " AND YEAR(EMBA_DH_REGISTRO) >= ? ";
		cSQL += " AND CLIE.CLIE_IN_INATIVO = 'N'  ";
		
		if(transp.equalsIgnoreCase("S")) {
			cSQL += " AND EMBA_ID_CD_STATUS = 16 ";
		}
		
		cSQL += " GROUP BY  ";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,  ";
		cSQL += " EMBA_DS_TIPOAGENDAMENTO,  ";
		cSQL += " SKUD.SKUD_DS_PEDIDOMONDELEZ,  ";
		cSQL += " SKUD.SKUD_DS_PEDIDOCLIENTE,  ";
		cSQL += " SKUD.SKUD_ID_CD_DELIVERY,  ";
		cSQL += " SKUD.SKUD_DS_TIPOVENDA,  ";
		cSQL += " NOFI.NOFI_NR_NOTAFISCAL,  ";
		cSQL += " NOFI.NOFI_DS_SERIENOTA,  ";
		cSQL += " CONVERT(VARCHAR(10),CAST(NOFI.NOFI_DH_EMISSAONOTA AS DATETIME),103),  ";
		cSQL += " EMBA.EMBA_DS_COD_TRANSPORTADORA,  ";
		cSQL += " TRANS.TRAN_NM_NOMETRANSPORTADORA,  ";
		cSQL += " EMBA.EMBA_IN_TRANSPORTADORACONFIRMADA,  ";
		cSQL += " EMBA.EMBA_DS_TIPOCARGA,  ";
		cSQL += " EMBA.EMBA_DS_VEICULO,  ";
		cSQL += " EMBA.EMBA_DS_TIPOVEICULO,  ";
		cSQL += " EMBA_ID_CD_SHIPMENT,  ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMBA.EMBA_DH_SHIPMENT, 'dd/MM/yyyy HH:mm')),  ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE,  ";
		cSQL += " CLIE.CLIE_DS_CNPJ,  ";
		cSQL += " CLIE.CLIE_DS_NOMECLIENTE,  ";
		cSQL += " CLIE.CLIE_DS_CIDADE,  ";
		cSQL += " CLIE.CLIE_DS_UF,  ";
		cSQL += " EMBA.EMBA_DS_PLANTA,  ";
		cSQL += " CASE WHEN EMBA.EMBA_DS_TIPOCARGA = 'TL' THEN LDTM.LDTM_DS_TL + 1 ELSE LDTM.LDTM_DS_LTL + 1 END,  ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMBA.EMBA_DH_LEADTIME, 'dd/MM/yyyy HH:mm')),  ";
		cSQL += " STAT.STAT_DS_STATUS,  ";
		cSQL += " EMBA.EMBA_IN_CUTOFF,  ";
		cSQL += " EMBA.EMBA_ID_MOTIVOCUTOFF,  ";
		cSQL += " EMBA.EMBA_ID_RESPONSAVELCUTOFF,  ";
		cSQL += " EMBA.EMBA_DS_PERIODO,  ";
		cSQL += " HQCL.HQCL_DS_GERENTENACIONAL,  ";
		cSQL += " HQCL.HQCL_DS_GERENTEREGIONAL,  ";
		cSQL += " HQCL.HQCL_DS_GERENTEAREA,  ";
		cSQL += " HQCL.HQCL_DS_VENDEDOR,  ";
		cSQL += " CONVERT(VARCHAR(10),SKUD.SKUD_DH_DELIVERY,103), ";
		cSQL += " MOTI.MOTI_DS_MOTIVO, ";
		cSQL += " RESP.RESP_NM_RESPONSAVEL, ";
		cSQL += " CNCL.CNCL_DS_CHANEL, ";
		cSQL += " CNCL.CNCL_DS_REGIAOCANAL ";

		

		pst = conn.prepareStatement(cSQL);
		
		pst.setString(1, periodo);
		pst.setString(2, ano);
		
		rs = pst.executeQuery();
		System.out.println("teste ");
		Thread.sleep(7200);
		while(rs.next()) {
			RelatorioBean ret = new RelatorioBean();
			
			ret.setEmba_id_cd_embarque(rs.getString("EMBA_ID_CD_EMBARQUE"));
			ret.setEmba_ds_tipoagendamento(rs.getString("EMBA_DS_TIPOAGENDAMENTO"));
			ret.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ"));
			ret.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE"));
			ret.setSkud_ds_tipovenda(rs.getString("SKUD_DS_TIPOVENDA"));
			ret.setNofi_nr_notafiscal(rs.getString("NOFI_NR_NOTAFISCAL"));
			ret.setNofi_ds_serienota(rs.getString("NOFI_DS_SERIENOTA"));
			ret.setNofi_dh_emissaonota(rs.getString("NOFI_DH_EMISSAONOTA"));
			ret.setEmba_ds_cod_transportadora(rs.getString("EMBA_DS_COD_TRANSPORTADORA"));
			ret.setTran_nm_nometransportadora(rs.getString("TRAN_NM_NOMETRANSPORTADORA"));
			
			ret.setEmba_in_transportadoraconfirmada(rs.getString("EMBA_IN_TRANSPORTADORACONFIRMADA"));
			ret.setEmba_ds_tipocarga(rs.getString("EMBA_DS_TIPOCARGA"));
			ret.setEmba_ds_veiculo(rs.getString("EMBA_DS_VEICULO"));
			ret.setEmba_ds_tipoveiculo(rs.getString("EMBA_DS_TIPOVEICULO"));
			ret.setEmba_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT"));
			ret.setEmba_dh_shipment(rs.getString("EMBA_DH_SHIPMENT"));
			ret.setEmba_id_cd_codcliente(rs.getString("EMBA_ID_CD_CODCLIENTE"));
			ret.setClie_ds_cnpj(rs.getString("CLIE_DS_CNPJ"));
			ret.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
			ret.setClie_ds_cidade(rs.getString("CLIE_DS_CIDADE"));
			ret.setClie_ds_uf(rs.getString("CLIE_DS_UF"));
			ret.setEmba_ds_planta(rs.getString("EMBA_DS_PLANTA"));
			ret.setDias_leadtime(rs.getString("DIAS_LEADTIME"));
			ret.setEmba_dh_leadtime(rs.getString("EMBA_DH_LEADTIME"));
			ret.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
			ret.setSkud_ds_pesoliquido(rs.getString("SKUD_DS_PESOLIQUIDO"));
			ret.setSkud_ds_volumecubico(rs.getString("SKUD_DS_VOLUMECUBICO"));
			ret.setSkud_ds_valor(rs.getString("SKUD_DS_VALOR"));
			ret.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE"));
			ret.setEmba_in_cutoff(rs.getString("EMBA_IN_CUTOFF"));
			ret.setEmba_ds_periodo(rs.getString("EMBA_DS_PERIODO"));
			ret.setHqcl_ds_gerentenacional(rs.getString("HQCL_DS_GERENTENACIONAL"));
			ret.setHqcl_ds_gerenteregional(rs.getString("HQCL_DS_GERENTEREGIONAL"));
			ret.setHqcl_ds_gerentearea(rs.getString("HQCL_DS_GERENTEAREA"));
			ret.setHqcl_ds_vendedor(rs.getString("HQCL_DS_VENDEDOR"));
			ret.setSkud_dh_delivery(rs.getString("SKUD_DH_DELIVERY"));
			ret.setSkud_id_cd_delivery(rs.getString("SKUD_ID_CD_DELIVERY"));
			//ret.setCalc_input_dhl_agendamento(rs.getString("DAAG_DH_REGISTRO"));
			ret.setEmba_ds_motivocutoff(rs.getString("EMBA_DS_MOTIVOCUTOFF"));
			ret.setEmba_ds_responsavelcutoff(rs.getString("EMBA_DS_RESPONSAVELCUTOFF"));
			ret.setCanal(rs.getString("CNCL_DS_CHANEL"));
			ret.setRegional(rs.getString("CNCL_DS_REGIAOCANAL"));
			retorno.add(ret);
			
			
		}
		
		
		
		return retorno;
	}
	
	
	public CsNgtbDataagendamentoDaagBean getDataAgendamentoDao(String id_embarque, String tipo_agendamento) throws SQLException {
		
		//INCLUINDO REGRA PARA CONSIDERAR DATA DE REAGENDA E RETIFICAÇÃO QND TIPO FOR RETIFICAÇÃO OU REAGENDAMENTO
		if(tipo_agendamento.equalsIgnoreCase("Retificação") || tipo_agendamento.equalsIgnoreCase("Reagendamento")) {
			tipo_agendamento = "Retificação, Reagendamento";
		}
		
		
		CsNgtbDataagendamentoDaagBean ret = new CsNgtbDataagendamentoDaagBean();
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;
		ResultSet rs = null;	
		
		String cSQL = "";
		cSQL += " SELECT TOP 1 ";
		cSQL += " DAAG_DS_TIPOAGENDAMENTO,  ";
		cSQL += " ISNULL(CONVERT(VARCHAR,FORMAT(DAAG_DH_AGENDAMENTO, 'dd/MM/yyyy')),'') DAAG_DH_AGENDAMENTO,  ";
		cSQL += " ISNULL(CONVERT(VARCHAR,FORMAT(DAAG_DH_REGISTRO, 'dd/MM/yyyy HH:mm')),'') DAAG_DH_REGISTRO,  ";
		cSQL += " DAAG_DS_HORAAGENDAMENTO,  ";
		cSQL += " DAAG_DS_SENHAAGENDAMENTO, ";
		cSQL += " MOTI.MOTI_DS_MOTIVO, ";
		cSQL += " RESP.RESP_NM_RESPONSAVEL ";
		cSQL += " FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK)";
		cSQL += " LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK)";
		cSQL += " ON MOTI.MOTI_ID_CD_MOTIVO = DAAG.DAAG_ID_CD_MOTIVO ";
		cSQL += " LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK)";
		cSQL += " ON RESP.RESP_ID_CD_RESPONSAVEL = DAAG.DAAG_ID_CD_RESPONSAVEL ";
		cSQL += " WHERE  ";
		cSQL += " DAAG_DS_TIPOAGENDAMENTO IN (?) ";
		cSQL += " AND DAAG_ID_CD_EMBARQUE = ?";
		cSQL += " ORDER BY DAAG_DH_REGISTRO DESC ";
		
		
		pst = conn.prepareStatement(cSQL);
		pst.setString(1, tipo_agendamento);
		pst.setString(2, id_embarque);
		
		
		rs = pst.executeQuery();
				
		while(rs.next()) {
			ret.setDaag_dh_agendamento(rs.getString("DAAG_DH_AGENDAMENTO"));
			ret.setDaag_ds_senhaagendamento(rs.getString("DAAG_DS_SENHAAGENDAMENTO"));
			ret.setDaag_ds_tipoagendamento(rs.getString("DAAG_DS_TIPOAGENDAMENTO"));
			ret.setMoti_ds_motivo(rs.getString("MOTI_DS_MOTIVO"));
			ret.setResp_nm_responsavel(rs.getString("RESP_NM_RESPONSAVEL"));
			ret.setDaag_dh_registro(rs.getString("DAAG_DH_REGISTRO"));
			ret.setDaag_ds_horaagendamento(rs.getString("DAAG_DS_HORAAGENDAMENTO"));
		}
		
		return ret;
	}
	
	public relatorioOcrBean getOcr(String idEmbarque) throws SQLException {
		
		relatorioOcrBean ocr = new relatorioOcrBean();
		String idFoupsOcr = "11,12,13,14,15,17,18";
		
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		
		String cSQL = "";
		cSQL += " SELECT TOP 1 ";
		cSQL += " CONVERT(VARCHAR,FORMAT(EMFO.EMFO_DH_REGISTRO, 'dd/MM/yyyy HH:mm')) as DT_OCR_AGENDAMENTO, ";
		cSQL += " MOTI.MOTI_DS_MOTIVO AS MOTIVO_OCR_AGEND, ";
		cSQL += " RESP.RESP_NM_RESPONSAVEL AS RESPONSAVEL_PENDENCIA ";
		cSQL += " FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK)";
		cSQL += " INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) ";
		cSQL += " ON MOTI.MOTI_ID_CD_MOTIVO = EMFO.EMFO_ID_CD_MOTIVO ";
		cSQL += " INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK) ";
		cSQL += " ON RESP.RESP_ID_CD_RESPONSAVEL = EMFO.EMFO_ID_CD_MOTIVO ";
		cSQL += " WHERE EMFO_ID_CD_FOLLOWUP IN ";
		cSQL += " (" + idFoupsOcr + ") ";
		cSQL += " AND EMFO.EMFO_ID_CD_EMBARQUE = ? ";
		cSQL += " ORDER BY EMFO_DH_REGISTRO DESC ";
		
		pst = conn.prepareStatement(cSQL);
		
		pst.setString(1, idEmbarque);
		
		rs = pst.executeQuery();
		
		if(rs.next()) {
			ocr.setDt_ocr_agendamento(rs.getString("DT_OCR_AGENDAMENTO"));
			ocr.setMotivo_ocr_agend(rs.getString("MOTIVO_OCR_AGEND"));
			ocr.setResponsavel_pendencia(rs.getString("RESPONSAVEL_PENDENCIA"));
		}else {
			ocr.setDt_ocr_agendamento("");
			ocr.setMotivo_ocr_agend("");
			ocr.setResponsavel_pendencia("");
		}
		
		return ocr;
		
	}
	
	public int calculaTmca(String idEmbarque) throws SQLException {
Connection conn = DAO.getConexao();
		String idFoupsPrimeiroContato = "7,8,9";
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String cSQL = "";
		cSQL += " SELECT ";
		cSQL += " ISNULL(DATEDIFF(DAY,( ";
		cSQL += " SELECT TOP 1    ";
		cSQL += " EMFO_DH_REGISTRO  ";
		cSQL += " FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK) ";
		cSQL += " WHERE EMFO_ID_CD_EMBARQUE = ? ";
		cSQL += " AND EMFO_ID_CD_FOLLOWUP IN (7,8,9)  ";
		cSQL += " ORDER BY EMFO_DH_REGISTRO ASC ";
		cSQL += " ), ";
		cSQL += " ( ";
		cSQL += " SELECT TOP 1 ";
		cSQL += " DAAG_DH_REGISTRO ";
		cSQL += " FROM CS_NGTB_DATAAGENDAMENTO_DAAG (NOLOCK) ";
		cSQL += " WHERE DAAG_ID_CD_EMBARQUE = ? ";
		cSQL += " AND DAAG_DS_TIPOAGENDAMENTO = 'Agendamento' ";
		cSQL += " ORDER BY DAAG_DH_REGISTRO DESC ";
		cSQL += " )),0) AS TMCA ";
		
		pst = conn.prepareStatement(cSQL);
		pst.setString(1, idEmbarque);
		pst.setString(2, idEmbarque);
		rs = pst.executeQuery();
		int ret = 0;
		if(rs.next()) {
			ret = rs.getInt("TMCA");
		}
		
		return ret;
		
	}
	
	public int calculaAging(String tipo, String idEmbarque) throws SQLException {
		String idFoupsPrimeiroContato = "7,8,9";
		String idFoupsConfirmacao = "";
		if(tipo.equalsIgnoreCase("agendamento")) {
			idFoupsConfirmacao = "19";
		}else {
			idFoupsConfirmacao = "20";
		}
		
		
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;
		ResultSet rs = null;	
		
		
		String cSQL = "";
		cSQL += " SELECT ";
		cSQL += " ISNULL(DATEDIFF(DAY,INI, FIM),0) AS AGING ";
		cSQL += "  ";
		cSQL += " FROM ";
		cSQL += " ( ";
		cSQL += " SELECT ";
		cSQL += " 	( ";
		cSQL += " 	SELECT TOP 1   ";
		cSQL += " 	EMFO_DH_REGISTRO ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK)";
		cSQL += " 	WHERE EMFO_ID_CD_EMBARQUE = " + idEmbarque;
		cSQL += " 	AND EMFO_ID_CD_FOLLOWUP IN (" + idFoupsPrimeiroContato + ") ";
		cSQL += " 	ORDER BY EMFO_DH_REGISTRO ASC ";
		cSQL += " 	) AS INI, ";
		cSQL += " 	( ";
		cSQL += " 	SELECT TOP 1   ";
		cSQL += " 	EMFO_DH_REGISTRO ";
		cSQL += " 	FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK) ";
		cSQL += " 	WHERE EMFO_ID_CD_EMBARQUE = " + idEmbarque;
		cSQL += " 	AND EMFO_ID_CD_FOLLOWUP IN (" + idFoupsConfirmacao + ") ";
		cSQL += " 	ORDER BY EMFO_DH_REGISTRO ASC ";
		cSQL += " 	) AS FIM ";
		cSQL += " ) AS T ";

		pst = conn.prepareStatement(cSQL);
		
		rs = pst.executeQuery();
		int ret = 0;
		if(rs.next()) {
			ret = rs.getInt("AGING");
		}
		
		return ret;
	}
	
	public String primeiroContato(String idEmbarque) throws SQLException {
		String ret = "";
		
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;
		ResultSet rs = null;	
		
		
		String cSQL = "";
		cSQL += " SELECT TOP 1    ";
		cSQL += " CONVERT(VARCHAR(18),EMFO_DH_REGISTRO,103) AS DH_REGISTRO  ";
		cSQL += " FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO (NOLOCK) ";
		cSQL += " WHERE EMFO_ID_CD_EMBARQUE = " + idEmbarque;
		cSQL += " AND EMFO_ID_CD_FOLLOWUP IN (7,8,9)  ";
		cSQL += " ORDER BY EMFO_DH_REGISTRO ASC  ";

		pst = conn.prepareStatement(cSQL);
		
		rs = pst.executeQuery();
		
		if(rs.next()) {
			ret = rs.getString("DH_REGISTRO");
		}
		
		return ret;
	}
	
	public String desvioLeadTime(String idEmbarque) throws SQLException {
		String ret = "";
		
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;
		ResultSet rs = null;	
		
		
		String cSQL = "";
		cSQL += " SELECT ";
		cSQL += " DATEDIFF(DAY,DH_LEADTIME, DH_AGENDA) AS DESVIO_LT ";
		cSQL += " FROM ";
		cSQL += " ( ";
		cSQL += " SELECT ";
		cSQL += " ( ";
		cSQL += " SELECT TOP 1 ";
		cSQL += " DAAG_DH_AGENDAMENTO ";
		cSQL += " FROM CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK)";
		cSQL += " WHERE DAAG_ID_CD_EMBARQUE = " + idEmbarque;
		cSQL += " ORDER BY DAAG_DH_REGISTRO DESC) AS DH_AGENDA, ";
		cSQL += " ( ";
		cSQL += " SELECT  ";
		cSQL += " EMBA_DH_LEADTIME ";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += " WHERE EMBA_ID_CD_EMBARQUE = " + idEmbarque + ") AS DH_LEADTIME) AS T ";

		pst = conn.prepareStatement(cSQL);
		
		rs = pst.executeQuery();
		
		if(rs.next()) {
			//VERIFICA SE O RETORNO DA QUERY > 0 SEGNIFICA QUE NÃO FOI AGENDADO NO PRAZO
			ret = rs.getString("DESVIO_LT");
		}
		
		
		return ret;
	}
	
	public MotivoResponsavelCutoffBean getMotivoResponsavelCutoff(String idEmbarque) throws SQLException {
		MotivoResponsavelCutoffBean ret = new MotivoResponsavelCutoffBean();
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;
		ResultSet rs = null;	
		
		String cSQL = "";
		cSQL += " SELECT ";
		cSQL += " MOTI.MOTI_DS_MOTIVO, ";
		cSQL += " RESP.RESP_NM_RESPONSAVEL ";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += " INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK)";
		cSQL += " ON MOTI.MOTI_ID_CD_MOTIVO = EMBA.EMBA_ID_CD_MOTIVO ";
		cSQL += " INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK)";
		cSQL += " ON RESP.RESP_ID_CD_RESPONSAVEL = EMBA.EMBA_ID_CD_RESPONSAVEL ";
		cSQL += " WHERE EMBA_ID_CD_EMBARQUE = " + idEmbarque;

		pst = conn.prepareStatement(cSQL);
		
		rs = pst.executeQuery();
		
		if(rs.next()) {
			ret.setMoti_ds_motivo(rs.getString("MOTI_DS_MOTIVO"));
			ret.setResp_nm_responsavel(rs.getString("RESP_NM_RESPONSAVEL"));
		}
		return ret;
	}
	
	
	public MotivoResponsavelDesvioLTBean getMotivoResponsavelDesvioLt(String idEmbarque) throws SQLException {
		MotivoResponsavelDesvioLTBean ret = new MotivoResponsavelDesvioLTBean();
		
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;
		ResultSet rs = null;	
		
		//FOLLOW DE DESVIO DE LEADTIME = 46
		String cSQL = "";
		cSQL += " SELECT ";
		cSQL += " MOTI.MOTI_DS_MOTIVO, ";
		cSQL += " RESP.RESP_NM_RESPONSAVEL ";
		cSQL += " FROM CS_ASTB_EMBARQUEFOUP_EMFO EMFO ";
		cSQL += " INNER JOIN CS_CDTB_FOLLOWUP_FOUP FOUP ";
		cSQL += " ON FOUP.FOUP_ID_CD_FOLLOWUP = EMFO.EMFO_ID_CD_FOLLOWUP ";
		cSQL += " INNER JOIN CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK)";
		cSQL += " ON MOTI.MOTI_ID_CD_MOTIVO = EMFO.EMFO_ID_CD_MOTIVO ";
		cSQL += " INNER JOIN CS_CDTB_RESPONSAVEL_RESP RESP (NOLOCK)";
		cSQL += " ON RESP.RESP_ID_CD_RESPONSAVEL = EMFO.EMFO_ID_CD_RESPONSAVEL ";
		cSQL += " WHERE EMFO.EMFO_ID_CD_FOLLOWUP = 46 ";
		cSQL += " AND EMFO.EMFO_ID_CD_EMBARQUE = " + idEmbarque;

		pst = conn.prepareStatement(cSQL);
		
		rs = pst.executeQuery();
		
		if(rs.next()) {
			ret.setMoti_ds_motivo(rs.getString("MOTI_DS_MOTIVO"));
			ret.setResp_nm_responsavel(rs.getString("RESP_NM_RESPONSAVEL"));
		}
		
		return ret;
	}
	
	public String onTimeScheduling(String idEmbarque) throws SQLException {
		String ret = "";
		
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;
		ResultSet rs = null;	
		
		
		String cSQL = "";
		cSQL += " SELECT ";
		cSQL += " ISNULL(DATEDIFF(DAY,INI,FIM),0) AS SCHEDULING";
		cSQL += " FROM ";
		cSQL += " ( ";
		cSQL += " SELECT ";
		cSQL += " ( ";
		cSQL += " SELECT TOP 1 ";
		cSQL += " DAAG_DH_REGISTRO ";
		cSQL += " FROM CS_NGTB_DATAAGENDAMENTO_DAAG (NOLOCK)";
		cSQL += " WHERE DAAG_DS_TIPOAGENDAMENTO = 'Agendamento' ";
		cSQL += " AND DAAG_ID_CD_EMBARQUE = " + idEmbarque;
		cSQL += " ORDER BY DAAG_DH_REGISTRO ASC ";
		cSQL += " ) AS FIM, ";
		cSQL += " (SELECT  ";
		cSQL += " EMBA_DH_SHIPMENT ";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA (NOLOCK)";
		cSQL += " WHERE EMBA_ID_CD_EMBARQUE ="  + idEmbarque + ") AS INI) AS T ";

		pst = conn.prepareStatement(cSQL);
		
		rs = pst.executeQuery();
		
		if(rs.next()) {
			//VERIFICA SE O RETORNO DA QUERY > 0 SEGNIFICA QUE NÃO FOI AGENDADO NO PRAZO
			if(rs.getInt("SCHEDULING") > 0) {
				ret = "Fora do prazo";
			}else {
				ret = "Dentro do prazo";
			}
		}
		
		
		return ret;
	}

}
