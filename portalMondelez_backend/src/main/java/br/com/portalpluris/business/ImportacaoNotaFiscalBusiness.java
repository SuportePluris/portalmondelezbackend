package br.com.portalpluris.business;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilParseToInt;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.NotaFiscalBean;
import br.com.portalpluris.dao.CsCdtbNofaFiscalDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsNgtbEmbarqueEmbaDao;
import br.com.portalpluris.dao.DAOLog;

public class ImportacaoNotaFiscalBusiness {

	public void importadorNotaFiscal() {

	Logger log = Logger.getLogger(this.getClass().getName());
		log.info("*** INICIO DO SERVIÇO DE IMPORTAÇÃO DE ARQUIVOOTA FISCAL ******");
		UtilMoveArquivo utilMove = new UtilMoveArquivo();
		// DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoNotaFiscalBusiness");
		logsBean.setLogsDsMetodo("importadorNotaFiscal");
		logsBean.setLogsInErro("N");

		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		NotaFiscalBean nofiBean = new NotaFiscalBean();
		NotaFiscalBean nofiBeanRet = new NotaFiscalBean();
		CsCdtbNofaFiscalDao nofiDao = new CsCdtbNofaFiscalDao();
		
		String diretorio = confDao.slctConf("conf.importacao.notafiscal");
		String diretorioBKP = confDao.slctConf("conf.importacao.notafiscal.bkp");		
		String diretorioErro = confDao.slctConf("conf.importacao.notafiscal.erro");
		
		File arquivoProcessado = null;
		File arquivoProcessadobkp = null;
		File arquivoProcessadoerro = new File(diretorioErro+ "/");
		
		CsNgtbEmbarqueEmbaDao embaDao = new CsNgtbEmbarqueEmbaDao();
		
		
		UtilParseToInt parseInt = new UtilParseToInt();

		int linha = 0;

		try {
			
			log.info("*** LISTANDO ARQUIVOS DO DIRETORIO ******");
			File arquivo = new File(diretorio);
			String[] arq = arquivo.list();
			
			
			if(arq != null){
				
			log.info("*** DIRETORIO LISTADO EFETUANDO LEITURA DOS ARQUIVOS ******");	

			for (int i = 0; i < arq.length; i++) {

				 arquivoProcessado = new File(diretorio + "/" + arq[i]);
				 arquivoProcessadobkp = new File(diretorioBKP + "/" + arq[i]);

				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yy");
				String dhImportacao = sdf.format(date);
				
				String nmArquivo = arq[i] + "_" + dhImportacao;

				if (arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

					log.info("*** INICIO DA LEITURA DO ARQUIVO " + arquivoProcessado.getName() + "******");
					FileInputStream file = new FileInputStream(arquivoProcessado);

					Workbook wb = WorkbookFactory.create(file);
					Sheet sheet = wb.getSheetAt(0);

						nofiBean = new NotaFiscalBean();			
						ArrayList<NotaFiscalBean> nofiBeanList = new ArrayList<NotaFiscalBean>();
						
						//RETORNA LISTA DE NOTA.
						nofiBeanList = trataNotaFiscal(sheet);
						
						CsNgtbEmbarqueEmbaDao daoEmba = new CsNgtbEmbarqueEmbaDao();
						
						for ( int j = 0; j < nofiBeanList.size(); j ++){
							
							linha =j;
							
							date = new Date();
							dhImportacao = sdf.format(date);
						
						boolean ifExists = nofiDao.slcNofi(nofiBeanList.get(j));

						if (!ifExists) {
							//log.info("UPDATE");
							//nofiDao.updateNofi(nofiBeanList.get(j), dhImportacao, nmArquivo);						
							log.info("INSERT");
							nofiDao.insertNofi(nofiBeanList.get(j), dhImportacao, nmArquivo);
							}
							
							
							//DANILLO *** 29/07/2021 *** INCLUSÃO DE PERIODO BASEADO NA DATA DE EMISSÃO DA NOTA FISCAL
							//29/07/2021
							daoEmba.updatePeriodoEmbarque(nofiBeanList.get(j).getNrDeliveryDocument(), 
																nofiBeanList.get(j).getDhEmissaoNota(), 
																	"S");
							
							
						
							
							//embaDao.updatePeriodo(nofiBeanList.get(j));
							
							
					}
						boolean movido = utilMove.moveArquivo(arquivoProcessado,arquivoProcessadobkp);
						
						if(movido){
							log.info("Arquivo movido com sucesso");
						}else{
							log.info("Não foi possivel mover o arquivo");
						}			
			}
		}
	} else {
		log.info("*** NAO FOI POSSIVEL OBTER OS ARQUIVOS DO DIRETORIO ******");
	}


		} catch (Exception e) {
			log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO NOTA FISCAL " + e + " ******");
			log.info("*** LINHA:" + linha + "************");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro( "LINHA DE ERRO:" +linha +e.getMessage());
			utilMove.moveArquivo(arquivoProcessado,arquivoProcessadoerro);

		} finally {
			daoLog.createLog(logsBean);
			log.info("**** FIM SERVIÇO DE IMPORTAÇÃO DE ARQUIVO OTA FISCAL ****");
		}

	}

	private ArrayList<NotaFiscalBean> trataNotaFiscal( Sheet sheet) throws ParseException {
		
		NotaFiscalBean nofiBean = new NotaFiscalBean();
		ArrayList<NotaFiscalBean> nofiBeanList = new ArrayList<NotaFiscalBean>();
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yy");
		
		for (int j = 2; j <= sheet.getLastRowNum(); j++) {	
			
			try{
			
				nofiBean = new NotaFiscalBean();
				
			System.out.println("Leitura da linha:" +j);
		
			if(sheet.getRow(j).getCell(1) != null){				
				nofiBean.setPlanta(sheet.getRow(j).getCell(1).toString());
			}
			if(sheet.getRow(j).getCell(2) != null){	
				nofiBean.setCodEmpresa(sheet.getRow(j).getCell(2).toString());
			}
			if(sheet.getRow(j).getCell(3) != null){	
				Cell pedido = sheet.getRow(j).getCell(3);
				pedido.setCellType(CellType.STRING);
				nofiBean.setNrPedido(pedido.toString());
			}
			if(sheet.getRow(j).getCell(4) != null){	
				nofiBean.setNrPedidoCliente(sheet.getRow(j).getCell(4).toString());
			}
			if(sheet.getRow(j).getCell(5) != null){	
				Cell deliveryDocu = sheet.getRow(j).getCell(5);
				deliveryDocu.setCellType(CellType.STRING);
				nofiBean.setNrDeliveryDocument(deliveryDocu.toString());
			}
			if(sheet.getRow(j).getCell(6) != null){	
				////
				String datasemformato = sheet.getRow(j).getCell(6).toString();					
				Date data = sdf2.parse(datasemformato);		
				String dtDelyvery = sdf.format(data);
				nofiBean.setDhDelivery(dtDelyvery);
				////
			}
			if(sheet.getRow(j).getCell(7) != null){	
				Cell nrNota = sheet.getRow(j).getCell(7);
				String nota = nrNota.toString();				
				nota = nota.replace(".0", "");
				nofiBean.setNrNotaFiscal(nota);
			}
			if(sheet.getRow(j).getCell(8) != null){	
				nofiBean.setSerieNota(sheet.getRow(j).getCell(8).toString());
			}
			if(sheet.getRow(j).getCell(9) != null){	
			Cell codCli = sheet.getRow(j).getCell(9);
			nofiBean.setCodCliente(codCli.toString());
			}
			if(sheet.getRow(j).getCell(10) != null){	
				nofiBean.setRazaoSocial(sheet.getRow(j).getCell(10).toString());
			}
			if(sheet.getRow(j).getCell(11) != null){	
				nofiBean.setCidadeCliente(sheet.getRow(j).getCell(11).toString());
			}
			if(sheet.getRow(j).getCell(12) != null){	
				nofiBean.setUfCliente(sheet.getRow(j).getCell(12).toString());
			}
			if(sheet.getRow(j).getCell(13) != null){	
				String datasemformato1 = sheet.getRow(j).getCell(13).toString();
				Date data2 = sdf2.parse(datasemformato1);	
				String dhEmissao = sdf.format(data2);					
				nofiBean.setDhEmissaoNota(dhEmissao);
			}
			if(sheet.getRow(j).getCell(14) != null){		
				Cell volkg = sheet.getRow(j).getCell(14);
				nofiBean.setVolumeNetSalesKg(volkg.toString());
			}
			if(sheet.getRow(j).getCell(15) != null){	
				Cell valnt = sheet.getRow(j).getCell(15);
				nofiBean.setValorNota(valnt.toString());
			}
			
			if(sheet.getRow(j).getCell(16) != null){	
				Cell vendaLiq = sheet.getRow(j).getCell(16);
				nofiBean.setValorVendaLiquida(vendaLiq.toString());
			}
			if(sheet.getRow(j).getCell(17) != null){	
				nofiBean.setLtHalf(sheet.getRow(j).getCell(17).toString());
			}	
			if(sheet.getRow(j).getCell(18) != null){	
				nofiBean.setLtFull(sheet.getRow(j).getCell(18).toString());
			}
			if(sheet.getRow(j).getCell(19) != null){	
				nofiBean.setCanalVendas(sheet.getRow(j).getCell(19).toString());
			}
			if(sheet.getRow(j).getCell(20) != null){	
				nofiBean.setCanal(sheet.getRow(j).getCell(20).toString());
			}
			if(sheet.getRow(j).getCell(21) != null){	
				Cell volumecx = sheet.getRow(j).getCell(21);
				nofiBean.setVolumeNetSalesCx(volumecx.toString());
			}

			if(sheet.getRow(j).getCell(22) != null){	
				nofiBean.setNomeVendedor(sheet.getRow(j).getCell(22).toString());
			}
			if(sheet.getRow(j).getCell(23) != null){	
				nofiBean.setNomeFantasia(sheet.getRow(j).getCell(23).toString());
			}
			if(sheet.getRow(j).getCell(24) != null){	
				nofiBean.setPlanta(sheet.getRow(j).getCell(24).toString());
			}
			if(sheet.getRow(j).getCell(25) != null){	
				nofiBean.setTipoPedido(sheet.getRow(j).getCell(25).toString());
			}
		
		nofiBeanList.add(nofiBean);	
		
			} catch (Exception e) {
			
				
			} 
		}
		return nofiBeanList;
	
		
	}

}
