package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbResponsavelRespBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsCdtbResponsavelRespDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbResponsavelRespBusiness {

	CsCdtbResponsavelRespDao respDao = new CsCdtbResponsavelRespDao();
	private DAOLog daoLog = new DAOLog();

	public String createUpdateResp(CsCdtbResponsavelRespBean respBean) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogResponsavel(respBean, "createUpdateResp");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = respDao.createUpdateResp(respBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getResponsavel(int resp_id_cd_responsavel) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogResponsavel(null, "getResponsavel");
		logsBean.setLogsTxInfoentrada(resp_id_cd_responsavel + "");

		CsCdtbResponsavelRespBean responsavelBean = new CsCdtbResponsavelRespBean();

		String jsonRetorno = "";

		try {

			responsavelBean = respDao.getResp(resp_id_cd_responsavel);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(responsavelBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListResponsaveis(String inativo) throws JsonProcessingException {

		List<CsCdtbResponsavelRespBean> list = new ArrayList<CsCdtbResponsavelRespBean>();

		String jsonRetorno = "";

		try {

			list = respDao.getLisResponsavel(inativo);

		} catch (Exception e) {
			e.printStackTrace();

		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(list);

		return jsonRetorno;
	}

	private EsLgtbLogsLogsBean getLogResponsavel(CsCdtbResponsavelRespBean respBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbResponsavelRespBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");
		logsBean.setLogsTxInfoentrada(getAllValues(respBean));

		if (respBean != null) {
			logsBean.setLogsDsChave(respBean.getResp_nm_responsavel());
		}

		return logsBean;
	}

	private String getAllValues(CsCdtbResponsavelRespBean respBean) {

		String all = "";

		if (respBean != null) {
			all = "" + respBean.getResp_id_cd_responsavel() + ";" + respBean.getResp_nm_responsavel() + ";"
					+ respBean.getResp_dh_registro() + ";" + respBean.getResp_in_inativo() + ";" + "";
		}

		return all;
	}

}
