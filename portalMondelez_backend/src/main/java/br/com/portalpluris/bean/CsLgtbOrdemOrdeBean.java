package br.com.portalpluris.bean;

import java.util.Date;

public class CsLgtbOrdemOrdeBean {
	
	String orde_ds_plant;
	String orde_ds_salesdocumenttype;
	String orde_ds_salesdocumentnumber;
	String orde_ds_item;
	String orde_ds_overallprocessingstatus;
	String orde_ds_entrydate;
	String orde_ds_soldtoparty;
	String orde_ds_materialnumber;
	String orde_ds_personnelnumber;
	String orde_ds_billingdate;
	Date orde_dh_requestdeliverydate;
	Date orde_dh_rddfromcustomer;
	Date orde_dh_deliveryconfirmationdate;
	Date orde_dh_deliveryscheduledate;
	String orde_ds_volumoftheitem;
	String orde_ds_netvalue;
	String orde_ds_discount;
	String orde_ds_icmivalue;
	String orde_ds_ipi3value;
	String orde_ds_ips3value;
	String orde_ds_icn3value;
	String orde_ds_ics3value;
	String orde_ds_customerponumber;
	String orde_ds_percentagediscountitem;
	String orde_ds_maxallowed;
	String orde_ds_minallowed;
	String orde_ds_simulatednumber;
	String orde_ds_sourcenfnumber;
	String orde_ds_sourcenfseries;
	String orde_ds_cancellingreason;
	String orde_ds_commercialpolicy;
	String orde_ds_deliveryoccurance;
	String orde_ds_deliveryoccurancesolndate;
	String orde_ds_delivery;
	String orde_ds_billingdocument;
	String orde_ds_ninedigitnfenumber;
	String orde_ds_series;
	String orde_ds_ordernumber;
	String orde_ds_orderitemreasoncode2;
	String orde_ds_derquantitysalesunits;
	String orde_ds_salesunit;
	String orde_ds_salesorganization;
	String orde_ds_distributionchannel;
	String orde_ds_division;
	String orde_ds_customerpotype;
	String orde_ds_quantidade;
	String orde_ds_orderreason;
	String orde_ds_reasonrejection;
	String orde_ds_backordernumber;
	String orde_ds_termspaymentkey;
	String orde_dh_registro;
	String orde_ds_statusprocessamento;
	String orde_dh_processamento;
	String orde_ds_nomearquivo;
	public String getOrde_ds_plant() {
		return orde_ds_plant;
	}
	public void setOrde_ds_plant(String orde_ds_plant) {
		this.orde_ds_plant = orde_ds_plant;
	}
	public String getOrde_ds_salesdocumenttype() {
		return orde_ds_salesdocumenttype;
	}
	public void setOrde_ds_salesdocumenttype(String orde_ds_salesdocumenttype) {
		this.orde_ds_salesdocumenttype = orde_ds_salesdocumenttype;
	}
	public String getOrde_ds_salesdocumentnumber() {
		return orde_ds_salesdocumentnumber;
	}
	public void setOrde_ds_salesdocumentnumber(String orde_ds_salesdocumentnumber) {
		this.orde_ds_salesdocumentnumber = orde_ds_salesdocumentnumber;
	}
	public String getOrde_ds_item() {
		return orde_ds_item;
	}
	public void setOrde_ds_item(String orde_ds_item) {
		this.orde_ds_item = orde_ds_item;
	}
	public String getOrde_ds_overallprocessingstatus() {
		return orde_ds_overallprocessingstatus;
	}
	public void setOrde_ds_overallprocessingstatus(
			String orde_ds_overallprocessingstatus) {
		this.orde_ds_overallprocessingstatus = orde_ds_overallprocessingstatus;
	}
	public String getOrde_ds_entrydate() {
		return orde_ds_entrydate;
	}
	public void setOrde_ds_entrydate(String orde_ds_entrydate) {
		this.orde_ds_entrydate = orde_ds_entrydate;
	}
	public String getOrde_ds_soldtoparty() {
		return orde_ds_soldtoparty;
	}
	public void setOrde_ds_soldtoparty(String orde_ds_soldtoparty) {
		this.orde_ds_soldtoparty = orde_ds_soldtoparty;
	}
	public String getOrde_ds_materialnumber() {
		return orde_ds_materialnumber;
	}
	public void setOrde_ds_materialnumber(String orde_ds_materialnumber) {
		this.orde_ds_materialnumber = orde_ds_materialnumber;
	}
	public String getOrde_ds_personnelnumber() {
		return orde_ds_personnelnumber;
	}
	public void setOrde_ds_personnelnumber(String orde_ds_personnelnumber) {
		this.orde_ds_personnelnumber = orde_ds_personnelnumber;
	}
	public String getOrde_ds_billingdate() {
		return orde_ds_billingdate;
	}
	public void setOrde_ds_billingdate(String orde_ds_billingdate) {
		this.orde_ds_billingdate = orde_ds_billingdate;
	}
	public Date getOrde_dh_requestdeliverydate() {
		return orde_dh_requestdeliverydate;
	}
	public void setOrde_dh_requestdeliverydate(Date orde_dh_requestdeliverydate) {
		this.orde_dh_requestdeliverydate = orde_dh_requestdeliverydate;
	}
	public Date getOrde_dh_rddfromcustomer() {
		return orde_dh_rddfromcustomer;
	}
	public void setOrde_dh_rddfromcustomer(Date orde_dh_rddfromcustomer) {
		this.orde_dh_rddfromcustomer = orde_dh_rddfromcustomer;
	}
	public Date getOrde_dh_deliveryconfirmationdate() {
		return orde_dh_deliveryconfirmationdate;
	}
	public void setOrde_dh_deliveryconfirmationdate(
			Date orde_dh_deliveryconfirmationdate) {
		this.orde_dh_deliveryconfirmationdate = orde_dh_deliveryconfirmationdate;
	}
	public Date getOrde_dh_deliveryscheduledate() {
		return orde_dh_deliveryscheduledate;
	}
	public void setOrde_dh_deliveryscheduledate(Date orde_dh_deliveryscheduledate) {
		this.orde_dh_deliveryscheduledate = orde_dh_deliveryscheduledate;
	}
	public String getOrde_ds_volumoftheitem() {
		return orde_ds_volumoftheitem;
	}
	public void setOrde_ds_volumoftheitem(String orde_ds_volumoftheitem) {
		this.orde_ds_volumoftheitem = orde_ds_volumoftheitem;
	}
	public String getOrde_ds_netvalue() {
		return orde_ds_netvalue;
	}
	public void setOrde_ds_netvalue(String orde_ds_netvalue) {
		this.orde_ds_netvalue = orde_ds_netvalue;
	}
	public String getOrde_ds_discount() {
		return orde_ds_discount;
	}
	public void setOrde_ds_discount(String orde_ds_discount) {
		this.orde_ds_discount = orde_ds_discount;
	}
	public String getOrde_ds_icmivalue() {
		return orde_ds_icmivalue;
	}
	public void setOrde_ds_icmivalue(String orde_ds_icmivalue) {
		this.orde_ds_icmivalue = orde_ds_icmivalue;
	}
	public String getOrde_ds_ipi3value() {
		return orde_ds_ipi3value;
	}
	public void setOrde_ds_ipi3value(String orde_ds_ipi3value) {
		this.orde_ds_ipi3value = orde_ds_ipi3value;
	}
	public String getOrde_ds_ips3value() {
		return orde_ds_ips3value;
	}
	public void setOrde_ds_ips3value(String orde_ds_ips3value) {
		this.orde_ds_ips3value = orde_ds_ips3value;
	}
	public String getOrde_ds_icn3value() {
		return orde_ds_icn3value;
	}
	public void setOrde_ds_icn3value(String orde_ds_icn3value) {
		this.orde_ds_icn3value = orde_ds_icn3value;
	}
	public String getOrde_ds_ics3value() {
		return orde_ds_ics3value;
	}
	public void setOrde_ds_ics3value(String orde_ds_ics3value) {
		this.orde_ds_ics3value = orde_ds_ics3value;
	}
	public String getOrde_ds_customerponumber() {
		return orde_ds_customerponumber;
	}
	public void setOrde_ds_customerponumber(String orde_ds_customerponumber) {
		this.orde_ds_customerponumber = orde_ds_customerponumber;
	}
	public String getOrde_ds_percentagediscountitem() {
		return orde_ds_percentagediscountitem;
	}
	public void setOrde_ds_percentagediscountitem(
			String orde_ds_percentagediscountitem) {
		this.orde_ds_percentagediscountitem = orde_ds_percentagediscountitem;
	}
	public String getOrde_ds_maxallowed() {
		return orde_ds_maxallowed;
	}
	public void setOrde_ds_maxallowed(String orde_ds_maxallowed) {
		this.orde_ds_maxallowed = orde_ds_maxallowed;
	}
	public String getOrde_ds_minallowed() {
		return orde_ds_minallowed;
	}
	public void setOrde_ds_minallowed(String orde_ds_minallowed) {
		this.orde_ds_minallowed = orde_ds_minallowed;
	}
	public String getOrde_ds_simulatednumber() {
		return orde_ds_simulatednumber;
	}
	public void setOrde_ds_simulatednumber(String orde_ds_simulatednumber) {
		this.orde_ds_simulatednumber = orde_ds_simulatednumber;
	}
	public String getOrde_ds_sourcenfnumber() {
		return orde_ds_sourcenfnumber;
	}
	public void setOrde_ds_sourcenfnumber(String orde_ds_sourcenfnumber) {
		this.orde_ds_sourcenfnumber = orde_ds_sourcenfnumber;
	}
	public String getOrde_ds_sourcenfseries() {
		return orde_ds_sourcenfseries;
	}
	public void setOrde_ds_sourcenfseries(String orde_ds_sourcenfseries) {
		this.orde_ds_sourcenfseries = orde_ds_sourcenfseries;
	}
	public String getOrde_ds_cancellingreason() {
		return orde_ds_cancellingreason;
	}
	public void setOrde_ds_cancellingreason(String orde_ds_cancellingreason) {
		this.orde_ds_cancellingreason = orde_ds_cancellingreason;
	}
	public String getOrde_ds_commercialpolicy() {
		return orde_ds_commercialpolicy;
	}
	public void setOrde_ds_commercialpolicy(String orde_ds_commercialpolicy) {
		this.orde_ds_commercialpolicy = orde_ds_commercialpolicy;
	}
	public String getOrde_ds_deliveryoccurance() {
		return orde_ds_deliveryoccurance;
	}
	public void setOrde_ds_deliveryoccurance(String orde_ds_deliveryoccurance) {
		this.orde_ds_deliveryoccurance = orde_ds_deliveryoccurance;
	}
	public String getOrde_ds_deliveryoccurancesolndate() {
		return orde_ds_deliveryoccurancesolndate;
	}
	public void setOrde_ds_deliveryoccurancesolndate(
			String orde_ds_deliveryoccurancesolndate) {
		this.orde_ds_deliveryoccurancesolndate = orde_ds_deliveryoccurancesolndate;
	}
	public String getOrde_ds_delivery() {
		return orde_ds_delivery;
	}
	public void setOrde_ds_delivery(String orde_ds_delivery) {
		this.orde_ds_delivery = orde_ds_delivery;
	}
	public String getOrde_ds_billingdocument() {
		return orde_ds_billingdocument;
	}
	public void setOrde_ds_billingdocument(String orde_ds_billingdocument) {
		this.orde_ds_billingdocument = orde_ds_billingdocument;
	}
	public String getOrde_ds_ninedigitnfenumber() {
		return orde_ds_ninedigitnfenumber;
	}
	public void setOrde_ds_ninedigitnfenumber(String orde_ds_ninedigitnfenumber) {
		this.orde_ds_ninedigitnfenumber = orde_ds_ninedigitnfenumber;
	}
	public String getOrde_ds_series() {
		return orde_ds_series;
	}
	public void setOrde_ds_series(String orde_ds_series) {
		this.orde_ds_series = orde_ds_series;
	}
	public String getOrde_ds_ordernumber() {
		return orde_ds_ordernumber;
	}
	public void setOrde_ds_ordernumber(String orde_ds_ordernumber) {
		this.orde_ds_ordernumber = orde_ds_ordernumber;
	}
	public String getOrde_ds_orderitemreasoncode2() {
		return orde_ds_orderitemreasoncode2;
	}
	public void setOrde_ds_orderitemreasoncode2(String orde_ds_orderitemreasoncode2) {
		this.orde_ds_orderitemreasoncode2 = orde_ds_orderitemreasoncode2;
	}
	public String getOrde_ds_derquantitysalesunits() {
		return orde_ds_derquantitysalesunits;
	}
	public void setOrde_ds_derquantitysalesunits(
			String orde_ds_derquantitysalesunits) {
		this.orde_ds_derquantitysalesunits = orde_ds_derquantitysalesunits;
	}
	public String getOrde_ds_salesunit() {
		return orde_ds_salesunit;
	}
	public void setOrde_ds_salesunit(String orde_ds_salesunit) {
		this.orde_ds_salesunit = orde_ds_salesunit;
	}
	public String getOrde_ds_salesorganization() {
		return orde_ds_salesorganization;
	}
	public void setOrde_ds_salesorganization(String orde_ds_salesorganization) {
		this.orde_ds_salesorganization = orde_ds_salesorganization;
	}
	public String getOrde_ds_distributionchannel() {
		return orde_ds_distributionchannel;
	}
	public void setOrde_ds_distributionchannel(String orde_ds_distributionchannel) {
		this.orde_ds_distributionchannel = orde_ds_distributionchannel;
	}
	public String getOrde_ds_division() {
		return orde_ds_division;
	}
	public void setOrde_ds_division(String orde_ds_division) {
		this.orde_ds_division = orde_ds_division;
	}
	public String getOrde_ds_customerpotype() {
		return orde_ds_customerpotype;
	}
	public void setOrde_ds_customerpotype(String orde_ds_customerpotype) {
		this.orde_ds_customerpotype = orde_ds_customerpotype;
	}
	public String getOrde_ds_quantidade() {
		return orde_ds_quantidade;
	}
	public void setOrde_ds_quantidade(String orde_ds_quantidade) {
		this.orde_ds_quantidade = orde_ds_quantidade;
	}
	public String getOrde_ds_orderreason() {
		return orde_ds_orderreason;
	}
	public void setOrde_ds_orderreason(String orde_ds_orderreason) {
		this.orde_ds_orderreason = orde_ds_orderreason;
	}
	public String getOrde_ds_reasonrejection() {
		return orde_ds_reasonrejection;
	}
	public void setOrde_ds_reasonrejection(String orde_ds_reasonrejection) {
		this.orde_ds_reasonrejection = orde_ds_reasonrejection;
	}
	public String getOrde_ds_backordernumber() {
		return orde_ds_backordernumber;
	}
	public void setOrde_ds_backordernumber(String orde_ds_backordernumber) {
		this.orde_ds_backordernumber = orde_ds_backordernumber;
	}
	public String getOrde_ds_termspaymentkey() {
		return orde_ds_termspaymentkey;
	}
	public void setOrde_ds_termspaymentkey(String orde_ds_termspaymentkey) {
		this.orde_ds_termspaymentkey = orde_ds_termspaymentkey;
	}
	public String getOrde_dh_registro() {
		return orde_dh_registro;
	}
	public void setOrde_dh_registro(String orde_dh_registro) {
		this.orde_dh_registro = orde_dh_registro;
	}
	public String getOrde_ds_statusprocessamento() {
		return orde_ds_statusprocessamento;
	}
	public void setOrde_ds_statusprocessamento(String orde_ds_statusprocessamento) {
		this.orde_ds_statusprocessamento = orde_ds_statusprocessamento;
	}
	public String getOrde_dh_processamento() {
		return orde_dh_processamento;
	}
	public void setOrde_dh_processamento(String orde_dh_processamento) {
		this.orde_dh_processamento = orde_dh_processamento;
	}
	public String getOrde_ds_nomearquivo() {
		return orde_ds_nomearquivo;
	}
	public void setOrde_ds_nomearquivo(String orde_ds_nomearquivo) {
		this.orde_ds_nomearquivo = orde_ds_nomearquivo;
	}		

}
