package br.com.portalpluris.bean;

import java.util.Date;

public class CsLgbtPedidosdeliveryPediBean {
	
	// PEDI_CD_SALESDOCUMENT
	private String pedi_cd_salesdocument;
	
	// PEDI_DS_NETVALUE
	private String pedi_ds_netvalue;
	
	// PEDI_DS_PURCHASEORDERNUMBER
	private String pedi_ds_purchaseordernumber;
	
	// PEDI_DH_CREATEDON
	private Date pedi_dh_createdon; 
	
	// PEDI_DS_CREATEDBY 
	private String pedi_ds_createdby;
	
	// PEDI_DS_SOLDTOPARTY
	private String pedi_ds_soldtoparty;
	
	// PEDI_DS_SALESDOCUMENTTYPE
	private String pedi_ds_salesdocumenttype;
	
	// PEDI_DH_DOCUMENTDATE 
	private Date pedi_dh_documentdate;
	
	// PEDI_DS_DISTRIBUTIONCHANNEL
	private String pedi_ds_distributionchannel;
	
	// PEDI_DS_DOCUMENTCURRENCY
	private String pedi_ds_documentcurrency; 
	
	// PEDI_DS_SALESORGANIZATION 
	private String pedi_ds_salesorganization;
	
	// PEDI_DH_REGISTRO 
	private Date pedi_dh_registro;
	
	// PEDI_DS_STATUSPROCESSAMENTO 
	private String pedi_ds_statusprocessamento;
	
	// PEDI_DH_PROCESSAMENTO 
	private Date pedi_dh_processamento;
	
	// PEDI_DS_NOMEARQUIVO 
	private String pedi_ds_nomearquivo;

	public String getPedi_cd_salesdocument() {
		return pedi_cd_salesdocument;
	}

	public void setPedi_cd_salesdocument(String pedi_cd_salesdocument) {
		this.pedi_cd_salesdocument = pedi_cd_salesdocument;
	}

	public String getPedi_ds_netvalue() {
		return pedi_ds_netvalue;
	}

	public void setPedi_ds_netvalue(String pedi_ds_netvalue) {
		this.pedi_ds_netvalue = pedi_ds_netvalue;
	}

	public String getPedi_ds_purchaseordernumber() {
		return pedi_ds_purchaseordernumber;
	}

	public void setPedi_ds_purchaseordernumber(String pedi_ds_purchaseordernumber) {
		this.pedi_ds_purchaseordernumber = pedi_ds_purchaseordernumber;
	}

	public Date getPedi_dh_createdon() {
		return pedi_dh_createdon;
	}

	public void setPedi_dh_createdon(Date pedi_dh_createdon) {
		this.pedi_dh_createdon = pedi_dh_createdon;
	}

	public String getPedi_ds_createdby() {
		return pedi_ds_createdby;
	}

	public void setPedi_ds_createdby(String pedi_ds_createdby) {
		this.pedi_ds_createdby = pedi_ds_createdby;
	}

	public String getPedi_ds_soldtoparty() {
		return pedi_ds_soldtoparty;
	}

	public void setPedi_ds_soldtoparty(String pedi_ds_soldtoparty) {
		this.pedi_ds_soldtoparty = pedi_ds_soldtoparty;
	}

	public String getPedi_ds_salesdocumenttype() {
		return pedi_ds_salesdocumenttype;
	}

	public void setPedi_ds_salesdocumenttype(String pedi_ds_salesdocumenttype) {
		this.pedi_ds_salesdocumenttype = pedi_ds_salesdocumenttype;
	}

	public Date getPedi_dh_documentdate() {
		return pedi_dh_documentdate;
	}

	public void setPedi_dh_documentdate(Date pedi_dh_documentdate) {
		this.pedi_dh_documentdate = pedi_dh_documentdate;
	}

	public String getPedi_ds_distributionchannel() {
		return pedi_ds_distributionchannel;
	}

	public void setPedi_ds_distributionchannel(String pedi_ds_distributionchannel) {
		this.pedi_ds_distributionchannel = pedi_ds_distributionchannel;
	}

	public String getPedi_ds_documentcurrency() {
		return pedi_ds_documentcurrency;
	}

	public void setPedi_ds_documentcurrency(String pedi_ds_documentcurrency) {
		this.pedi_ds_documentcurrency = pedi_ds_documentcurrency;
	}

	public String getPedi_ds_salesorganization() {
		return pedi_ds_salesorganization;
	}

	public void setPedi_ds_salesorganization(String pedi_ds_salesorganization) {
		this.pedi_ds_salesorganization = pedi_ds_salesorganization;
	}

	public Date getPedi_dh_registro() {
		return pedi_dh_registro;
	}

	public void setPedi_dh_registro(Date pedi_dh_registro) {
		this.pedi_dh_registro = pedi_dh_registro;
	}

	public String getPedi_ds_statusprocessamento() {
		return pedi_ds_statusprocessamento;
	}

	public void setPedi_ds_statusprocessamento(String pedi_ds_statusprocessamento) {
		this.pedi_ds_statusprocessamento = pedi_ds_statusprocessamento;
	}

	public Date getPedi_dh_processamento() {
		return pedi_dh_processamento;
	}

	public void setPedi_dh_processamento(Date pedi_dh_processamento) {
		this.pedi_dh_processamento = pedi_dh_processamento;
	}

	public String getPedi_ds_nomearquivo() {
		return pedi_ds_nomearquivo;
	}

	public void setPedi_ds_nomearquivo(String pedi_ds_nomearquivo) {
		this.pedi_ds_nomearquivo = pedi_ds_nomearquivo;
	}
	
}
