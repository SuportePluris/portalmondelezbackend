package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.CsNgtbEmailsEmaiBean;

public class CsNgtbEmailsEmaiDao {

	public List<CsNgtbEmailsEmaiBean> teste() {
		
		List<CsNgtbEmailsEmaiBean> lstEmaiBean = new ArrayList<CsNgtbEmailsEmaiBean>();
		
		CsNgtbEmailsEmaiBean emaiBean;		
		
		//emaiBean = new CsNgtbEmailsEmaiBean();
		//emaiBean.setEmaiDsDestinatario("rafaelsilva@plurismidia.com.br");
		//lstEmaiBean.add(emaiBean);
		
		emaiBean = new CsNgtbEmailsEmaiBean();
		emaiBean.setEmaiDsDestinatario("supervisaomondelez@plurismidia.com.br");
		lstEmaiBean.add(emaiBean);
		
		emaiBean = new CsNgtbEmailsEmaiBean();
		emaiBean.setEmaiDsDestinatario("coordenacaomondelez@plurismidia.com.br");
		lstEmaiBean.add(emaiBean);
		
		
		
		return lstEmaiBean;
	}
	
	public List<CsNgtbEmailsEmaiBean> getEmails(int embaIdCdEmbarqueEmba, String destinatario, int userIdCdUsuario) throws SQLException {

		List<CsNgtbEmailsEmaiBean> lstEmaiBean = new ArrayList<CsNgtbEmailsEmaiBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;

		String query = "";
		
		if(destinatario.equalsIgnoreCase("emailTransportadora")) {
			
			query = ""
					+ " SELECT DISTINCT "
					+ " 	TRML.TRML_DS_EMAIL AS EMAIL"
					+ " FROM "
					+ " 	CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) "
					+ " 	INNER JOIN CS_CDTB_TRANSPORTADORA_TRAN TRANSP (NOLOCK) "
					+ " 		ON EMBA.EMBA_DS_COD_TRANSPORTADORA = TRANSP.TRAN_DS_CODIGOTRANSPORTADORA "
					+ " 	INNER JOIN CS_CDTB_TRANSPORTADORAEMAIL_TRML TRML (NOLOCK) "
					+ " 		ON TRANSP.TRAN_DS_CODIGOTRANSPORTADORA = TRML.TRML_DS_CODIGOTRANSPORTADORA "
					+ " WHERE "
					+ " 	 EMBA.EMBA_ID_CD_EMBARQUE = ? "
					+ " 	 AND TRML.TRML_DS_EMAIL IS NOT NULL "
					+ " UNION ALL "
					+ " SELECT DISTINCT "
					+ " 	U2.USER_DS_EMAIL AS EMAIL "
					+ " FROM "
					+ " 	CS_CDTB_USUARIO_USER U1 (NOLOCK) "
					+ " 	INNER JOIN CS_CDTB_USUARIO_USER U2 (NOLOCK) "
					+ " 		ON U1.USER_ID_CD_CANAL = U2.USER_ID_CD_CANAL "
					+ " WHERE "
					+ " 	U1.USER_ID_CD_USUARIO = ? "
					+ " 	AND U2.USER_DS_EMAIL <> '' "
					+ " 	AND U2.USER_DS_EMAIL IS NOT NULL "
					+ " UNION ALL "
					+ " SELECT DISTINCT "
					+ " 	DEFI_DS_EMAIL AS EMAIL "
					+ " FROM "
					+ " 	CS_CDTB_DESTINATARIOSFIXOS_DEFI (NOLOCK) "
					+ "";
			
		}else if(destinatario.equalsIgnoreCase("emailCliente")) {
			
			query = "" 
					+ " SELECT DISTINCT "
					+ " 	COCL.COCL_DS_EMAILCONTATOCLIENTE AS EMAIL" 
					+ " FROM "
					+ " 	CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) " 
					+ " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) "
					+ " 		ON EMBA.EMBA_ID_CD_CODCLIENTE = CLIE.CLIE_ID_CD_CODIGOCLIENTE " 
					+ " 	INNER JOIN CS_CDTB_CONTATOCLIENTE_COCL COCL (NOLOCK) "
					+ " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = COCL.COCL_ID_CD_CODIGOCLIENTE "
					+ " WHERE "
					+ " 	 EMBA.EMBA_ID_CD_EMBARQUE = ? "
					+ " UNION ALL "
					+ " SELECT DISTINCT "
					+ " 	U2.USER_DS_EMAIL AS EMAIL "
					+ " FROM "
					+ " 	CS_CDTB_USUARIO_USER U1 (NOLOCK) "
					+ " 	INNER JOIN CS_CDTB_USUARIO_USER U2 (NOLOCK) "
					+ " 		ON U1.USER_ID_CD_CANAL = U2.USER_ID_CD_CANAL "
					+ " WHERE "
					+ " 	U1.USER_ID_CD_USUARIO = ? "
					+ " UNION ALL "
					+ " SELECT DISTINCT "
					+ " 	DEFI_DS_EMAIL AS EMAIL "
					+ " FROM "
					+ " 	CS_CDTB_DESTINATARIOSFIXOS_DEFI (NOLOCK) "
					+ "";
		}

		pst = conn.prepareStatement(query);
		pst.setInt(1, embaIdCdEmbarqueEmba);
		pst.setInt(2, userIdCdUsuario);

		rs = pst.executeQuery();

		while (rs.next()) {

			CsNgtbEmailsEmaiBean emaiBean = new CsNgtbEmailsEmaiBean();
			emaiBean.setEmaiDsDestinatario(rs.getString("EMAIL"));

			lstEmaiBean.add(emaiBean);
		}

		return lstEmaiBean;
	}

	public void gravaLogEnvio(CsNgtbEmailsEmaiBean emaiBean) throws SQLException {

		Connection conn = DAO.getConexao();

		
		
			String query = "" + " INSERT INTO CS_NGTB_EMAILS_EMAI ( "
					+ " 	USER_ID_CD_USUARIO, EMBA_ID_CD_EMBARQUE, EMAI_TX_CONTEUDO, EMAI_DS_ASSUNTO, EMAI_DS_DESTINATARIO, EMAI_IN_ENVIADO, EMAI_DS_ERRO "
					+ " )VALUES (?, ?, ?, ?, ?, ?, ?) " + "";
	
			PreparedStatement pst = conn.prepareStatement(query);
	
			pst.setInt(1, emaiBean.getUserIdCdUsuario());
			pst.setInt(2, emaiBean.getEmbaIdCdEmbarque());
			pst.setString(3, emaiBean.getEmaiTxConteudo());
			pst.setString(4, emaiBean.getEmaiDsAssunto());
			pst.setString(5, emaiBean.getEmaiDsDestinatario());
			pst.setString(6, emaiBean.getEmaiInEnviado());
			pst.setString(7, emaiBean.getEmaiDsErro());
	
			pst.execute();
			
		

	}
}
