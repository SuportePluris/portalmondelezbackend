package br.com.portalpluris.bean;

public class ImportacaoCodTranspBean {
	
	String cod_transp;
	String id_shipment;
	
	public String getCod_transp() {
		return cod_transp;
	}
	public void setCod_transp(String cod_transp) {
		this.cod_transp = cod_transp;
	}
	public String getId_shipment() {
		return id_shipment;
	}
	public void setId_shipment(String id_shipment) {
		this.id_shipment = id_shipment;
	}	

}
