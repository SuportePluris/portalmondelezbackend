package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbSkudeliverySkudBean;

public class CsLgbtskudeliverySkudDao {
	
	/**
	 * Metodo responsavel por realizar o insert das informacoes da planilha de sku delivery na tabela CS_LGTB_SKUDELIVERY_SKUD
	 * @author Caio Fernandes
	 * @param List<CsLgtbSkudeliverySkudBean> lstSkuDeliveryBean
	 * */
	public void insertSkuDelivery(List<CsLgtbSkudeliverySkudBean> lstSkuDeliveryBean, String nmArquivo) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		UtilDate util = new UtilDate();
		
		log.info("[insertSkuDelivery] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstSkuDeliveryBean.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertSkuDelivery] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" 	INSERT CS_LGTB_SKUDELIVERY_SKUD ( "
						+ "		SKUD_CD_DELIVERY, "
						+ "		SKUD_CD_REFERENCEDOCUMENT, 	"
						+ "		SKUD_DS_ITEM, 	"
						+ " 	SKUD_DS_SHIPTOPARTY, 	"
						+ " 	SKUD_DS_NAMESHIPTOPARTY,	"
						+ " 	SKUD_DS_MATERIAL,	"
						+ " 	SKUD_DS_DESCRIPTION, "
						+ " 	SKUD_DS_DELIVERYQUANTITY, "
						+ " 	SKUD_DS_SALESUNIT, "
						+ " 	SKUD_DH_MATERIALAVAILDATE, "
						+ " 	SKUD_DS_NETWEIGHT, "
						+ " 	SKUD_DS_TOTALWEIGHT, "
						+ " 	SKUD_DS_WEIGHTUNIT, "
						+ " 	SKUD_DS_VOLUME, "
						+ " 	SKUD_DS_VOLUMEUNIT, "
						+ " 	SKUD_DS_STORAGELOCATION, "
						+ " 	SKUD_DS_BATCH, "
						+ " 	SKUD_DS_PLANT, "
						+ " 	SKUD_DH_ACTGDSMVMNTDATE, "
						+ " 	SKUD_DH_DELIVERYDATE, "
						+ " 	SKUD_DS_ROUTE, "
						+ " 	SKUD_DS_ACTUALDELIVERYQTY, "
						+ " 	SKUD_DS_VENDORLOCATION, "
						+ " 	SKUD_DS_LOCATIONSHIPTOPARTY, "
						+ " 	SKUD_DH_LOADINGDATE, "
						+ " 	SKUD_DS_DELIVERYTYPE, "
						+ " 	SKUD_DS_SHIPPINPOINT, "
						+ " 	SKUD_DS_DELIVERYPRIORITY, "
						+ " 	SKUD_DH_GOODSISSUEDATE, "
						+ " 	SKUD_DH_TRANSPTNPLANGDATE, "
						+ " 	SKUD_DH_PICKINGDATE, "
						+ " 	SKUD_DS_LOCATIONSOLDTOPARTY, "
						+ " 	SKUD_DS_VENDOR, "
						+ " 	SKUD_DS_DISTRIBUTIONCHANNEL, "
						+ " 	SKUD_DS_SALESORGANIZATION, "
						+ " 	SKUD_DS_WAREHOUSENUMBER, "
						+ " 	SKUD_CD_SHIPMENT, "
						+ " 	SKUD_DS_NOMEARQUIVO )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?, ?,?,?,?,?,?,?,?,?, ?, ?,?,?,?,?, ?,?,?,?, ?, ?, ?,?,?,?,?,?,?,?)	"
						+ "");
				
				pst.setString(1, lstSkuDeliveryBean.get(i).getSkud_cd_delivery());
				pst.setString(2, lstSkuDeliveryBean.get(i).getSkud_cd_referencedocument());
				pst.setString(3, lstSkuDeliveryBean.get(i).getSkud_ds_item());
				pst.setString(4, lstSkuDeliveryBean.get(i).getSkud_ds_shiptoparty());
				pst.setString(5, lstSkuDeliveryBean.get(i).getSkud_ds_nameshiptoparty());
				pst.setString(6, lstSkuDeliveryBean.get(i).getSkud_ds_material());
				pst.setString(7, lstSkuDeliveryBean.get(i).getSkud_ds_description());
				pst.setString(8, lstSkuDeliveryBean.get(i).getSkud_ds_deliveryquantity());
				pst.setString(9, lstSkuDeliveryBean.get(i).getSkud_ds_salesunit());
				pst.setString(10, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstSkuDeliveryBean.get(i).getSkud_dh_materialavaildate())); 
				pst.setString(11, lstSkuDeliveryBean.get(i).getSkud_ds_netweight());
				pst.setString(12, lstSkuDeliveryBean.get(i).getSkud_ds_totalweight());
				pst.setString(13, lstSkuDeliveryBean.get(i).getSkud_ds_weightunit());
				pst.setString(14, lstSkuDeliveryBean.get(i).getSkud_ds_volume());
				pst.setString(15, lstSkuDeliveryBean.get(i).getSkud_ds_volumeunit());
				pst.setString(16, lstSkuDeliveryBean.get(i).getSkud_ds_storagelocation());
				pst.setString(17, lstSkuDeliveryBean.get(i).getSkud_ds_batch());
				pst.setString(18, lstSkuDeliveryBean.get(i).getSkud_ds_plant());
				pst.setString(19, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstSkuDeliveryBean.get(i).getSkud_dh_actgdsmvmntdate()));
				pst.setString(20, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstSkuDeliveryBean.get(i).getSkud_dh_deliverydate())); 
				pst.setString(21, lstSkuDeliveryBean.get(i).getSkud_ds_route());
				pst.setString(22, lstSkuDeliveryBean.get(i).getSkud_ds_actualdeliveryqty());
				pst.setString(23, lstSkuDeliveryBean.get(i).getSkud_ds_vendorlocation());
				pst.setString(24, lstSkuDeliveryBean.get(i).getSkud_ds_locationshiptoparty());
				pst.setString(25, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstSkuDeliveryBean.get(i).getSkud_dh_loadingdate()));
				pst.setString(26, lstSkuDeliveryBean.get(i).getSkud_ds_deliverytype());
				pst.setString(27, lstSkuDeliveryBean.get(i).getSkud_ds_shippinpoint());
				pst.setString(28, lstSkuDeliveryBean.get(i).getSkud_ds_deliverypriority());
				pst.setString(29, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstSkuDeliveryBean.get(i).getSkud_dh_goodsissuedate())); 
				pst.setString(30, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstSkuDeliveryBean.get(i).getSkud_dh_transptnplangdate()));
				pst.setString(31, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstSkuDeliveryBean.get(i).getSkud_dh_pickingdate())); 
				pst.setString(32, lstSkuDeliveryBean.get(i).getSkud_ds_locationsoldtoparty());
				pst.setString(33, lstSkuDeliveryBean.get(i).getSkud_ds_vendor());
				pst.setString(34, lstSkuDeliveryBean.get(i).getSkud_ds_distributionchannel());
				pst.setString(35, lstSkuDeliveryBean.get(i).getSkud_ds_salesorganization());
				pst.setString(36, lstSkuDeliveryBean.get(i).getSkud_ds_warehousenumber());
				pst.setString(37, lstSkuDeliveryBean.get(i).getSkud_cd_shipment());
				pst.setString(38, nmArquivo);
						
				pst.execute();
				pst.close();
				
				log.info("[insertSkuDelivery] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[insertSkuDelivery] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		//conn.close();
		
		log.info("[insertSkuDelivery] - FIM ");
		
	}

}
