package br.com.portalpluris.business;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilParseToInt;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.HierarquiaClienteBean;
import br.com.portalpluris.dao.CsCdtbHierarquiaClienteDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.DAOLog;

public class ImportacaoHierarquiaClienteBusiness {

	public void importadorArquivoHierarquiaCliente() {

		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("*** INICIO DO SERVIÇO DE IMPORTAÇÃO DE ARQUIVO HIERARQUIA CLIENTE ******");

		// DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoHierarquiaClienteBusiness");
		logsBean.setLogsDsMetodo("importadorArquivoHierarquiaCliente");
		logsBean.setLogsInErro("N");
		UtilMoveArquivo utilMove = new UtilMoveArquivo();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		String diretorio = confDao.slctConf("conf.importacao.hierarquia.cliente");
		String diretorioBKP = confDao.slctConf("conf.importacao.hierarquia.cliente.bkp");		
		String diretorioErro = confDao.slctConf("conf.importacao.hierarquia.cliente.erro");
		
		File arquivoProcessado = null;
		File arquivoProcessadobkp = null;
		File arquivoProcessadoerro = new File(diretorioErro+ "/");
		

		int linha = 0;

		HierarquiaClienteBean hierarquiaCliente = new HierarquiaClienteBean();
		HierarquiaClienteBean hierarquiaClieRet = new HierarquiaClienteBean();
		UtilParseToInt parseInt = new UtilParseToInt();
		CsCdtbHierarquiaClienteDao hirarDao = new CsCdtbHierarquiaClienteDao();

		try {

			File arquivo = new File(diretorio);
			String[] arq = arquivo.list();

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String dhImportacao = sdf.format(date);

			if(arq != null){

			for (int i = 0; i < arq.length; i++) {

				 arquivoProcessado = new File(diretorio + "/" + arq[i]);
				 arquivoProcessadobkp = new File(diretorioBKP + "/" + arq[i]);

				String nmArquivo = arq[i] + "_" + dhImportacao;

				if (arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

					log.info("*** INICIO DA LEITURA DO ARQUIVO " + arquivoProcessado.getName() + "******");
					FileInputStream file = new FileInputStream(arquivoProcessado);

					Workbook wb = WorkbookFactory.create(file);
					Sheet sheet = wb.getSheetAt(0);

						for (int j = 7; j < sheet.getLastRowNum(); j++) {

							linha = j;

							Cell source = sheet.getRow(j).getCell(1);
							source.setCellType(CellType.STRING);
							source.toString();
							int clienteCustumer = parseInt.toInt(source.toString());
							hierarquiaCliente.setCdCliente(clienteCustumer);

							hierarquiaCliente.setRazaoSocial(sheet.getRow(j).getCell(2).toString());
							hierarquiaCliente.setNivel1(sheet.getRow(j).getCell(3).toString());
							hierarquiaCliente.setNivel2(sheet.getRow(j).getCell(4).toString());
							hierarquiaCliente.setCdGerenteNacional(sheet.getRow(j).getCell(5).toString());
							hierarquiaCliente.setDsGerenteNacional(sheet.getRow(j).getCell(6).toString());
							hierarquiaCliente.setCdGerenteRegional(sheet.getRow(j).getCell(7).toString());
							hierarquiaCliente.setDsGerenteRegional(sheet.getRow(j).getCell(8).toString());
							hierarquiaCliente.setCdGerenteArea(sheet.getRow(j).getCell(9).toString());
							hierarquiaCliente.setDsGerenteArea(sheet.getRow(j).getCell(10).toString());
							hierarquiaCliente.setCdVendedor(sheet.getRow(j).getCell(11).toString());
							hierarquiaCliente.setDsVendedor(sheet.getRow(j).getCell(12).toString());

							Cell s1 = sheet.getRow(j).getCell(13);
							s1.setCellType(CellType.STRING);
							s1.toString();
							int salesDistrict = parseInt.toInt(source.toString());
							hierarquiaCliente.setSalesDistrinct(salesDistrict);

							log.info("*** INICIO DA VALIDAÇÃO SE É UM UPDATE OU INSERT ******");

							hierarquiaClieRet = hirarDao.slcHierarquiaCliente(clienteCustumer);

							if (hierarquiaClieRet.getCdCliente() == 0) {

								log.info("INSERT");

								hirarDao.insertHierarquiaCliente(hierarquiaCliente, clienteCustumer, dhImportacao,
										nmArquivo);

							} else {

								log.info("UPDATE");
								hirarDao.updateHierarquiaCliente(hierarquiaCliente, clienteCustumer, dhImportacao,
										nmArquivo);
							}

						}

					boolean movido = utilMove.moveArquivo(arquivoProcessado,arquivoProcessadobkp);
					
					if(movido){
						log.info("Arquivo movido com sucesso");
					}else{
						log.info("Não foi possivel mover o arquivo");
					}

				}

			}
		}

		} catch (Exception e) {
			log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO HIERARQUIA CLIENTE" + e + " ******");
			log.info("*** LINHA:" + linha + "************");
			logsBean.setLogsTxErro( "LINHA DE ERRO:" +linha +e.getMessage());
			logsBean.setLogsTxErro(e.getMessage());

			utilMove.moveArquivo(arquivoProcessado,arquivoProcessadoerro);

		} finally {
			daoLog.createLog(logsBean);
			log.info("**** FIM SERVIÇO DE IMPORTAÇÃO DE ARQUIVO HIERARQUIA CLIENTE ****");
		}

	}

}
