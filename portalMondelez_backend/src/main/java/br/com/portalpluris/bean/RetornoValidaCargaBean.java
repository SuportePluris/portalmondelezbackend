package br.com.portalpluris.bean;

import java.util.ArrayList;

public class RetornoValidaCargaBean {
	
	
	private RetornoQuantidadeImportadosBean qtdProcessados = new RetornoQuantidadeImportadosBean ();
	

	private ArrayList<RetornoCargaBean> validacaoCarga = new ArrayList<RetornoCargaBean>();



	public RetornoQuantidadeImportadosBean getQtdProcessados() {
		return qtdProcessados;
	}


	public void setQtdProcessados(RetornoQuantidadeImportadosBean qtdProcessados) {
		this.qtdProcessados = qtdProcessados;
	}


	public ArrayList<RetornoCargaBean> getValidacaoCarga() {
		return validacaoCarga;
	}


	public void setValidacaoCarga(ArrayList<RetornoCargaBean> validacaoCarga) {
		this.validacaoCarga = validacaoCarga;
	}
	
	
	
	
	
	
	
}
