package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbFollowupFoupBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.RetornoListFoupBean;
import br.com.portalpluris.dao.DAOLog;
import br.com.portalpluris.dao.CsCdtbFollowupFoupDao;

public class CsCdtbFollowupFoupBusiness {

	CsCdtbFollowupFoupDao foupDao = new CsCdtbFollowupFoupDao();
	private DAOLog daoLog = new DAOLog();

	public String createUpdateFoup(CsCdtbFollowupFoupBean foupBean) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogFoup(foupBean, "createUpdateFoup");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = foupDao.createUpdateFoup(foupBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListCadastroFoup(String foupInAtivo) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogFoup(null, "getListFoup");
		logsBean.setLogsTxInfoentrada(foupInAtivo);

		List<CsCdtbFollowupFoupBean> lstFoupBean = new ArrayList<CsCdtbFollowupFoupBean>();

		String jsonRetorno = "";

		try {

			lstFoupBean = foupDao.getListFoup(foupInAtivo);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(lstFoupBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}
	
	
	
	

	public String getFoup(int foupIdCdFollowup) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogFoup(null, "getFoup");
		logsBean.setLogsTxInfoentrada(foupIdCdFollowup + "");

		CsCdtbFollowupFoupBean foupBean = new CsCdtbFollowupFoupBean();

		String jsonRetorno = "";

		try {

			foupBean = foupDao.getFoup(foupIdCdFollowup);
		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(foupBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	private EsLgtbLogsLogsBean getLogFoup(CsCdtbFollowupFoupBean foupBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbFollowupFoupBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");
		logsBean.setLogsTxInfoentrada(getAllValues(foupBean));

		if (foupBean != null) {
			logsBean.setLogsDsChave(foupBean.getFoup_ds_followup());
		}

		return logsBean;
	}

	private String getAllValues(CsCdtbFollowupFoupBean foupBean) {

		String all = "";

		if (foupBean != null) {
			all = "" + foupBean.getFoup_id_cd_followup() + ";" + foupBean.getFoup_ds_followup() + ";"
					+ foupBean.getFoup_dh_registro() + ";" + foupBean.getFoup_in_inativo() + ";"
					+ foupBean.getFoup_id_cd_usuarioatualizacao() + ";" + foupBean.getFoup_dh_atualizacao() + ";"
					+ foupBean.getFoup_id_cd_usuariocriacao() + ";" + "";
		}

		return all;
	}

	public String getFoupbyStatus(String idStatus) throws JsonProcessingException {
		
		
		ArrayList<RetornoListFoupBean> retFoupList = new ArrayList<RetornoListFoupBean>();
		RetornoListFoupBean retFoup = new RetornoListFoupBean();
		
		String jsonRetorno = "";

		try {

			retFoupList = foupDao.getListFoupByStatus(idStatus);

			if (retFoupList.size() <= 0){	
								
				retFoup.setError("Não foi encontrado Foups para esse Id Status");
				retFoupList.add(retFoup);
			}

			
		} catch (Exception e) {
			e.printStackTrace();		
			
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retFoupList);	
	
		return jsonRetorno;
	
	}
}

