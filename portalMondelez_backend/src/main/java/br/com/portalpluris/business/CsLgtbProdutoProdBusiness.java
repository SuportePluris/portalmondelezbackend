package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import br.com.catapi.util.UtilMoveArquivo;
import br.com.portalpluris.bean.CsLgtbProdutoProdBean;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsLgtbProdutoProdDao;

public class CsLgtbProdutoProdBusiness {

	/**
	 * Metodo responsavel por realizar leitura e gravacao das informacoes de produto
	 * disponibilizadas via arquivo TXT no banco de dados
	 * 
	 * @author Guilherme Sabino
	 */

	public void gravarProduto() {

		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("[gravarProduto] - INICIO ");

		try {

			CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
			UtilMoveArquivo utilMove = new UtilMoveArquivo();

			// CONFIGURACOES DE DIRETORIOS
			String dirProduto = confDao.slctConf("conf.importacao.agendamento.shipment");
			String diretorioBkp = confDao.slctConf("conf.importacao.agendamento.shipment.bkp");
			String diretorioErro = confDao.slctConf("conf.importacao.agendamento.shipment.erro");

			// LEITURA DOS ARQUIVOS DO DIRETORIO
			File arquivo = new File(dirProduto);
			String[] arq = arquivo.list();

			if (arq != null) {

				CsLgtbProdutoProdBean arqRet = new CsLgtbProdutoProdBean();

				log.info("[gravarProduto] - CARREGANDO ARQUIVOS TXT ");
				log.info(" ******** " + arq.length + " arquivo(s) carregado(s). ******** ");

				for (int i = 0; i < arq.length; i++) {

					// ARQUIVO A SER PROCESSADO
					File arquivoProcessado = new File(dirProduto + "/" + arq[i]);

					try {

						if (arquivoProcessado.getName().toUpperCase().endsWith(".TXT")) {

							String nmArquivo = arquivoProcessado.getName();
							log.info("INICIANDO LEITURA DO ARQUIVO TXT: " + nmArquivo);

							// INSTANCIAR ARQUIVO
							FileInputStream fileInputStream = new FileInputStream(arquivoProcessado);
							BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));

							// CONTROLES DO ARQUIVO
							String conteudo = "";
							int linha = 0;

							// FORMATANDO DATA
							Date date = new Date();
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
							String dhProcessamento = sdf.format(date);

							// FOR DAS LINHAS DO ARQUIVO
							while (((conteudo = bufferedReader.readLine()) != null)) {

								arqRet = new CsLgtbProdutoProdBean();

								try {

									linha++;
									log.info("LENDO CONTEUDO DO ARQUIVO ");
									log.info("CONTEUDO DA LINHA: " + linha + "" + conteudo);

									// IDENTIFICAR FINAL DO ARQUIVO
									if (conteudo.substring(0, 10).indexOf("TOTAL") > -1) {

										// FIM DO ARQUIVO IDENTIFICADO
										log.info("FIM DO ARQUIVO IDENTIFICADO");

									} else {

										// LEITURA DOS CAMPOS
										// OS VALORES AO LADO REPRESENTAM O TAMANHO DO CAMPO
										// NÃO HÁ UM DELIMITADOR, SOMENTE PELO TAMANHO DA STRING.

										arqRet.setProd_cd_materialcode(conteudo.substring(1, 18).trim()); // 18
										arqRet.setProd_ds_materialdescription(conteudo.substring(19, 58).trim()); // 39
										arqRet.setProd_ds_divisiondescription(conteudo.substring(59, 78).trim()); // 19

										// REALIZAR O INSERT DAS LINHAS NO BANCO
										CsLgtbProdutoProdDao produtoProdDao = new CsLgtbProdutoProdDao();
										produtoProdDao.insertProduto(arqRet, nmArquivo, dhProcessamento);

									}

								} catch (Exception e) {

									log.info("ERRO NA LEITURA DA LINHA:" + linha);
									log.info("Erro:" + e);
									e.printStackTrace();

								}

							}

							// MOVER ARQUIVO PARA PASTA BKP
							File arquivoProcessadoBkp = new File(diretorioBkp + "/" + arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoBkp);
						}

					} catch (Exception e) {

						log.info("[gravarShipment] - ERRO AO LER O ARQUIVO PRODUTO ");

						// MOVER ARQUIVO PARA PASTA DE ERRO
						if (arq != null) {
							File arquivoProcessadoErro = new File(diretorioErro + "/" + arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoErro);
						}

					}
				}

			}

		} catch (Exception e) {
			log.info(" [gravarProduto] - ERRO AO GRAVAR PRODUTO ");
		}
	}
}
