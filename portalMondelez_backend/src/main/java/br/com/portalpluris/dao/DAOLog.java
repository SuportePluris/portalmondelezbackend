package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import br.com.portalpluris.bean.EsLgtbLogsLogsBean;



public class DAOLog {
	
	public void createLog(EsLgtbLogsLogsBean logsBean) {
		
		try {
			
			Connection conn = DAO.getConexao();
			
			String sql = ""
					+ "	INSERT INTO ES_LGTB_LOGS_LOGS ( "
					+ " 	LOGS_DS_CLASSE, "
					+ "		LOGS_DS_METODO, "
					+ "		LOGS_DS_CHAVE, "
					+ "		LOGS_TX_INFOENTRADA, "
					+ "		LOGS_TX_INFOSAIDA, "
					+ "		LOGS_TX_INFOADICIONAIS, "
					+ "		LOGS_DH_INICIO, "
					+ "		LOGS_DH_FIM, "
					+ "		LOGS_IN_ERRO, "
					+ "		LOGS_TX_ERRO "
					+ "	) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
					+ "";
			
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, logsBean.getLogsDsClasse());
			pst.setString(2, logsBean.getLogsDsMetodo());
			pst.setString(3, logsBean.getLogsDsChave());
			pst.setString(4, logsBean.getLogsTxInfoentrada());
			pst.setString(5, logsBean.getLogsTxInfosaida());
			pst.setString(6, logsBean.getLogsTxInfoadicionais());
			pst.setTimestamp(7, logsBean.getLogsDhInicio());
			pst.setTimestamp(8, logsBean.getLogsDhFim());
			pst.setString(9, logsBean.getLogsInErro());
			pst.setString(10, logsBean.getLogsTxErro());
			
			pst.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}