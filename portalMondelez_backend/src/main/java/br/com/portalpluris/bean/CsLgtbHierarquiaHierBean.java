package br.com.portalpluris.bean;

import java.sql.Timestamp;

public class CsLgtbHierarquiaHierBean {

	String hier_cd_hierarquiacode;
	String hier_ds_constant;
	String hier_ds_saleshierarchy;
	String hier_ds_description;
	String hier_ds_saleshierarchyregion;
	String hier_ds_description2;
	String hier_ds_salesareahierarchy;
	String hier_ds_description3;
	String hier_ds_territorysaleshierarchy;
	String hier_ds_namealpha;
	String hier_ds_addressnumbersalesman;
	String hier_ds_customercode;
	Timestamp hier_dh_registro;
	String hier_ds_statusprocessamento;
	Timestamp hier_dh_processamento;
	String hier_ds_nomearquivo;

	public String getHier_cd_hierarquiacode() {
		return hier_cd_hierarquiacode;
	}

	public void setHier_cd_hierarquiacode(String hier_cd_hierarquiacode) {
		this.hier_cd_hierarquiacode = hier_cd_hierarquiacode;
	}

	public String getHier_ds_constant() {
		return hier_ds_constant;
	}

	public void setHier_ds_constant(String hier_ds_constant) {
		this.hier_ds_constant = hier_ds_constant;
	}

	public String getHier_ds_saleshierarchy() {
		return hier_ds_saleshierarchy;
	}

	public void setHier_ds_saleshierarchy(String hier_ds_saleshierarchy) {
		this.hier_ds_saleshierarchy = hier_ds_saleshierarchy;
	}

	public String getHier_ds_description() {
		return hier_ds_description;
	}

	public void setHier_ds_description(String hier_ds_description) {
		this.hier_ds_description = hier_ds_description;
	}

	public String getHier_ds_saleshierarchyregion() {
		return hier_ds_saleshierarchyregion;
	}

	public void setHier_ds_saleshierarchyregion(String hier_ds_saleshierarchyregion) {
		this.hier_ds_saleshierarchyregion = hier_ds_saleshierarchyregion;
	}

	public String getHier_ds_description2() {
		return hier_ds_description2;
	}

	public void setHier_ds_description2(String hier_ds_description2) {
		this.hier_ds_description2 = hier_ds_description2;
	}

	public String getHier_ds_salesareahierarchy() {
		return hier_ds_salesareahierarchy;
	}

	public void setHier_ds_salesareahierarchy(String hier_ds_salesareahierarchy) {
		this.hier_ds_salesareahierarchy = hier_ds_salesareahierarchy;
	}

	public String getHier_ds_description3() {
		return hier_ds_description3;
	}

	public void setHier_ds_description3(String hier_ds_description3) {
		this.hier_ds_description3 = hier_ds_description3;
	}

	public String getHier_ds_territorysaleshierarchy() {
		return hier_ds_territorysaleshierarchy;
	}

	public void setHier_ds_territorysaleshierarchy(String hier_ds_territorysaleshierarchy) {
		this.hier_ds_territorysaleshierarchy = hier_ds_territorysaleshierarchy;
	}

	public String getHier_ds_namealpha() {
		return hier_ds_namealpha;
	}

	public void setHier_ds_namealpha(String hier_ds_namealpha) {
		this.hier_ds_namealpha = hier_ds_namealpha;
	}

	public String getHier_ds_addressnumbersalesman() {
		return hier_ds_addressnumbersalesman;
	}

	public void setHier_ds_addressnumbersalesman(String hier_ds_addressnumbersalesman) {
		this.hier_ds_addressnumbersalesman = hier_ds_addressnumbersalesman;
	}

	public String getHier_ds_customercode() {
		return hier_ds_customercode;
	}

	public void setHier_ds_customercode(String hier_ds_customercode) {
		this.hier_ds_customercode = hier_ds_customercode;
	}

	public Timestamp getHier_dh_registro() {
		return hier_dh_registro;
	}

	public void setHier_dh_registro(Timestamp hier_dh_registro) {
		this.hier_dh_registro = hier_dh_registro;
	}

	public String getHier_ds_statusprocessamento() {
		return hier_ds_statusprocessamento;
	}

	public void setHier_ds_statusprocessamento(String hier_ds_statusprocessamento) {
		this.hier_ds_statusprocessamento = hier_ds_statusprocessamento;
	}

	public Timestamp getHier_dh_processamento() {
		return hier_dh_processamento;
	}

	public void setHier_dh_processamento(Timestamp hier_dh_processamento) {
		this.hier_dh_processamento = hier_dh_processamento;
	}

	public String getHier_ds_nomearquivo() {
		return hier_ds_nomearquivo;
	}

	public void setHier_ds_nomearquivo(String hier_ds_nomearquivo) {
		this.hier_ds_nomearquivo = hier_ds_nomearquivo;
	}

}
