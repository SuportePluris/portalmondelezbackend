package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.logging.Logger;

import br.com.catapi.util.UtilMoveArquivo;
import br.com.portalpluris.bean.CsLgtbCustomerCustBean;
import br.com.portalpluris.bean.CsLgtbShipmentShipBean;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsLgtbCustomerCustDao;

public class CsLgtbCustomerCustBusiness {

	/**
	 * Metodo responsavel por realizar leitura e gravacao das informacoes de customer disponibilizadas via arquivo TXT no banco de dados
	 * @author Caio Fernandes
	 * */
	public void gravarCustomer() {

		Logger log = Logger.getLogger(this.getClass().getName());	
		log.info("[gravarCustomer] - INICIO ");

		try {

			List<CsLgtbCustomerCustBean> lstCustomer = new ArrayList<CsLgtbCustomerCustBean>();
			CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
			UtilMoveArquivo utilMove = new UtilMoveArquivo();

			// CONFIGURACOES DE DIRETORIOS
			//String dirCustomer = "C:\\Users\\caraujo\\Desktop\\MONDELEZ_IMP\\SHIPMENT\\";
			String dirCustomer = confDao.slctConf("conf.importacao.cadastro.diretorio.customer");
			String dirCustomerBkp = confDao.slctConf("conf.importacao.cadastro.diretorio.customer.bkp");
			String dirCustomerErro = confDao.slctConf("conf.importacao.cadastro.diretorio.customer.erro");

			// LEITURA DOS ARQUIVOS DO DIRETORIO
			File arquivo = new File(dirCustomer);
			String[] arq = arquivo.list();

			if(arq != null){

				CsLgtbCustomerCustBean customerBean = new CsLgtbCustomerCustBean();
				log.info("[gravarCustomer] - CARREGANDO ARQUIVOS TXT ");
				log.info( " ******** "+ arq.length + " ARQUIVOS(S) CARREGADOS(S). ******* ");

				for (int i = 0; i < arq.length; i++) {
					
					// ARQUIVO A SER PROCESSADO
					File arquivoProcessado = new File(dirCustomer + "\\" +  arq[i]);
					
					try {

						if(arquivoProcessado.getName().toUpperCase().endsWith(".TXT")){
							
							String nmArquivo = arquivoProcessado.getName();
							log.info("INICIANDO LEITURA DO ARQUIVO TXT: " + nmArquivo);	
							
							// INSTANCIAR ARQUIVO
							FileInputStream fileInputStream = new FileInputStream(arquivoProcessado);
							BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
							
							// CONTROLES DO ARQUIVO
							String conteudo = "";
							int linha = 0;

							// FOR DAS LINHAS DO ARQUIVO
							while (((conteudo = bufferedReader.readLine()) != null)) {

								try{
									
									linha++;
									log.info("LENDO CONTEUDO DO ARQUIVO ");
									log.info("CONTEUDO DA LINHA: " + linha + "" + conteudo);	

									// SETAR VALORES DO ARQUIVO NO BEAN
									lstCustomer = setListaCustomerBean(lstCustomer, conteudo);
									
								} catch (Exception e) {
									log.info("ERRO NA LEITURA DA LINHA:"+ linha);
									log.info("Erro:"+e);
									e.printStackTrace();				

								}
							}		
							
							// REALIZAR O INSERT DA LISTA NO BANCO
							CsLgtbCustomerCustDao customerCustDao = new CsLgtbCustomerCustDao();
							customerCustDao.insertCustomer(lstCustomer, nmArquivo);
							
							// MOVER ARQUIVO PARA PASTA BKP
							File arquivoProcessadoBkp = new File(dirCustomerBkp + "\\" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoBkp);
						}
						
					} catch (Exception e) {
						log.info("[gravarShipment] - ERRO AO LER AQUIVO CUSTOMER ");
						
						// MOVER ARQUIVO PARA PASTA DE ERRO
						if(arq != null) {
							File arquivoProcessadoErro = new File(dirCustomerErro + "\\" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoErro);
						}
						
					}

				}
			}

		} catch (Exception e) {
			log.info("[gravarCustomer] - ERRO AO GRAVAR CUSTOMER ");
		}

	}

	/**
	 * Metodo responsavel por obter os valores do arquivo e retornar a lista de CsLgtbCustomerCustBean preenchida.
	 * @author Caio Fernandes
	 * @param List<CsLgtbCustomerCustBean>, String, String
	 * @return List<CsLgtbCustomerCustBean>
	 * */
	private List<CsLgtbCustomerCustBean> setListaCustomerBean(List<CsLgtbCustomerCustBean> lstCustomer, String conteudo) {
			
		// LEITURA DOS CAMPOS DO ARQUIVO
		CsLgtbCustomerCustBean customerBean = new CsLgtbCustomerCustBean();
		
		// IDENTIFICAR FINAL DO ARQUIVO
		if(conteudo.substring(0,10).indexOf("TOTAL") > -1) {
			// FIM DO ARQUIVO IDENTIFICADO
			return lstCustomer;
		}
		
		customerBean.setCust_cd_customercode(conteudo.substring(0,10).trim());
		customerBean.setCust_ds_customername(conteudo.substring(10,45).trim());
		customerBean.setCust_ds_addressship(conteudo.substring(45,80).trim());
		customerBean.setCust_ds_adresscomplement(conteudo.substring(80,115).trim());
		customerBean.setCust_ds_adresscomplement2(conteudo.substring(115,356).trim());
		customerBean.setCust_ds_citymunicipio(conteudo.substring(356,391).trim());
		customerBean.setCust_ds_cep(conteudo.substring(391,401).trim());
		customerBean.setCust_ds_state(conteudo.substring(401,404).trim());
		customerBean.setCust_ds_city(conteudo.substring(404,439).trim());
		customerBean.setCust_ds_codddphone(conteudo.substring(439,455).trim());
		customerBean.setCust_ds_phonenumber(conteudo.substring(455,471).trim());
		customerBean.setCust_ds_customernumber(conteudo.substring(471,487).trim());
		customerBean.setCust_ds_impadicind(conteudo.substring(487,505).trim());
		customerBean.setcust_ds_ibgecode(conteudo.substring(505,507).trim());
		customerBean.setCust_ds_statecode(conteudo.substring(507,510).trim());
		customerBean.setCust_ds_codcitycustomer(conteudo.substring(510,514).trim());
		customerBean.setCust_ds_citydescription(conteudo.substring(514,549).trim());
		
		lstCustomer.add(customerBean);
		
		return lstCustomer;
	}

}
