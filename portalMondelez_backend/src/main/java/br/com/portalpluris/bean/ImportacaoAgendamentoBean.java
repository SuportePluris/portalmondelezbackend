package br.com.portalpluris.bean;

import java.util.logging.Logger;

import br.com.portalpluris.business.CsLgbtPedidosdeliveryPediBusiness;
import br.com.portalpluris.business.CsLgtbCustomerCustBusiness;
import br.com.portalpluris.business.CsLgtbNotaFiscalNofiBusiness;
import br.com.portalpluris.business.CsLgtbOrdemOrdeBusiness;
import br.com.portalpluris.business.CsLgtbShipmentShipBusiness;
import br.com.portalpluris.business.CsLgtbSkudeliverySkudBusiness;

public class ImportacaoAgendamentoBean {
	
	
	/**
	 * Metodo responsavel por iniciar a importacao dos arquivos de carga de agendamento
	 * @author Caio Fernandes
	 * */
	public void iniciarImportacao() {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("[iniciarImportacao] - INICIO IMPORTACAO CARGA");
		
		try {
			
			// 1. *** IMPORTACAO ORDEM ***
			log.info("[iniciarImportacao] - IMPORTACAO ORDEM");
			CsLgtbOrdemOrdeBusiness csLgtbOrdemOrdeBusiness = new CsLgtbOrdemOrdeBusiness();
			csLgtbOrdemOrdeBusiness.gravarOrdem();
			
			// 2. *** IMPORTACAO ORDEM ***
			log.info("[iniciarImportacao] - IMPORTACAO NOTA FISCAL");
			CsLgtbNotaFiscalNofiBusiness csLgtbNotaFiscalNofiBusiness = new CsLgtbNotaFiscalNofiBusiness();
			csLgtbNotaFiscalNofiBusiness.gravarNotaFiscal();
			
			// shipment
			
			
		} catch (Exception e) {
			log.info("[iniciarImportacao] - ERRO IMPORTACAO CARGA " + e.getMessage());
		}
		
		log.info("[iniciarImportacao] - FIM IMPORTACAO CARGA");
		
	}
	
	public static void main(String[] args) {
		//iniciarImportacao();
	}

}
