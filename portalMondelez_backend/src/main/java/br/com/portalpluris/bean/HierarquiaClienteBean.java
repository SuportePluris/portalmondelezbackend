package br.com.portalpluris.bean;

public class HierarquiaClienteBean {

	private int cdCliente;
	private String razaoSocial;
	private String nivel1;
	private String nivel2;
	private String cdGerenteNacional;
	private String dsGerenteNacional;
	private String cdGerenteRegional;
	private String dsGerenteRegional;
	private String cdGerenteArea;
	private String dsGerenteArea;
	private String cdVendedor;
	private String dsVendedor;
	private int salesDistrinct;

	public int getCdCliente() {
		return cdCliente;
	}

	public void setCdCliente(int cdCliente) {
		this.cdCliente = cdCliente;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNivel1() {
		return nivel1;
	}

	public void setNivel1(String nivel1) {
		this.nivel1 = nivel1;
	}

	public String getNivel2() {
		return nivel2;
	}

	public void setNivel2(String nivel2) {
		this.nivel2 = nivel2;
	}

	public String getCdGerenteNacional() {
		return cdGerenteNacional;
	}

	public void setCdGerenteNacional(String cdGerenteNacional) {
		this.cdGerenteNacional = cdGerenteNacional;
	}

	public String getDsGerenteNacional() {
		return dsGerenteNacional;
	}

	public void setDsGerenteNacional(String dsGerenteNacional) {
		this.dsGerenteNacional = dsGerenteNacional;
	}

	public String getCdGerenteRegional() {
		return cdGerenteRegional;
	}

	public void setCdGerenteRegional(String cdGerenteRegional) {
		this.cdGerenteRegional = cdGerenteRegional;
	}

	public String getDsGerenteRegional() {
		return dsGerenteRegional;
	}

	public void setDsGerenteRegional(String dsGerenteRegional) {
		this.dsGerenteRegional = dsGerenteRegional;
	}

	public String getCdGerenteArea() {
		return cdGerenteArea;
	}

	public void setCdGerenteArea(String cdGerenteArea) {
		this.cdGerenteArea = cdGerenteArea;
	}

	public String getDsGerenteArea() {
		return dsGerenteArea;
	}

	public void setDsGerenteArea(String dsGerenteArea) {
		this.dsGerenteArea = dsGerenteArea;
	}

	public String getCdVendedor() {
		return cdVendedor;
	}

	public void setCdVendedor(String cdVendedor) {
		this.cdVendedor = cdVendedor;
	}

	public String getDsVendedor() {
		return dsVendedor;
	}

	public void setDsVendedor(String dsVendedor) {
		this.dsVendedor = dsVendedor;
	}

	public int getSalesDistrinct() {
		return salesDistrinct;
	}

	public void setSalesDistrinct(int salesDistrinct) {
		this.salesDistrinct = salesDistrinct;
	}

}
