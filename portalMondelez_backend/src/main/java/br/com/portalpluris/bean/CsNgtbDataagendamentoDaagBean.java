package br.com.portalpluris.bean;

public class CsNgtbDataagendamentoDaagBean {

	private String daag_ds_tipoagendamento;
	private String daag_dh_agendamento;
	private String moti_ds_motivo;
	private String resp_nm_responsavel;
	private String daag_ds_senhaagendamento;
	private String daag_dh_registro;
	private String daag_ds_horaagendamento;
	
	
	public String getDaag_ds_horaagendamento() {
		return daag_ds_horaagendamento;
	}
	public void setDaag_ds_horaagendamento(String daag_ds_horaagendamento) {
		this.daag_ds_horaagendamento = daag_ds_horaagendamento;
	}
	public String getDaag_dh_registro() {
		return daag_dh_registro;
	}
	public void setDaag_dh_registro(String daag_dh_registro) {
		this.daag_dh_registro = daag_dh_registro;
	}
	public String getMoti_ds_motivo() {
		return moti_ds_motivo;
	}
	public void setMoti_ds_motivo(String moti_ds_motivo) {
		this.moti_ds_motivo = moti_ds_motivo;
	}
	public String getResp_nm_responsavel() {
		return resp_nm_responsavel;
	}
	public void setResp_nm_responsavel(String resp_nm_responsavel) {
		this.resp_nm_responsavel = resp_nm_responsavel;
	}
	public String getDaag_ds_tipoagendamento() {
		return daag_ds_tipoagendamento;
	}
	public void setDaag_ds_tipoagendamento(String daag_ds_tipoagendamento) {
		this.daag_ds_tipoagendamento = daag_ds_tipoagendamento;
	}
	public String getDaag_dh_agendamento() {
		return daag_dh_agendamento;
	}
	public void setDaag_dh_agendamento(String daag_ds_agendamento) {
		this.daag_dh_agendamento = daag_ds_agendamento;
	}
	public String getDaag_ds_senhaagendamento() {
		return daag_ds_senhaagendamento;
	}
	public void setDaag_ds_senhaagendamento(String daag_ds_senhaagendamento) {
		this.daag_ds_senhaagendamento = daag_ds_senhaagendamento;
	}
	
}
