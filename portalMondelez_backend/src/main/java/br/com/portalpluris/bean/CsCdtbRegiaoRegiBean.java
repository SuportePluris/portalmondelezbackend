package br.com.portalpluris.bean;

public class CsCdtbRegiaoRegiBean {

	int id_regi_cd_regiao;
	String regi_ds_regiao;
	String regi_in_inativo;
	String regi_dh_registro;
	
	public int getId_regi_cd_regiao() {
		return id_regi_cd_regiao;
	}
	public void setId_regi_cd_regiao(int id_regi_cd_regiao) {
		this.id_regi_cd_regiao = id_regi_cd_regiao;
	}
	public String getRegi_ds_regiao() {
		return regi_ds_regiao;
	}
	public void setRegi_ds_regiao(String regi_ds_regiao) {
		this.regi_ds_regiao = regi_ds_regiao;
	}
	public String getRegi_in_inativo() {
		return regi_in_inativo;
	}
	public void setRegi_in_inativo(String regi_in_inativo) {
		this.regi_in_inativo = regi_in_inativo;
	}
	public String getRegi_dh_registro() {
		return regi_dh_registro;
	}
	public void setRegi_dh_registro(String regi_dh_registro) {
		this.regi_dh_registro = regi_dh_registro;
	}
	
}
