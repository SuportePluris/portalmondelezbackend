package br.com.portalpluris.business;

import java.sql.SQLException;

import br.com.portalpluris.dao.ProcessaCargaDao;


public class ProcessaCargaBusiness {
	
	/**
	 * Método responsável por gerenciar o processamento das cargas inseridas nas tabelas LGTBs que refletem os seguintes arquivos:
	 * 1. Base de pedidos.xlsx (CS_LGTB_PEDIDOSDELIVERY_PEDI) 
	 * 2. DNs Alocadas.xlsx (CS_LGTB_SKUDELIVERY_SKUD) 
	 * 3. Planejamento de Transportes.xlsx (CS_LGTB_SHIPMENT_SHIP e CS_LGTB_RESUMODNS_REDN)
	 * @author Caio Fernandes
	 * */
	public void iniciarProcessamentoCarga() throws SQLException {
		
		ProcessaCargaDao processaCargaDao = new ProcessaCargaDao();
		processaCargaDao.selectLgtbsCarga();
		
	}
	
}
