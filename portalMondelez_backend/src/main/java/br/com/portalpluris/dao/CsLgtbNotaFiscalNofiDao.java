package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.elasticsearch.cluster.metadata.AliasAction.Add;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbNotaFiscalNofiBean;

public class CsLgtbNotaFiscalNofiDao {
	
	/**
	 * Metodo responsavel por realizar o insert das informacoes do arquivo TXT de ordem na tabela CS_LGTB_NOTAFISCAL_NOFI
	 * @author Caio Fernandes
	 * @param List<CsLgtbNotaFiscalNofiBean>, String
	 * */
	public void insertNotaFiscal(List<CsLgtbNotaFiscalNofiBean> lstNf, String nmArquivo) throws SQLException {
		
		UtilDate util = new UtilDate();
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[insertNotaFiscal] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE NOTA FISCAL
			for(int i = 0; i < lstNf.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertNotaFiscal] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" INSERT CS_LGTB_NOTAFISCAL_NOFI ( "
						+ "		NOFI_DS_COMPANY, " 	
						+ "		NOFI_DS_NONAME, 	" 	
						+ "		NOFI_DS_NOTAFISCALSERIE, 	"
						+ "		NOFI_DS_NOTAFISCALNUMBER, 	"		
						+ "		NOFI_DS_SALESORDERNUMBER, 	"		
						+ "		NOFI_DS_SALESORDERNUMBER2, 	"		
						+ "		NOFI_DS_SALESORDERNUMBER3, 	"		
						+ "		NOFI_DS_SALESORDERNUMBER4, 	"		
						+ "		NOFI_DH_NOTAFISCALDATE, 	"		
						+ "		NOFI_DH_DELIVERYDATE, 	"		
						+ "		NOFI_CD_CUSTOMERCODE, 	"
						+ "		NOFI_DS_TOTALCOSTNOTAFISCAL, 	"		
						+ "		NOFI_DS_QUANTITY, 	"		
						+ "		NOFI_DS_NETWEIGHT, 	"		
						+ "		NOFI_DS_GROSSWEIGHT, 	"		
						+ "		NOFI_DS_CATCOD6, 	"		
						+ "		NOFI_DS_CATCOD7, 	"		
						+ "		NOFI_DS_CATCOD8, 	"		
						+ "		NOFI_DS_CATCOD10, 	"		
						+ "		NOFI_DS_DISTRCHANNEL, 	"		
						+ "		NOFI_DS_VENDORCODE, 	"		
						+ "		NOFI_DS_CFOPCODE, 	"		
						+ "		NOFI_DS_DESCRIPTION, 	"		
						+ "		NOFI_DS_CARRIERNAME, 	"		
						+ "		NOFI_DS_CODELOCATION, 	"		
						+ "		NOFI_DS_DOCTYPE, 	"		
						+ "		NOFI_DS_NEXTNUMBER, 	"		
						+ "		NOFI_DS_VOLUMENOTA, 	"		
						+ "		NOFI_DS_NETVALUE, 	"		
						+ "		NOFI_DS_INVOICENUMBER, 	"		
						+ "		NOFI_DH_DATEINVOICE, 	"		
						+ "		NOFI_DS_STATUSPROCESSAMENTO, 	"		
						+ " 	NOFI_DS_NOMEARQUIVO )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)	"
						+ "");
				
				pst.setString(1, lstNf.get(i).getNofi_ds_company());
				pst.setString(2, lstNf.get(i).getNofi_ds_noname());
				pst.setString(3, lstNf.get(i).getNofi_ds_notafiscalserie());
				pst.setString(4, lstNf.get(i).getNofi_ds_notafiscalnumber());
				pst.setString(5, lstNf.get(i).getNofi_ds_salesordernumber());
				pst.setString(6, lstNf.get(i).getNofi_ds_salesordernumber2());
				pst.setString(7, lstNf.get(i).getNofi_ds_salesordernumber3());
				pst.setString(8, lstNf.get(i).getNofi_ds_salesordernumber4());
				pst.setString(9, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstNf.get(i).getNofi_dh_notafiscaldate()));
				pst.setString(10, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstNf.get(i).getNofi_dh_deliverydate()));
				pst.setString(11, lstNf.get(i).getNofi_cd_customercode());
				pst.setString(12, lstNf.get(i).getNofi_ds_totalcostnotafiscal());
				pst.setString(13, lstNf.get(i).getNofi_ds_quantity());
				pst.setString(14, lstNf.get(i).getNofi_ds_netweight());
				pst.setString(15, lstNf.get(i).getNofi_ds_grossweight());
				pst.setString(16, lstNf.get(i).getNofi_ds_catcod6());
				pst.setString(17, lstNf.get(i).getNofi_ds_catcod7());
				pst.setString(18, lstNf.get(i).getNofi_ds_catcod8());
				pst.setString(19, lstNf.get(i).getNofi_ds_catcod10());
				pst.setString(20, lstNf.get(i).getNofi_ds_distrchannel());
				pst.setString(21, lstNf.get(i).getNofi_ds_vendorcode());
				pst.setString(22, lstNf.get(i).getNofi_ds_cfopcode());
				pst.setString(23, lstNf.get(i).getNofi_ds_description());
				pst.setString(24, lstNf.get(i).getNofi_ds_carriername());
				pst.setString(25, lstNf.get(i).getNofi_ds_codelocation());
				pst.setString(26, lstNf.get(i).getNofi_ds_doctype());
				pst.setString(27, lstNf.get(i).getNofi_ds_nextnumber());
				pst.setString(28, lstNf.get(i).getNofi_ds_volumenota());
				pst.setString(29, lstNf.get(i).getNofi_ds_netvalue());
				pst.setString(30, lstNf.get(i).getNofi_ds_invoicenumber());
				pst.setString(31, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstNf.get(i).getNofi_dh_dateinvoice()));
				pst.setString(32, "N");
				pst.setString(33, nmArquivo);
				
				pst.execute();
				pst.close();
				
				log.info("[insertNotaFiscal] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[insertNotaFiscal] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		//conn.close();
		
		log.info("[insertNotaFiscal] - FIM ");
		
	}
	
	//DANILLO *** 29/07/2021 *** INCLUSÃO DE PERIODO BASEADO NA DATA DE EMISSÃO DA NOTA FISCAL CASO NÃO EXISTA PEGAR O MES VIGENTE
	public String slctNotaFiscalByShipment(String idCdShipment) {

		String returnDate = "";
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			
			cSQL += "	SELECT DISTINCT ";
			cSQL += "		NOFI.NOFI_DH_EMISSAONOTA "  ;
			cSQL += "	FROM " ;
			cSQL += "		CS_CDTB_NOTAFISCAL_NOFI NOFI (NOLOCK) INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
			cSQL += "			ON NOFI.NOFI_NR_DELIVERYDOCUMENT = SKUD.SKUD_ID_CD_DELIVERY " ;
			cSQL += "		INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)" ;
			cSQL += "			ON EMBA_ID_CD_SHIPMENT = SKUD_ID_CD_SHIPMENT ";
			cSQL += "	WHERE " ;
			cSQL += "		SKUD.SKUD_ID_CD_DELIVERY = ?  ";

			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, idCdShipment);

			rs = pst.executeQuery();

			if (rs.next()) {
				returnDate = rs.getString("NOFI_DH_EMISSAONOTA"); //
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		} 

		
		return returnDate;
	}

	public ArrayList<CsLgtbNotaFiscalNofiBean> slctNotaFiscalByPedidoMond( String idpedidomond) {
		
		ArrayList<CsLgtbNotaFiscalNofiBean> retNofiList = new ArrayList<CsLgtbNotaFiscalNofiBean>();
		
		try{
		
		DAO dao = new DAO();
		Connection con = dao.getConexao();

		ResultSet rs = null;

		String cSQL = " SELECT  * FROM CS_LGTB_NOTAFISCAL_NOFI WHERE NOFI_DS_SALESORDERNUMBER2 = ? ";
		
		PreparedStatement pst = con.prepareStatement(cSQL);

		pst.setString(1, idpedidomond);
		
		rs = pst.executeQuery();

//		if (rs.next()) {
		while (rs.next()) {
			
			CsLgtbNotaFiscalNofiBean retNofi = new CsLgtbNotaFiscalNofiBean();
			
			retNofi.setNofi_ds_company(rs.getString("NOFI_DS_COMPANY"));
			retNofi.setNofi_ds_noname(rs.getString("NOFI_DS_NONAME"));
			retNofi.setNofi_ds_notafiscalserie(rs.getString("NOFI_DS_NOTAFISCALSERIE"));
			retNofi.setNofi_ds_notafiscalnumber(rs.getString("NOFI_DS_NOTAFISCALNUMBER"));
			retNofi.setNofi_ds_salesordernumber(rs.getString("NOFI_DS_SALESORDERNUMBER"));
			retNofi.setNofi_ds_salesordernumber2(rs.getString("NOFI_DS_SALESORDERNUMBER2"));
			retNofi.setNofi_ds_salesordernumber3(rs.getString("NOFI_DS_SALESORDERNUMBER3"));
			retNofi.setNofi_ds_salesordernumber4(rs.getString("NOFI_DS_SALESORDERNUMBER4"));
			retNofi.setNofi_dh_notafiscaldate(rs.getDate("NOFI_DH_NOTAFISCALDATE"));
			retNofi.setNofi_cd_customercode(rs.getString("NOFI_CD_CUSTOMERCODE"));
			retNofi.setNofi_ds_totalcostnotafiscal(rs.getString("NOFI_DS_TOTALCOSTNOTAFISCAL"));
			retNofi.setNofi_ds_quantity(rs.getString("NOFI_DS_QUANTITY"));
			retNofi.setNofi_ds_netweight(rs.getString("NOFI_DS_NETWEIGHT"));
			retNofi.setNofi_ds_grossweight(rs.getString("NOFI_DS_GROSSWEIGHT"));
			retNofi.setNofi_ds_distrchannel(rs.getString("NOFI_DS_DISTRCHANNEL"));
			retNofi.setNofi_ds_vendorcode(rs.getString("NOFI_DS_VENDORCODE"));
			retNofi.setNofi_ds_cfopcode(rs.getString("NOFI_DS_CFOPCODE"));
			retNofi.setNofi_ds_description(rs.getString("NOFI_DS_DESCRIPTION"));
			retNofi.setNofi_ds_carriername(rs.getString("NOFI_DS_CARRIERNAME"));
			retNofi.setNofi_ds_codelocation(rs.getString("NOFI_DS_CODELOCATION"));
			retNofi.setNofi_ds_doctype(rs.getString("NOFI_DS_DOCTYPE"));
			retNofi.setNofi_ds_nextnumber(rs.getString("NOFI_DS_NEXTNUMBER"));
			retNofi.setNofi_ds_volumenota(rs.getString("NOFI_DS_VOLUMENOTA"));
			retNofi.setNofi_ds_netvalue(rs.getString("NOFI_DS_NETVALUE"));
			retNofi.setNofi_ds_invoicenumber(rs.getString("NOFI_DS_INVOICENUMBER"));
			
			retNofiList.add(retNofi);
		}
//		}else{			
//			retNofi.setErro("1");
//			retNofi.setRetorno("Nao foi encontrado Nota Fiscal vinculada a esse Pedido: "+ idpedidomond);			
//		}		
		
		}catch (Exception e) {
			e.printStackTrace();
			
			CsLgtbNotaFiscalNofiBean retNofi = new CsLgtbNotaFiscalNofiBean();
			
			retNofi.setErro("1");
			retNofi.setRetorno("Nao foi encontrado Nota Fiscal vinculada a esse Pedido: "+ idpedidomond);	
			
			retNofiList.add(retNofi);
		}
	
		return retNofiList;
	}

}

