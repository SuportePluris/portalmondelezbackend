package br.com.portalpluris.bean;

public class relatorioOcrBean {
	
	public String dt_ocr_agendamento;
	public String motivo_ocr_agend;
	public String responsavel_pendencia;
	
	public String getDt_ocr_agendamento() {
		return dt_ocr_agendamento;
	}
	public void setDt_ocr_agendamento(String dt_ocr_agendamento) {
		this.dt_ocr_agendamento = dt_ocr_agendamento;
	}
	public String getMotivo_ocr_agend() {
		return motivo_ocr_agend;
	}
	public void setMotivo_ocr_agend(String motivo_ocr_agend) {
		this.motivo_ocr_agend = motivo_ocr_agend;
	}
	public String getResponsavel_pendencia() {
		return responsavel_pendencia;
	}
	public void setResponsavel_pendencia(String responsavel_pendencia) {
		this.responsavel_pendencia = responsavel_pendencia;
	}
	
	

}
