package br.com.portalpluris.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter{

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Value("${security.jwt.signing-key}")
	private String signingKey;
	
	//GERA OS TOKENS 
	@Bean
	public TokenStore tokenStore(){
		return new JwtTokenStore(accessTokenConverter());
	}
	
	
	/**GERAR TOKEN APARTIR DE UMA CHAVE DE CRIPTOGRAFIA jwt **/
	@Bean
	public JwtAccessTokenConverter accessTokenConverter(){
		
		JwtAccessTokenConverter tokenConverter = new JwtAccessTokenConverter();
		tokenConverter.setSigningKey(signingKey);
		return tokenConverter;
		
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints)throws Exception {
		endpoints
			.tokenStore(tokenStore())
			.accessTokenConverter(accessTokenConverter())
			.authenticationManager(authenticationManager);
		
		 endpoints.addInterceptor(new HandlerInterceptorAdapter() {
	            @Override
	            public boolean preHandle(HttpServletRequest hsr, HttpServletResponse rs, Object o) throws Exception {
	                rs.setHeader("Access-Control-Allow-Origin", "*");
	                rs.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
	                rs.setHeader("Access-Control-Max-Age", "36000");
	                rs.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
	                return true;
	            }
	        });
	}
	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		
		//CONFIGURAR AS APLICAÇÕES QUE IRÃO ACESSAR AS APIS
		
		// PORTAL NDG DE OPERADOR
		System.out.println("\n***** Liberando usuário [ portal-suporte-dev-pluris ] *****\n ");
		clients
			.inMemory()
			//.withClient("portal-suporte-dev-pluris") 							//USER NAME PARA O OAUTH 
			.withClient("portal-mondelez") 							//USER NAME PARA O OAUTH 
			//.secret("PLURIS_9asd0-2kjh21iosow21810asodj_sd92dx")  //PASSWORD  PARA O OAUTH 
			.secret("PLURIS_789orensdhsads_dsdfdz_sd92dx")  //PASSWORD  PARA O OAUTH 
			.scopes("read","write") 									//PERMISÃO DE LEITURA (SELECT) E ESCRITA (DELETE/UPDATE)
			.authorizedGrantTypes("password")							//GRANT TYPE DO BODY 
			.accessTokenValiditySeconds(36000); 							//TEMPO DE VALIDADE DO TOKEN 30min (1800milliseconds) // 36000 = 10h
		
		
		//PORTAL NGD DE IMPORTAÇÕES
		/*System.out.println("\n***** Liberando uruário [ portal-ndg-importacao ] *****\n ");
		clients
			.inMemory()
			.withClient("portal-ndg-importacao") 							//USER NAME PARA O OAUTH 
			.secret("PLURIS_ihy7678TYU87kjkhtFUDT7gjuHJGFDYTR6567GyGu")     //PASSWORD  PARA O OAUTH 
			.scopes("read","write") 										//PERMISÃO DE LEITURA (SELECT) E ESCRITA (DELETE/UPDATE)
			.authorizedGrantTypes("password")								//GRANT TYPE DO BODY 
			.accessTokenValiditySeconds(1800); 								//TEMPO DE VALIDADE DO TOKEN 30min (1800milliseconds)
		
		*/
	}
	
}  
