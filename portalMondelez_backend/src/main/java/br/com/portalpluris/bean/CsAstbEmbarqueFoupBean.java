package br.com.portalpluris.bean;

public class CsAstbEmbarqueFoupBean {
	
	String emfo_id_cd_embafollow;
	String emfo_id_cd_embarque;
	String emfo_id_cd_followup;
	String emfo_id_cd_motivo;
	
	String emfo_id_cd_responsavel;
	String emfo_dh_registro;
	String emfo_id_cd_usuarioatualizacao;
	String emfo_dh_atualizacao;

	String emfo_id_cd_usuariocriacao;
	
	String emfo_tx_followup;
	
	
	public String getEmfo_tx_followup() {
		return emfo_tx_followup;
	}
	public void setEmfo_tx_followup(String emfo_tx_followup) {
		this.emfo_tx_followup = emfo_tx_followup;
	}
	public String getEmfo_id_cd_usuariocriacao() {
		return emfo_id_cd_usuariocriacao;
	}
	public void setEmfo_id_cd_usuariocriacao(String emfo_id_cd_usuariocriacao) {
		this.emfo_id_cd_usuariocriacao = emfo_id_cd_usuariocriacao;
	}
	public String getEmfo_id_cd_motivo() {
		return emfo_id_cd_motivo;
	}
	public void setEmfo_id_cd_motivo(String emfo_id_cd_motivo) {
		this.emfo_id_cd_motivo = emfo_id_cd_motivo;
	}
	public String getEmfo_id_cd_embafollow() {
		return emfo_id_cd_embafollow;
	}
	public void setEmfo_id_cd_embafollow(String emfo_id_cd_embafollow) {
		this.emfo_id_cd_embafollow = emfo_id_cd_embafollow;
	}
	public String getEmfo_id_cd_embarque() {
		return emfo_id_cd_embarque;
	}
	public void setEmfo_id_cd_embarque(String emfo_id_cd_embarque) {
		this.emfo_id_cd_embarque = emfo_id_cd_embarque;
	}

	public String getEmfo_id_cd_followup() {
		return emfo_id_cd_followup;
	}
	public void setEmfo_id_cd_followup(String emfo_id_cd_followup) {
		this.emfo_id_cd_followup = emfo_id_cd_followup;
	}
	public String getEmfo_id_cd_responsavel() {
		return emfo_id_cd_responsavel;
	}
	public void setEmfo_id_cd_responsavel(String emfo_id_cd_responsavel) {
		this.emfo_id_cd_responsavel = emfo_id_cd_responsavel;
	}
	public String getEmfo_dh_registro() {
		return emfo_dh_registro;
	}
	public void setEmfo_dh_registro(String emfo_dh_registro) {
		this.emfo_dh_registro = emfo_dh_registro;
	}
	public String getEmfo_id_cd_usuarioatualizacao() {
		return emfo_id_cd_usuarioatualizacao;
	}
	public void setEmfo_id_cd_usuarioatualizacao(String emfo_id_cd_usuarioatualizacao) {
		this.emfo_id_cd_usuarioatualizacao = emfo_id_cd_usuarioatualizacao;
	}
	public String getEmfo_dh_atualizacao() {
		return emfo_dh_atualizacao;
	}
	public void setEmfo_dh_atualizacao(String emfo_dh_atualizacao) {
		this.emfo_dh_atualizacao = emfo_dh_atualizacao;
	}
	
	
	
	
	
	

}
