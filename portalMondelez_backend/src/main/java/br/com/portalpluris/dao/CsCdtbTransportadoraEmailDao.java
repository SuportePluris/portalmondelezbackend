package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbTransportadoraEmail;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;

public class CsCdtbTransportadoraEmailDao {

	public boolean slctTransMail(
			CsCdtbTransportadoraEmail transMail) {		
		
		CsCdtbTransportadoraEmail retTransMail = new CsCdtbTransportadoraEmail();	
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraEmailDao");
		logsBean.setLogsDsMetodo("slctTransMail");
		logsBean.setLogsInErro("N");
		
		boolean exist = false;
		
		try {
		
		
			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "	SELECT    ";
			cSQL += "	TRML_DS_CODIGOTRANSPORTADORA,";
			cSQL +="	TRML_DS_RAZAOSCOAIL,";
			cSQL += "	TRML_DS_CNPJ,";
			cSQL += "	TRML_DS_EMAIL,";
			cSQL += "	TRML_DS_INATIVO,";
			cSQL += "	TRML_ID_USUARIOATUALIZACAO,";
			cSQL += "	TRML_DH_IMPORTACAO";
			cSQL += " 		FROM CS_CDTB_TRANSPORTADORAEMAIL_TRML (NOLOCK) ";
			cSQL += " 	WHERE  ";
			cSQL += " 		TRML_DS_EMAIL = ? ";
			cSQL += " 		AND  TRML_DS_CODIGOTRANSPORTADORA = ?";
					
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, transMail.getTrml_ds_email());
			pst.setString(2, transMail.getTrml_ds_codigotransportadora());
			
			rs = pst.executeQuery();

			if (rs.next()) {				
						
				exist = true;
			}
			
		
		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			daoLog.createLog(logsBean);

		}

		return exist;
		
	}

	public void insertTransMail(CsCdtbTransportadoraEmail transMail, String dhImportacao, String nmArquivo) {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		CsCdtbTransportadoraEmailDao transMailDao = new CsCdtbTransportadoraEmailDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraEmailDao");
		logsBean.setLogsDsMetodo("insertTransMail");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Insert na base Email Transportadora *****");
		
		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");
		
		int user = Integer.parseInt(idUser);
		
		try {
		
		boolean ifExist = transMailDao.slctTransMail(transMail);		
				
				if(!ifExist){			
			
				DAO dao = new DAO();
				Connection con = dao.getConexao();

				PreparedStatement pst = con.prepareStatement(""
				+" 	INSERT INTO CS_CDTB_TRANSPORTADORAEMAIL_TRML "
				+"( "
				+" TRML_DS_CODIGOTRANSPORTADORA, "
				+" TRML_DS_RAZAOSCOAIL, "
				+" TRML_DS_CNPJ, "
				+" TRML_DS_EMAIL, "
				+" TRML_DS_INATIVO, "
				+" TRML_DH_IMPORTACAO, "
				+" TRML_ID_USUARIOATUALIZACAO, "
				+" TRML_NM_ARQUIVOPROCESSADO "
				+")"
				+"VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

				pst.setString(1, transMail.getTrml_ds_codigotransportadora());
				pst.setString(2, transMail.getTrml_ds_razaoscoail());
				pst.setString(3, transMail.getTrml_ds_cnpj());
				pst.setString(4, transMail.getTrml_ds_email());
				pst.setString(5, transMail.getTrml_ds_inativo());
				pst.setString(6, dhImportacao);
				pst.setInt(7, user);
				pst.setString(8, nmArquivo);
				
				pst.execute();
			
		}
	
			
		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			daoLog.createLog(logsBean);

		}

	}

	public void updateTransMail(CsCdtbTransportadoraEmail transMail,
			String dhImportacao, String nmArquivo) {		
		

		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraEmailDao");
		logsBean.setLogsDsMetodo("updateTransMail");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Update na base Email Transportadora *****");
		
		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst;

			pst = con.prepareStatement(""
					
			+ "		UPDATE  CS_CDTB_TRANSPORTADORAEMAIL_TRML SET"
			+" TRML_DS_RAZAOSCOAIL = ?,"
			+" TRML_DS_CNPJ = ?,"
			+" TRML_DS_EMAIL = ?,"
			+" TRML_DS_INATIVO = ?,"
			+" TRML_DH_IMPORTACAO = ?,"
			+" TRML_ID_USUARIOATUALIZACAO = ?,"
			+" TRML_NM_ARQUIVOPROCESSADO = ? "
			+" WHERE TRML_DS_CODIGOTRANSPORTADORA = ?");
			
			pst.setString(1, transMail.getTrml_ds_razaoscoail());
			pst.setString(2, transMail.getTrml_ds_cnpj());
			pst.setString(3, transMail.getTrml_ds_email());
			pst.setString(4, "N");
			pst.setString(5, dhImportacao);
			pst.setString(6, idUser);
			pst.setString(7, nmArquivo);
			pst.setString(8, transMail.getTrml_ds_codigotransportadora());
			
			pst.executeUpdate();

		} catch (Exception e) {

			log.info("***** Erro no UPDATE na base do Transportadora Email  " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  UPDATE na base doTransportadora Email  *****");
			daoLog.createLog(logsBean);

		}
		
		
		
	}

	public String insertTransEmail(CsCdtbTransportadoraEmail transBean,
			String dhImportacao, String arq, String iduser) throws SQLException {
		
		
		String retorno = null;
		
		CsCdtbTransportadoraEmailDao transMailDao = new CsCdtbTransportadoraEmailDao();
		
		boolean ifExist = transMailDao.slctTransMail(transBean);		
		
		if(!ifExist){			
	
			DAO dao = new DAO();
			Connection con = dao.getConexao();
	
			PreparedStatement pst = con.prepareStatement(""
			+" 	INSERT INTO CS_CDTB_TRANSPORTADORAEMAIL_TRML "
			+"( "
			+" TRML_DS_CODIGOTRANSPORTADORA, "
			+" TRML_DS_RAZAOSCOAIL, "
			+" TRML_DS_CNPJ, "
			+" TRML_DS_EMAIL, "
			+" TRML_DS_INATIVO, "
			+" TRML_DH_IMPORTACAO, "
			+" TRML_ID_USUARIOATUALIZACAO, "
			+" TRML_NM_ARQUIVOPROCESSADO "
			+")"
			+"VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
	
			pst.setString(1, transBean.getTrml_ds_codigotransportadora());
			pst.setString(2, transBean.getTrml_ds_razaoscoail());
			pst.setString(3, transBean.getTrml_ds_cnpj());
			pst.setString(4, transBean.getTrml_ds_email());
			pst.setString(5, transBean.getTrml_ds_inativo());
			pst.setString(6, dhImportacao);
			pst.setString(7, iduser);
			pst.setString(8, arq);
			
			boolean sucess = pst.execute();
	
			if(sucess){
				retorno = "Contato Transportadora Inserido";
			}else{
				retorno = " Erro ao Inserir Contato Transportadora";
			}
		
		}else{
			
			retorno = " Esse e-mail já existe para o codigo de transportadora:"+ transBean.getTrml_ds_codigotransportadora();
			
		}
				

		return null;
	}


}
