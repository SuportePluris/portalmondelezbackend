package br.com.portalpluris.bean;

public class CsCdtbResponsavelRespBean {

	int resp_id_cd_responsavel;
	private String resp_nm_responsavel;
	private String resp_in_inativo;
	private String resp_dh_registro;
	
	
	public String getResp_dh_registro() {
		return resp_dh_registro;
	}
	public void setResp_dh_registro(String resp_dh_registro) {
		this.resp_dh_registro = resp_dh_registro;
	}
	public int getResp_id_cd_responsavel() {
		return resp_id_cd_responsavel;
	}

	public void setResp_id_cd_responsavel(int resp_id_cd_responsavel) {
		this.resp_id_cd_responsavel = resp_id_cd_responsavel;
	}
	public String getResp_nm_responsavel() {
		return resp_nm_responsavel;
	}
	public void setResp_nm_responsavel(String resp_nm_responsavel) {
		this.resp_nm_responsavel = resp_nm_responsavel;
	}
	public String getResp_in_inativo() {
		return resp_in_inativo;
	}
	public void setResp_in_inativo(String resp_in_inativo) {
		this.resp_in_inativo = resp_in_inativo;
	}

	
}
