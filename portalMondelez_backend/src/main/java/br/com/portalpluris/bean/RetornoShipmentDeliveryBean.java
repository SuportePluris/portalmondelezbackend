package br.com.portalpluris.bean;

import java.util.ArrayList;


public class RetornoShipmentDeliveryBean {	
	
	private RetornoShipmentBean retShip = new RetornoShipmentBean();
	
	private ArrayList<RetornoDeliveryBean> retDelivery = new ArrayList<RetornoDeliveryBean>();
	

	public ArrayList<RetornoDeliveryBean> getRetDelivery() {
		return retDelivery;
	}

	public void setRetDelivery(ArrayList<RetornoDeliveryBean> retDelivery) {
		this.retDelivery = retDelivery;
	}

	public RetornoShipmentBean getRetShip() {
		return retShip;
	}

	public void setRetShip(RetornoShipmentBean retShip) {
		this.retShip = retShip;
	}

	
	
	

}
