package br.com.portalpluris.bean;

import java.sql.Timestamp;

import br.com.portalpluris.dao.CsCdtbCanalCanaDao;

public class CsAstbUsuariocanalUscaBean {

	CsCdtbUsuarioUserBean csCdtbUsuarioUserBean =  new CsCdtbUsuarioUserBean();
	CsCdtbCanalCanaBean csCdtbCanalCanaBean =  new CsCdtbCanalCanaBean();
	
	public CsCdtbUsuarioUserBean getCsCdtbUsuarioUserBean() {
		return csCdtbUsuarioUserBean;
	}
	public void setCsCdtbUsuarioUserBean(CsCdtbUsuarioUserBean csCdtbUsuarioUserBean) {
		this.csCdtbUsuarioUserBean = csCdtbUsuarioUserBean;
	}
	public CsCdtbCanalCanaBean getCsCdtbCanalCanaBean() {
		return csCdtbCanalCanaBean;
	}
	public void setCsCdtbCanalCanaBean(CsCdtbCanalCanaBean csCdtbCanalCanaBean) {
		this.csCdtbCanalCanaBean = csCdtbCanalCanaBean;
	}
	
}
