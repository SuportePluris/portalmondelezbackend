package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilParseToInt;
import br.com.portalpluris.bean.CsCdtbTransportadoraTranBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.dao.CsCdtbTransportadoraTranDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.DAOLog;

public class ImportacaoTransportadoraBusiness {

	public void importadorArquivoTransportadora() {

		UtilParseToInt utilParse = new UtilParseToInt();

		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("*** INICIO DO SERVIÇO DE IMPORTAÇÃO DE ARQUIVO DE TRANSPORTADORA ******");
		UtilMoveArquivo utilMove = new UtilMoveArquivo();
		// DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoTransportadoraBusiness");
		logsBean.setLogsDsMetodo("importadorArquivoTransportadora");
		logsBean.setLogsInErro("N");

		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		String diretorio = confDao.slctConf("conf.importacao.transportadora");
		String diretorioBKP = confDao.slctConf("conf.importacao.transportadora.bkp");
		
		String diretorioErro = confDao.slctConf("conf.importacao.transportadora.erro");
		
		File arquivoProcessado = null;
		File arquivoProcessadobkp = null;
		File arquivoProcessadoerro = new File(diretorioErro+ "/");
		
//		String diretorio = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond";
//		String diretorioBKP = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond\\bkp";	
		
		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");
		int user = utilParse.toInt(idUser);

		File arquivo = new File(diretorio);
		String[] arq = arquivo.list();

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		String dhImportacao = sdf.format(date);
		Timestamp timestamp = new Timestamp(date.getTime());

		CsCdtbTransportadoraTranBean transBean = new CsCdtbTransportadoraTranBean();
		CsCdtbTransportadoraTranDao transDao = new CsCdtbTransportadoraTranDao();
		
		int linha = 0;

		try {

			if(arq != null){
				
			for (int i = 0; i < arq.length; i++) {

				 arquivoProcessado = new File(diretorio + "/" + arq[i]);
				 arquivoProcessadobkp = new File(diretorioBKP + "/" + arq[i]);

				String nmArquivo = arq[i] + "_" + dhImportacao;

				if (arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

					FileInputStream file = new FileInputStream(arquivoProcessado);
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file));

					Workbook wb = WorkbookFactory.create(file);
					Sheet sheet = wb.getSheetAt(0);

						log.info("*** INICIO DA LEITURA DO ARQUIVO " + arquivoProcessado.getName() + "******");

						for (int j = 1; j < sheet.getLastRowNum(); j++) {
							
							linha = j;
							
							if((sheet.getRow(j).getCell(0) == null && sheet.getRow(j).getCell(1) == null)){
								break;
							}

							transBean = new CsCdtbTransportadoraTranBean();							
							
							if(sheet.getRow(j).getCell(0) != null ||  !sheet.getRow(j).getCell(0).equals(" ")){
								
								transBean.setTran_id_customer(sheet.getRow(j).getCell(0).toString());
							}
							Cell cdContrato = sheet.getRow(j).getCell(1);
							cdContrato.setCellType(CellType.STRING);
							transBean.setTran_ds_contrato(cdContrato.toString());
							
							Cell cdRemit = sheet.getRow(j).getCell(2);
							cdRemit.setCellType(CellType.STRING);
							transBean.setTran_ds_remitto(cdRemit.toString());
							
							Cell cdTrans = sheet.getRow(j).getCell(3);
							cdTrans.setCellType(CellType.STRING);
							cdTrans.toString();
							transBean.setTran_nm_nometransportadora(sheet.getRow(j).getCell(4).toString());
							transBean.setTran_ds_codigotransportadora(cdTrans.toString());
							transBean.setTran_ds_uf(sheet.getRow(j).getCell(6).toString());
							transBean.setTran_ds_cnpj(sheet.getRow(j).getCell(7).toString());
							transBean.setTran_ds_contato(sheet.getRow(j).getCell(8).toString());
							transBean.setTran_ds_email(sheet.getRow(j).getCell(9).toString());
							transBean.setTran_ds_telefone(sheet.getRow(j).getCell(10).toString());
							transBean.setTran_ds_cockpit(sheet.getRow(j).getCell(11).toString());
							transBean.setTran_ds_status(sheet.getRow(j).getCell(12).toString());
							transBean.setTran_id_cd_usuarioatualizacao(user);
							transBean.setTran_in_inativo("N");
							transBean.setTran_dh_registro(timestamp.toString());
							

							log.info("*** VERIFICAR SE É UM NOVO REGISTRO OU UM UPDATE, ID TRANSPORTADORA: "
									+ sheet.getRow(j).getCell(2).toString() + " ******");

							boolean ifExist = transDao.slcTran(transBean.getTran_ds_codigotransportadora());

							if (!ifExist) {

								log.info("UPDATE");
								transDao.updateTrans(transBean, dhImportacao, nmArquivo);

							} else {

								log.info("INSERT");
								transDao.insertTrans(transBean, dhImportacao, nmArquivo);
							}
					}
					
					boolean movido = utilMove.moveArquivo(arquivoProcessado,arquivoProcessadobkp);
					
					if(movido){
						log.info("Arquivo movido com sucesso");
					}else{
						log.info("Não foi possivel mover o arquivo");
					}
					
					
				}
			}
		}

		} catch (Exception e) {
			log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO TRANSPORTADORA" + e + " ******");
			logsBean.setLogsInErro("S");
			log.info("*** LINHA:" + linha + "************");
			logsBean.setLogsTxErro( "LINHA DE ERRO:" +linha +e.getMessage());
			utilMove.moveArquivo(arquivoProcessado,arquivoProcessadoerro);
			
		} finally {
			daoLog.createLog(logsBean);
			log.info("**** FIM SERVIÇO DE IMPORTAÇÃO DE ARQUIVO TRANSPORTADORA ****");
		}

	}
}
