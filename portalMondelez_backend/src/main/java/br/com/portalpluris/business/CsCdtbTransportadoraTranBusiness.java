package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbTransportadoraEmail;
import br.com.portalpluris.bean.CsCdtbTransportadoraTranBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsCdtbTransportadoraTranDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbTransportadoraTranBusiness {

	CsCdtbTransportadoraTranDao tranDao = new CsCdtbTransportadoraTranDao();
	private DAOLog daoLog = new DAOLog();

	public String createUpdateTran(CsCdtbTransportadoraTranBean tranBean) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogTransportadora(tranBean, "createUpdateTran");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = tranDao.createUpdateTran(tranBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	
	public String getListTransportadoraByCodTransp(String codTransportadora) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogTransportadora(null, "getListTransportadoraByCodTransp");
		logsBean.setLogsTxInfoentrada(null);

		List<CsCdtbTransportadoraEmail> listContatosTransportadoraBean = new ArrayList<CsCdtbTransportadoraEmail>();

		String jsonRetorno = "";

		try {

			listContatosTransportadoraBean = tranDao.getListContatosTran(codTransportadora);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listContatosTransportadoraBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}
	
	
	public String getListTransportadora(String tranInAtivo) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogTransportadora(null, "getListTransportadora");
		logsBean.setLogsTxInfoentrada(tranInAtivo);

		List<CsCdtbTransportadoraTranBean> listTransportadoraBean = new ArrayList<CsCdtbTransportadoraTranBean>();

		String jsonRetorno = "";

		try {

			listTransportadoraBean = tranDao.getListTran(tranInAtivo);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listTransportadoraBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getTransportadora(String tranIdCdTransportadora) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogTransportadora(null, "getTransportadora");
		logsBean.setLogsTxInfoentrada(tranIdCdTransportadora + "");

		CsCdtbTransportadoraTranBean transportadoraBean = new CsCdtbTransportadoraTranBean();

		String jsonRetorno = "";

		try {

			transportadoraBean = tranDao.getTran(tranIdCdTransportadora);
		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(transportadoraBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	private EsLgtbLogsLogsBean getLogTransportadora(CsCdtbTransportadoraTranBean transportadoraBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraTranBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");
		logsBean.setLogsTxInfoentrada(getAllValues(transportadoraBean));

		if (transportadoraBean != null) {
			logsBean.setLogsDsChave(transportadoraBean.getTran_id_cd_transportadora() + "");
		}

		return logsBean;
	}

	private String getAllValues(CsCdtbTransportadoraTranBean transportadoraBean) {

		String all = "";

		if (transportadoraBean != null) {
			all = "" + transportadoraBean.getTran_id_cd_transportadora() + ";"
					+ transportadoraBean.getTran_id_cd_transportadora() + ";"
					+ transportadoraBean.getTran_ds_codigotransportadora() + ";"
					+ transportadoraBean.getTran_dh_registro() + ";"
					// + transportadoraBean.getTranDsTipocargatransportadora() + ";"
					+ transportadoraBean.getTran_in_inativo() + ";"
					+ transportadoraBean.getTran_id_cd_usuarioatualizacao() + ";"
					+ transportadoraBean.getTran_dh_atualizacao() + ";" + "";
		}

		return all;
	}

}
