package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.AuxiliarCsCdtbFollowupFoupBean;
import br.com.portalpluris.bean.CsAstbEmbarqueFoupBean;
import br.com.portalpluris.bean.CsCdtbFollowupFoupBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsAstbEmbarqueFoupDao {

	/**
	 * Metodo responsavel por criar um registro de associacao de embarque e foup
	 * 
	 * @author Caio Fernandes
	 * @param CsAstbEmbarqueFoupBean
	 * @return RetornoBean retBean
	 */
	public RetornoBean createEmbarqueFoup(CsAstbEmbarqueFoupBean embarqueFoupBean) {

		RetornoBean retBean = new RetornoBean();
		Connection conn = DAO.getConexao();

		// VERIFICAR SE O REGISTRO JA EXISTE ATRAVES DO ID DA EMFO
		if (conn != null) {

			if (existeEmfo(embarqueFoupBean, conn)) {

				// UPDATE
				try {
					PreparedStatement pst;

					pst = conn.prepareStatement("" + " UPDATE " + "		CS_ASTB_EMBARQUEFOUP_EMFO " + "SET	"
							+ " 	EMFO_ID_CD_EMBARQUE = ?, " 
							+ " 	EMFO_ID_CD_FOLLOWUP = ?, "
							+ " 	EMFO_DS_RESPONSAVEL = ?, " 
							+ " 	EMFO_ID_CD_USUARIOATUALIZACAO = ?, "
							+ " 	EMFO_DH_ATUALIZACAO = GETDATE(), "
							+ "		EMFO_TX_FOLLOWUP = ?" 
							+ " WHERE " 
							+ "		EMFO_ID_CD_EMBAFOLLOW = ? "
							+ "");

					pst.setString(1, embarqueFoupBean.getEmfo_id_cd_embarque());
					pst.setString(2, embarqueFoupBean.getEmfo_id_cd_followup());
					pst.setString(3, embarqueFoupBean.getEmfo_id_cd_responsavel());
					pst.setString(4, embarqueFoupBean.getEmfo_id_cd_usuarioatualizacao());
					pst.setString(5, embarqueFoupBean.getEmfo_tx_followup());
					pst.setString(6, embarqueFoupBean.getEmfo_id_cd_embafollow());

					pst.execute();
					//conn.close();
					pst.close();

					retBean.setSucesso(true);
					retBean.setProcesso("UPDATE");
					retBean.setMsg("EMBARQUE FOUP ATUALIZADO COM SUCESSO! ");

				} catch (Exception e) {
					e.printStackTrace();
					retBean.setSucesso(false);
					retBean.setProcesso("UPDATE");
					retBean.setMsg(e.getMessage());
					System.out.println("[createEmbarqueFoup] - ERRO AO ATUALIZAR EMFO ");
				}

			} else {

				// INSERT
				try {
					PreparedStatement pst;

					pst = conn.prepareStatement("" + " INSERT INTO CS_ASTB_EMBARQUEFOUP_EMFO ("
							+ " 	EMFO_ID_CD_EMBARQUE, " 
							+ " 	EMFO_ID_CD_FOLLOWUP, " 
							+ " 	EMFO_ID_CD_RESPONSAVEL,"
							+ "		EMFO_ID_CD_MOTIVO,"
							+ "		EMFO_ID_CD_USUARIOCRIACAO, "
							+ "		EMFO_TX_FOLLOWUP "
							+ " )VALUES (?, ?, ?, ?, ?, ?)" + "");

					pst.setString(1, embarqueFoupBean.getEmfo_id_cd_embarque());
					pst.setString(2, embarqueFoupBean.getEmfo_id_cd_followup());
					pst.setString(3, embarqueFoupBean.getEmfo_id_cd_responsavel());
					pst.setString(4, embarqueFoupBean.getEmfo_id_cd_motivo());
					pst.setString(5, embarqueFoupBean.getEmfo_id_cd_usuariocriacao());
					pst.setString(6, embarqueFoupBean.getEmfo_tx_followup());

					pst.execute();
					//conn.close();
					pst.close();

					retBean.setSucesso(true);
					retBean.setProcesso("INSERT");
					retBean.setMsg("EMBARQUE FOUP INSERIDO COM SUCESSO! ");

				} catch (Exception e) {
					e.printStackTrace();
					retBean.setSucesso(false);
					retBean.setProcesso("INSERT");
					retBean.setMsg(e.getMessage());
					System.out.println("[createEmbarqueFoup] - ERRO AO INSERIR EMFO ");
				}
			}

		} else {
			retBean.setSucesso(false);
			retBean.setMsg("NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}

		return retBean;
	}

	/**
	 * Metodo responsavel buscar as informacoes de follow-up atraves do codigo do
	 * embarque
	 * 
	 * @author Caio Fernandes
	 * @param String
	 * @return RetornoBean retBean
	 */
	public List<AuxiliarCsCdtbFollowupFoupBean> getEmbarqueFoup(String idEmbarque) {

		ResultSet ret = null;
		Connection conn = DAO.getConexao();

		// LISTA DE FOUPS A SER RETORNADA
		List<AuxiliarCsCdtbFollowupFoupBean> lstFoups = new ArrayList<AuxiliarCsCdtbFollowupFoupBean>();

		if (conn != null) {

			try {

				// REALIZAR A BUSCA DE FOLLOWUPS DO EMBARQUE
				PreparedStatement pst;
				pst = conn.prepareStatement("" 
						+ "		SELECT 													   "
						+ "			EMFO_ID_CD_EMBAFOLLOW, "
						+ "			EMFO_ID_CD_EMBARQUE, 				   "
						+ "			EMFO_ID_CD_USUARIOCRIACAO, 	   "
						+ "			USER_DS_NOMEUSUARIO, 		   "
						+ "			EMFO_ID_CD_FOLLOWUP,		   "
						+ "			FOUP_DS_FOLLOWUP, 		   "
						+ "			EMFO_ID_CD_MOTIVO, 		   "
						+ "			MOTI_DS_MOTIVO, 	   "
						+ "			EMFO_ID_CD_RESPONSAVEL, 	   "
						+ "			RESP_NM_RESPONSAVEL, 			   "
						+ "			FORMAT (EMFO_DH_REGISTRO, 'dd/MM/yyyy HH:mm') as EMFO_DH_REGISTRO,	"
						+ "			EMFO_TX_FOLLOWUP " 											
						+ "		FROM 													   "
						+ "			CS_ASTB_EMBARQUEFOUP_EMFO EMFO 	(NOLOCK)					   "
						+ "			LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTI	(NOLOCK)				   "
						+ "				ON EMFO.EMFO_ID_CD_MOTIVO =  MOTI_ID_CD_MOTIVO	   "
						+ "			INNER JOIN CS_CDTB_FOLLOWUP_FOUP FOUP (NOLOCK)			   "
						+ "				ON EMFO_ID_CD_FOLLOWUP = FOUP_ID_CD_FOLLOWUP	   "
						+ "			INNER JOIN CS_CDTB_USUARIO_USER USUARIO	(NOLOCK)			   "
						+ "				ON EMFO_ID_CD_USUARIOCRIACAO = USER_ID_CD_USUARIO  "
						+ "			LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESP	(NOLOCK)		   "
						+ "				ON EMFO_ID_CD_RESPONSAVEL = RESP_ID_CD_RESPONSAVEL "
						+ "		WHERE 													   "
						+ "			EMFO_ID_CD_EMBARQUE = ?							   "
						+ "		ORDER BY 												   "
						+ "			EMFO_DH_REGISTRO DESC								   " + "");

				pst.setLong(1, Long.parseLong(idEmbarque));
				ret = pst.executeQuery();

				while (ret.next()) {

					// PREENCHER BEAN COM O RETORNO DA QUERY
					AuxiliarCsCdtbFollowupFoupBean foupBean = setListaFoupsAuxiliarBean(ret);

					// INSERIR NA LISTA DE FOUPS
					lstFoups.add(foupBean);

				}

			} catch (Exception e) {
 				e.printStackTrace();
				System.out.println("[getEmbarqueFoup] - ERRO AO CONSULTAR LISTA DE FOLLOWUPS");
			}

		} else {
			System.out.println("[getEmbarqueFoup] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}

		return lstFoups;
	}

	/**
	 * Metodo responsavel setar informacoes no bean de Follow Up
	 * 
	 * @author Caio Fernandes
	 * @param ResultSet
	 * @return CsCdtbFollowupFoupBean foupBean
	 */
	private CsCdtbFollowupFoupBean setListaFoupsBean(ResultSet ret) throws SQLException {

		// POPULAR LISTA COM O RETORNO DA QUERY
		CsCdtbFollowupFoupBean foupBean = new CsCdtbFollowupFoupBean();

		foupBean.setFoup_id_cd_followup(ret.getInt("FOUP_ID_CD_FOLLOWUP"));
		foupBean.setFoup_ds_followup(ret.getString("FOUP_DS_FOLLOWUP"));
		foupBean.setFoup_dh_registro(ret.getString("FOUP_DH_REGISTRO"));
		foupBean.setFoup_in_inativo(ret.getString("FOUP_IN_INATIVO"));
		foupBean.setFoup_id_cd_usuarioatualizacao(ret.getInt("FOUP_ID_CD_USUARIOATUALIZACAO"));
		foupBean.setFoup_dh_atualizacao(ret.getString("FOUP_DH_ATUALIZACAO"));
		foupBean.setFoup_id_cd_usuariocriacao(ret.getInt("FOUP_ID_CD_USUARIOCRIACAO"));

		return foupBean;

	}
	
	private AuxiliarCsCdtbFollowupFoupBean setListaFoupsAuxiliarBean(ResultSet ret) throws SQLException {

		// POPULAR LISTA COM O RETORNO DA QUERY
		AuxiliarCsCdtbFollowupFoupBean foupBean = new AuxiliarCsCdtbFollowupFoupBean();

		foupBean.setEmfo_id_cd_embafollow(ret.getString("emfo_id_cd_embafollow"));
		foupBean.setEmfo_id_cd_embarque(ret.getString("emfo_id_cd_embarque"));
		foupBean.setEmfo_id_cd_usuariocriacao(ret.getString("emfo_id_cd_usuariocriacao"));
		foupBean.setUser_ds_nomeusuario(ret.getString("user_ds_nomeusuario"));
		foupBean.setEmfo_id_cd_followup(ret.getString("emfo_id_cd_followup"));
		foupBean.setFoup_ds_followup(ret.getString("foup_ds_followup"));
		foupBean.setEmfo_id_cd_motivo(ret.getString("emfo_id_cd_motivo"));
		foupBean.setMoti_ds_motivo(ret.getString("moti_ds_motivo"));
		foupBean.setEmfo_id_cd_responsavel(ret.getString("emfo_id_cd_responsavel"));
		foupBean.setResp_nm_responsavel(ret.getString("resp_nm_responsavel"));
		foupBean.setEmfo_dh_registro(ret.getString("emfo_dh_registro"));
		foupBean.setEmfo_tx_followup(ret.getString("emfo_tx_followup"));
		
		
		return foupBean;

	}

	/**
	 * Metodo responsavel por verificar se existe registro na tabela EMFO
	 * 
	 * @author Caio Fernandes
	 * @param CsAstbEmbarqueFoupBean, Connection
	 * @return RetornoBean retBean
	 */
	public boolean existeEmfo(CsAstbEmbarqueFoupBean embarqueFoupBean, Connection conn) {

		ResultSet ret = null;
		boolean retorno = false;

		try {
			PreparedStatement pst;

			pst = conn.prepareStatement("" + "  SELECT " + "	 	EMFO_ID_CD_EMBAFOLLOW, "
					+ " 	EMFO_ID_CD_EMBARQUE, " + " 	EMFO_ID_CD_FOLLOWUP, " + " 	EMFO_DS_RESPONSAVEL, "
					+ " 	EMFO_DH_REGISTRO, " + " 	EMFO_ID_CD_USUARIOATUALIZACAO, " + " 	EMFO_DH_ATUALIZACAO "
					+ "  FROM  " + "  	CS_ASTB_EMBARQUEFOUP_EMFO (NOLOCK)  " + "  WHERE  "
					+ "  	EMFO_ID_CD_EMBAFOLLOW = ? " + " ");

			pst.setString(1, embarqueFoupBean.getEmfo_id_cd_embafollow());

			ret = pst.executeQuery();

			if (ret.next()) {
				retorno = true;
			} else {
				retorno = false;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return retorno;
	}

}
