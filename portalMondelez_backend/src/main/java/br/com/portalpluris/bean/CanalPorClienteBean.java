package br.com.portalpluris.bean;

public class CanalPorClienteBean {
	
	
	private String cncl_id_conalporcliente;
	private String cncl_cd_custumer;
	private String cncl_ds_chanel;
	private String cncl_cd_holding;
	private String cncl_nm_holding;
	private String cncl_ds_country;
	private String cncl_nm_razaosocial;
	private String cncl_ds_fantasia;
	private String cncl_ds_cidade;
	private String cncl_ds_cep;
	private String cncl_ds_estado;
	private String cncl_ds_rua;
	private String cncl_ds_telefone;
	private String cncl_ds_industria;
	private String cncl_cd_nielsen;
	private String cncl_ds_district;
	private String cncl_ds_cnpj;
	private String cncl_ds_transportzone;
	private String cncl_ds_inscricaoestadual;
	private String cncl_ds_subtrib;
	private String cncl_ds_taxajurd;
	private String cncl_ds_hiearchyassignment;
	private String cncl_cd_codigopai;
	private String cncl_ds_condpagamento;
	private String cncl_ds_aceitasaldo;
	private String cncl_ds_rebate;
	private String cncl_cd_salesdistrict;
	private String cncl_ds_ordercombinat;
	private String cncl_cd_deliveryprior;
	private String cncl_ds_paytterms;
	private String cncl_ds_pricedetermination;
	private String cncl_ds_typeclass;
	private String cncl_id_usuarioatualizacao;
	private String cncl_dh_importacao;
	private String cncl_dh_atualizacao;
	private String cncl_ds_nomearquivo;
	private String cncl_ds_inativo;
	private String cncl_ds_level1node;
	private String cncl_ds_level2node;
	private String cncl_ds_level3node;
	private String cncl_ds_level4node;
	private String cncl_ds_level5node;
	private String cncl_ds_level6node;
	private String cncl_ds_grupocanal;
	private String cncl_ds_segmento;
	private String cncl_ds_rede;
	private String cncl_ds_regiaovendas;
	private String cncl_ds_areavenda;
	private String cncl_ds_canalcustumer;
	private String cncl_ds_regiaocanal;
	
	
	
	
	public String getCncl_ds_level1node() {
		return cncl_ds_level1node;
	}
	public void setCncl_ds_level1node(String cncl_ds_level1node) {
		this.cncl_ds_level1node = cncl_ds_level1node;
	}
	public String getCncl_ds_level2node() {
		return cncl_ds_level2node;
	}
	public void setCncl_ds_level2node(String cncl_ds_level2node) {
		this.cncl_ds_level2node = cncl_ds_level2node;
	}
	public String getCncl_ds_level3node() {
		return cncl_ds_level3node;
	}
	public void setCncl_ds_level3node(String cncl_ds_level3node) {
		this.cncl_ds_level3node = cncl_ds_level3node;
	}
	public String getCncl_ds_level4node() {
		return cncl_ds_level4node;
	}
	public void setCncl_ds_level4node(String cncl_ds_level4node) {
		this.cncl_ds_level4node = cncl_ds_level4node;
	}
	public String getCncl_ds_level5node() {
		return cncl_ds_level5node;
	}
	public void setCncl_ds_level5node(String cncl_ds_level5node) {
		this.cncl_ds_level5node = cncl_ds_level5node;
	}
	public String getCncl_ds_level6node() {
		return cncl_ds_level6node;
	}
	public void setCncl_ds_level6node(String cncl_ds_level6node) {
		this.cncl_ds_level6node = cncl_ds_level6node;
	}
	public String getCncl_ds_regiaocanal() {
		return cncl_ds_regiaocanal;
	}
	public void setCncl_ds_regiaocanal(String cncl_ds_regiaocanal) {
		this.cncl_ds_regiaocanal = cncl_ds_regiaocanal;
	}
	public String getCncl_ds_canalcustumer() {
		return cncl_ds_canalcustumer;
	}
	public void setCncl_ds_canalcustumer(String cncl_ds_canalcustumer) {
		this.cncl_ds_canalcustumer = cncl_ds_canalcustumer;
	}
	public String getCncl_ds_areavenda() {
		return cncl_ds_areavenda;
	}
	public void setCncl_ds_areavenda(String cncl_ds_areavenda) {
		this.cncl_ds_areavenda = cncl_ds_areavenda;
	}
	public String getCncl_ds_regiaovendas() {
		return cncl_ds_regiaovendas;
	}
	public void setCncl_ds_regiaovendas(String cncl_ds_regiaovendas) {
		this.cncl_ds_regiaovendas = cncl_ds_regiaovendas;
	}

	
	public String getCncl_ds_rede() {
		return cncl_ds_rede;
	}
	public void setCncl_ds_rede(String cncl_ds_rede) {
		this.cncl_ds_rede = cncl_ds_rede;
	}
	public String getCncl_ds_segmento() {
		return cncl_ds_segmento;
	}
	public void setCncl_ds_segmento(String cncl_ds_segmento) {
		this.cncl_ds_segmento = cncl_ds_segmento;
	}
	public String getCncl_ds_grupocanal() {
		return cncl_ds_grupocanal;
	}
	public void setCncl_ds_grupocanal(String cncl_ds_grupocanal) {
		this.cncl_ds_grupocanal = cncl_ds_grupocanal;
	}

	public String getCncl_id_conalporcliente() {
		return cncl_id_conalporcliente;
	}
	public void setCncl_id_conalporcliente(String cncl_id_conalporcliente) {
		this.cncl_id_conalporcliente = cncl_id_conalporcliente;
	}
	public String getCncl_cd_custumer() {
		return cncl_cd_custumer;
	}
	public void setCncl_cd_custumer(String cncl_cd_custumer) {
		this.cncl_cd_custumer = cncl_cd_custumer;
	}
	public String getCncl_ds_chanel() {
		return cncl_ds_chanel;
	}
	public void setCncl_ds_chanel(String cncl_ds_chanel) {
		this.cncl_ds_chanel = cncl_ds_chanel;
	}
	public String getCncl_cd_holding() {
		return cncl_cd_holding;
	}
	public void setCncl_cd_holding(String cncl_cd_holding) {
		this.cncl_cd_holding = cncl_cd_holding;
	}
	public String getCncl_nm_holding() {
		return cncl_nm_holding;
	}
	public void setCncl_nm_holding(String cncl_nm_holding) {
		this.cncl_nm_holding = cncl_nm_holding;
	}
	public String getCncl_ds_country() {
		return cncl_ds_country;
	}
	public void setCncl_ds_country(String cncl_ds_country) {
		this.cncl_ds_country = cncl_ds_country;
	}
	public String getCncl_nm_razaosocial() {
		return cncl_nm_razaosocial;
	}
	public void setCncl_nm_razaosocial(String cncl_nm_razaosocial) {
		this.cncl_nm_razaosocial = cncl_nm_razaosocial;
	}
	public String getCncl_ds_fantasia() {
		return cncl_ds_fantasia;
	}
	public void setCncl_ds_fantasia(String cncl_ds_fantasia) {
		this.cncl_ds_fantasia = cncl_ds_fantasia;
	}
	public String getCncl_ds_cidade() {
		return cncl_ds_cidade;
	}
	public void setCncl_ds_cidade(String cncl_ds_cidade) {
		this.cncl_ds_cidade = cncl_ds_cidade;
	}
	public String getCncl_ds_cep() {
		return cncl_ds_cep;
	}
	public void setCncl_ds_cep(String cncl_ds_cep) {
		this.cncl_ds_cep = cncl_ds_cep;
	}
	public String getCncl_ds_estado() {
		return cncl_ds_estado;
	}
	public void setCncl_ds_estado(String cncl_ds_estado) {
		this.cncl_ds_estado = cncl_ds_estado;
	}
	public String getCncl_ds_rua() {
		return cncl_ds_rua;
	}
	public void setCncl_ds_rua(String cncl_ds_rua) {
		this.cncl_ds_rua = cncl_ds_rua;
	}
	public String getCncl_ds_telefone() {
		return cncl_ds_telefone;
	}
	public void setCncl_ds_telefone(String cncl_ds_telefone) {
		this.cncl_ds_telefone = cncl_ds_telefone;
	}
	public String getCncl_ds_industria() {
		return cncl_ds_industria;
	}
	public void setCncl_ds_industria(String cncl_ds_industria) {
		this.cncl_ds_industria = cncl_ds_industria;
	}
	public String getCncl_cd_nielsen() {
		return cncl_cd_nielsen;
	}
	public void setCncl_cd_nielsen(String cncl_cd_nielsen) {
		this.cncl_cd_nielsen = cncl_cd_nielsen;
	}
	public String getCncl_ds_district() {
		return cncl_ds_district;
	}
	public void setCncl_ds_district(String cncl_ds_district) {
		this.cncl_ds_district = cncl_ds_district;
	}
	public String getCncl_ds_cnpj() {
		return cncl_ds_cnpj;
	}
	public void setCncl_ds_cnpj(String cncl_ds_cnpj) {
		this.cncl_ds_cnpj = cncl_ds_cnpj;
	}
	public String getCncl_ds_transportzone() {
		return cncl_ds_transportzone;
	}
	public void setCncl_ds_transportzone(String cncl_ds_transportzone) {
		this.cncl_ds_transportzone = cncl_ds_transportzone;
	}
	public String getCncl_ds_inscricaoestadual() {
		return cncl_ds_inscricaoestadual;
	}
	public void setCncl_ds_inscricaoestadual(String cncl_ds_inscricaoestadual) {
		this.cncl_ds_inscricaoestadual = cncl_ds_inscricaoestadual;
	}
	public String getCncl_ds_subtrib() {
		return cncl_ds_subtrib;
	}
	public void setCncl_ds_subtrib(String cncl_ds_subtrib) {
		this.cncl_ds_subtrib = cncl_ds_subtrib;
	}
	public String getCncl_ds_taxajurd() {
		return cncl_ds_taxajurd;
	}
	public void setCncl_ds_taxajurd(String cncl_ds_taxajurd) {
		this.cncl_ds_taxajurd = cncl_ds_taxajurd;
	}
	public String getCncl_ds_hiearchyassignment() {
		return cncl_ds_hiearchyassignment;
	}
	public void setCncl_ds_hiearchyassignment(String cncl_ds_hiearchyassignment) {
		this.cncl_ds_hiearchyassignment = cncl_ds_hiearchyassignment;
	}
	public String getCncl_cd_codigopai() {
		return cncl_cd_codigopai;
	}
	public void setCncl_cd_codigopai(String cncl_cd_codigopai) {
		this.cncl_cd_codigopai = cncl_cd_codigopai;
	}
	public String getCncl_ds_condpagamento() {
		return cncl_ds_condpagamento;
	}
	public void setCncl_ds_condpagamento(String cncl_ds_condpagamento) {
		this.cncl_ds_condpagamento = cncl_ds_condpagamento;
	}
	public String getCncl_ds_aceitasaldo() {
		return cncl_ds_aceitasaldo;
	}
	public void setCncl_ds_aceitasaldo(String cncl_ds_aceitasaldo) {
		this.cncl_ds_aceitasaldo = cncl_ds_aceitasaldo;
	}
	public String getCncl_ds_rebate() {
		return cncl_ds_rebate;
	}
	public void setCncl_ds_rebate(String cncl_ds_rebate) {
		this.cncl_ds_rebate = cncl_ds_rebate;
	}
	public String getCncl_cd_salesdistrict() {
		return cncl_cd_salesdistrict;
	}
	public void setCncl_cd_salesdistrict(String cncl_cd_salesdistrict) {
		this.cncl_cd_salesdistrict = cncl_cd_salesdistrict;
	}
	public String getCncl_ds_ordercombinat() {
		return cncl_ds_ordercombinat;
	}
	public void setCncl_ds_ordercombinat(String cncl_ds_ordercombinat) {
		this.cncl_ds_ordercombinat = cncl_ds_ordercombinat;
	}
	public String getCncl_cd_deliveryprior() {
		return cncl_cd_deliveryprior;
	}
	public void setCncl_cd_deliveryprior(String cncl_cd_deliveryprior) {
		this.cncl_cd_deliveryprior = cncl_cd_deliveryprior;
	}
	public String getCncl_ds_paytterms() {
		return cncl_ds_paytterms;
	}
	public void setCncl_ds_paytterms(String cncl_ds_paytterms) {
		this.cncl_ds_paytterms = cncl_ds_paytterms;
	}
	public String getCncl_ds_pricedetermination() {
		return cncl_ds_pricedetermination;
	}
	public void setCncl_ds_pricedetermination(String cncl_ds_pricedetermination) {
		this.cncl_ds_pricedetermination = cncl_ds_pricedetermination;
	}
	public String getCncl_ds_typeclass() {
		return cncl_ds_typeclass;
	}
	public void setCncl_ds_typeclass(String cncl_ds_typeclass) {
		this.cncl_ds_typeclass = cncl_ds_typeclass;
	}
	public String getCncl_id_usuarioatualizacao() {
		return cncl_id_usuarioatualizacao;
	}
	public void setCncl_id_usuarioatualizacao(String cncl_id_usuarioatualizacao) {
		this.cncl_id_usuarioatualizacao = cncl_id_usuarioatualizacao;
	}
	public String getCncl_dh_importacao() {
		return cncl_dh_importacao;
	}
	public void setCncl_dh_importacao(String cncl_dh_importacao) {
		this.cncl_dh_importacao = cncl_dh_importacao;
	}
	public String getCncl_dh_atualizacao() {
		return cncl_dh_atualizacao;
	}
	public void setCncl_dh_atualizacao(String cncl_dh_atualizacao) {
		this.cncl_dh_atualizacao = cncl_dh_atualizacao;
	}
	public String getCncl_ds_nomearquivo() {
		return cncl_ds_nomearquivo;
	}
	public void setCncl_ds_nomearquivo(String cncl_ds_nomearquivo) {
		this.cncl_ds_nomearquivo = cncl_ds_nomearquivo;
	}
	public String getCncl_ds_inativo() {
		return cncl_ds_inativo;
	}
	public void setCncl_ds_inativo(String cncl_ds_inativo) {
		this.cncl_ds_inativo = cncl_ds_inativo;
	}
	
	

}
