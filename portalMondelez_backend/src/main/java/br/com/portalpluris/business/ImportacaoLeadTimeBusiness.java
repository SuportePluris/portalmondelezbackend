package br.com.portalpluris.business;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilParseToInt;
import br.com.portalpluris.bean.ArquivoLeadTimeBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.dao.CsCdtbLeadTimeLdtmDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.DAOLog;

public class ImportacaoLeadTimeBusiness {

	public void importadorArquivoLeadTime() {

		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("*** INICIO DO SERVIÇO DE IMPORTAÇÃO DE ARQUIVO LEAD TIME ******");

		// DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoLeadTimeBusiness");
		logsBean.setLogsDsMetodo("importadorArquivoLeadTime");
		logsBean.setLogsInErro("N");
		
		UtilMoveArquivo utilMove = new UtilMoveArquivo();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		String diretorio = confDao.slctConf("conf.importacao.leadtime");
		String diretorioBKP = confDao.slctConf("conf.importacao.leadtime.bkp");
		String diretorioErro = confDao.slctConf("conf.importacao.leadtime.bkp");

		File arquivoProcessado = null;
		File arquivoProcessadobkp = null;
		File arquivoProcessadoerro = new File(diretorioErro+ "/");
		
//		String diretorio = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond";
//		String diretorioBKP = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond\\bkp";	

		ArquivoLeadTimeBean leadTime = new ArquivoLeadTimeBean();
		UtilParseToInt parseInt = new UtilParseToInt();

		int linha = 0;

		try {

			File arquivo = new File(diretorio);
			String[] arq = arquivo.list();

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String dhImportacao = sdf.format(date);
			
			if(arq != null){

			for (int i = 0; i < arq.length; i++) {

				 arquivoProcessado = new File(diretorio + "\\" + arq[i]);
				 arquivoProcessadobkp = new File(diretorioBKP + "\\" + arq[i]);

				CsCdtbLeadTimeLdtmDao leadDAO = new CsCdtbLeadTimeLdtmDao();

				if (arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

					log.info("*** INICIO DA LEITURA DO ARQUIVO " + arquivoProcessado.getName() + "******");
					FileInputStream file = new FileInputStream(arquivoProcessado);

					Workbook wb = WorkbookFactory.create(file);
					Sheet sheet = wb.getSheetAt(0);

						for (int j = 1; j < sheet.getLastRowNum(); j++) {

							linha = j;

							leadTime = new ArquivoLeadTimeBean();

							leadTime.setPlantaCidade(sheet.getRow(j).getCell(0).toString());

							// SE A CELULA FOR DIFERENTE DE VAZIO FAZ O PROCESSO
							if (!sheet.getRow(j).getCell(0).toString().isEmpty()) {

								Cell source = sheet.getRow(j).getCell(1);
								source.setCellType(CellType.STRING);
								source.toString();
								int sourceLocation = parseInt.toInt(source.toString());

								leadTime.setSourceLocationID(sourceLocation);
								leadTime.setSourceLocationName(sheet.getRow(j).getCell(2).toString());
								leadTime.setSourceProvince(sheet.getRow(j).getCell(3).toString());
								leadTime.setDestinationCity(sheet.getRow(j).getCell(4).toString());
								leadTime.setCodProvinceDestination(sheet.getRow(j).getCell(5).toString());
								leadTime.setNewZone(sheet.getRow(j).getCell(6).toString());

								Cell ltlcel = sheet.getRow(j).getCell(7);
								ltlcel.setCellType(CellType.STRING);
								ltlcel.toString();
								int ltl = parseInt.toInt(ltlcel.toString());								
								leadTime.setLtl(ltl);
								
								Cell ltcel = sheet.getRow(j).getCell(8);
								ltcel.setCellType(CellType.STRING);
								ltcel.toString();
								int tl = parseInt.toInt(ltcel.toString());
								leadTime.setTl(tl);


								log.info("*** INICIO DA VALIDAÇÃO SE É UM UPDATE OU INSERT ******");

								boolean validaUpdateInsert = leadDAO
										.slcNomePlanta(sheet.getRow(j).getCell(0).toString());

								if (validaUpdateInsert) {
									log.info("INSERT");
									leadDAO.insertLeadTime(leadTime, dhImportacao, sourceLocation, ltl, tl);

								} else {
									log.info("UPDATE");
									leadDAO.updateLeadTime(leadTime, dhImportacao, sourceLocation, ltl, tl);
								}

							}
						}

					boolean movido = utilMove.moveArquivo(arquivoProcessado,arquivoProcessadobkp);
					
					if(movido){
						log.info("Arquivo movido com sucesso");
					}else{
						log.info("Não foi possivel mover o arquivo");
					}
				}
			}
		}

		} catch (Exception e) {
			log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO LEAD TIME" + e + " ******");		
			logsBean.setLogsInErro("S");
			log.info("*** LINHA:" + linha + "************");
			logsBean.setLogsTxErro( "LINHA DE ERRO:" +linha +e.getMessage());
			utilMove.moveArquivo(arquivoProcessado,arquivoProcessadoerro);

		} finally {
			daoLog.createLog(logsBean);
			log.info("**** FIM SERVIÇO DE IMPORTAÇÃO DE ARQUIVO LEAD TIME ****");
		}

	}
}
