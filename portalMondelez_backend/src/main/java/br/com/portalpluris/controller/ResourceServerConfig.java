package br.com.portalpluris.controller;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter{

	//CLASSE PAA CONFIGURAR QUAIS APIS ESTARAM DENTRO DAS CONFIGURAÇÕES DE SEGURANÇA
		
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				
				//AUTENTICAR
				
				//.antMatchers("/portalpluris/**").authenticated()
		
				//NÃO AUTENTICAR (PERMITIR A TODOS)
				//.antMatchers("/portalpluris/validacarga").permitAll()
				.antMatchers("/portalpluris/getstatusapp").permitAll()
				.antMatchers("/oauth/token").permitAll();					//QUALQUER UM ACESSA SEM SENHA
				
				
				//.antMatchers("/portalpluris/**").authenticated()
				//NEGAR QUALQUER OUTRA CONTROLLER PARA LEMBRAR QUE É NECESSÁRIO CRIAR UMA REGRA ACIMA PARA ELA
				//.anyRequest().denyAll();
			
		
			//QUALQUER UM ACESSA SEM SENHA
				//.antMatchers("/portalndgoperador/**", "/portalnd/**").authenticated()
				//.antMatchers("/portalndgoperador/**",
				//		 "/portalnd/**"	).authenticated() 				//ACESSO APENAS AUTENTICADO
				
				//.antMatchers("/portalndg/**").permitAll() 			//QUALQUER UM ACESSA SEM SENHA
				//.antMatchers("/portalndgoperador/**").permitAll() 	//QUALQUER UM ACESSA SEM SENHA
				
				//.antMatchers("/portalndg/**").hasRole("USER")  				//ADICIONAR A REGRA PARA USER
				//.antMatchers("/portalndg/**").hasRole("ADMIN") 				//ADICIONAR A REGRA PARA ADMIN
				//.antMatchers("/portalndg/**").hasAnyRole("USER", "ADMIN");    //ADICIONAR VAR ARGS... NA REGRA 
				
				//.anyRequest().denyAll();										//NEGAR QUALQUER OUTRA SOLICITAÇÃO ***PARA CASO CRIAR ALGUMA API LEMBRAR DE INCLUIR A REGRA AQUI
			
	}
	
}
