package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.CsCdtbStatusStatBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.StatusQuantidadeBean;

public class CsCdtbStatusStatDao {

	public RetornoBean createUpdateStat(CsCdtbStatusStatBean statBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		if (statBean.getStat_id_cd_status() > 0) {
			retBean = updateStat(statBean);

			return retBean;
		}

		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		
		if(statBean.getStatus_in_inativo().equalsIgnoreCase("S")) {
			pst = conn.prepareStatement("" 	+ " INSERT INTO CS_CDTB_STATUSAGENDAMENTO_STAT ( " 
											+ "		STAT_DS_STATUS, "
											+ "		STAT_ID_CD_USUARIOATUALIZACAO, "
											+ "		STAT_IN_INATIVO "
											+ " ) VALUES ( ?, ?, ? ) ");

			pst.setString(1, statBean.getStatus_ds_descricaostatus());
			pst.setLong(2, statBean.getStatus_id_cd_usuarioatualizacao());
			pst.setString(3, statBean.getStatus_in_inativo());
			
		}else {
			pst = conn.prepareStatement("" + " INSERT INTO CS_CDTB_STATUSAGENDAMENTO_STAT ( " + " 		STAT_DS_STATUS, "
					+ " 		STAT_ID_CD_USUARIOATUALIZACAO " + " ) VALUES ( " + " 		?, ? " + ") " + "");

			pst.setString(1, statBean.getStatus_ds_descricaostatus());
			pst.setLong(2, statBean.getStatus_id_cd_usuarioatualizacao());
		}


		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Status criado com sucesso");

		return retBean;
	}

	private RetornoBean updateStat(CsCdtbStatusStatBean statBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		String queryUpdate = " UPDATE CS_CDTB_STATUSAGENDAMENTO_STAT SET " + " STAT_ID_CD_USUARIOATUALIZACAO = "
				+ statBean.getStatus_id_cd_usuarioatualizacao() + ", " + " STAT_DH_ATUALIZACAO = GETDATE()";

		if (statBean.getStatus_ds_descricaostatus() != null && statBean.getStatus_ds_descricaostatus() != "") {
			queryUpdate += ", STAT_DS_STATUS = '" + statBean.getStatus_ds_descricaostatus() + "'";
		}

		if (statBean.getStatus_in_inativo() != null && statBean.getStatus_in_inativo() != "") {
			queryUpdate += ", STAT_IN_INATIVO = '" + statBean.getStatus_in_inativo() + "'";
		}

		queryUpdate += " WHERE STAT_ID_CD_STATUS = " + statBean.getStat_id_cd_status();

		pst = conn.prepareStatement(queryUpdate);
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Status atualizado com sucesso");

		return retBean;
	}

	public List<CsCdtbStatusStatBean> getListStat(String statInAtivo) throws SQLException {

		List<CsCdtbStatusStatBean> listStatBean = new ArrayList<CsCdtbStatusStatBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " + "		STAT_ID_CD_STATUS, " + "		STAT_DS_STATUS, "
				+ "		STAT_DH_REGISTRO, " + "		STAT_IN_INATIVO, " + "		STAT_ID_CD_USUARIOATUALIZACAO, "
				+ "		STAT_DH_ATUALIZACAO " + " FROM " + "		CS_CDTB_STATUSAGENDAMENTO_STAT (NOLOCK) " + "";

		//COMENTADO PARA TRAZER TODOS OS STATUS E NAO SOMENTE OS INATIVOS
		//if (statInAtivo.equalsIgnoreCase("S") || statInAtivo.equalsIgnoreCase("N")) {
		//	query += " WHERE STAT_IN_INATIVO = '" + statInAtivo + "'";
		//}

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbStatusStatBean statBean = setStatBean(rs);

			listStatBean.add(statBean);
		}

		return listStatBean;
	}
	
	public List<StatusQuantidadeBean> getListStatQtd(String statInAtivo, String idFuncionario) throws SQLException {

		List<StatusQuantidadeBean> listStatBean = new ArrayList<StatusQuantidadeBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String cSQL = "";
		cSQL += "  ";
		cSQL += "    ";
		cSQL += "   SELECT  ";
		cSQL += "   STAT.STAT_ID_CD_STATUS,  ";
		cSQL += "   STAT.STAT_DS_STATUS,  ";
		cSQL += "   ISNULL(EMBA.QTD,0) AS QTD  ";
		cSQL += "   FROM   ";
		cSQL += "   (  ";
		cSQL += "   SELECT DISTINCT  ";
		cSQL += "   STAT_ID_CD_STATUS,    ";
		cSQL += "   STAT_DS_STATUS  ";
		cSQL += "   FROM CS_CDTB_STATUSAGENDAMENTO_STAT (NOLOCK) ";
		cSQL += "   WHERE STAT_IN_INATIVO = '" + statInAtivo + "') AS STAT  ";
		cSQL += "   LEFT JOIN  ";
		cSQL += "   (  ";
		cSQL += "  SELECT  ";
		cSQL += "  COUNT(DISTINCT EMBA_ID_CD_EMBARQUE) AS QTD,  ";
		cSQL += "  EMBA_ID_CD_STATUS  ";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) ";
		cSQL += " 	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK) ";
		cSQL += " 		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT ";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS ";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE ";

		cSQL += "  WHERE CLIE.CLIE_IN_INATIVO = 'N' ";
		
		//SE NÃO POSSUIR REGIÃO E CANAL, VÊ TUDO
		csCdtbUsuarioUserDao userDao = new csCdtbUsuarioUserDao();
		ResultSet result = userDao.getRegiaoCanalByUser(idFuncionario);
		
		if(result.next()) {
			
			cSQL += " AND CLIE.CLIE_ID_CD_CODIGOCLIENTE IN (";
			
				cSQL += " SELECT ";
				cSQL += " 	DISTINCT CNCL_CD_CUSTUMER ";
				cSQL += " FROM ";
				cSQL += " 	CS_CDTB_CANALPORCLIENTE_CNCL CNCL (NOLOCK) ";
				cSQL += " 	INNER JOIN CS_CDTB_REGIAO_REGI REGI (NOLOCK) ";
				cSQL += " 		ON UPPER(LTRIM(RTRIM(CNCL.CNCL_DS_REGIAOCANAL))) = UPPER(LTRIM(RTRIM(REGI.REGI_DS_REGIAO))) ";
				cSQL += " 	INNER JOIN CS_CDTB_CANAL_CANA CANA (NOLOCK) ";
				cSQL += " 		ON UPPER(LTRIM(RTRIM(CNCL.CNCL_DS_CHANEL))) = UPPER(LTRIM(RTRIM(CANA.CANA_DS_CANAL))) ";
				cSQL += " 	INNER JOIN CS_ASTB_USERREGIAOCANAL_URCS URCS (NOLOCK) ";
				cSQL += " 		ON REGI.ID_REGI_CD_REGIAO = URCS.URCS_ID_CD_REGIAO ";
				cSQL += " 		AND CANA.CANA_ID_CD_CANAL = URCS.URCS_ID_CD_CANAL ";
				cSQL += " WHERE ";
				cSQL += " 	URCS.URCS_ID_CD_USUARIO = '" + idFuncionario + "' ";
			 
			cSQL += ")";
		}
		
		cSQL += "  GROUP BY EMBA_ID_CD_STATUS  ";
		cSQL += "   ) AS EMBA  ";
		cSQL += "   ON EMBA.EMBA_ID_CD_STATUS = STAT.STAT_ID_CD_STATUS  ";

		//COMENTADO PARA TRAZER TODOS OS STATUS E NAO SOMENTE OS INATIVOS
		//if (statInAtivo.equalsIgnoreCase("S") || statInAtivo.equalsIgnoreCase("N")) {
		//	query += " WHERE STAT_IN_INATIVO = '" + statInAtivo + "'";
		//}

		pst = conn.prepareStatement(cSQL);		
		rs = pst.executeQuery();

		while (rs.next()) {

			StatusQuantidadeBean statBean = setStatQtdBean(rs);

			listStatBean.add(statBean);
		}

		return listStatBean;
	}

	public CsCdtbStatusStatBean getStat(int statIdCdStatus) throws SQLException {

		CsCdtbStatusStatBean statBean = new CsCdtbStatusStatBean();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " + "		STAT_ID_CD_STATUS, " + "		STAT_DS_STATUS, "
				+ "		STAT_DH_REGISTRO, " + "		STAT_IN_INATIVO, " + "		STAT_ID_CD_USUARIOATUALIZACAO, "
				+ "		STAT_DH_ATUALIZACAO " + " FROM " + "		CS_CDTB_STATUSAGENDAMENTO_STAT (NOLOCK) "
				+ " WHERE " + "		STAT_ID_CD_STATUS = ?";

		pst = conn.prepareStatement(query);
		pst.setInt(1, statIdCdStatus);

		rs = pst.executeQuery();

		while (rs.next()) {

			statBean = setStatBean(rs);
		}

		return statBean;
	}

	private CsCdtbStatusStatBean setStatBean(ResultSet rs) throws SQLException {

		CsCdtbStatusStatBean statBean = new CsCdtbStatusStatBean();

		statBean.setStat_id_cd_status(rs.getInt("STAT_ID_CD_STATUS"));
		statBean.setStatus_ds_descricaostatus(rs.getString("STAT_DS_STATUS"));
		statBean.setStatus_dh_registro(rs.getTimestamp("STAT_DH_REGISTRO"));
		statBean.setStatus_in_inativo(rs.getString("STAT_IN_INATIVO"));
		statBean.setStatus_id_cd_usuarioatualizacao(rs.getInt("STAT_ID_CD_USUARIOATUALIZACAO"));
		statBean.setStatus_dh_atualizacao(rs.getTimestamp("STAT_DH_ATUALIZACAO"));

		return statBean;
	}
	
	private StatusQuantidadeBean setStatQtdBean(ResultSet rs) throws SQLException {

		StatusQuantidadeBean statBean = new StatusQuantidadeBean();

		statBean.setStat_id_cd_status(rs.getLong("STAT_ID_CD_STATUS"));
		statBean.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
		statBean.setQtd(rs.getInt("QTD"));

		return statBean;
	}

}
