package br.com.portalpluris.bean;

import java.sql.Timestamp;

public class EsLgtbLogsLogsBean {

	String logsDsClasse;
	String logsDsMetodo;
	String logsDsChave;
	String logsTxInfoentrada;
	String logsTxInfosaida;
	String logsTxInfoadicionais;
	Timestamp logsDhInicio;
	Timestamp logsDhFim;
	String logsInErro;
	String logsTxErro;

	public String getLogsInErro() {
		return logsInErro;
	}

	public void setLogsInErro(String logsInErro) {
		this.logsInErro = logsInErro;
	}

	public String getLogsTxErro() {
		return logsTxErro;
	}

	public void setLogsTxErro(String logsTxErro) {
		this.logsTxErro = logsTxErro;
	}

	public String getLogsDsClasse() {
		return logsDsClasse;
	}

	public void setLogsDsClasse(String logsDsClasse) {
		this.logsDsClasse = logsDsClasse;
	}

	public String getLogsDsMetodo() {
		return logsDsMetodo;
	}

	public void setLogsDsMetodo(String logsDsMetodo) {
		this.logsDsMetodo = logsDsMetodo;
	}

	public String getLogsDsChave() {
		return logsDsChave;
	}

	public void setLogsDsChave(String logsDsChave) {
		this.logsDsChave = logsDsChave;
	}

	public String getLogsTxInfoentrada() {
		return logsTxInfoentrada;
	}

	public void setLogsTxInfoentrada(String logsTxInfoentrada) {
		this.logsTxInfoentrada = logsTxInfoentrada;
	}

	public String getLogsTxInfosaida() {
		return logsTxInfosaida;
	}

	public void setLogsTxInfosaida(String logsTxInfosaida) {
		this.logsTxInfosaida = logsTxInfosaida;
	}

	public String getLogsTxInfoadicionais() {
		return logsTxInfoadicionais;
	}

	public void setLogsTxInfoadicionais(String logsTxInfoadicionais) {
		this.logsTxInfoadicionais = logsTxInfoadicionais;
	}

	public Timestamp getLogsDhInicio() {
		return logsDhInicio;
	}

	public void setLogsDhInicio(Timestamp logsDhInicio) {
		this.logsDhInicio = logsDhInicio;
	}

	public Timestamp getLogsDhFim() {
		return logsDhFim;
	}

	public void setLogsDhFim(Timestamp logsDhFim) {
		this.logsDhFim = logsDhFim;
	}

}
