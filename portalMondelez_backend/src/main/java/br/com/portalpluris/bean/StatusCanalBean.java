package br.com.portalpluris.bean;

public class StatusCanalBean {

	private String emba_id_cd_status;
	private String idFuncionario;

	public String getEmba_id_cd_status() {
		return emba_id_cd_status;
	}
	public void setEmba_id_cd_status(String emba_id_cd_status) {
		this.emba_id_cd_status = emba_id_cd_status;
	}
	public String getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(String idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	
}
