package br.com.portalpluris.controller;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import br.com.portalpluris.bean.CsCdtbUsuarioUserBean;
import br.com.portalpluris.dao.csCdtbUsuarioUserDao;

/**CLASSE DE CONSULTA USUÁRIOS VALIDOS PARA ACESSO AOS SERVIÇOS**/
@Service	
public class UsuarioService implements UserDetailsService{

	@Override
	public UserDetails loadUserByUsername(String username){
		
		csCdtbUsuarioUserDao funcDao = new csCdtbUsuarioUserDao();
		CsCdtbUsuarioUserBean usuarioBean = funcDao.selectUserByUserName(username);
		
		System.out.println("[LOGIN USER] trying load login user : " + username);
		
		UserDetails u = User
				.builder()
				.username(usuarioBean.getUser_ds_login())
				.password(usuarioBean.getUser_ds_senha())
				.roles("TOTAL_ACESS")
				.build();
		
		return u;
		
//		return User
//				.builder()
//				.username(usuarioBean.getFuncDsLoginname())
//				.password(usuarioBean.getFuncDsPassword())
//				.roles(usuarioBean.getFuncDsPermissao())
//				.build();
		
	}
	
}
