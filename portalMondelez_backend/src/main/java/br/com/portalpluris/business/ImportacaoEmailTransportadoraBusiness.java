package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.google.common.io.Files;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.portalpluris.bean.CsCdtbTransportadoraEmail;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.dao.CsCdtbTransportadoraEmailDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.DAOLog;

public class ImportacaoEmailTransportadoraBusiness {
	
	
	public void importadorEmailTransportadora(){		
		
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		Logger log = Logger.getLogger(this.getClass().getName());

		log.info("*** INICIO DO SERVIÇO DE IMPORTAÇÃO EMAIL TRANSPORTADORA ******");

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoEmailTransportadoraBusiness");
		logsBean.setLogsDsMetodo("importadorEmailTransportadora");
		logsBean.setLogsInErro("N");

		CsCdtbTransportadoraEmail transMail = new CsCdtbTransportadoraEmail();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();		
//
		String diretorio = confDao.slctConf("conf.importacao.email.transportadora");
		String diretorioBKP = confDao.slctConf("conf.importacao.email.transportadora.bkp");
		String diretorioErro = confDao.slctConf("conf.importacao.email.transportadora.erro");
		
		File arquivoProcessado = null;
		File arquivoProcessadobkp = null;
		File arquivoProcessadoerro = new File(diretorioErro+ "/");
		
		int linha = 0;
		
//		String diretorio = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond";
//		String diretorioBKP = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond\\bkp";	
		
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dhImportacao = sdf.format(date);
		UtilMoveArquivo utilMove = new UtilMoveArquivo();
		
		try {

			File arquivo = new File(diretorio);
			String[] arq = arquivo.list();
			
			if(arq != null){
				
			
			for (int i = 0; i < arq.length; i++) {

				 arquivoProcessado = new File(diretorio + "/" + arq[i]);
				 arquivoProcessadobkp = new File(diretorioBKP + "/" + arq[i]);
				
				String nmArquivo = arq[i] + "_" + dhImportacao;

				if (arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")
						&& arquivoProcessado.getName().toUpperCase().startsWith("BASE E-MAIL")) {
	
					FileInputStream file = new FileInputStream(arquivoProcessado);
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file));					

					Workbook wb = WorkbookFactory.create(file);
					Sheet sheet = wb.getSheetAt(0);
					
					log.info("*** INICIO DA LEITURA DO ARQUIVO " + arquivoProcessado.getName() + "******");								
					
						transMail = setTransportadoraEmail(sheet,dhImportacao,nmArquivo);
					
						boolean movido = utilMove.moveArquivo(arquivoProcessado,arquivoProcessadobkp);
						
						if(movido){
							log.info("Arquivo movido com sucesso");
						}else{
							log.info("Não foi possivel mover o arquivo");
						}
				}
				
			}
		}
		
		} catch (Exception e) {
			log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO DE  EMAIL TRANSPORTADORA" + e + " ******");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
			utilMove.moveArquivo(arquivoProcessado,arquivoProcessadoerro);

		} finally {

			daoLog.createLog(logsBean);
			log.info("**** FIM SERVIÇO DE IMPORTAÇÃO DE EMAIL TRANSPORTADORA ****");
		}
		
	}

	private CsCdtbTransportadoraEmail setTransportadoraEmail(Sheet sheet, String dhImportacao, String nmArquivo) {		
		
		Logger log = Logger.getLogger(this.getClass().getName());		
		
		CsCdtbTransportadoraEmail transMail = new CsCdtbTransportadoraEmail();
		CsCdtbTransportadoraEmail retTransMail = new CsCdtbTransportadoraEmail();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsCdtbTransportadoraEmailDao transMailDao = new CsCdtbTransportadoraEmailDao();
		
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoEmailTransportadoraBusiness");
		logsBean.setLogsDsMetodo("setTransportadoraEmail");
		logsBean.setLogsInErro("N");
		
		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");
		
		try {		
			
			for (int j = 1; j < sheet.getLastRowNum(); j++) {
				
				Cell emails = sheet.getRow(j).getCell(14);
				
				if(emails == null){
					
					continue;
					
				}else{
					
					String[] lstEmail = emails.toString().split(";");
					
					for(int i =0; i < lstEmail.length; i++){
						
						transMail.setTrml_ds_codigotransportadora(sheet.getRow(j).getCell(3).toString());
						transMail.setTrml_ds_razaoscoail(sheet.getRow(j).getCell(4).toString());
						
						Cell cpnj = sheet.getRow(j).getCell(7);
						cpnj.setCellType(CellType.STRING);			
						transMail.setTrml_ds_cnpj(cpnj.toString());	
						transMail.setTrml_ds_inativo("N");
						transMail.setTrml_id_usuarioatualizacao(idUser);
						transMail.setTrml_dh_importacao(dhImportacao);
						transMail.setTrml_ds_email(lstEmail[i].trim());				
										
						log.info("Insert");
						transMailDao.insertTransMail(transMail,dhImportacao,nmArquivo);
							
					
						
					}				
					
				}			
	
			}
		
		} catch (Exception e) {
			log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO DA BASE TRANSPORTADORA E-MAIL" + e + " ******");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			daoLog.createLog(logsBean);
			log.info("**** FIM SERVIÇO DE IMPORTAÇÃO DA BASE TRANSPORTADORA E-MAIL ****");
		}
		
		return transMail;
	}
	
}
