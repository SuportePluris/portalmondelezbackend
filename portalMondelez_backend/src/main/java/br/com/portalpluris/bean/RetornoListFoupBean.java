package br.com.portalpluris.bean;

public class RetornoListFoupBean {

	private String stat_id_cd_status;
	private String stat_ds_status;
	private String foup_id_cd_followup;
	private String foup_ds_followup;
	private String sucess;
	private String error;
	
	
	
	
	public String getStat_ds_status() {
		return stat_ds_status;
	}
	public void setStat_ds_status(String stat_ds_status) {
		this.stat_ds_status = stat_ds_status;
	}
	public String getStat_id_cd_status() {
		return stat_id_cd_status;
	}
	public void setStat_id_cd_status(String stat_id_cd_status) {
		this.stat_id_cd_status = stat_id_cd_status;
	}
	public String getSucess() {
		return sucess;
	}
	public void setSucess(String sucess) {
		this.sucess = sucess;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getFoup_id_cd_followup() {
		return foup_id_cd_followup;
	}
	public void setFoup_id_cd_followup(String foup_id_cd_followup) {
		this.foup_id_cd_followup = foup_id_cd_followup;
	}
	public String getFoup_ds_followup() {
		return foup_ds_followup;
	}
	public void setFoup_ds_followup(String foup_ds_followup) {
		this.foup_ds_followup = foup_ds_followup;
	}
	
	
	
	
	
}
