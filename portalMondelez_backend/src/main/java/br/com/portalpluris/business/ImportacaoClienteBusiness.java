package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.google.common.io.Files;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.portalpluris.bean.BaseClienteConsolidadaBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.dao.CsCdtbClienteClieDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.DAOLog;

public class ImportacaoClienteBusiness {

	public void importadorArquivoBaseCliente() throws IOException, EncryptedDocumentException, InvalidFormatException {

		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("*** INICIO DO SERVIÇO DE IMPORTAÇÃO DE ARQUIVO DE BASE DE CLIENTE ******");

		UtilMoveArquivo utilMove = new UtilMoveArquivo();
		
		// DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoClienteBusiness");
		logsBean.setLogsDsMetodo("importadorArquivoBaseCliente");
		logsBean.setLogsInErro("N");

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dhImportacao = sdf.format(date);

		BaseClienteConsolidadaBean baseCliente = new BaseClienteConsolidadaBean();
		BaseClienteConsolidadaBean baseClienteRet = new BaseClienteConsolidadaBean();
		CsCdtbClienteClieDao cliDao = new CsCdtbClienteClieDao();

		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");
//
		String diretorio = confDao.slctConf("conf.importacao.base.cliente");
		String diretorioBKP = confDao.slctConf("conf.importacao.base.cliente.bkp");
		String diretorioErro = confDao.slctConf("conf.importacao.base.cliente.erro");
		
		File arquivoProcessado = null;
		File arquivoProcessadobkp = null;
		File arquivoProcessadoerro = new File(diretorioErro+ "/");
		
		int linha = 0;
		

//		String diretorio = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond";
//		String diretorioBKP = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond\\bkp";	
//		
		try {			
		

			File arquivo = new File(diretorio);
			String[] arq = arquivo.list();

			if(arq != null){
				
			for (int i = 0; i < arq.length; i++) {

				 arquivoProcessado = new File(diretorio + "/" + arq[i]);
				 arquivoProcessadobkp = new File(diretorioBKP + "/" + arq[i]);

				String nmArquivo = arq[i] + "_" + dhImportacao;

				if (arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

					FileInputStream file = new FileInputStream(arquivoProcessado);
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file));

					Workbook wb = WorkbookFactory.create(file);
					Sheet sheet = wb.getSheetAt(0);

						log.info("*** INICIO DA LEITURA DO ARQUIVO " + arquivoProcessado.getName() + "******");

						for (int j = 1; j < sheet.getLastRowNum(); j++) {		
							
							linha = j;
							
							baseCliente = populaBean(j,sheet,nmArquivo,idUser);							
							
							log.info("*** VERIFICAR SE É UM NOVO REGISTRO OU UM UPDATE, ID CLIENTE: "
									+ baseCliente.getClie_id_cd_codigocliente()+ " ******");
							
							baseClienteRet = cliDao.slcIdCliente(baseCliente.getClie_id_cd_codigocliente());

							if (baseClienteRet.getClie_id_cd_cliente() == null) {

								log.info("INSERT");
								// CRIA NOVO CLIENTE
								cliDao.insertCliente(baseCliente);

							} else {

								log.info("UPDATE");
								// ATUALIZA CLIENTE
								cliDao.updateCliente(baseCliente);

							}
							

						}
					
				}
				boolean movido = utilMove.moveArquivo(arquivoProcessado,arquivoProcessadobkp);
				
				if(movido){
					log.info("Arquivo movido com sucesso");
				}else{
					log.info("Não foi possivel mover o arquivo");
				}
			}
		}

		} catch (Exception e) {
			log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO DE BASE DE CLIENTE" + e + " ******");
			logsBean.setLogsInErro("S");
			log.info("*** LINHA:" + linha + "************");
			logsBean.setLogsTxErro( "LINHA DE ERRO:" +linha +e.getMessage());
			utilMove.moveArquivo(arquivoProcessado,arquivoProcessadoerro);

		} finally {

			daoLog.createLog(logsBean);
			log.info("**** FIM SERVIÇO DE IMPORTAÇÃO DE ARQUIVO CLIENTE BASE ****");
		}

	}

	private BaseClienteConsolidadaBean populaBean(int j, Sheet sheet, String nmArquivo, String idUser) {

		BaseClienteConsolidadaBean baseCliente = new BaseClienteConsolidadaBean();

		// PRIMEIRA COLUNA ESTÁ COMO NUMERICO É NECESSÁRIO ALTERAR O TIPO
		Cell idcliente = sheet.getRow(j).getCell(0);
		idcliente.setCellType(CellType.STRING);
		idcliente.toString();														
		baseCliente.setClie_id_cd_codigocliente(idcliente.toString());
		
		
		if(sheet.getRow(j).getCell(1) == null){								
		}else{								
			
			baseCliente.setClie_ds_nomecliente(sheet.getRow(j).getCell(1).toString()); // CLIE_DS_NOMECLIENTE
		}
		
		if(sheet.getRow(j).getCell(2) != null){							
			Cell cpnj = sheet.getRow(j).getCell(2);
			cpnj.setCellType(CellType.STRING);
			cpnj.toString();
			baseCliente.setClie_ds_cnpj(cpnj.toString()); // CLIE_DS_CNPJ
		}else{						
		}
		if(sheet.getRow(j).getCell(3) == null){								
		}else{								

			baseCliente.setClie_ds_cidade(sheet.getRow(j).getCell(3).toString()); // CLIE_DS_CIDADE
		}
		if(sheet.getRow(j).getCell(4) == null){								
		}else{								

			baseCliente.setClie_ds_uf(sheet.getRow(j).getCell(4).toString()); // CLIE_DS_UF
		}
		if(sheet.getRow(j).getCell(5) == null){								
		}else{								
			
			baseCliente.setClie_ds_regiao(sheet.getRow(j).getCell(5).toString()); // CLIE_DS_REGIAO
		}
		if(sheet.getRow(j).getCell(6) == null){								
		}else{	
			baseCliente.setClie_ds_agrupamento(sheet.getRow(j).getCell(6).toString()); // CLIE_DS_AGRUPAMENTO
		}
		
		if(sheet.getRow(j).getCell(7) == null){								
		}else{								

			baseCliente.setClie_ds_endereco(sheet.getRow(j).getCell(7).toString()); // CLIE_DS_ENDERECO
		}
		
		if(sheet.getRow(j).getCell(8) == null){								
		}else{								
			baseCliente.setClie_ds_cep(sheet.getRow(j).getCell(8).toString()); // CLIE_DS_CEP
		}
		if(sheet.getRow(j).getCell(9) == null){								
		}else{								

			baseCliente.setClie_ds_temagendamento(sheet.getRow(j).getCell(9).toString()); // CLIE_DS_TEMAGENDAMENTO
		}
		if(sheet.getRow(j).getCell(10) == null){								
		}else{								

			baseCliente.setClie_ds_nfobrigatoria(sheet.getRow(j).getCell(10).toString()); // CLIE_DS_NFOBRIGATORIA
		}
		if(sheet.getRow(j).getCell(11) == null){								
		}else{								

			baseCliente.setClie_ds_agendamentoobrigatorio(sheet.getRow(j).getCell(11).toString().trim()); // CLIE_DS_AGENDAMENTOOBRIGATORIO
		}
		if(sheet.getRow(j).getCell(12) == null){								
		}else{								

			baseCliente.setClie_ds_tipoagendamento(sheet.getRow(j).getCell(12).toString().trim()); // CLIE_DS_TIPOAGENDAMENTO
		}
		if(sheet.getRow(j).getCell(13) == null){								
		}else{								

			baseCliente.setClie_ds_diasrecebimento(sheet.getRow(j).getCell(13).toString().trim()); // CLIE_DS_DIASRECEBIMENTO
		}
		if(sheet.getRow(j).getCell(14) == null){								
		}else{								
			baseCliente.setClie_ds_horariorecebimento(sheet.getRow(j).getCell(14).toString().trim()); // CLIE_DS_HORARIORECEBIMENTO								
		}

		if(sheet.getRow(j).getCell(15) == null){								
		}else{																
			baseCliente.setClie_ds_esperacliente(sheet.getRow(j).getCell(15).toString().trim()); // CLIE_DS_ESPERACLIENTE
		}

		if(sheet.getRow(j).getCell(16) == null){								
		}else{								
			
			baseCliente.setClie_ds_veiculodedicado(sheet.getRow(j).getCell(16).toString().trim()); // CLIE_DS_VEICULODEDICADO
		}

		if(sheet.getRow(j).getCell(17) == null){								
		}else{																
			baseCliente.setClie_ds_pagataxaveiculodedicado(sheet.getRow(j).getCell(17).toString().trim());// CLIE_DS_PAGATAXAVEICULODEDICADO
		}

		if(sheet.getRow(j).getCell(18) == null){								
		}else{								
			baseCliente.setClie_ds_tipodescarga(sheet.getRow(j).getCell(18).toString().trim()); // CLIE_DS_TIPODESCARGA								
		}


		if(sheet.getRow(j).getCell(19) == null){								
		}else{								
			
			baseCliente.setClie_ds_taxadescarga(sheet.getRow(j).getCell(19).toString().trim()); // CLIE_DS_TAXADESCARGA
		}
		
		if(sheet.getRow(j).getCell(20) == null){								
		}else{								
			
			baseCliente.setClie_ds_fefoespecial(sheet.getRow(j).getCell(20).toString().trim()); // CLIE_DS_FEFOESPECIAL
		}
		
		if(sheet.getRow(j).getCell(21) == null){								
		}else{								
			
			baseCliente.setClie_ds_paletizacaoespecial(sheet.getRow(j).getCell(21).toString().trim()); // CLIE_DS_PALETIZACAOESPECIAL
		}
		
		if(sheet.getRow(j).getCell(22) == null){								
		}else{								
			
			baseCliente.setClie_ds_clienteexigepalete(sheet.getRow(j).getCell(22).toString().trim()); // CLIE_DS_CLIENTEEXIGEPALETE
		}
		if(sheet.getRow(j).getCell(23) == null){								
		}else{								
			
			baseCliente.setClie_ds_paletizacaomondelez(sheet.getRow(j).getCell(23).toString().trim()); // CLIE_DS_PALETIZACAOMONDELEZ
		}
		if(sheet.getRow(j).getCell(24) == null){								
		}else{								

			baseCliente.setClie_ds_tipopaletizacao(sheet.getRow(j).getCell(24).toString().trim()); // CLIE_DS_TIPOPALETIZACAO
		}
		
		if(sheet.getRow(j).getCell(25) == null){								
		}else{								
			
			baseCliente.setClie_ds_alturpalete(sheet.getRow(j).getCell(25).toString().trim()); // CLIE_DS_ALTURPALETE
		}
		if(sheet.getRow(j).getCell(26) == null){								
		}else{								
			
			baseCliente.setClie_cd_veiculomax(sheet.getRow(j).getCell(26).toString().trim()); // CLIE_CD_VEICULOMAX								
		}
		if(sheet.getRow(j).getCell(27) == null){								
		}else{								
			baseCliente.setClie_ds_tipoveiculomax(sheet.getRow(j).getCell(27).toString().trim()); // CLIE_DS_TIPOVEICULOMAX								
		}
		if(sheet.getRow(j).getCell(28) == null){								
		}else{								
			baseCliente.setClie_ds_pesoveiculo(sheet.getRow(j).getCell(28).toString().trim()); // CLIE_DS_PESOVEICULO
		}
		if(sheet.getRow(j).getCell(29) == null){								
		}else{								
			baseCliente.setClie_ds_datarevisao(sheet.getRow(j).getCell(29).toString().trim()); // CLIE_DS_DATAREVISAO
		}
		if(sheet.getRow(j).getCell(30) == null){								
		}else{								
			baseCliente.setClie_ds_resprevisao(sheet.getRow(j).getCell(30).toString().trim()); // CLIE_DS_RESPREVISAO
		}
		
		if((sheet.getRow(j).getCell(31) == null)){
			baseCliente.setClie_ds_observacao(null);
		}else{
			
			baseCliente.setClie_ds_observacao(sheet.getRow(j).getCell(31).toString().trim()); // CLIE_DS_OBSERVACAO
		}
		
		if(sheet.getRow(j).getCell(32) == null){								
		}else{								
			baseCliente.setCli_ds_restricaocirculacao(sheet.getRow(j).getCell(32).toString().trim()); // CLI_DS_RESTRICAOCIRCULACAO
		}
		if(sheet.getRow(j).getCell(33) == null){								
		}else{								
			baseCliente.setClie_ds_tiporestricao(sheet.getRow(j).getCell(33).toString().trim()); // CLIE_DS_TIPORESTRICAO
		}
		if(sheet.getRow(j).getCell(34) == null){								
		}else{								
			baseCliente.setClie_ds_horariorestricao(sheet.getRow(j).getCell(34).toString().trim()); // CLIE_DS_HORARIORESTRICAO
		}
		baseCliente.setClie_nm_arquivoprocessado(nmArquivo);
		baseCliente.setClie_id_cd_usuarioatualizacao(idUser);
		
		
		
		
		return baseCliente;
	}

}
