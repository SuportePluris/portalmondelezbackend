package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbStatusStatBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.StatusQuantidadeBean;
import br.com.portalpluris.dao.CsCdtbStatusStatDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbStatusStatBusiness {

	CsCdtbStatusStatDao statDao = new CsCdtbStatusStatDao();
	private DAOLog daoLog = new DAOLog();

	public String createUpdateStat(CsCdtbStatusStatBean statusBean) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogStatus(statusBean, "createUpdateStat");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = statDao.createUpdateStat(statusBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListStatus(String statusInAtivo) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogStatus(null, "getListStatus");
		logsBean.setLogsTxInfoentrada(statusInAtivo);

		List<CsCdtbStatusStatBean> listStatusBean = new ArrayList<CsCdtbStatusStatBean>();

		String jsonRetorno = "";

		try {

			listStatusBean = statDao.getListStat(statusInAtivo);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listStatusBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}
	
	public String getListStatusQtd(String statusInAtivo, String idFuncionario) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogStatus(null, "getListStatusQtd");
		logsBean.setLogsTxInfoentrada(statusInAtivo);

		List<StatusQuantidadeBean> listStatusBean = new ArrayList<StatusQuantidadeBean>();

		String jsonRetorno = "";

		try {

			listStatusBean = statDao.getListStatQtd(statusInAtivo, idFuncionario);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listStatusBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getStatus(int statIdCdStatus) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogStatus(null, "getStatus");
		logsBean.setLogsTxInfoentrada(statIdCdStatus + "");

		CsCdtbStatusStatBean statusBean = new CsCdtbStatusStatBean();

		String jsonRetorno = "";

		try {

			statusBean = statDao.getStat(statIdCdStatus);
		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(statusBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	private EsLgtbLogsLogsBean getLogStatus(CsCdtbStatusStatBean statusBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbStatusStatBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");
		logsBean.setLogsTxInfoentrada(getAllValues(statusBean));

		if (statusBean != null) {
			logsBean.setLogsDsChave(statusBean.getStatus_ds_descricaostatus());
		}

		return logsBean;
	}

	private String getAllValues(CsCdtbStatusStatBean statusBean) {

		String all = "";

		if (statusBean != null) {
			all = "" + statusBean.getStat_id_cd_status() + ";" + statusBean.getStatus_ds_descricaostatus() + ";"
					+ statusBean.getStatus_dh_registro() + ";" + statusBean.getStatus_in_inativo() + ";"
					+ statusBean.getStatus_id_cd_usuarioatualizacao() + ";" + statusBean.getStatus_dh_atualizacao() + ";" + "";
		}

		return all;
	}

}
