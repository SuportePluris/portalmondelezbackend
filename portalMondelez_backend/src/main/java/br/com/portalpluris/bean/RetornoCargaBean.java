package br.com.portalpluris.bean;

public class RetornoCargaBean {
	
	
	private String numeroNotaFiscal;
	private String pedidoMond;
	private String ordeDeliveryNote;
	private String idShipment;
	private String shipmentDelivery;
	private String dhRegistroShipment;
	private String dhRegistroOrde;
	private String dhRegistroNotaFiscal;
	
	public String getNumeroNotaFiscal() {
		return numeroNotaFiscal;
	}
	public void setNumeroNotaFiscal(String numeroNotaFiscal) {
		this.numeroNotaFiscal = numeroNotaFiscal;
	}
	public String getPedidoMond() {
		return pedidoMond;
	}
	public void setPedidoMond(String pedidoMond) {
		this.pedidoMond = pedidoMond;
	}
	public String getOrdeDeliveryNote() {
		return ordeDeliveryNote;
	}
	public void setOrdeDeliveryNote(String ordeDeliveryNote) {
		this.ordeDeliveryNote = ordeDeliveryNote;
	}
	public String getIdShipment() {
		return idShipment;
	}
	public void setIdShipment(String idShipment) {
		this.idShipment = idShipment;
	}
	public String getShipmentDelivery() {
		return shipmentDelivery;
	}
	public void setShipmentDelivery(String shipmentDelivery) {
		this.shipmentDelivery = shipmentDelivery;
	}
	public String getDhRegistroShipment() {
		return dhRegistroShipment;
	}
	public void setDhRegistroShipment(String dhRegistroShipment) {
		this.dhRegistroShipment = dhRegistroShipment;
	}
	public String getDhRegistroOrde() {
		return dhRegistroOrde;
	}
	public void setDhRegistroOrde(String dhRegistroOrde) {
		this.dhRegistroOrde = dhRegistroOrde;
	}
	public String getDhRegistroNotaFiscal() {
		return dhRegistroNotaFiscal;
	}
	public void setDhRegistroNotaFiscal(String dhRegistroNotaFiscal) {
		this.dhRegistroNotaFiscal = dhRegistroNotaFiscal;
	}
	
	
	

}
