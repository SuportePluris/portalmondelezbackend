package br.com.portalpluris.bean;

public class StatusEmbarqueBean {
	
	private int emba_id_cd_embarque;
	private int emba_id_cd_usuarioatualizacao;
	private int stat_id_cd_status;
	
	
	public int getEmba_id_cd_embarque() {
		return emba_id_cd_embarque;
	}
	public void setEmba_id_cd_embarque(int emba_id_cd_embarque) {
		this.emba_id_cd_embarque = emba_id_cd_embarque;
	}
	public int getEmba_id_cd_usuarioatualizacao() {
		return emba_id_cd_usuarioatualizacao;
	}
	public void setEmba_id_cd_usuarioatualizacao(int emba_id_cd_usuarioatualizacao) {
		this.emba_id_cd_usuarioatualizacao = emba_id_cd_usuarioatualizacao;
	}
	public int getStat_id_cd_status() {
		return stat_id_cd_status;
	}
	public void setStat_id_cd_status(int stat_id_cd_status) {
		this.stat_id_cd_status = stat_id_cd_status;
	}
	
	

}
