package br.com.portalpluris.controller;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.portalpluris.bean.ImportacaoAgendamentoBean;
import br.com.portalpluris.bean.ImportacaoCadastroBean;
import br.com.portalpluris.bean.ImportacaoCargaBean;
import br.com.portalpluris.business.CsLgtbHierarquiaHierBusiness;
import br.com.portalpluris.business.CsLgtbProdutoProdBusiness;
import br.com.portalpluris.business.ImportacaoCanalPorClienteBusiness;
import br.com.portalpluris.business.ImportacaoClienteBusiness;
import br.com.portalpluris.business.ImportacaoContatoClienteBusiness;
import br.com.portalpluris.business.ImportacaoEmailTransportadoraBusiness;
import br.com.portalpluris.business.ImportacaoHierarquiaClienteBusiness;
import br.com.portalpluris.business.ImportacaoLeadTimeBusiness;
import br.com.portalpluris.business.ImportacaoNotaFiscalBusiness;
import br.com.portalpluris.business.ImportacaoTransportadoraBusiness;
import br.com.portalpluris.business.ProcessaCargaBusiness;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;

@Component
public class Tasks {

	Logger log = Logger.getLogger(this.getClass().getName());

	/**
	 * Método que executa tarefas periódicamente
	 * 
	 * @throws SocketException
	 * @throws IOException
	 * @author Rogério Nunes
	 * @throws SQLException
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	public String getFixedDelay() {        
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		String tempoProcessarNovamente = confDao.slctConf("conf.importacao.processamento.time.minutos");
		
		if(!tempoProcessarNovamente.equalsIgnoreCase("") && StringUtils.isNumeric(tempoProcessarNovamente) ){
			tempoProcessarNovamente = (Long.parseLong(tempoProcessarNovamente)*60)+"";
		}
		
		return tempoProcessarNovamente;
	}
	
	//@Scheduled(fixedDelayString = "#{@syncScheduler.getFixedDelay()}")
	@Scheduled(fixedRate = 3600000) // 3600000= 1 HORA  // 900000 = 15minutos	
	public void reportCurrentTime() throws SocketException, IOException, SQLException, EncryptedDocumentException, InvalidFormatException {
		
		log.info("[TASKS] - EXECUTANDO TAREFAS ");
		
		ImportacaoClienteBusiness mondelezMethod = new ImportacaoClienteBusiness();
		ImportacaoLeadTimeBusiness leadTime = new ImportacaoLeadTimeBusiness();
		ImportacaoTransportadoraBusiness transMethod = new ImportacaoTransportadoraBusiness();
		ImportacaoHierarquiaClienteBusiness hierarquiaMethod = new ImportacaoHierarquiaClienteBusiness();
		ImportacaoNotaFiscalBusiness nfMethod = new ImportacaoNotaFiscalBusiness();
		ImportacaoContatoClienteBusiness contatoCli = new ImportacaoContatoClienteBusiness();
		ImportacaoEmailTransportadoraBusiness transMail = new ImportacaoEmailTransportadoraBusiness();
		ImportacaoCargaBean importacaoCarga = new ImportacaoCargaBean();
		ProcessaCargaBusiness processaCarga = new ProcessaCargaBusiness();
		CsLgtbProdutoProdBusiness importaProduto = new CsLgtbProdutoProdBusiness();
		CsLgtbHierarquiaHierBusiness importaHierarquia = new CsLgtbHierarquiaHierBusiness();

		ImportacaoCanalPorClienteBusiness canalCliente = new ImportacaoCanalPorClienteBusiness();
		ImportacaoCadastroBean importacaoCadastro = new ImportacaoCadastroBean();
		ImportacaoAgendamentoBean importacaoAgendamento = new ImportacaoAgendamentoBean();
		
		mondelezMethod.importadorArquivoBaseCliente();
		leadTime.importadorArquivoLeadTime();
		transMethod.importadorArquivoTransportadora();
		hierarquiaMethod.importadorArquivoHierarquiaCliente();
		nfMethod.importadorNotaFiscal();
		contatoCli.importadorContatoCliente();
		transMail.importadorEmailTransportadora();

		//canalCliente.importadorCanalCliente();
		
	
		// *** IMPORTACAO CARGA DE PRODUTO TXT ***
		//importaProduto.gravarProduto(); // B2B

		// *** IMPORTACAO CARGA DE HIERARQUIA TXT ***
		//importaHierarquia.gravarHierarquia(); //B2B

		importacaoCarga.iniciarImportacao(); // ANTIGO 
		//importacaoCadastro.iniciarImportacao(); // B2B

		// *** PROCESSAMENTO CARGA DE AGENDAMENTO ***
		processaCarga.iniciarProcessamentoCarga(); // ANTIGO
		//importacaoAgendamento.iniciarImportacao(); // B2B
		
		log.info("[TASKS] - FIM DE EXECUCAO TAREFAS ");
	}

}