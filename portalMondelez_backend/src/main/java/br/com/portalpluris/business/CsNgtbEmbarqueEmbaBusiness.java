package br.com.portalpluris.business;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.EmailsUtil;
import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsNgtbEmbarqueAgendamentoEmbaBean;
import br.com.portalpluris.bean.CsNgtbEmbarqueEmbaBean;
import br.com.portalpluris.bean.EmbaShipEmshBean;
import br.com.portalpluris.bean.EmbarqueAgendamentoBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.RetornoShipmentBean;
import br.com.portalpluris.bean.StatusEmbarqueBean;
import br.com.portalpluris.dao.CsNgtbEmbarqueEmbaDao;
import br.com.portalpluris.dao.DAOLog;

public class CsNgtbEmbarqueEmbaBusiness {

	private CsNgtbEmbarqueEmbaDao embaDao = new CsNgtbEmbarqueEmbaDao();
	private DAOLog daoLog = new DAOLog();

	public String getEmbarqueByStatus(String emba_id_cd_status, String idFuncionario) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "getEmbarqueByStatus");
		logsBean.setLogsTxInfoentrada(emba_id_cd_status + "");
		logsBean.setLogsDsChave(emba_id_cd_status + "");

		List<RetornoShipmentBean> retornoShipmentBean = new ArrayList<RetornoShipmentBean>();

		String jsonRetorno = "";

		try {

			retornoShipmentBean = embaDao.getEmba(emba_id_cd_status, idFuncionario);	
		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retornoShipmentBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListEmba(int embaIdCdStatusagendamento) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "getListEmba");
		logsBean.setLogsTxInfoentrada(embaIdCdStatusagendamento + "");
		logsBean.setLogsDsChave(embaIdCdStatusagendamento + "");

		List<CsNgtbEmbarqueEmbaBean> lstEmbaBean = new ArrayList<CsNgtbEmbarqueEmbaBean>();

		String jsonRetorno = "";

		try {

			lstEmbaBean = embaDao.getListEmba(embaIdCdStatusagendamento);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(lstEmbaBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	private EsLgtbLogsLogsBean getLogEmba(CsNgtbEmbarqueEmbaBean embarqueBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsNgtbEmbarqueEmbaBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");

		return logsBean;
	}

	public String getSomaEmba(String filtro, String criterio, String pa) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "getSomaEmba");
		logsBean.setLogsTxInfoentrada(filtro + ";" + criterio + ";" + pa);
		logsBean.setLogsDsChave(filtro + ";" + criterio + ";" + pa);

		EmbaShipEmshBean enshBean = new EmbaShipEmshBean();

		String jsonRetorno = "";

		try {

			enshBean = embaDao.getSomaEmba(filtro, criterio, pa);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(enshBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListEmba(String filtro, String criterio, String pa) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "getListEmba");
		logsBean.setLogsTxInfoentrada(filtro + ";" + criterio + ";" + pa);
		logsBean.setLogsDsChave(filtro + ";" + criterio + ";" + pa);

		List<EmbaShipEmshBean> lstEnshBean = new ArrayList<EmbaShipEmshBean>();

		String jsonRetorno = "";

		try {

			lstEnshBean = embaDao.getListEmba(filtro, criterio, pa);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(lstEnshBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String envioEmbarque(int embaIdCdEmbarque, int userIdCdUsuario) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "envioEmbarque");
		logsBean.setLogsTxInfoentrada(embaIdCdEmbarque + ";" + userIdCdUsuario);
		logsBean.setLogsDsChave(embaIdCdEmbarque + "");

		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

//			retBean = EmailsUtil.sendMail(embaIdCdEmbarque, userIdCdUsuario, null, null);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getLstAgendamento(int embaIdCdEmbarque) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "getLstAgendamento");
		logsBean.setLogsTxInfoentrada(embaIdCdEmbarque + "");
		logsBean.setLogsDsChave(embaIdCdEmbarque + "");

		List<EmbarqueAgendamentoBean> lstEmbaAgenBean = new ArrayList<EmbarqueAgendamentoBean>();
		String jsonRetorno = "";

		try {

			lstEmbaAgenBean = embaDao.getLstAgendamento(embaIdCdEmbarque);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(lstEmbaAgenBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getAgendamento(int daag_id_cd_agendamento) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "getAgendamento");
		logsBean.setLogsTxInfoentrada(daag_id_cd_agendamento + "");
		logsBean.setLogsDsChave(daag_id_cd_agendamento + "");

		EmbarqueAgendamentoBean embaAgenBean = new EmbarqueAgendamentoBean();
		String jsonRetorno = "";

		try {

			embaAgenBean = embaDao.getAgendamento(daag_id_cd_agendamento);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(embaAgenBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String createUpdateAgendamento(EmbarqueAgendamentoBean embaBembaAgenBeanean) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "createUpdateAgendamento");
		logsBean.setLogsTxInfoentrada(embaBembaAgenBeanean.getDaag_id_cd_embarque() + "");
		logsBean.setLogsDsChave(embaBembaAgenBeanean.getDaag_id_cd_embarque() + "");

		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = embaDao.createUpdateAgendamento(embaBembaAgenBeanean);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}
	
	/**
	 * Metodo responsavel por realizar o update do lead time
	 * @param String, String
	 * @author Caio Fernandes
	 * @return 
	 * @throws JsonProcessingException 
	 * @throws NumberFormatException 
	 * @throws ParseException 
	 * */
	public String updateLeadTime(CsNgtbEmbarqueAgendamentoEmbaBean embarqueAgendamentoBean) throws NumberFormatException, JsonProcessingException, ParseException {
		
		EsLgtbLogsLogsBean logsBean = getLogEmba(null, "updateLeadTime");
		logsBean.setLogsTxInfoentrada(embarqueAgendamentoBean.getUpdt_dh_leadtime() + ";" + embarqueAgendamentoBean.getEmba_id_cd_embarque() + ";" + embarqueAgendamentoBean.getEmba_id_cd_usuarioatualizacao());
		logsBean.setLogsDsChave(embarqueAgendamentoBean.getEmba_id_cd_embarque() + "");

		String jsonRetorno = "";
		
		try {

			RetornoBean ret = embaDao.updateLeadTime(embarqueAgendamentoBean);
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(ret);

		} catch (Exception e) {
			e.printStackTrace();
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
		
	}
	
	public void setEmbarqueEmUsoBusiness(int emba_id_cd_embarque, String in_uso, int emba_id_cd_usuarioatualizacao) throws SQLException {
		
		embaDao.setEmbarqueEmUsoDao(emba_id_cd_embarque, in_uso, emba_id_cd_usuarioatualizacao);
		
		
	}
	
	
	public String updateStatusEmbarque(StatusEmbarqueBean statusEmbarqueBean) throws JsonProcessingException {
		
		
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = embaDao.updateStatusEmbarque(statusEmbarqueBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		
		return jsonRetorno;
		
		
	}

}
