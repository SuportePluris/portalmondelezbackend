package br.com.portalpluris.business;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import java.util.logging.Logger;

import br.com.portalpluris.bean.ImportacaoCancelarPedidoBean;
import br.com.portalpluris.bean.ImportacaoCodTranspBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsNgtbEmbarqueEmbaDao;
import br.com.portalpluris.dao.DAO;
import br.com.portalpluris.dao.ImportacaoPortalDao;

public class ImportacaoPortalBusiness {
	
	
	/**
	 * Metodo responsavel por executar as regras de update do arquivo upado no portal
	 * @author Caio Fernandes
	 * @param List<ImportacaoCodTranspBean>
	 * @throws SQLException 
	 * */
	public RetornoBean updateCodTransp(List<ImportacaoCodTranspBean> lstCodTranspBean) {
		
		ImportacaoPortalDao portalDao = new ImportacaoPortalDao();
		Logger log = Logger.getLogger(this.getClass().getName());	
		RetornoBean retBean = new RetornoBean();
		
		log.info("[updateCodTransp] - INICIO ");
		
		try {
			
			portalDao.updateCodTransp(lstCodTranspBean);
			retBean.setSucesso(true);
			retBean.setMsg("Registros atualizados com sucesso");
			
		} catch (Exception e) {
			
			retBean.setSucesso(true);
			retBean.setMsg("Erro ao atualizar registros");
			
		}
			
		
		return retBean;
	}
	
	/**
	 * Metodo responsavel por executar as regras de update para cancelar os pedidos contidos no arquivo upado no portal
	 * @author Caio Fernandes
	 * @param List<ImportacaoCodTranspBean>
	 * @throws SQLException 
	 * */
	public RetornoBean updateCancelarPedido(List<ImportacaoCancelarPedidoBean> lstCancelarPedidoBean) {
		
		ImportacaoPortalDao portalDao = new ImportacaoPortalDao();
		Logger log = Logger.getLogger(this.getClass().getName());	
		RetornoBean retBean = new RetornoBean();
		
		log.info("[updateCancelarPedido] - INICIO ");
		
		try {
			
			portalDao.updatePedidoCancelado(lstCancelarPedidoBean);
			retBean.setSucesso(true);
			retBean.setMsg("Registros atualizados com sucesso");
			
		} catch (Exception e) {
			
			retBean.setSucesso(true);
			retBean.setMsg("Erro ao atualizar registros");
			
		}
			
		
		return retBean;
	}

}
