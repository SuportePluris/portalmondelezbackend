package br.com.portalpluris.bean;

public class MotivoResponsavelCutoffBean {
	private String moti_ds_motivo;
	private String resp_nm_responsavel;
	
	public String getMoti_ds_motivo() {
		return moti_ds_motivo;
	}
	public void setMoti_ds_motivo(String moti_ds_motivo) {
		this.moti_ds_motivo = moti_ds_motivo;
	}
	public String getResp_nm_responsavel() {
		return resp_nm_responsavel;
	}
	public void setResp_nm_responsavel(String resp_nm_nomeresponsavel) {
		this.resp_nm_responsavel = resp_nm_nomeresponsavel;
	}
	
}
