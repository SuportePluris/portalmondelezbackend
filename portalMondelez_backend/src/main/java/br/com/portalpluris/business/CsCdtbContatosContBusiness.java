package br.com.portalpluris.business;

import java.sql.SQLException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.portalpluris.bean.ContatosBean;
import br.com.portalpluris.bean.RetornoInsertContatoBean;
import br.com.portalpluris.dao.CsCdtbContatosContDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbContatosContBusiness {
//	CRIADO POR RAFA 03/08/2021
	private DAOLog daoLog = new DAOLog();
	private CsCdtbContatosContDao contatosDao = new CsCdtbContatosContDao();

	public String getContatosTransportadora(String idEmbarque) throws SQLException {
		String retorno = "";
		try {
			retorno = contatosDao.getContatos("TRANSPORTADORA", idEmbarque);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retorno;
	}
	
	public String getContatosCliente(String idEmbarque) throws SQLException {
		String retorno = "";
		try {
			retorno = contatosDao.getContatos("CLIENTE", idEmbarque);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retorno;
	}
	
	public String getContatosFocal(String idEmbarque) throws SQLException {
		String retorno = "";
		try {
			retorno = contatosDao.getContatos("FOCAL", idEmbarque);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retorno;
	}


	public String insertContatoCliente(ContatosBean contBean) throws SQLException, JsonProcessingException {
		
		ObjectMapper mapper = new ObjectMapper();
		
		
		RetornoInsertContatoBean insertContatoRet = new RetornoInsertContatoBean();
		
		//VERIFICA SE O CONTATO JÁ EXISTE.
		boolean ifExist = contatosDao.slctContato(contBean);
			
		if(!ifExist){
			
			insertContatoRet = contatosDao.insertContato(contBean);
			
		}else{
			
			 insertContatoRet.setMenssagem("O contato ja esta cadastrado");
			 insertContatoRet.setIdErro("2");			 
			 String retorno = mapper.writeValueAsString(insertContatoRet);
			 
			 return retorno;
		}
		
		//CASO NÃO EXISTA O CONTATO INSERT
		
		String retorno = mapper.writeValueAsString(insertContatoRet);
		
		return retorno;
		
	}
	
}
