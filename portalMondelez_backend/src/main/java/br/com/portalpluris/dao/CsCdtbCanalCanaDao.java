package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsAstbUsuariocanalUscaBean;
import br.com.portalpluris.bean.CsCdtbCanalCanaBean;
import br.com.portalpluris.bean.CsCdtbTransportadoraTranBean;
import br.com.portalpluris.bean.CsCdtbUsuarioUserBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsCdtbCanalCanaDao {

	public RetornoBean createCanal(CsCdtbCanalCanaBean bean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		pst = conn.prepareStatement("" 
				+ " INSERT INTO CS_CDTB_CANAL_CANA ( "
				+ " 		CANA_DS_CANAL, " 
				+ "			CANA_IN_INATIVO "
				+ " ) VALUES ( ?, ? )");

		pst.setString(1, bean.getCana_ds_canal());
		pst.setString(2, bean.getCana_in_inativo());
				
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Cadastro efetuado com sucesso!");

		return retBean;
	}
	

	private RetornoBean deleteCanal(CsCdtbCanalCanaBean bean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		pst = conn.prepareStatement("  	DELETE "
								   + "		CS_CDTB_CANAL_CANA "
								   + "	WHERE "
								   + "		CANA_ID_CD_CANAL = ? ");

		pst.setString(1, bean.getCana_id_cd_canal());
		
		//pst = conn.prepareStatement(queryUpdate);
		pst.executeUpdate();

		retBean.setSucesso(true);
		retBean.setProcesso("DELETE");
		retBean.setMsg("Cadastro excluído com sucesso!");

		return retBean;
	}

	
	
	
	public List<CsCdtbCanalCanaBean> getListCanal(String inativo, String idUsuario) throws SQLException {

	List<CsCdtbCanalCanaBean> listBean = new ArrayList<CsCdtbCanalCanaBean>();

		
		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		//String query = "SELECT DISTINCT CNCL_DS_CHANEL FROM CS_CDTB_CANALPORCLIENTE_CNCL";
		String query =    "  " 
							+ "	SELECT "
							+ "		CANA_ID_CD_CANAL, "
							+ "		CANA_DS_CANAL, "
							+ "		case when CANA_IN_INATIVO = 'N' THEN 'NÃO' "
							+ "			 when CANA_IN_INATIVO = 'S' THEN 'SIM' "
							+ "		ELSE '' END AS CANA_IN_INATIVO	, "
							+ "		CANA_DH_REGISTRO "
							+ "	FROM  "
							+ "		CS_CDTB_CANAL_CANA (NOLOCK)"
							+ "	WHERE "
							+ "		CANA_ID_CD_CANAL NOT IN (SELECT CANA_ID_CD_CANAL FROM CS_ASTB_USUARIOCANAL_USCA (NOLOCK) WHERE USER_ID_CD_USUARIO = '" +idUsuario+ "')";
		
		if (inativo != null && !inativo.equals("") && !inativo.equals("null")) {
			query += " AND CANA_IN_INATIVO = '" + inativo+"'";

		}
		query += " ORDER BY CANA_DS_CANAL ";


		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbCanalCanaBean canalBean = setCanalBean(rs);

			listBean.add(canalBean);
		}

		return listBean;
	}
	
	public List<CsCdtbCanalCanaBean> getListCanal(String inativo) throws SQLException {

	List<CsCdtbCanalCanaBean> listBean = new ArrayList<CsCdtbCanalCanaBean>();

		
		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query =    "  " 
							+ "	SELECT "
							+ "		CANA_ID_CD_CANAL, "
							+ "		CANA_DS_CANAL, "
							+ "		case when CANA_IN_INATIVO = 'N' THEN 'NÃO' "
							+ "			 when CANA_IN_INATIVO = 'S' THEN 'SIM' "
							+ "		ELSE '' END AS CANA_IN_INATIVO	, "
							+ "		CANA_DH_REGISTRO "
							+ "	FROM  "
							+ "		CS_CDTB_CANAL_CANA (NOLOCK)"
							+ "	WHERE "
							+ "		CANA_IN_INATIVO = '" + inativo +"'"
							+ " ORDER BY CANA_DS_CANAL";

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbCanalCanaBean canalBean = setCanalBean(rs);

			listBean.add(canalBean);
		}

		return listBean;
	}
	
	
	private CsCdtbCanalCanaBean setCanalBean(ResultSet rs) throws SQLException {

		// CANAL 
		CsCdtbCanalCanaBean canalBean = new CsCdtbCanalCanaBean();
			canalBean.setCana_id_cd_canal(rs.getString("CANA_ID_CD_CANAL"));
			canalBean.setCana_ds_canal(rs.getString("CANA_DS_CANAL"));
			canalBean.setCana_dh_registro(rs.getString("CANA_DH_REGISTRO"));
			canalBean.setCana_in_inativo(rs.getString("CANA_IN_INATIVO"));

		return canalBean;
	}

	public RetornoBean updateCanal(CsCdtbCanalCanaBean bean) {

		RetornoBean retBean = new RetornoBean();
		
		try {

 			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst;

			pst = con.prepareStatement("" 
					+ "	UPDATE "
					+ "		CS_CDTB_CANAL_CANA  "
					+ "	SET"
					+ "		CANA_DS_CANAL = ? ," 
					+ " 	CANA_IN_INATIVO = ? "
					+ "	WHERE "
					+ "		CANA_ID_CD_CANAL = ? ");

			pst.setString(1, bean.getCana_ds_canal());
			pst.setString(2, bean.getCana_in_inativo());
			pst.setString(3, bean.getCana_id_cd_canal());

			pst.executeUpdate();
			
			retBean.setSucesso(true);
			retBean.setProcesso("CADASTRO");
			retBean.setMsg("Cadastro alterado com sucesso!");

		} catch (Exception e) {
			
			retBean.setSucesso(false);
			retBean.setProcesso("DELETE");
			retBean.setMsg("Erro ao alterar Canal!");

			System.out.println("***** Erro no UPDATE de Canal " + e + " *****");
			
		}
		
		return retBean;

	}


}
