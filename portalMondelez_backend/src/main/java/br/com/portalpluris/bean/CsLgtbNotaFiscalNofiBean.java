package br.com.portalpluris.bean;

import java.util.Date;

public class CsLgtbNotaFiscalNofiBean {
	
	String nofi_ds_company;
	String nofi_ds_noname;
	String nofi_ds_notafiscalserie;
	String nofi_ds_notafiscalnumber;
	String nofi_ds_salesordernumber;
	String nofi_ds_salesordernumber2;
	String nofi_ds_salesordernumber3;
	String nofi_ds_salesordernumber4;
	Date nofi_dh_notafiscaldate;
	Date nofi_dh_deliverydate;
	String nofi_cd_customercode;
	String nofi_ds_totalcostnotafiscal;
	String nofi_ds_quantity;
	String nofi_ds_netweight;
	String nofi_ds_grossweight;
	String nofi_ds_catcod6;
	String nofi_ds_catcod7;
	String nofi_ds_catcod8;
	String nofi_ds_catcod10;
	String nofi_ds_distrchannel;
	String nofi_ds_vendorcode;
	String nofi_ds_cfopcode;
	String nofi_ds_description;
	String nofi_ds_carriername;
	String nofi_ds_codelocation;
	String nofi_ds_doctype;
	String nofi_ds_nextnumber;
	String nofi_ds_volumenota;
	String nofi_ds_netvalue;
	String nofi_ds_invoicenumber;
	Date nofi_dh_dateinvoice;
	String nofi_dh_registro;
	String nofi_ds_statusprocessamento;
	String nofi_dh_processamento;
	String nofi_ds_nomearquivo;
	
	String retorno;
	String erro;
	
	
	
	
	public String getRetorno() {
		return retorno;
	}
	public void setRetorno(String retorno) {
		this.retorno = retorno;
	}
	public String getErro() {
		return erro;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public String getNofi_ds_company() {
		return nofi_ds_company;
	}
	public void setNofi_ds_company(String nofi_ds_company) {
		this.nofi_ds_company = nofi_ds_company;
	}
	public String getNofi_ds_noname() {
		return nofi_ds_noname;
	}
	public void setNofi_ds_noname(String nofi_ds_noname) {
		this.nofi_ds_noname = nofi_ds_noname;
	}
	public String getNofi_ds_notafiscalserie() {
		return nofi_ds_notafiscalserie;
	}
	public void setNofi_ds_notafiscalserie(String nofi_ds_notafiscalserie) {
		this.nofi_ds_notafiscalserie = nofi_ds_notafiscalserie;
	}
	public String getNofi_ds_notafiscalnumber() {
		return nofi_ds_notafiscalnumber;
	}
	public void setNofi_ds_notafiscalnumber(String nofi_ds_notafiscalnumber) {
		this.nofi_ds_notafiscalnumber = nofi_ds_notafiscalnumber;
	}
	public String getNofi_ds_salesordernumber() {
		return nofi_ds_salesordernumber;
	}
	public void setNofi_ds_salesordernumber(String nofi_ds_salesordernumber) {
		this.nofi_ds_salesordernumber = nofi_ds_salesordernumber;
	}
	public String getNofi_ds_salesordernumber2() {
		return nofi_ds_salesordernumber2;
	}
	public void setNofi_ds_salesordernumber2(String nofi_ds_salesordernumber2) {
		this.nofi_ds_salesordernumber2 = nofi_ds_salesordernumber2;
	}
	public String getNofi_ds_salesordernumber3() {
		return nofi_ds_salesordernumber3;
	}
	public void setNofi_ds_salesordernumber3(String nofi_ds_salesordernumber3) {
		this.nofi_ds_salesordernumber3 = nofi_ds_salesordernumber3;
	}
	public String getNofi_ds_salesordernumber4() {
		return nofi_ds_salesordernumber4;
	}
	public void setNofi_ds_salesordernumber4(String nofi_ds_salesordernumber4) {
		this.nofi_ds_salesordernumber4 = nofi_ds_salesordernumber4;
	}
	public Date getNofi_dh_notafiscaldate() {
		return nofi_dh_notafiscaldate;
	}
	public void setNofi_dh_notafiscaldate(Date nofi_dh_notafiscaldate) {
		this.nofi_dh_notafiscaldate = nofi_dh_notafiscaldate;
	}
	public Date getNofi_dh_deliverydate() {
		return nofi_dh_deliverydate;
	}
	public void setNofi_dh_deliverydate(Date nofi_dh_deliverydate) {
		this.nofi_dh_deliverydate = nofi_dh_deliverydate;
	}
	public String getNofi_cd_customercode() {
		return nofi_cd_customercode;
	}
	public void setNofi_cd_customercode(String nofi_cd_customercode) {
		this.nofi_cd_customercode = nofi_cd_customercode;
	}
	public String getNofi_ds_totalcostnotafiscal() {
		return nofi_ds_totalcostnotafiscal;
	}
	public void setNofi_ds_totalcostnotafiscal(String nofi_ds_totalcostnotafiscal) {
		this.nofi_ds_totalcostnotafiscal = nofi_ds_totalcostnotafiscal;
	}
	public String getNofi_ds_quantity() {
		return nofi_ds_quantity;
	}
	public void setNofi_ds_quantity(String nofi_ds_quantity) {
		this.nofi_ds_quantity = nofi_ds_quantity;
	}
	public String getNofi_ds_netweight() {
		return nofi_ds_netweight;
	}
	public void setNofi_ds_netweight(String nofi_ds_netweight) {
		this.nofi_ds_netweight = nofi_ds_netweight;
	}
	public String getNofi_ds_grossweight() {
		return nofi_ds_grossweight;
	}
	public void setNofi_ds_grossweight(String nofi_ds_grossweight) {
		this.nofi_ds_grossweight = nofi_ds_grossweight;
	}
	public String getNofi_ds_catcod6() {
		return nofi_ds_catcod6;
	}
	public void setNofi_ds_catcod6(String nofi_ds_catcod6) {
		this.nofi_ds_catcod6 = nofi_ds_catcod6;
	}
	public String getNofi_ds_catcod7() {
		return nofi_ds_catcod7;
	}
	public void setNofi_ds_catcod7(String nofi_ds_catcod7) {
		this.nofi_ds_catcod7 = nofi_ds_catcod7;
	}
	public String getNofi_ds_catcod8() {
		return nofi_ds_catcod8;
	}
	public void setNofi_ds_catcod8(String nofi_ds_catcod8) {
		this.nofi_ds_catcod8 = nofi_ds_catcod8;
	}
	public String getNofi_ds_catcod10() {
		return nofi_ds_catcod10;
	}
	public void setNofi_ds_catcod10(String nofi_ds_catcod10) {
		this.nofi_ds_catcod10 = nofi_ds_catcod10;
	}
	public String getNofi_ds_distrchannel() {
		return nofi_ds_distrchannel;
	}
	public void setNofi_ds_distrchannel(String nofi_ds_distrchannel) {
		this.nofi_ds_distrchannel = nofi_ds_distrchannel;
	}
	public String getNofi_ds_vendorcode() {
		return nofi_ds_vendorcode;
	}
	public void setNofi_ds_vendorcode(String nofi_ds_vendorcode) {
		this.nofi_ds_vendorcode = nofi_ds_vendorcode;
	}
	public String getNofi_ds_cfopcode() {
		return nofi_ds_cfopcode;
	}
	public void setNofi_ds_cfopcode(String nofi_ds_cfopcode) {
		this.nofi_ds_cfopcode = nofi_ds_cfopcode;
	}
	public String getNofi_ds_description() {
		return nofi_ds_description;
	}
	public void setNofi_ds_description(String nofi_ds_description) {
		this.nofi_ds_description = nofi_ds_description;
	}
	public String getNofi_ds_carriername() {
		return nofi_ds_carriername;
	}
	public void setNofi_ds_carriername(String nofi_ds_carriername) {
		this.nofi_ds_carriername = nofi_ds_carriername;
	}
	public String getNofi_ds_codelocation() {
		return nofi_ds_codelocation;
	}
	public void setNofi_ds_codelocation(String nofi_ds_codelocation) {
		this.nofi_ds_codelocation = nofi_ds_codelocation;
	}
	public String getNofi_ds_doctype() {
		return nofi_ds_doctype;
	}
	public void setNofi_ds_doctype(String nofi_ds_doctype) {
		this.nofi_ds_doctype = nofi_ds_doctype;
	}
	public String getNofi_ds_nextnumber() {
		return nofi_ds_nextnumber;
	}
	public void setNofi_ds_nextnumber(String nofi_ds_nextnumber) {
		this.nofi_ds_nextnumber = nofi_ds_nextnumber;
	}
	public String getNofi_ds_volumenota() {
		return nofi_ds_volumenota;
	}
	public void setNofi_ds_volumenota(String nofi_ds_volumenota) {
		this.nofi_ds_volumenota = nofi_ds_volumenota;
	}
	public String getNofi_ds_netvalue() {
		return nofi_ds_netvalue;
	}
	public void setNofi_ds_netvalue(String nofi_ds_netvalue) {
		this.nofi_ds_netvalue = nofi_ds_netvalue;
	}
	public String getNofi_ds_invoicenumber() {
		return nofi_ds_invoicenumber;
	}
	public void setNofi_ds_invoicenumber(String nofi_ds_invoicenumber) {
		this.nofi_ds_invoicenumber = nofi_ds_invoicenumber;
	}
	public Date getNofi_dh_dateinvoice() {
		return nofi_dh_dateinvoice;
	}
	public void setNofi_dh_dateinvoice(Date nofi_dh_dateinvoice) {
		this.nofi_dh_dateinvoice = nofi_dh_dateinvoice;
	}
	public String getNofi_dh_registro() {
		return nofi_dh_registro;
	}
	public void setNofi_dh_registro(String nofi_dh_registro) {
		this.nofi_dh_registro = nofi_dh_registro;
	}
	public String getNofi_ds_statusprocessamento() {
		return nofi_ds_statusprocessamento;
	}
	public void setNofi_ds_statusprocessamento(String nofi_ds_statusprocessamento) {
		this.nofi_ds_statusprocessamento = nofi_ds_statusprocessamento;
	}
	public String getNofi_dh_processamento() {
		return nofi_dh_processamento;
	}
	public void setNofi_dh_processamento(String nofi_dh_processamento) {
		this.nofi_dh_processamento = nofi_dh_processamento;
	}
	public String getNofi_ds_nomearquivo() {
		return nofi_ds_nomearquivo;
	}
	public void setNofi_ds_nomearquivo(String nofi_ds_nomearquivo) {
		this.nofi_ds_nomearquivo = nofi_ds_nomearquivo;
	}
}
