package br.com.portalpluris.bean;

import java.sql.Timestamp;

public class EmbaShipEmshBean {

	int embaIdCdEmbarque;
	int shipIdCdShipment;
	double shipNrPeso;
	double shipNrCubagem;
	double shipNrValor;
	double shipNrVolumes;
	Timestamp shipDhLeadtime;
	String shipDsTipocarga;
	String shipDsCodigotransportadora;
	String tranNmNometransportadora;
	String clieDsNomecliente;
	String shipIdCdDeliverynote;

	public int getEmbaIdCdEmbarque() {
		return embaIdCdEmbarque;
	}

	public void setEmbaIdCdEmbarque(int embaIdCdEmbarque) {
		this.embaIdCdEmbarque = embaIdCdEmbarque;
	}

	public int getShipIdCdShipment() {
		return shipIdCdShipment;
	}

	public void setShipIdCdShipment(int shipIdCdShipment) {
		this.shipIdCdShipment = shipIdCdShipment;
	}

	public double getShipNrPeso() {
		return shipNrPeso;
	}

	public void setShipNrPeso(double shipNrPeso) {
		this.shipNrPeso = shipNrPeso;
	}

	public double getShipNrCubagem() {
		return shipNrCubagem;
	}

	public void setShipNrCubagem(double shipNrCubagem) {
		this.shipNrCubagem = shipNrCubagem;
	}

	public double getShipNrValor() {
		return shipNrValor;
	}

	public void setShipNrValor(double shipNrValor) {
		this.shipNrValor = shipNrValor;
	}

	public double getShipNrVolumes() {
		return shipNrVolumes;
	}

	public void setShipNrVolumes(double shipNrVolumes) {
		this.shipNrVolumes = shipNrVolumes;
	}

	public Timestamp getShipDhLeadtime() {
		return shipDhLeadtime;
	}

	public void setShipDhLeadtime(Timestamp shipDhLeadtime) {
		this.shipDhLeadtime = shipDhLeadtime;
	}

	public String getShipDsTipocarga() {
		return shipDsTipocarga;
	}

	public void setShipDsTipocarga(String shipDsTipocarga) {
		this.shipDsTipocarga = shipDsTipocarga;
	}

	public String getShipDsCodigotransportadora() {
		return shipDsCodigotransportadora;
	}

	public void setShipDsCodigotransportadora(String shipDsCodigotransportadora) {
		this.shipDsCodigotransportadora = shipDsCodigotransportadora;
	}

	public String getTranNmNometransportadora() {
		return tranNmNometransportadora;
	}

	public void setTranNmNometransportadora(String tranNmNometransportadora) {
		this.tranNmNometransportadora = tranNmNometransportadora;
	}

	public String getClieDsNomecliente() {
		return clieDsNomecliente;
	}

	public void setClieDsNomecliente(String clieDsNomecliente) {
		this.clieDsNomecliente = clieDsNomecliente;
	}

	public String getShipIdCdDeliverynote() {
		return shipIdCdDeliverynote;
	}

	public void setShipIdCdDeliverynote(String shipIdCdDeliverynote) {
		this.shipIdCdDeliverynote = shipIdCdDeliverynote;
	}

}
