package br.com.portalpluris.business;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.portalpluris.bean.CsCdtbTransportadoraEmail;
import br.com.portalpluris.dao.CsCdtbTransportadoraEmailDao;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CsCdtbTransportadoraEmailTrmlBusiness {

	public String insertTransEmail(String iduser, String codtransportadora,
			String razaosocial, String cnpj, String email) throws SQLException {
		
		
		CsCdtbTransportadoraEmailDao transDao = new CsCdtbTransportadoraEmailDao();
		
		CsCdtbTransportadoraEmail transBean = new CsCdtbTransportadoraEmail();
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dhImportacao = sdf.format(date);
		
		transBean.setTrml_ds_cnpj(cnpj);
		transBean.setTrml_ds_codigotransportadora(codtransportadora);
		transBean.setTrml_ds_email(email);
		transBean.setTrml_ds_inativo("N");
		transBean.setTrml_ds_razaoscoail(razaosocial);
		transBean.setTrml_id_transportadoraemail(email);
		
		String retorno = transDao.insertTransEmail(transBean, dhImportacao, "Inserido via Portal",iduser);

		return retorno;
	}

}
