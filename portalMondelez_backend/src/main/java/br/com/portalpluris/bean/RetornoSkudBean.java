package br.com.portalpluris.bean;

public class RetornoSkudBean {

	private String skud_id_cd_shipment;
	private String skud_id_cd_delivery;
	private String skud_ds_pesobruto;
	private String skud_ds_pesoliquido;
	private String skud_ds_codsku;
	private String skud_ds_itemsku;
	private String skud_ds_volumecubico;
	private String skud_nr_quantidade;
	private String skud_dh_delivery;
	private String skud_ds_pedidomondelez;
	private String skud_ds_pedidocliente;
	private String skud_ds_tipovenda;
	private String skud_dh_registro;
	private String skud_dh_atualizacao;
	private String skud_ds_valor;
	
	
	public String getSkud_id_cd_shipment() {
		return skud_id_cd_shipment;
	}
	public void setSkud_id_cd_shipment(String skud_id_cd_shipment) {
		this.skud_id_cd_shipment = skud_id_cd_shipment;
	}
	public String getSkud_id_cd_delivery() {
		return skud_id_cd_delivery;
	}
	public void setSkud_id_cd_delivery(String skud_id_cd_delivery) {
		this.skud_id_cd_delivery = skud_id_cd_delivery;
	}
	public String getSkud_ds_pesobruto() {
		return skud_ds_pesobruto;
	}
	public void setSkud_ds_pesobruto(String skud_ds_pesobruto) {
		this.skud_ds_pesobruto = skud_ds_pesobruto;
	}

	public String getSkud_ds_pesoliquido() {
		return skud_ds_pesoliquido;
	}
	public void setSkud_ds_pesoliquido(String skud_ds_pesoliquido) {
		this.skud_ds_pesoliquido = skud_ds_pesoliquido;
	}
	public String getSkud_ds_codsku() {
		return skud_ds_codsku;
	}
	public void setSkud_ds_codsku(String skud_ds_codsku) {
		this.skud_ds_codsku = skud_ds_codsku;
	}
	public String getSkud_ds_itemsku() {
		return skud_ds_itemsku;
	}
	public void setSkud_ds_itemsku(String skud_ds_itemsku) {
		this.skud_ds_itemsku = skud_ds_itemsku;
	}
	public String getSkud_ds_volumecubico() {
		return skud_ds_volumecubico;
	}
	public void setSkud_ds_volumecubico(String skud_ds_volumecubico) {
		this.skud_ds_volumecubico = skud_ds_volumecubico;
	}
	public String getSkud_nr_quantidade() {
		return skud_nr_quantidade;
	}
	public void setSkud_nr_quantidade(String skud_nr_quantidade) {
		this.skud_nr_quantidade = skud_nr_quantidade;
	}
	public String getSkud_dh_delivery() {
		return skud_dh_delivery;
	}
	public void setSkud_dh_delivery(String skud_dh_delivery) {
		this.skud_dh_delivery = skud_dh_delivery;
	}
	public String getSkud_ds_pedidomondelez() {
		return skud_ds_pedidomondelez;
	}
	public void setSkud_ds_pedidomondelez(String skud_ds_pedidomondelez) {
		this.skud_ds_pedidomondelez = skud_ds_pedidomondelez;
	}
	public String getSkud_ds_pedidocliente() {
		return skud_ds_pedidocliente;
	}
	public void setSkud_ds_pedidocliente(String skud_ds_pedidocliente) {
		this.skud_ds_pedidocliente = skud_ds_pedidocliente;
	}
	public String getSkud_ds_tipovenda() {
		return skud_ds_tipovenda;
	}
	public void setSkud_ds_tipovenda(String skud_ds_tipovenda) {
		this.skud_ds_tipovenda = skud_ds_tipovenda;
	}
	public String getSkud_dh_registro() {
		return skud_dh_registro;
	}
	public void setSkud_dh_registro(String skud_dh_registro) {
		this.skud_dh_registro = skud_dh_registro;
	}
	public String getSkud_dh_atualizacao() {
		return skud_dh_atualizacao;
	}
	public void setSkud_dh_atualizacao(String skud_dh_atualizacao) {
		this.skud_dh_atualizacao = skud_dh_atualizacao;
	}
	public String getSkud_ds_valor() {
		return skud_ds_valor;
	}
	public void setSkud_ds_valor(String skud_ds_valor) {
		this.skud_ds_valor = skud_ds_valor;
	}

	
	
	
	
}
