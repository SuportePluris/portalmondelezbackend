package br.com.portalpluris.bean;

import java.util.Date;

public class CsLgtbShipmentShipBean {
	
	// SHIP_CD_SHIPMENTID 
	private String ship_cd_shipmentid;
	
	// SHIP_DS_STOPTYPE 
	private String ship_ds_stoptype;
	
	// SHIP_DS_STOPNUMBER
	private String ship_ds_stopnumber;
	
	// SHIP_DS_TOTALSTOPS
	private String ship_ds_totalstops;
	
	// SHIP_CD_SERVICEPROVIDERID
	private String ship_cd_serviceproviderid;
	
	// SHIP_DS_SERVICEPROVIDERID
	private String ship_ds_serviceproviderid;
	
	// SHIP_DS_EQUIPMENTGROUP 
	private String ship_ds_equipmentgroup;
	
	// SHIP_DS_SOURCELOCATIONID
	private String ship_ds_sourcelocationid;
	
	// SHIP_DS_SOURCELOCATIONNAME
	private String ship_ds_sourcelocationname;
	
	// SHIP_DS_SOURCECITY
	private String ship_ds_sourcecity;
	
	// SHIP_DS_SOURCEPROVINCECODE
	private String ship_ds_sourceprovincecode;
	
	// SHIP_DS_SOURCEZONE1
	private String ship_ds_sourcezone1;
	
	// SHIP_DS_DESTLOCATIONID
	private String ship_ds_destlocationid;
	
	// SHIP_DS_DESTLOCATIONNAME
	private String ship_ds_destlocationname;
	
	// SHIP_DS_DESTCITY
	private String ship_ds_destcity;
	
	// SHIP_DS_PROVINCECODE 
	private String ship_ds_provincecode;
	
	// SHIP_DS_DESTZONE1
	private String ship_ds_destzone1;
	
	// SHIP_DS_STOPDISTANCE
	private String ship_ds_stopdistance;
	
	// SHIP_DS_STOPDISTANCEUOM
	private String ship_ds_stopdistanceuom;
	
	// SHIP_DS_STARTTIME
	private Date ship_ds_starttime;
	
	// SHIP_DS_WEEK 
	private String ship_ds_week;
	
	// SHIP_DS_MONTH
	private String ship_ds_month;
	
	// SHIP_DS_DECLAREDVALUE
	private String ship_ds_declaredvalue;
	
	// SHIP_DS_NETFREIGHTAMOUNT 
	private String ship_ds_netfreightamount;
	
	// SHIP_DS_NUMBERORDERRELEASES 
	private String ship_ds_numberorderreleases;
	
	// SHIP_DS_GROSSWEIGHT 
	private String ship_ds_grossweight;
	
	// SHIP_DS_TOTALGROSSWEIGHT
	private String ship_ds_totalgrossweight;
	
	// SHIP_DS_GROSSVOLUME
	private String ship_ds_grossvolume;
	
	// SHIP_DS_TOTALGROSSVOLUME
	private String ship_ds_totalgrossvolume;
	
	// SHIP_DS_CUBEDWEIGHT 
	private String ship_ds_cubedweight;
	
	// SHIP_DS_CASES 
	private String ship_ds_cases;
	
	// SHIP_DS_VOLUMEVEHICLECAPACITY
	private String ship_ds_volumevehiclecapacity;
	
	// SHIP_DS_VOLUMEVEHICLECAPCTYUOMCODE
	private String ship_ds_volumevehiclecapctyuomcode;
	
	// SHIP_DS_WEIGHTVEHICLECAPACITY 
	private String ship_ds_weightvehiclecapacity;
	
	// SHIP_DS_WEIGHTVEHICLECAPCTYWOMCODE
	private String ship_ds_weightvehiclecapctywomcode;
	
	// SHIP_DS_VOLUMEVEHICLEOCCUPANCY 
	private String ship_ds_volumevehicleoccupancy;
	
	// SHIP_DS_WEIGHTVEHICLEOCCUPANCY
	private String ship_ds_weightvehicleoccupancy;
	
	// SHIP_DS_CORECARRIER 
	private String ship_ds_corecarrier;
	
	// SHIP_DS_PLANNEDSCAC
	private String ship_ds_plannedscac;
	
	// SHIP_DS_PLANNEDSERVICEPROVIDERNAME
	private String ship_ds_plannedserviceprovidername;
	
	// SHIP_DS_BULKPLANGID
	private String ship_ds_bulkplangid;
	
	// SHIP_DS_STATUSOFERTA
	private String ship_ds_statusoferta;
	
	// SHIP_DS_BUYSHIPMENT
	private String ship_ds_buyshipment;
	
	// SHIP_DS_TRUCKOUCARRETA
	private String ship_ds_truckoucarreta;
	
	// SHIP_DS_CLIENTEAGENDADO
	private String ship_ds_clienteagendado;
	
	// SHIP_DS_AGENDAMENTOCOMNF
	private String ship_ds_agendamentocomnf;
	
	// SHIP_DS_TIPO
	private String ship_ds_tipo;
	
	// SHIP_DS_TRANSPORTADORA
	private String ship_ds_transportadora;
	
	// SHIP_DS_CONTANDESHIPMENTS
	private String ship_ds_contandeshipments;
	
	// SHIP_DS_FAIXA
	private String ship_ds_faixa;
	
	// SHIP_DS_CHAVESHIPMENT
	private String ship_ds_chaveshipment;
	
	// SHIP_DS_SHIPMENTS
	private String ship_ds_shipments;
	
	// SHIP_DS_PLANTA 
	private String ship_ds_planta;
	
	// SHIP_CD_CODCLIENTE
	private String ship_cd_codcliente;
	
	// SHIP_DS_CLIENTE
	private String ship_ds_cliente;
	
	// SHIP_DS_PESO
	private String ship_ds_peso;
	
	// SHIP_DS_LIBERARCROSSDOCKING
	private String ship_ds_liberarcrossdocking;
	 
	// SHIP_DH_REGISTRO
	private Date ship_dh_registro;
	
	// SHIP_DS_STATUSPROCESSAMENTO
	private String ship_ds_statusprocessamento;
	
	// SHIP_DH_PROCESSAMENTO 
	private Date ship_dh_processamento;
	
	// SHIP_DS_NOMEARQUIVO 
	private String ship_ds_nomearquivo;

	public String getShip_cd_shipmentid() {
		return ship_cd_shipmentid;
	}

	public void setShip_cd_shipmentid(String ship_cd_shipmentid) {
		this.ship_cd_shipmentid = ship_cd_shipmentid;
	}

	public String getShip_ds_stoptype() {
		return ship_ds_stoptype;
	}

	public void setShip_ds_stoptype(String ship_ds_stoptype) {
		this.ship_ds_stoptype = ship_ds_stoptype;
	}

	public String getShip_ds_stopnumber() {
		return ship_ds_stopnumber;
	}

	public void setShip_ds_stopnumber(String ship_ds_stopnumber) {
		this.ship_ds_stopnumber = ship_ds_stopnumber;
	}

	public String getShip_ds_totalstops() {
		return ship_ds_totalstops;
	}

	public void setShip_ds_totalstops(String ship_ds_totalstops) {
		this.ship_ds_totalstops = ship_ds_totalstops;
	}

	public String getShip_cd_serviceproviderid() {
		return ship_cd_serviceproviderid;
	}

	public void setShip_cd_serviceproviderid(String ship_cd_serviceproviderid) {
		this.ship_cd_serviceproviderid = ship_cd_serviceproviderid;
	}

	public String getShip_ds_serviceproviderid() {
		return ship_ds_serviceproviderid;
	}

	public void setShip_ds_serviceproviderid(String ship_ds_serviceproviderid) {
		this.ship_ds_serviceproviderid = ship_ds_serviceproviderid;
	}

	public String getShip_ds_equipmentgroup() {
		return ship_ds_equipmentgroup;
	}

	public void setShip_ds_equipmentgroup(String ship_ds_equipmentgroup) {
		this.ship_ds_equipmentgroup = ship_ds_equipmentgroup;
	}

	public String getShip_ds_sourcelocationid() {
		return ship_ds_sourcelocationid;
	}

	public void setShip_ds_sourcelocationid(String ship_ds_sourcelocationid) {
		this.ship_ds_sourcelocationid = ship_ds_sourcelocationid;
	}

	public String getShip_ds_sourcelocationname() {
		return ship_ds_sourcelocationname;
	}

	public void setShip_ds_sourcelocationname(String ship_ds_sourcelocationname) {
		this.ship_ds_sourcelocationname = ship_ds_sourcelocationname;
	}

	public String getShip_ds_sourcecity() {
		return ship_ds_sourcecity;
	}

	public void setShip_ds_sourcecity(String ship_ds_sourcecity) {
		this.ship_ds_sourcecity = ship_ds_sourcecity;
	}

	public String getShip_ds_sourceprovincecode() {
		return ship_ds_sourceprovincecode;
	}

	public void setShip_ds_sourceprovincecode(String ship_ds_sourceprovincecode) {
		this.ship_ds_sourceprovincecode = ship_ds_sourceprovincecode;
	}

	public String getShip_ds_sourcezone1() {
		return ship_ds_sourcezone1;
	}

	public void setShip_ds_sourcezone1(String ship_ds_sourcezone1) {
		this.ship_ds_sourcezone1 = ship_ds_sourcezone1;
	}

	public String getShip_ds_destlocationid() {
		return ship_ds_destlocationid;
	}

	public void setShip_ds_destlocationid(String ship_ds_destlocationid) {
		this.ship_ds_destlocationid = ship_ds_destlocationid;
	}

	public String getShip_ds_destlocationname() {
		return ship_ds_destlocationname;
	}

	public void setShip_ds_destlocationname(String ship_ds_destlocationname) {
		this.ship_ds_destlocationname = ship_ds_destlocationname;
	}

	public String getShip_ds_destcity() {
		return ship_ds_destcity;
	}

	public void setShip_ds_destcity(String ship_ds_destcity) {
		this.ship_ds_destcity = ship_ds_destcity;
	}

	public String getShip_ds_provincecode() {
		return ship_ds_provincecode;
	}

	public void setShip_ds_provincecode(String ship_ds_provincecode) {
		this.ship_ds_provincecode = ship_ds_provincecode;
	}

	public String getShip_ds_destzone1() {
		return ship_ds_destzone1;
	}

	public void setShip_ds_destzone1(String ship_ds_destzone1) {
		this.ship_ds_destzone1 = ship_ds_destzone1;
	}

	public String getShip_ds_stopdistance() {
		return ship_ds_stopdistance;
	}

	public void setShip_ds_stopdistance(String ship_ds_stopdistance) {
		this.ship_ds_stopdistance = ship_ds_stopdistance;
	}

	public String getShip_ds_stopdistanceuom() {
		return ship_ds_stopdistanceuom;
	}

	public void setShip_ds_stopdistanceuom(String ship_ds_stopdistanceuom) {
		this.ship_ds_stopdistanceuom = ship_ds_stopdistanceuom;
	}

	public Date getShip_ds_starttime() {
		return ship_ds_starttime;
	}

	public void setShip_ds_starttime(Date ship_ds_starttime) {
		this.ship_ds_starttime = ship_ds_starttime;
	}

	public String getShip_ds_week() {
		return ship_ds_week;
	}

	public void setShip_ds_week(String ship_ds_week) {
		this.ship_ds_week = ship_ds_week;
	}

	public String getShip_ds_month() {
		return ship_ds_month;
	}

	public void setShip_ds_month(String ship_ds_month) {
		this.ship_ds_month = ship_ds_month;
	}

	public String getShip_ds_declaredvalue() {
		return ship_ds_declaredvalue;
	}

	public void setShip_ds_declaredvalue(String ship_ds_declaredvalue) {
		this.ship_ds_declaredvalue = ship_ds_declaredvalue;
	}

	public String getShip_ds_netfreightamount() {
		return ship_ds_netfreightamount;
	}

	public void setShip_ds_netfreightamount(String ship_ds_netfreightamount) {
		this.ship_ds_netfreightamount = ship_ds_netfreightamount;
	}

	public String getShip_ds_numberorderreleases() {
		return ship_ds_numberorderreleases;
	}

	public void setShip_ds_numberorderreleases(String ship_ds_numberorderreleases) {
		this.ship_ds_numberorderreleases = ship_ds_numberorderreleases;
	}

	public String getShip_ds_grossweight() {
		return ship_ds_grossweight;
	}

	public void setShip_ds_grossweight(String ship_ds_grossweight) {
		this.ship_ds_grossweight = ship_ds_grossweight;
	}

	public String getShip_ds_totalgrossweight() {
		return ship_ds_totalgrossweight;
	}

	public void setShip_ds_totalgrossweight(String ship_ds_totalgrossweight) {
		this.ship_ds_totalgrossweight = ship_ds_totalgrossweight;
	}

	public String getShip_ds_grossvolume() {
		return ship_ds_grossvolume;
	}

	public void setShip_ds_grossvolume(String ship_ds_grossvolume) {
		this.ship_ds_grossvolume = ship_ds_grossvolume;
	}

	public String getShip_ds_totalgrossvolume() {
		return ship_ds_totalgrossvolume;
	}

	public void setShip_ds_totalgrossvolume(String ship_ds_totalgrossvolume) {
		this.ship_ds_totalgrossvolume = ship_ds_totalgrossvolume;
	}

	public String getShip_ds_cubedweight() {
		return ship_ds_cubedweight;
	}

	public void setShip_ds_cubedweight(String ship_ds_cubedweight) {
		this.ship_ds_cubedweight = ship_ds_cubedweight;
	}

	public String getShip_ds_cases() {
		return ship_ds_cases;
	}

	public void setShip_ds_cases(String ship_ds_cases) {
		this.ship_ds_cases = ship_ds_cases;
	}

	public String getShip_ds_volumevehiclecapacity() {
		return ship_ds_volumevehiclecapacity;
	}

	public void setShip_ds_volumevehiclecapacity(
			String ship_ds_volumevehiclecapacity) {
		this.ship_ds_volumevehiclecapacity = ship_ds_volumevehiclecapacity;
	}

	public String getShip_ds_volumevehiclecapctyuomcode() {
		return ship_ds_volumevehiclecapctyuomcode;
	}

	public void setShip_ds_volumevehiclecapctyuomcode(
			String ship_ds_volumevehiclecapctyuomcode) {
		this.ship_ds_volumevehiclecapctyuomcode = ship_ds_volumevehiclecapctyuomcode;
	}

	public String getShip_ds_weightvehiclecapacity() {
		return ship_ds_weightvehiclecapacity;
	}

	public void setShip_ds_weightvehiclecapacity(
			String ship_ds_weightvehiclecapacity) {
		this.ship_ds_weightvehiclecapacity = ship_ds_weightvehiclecapacity;
	}

	public String getShip_ds_weightvehiclecapctywomcode() {
		return ship_ds_weightvehiclecapctywomcode;
	}

	public void setShip_ds_weightvehiclecapctywomcode(
			String ship_ds_weightvehiclecapctywomcode) {
		this.ship_ds_weightvehiclecapctywomcode = ship_ds_weightvehiclecapctywomcode;
	}

	public String getShip_ds_volumevehicleoccupancy() {
		return ship_ds_volumevehicleoccupancy;
	}

	public void setShip_ds_volumevehicleoccupancy(
			String ship_ds_volumevehicleoccupancy) {
		this.ship_ds_volumevehicleoccupancy = ship_ds_volumevehicleoccupancy;
	}

	public String getShip_ds_weightvehicleoccupancy() {
		return ship_ds_weightvehicleoccupancy;
	}

	public void setShip_ds_weightvehicleoccupancy(
			String ship_ds_weightvehicleoccupancy) {
		this.ship_ds_weightvehicleoccupancy = ship_ds_weightvehicleoccupancy;
	}

	public String getShip_ds_corecarrier() {
		return ship_ds_corecarrier;
	}

	public void setShip_ds_corecarrier(String ship_ds_corecarrier) {
		this.ship_ds_corecarrier = ship_ds_corecarrier;
	}

	public String getShip_ds_plannedscac() {
		return ship_ds_plannedscac;
	}

	public void setShip_ds_plannedscac(String ship_ds_plannedscac) {
		this.ship_ds_plannedscac = ship_ds_plannedscac;
	}

	public String getShip_ds_plannedserviceprovidername() {
		return ship_ds_plannedserviceprovidername;
	}

	public void setShip_ds_plannedserviceprovidername(
			String ship_ds_plannedserviceprovidername) {
		this.ship_ds_plannedserviceprovidername = ship_ds_plannedserviceprovidername;
	}

	public String getShip_ds_bulkplangid() {
		return ship_ds_bulkplangid;
	}

	public void setShip_ds_bulkplangid(String ship_ds_bulkplangid) {
		this.ship_ds_bulkplangid = ship_ds_bulkplangid;
	}

	public String getShip_ds_statusoferta() {
		return ship_ds_statusoferta;
	}

	public void setShip_ds_statusoferta(String ship_ds_statusoferta) {
		this.ship_ds_statusoferta = ship_ds_statusoferta;
	}

	public String getShip_ds_buyshipment() {
		return ship_ds_buyshipment;
	}

	public void setShip_ds_buyshipment(String ship_ds_buyshipment) {
		this.ship_ds_buyshipment = ship_ds_buyshipment;
	}

	public String getShip_ds_truckoucarreta() {
		return ship_ds_truckoucarreta;
	}

	public void setShip_ds_truckoucarreta(String ship_ds_truckoucarreta) {
		this.ship_ds_truckoucarreta = ship_ds_truckoucarreta;
	}

	public String getShip_ds_clienteagendado() {
		return ship_ds_clienteagendado;
	}

	public void setShip_ds_clienteagendado(String ship_ds_clienteagendado) {
		this.ship_ds_clienteagendado = ship_ds_clienteagendado;
	}

	public String getShip_ds_agendamentocomnf() {
		return ship_ds_agendamentocomnf;
	}

	public void setShip_ds_agendamentocomnf(String ship_ds_agendamentocomnf) {
		this.ship_ds_agendamentocomnf = ship_ds_agendamentocomnf;
	}

	public String getShip_ds_tipo() {
		return ship_ds_tipo;
	}

	public void setShip_ds_tipo(String ship_ds_tipo) {
		this.ship_ds_tipo = ship_ds_tipo;
	}

	public String getShip_ds_transportadora() {
		return ship_ds_transportadora;
	}

	public void setShip_ds_transportadora(String ship_ds_transportadora) {
		this.ship_ds_transportadora = ship_ds_transportadora;
	}

	public String getShip_ds_contandeshipments() {
		return ship_ds_contandeshipments;
	}

	public void setShip_ds_contandeshipments(String ship_ds_contandeshipments) {
		this.ship_ds_contandeshipments = ship_ds_contandeshipments;
	}

	public String getShip_ds_faixa() {
		return ship_ds_faixa;
	}

	public void setShip_ds_faixa(String ship_ds_faixa) {
		this.ship_ds_faixa = ship_ds_faixa;
	}

	public String getShip_ds_chaveshipment() {
		return ship_ds_chaveshipment;
	}

	public void setShip_ds_chaveshipment(String ship_ds_chaveshipment) {
		this.ship_ds_chaveshipment = ship_ds_chaveshipment;
	}

	public String getShip_ds_shipments() {
		return ship_ds_shipments;
	}

	public void setShip_ds_shipments(String ship_ds_shipments) {
		this.ship_ds_shipments = ship_ds_shipments;
	}

	public String getShip_ds_planta() {
		return ship_ds_planta;
	}

	public void setShip_ds_planta(String ship_ds_planta) {
		this.ship_ds_planta = ship_ds_planta;
	}

	public String getShip_cd_codcliente() {
		return ship_cd_codcliente;
	}

	public void setShip_cd_codcliente(String ship_cd_codcliente) {
		this.ship_cd_codcliente = ship_cd_codcliente;
	}

	public String getShip_ds_cliente() {
		return ship_ds_cliente;
	}

	public void setShip_ds_cliente(String ship_ds_cliente) {
		this.ship_ds_cliente = ship_ds_cliente;
	}

	public String getShip_ds_peso() {
		return ship_ds_peso;
	}

	public void setShip_ds_peso(String ship_ds_peso) {
		this.ship_ds_peso = ship_ds_peso;
	}

	public String getShip_ds_liberarcrossdocking() {
		return ship_ds_liberarcrossdocking;
	}

	public void setShip_ds_liberarcrossdocking(String ship_ds_liberarcrossdocking) {
		this.ship_ds_liberarcrossdocking = ship_ds_liberarcrossdocking;
	}

	public Date getShip_dh_registro() {
		return ship_dh_registro;
	}

	public void setShip_dh_registro(Date ship_dh_registro) {
		this.ship_dh_registro = ship_dh_registro;
	}

	public String getShip_ds_statusprocessamento() {
		return ship_ds_statusprocessamento;
	}

	public void setShip_ds_statusprocessamento(String ship_ds_statusprocessamento) {
		this.ship_ds_statusprocessamento = ship_ds_statusprocessamento;
	}

	public Date getShip_dh_processamento() {
		return ship_dh_processamento;
	}

	public void setShip_dh_processamento(Date ship_dh_processamento) {
		this.ship_dh_processamento = ship_dh_processamento;
	}

	public String getShip_ds_nomearquivo() {
		return ship_ds_nomearquivo;
	}

	public void setShip_ds_nomearquivo(String ship_ds_nomearquivo) {
		this.ship_ds_nomearquivo = ship_ds_nomearquivo;
	}	

}
