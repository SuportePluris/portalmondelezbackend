package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import br.com.catapi.util.UtilMoveArquivo;
import br.com.portalpluris.bean.CsLgtbHierarquiaHierBean;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsLgtbHierarquiaHierDao;

public class CsLgtbHierarquiaHierBusiness {

	/**
	 * Metodo responsavel por realizar leitura e gravacao das informacoes de
	 * hierarquia disponibilizadas via arquivo TXT no banco de dados
	 * 
	 * @author Guilherme Sabino
	 */

	public void gravarHierarquia() {

		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("[gravarHierarquia] - INICIO ");

		try {

			CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
			UtilMoveArquivo utilMove = new UtilMoveArquivo();

			// CONFIGURACOES DE DIRETORIOS
			String dirHierarquia = confDao.slctConf("conf.importacao.cadastro.diretorio.hierarquia");
			String diretorioBkp = confDao.slctConf("conf.importacao.cadastro.diretorio.hierarquia.bkp");
			String diretorioErro = confDao.slctConf("conf.importacao.cadastro.diretorio.erro");

			// LEITURA DOS ARQUIVOS DO DIRETORIO
			File arquivo = new File(dirHierarquia);
			String[] arq = arquivo.list();

			if (arq != null) {

				CsLgtbHierarquiaHierBean arqRet = new CsLgtbHierarquiaHierBean();

				log.info("[gravarHierarquia] - CARREGANDO ARQUIVOS TXT ");
				log.info(" ******** " + arq.length + " arquivo(s) carregado(s). ******** ");

				for (int i = 0; i < arq.length; i++) {

					// ARQUIVO A SER PROCESSADO
					File arquivoProcessado = new File(dirHierarquia + "/" + arq[i]);

					try {

						if (arquivoProcessado.getName().toUpperCase().endsWith(".TXT")) {

							String nmArquivo = arquivoProcessado.getName();
							log.info("INICIANDO LEITURA DO ARQUIVO TXT: " + nmArquivo);

							// INSTANCIAR ARQUIVO
							FileInputStream fileInputStream = new FileInputStream(arquivoProcessado);
							BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));

							// CONTROLES DO ARQUIVO
							String conteudo = "";
							int linha = 0;

							// FORMATANDO DATA
							Date date = new Date();
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
							String dhProcessamento = sdf.format(date);

							// FOR DAS LINHAS DO ARQUIVO
							while (((conteudo = bufferedReader.readLine()) != null)) {

								arqRet = new CsLgtbHierarquiaHierBean();

								try {

									linha++;
									log.info("LENDO CONTEUDO DO ARQUIVO ");
									log.info("CONTEUDO DA LINHA: " + linha + "" + conteudo);

									// IDENTIFICAR FINAL DO ARQUIVO
									if (conteudo.substring(0, 10).indexOf("TOTAL") > -1) {

										// FIM DO ARQUIVO IDENTIFICADO
										log.info("FIM DO ARQUIVO IDENTIFICADO");

									} else {

										// LEITURA DOS CAMPOS
										// OS VALORES AO LADO REPRESENTAM O TAMANHO DO CAMPO
										// NÃO HÁ UM DELIMITADOR, SOMENTE PELO TAMANHO DA STRING.

										arqRet.setHier_cd_hierarquiacode(conteudo.substring(1, 1).trim()); // 01
										arqRet.setHier_ds_constant(conteudo.substring(2, 21).trim()); // 20
										arqRet.setHier_ds_saleshierarchy(conteudo.substring(22, 31).trim()); // 09
										arqRet.setHier_ds_description(conteudo.substring(32, 66).trim()); // 34
										arqRet.setHier_ds_saleshierarchyregion(conteudo.substring(67, 76).trim()); // 09
										arqRet.setHier_ds_description2(conteudo.substring(77, 111).trim()); // 34
										arqRet.setHier_ds_salesareahierarchy(conteudo.substring(112, 121).trim()); // 09
										arqRet.setHier_ds_description3(conteudo.substring(122, 156).trim()); // 34
										arqRet.setHier_ds_territorysaleshierarchy(conteudo.substring(157, 166).trim()); // 09
										arqRet.setHier_ds_namealpha(conteudo.substring(167, 201).trim()); // 34
										arqRet.setHier_ds_addressnumbersalesman(conteudo.substring(202, 236).trim()); // 34
										arqRet.setHier_ds_customercode(conteudo.substring(237, 246).trim()); // 09

										// REALIZAR O INSERT DAS LINHAS NO BANCO
										CsLgtbHierarquiaHierDao HierarquiaHierDao = new CsLgtbHierarquiaHierDao();
										HierarquiaHierDao.insertHierarquia(arqRet, nmArquivo, dhProcessamento);

									}

								} catch (Exception e) {

									log.info("ERRO NA LEITURA DA LINHA:" + linha);
									log.info("Erro:" + e);
									e.printStackTrace();

								}

							}

							// MOVER ARQUIVO PARA PASTA BKP
							File arquivoProcessadoBkp = new File(diretorioBkp + "/" + arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoBkp);
						}

					} catch (Exception e) {

						log.info("[gravarShipment] - ERRO AO LER O ARQUIVO HIERARQUIA ");

						// MOVER ARQUIVO PARA PASTA DE ERRO
						if (arq != null) {
							File arquivoProcessadoErro = new File(diretorioErro + "/" + arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoErro);
						}

					}
				}

			}

		} catch (Exception e) {
			log.info(" [gravarHierarquia] - ERRO AO GRAVAR HIERARQUIA ");
		}
	}

}
