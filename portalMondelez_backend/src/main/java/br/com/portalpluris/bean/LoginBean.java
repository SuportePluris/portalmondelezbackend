package br.com.portalpluris.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginBean {

	long idFuncCdFuncionario = 0;
	String funcDsLoginname;
	String funcNmFuncionario;
	String funcDsPassword;
	String funcDsPermissao;
	String dsEmpresa;
	boolean isLoginValidated = false;
	int idEmbaCdEmpresabanco;
	int idChamCdChamado;
	int maniNrSequencia;

	public int getIdEmbaCdEmpresabanco() {
		return idEmbaCdEmpresabanco;
	}

	public void setIdEmbaCdEmpresabanco(int idEmbaCdEmpresabanco) {
		this.idEmbaCdEmpresabanco = idEmbaCdEmpresabanco;
	}

	public int getIdChamCdChamado() {
		return idChamCdChamado;
	}

	public void setIdChamCdChamado(int idChamCdChamado) {
		this.idChamCdChamado = idChamCdChamado;
	}

	public int getManiNrSequencia() {
		return maniNrSequencia;
	}

	public void setManiNrSequencia(int maniNrSequencia) {
		this.maniNrSequencia = maniNrSequencia;
	}

	public long getIdFuncCdFuncionario() {
		return idFuncCdFuncionario;
	}

	public void setIdFuncCdFuncionario(long idFuncCdFuncionario) {
		this.idFuncCdFuncionario = idFuncCdFuncionario;
	}

	public String getFuncDsLoginname() {
		return funcDsLoginname;
	}

	public void setFuncDsLoginname(String funcDsLoginname) {
		this.funcDsLoginname = funcDsLoginname;
	}

	public String getFuncNmFuncionario() {
		return funcNmFuncionario;
	}

	public void setFuncNmFuncionario(String funcNmFuncionario) {
		this.funcNmFuncionario = funcNmFuncionario;
	}

	public String getFuncDsPassword() {
		return funcDsPassword;
	}

	public void setFuncDsPassword(String funcDsPassword) {
		this.funcDsPassword = funcDsPassword;
	}

	public String getFuncDsPermissao() {
		return funcDsPermissao;
	}

	public void setFuncDsPermissao(String funcDsPermissao) {
		this.funcDsPermissao = funcDsPermissao;
	}

	public boolean isLoginValidated() {
		return isLoginValidated;
	}

	public void setLoginValidated(boolean isLoginValidated) {
		this.isLoginValidated = isLoginValidated;
	}

	public String getDsEmpresa() {
		return dsEmpresa;
	}

	public void setDsEmpresa(String dsEmpresa) {
		this.dsEmpresa = dsEmpresa;
	}

}
