package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.CsCdtbResponsavelRespBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsCdtbResponsavelRespDao {
	
	public RetornoBean createUpdateResp(CsCdtbResponsavelRespBean respBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		if (respBean.getResp_id_cd_responsavel() > 0) {
			retBean = updateResp(respBean);

			return retBean;
		}

		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		
//		if(respBean.getResp_in_inativo().equalsIgnoreCase("S")) {
			pst = conn.prepareStatement("" 	+ " INSERT INTO CS_CDTB_RESPONSAVEL_RESP ( " 
											+ "		RESP_NM_RESPONSAVEL, "
											+ "		RESP_IN_INATIVO "
											+ " ) VALUES ( ?, ? ) ");

			pst.setString(1, respBean.getResp_nm_responsavel());
			pst.setString(2, respBean.getResp_in_inativo());
			
//		}else {
//			pst = conn.prepareStatement("" 	+ " INSERT INTO CS_CDTB_RESPONSAVEL_RESP ( " 
//											+ " 		RESP_NM_RESPONSAVEL "
//											+ " ) VALUES ( ? ) ");
//
//			pst.setString(1, respBean.getResp_nm_responsavel());
//		}

		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Responsavel criado com sucesso");

		return retBean;
	}
	
	private RetornoBean updateResp(CsCdtbResponsavelRespBean respBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		String queryUpdate = " UPDATE CS_CDTB_RESPONSAVEL_RESP SET ";

		if (respBean.getResp_nm_responsavel() != null && respBean.getResp_nm_responsavel() != "") {
			queryUpdate += " RESP_NM_RESPONSAVEL = '" + respBean.getResp_nm_responsavel() + "'";
		}

		if (respBean.getResp_in_inativo() != null && respBean.getResp_in_inativo() != "") {
			queryUpdate += ", RESP_IN_INATIVO = '" + respBean.getResp_in_inativo() + "'";
		}

		queryUpdate += " WHERE RESP_ID_CD_RESPONSAVEL = " + respBean.getResp_id_cd_responsavel();

		pst = conn.prepareStatement(queryUpdate);
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Responsavel atualizado com sucesso");

		return retBean;
	}
	
	public CsCdtbResponsavelRespBean getResp(int resp_id_cd_responsavel) throws SQLException {

		CsCdtbResponsavelRespBean respBean = new CsCdtbResponsavelRespBean();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = ""	+ " SELECT " + "		RESP_ID_CD_RESPONSAVEL, " 
							+ "		RESP_NM_RESPONSAVEL, " + "		RESP_DH_REGISTRO, " 
							+ "		RESP_IN_INATIVO " + " FROM " 
							+ "		CS_CDTB_RESPONSAVEL_RESP (NOLOCK) "
							+ " WHERE " + "		RESP_ID_CD_RESPONSAVEL = ?";

		pst = conn.prepareStatement(query);
		pst.setInt(1, resp_id_cd_responsavel);

		rs = pst.executeQuery();

		while (rs.next()) {

			respBean = setbean(rs);
		}

		return respBean;
	}

	public List<CsCdtbResponsavelRespBean> getLisResponsavel(String inativo) throws SQLException {

		List<CsCdtbResponsavelRespBean> list = new ArrayList<CsCdtbResponsavelRespBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" 
				+ " SELECT "
				+ " 	RESP_ID_CD_RESPONSAVEL, "
				+ " 	RESP_NM_RESPONSAVEL, "	
				+ " 	RESP_IN_INATIVO, "
				+ " 	RESP_DH_REGISTRO "
				+ " FROM "
				+ " 	CS_CDTB_RESPONSAVEL_RESP  (NOLOCK)"
				+ " WHERE	"
				+ " 	RESP_IN_INATIVO = '" + inativo +"'"
				+ " ORDER BY "
				+ "		RESP_NM_RESPONSAVEL ";

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbResponsavelRespBean bean = setbean(rs);

			list.add(bean);
		}

		return list;
	}

	private CsCdtbResponsavelRespBean setbean(ResultSet rs) throws SQLException {

		CsCdtbResponsavelRespBean bean = new CsCdtbResponsavelRespBean();

		bean.setResp_id_cd_responsavel(rs.getInt("RESP_ID_CD_RESPONSAVEL"));
		bean.setResp_nm_responsavel(rs.getString("RESP_NM_RESPONSAVEL"));
		bean.setResp_in_inativo(rs.getString("RESP_IN_INATIVO"));
		bean.setResp_dh_registro(rs.getString("RESP_DH_REGISTRO"));
		
		return bean;
	}

}
