package br.com.portalpluris.bean;

public class CsCdtbTransportadoraEmail {
	
	private String trml_id_transportadoraemail;
	private String trml_ds_codigotransportadora;
	private String trml_ds_razaoscoail;
	private String trml_ds_cnpj;
	private String trml_ds_email;
	private String trml_ds_inativo;
	private String trml_dh_importacao;
	private String trml_id_usuarioatualizacao;
	private String trml_nm_arquivoprocessado;
	
	
	
	public String getTrml_id_transportadoraemail() {
		return trml_id_transportadoraemail;
	}
	public void setTrml_id_transportadoraemail(String trml_id_transportadoraemail) {
		this.trml_id_transportadoraemail = trml_id_transportadoraemail;
	}
	public String getTrml_ds_razaoscoail() {
		return trml_ds_razaoscoail;
	}
	public void setTrml_ds_razaoscoail(String trml_ds_razaoscoail) {
		this.trml_ds_razaoscoail = trml_ds_razaoscoail;
	}
	public String getTrml_ds_cnpj() {
		return trml_ds_cnpj;
	}
	public void setTrml_ds_cnpj(String trml_ds_cnpj) {
		this.trml_ds_cnpj = trml_ds_cnpj;
	}
	public String getTrml_nm_arquivoprocessado() {
		return trml_nm_arquivoprocessado;
	}
	public void setTrml_nm_arquivoprocessado(String trml_nm_arquivoprocessado) {
		this.trml_nm_arquivoprocessado = trml_nm_arquivoprocessado;
	}
	public String getTrml_ds_codigotransportadora() {
		return trml_ds_codigotransportadora;
	}
	public void setTrml_ds_codigotransportadora(String trml_ds_codigotransportadora) {
		this.trml_ds_codigotransportadora = trml_ds_codigotransportadora;
	}
	
	public String getTrml_ds_email() {
		return trml_ds_email;
	}
	public void setTrml_ds_email(String trml_ds_email) {
		this.trml_ds_email = trml_ds_email;
	}
	public String getTrml_ds_inativo() {
		return trml_ds_inativo;
	}
	public void setTrml_ds_inativo(String trml_ds_inativo) {
		this.trml_ds_inativo = trml_ds_inativo;
	}
	public String getTrml_dh_importacao() {
		return trml_dh_importacao;
	}
	public void setTrml_dh_importacao(String trml_dh_importacao) {
		this.trml_dh_importacao = trml_dh_importacao;
	}
	public String getTrml_id_usuarioatualizacao() {
		return trml_id_usuarioatualizacao;
	}
	public void setTrml_id_usuarioatualizacao(String trml_id_usuarioatualizacao) {
		this.trml_id_usuarioatualizacao = trml_id_usuarioatualizacao;
	}
	
	
	
	

}
