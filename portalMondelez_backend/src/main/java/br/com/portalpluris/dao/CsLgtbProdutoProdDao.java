package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbProdutoProdBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;

public class CsLgtbProdutoProdDao {
	
	public void insertProduto(CsLgtbProdutoProdBean arqRet, String nmArquivo, String dhProcessamento) {
			
			Logger log = Logger.getLogger(this.getClass().getName());
			//DADOS DE LOG
			EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
			DAOLog daoLog = new DAOLog();

			logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
			logsBean.setLogsDsClasse("CsLgtbProdutoProdDao");
			logsBean.setLogsDsMetodo("insertProduto");
			logsBean.setLogsInErro("N");
			
			log.info(" ***** INICIO DO INSERT ***** ");				
			
			try{
				
			DAO dao = new DAO();
			Connection con = dao.getConexao();				
				
				PreparedStatement pst = con.prepareStatement("");			
			
				pst = con.prepareStatement(""
						+ "		INSERT INTO CS_LGTB_PRODUTO_PROD ("
							+ " PROD_CD_MATERIALCODE,"
							+ " PROD_DS_MATERIALDESCRIPTION,"
							+ " PROD_DS_DIVISIONDESCRIPTION,"
							+ " PROD_DS_STATUSPROCESSAMENTO,"
							+ " PROD_DH_PROCESSAMENTO,"
							+ " PROD_DS_NOMEARQUIVO"
						+ ") VALUES ( ?, ?, ?, ?, ?, ? )" // 06
						+ "" );
			
					pst.setString(1, arqRet.getProd_cd_materialcode().trim());
					pst.setString(2, arqRet.getProd_ds_materialdescription().trim());
					pst.setString(3, arqRet.getProd_ds_divisiondescription().trim());
					pst.setLong(4, 'N');
					pst.setString(5, dhProcessamento);
					pst.setString(6, nmArquivo);
					
					pst.execute();
				
					log.info(" ***** FIM DO INSERT ***** ");	
				
			}catch(Exception e){
				
				log.info(" ***** ERRO NO INSERT NA BASE DE DADOS" + e  + " ***** ");	
				e.printStackTrace();
				logsBean.setLogsInErro("S");
				logsBean.setLogsTxErro(e.getMessage());
		
			}finally{
				
				daoLog.createLog(logsBean);
			}
		}
	}
