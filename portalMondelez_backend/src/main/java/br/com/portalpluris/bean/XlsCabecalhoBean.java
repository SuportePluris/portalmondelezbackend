package br.com.portalpluris.bean;

public class XlsCabecalhoBean {
	
	private String cncl_ds_cnpj;
	private String cncl_nm_razaosocial;
	private String cncl_ds_chanel;
	private String clie_ds_nomecliente;
	private String clie_ds_cidade;
	private String clie_ds_uf;
	private String clie_id_cd_codigocliente;
	private String tran_ds_cnpj;
	private String tran_nm_nometransportadora;
	
	
	
	
	public String getCncl_nm_razaosocial() {
		return cncl_nm_razaosocial;
	}
	public void setCncl_nm_razaosocial(String cncl_nm_razaosocial) {
		this.cncl_nm_razaosocial = cncl_nm_razaosocial;
	}
	public String getCncl_ds_cnpj() {
		return cncl_ds_cnpj;
	}
	public void setCncl_ds_cnpj(String cncl_ds_cnpj) {
		this.cncl_ds_cnpj = cncl_ds_cnpj;
	}
	public String getClie_ds_nomecliente() {
		return clie_ds_nomecliente;
	}
	public void setClie_ds_nomecliente(String clie_ds_nomecliente) {
		this.clie_ds_nomecliente = clie_ds_nomecliente;
	}
	public String getClie_ds_cidade() {
		return clie_ds_cidade;
	}
	public void setClie_ds_cidade(String clie_ds_cidade) {
		this.clie_ds_cidade = clie_ds_cidade;
	}
	public String getClie_ds_uf() {
		return clie_ds_uf;
	}
	public void setClie_ds_uf(String clie_ds_uf) {
		this.clie_ds_uf = clie_ds_uf;
	}
	public String getClie_id_cd_codigocliente() {
		return clie_id_cd_codigocliente;
	}
	public void setClie_id_cd_codigocliente(String clie_id_cd_codigocliente) {
		this.clie_id_cd_codigocliente = clie_id_cd_codigocliente;
	}
	public String getCncl_ds_chanel() {
		return cncl_ds_chanel;
	}
	public void setCncl_ds_chanel(String cncl_ds_chanel) {
		this.cncl_ds_chanel = cncl_ds_chanel;
	}
	public String getTran_ds_cnpj() {
		return tran_ds_cnpj;
	}
	public void setTran_ds_cnpj(String tran_ds_cnpj) {
		this.tran_ds_cnpj = tran_ds_cnpj;
	}
	public String getTran_nm_nometransportadora() {
		return tran_nm_nometransportadora;
	}
	public void setTran_nm_nometransportadora(String tran_nm_nometransportadora) {
		this.tran_nm_nometransportadora = tran_nm_nometransportadora;
	}
	

}
