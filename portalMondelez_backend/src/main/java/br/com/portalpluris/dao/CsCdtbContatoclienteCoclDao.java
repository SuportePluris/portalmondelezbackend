package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbContatoclienteCoclBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsCdtbContatoclienteCoclDao {

	public RetornoBean createUpdateContatocliente(CsCdtbContatoclienteCoclBean coclBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

	
		if (Long.parseLong(coclBean.getCocl_id_cd_contatocliente()) > 0l) {
			retBean = updatecocl(coclBean);

			return retBean;
		}

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		pst = conn.prepareStatement(
				"" + " INSERT INTO CS_CDTB_CONTATOCLIENTE_COCL ( " + " 		COCL_ID_CD_CODIGOCLIENTE, "
						+ " 		COCL_DS_EMAILCONTATOCLIENTE" + " ) VALUES ( " + " 		?, ? " + ") " + "");

		pst.setString(1, coclBean.getCocl_id_cd_codigocliente());
		pst.setString(2, coclBean.getCocl_ds_emailcontatocliente());

		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Contato criado com sucesso");

		return retBean;
	}

	private RetornoBean updatecocl(CsCdtbContatoclienteCoclBean coclBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		String queryUpdate = " UPDATE CS_CDTB_CONTATOCLIENTE_COCL SET " + " COCL_ID_CD_USUARIOATUALIZACAO = "
				+ coclBean.getCocl_id_cd_usuarioatualizacao() + ", " + " COCL_DH_ATUALIZACAO = GETDATE()";

		if (coclBean.getCocl_ds_emailcontatocliente() != null && coclBean.getCocl_ds_emailcontatocliente() != "") {
			queryUpdate += ", COCL_DS_EMAILCONTATOCLIENTE = '" + coclBean.getCocl_ds_emailcontatocliente() + "'";
		}

		if (coclBean.getCocl_in_inativo() != null && coclBean.getCocl_in_inativo().equals("")) {
			queryUpdate += ", COCL_IN_INATIVO = '" + coclBean.getCocl_in_inativo() + "'";
		}

		queryUpdate += " WHERE COCL_ID_CD_CONTATOCLIENTE = " + coclBean.getCocl_id_cd_contatocliente();

		pst = conn.prepareStatement(queryUpdate);
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Contato atualizado com sucesso");

		return retBean;

	}

	public List<CsCdtbContatoclienteCoclBean> getContatoCliente(int coclIdCdCodigocliente) throws SQLException {

		List<CsCdtbContatoclienteCoclBean> lstCoclBean = new ArrayList<CsCdtbContatoclienteCoclBean>();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		ResultSet rs = null;

		pst = conn.prepareStatement(
				"" + "  SELECT " + "  	COCL_ID_CD_CONTATOCLIENTE, " + "  	COCL_ID_CD_CODIGOCLIENTE, "
						+ "  	COCL_DS_EMAILCONTATOCLIENTE, " + "  	COCL_ID_CD_USUARIOATUALIZACAO, "
						+ "  	COCL_ID_CD_USUARIOCRIACAO, " + "  	COCL_DH_REGISTRO, " + "  	COCL_DH_ATUALIZACAO, "
						+ "  	COCL_IN_INATIVO " + "  FROM " + "  	CS_CDTB_CONTATOCLIENTE_COCL (NOLOCK) " + "  WHERE "
						+ "  	COCL_ID_CD_CODIGOCLIENTE = ? " + "");

		pst.setInt(1, coclIdCdCodigocliente);

		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbContatoclienteCoclBean coclBean = setCoclBean(rs);

			lstCoclBean.add(coclBean);
		}

		return lstCoclBean;
	}

	private CsCdtbContatoclienteCoclBean setCoclBean(ResultSet rs) throws SQLException {

		CsCdtbContatoclienteCoclBean coclBean = new CsCdtbContatoclienteCoclBean();

		coclBean.setCocl_dh_atualizacao(rs.getString("COCL_DH_ATUALIZACAO"));
		coclBean.setCocl_dh_registro(rs.getString("COCL_DH_REGISTRO"));
		coclBean.setCocl_ds_emailcontatocliente(rs.getString("COCL_DS_EMAILCONTATOCLIENTE"));
		coclBean.setCocl_id_cd_codigocliente(rs.getString("COCL_ID_CD_CODIGOCLIENTE"));
		coclBean.setCocl_id_cd_contatocliente(rs.getString("COCL_ID_CD_CONTATOCLIENTE"));
		coclBean.setCocl_id_cd_usuarioatualizacao(rs.getString("COCL_ID_CD_USUARIOATUALIZACAO"));
		coclBean.setCocl_id_cd_usuariocriacao(rs.getString("COCL_ID_CD_USUARIOCRIACAO"));
		coclBean.setCocl_in_inativo(rs.getString("COCL_IN_INATIVO"));

		return coclBean;
	}

	public boolean slctContato(String idCliente, String lstEmail) {
		
		CsCdtbContatoclienteCoclBean contatoCliBean = new CsCdtbContatoclienteCoclBean();
		contatoCliBean.setCocl_id_cd_codigocliente("0");
		
		boolean falso = false;
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "	SELECT   ";
		 	cSQL += " COCL_ID_CD_CONTATOCLIENTE, ";
		 	cSQL += " COCL_ID_CD_CODIGOCLIENTE,";
		 	cSQL += " COCL_DS_EMAILCONTATOCLIENTE,";
		 	cSQL += " COCL_ID_CD_USUARIOATUALIZACAO,";
		 	cSQL += " COCL_ID_CD_USUARIOCRIACAO,";
		 	cSQL += " COCL_DH_REGISTRO,";
		 	cSQL += " COCL_DS_GRUPOCLIENTE,";
		 	cSQL += " COCL_DH_ATUALIZACAO,";
		 	cSQL += " COCL_IN_INATIVO,";
		 	cSQL += " COCL_DS_NOMECLIENTE,";
		 	cSQL += " COCl_DS_TELEFONECLIENTE,";
			cSQL += " COCl_DS_CEPCLIENTE,";
			cSQL += " COCl_DS_BAIRROCLIENTE,";
			cSQL += " COCl_DS_CIDADECLIENTE,";
			cSQL += " COCl_DS_ENDERECOCLIENTE,";
			cSQL += " COCl_DS_CNPJCLIENTE";
			cSQL += " FROM  CS_CDTB_CONTATOCLIENTE_COCL (NOLOCK) ";
			cSQL += " WHERE COCL_ID_CD_CODIGOCLIENTE = ? ";
			cSQL += " AND COCL_DS_EMAILCONTATOCLIENTE = ? ";
			

			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, idCliente);
			pst.setString(2, lstEmail);

			rs = pst.executeQuery();			

			if (rs.next()) {
				
				falso = true;
				
			}
			
			
		} catch (Exception e) {			

		} 
		
		return falso;
	}

	private CsCdtbContatoclienteCoclBean setContatoCliente(ResultSet rs) throws SQLException {

		CsCdtbContatoclienteCoclBean contatoCliBean = new CsCdtbContatoclienteCoclBean();
		
		contatoCliBean.setCocl_dh_atualizacao(rs.getString("COCL_DH_ATUALIZACAO"));
		contatoCliBean.setCocl_dh_registro(rs.getString("COCL_DH_REGISTRO"));
		contatoCliBean.setCocl_ds_bairrocliente(rs.getString("COCl_DS_BAIRROCLIENTE"));
		contatoCliBean.setCocl_ds_cepcliente(rs.getString("COCl_DS_CEPCLIENTE"));
		contatoCliBean.setCocl_ds_cidadecliente(rs.getString("COCl_DS_CIDADECLIENTE"));
		contatoCliBean.setCocl_ds_cnjcliente(rs.getString("COCl_DS_CNPJCLIENTE"));
		contatoCliBean.setCocl_ds_emailcontatocliente(rs.getString("COCL_DS_EMAILCONTATOCLIENTE"));
		contatoCliBean.setCocl_ds_enderecocliente(rs.getString("COCl_DS_ENDERECOCLIENTE"));
		contatoCliBean.setCocl_ds_nomecliente(rs.getString("COCL_DS_NOMECLIENTE"));
		contatoCliBean.setCocl_ds_telefonecliente(rs.getString("COCl_DS_TELEFONECLIENTE"));
		contatoCliBean.setCocl_id_cd_codigocliente(rs.getString("COCL_ID_CD_CODIGOCLIENTE"));
		contatoCliBean.setCocl_id_cd_contatocliente(rs.getString("COCL_ID_CD_CONTATOCLIENTE"));
		contatoCliBean.setCocl_id_cd_usuarioatualizacao(rs.getString("COCL_ID_CD_USUARIOATUALIZACAO"));
		contatoCliBean.setCocl_id_cd_usuariocriacao(rs.getString("COCL_ID_CD_USUARIOCRIACAO"));
		contatoCliBean.setCocl_in_inativo(rs.getString("COCL_IN_INATIVO"));
		contatoCliBean.setCocl_ds_grupocliente(rs.getString("COCL_DS_GRUPOCLIENTE"));
		contatoCliBean.setCocl_ds_ufcliente(rs.getString("COCL_DS_UFCLIENTE"));
		
		return contatoCliBean;
	}

	public void insertContatoCliente(Sheet sheet, String dhImportacao,
			String nmArquivo, CsCdtbContatoclienteCoclBean contatoCliBean, String idCliente, int j) {
		
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		Logger log = Logger.getLogger(this.getClass().getName());
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");
		CsCdtbContatoclienteCoclDao contatoCliDao = new CsCdtbContatoclienteCoclDao();
		
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbContatoclienteCoclDao");
		logsBean.setLogsDsMetodo("insertContatoCliente");
		logsBean.setLogsInErro("N");
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();		
		
				
			 String email = sheet.getRow(j).getCell(4).toString();
			 
			 String[] lstEmail = email.split(",");
			 
			 for (int i =0; i < lstEmail.length; i++){
				 
				 boolean ifExists = contatoCliDao.slctContato(idCliente,lstEmail[i]);		
				 
				 	if(!ifExists){
				 		
				 		contatoCliBean.setCocl_ds_grupocliente(sheet.getRow(j).getCell(0).toString());				
				 		contatoCliBean.setCocl_id_cd_codigocliente(idCliente);
				 		Cell celCnpj = sheet.getRow(1).getCell(2);
				 		celCnpj.setCellType(CellType.STRING);				 		
				 		contatoCliBean.setCocl_ds_cnjcliente(celCnpj.toString());
				 		contatoCliBean.setCocl_ds_nomecliente(sheet.getRow(j).getCell(3).toString());
				 		contatoCliBean.setCocl_ds_emailcontatocliente(lstEmail[i]);
				 		
				 		if(sheet.getRow(j).getCell(5) != null  ){
				 			contatoCliBean.setCocl_ds_telefonecliente(sheet.getRow(j).getCell(5).toString());
				 			
				 		}else{
				 			contatoCliBean.setCocl_ds_telefonecliente("");
				 		}
				 		
				 		contatoCliBean.setCocl_ds_enderecocliente(sheet.getRow(j).getCell(6).toString());
				 		contatoCliBean.setCocl_ds_bairrocliente(sheet.getRow(j).getCell(7).toString());
				 		contatoCliBean.setCocl_ds_cidadecliente(sheet.getRow(j).getCell(8).toString());
				 		contatoCliBean.setCocl_ds_ufcliente(sheet.getRow(j).getCell(9).toString());
				 		contatoCliBean.setCocl_ds_cepcliente(sheet.getRow(j).getCell(10).toString());
				 		contatoCliBean.setCocl_dh_registro(dhImportacao);
				 		contatoCliBean.setCocl_id_cd_usuarioatualizacao(idUser);
				 		contatoCliBean.setCocl_in_inativo("N");
				 		
				 		
				 		PreparedStatement pst = con.prepareStatement(""							
				 				+ " INSERT INTO CS_CDTB_CONTATOCLIENTE_COCL ("
				 				+ " COCL_DS_GRUPOCLIENTE,"
				 				+ " COCL_ID_CD_CODIGOCLIENTE,"
				 				+ " COCl_DS_CNPJCLIENTE,"
				 				+ " COCL_DS_NOMECLIENTE,"
				 				+ " COCL_DS_EMAILCONTATOCLIENTE,"
				 				+ " COCl_DS_TELEFONECLIENTE,"
				 				+ " COCl_DS_ENDERECOCLIENTE,"
				 				+ " COCl_DS_BAIRROCLIENTE,"
				 				+ " COCl_DS_CIDADECLIENTE,"
				 				+ " COCL_DS_UFCLIENTE,"
				 				+ " COCl_DS_CEPCLIENTE,"
				 				+ " COCL_DH_REGISTRO,"
				 				+ " COCL_ID_CD_USUARIOATUALIZACAO,"
				 				+ " COCL_IN_INATIVO"
				 				+" ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");				
				 		
				 		pst.setString(1, contatoCliBean.getCocl_ds_grupocliente());
				 		pst.setString(2,idCliente);
				 		pst.setString(3,contatoCliBean.getCocl_ds_cnjcliente());
				 		pst.setString(4,contatoCliBean.getCocl_ds_nomecliente());
				 		pst.setString(5,contatoCliBean.getCocl_ds_emailcontatocliente());
				 		pst.setString(6,contatoCliBean.getCocl_ds_telefonecliente());
				 		pst.setString(7,contatoCliBean.getCocl_ds_enderecocliente());
				 		pst.setString(8,contatoCliBean.getCocl_ds_bairrocliente());
				 		pst.setString(9,contatoCliBean.getCocl_ds_cidadecliente());
				 		pst.setString(10,contatoCliBean.getCocl_ds_ufcliente());
				 		pst.setString(11,contatoCliBean.getCocl_ds_cepcliente());
				 		pst.setString(12,dhImportacao);
				 		pst.setString(13, idUser);
				 		pst.setString(14, "N");
				 		
				 		pst.execute();
				 		
				 	}
			 }				 
			 
			
		} catch (Exception e) {

			log.info("***** Erro no INSERT na base Contato Cliente  " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
			

		} finally {

			log.info("***** Fim do  INSERT na base Contato Cliente  *****");
			daoLog.createLog(logsBean);

		}
	}

}