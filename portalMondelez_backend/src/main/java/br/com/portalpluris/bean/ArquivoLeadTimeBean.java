package br.com.portalpluris.bean;

public class ArquivoLeadTimeBean {

	private String plantaCidade;
	private int sourceLocationID;
	private String sourceLocationName;
	private String sourceProvince;
	private String destinationCity;
	private String codProvinceDestination;
	private String newZone;
	private int ltl;
	private int tl;
	private String dataAlteracao;

	public String getPlantaCidade() {
		return plantaCidade;
	}

	public void setPlantaCidade(String plantaCidade) {
		this.plantaCidade = plantaCidade;
	}

	public int getSourceLocationID() {
		return sourceLocationID;
	}

	public void setSourceLocationID(int sourceLocationID) {
		this.sourceLocationID = sourceLocationID;
	}

	public String getSourceLocationName() {
		return sourceLocationName;
	}

	public void setSourceLocationName(String sourceLocationName) {
		this.sourceLocationName = sourceLocationName;
	}

	public String getSourceProvince() {
		return sourceProvince;
	}

	public void setSourceProvince(String sourceProvince) {
		this.sourceProvince = sourceProvince;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public String getCodProvinceDestination() {
		return codProvinceDestination;
	}

	public void setCodProvinceDestination(String codProvinceDestination) {
		this.codProvinceDestination = codProvinceDestination;
	}

	public String getNewZone() {
		return newZone;
	}

	public void setNewZone(String newZone) {
		this.newZone = newZone;
	}

	public int getLtl() {
		return ltl;
	}

	public void setLtl(int ltl) {
		this.ltl = ltl;
	}

	public int getTl() {
		return tl;
	}

	public void setTl(int tl) {
		this.tl = tl;
	}

	public String getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(String dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

}
