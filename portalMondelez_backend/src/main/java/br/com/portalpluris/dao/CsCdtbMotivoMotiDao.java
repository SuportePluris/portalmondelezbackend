package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.CsCdtbMotivoMotiBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsCdtbMotivoMotiDao {

	public RetornoBean createUpdateMotivo(CsCdtbMotivoMotiBean motivoBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		if (motivoBean.getMoti_id_cd_motivo() > 0) {
			retBean = updateMotivo(motivoBean);

			return retBean;
		}

		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		PreparedStatement pst2;
			
		// Insert do Motivo
			pst = conn.prepareStatement("" 
					+ " INSERT INTO CS_CDTB_MOTIVO_MOTI ( " 
					+ "		MOTI_DS_MOTIVO, "
					+ "		MOTI_IN_INATIVO " + " ) VALUES ( ?, ? ) ");

			pst.setString(1, motivoBean.getMoti_ds_motivo());
			pst.setString(2, motivoBean.getMoti_in_inativo());
			pst.execute();
			
		// Insert da Associação tipo x motivo
			pst2 = conn.prepareStatement("" 
					+ " INSERT INTO CS_ASTB_TIPOMOTIVO_TIMO ( " 
					+ "		MOTI_ID_CD_MOTIVO, "
					+ "		TIPO_ID_CD_TIPO " + " ) VALUES ( ?, ? ) ");

			pst2.setLong(1, getLastId());
			pst2.setString(2, motivoBean.getTipo_id_cd_tipo());

		pst2.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Motivo criado com sucesso");

		return retBean;
	}
	
	private int getLastId() throws SQLException {
		
		int moti_id_cd_motivo = 0;
		
		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		ResultSet rs = null;
		
		pst = conn.prepareStatement("" 
				+ " SELECT MAX(MOTI_ID_CD_MOTIVO) AS MOTI_ID_CD_MOTIVO FROM CS_CDTB_MOTIVO_MOTI (NOLOCK)");
		
		rs = pst.executeQuery();
		
		if(rs.next()) {
			moti_id_cd_motivo = rs.getInt("MOTI_ID_CD_MOTIVO");
		}
		
		return moti_id_cd_motivo;
	}

	private RetornoBean updateMotivo(CsCdtbMotivoMotiBean motivoBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		PreparedStatement pst2;

		String queryUpdate = " UPDATE CS_CDTB_MOTIVO_MOTI SET ";
		String queryUpdate2 = " UPDATE CS_ASTB_TIPOMOTIVO_TIMO SET ";

		// Update do Motivo
		
		if (motivoBean.getMoti_ds_motivo() != null && motivoBean.getMoti_ds_motivo() != "") {
			queryUpdate += " MOTI_DS_MOTIVO = '" + motivoBean.getMoti_ds_motivo() + "'";
		}

		if (motivoBean.getMoti_in_inativo() != null && motivoBean.getMoti_in_inativo() != "") {
			queryUpdate += ", MOTI_IN_INATIVO = '" + motivoBean.getMoti_in_inativo() + "'";
		}

		queryUpdate += " WHERE MOTI_ID_CD_MOTIVO = " + motivoBean.getMoti_id_cd_motivo();
		
		// Update da Associação tipo x motivo
		
		queryUpdate2 += " MOTI_ID_CD_MOTIVO = '" + motivoBean.getMoti_id_cd_motivo() + "'";
		
		queryUpdate2 += ", TIPO_ID_CD_TIPO = '" + motivoBean.getTipo_id_cd_tipo()+ "'";
		
		queryUpdate2 += " WHERE MOTI_ID_CD_MOTIVO = " + motivoBean.getMoti_id_cd_motivo();
		
		pst = conn.prepareStatement(queryUpdate);
		pst2 = conn.prepareStatement(queryUpdate2);
		
		pst.execute();
		pst2.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Motivo atualizado com sucesso");

		return retBean;
	}

	public List<CsCdtbMotivoMotiBean> getListMotivo(String tipo, String inativo) throws SQLException {

		List<CsCdtbMotivoMotiBean> list = new ArrayList<CsCdtbMotivoMotiBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" 
				+ " SELECT " 
				+ " 	MOTI.MOTI_ID_CD_MOTIVO, " 
				+ " 	MOTI.MOTI_DS_MOTIVO, "
				+ " 	MOTI.MOTI_IN_INATIVO, " 
				+ " 	MOTI.MOTI_DH_REGISTRO " 
				+ " FROM  "
				+ " CS_ASTB_TIPOMOTIVO_TIMO TIMO (NOLOCK) INNER JOIN CS_CDTB_TIPO_TIPO TIPO  (NOLOCK) "
				+ " 	ON   TIMO.TIPO_ID_CD_TIPO = TIPO.TIPO_ID_CD_TIPO " 
				+ " INNER JOIN  CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) "
				+ " 	ON TIMO.MOTI_ID_CD_MOTIVO = MOTI.MOTI_ID_CD_MOTIVO  " 
				+ " WHERE " 
				+ " 	TIPO.TIPO_ID_CD_TIPO = '"+ tipo + "'" 
				+ " 	AND MOTI.MOTI_IN_INATIVO = '" + inativo	+ "'" 
				+ " ORDER BY MOTI_DS_MOTIVO ";

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbMotivoMotiBean bean = setBean(rs);

			list.add(bean);
		}

		return list;
	}

	public CsCdtbMotivoMotiBean getMoti(int moti_id_cd_motivo) throws SQLException {

		CsCdtbMotivoMotiBean motiBean = new CsCdtbMotivoMotiBean();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		if (moti_id_cd_motivo > 0) {

		}

		PreparedStatement pst;
		String query = "" + " SELECT " + "		MOTI.MOTI_ID_CD_MOTIVO, " + "		MOTI.MOTI_DS_MOTIVO, "
				+ "		MOTI.MOTI_DH_REGISTRO, " + "		MOTI.MOTI_IN_INATIVO " + "		TIPO.TIPO_DS_TIPO "
				+ " FROM " + " 	CS_ASTB_TIPOMOTIVO_TIMO TIMO (NOLOCK) INNER JOIN CS_CDTB_TIPO_TIPO TIPO (NOLOCK)   "
				+ " 	ON   TIMO.TIPO_ID_CD_TIPO = TIPO.TIPO_ID_CD_TIPO " + " INNER JOIN  CS_CDTB_MOTIVO_MOTI MOTI (NOLOCK) "
				+ " 	ON TIMO.MOTI_ID_CD_MOTIVO = MOTI.MOTI_ID_CD_MOTIVO  " + " WHERE "
				+ "		MOTI.MOTI_ID_CD_MOTIVO = ?";

		pst = conn.prepareStatement(query);
		pst.setInt(1, moti_id_cd_motivo);

		rs = pst.executeQuery();

		while (rs.next()) {

			motiBean = setBean(rs);
		}

		return motiBean;
	}

	private CsCdtbMotivoMotiBean setBean(ResultSet rs) throws SQLException {

		CsCdtbMotivoMotiBean bean = new CsCdtbMotivoMotiBean();

		bean.setMoti_id_cd_motivo(rs.getInt("MOTI_ID_CD_MOTIVO"));
		bean.setMoti_ds_motivo(rs.getString("MOTI_DS_MOTIVO"));
		bean.setMoti_in_inativo(rs.getString("MOTI_IN_INATIVO"));
		bean.setMoti_dh_registro(rs.getString("MOTI_DH_REGISTRO"));

		return bean;
	}

}
