package br.com.portalpluris.bean;

public class CsCdtbMotivoMotiBean {

	int moti_id_cd_motivo;
	private String moti_ds_motivo;
	private String moti_in_inativo;
	private String moti_dh_registro;
	private String tipo_id_cd_tipo;

	public String getTipo_id_cd_tipo() {
		return tipo_id_cd_tipo;
	}

	public void setTipo_id_cd_tipo(String tipo_id_cd_tipo) {
		this.tipo_id_cd_tipo = tipo_id_cd_tipo;
	}

	public int getMoti_id_cd_motivo() {
		return moti_id_cd_motivo;
	}

	public void setMoti_id_cd_motivo(int moti_id_cd_motivo) {
		this.moti_id_cd_motivo = moti_id_cd_motivo;
	}

	public String getMoti_ds_motivo() {
		return moti_ds_motivo;
	}

	public void setMoti_ds_motivo(String moti_ds_motivo) {
		this.moti_ds_motivo = moti_ds_motivo;
	}

	public String getMoti_in_inativo() {
		return moti_in_inativo;
	}

	public void setMoti_in_inativo(String moti_in_inativo) {
		this.moti_in_inativo = moti_in_inativo;
	}

	public String getMoti_dh_registro() {
		return moti_dh_registro;
	}

	public void setMoti_dh_registro(String moti_dh_registro) {
		this.moti_dh_registro = moti_dh_registro;
	}

}
