package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CsDmtbConfiguracaoConfDao {

	public String slctConf(String confChave) {

		String conf = "";

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "	SELECT   ";
			cSQL += "	CONF_DS_VALSTRING";
			cSQL += " 		FROM CS_DMTB_CONFIGURACAO_CONF (NOLOCK)";
			cSQL += " 	WHERE  CONF_DS_CHAVE = ? ";

			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, confChave);

			rs = pst.executeQuery();

			if (rs.next()) {

				conf = rs.getString("CONF_DS_VALSTRING");

			}

		} catch (Exception e) {

			System.out.println("ERRO NA CONSULTA DA CONFIGURAÇÃO" + e);

		}

		return conf;
	}

}
