package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.portalpluris.bean.CsCdtbRegiaoRegiBean;
import br.com.portalpluris.dao.CsCdtbRegiaoRegiDao;



public class CsCdtbRegiaoRegiBusiness {

	public String getListRegiao(String inativo) throws JsonProcessingException {

		CsCdtbRegiaoRegiDao dao = new CsCdtbRegiaoRegiDao();
		List<CsCdtbRegiaoRegiBean> listBean = new ArrayList<CsCdtbRegiaoRegiBean>();

		String jsonRetorno = "";

		try {

			listBean = dao.getListRegiao(inativo);

		} catch (Exception e) {
			e.printStackTrace();

		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listBean);

		return jsonRetorno;
		
	}
	
}
