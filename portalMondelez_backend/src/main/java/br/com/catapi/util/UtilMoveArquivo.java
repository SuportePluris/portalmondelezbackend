package br.com.catapi.util;

import java.io.File;
import java.util.Date;

public class UtilMoveArquivo {

	public boolean moveArquivo(File arquivoProcessado, File arquivoProcessadobkp) {

		Date data = new Date();
		UtilDate utilDate = new UtilDate();

		String strData = utilDate.convertDateToYYYY_MM_dd_HH_MM_SS(data);
		strData = strData.replace("-", "_");
		strData = strData.replace(" ", "_");
		strData = strData.replace(":", "_");

		boolean sucesso = arquivoProcessado.renameTo(new File(arquivoProcessadobkp + strData));

		return sucesso;
	}

}