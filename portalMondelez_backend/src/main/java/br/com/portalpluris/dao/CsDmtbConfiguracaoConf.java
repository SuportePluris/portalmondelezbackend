package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CsDmtbConfiguracaoConf {

	public static String getConfiguracao(String confDsChave) {

		String confDsValstring = "";

		try {

			Connection conn = DAO.getConexao();
			ResultSet rs;
			PreparedStatement pst;

			String query = "" 	+ " SELECT " 
									+ " 	CONF_DS_VALSTRING " 
								+ " FROM  "
									+ " 	CS_DMTB_CONFIGURACAO_CONF (NOLOCK) " 
								+ " WHERE " 
									+ " 	CONF_DS_CHAVE = ? " 
								+ " ";

			pst = conn.prepareStatement(query);
			pst.setString(1, confDsChave);

			rs = pst.executeQuery();

			if (rs.next()) {

				confDsValstring = rs.getString("CONF_DS_VALSTRING");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return confDsValstring;
	}
}
