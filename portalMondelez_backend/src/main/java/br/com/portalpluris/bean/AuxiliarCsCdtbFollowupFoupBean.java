package br.com.portalpluris.bean;

public class AuxiliarCsCdtbFollowupFoupBean {

	
	private String emfo_id_cd_embafollow;
	private String emfo_id_cd_embarque;
	private String emfo_id_cd_usuariocriacao;
	private String user_ds_nomeusuario;
	private String emfo_id_cd_followup;
	private String foup_ds_followup;
	private String emfo_id_cd_motivo;
	private String moti_ds_motivo;
	private String emfo_id_cd_responsavel;
	private String resp_nm_responsavel;
	private String emfo_dh_registro;
	private String emfo_tx_followup;
	
	public String getEmfo_tx_followup() {
		return emfo_tx_followup;
	}
	public void setEmfo_tx_followup(String emfo_tx_followup) {
		this.emfo_tx_followup = emfo_tx_followup;
	}
	public String getEmfo_id_cd_embafollow() {
		return emfo_id_cd_embafollow;
	}
	public void setEmfo_id_cd_embafollow(String emfo_id_cd_embafollow) {
		this.emfo_id_cd_embafollow = emfo_id_cd_embafollow;
	}
	public String getEmfo_id_cd_embarque() {
		return emfo_id_cd_embarque;
	}
	public void setEmfo_id_cd_embarque(String emfo_id_cd_embarque) {
		this.emfo_id_cd_embarque = emfo_id_cd_embarque;
	}
	public String getEmfo_id_cd_usuariocriacao() {
		return emfo_id_cd_usuariocriacao;
	}
	public void setEmfo_id_cd_usuariocriacao(String emfo_id_cd_usuariocriacao) {
		this.emfo_id_cd_usuariocriacao = emfo_id_cd_usuariocriacao;
	}
	public String getUser_ds_nomeusuario() {
		return user_ds_nomeusuario;
	}
	public void setUser_ds_nomeusuario(String user_ds_nomeusuario) {
		this.user_ds_nomeusuario = user_ds_nomeusuario;
	}
	public String getEmfo_id_cd_followup() {
		return emfo_id_cd_followup;
	}
	public void setEmfo_id_cd_followup(String emfo_id_cd_followup) {
		this.emfo_id_cd_followup = emfo_id_cd_followup;
	}
	public String getFoup_ds_followup() {
		return foup_ds_followup;
	}
	public void setFoup_ds_followup(String foup_ds_followup) {
		this.foup_ds_followup = foup_ds_followup;
	}
	public String getEmfo_id_cd_motivo() {
		return emfo_id_cd_motivo;
	}
	public void setEmfo_id_cd_motivo(String emfo_id_cd_motivo) {
		this.emfo_id_cd_motivo = emfo_id_cd_motivo;
	}
	public String getMoti_ds_motivo() {
		return moti_ds_motivo;
	}
	public void setMoti_ds_motivo(String moti_ds_motivo) {
		this.moti_ds_motivo = moti_ds_motivo;
	}
	public String getEmfo_id_cd_responsavel() {
		return emfo_id_cd_responsavel;
	}
	public void setEmfo_id_cd_responsavel(String emfo_id_cd_responsavel) {
		this.emfo_id_cd_responsavel = emfo_id_cd_responsavel;
	}
	public String getResp_nm_responsavel() {
		return resp_nm_responsavel;
	}
	public void setResp_nm_responsavel(String resp_nm_responsavel) {
		this.resp_nm_responsavel = resp_nm_responsavel;
	}
	public String getEmfo_dh_registro() {
		return emfo_dh_registro;
	}
	public void setEmfo_dh_registro(String emfo_dh_registro) {
		this.emfo_dh_registro = emfo_dh_registro;
	}
	
	
	

}
