package br.com.portalpluris.bean;

import java.sql.Timestamp;

public class CsLgtbProdutoProdBean {

	String prod_cd_materialcode;
	String prod_ds_materialdescription;
	String prod_ds_divisiondescription;
	Timestamp prod_dh_registro;
	String prod_ds_statusprocessamento;
	Timestamp prod_dh_processamento;
	String prod_ds_nomearquivo;

	public String getProd_cd_materialcode() {
		return prod_cd_materialcode;
	}

	public void setProd_cd_materialcode(String prod_cd_materialcode) {
		this.prod_cd_materialcode = prod_cd_materialcode;
	}

	public String getProd_ds_materialdescription() {
		return prod_ds_materialdescription;
	}

	public void setProd_ds_materialdescription(String prod_ds_materialdescription) {
		this.prod_ds_materialdescription = prod_ds_materialdescription;
	}

	public String getProd_ds_divisiondescription() {
		return prod_ds_divisiondescription;
	}

	public void setProd_ds_divisiondescription(String prod_ds_divisiondescription) {
		this.prod_ds_divisiondescription = prod_ds_divisiondescription;
	}

	public Timestamp getProd_dh_registro() {
		return prod_dh_registro;
	}

	public void setProd_dh_registro(Timestamp prod_dh_registro) {
		this.prod_dh_registro = prod_dh_registro;
	}

	public String getProd_ds_statusprocessamento() {
		return prod_ds_statusprocessamento;
	}

	public void setProd_ds_statusprocessamento(String prod_ds_statusprocessamento) {
		this.prod_ds_statusprocessamento = prod_ds_statusprocessamento;
	}

	public Timestamp getProd_dh_processamento() {
		return prod_dh_processamento;
	}

	public void setProd_dh_processamento(Timestamp prod_dh_processamento) {
		this.prod_dh_processamento = prod_dh_processamento;
	}

	public String getProd_ds_nomearquivo() {
		return prod_ds_nomearquivo;
	}

	public void setProd_ds_nomearquivo(String prod_ds_nomearquivo) {
		this.prod_ds_nomearquivo = prod_ds_nomearquivo;
	}

}
