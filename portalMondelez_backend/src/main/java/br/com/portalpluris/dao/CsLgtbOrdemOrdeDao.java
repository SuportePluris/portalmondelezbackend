package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbCustomerCustBean;
import br.com.portalpluris.bean.CsLgtbOrdemOrdeBean;

public class CsLgtbOrdemOrdeDao {
	
	/**
	 * Metodo responsavel por realizar o insert das informacoes do arquivo TXT de ordem na tabela CS_LGTB_ORDEM_ORDE
	 * @author Caio Fernandes
	 * @param List<CsLgtbCustomerCustBean>, String
	 * */
	public void insertOrdem(List<CsLgtbOrdemOrdeBean> lstOrdem, String nmArquivo) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		UtilDate utilDate = new UtilDate();
		Connection conn = DAO.getConexao();
		
		log.info("[insertOrdem] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstOrdem.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertOrdem] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" INSERT CS_LGTB_ORDEM_ORDE ( "
						+ "		ORDE_DS_PLANT, " 	
						+ "		ORDE_DS_SALESDOCUMENTTYPE, 	" 	
						+ "		ORDE_DS_SALESDOCUMENTNUMBER, 	"				
						+ "		ORDE_DS_ITEM, 	"					
						+ "		ORDE_DS_OVERALLPROCESSINGSTATUS, 	"
						+ "		ORDE_DS_ENTRYDATE, 	"
						+ "		ORDE_DS_SOLDTOPARTY, 	"
						+ "		ORDE_DS_MATERIALNUMBER, 	"
						+ "		ORDE_DS_PERSONNELNUMBER, 	"
						+ "		ORDE_DS_BILLINGDATE, 	"
						+ "		ORDE_DH_REQUESTDELIVERYDATE, 	"
						+ "		ORDE_DH_RDDFROMCUSTOMER, 	"
						+ "		ORDE_DH_DELIVERYCONFIRMATIONDATE, 	"
						+ "		ORDE_DH_DELIVERYSCHEDULEDATE, 	"
						+ "		ORDE_DS_VOLUMOFTHEITEM, 	"
						+ "		ORDE_DS_NETVALUE, 	"
						+ "		ORDE_DS_DISCOUNT, 	"
						+ "		ORDE_DS_ICMIVALUE, 	"
						+ "		ORDE_DS_IPI3VALUE, 	"
						+ "		ORDE_DS_IPS3VALUE, 	"
						+ "		ORDE_DS_ICN3VALUE, 	"
						+ "		ORDE_DS_ICS3VALUE, 	"
						+ "		ORDE_DS_CUSTOMERPONUMBER, 	"
						+ "		ORDE_DS_PERCENTAGEDISCOUNTITEM, 	"
						+ "		ORDE_DS_MAXALLOWED, 	"
						+ "		ORDE_DS_MINALLOWED, 	"
						+ "		ORDE_DS_SIMULATEDNUMBER, 	"
						+ "		ORDE_DS_SOURCENFNUMBER, 	"
						+ "		ORDE_DS_SOURCENFSERIES, 	"
						+ "		ORDE_DS_CANCELLINGREASON, 	"
						+ "		ORDE_DS_COMMERCIALPOLICY, 	"
						+ "		ORDE_DS_DELIVERYOCCURANCE, 	"
						+ "		ORDE_DS_DELIVERYOCCURANCESOLNDATE, 	"
						+ "		ORDE_DS_DELIVERY, 	"
						+ "		ORDE_DS_BILLINGDOCUMENT, 	"
						+ "		ORDE_DS_NINEDIGITNFENUMBER, 	"
						+ "		ORDE_DS_SERIES, 	"
						+ "		ORDE_DS_ORDERNUMBER, 	"
						+ "		ORDE_DS_ORDERITEMREASONCODE2, 	"
						+ "		ORDE_DS_DERQUANTITYSALESUNITS, 	"
						+ "		ORDE_DS_SALESUNIT, 	"
						+ "		ORDE_DS_SALESORGANIZATION, 	"
						+ "		ORDE_DS_DISTRIBUTIONCHANNEL, 	"
						+ "		ORDE_DS_DIVISION, 	"
						+ "		ORDE_DS_CUSTOMERPOTYPE, 	"
						+ "		ORDE_DS_QUANTIDADE, 	"
						+ "		ORDE_DS_ORDERREASON, 	"
						+ "		ORDE_DS_REASONREJECTION, 	"
						+ "		ORDE_DS_BACKORDERNUMBER, 	"
						+ "		ORDE_DS_TERMSPAYMENTKEY, 	"
						+ "		ORDE_DS_STATUSPROCESSAMENTO, 	"
						+ "		ORDE_DH_PROCESSAMENTO, 	"
						+ " 	ORDE_DS_NOMEARQUIVO )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?,?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, GETDATE(),?)	"
						+ "");
				
				pst.setString(1, lstOrdem.get(i).getOrde_ds_plant());
				pst.setString(2, lstOrdem.get(i).getOrde_ds_salesdocumenttype());
				pst.setString(3, lstOrdem.get(i).getOrde_ds_salesdocumentnumber());
				pst.setString(4, lstOrdem.get(i).getOrde_ds_item());
				pst.setString(5, lstOrdem.get(i).getOrde_ds_overallprocessingstatus());
				pst.setString(6, lstOrdem.get(i).getOrde_ds_entrydate());
				pst.setString(7, lstOrdem.get(i).getOrde_ds_soldtoparty());
				pst.setString(8, lstOrdem.get(i).getOrde_ds_materialnumber());
				pst.setString(9, lstOrdem.get(i).getOrde_ds_personnelnumber());
				pst.setString(10, lstOrdem.get(i).getOrde_ds_billingdate());
				pst.setString(11, utilDate.convertDateToYYYY_MM_dd_HH_MM_SS(lstOrdem.get(i).getOrde_dh_requestdeliverydate())); // data
				pst.setString(12, utilDate.convertDateToYYYY_MM_dd_HH_MM_SS(lstOrdem.get(i).getOrde_dh_rddfromcustomer())); // data
				pst.setString(13, utilDate.convertDateToYYYY_MM_dd_HH_MM_SS(lstOrdem.get(i).getOrde_dh_deliveryconfirmationdate())); // data
				pst.setString(14, utilDate.convertDateToYYYY_MM_dd_HH_MM_SS(lstOrdem.get(i).getOrde_dh_deliveryscheduledate())); // data
				pst.setString(15, lstOrdem.get(i).getOrde_ds_volumoftheitem());
				pst.setString(16, lstOrdem.get(i).getOrde_ds_netvalue());
				pst.setString(17, lstOrdem.get(i).getOrde_ds_discount());
				pst.setString(18, lstOrdem.get(i).getOrde_ds_icmivalue());
				pst.setString(19, lstOrdem.get(i).getOrde_ds_ipi3value());
				pst.setString(20, lstOrdem.get(i).getOrde_ds_ips3value());
				pst.setString(21, lstOrdem.get(i).getOrde_ds_icn3value());
				pst.setString(22, lstOrdem.get(i).getOrde_ds_ics3value());
				pst.setString(23, lstOrdem.get(i).getOrde_ds_customerponumber());
				pst.setString(24, lstOrdem.get(i).getOrde_ds_percentagediscountitem());
				pst.setString(25, lstOrdem.get(i).getOrde_ds_maxallowed());
				pst.setString(26, lstOrdem.get(i).getOrde_ds_minallowed());
				pst.setString(27, lstOrdem.get(i).getOrde_ds_simulatednumber());
				pst.setString(28, lstOrdem.get(i).getOrde_ds_sourcenfnumber());
				pst.setString(29, lstOrdem.get(i).getOrde_ds_sourcenfseries());
				pst.setString(30, lstOrdem.get(i).getOrde_ds_cancellingreason());
				pst.setString(31, lstOrdem.get(i).getOrde_ds_commercialpolicy());
				pst.setString(32, lstOrdem.get(i).getOrde_ds_deliveryoccurance());
				pst.setString(33, lstOrdem.get(i).getOrde_ds_deliveryoccurancesolndate());
				pst.setString(34, lstOrdem.get(i).getOrde_ds_delivery());
				pst.setString(35, lstOrdem.get(i).getOrde_ds_billingdocument());
				pst.setString(36, lstOrdem.get(i).getOrde_ds_ninedigitnfenumber());
				pst.setString(37, lstOrdem.get(i).getOrde_ds_series());
				pst.setString(38, lstOrdem.get(i).getOrde_ds_ordernumber());
				pst.setString(39, lstOrdem.get(i).getOrde_ds_orderitemreasoncode2());
				pst.setString(40, lstOrdem.get(i).getOrde_ds_derquantitysalesunits());
				pst.setString(41, lstOrdem.get(i).getOrde_ds_salesunit());
				pst.setString(42, lstOrdem.get(i).getOrde_ds_salesorganization());
				pst.setString(43, lstOrdem.get(i).getOrde_ds_distributionchannel());
				pst.setString(44, lstOrdem.get(i).getOrde_ds_division());
				pst.setString(45, lstOrdem.get(i).getOrde_ds_customerpotype());
				pst.setString(46, lstOrdem.get(i).getOrde_ds_quantidade());
				pst.setString(47, lstOrdem.get(i).getOrde_ds_orderreason());
				pst.setString(48, lstOrdem.get(i).getOrde_ds_reasonrejection());
				pst.setString(49, lstOrdem.get(i).getOrde_ds_backordernumber());
				pst.setString(50, lstOrdem.get(i).getOrde_ds_termspaymentkey());
				pst.setString(51, "N");
				pst.setString(52, nmArquivo);
				
				pst.execute();
				pst.close();
				
				log.info("[insertOrdem] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[insertOrdem] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		//conn.close();
		
		log.info("[insertOrdem] - FIM ");
		
	}

}
