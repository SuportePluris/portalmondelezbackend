package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.CsCdtbFollowupFoupBean;
import br.com.portalpluris.bean.RetornoListFoupBean;

public class CsCdtbFollowupFoupDao {

	public RetornoBean createUpdateFoup(CsCdtbFollowupFoupBean foupBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		if (foupBean.getFoup_id_cd_followup() > 0) {
			retBean = updateFoup(foupBean);

			return retBean;
		}

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		pst = conn.prepareStatement("" + " INSERT INTO CS_CDTB_FOLLOWUP_FOUP ( " 
				+ " 	FOUP_DS_FOLLOWUP, "
				+ " 	FOUP_ID_CD_USUARIOCRIACAO," 
				+ "		FOUP_IN_INATIVO, "
				+ "		STAT_ID_CD_STATUS"
				+ " ) VALUES ( " + " 		?, ?, ?, ? "
				+ ") " + "");

		pst.setString(1, foupBean.getFoup_ds_followup());
		pst.setLong(2, foupBean.getFoup_id_cd_usuariocriacao());
		pst.setString(3, foupBean.getFoup_in_inativo());
		pst.setInt(4, foupBean.getStat_id_cd_status());
		
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Follow criado com sucesso");

		return retBean;
	}

	private RetornoBean updateFoup(CsCdtbFollowupFoupBean foupBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		String queryUpdate = " UPDATE CS_CDTB_FOLLOWUP_FOUP SET " + " FOUP_ID_CD_USUARIOATUALIZACAO = "
				+ foupBean.getFoup_id_cd_usuarioatualizacao() + ", " + " FOUP_DH_ATUALIZACAO = GETDATE()";

		if (foupBean.getFoup_ds_followup() != null && foupBean.getFoup_ds_followup() != "") {
			queryUpdate += ", FOUP_DS_FOLLOWUP = '" + foupBean.getFoup_ds_followup() + "'";
		}

		if (foupBean.getFoup_in_inativo() != null && !foupBean.getFoup_in_inativo().equals("")) {
			queryUpdate += ", FOUP_IN_INATIVO = '" + foupBean.getFoup_in_inativo() + "'";
		}
		
		if (foupBean.getStat_id_cd_status() != 0 ){
			queryUpdate += ", STAT_ID_CD_STATUS = '" + foupBean.getStat_id_cd_status() + "'";
		}

		queryUpdate += " WHERE FOUP_ID_CD_FOLLOWUP = " + foupBean.getFoup_id_cd_followup();

		pst = conn.prepareStatement(queryUpdate);
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Follow atualizado com sucesso");

		return retBean;
	}

	public List<CsCdtbFollowupFoupBean> getListFoup(String foupInAtivo) throws SQLException {

		List<CsCdtbFollowupFoupBean> lstFoupBean = new ArrayList<CsCdtbFollowupFoupBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" 
				+ " SELECT "
				+ " 	FOUP.FOUP_ID_CD_FOLLOWUP, "
				+ " 	FOUP.FOUP_DS_FOLLOWUP, "
				+ " 	FORMAT (FOUP.FOUP_DH_REGISTRO, 'dd/MM/yyyy ') AS FOUP_DH_REGISTRO,  "
				+ " 	CASE WHEN FOUP.FOUP_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS FOUP_IN_INATIVO, "
				+ " 	FOUP.FOUP_ID_CD_USUARIOATUALIZACAO, "
				+ " 	FORMAT (FOUP.FOUP_DH_ATUALIZACAO, 'dd/MM/yyyy ') AS FOUP_DH_ATUALIZACAO, "
				+ " 	FOUP.FOUP_ID_CD_USUARIOCRIACAO, "
				+ " 	STAT.STAT_ID_CD_STATUS, "
				+ " 	STAT.STAT_DS_STATUS "
				+ " FROM "
				+ " 	CS_CDTB_FOLLOWUP_FOUP FOUP (NOLOCK) "
				+ " 	LEFT JOIN CS_CDTB_STATUSAGENDAMENTO_STAT STAT (NOLOCK) "
				+ " 		ON FOUP.STAT_ID_CD_STATUS = STAT.STAT_ID_CD_STATUS "
				+ "";

		if (foupInAtivo.equalsIgnoreCase("S") || foupInAtivo.equalsIgnoreCase("N")) {
			query += " WHERE FOUP_IN_INATIVO = '" + foupInAtivo + "'";
		}

		query += "ORDER BY FOUP_DS_FOLLOWUP";

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbFollowupFoupBean foupBean = setFoupBean(rs);

			lstFoupBean.add(foupBean);
		}

		return lstFoupBean;
	}
	
	
	public List<CsCdtbFollowupFoupBean> getListCmbFoup(String idTipoTela, String foupInAtivo) throws SQLException {

		List<CsCdtbFollowupFoupBean> lstFoupBean = new ArrayList<CsCdtbFollowupFoupBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " 
				+ "		FOUP_ID_CD_FOLLOWUP, " 
				+ "		FOUP_DS_FOLLOWUP, "
				//+ " 	FORMAT (FOUP_DH_REGISTRO, 'dd/MM/yyyy ') as FOUP_DH_REGISTRO,  "
				+ " 	FOUP_DH_REGISTRO,  "
				+ " 	CASE WHEN FOUP_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS FOUP_IN_INATIVO, "
				+ "		FOUP_ID_CD_USUARIOATUALIZACAO, "
				//+ " 	FORMAT (FOUP_DH_ATUALIZACAO, 'dd/MM/yyyy ') as FOUP_DH_ATUALIZACAO, "
				+ " 	FOUP_DH_ATUALIZACAO, "
				+ "		FOUP_ID_CD_USUARIOCRIACAO " 
				+ " FROM " 
				+ "		CS_CDTB_FOLLOWUP_FOUP (NOLOCK) " + "";

		if (foupInAtivo.equalsIgnoreCase("S") || foupInAtivo.equalsIgnoreCase("N")) {
			query += " WHERE FOUP_IN_INATIVO = '" + foupInAtivo + "'";
		}

		query += "ORDER BY FOUP_DS_FOLLOWUP";

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbFollowupFoupBean foupBean = setFoupBean(rs);

			lstFoupBean.add(foupBean);
		}

		return lstFoupBean;
	}
	

	public CsCdtbFollowupFoupBean getFoup(int foupIdCdFollowup) throws SQLException {

		CsCdtbFollowupFoupBean foupBean = new CsCdtbFollowupFoupBean();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " + "		FOUP_ID_CD_FOLLOWUP, " + "		FOUP_DS_FOLLOWUP, "
				+ "		FOUP_DH_REGISTRO, " + "		FOUP_IN_INATIVO, " + "		FOUP_ID_CD_USUARIOATUALIZACAO, "
				+ "		FOUP_DH_ATUALIZACAO, " + "		FOUP_ID_CD_USUARIOCRIACAO " + " FROM "
				+ "		CS_CDTB_FOLLOWUP_FOUP (NOLOCK) " + " WHERE " + "		FOUP_ID_CD_FOLLOWUP = ?";

		pst = conn.prepareStatement(query);
		pst.setInt(1, foupIdCdFollowup);

		rs = pst.executeQuery();

		while (rs.next()) {

			foupBean = setFoupBean(rs);
		}

		return foupBean;
	}

	private CsCdtbFollowupFoupBean setFoupBean(ResultSet rs) throws SQLException {

		CsCdtbFollowupFoupBean foupBean = new CsCdtbFollowupFoupBean();

		foupBean.setFoup_id_cd_followup(rs.getInt("FOUP_ID_CD_FOLLOWUP"));
		foupBean.setFoup_ds_followup(rs.getString("FOUP_DS_FOLLOWUP"));
		foupBean.setFoup_dh_registro(rs.getString("FOUP_DH_REGISTRO"));
		foupBean.setFoup_in_inativo(rs.getString("FOUP_IN_INATIVO"));
		foupBean.setFoup_id_cd_usuarioatualizacao(rs.getInt("FOUP_ID_CD_USUARIOATUALIZACAO"));
		foupBean.setFoup_dh_atualizacao(rs.getString("FOUP_DH_ATUALIZACAO"));
		foupBean.setFoup_id_cd_usuariocriacao(rs.getInt("FOUP_ID_CD_USUARIOCRIACAO"));
		foupBean.setStat_id_cd_status(rs.getInt("STAT_ID_CD_STATUS"));
		
		ResultSetMetaData meta = rs.getMetaData();
	    int numCol = meta.getColumnCount();
	    for (int i = 1; i <= numCol; i++) {
	        if(meta.getColumnName(i).equalsIgnoreCase("STAT_DS_STATUS")) {
	        	foupBean.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
	        	break;
	        }
	 
	    }
		
		return foupBean;
	}

	public ArrayList<RetornoListFoupBean> getListFoupByStatus(String idStatus) throws SQLException {
		
		ArrayList<RetornoListFoupBean> retFoupList = new ArrayList<RetornoListFoupBean>();
		
		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" 
				+ " SELECT " 
				+ " 	FOUP_ID_CD_FOLLOWUP,"
				+ " 	FOUP_DS_FOLLOWUP,"
				+ " 	STAT.STAT_ID_CD_STATUS,"
				+ " 	STAT.STAT_DS_STATUS"
				+ " FROM  CS_CDTB_FOLLOWUP_FOUP FOUP (NOLOCK)"
				+ " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT STAT (NOLOCK)"
				+ " 		ON FOUP.STAT_ID_CD_STATUS = STAT.STAT_ID_CD_STATUS"
				+ " 	WHERE STAT.STAT_ID_CD_STATUS = ?"
				+ " 		  AND FOUP.FOUP_IN_INATIVO = ?";
				
				
		pst = conn.prepareStatement(query);
		pst.setString(1, idStatus);
		pst.setString(2, "N");

		rs = pst.executeQuery();

		while (rs.next()) {

			RetornoListFoupBean retFoup = new RetornoListFoupBean();
			
			retFoup.setFoup_ds_followup(rs.getString("FOUP_DS_FOLLOWUP"));
			retFoup.setFoup_id_cd_followup(rs.getString("FOUP_ID_CD_FOLLOWUP"));
			retFoup.setStat_id_cd_status(rs.getString("STAT_ID_CD_STATUS"));
			retFoup.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
			retFoup.setSucess("Sucesso");
			retFoupList.add(retFoup);
		}
	
		return retFoupList;
	}
}
