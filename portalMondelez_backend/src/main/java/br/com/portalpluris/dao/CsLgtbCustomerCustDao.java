package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import br.com.portalpluris.bean.CsLgtbCustomerCustBean;

public class CsLgtbCustomerCustDao {
	
	/**
	 * Metodo responsavel por realizar o insert das informacoes do arquivo TXT de customer na tabela CS_LGTB_CUSTOMER_CUST
	 * @author Caio Fernandes
	 * @param List<CsLgtbCustomerCustBean> lstCustomer, String
	 * */
	public void insertCustomer(List<CsLgtbCustomerCustBean> lstCustomer, String nmArquivo) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[insertCustomer] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstCustomer.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertCustomer] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" INSERT CS_LGTB_CUSTOMER_CUST ( "
						+ "		CUST_CD_CUSTOMERCODE, " 	
						+ "		CUST_DS_CUSTOMERNAME, 	" 	
						+ "		CUST_DS_ADDRESSSHIP, 	"				
						+ "		CUST_DS_ADRESSCOMPLEMENT, 	"					
						+ "		CUST_DS_ADRESSCOMPLEMENT2, 	"
						+ "		CUST_DS_CITYMUNICIPIO, 	"
						+ "		CUST_DS_CEP, 	"
						+ "		CUST_DS_STATE, 	"
						+ "		CUST_DS_CITY, 	"
						+ "		CUST_DS_CODDDPHONE, 	"
						+ "		CUST_DS_PHONENUMBER, 	"
						+ "		CUST_DS_CUSTOMERNUMBER, 	"
						+ "		CUST_DS_IMPADICIND, 	"
						+ "		CUST_DS_IBGECODE, 	"
						+ "		CUST_DS_STATECODE, 	"
						+ "		CUST_DS_CODCITYCUSTOMER, 	"
						+ "		CUST_DS_CITYDESCRIPTION, 	"
						+ "		CUST_DH_PROCESSAMENTO, 	"
						+ "		CUST_DS_STATUSPROCESSAMENTO, 	"
						+ " 	CUST_DS_NOMEARQUIVO )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,GETDATE(),?,?)	"
						+ "");
				
				pst.setString(1, lstCustomer.get(i).getCust_cd_customercode());
				pst.setString(2, lstCustomer.get(i).getCust_ds_customername());
				pst.setString(3, lstCustomer.get(i).getCust_ds_addressship());
				pst.setString(4, lstCustomer.get(i).getCust_ds_adresscomplement());
				pst.setString(5, lstCustomer.get(i).getCust_ds_adresscomplement2());
				pst.setString(6, lstCustomer.get(i).getCust_ds_citymunicipio());
				pst.setString(7, lstCustomer.get(i).getCust_ds_cep());
				pst.setString(8, lstCustomer.get(i).getCust_ds_state());
				pst.setString(9, lstCustomer.get(i).getCust_ds_city());
				pst.setString(10, lstCustomer.get(i).getCust_ds_codddphone());
				pst.setString(11, lstCustomer.get(i).getCust_ds_phonenumber());
				pst.setString(12, lstCustomer.get(i).getCust_ds_customernumber());
				pst.setString(13, lstCustomer.get(i).getCust_ds_impadicind());
				pst.setString(14, lstCustomer.get(i).getcust_ds_ibgecode());
				pst.setString(15, lstCustomer.get(i).getCust_ds_statecode());
				pst.setString(16, lstCustomer.get(i).getCust_ds_codcitycustomer());
				pst.setString(17, lstCustomer.get(i).getCust_ds_citydescription());
				pst.setString(18, "N");
				pst.setString(19, nmArquivo);
				
				pst.execute();
				pst.close();
				
				log.info("[insertCustomer] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[insertCustomer] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		//conn.close();
		
		log.info("[insertCustomer] - FIM ");
		
	}

}
