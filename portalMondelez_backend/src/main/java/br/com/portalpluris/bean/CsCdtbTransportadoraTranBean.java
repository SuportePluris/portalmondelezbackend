package br.com.portalpluris.bean;

import java.sql.Timestamp;

public class CsCdtbTransportadoraTranBean {

	int tran_id_cd_transportadora;
	String tran_id_customer;
	String tran_ds_remitto;
	String tran_nm_nometransportadora;
	String tran_ds_contrato;
	String tran_ds_codigotransportadora;
	String tran_dh_registro;
	String tran_in_inativo;
	int tran_id_cd_usuarioatualizacao;
	String tran_dh_atualizacao;
	String tran_ds_cnpj;
	String tran_ds_cockpit;
	String tran_ds_status;
	String tran_ds_contato;
	String tran_ds_uf;
	String tran_ds_email;
	String tran_ds_telefone;

	
	
	
	public String getTran_ds_contato() {
		return tran_ds_contato;
	}

	public void setTran_ds_contato(String tran_ds_contato) {
		this.tran_ds_contato = tran_ds_contato;
	}

	public String getTran_ds_uf() {
		return tran_ds_uf;
	}

	public void setTran_ds_uf(String tran_ds_uf) {
		this.tran_ds_uf = tran_ds_uf;
	}

	public String getTran_ds_email() {
		return tran_ds_email;
	}

	public void setTran_ds_email(String tran_ds_email) {
		this.tran_ds_email = tran_ds_email;
	}

	public String getTran_ds_telefone() {
		return tran_ds_telefone;
	}

	public void setTran_ds_telefone(String tran_ds_telefone) {
		this.tran_ds_telefone = tran_ds_telefone;
	}

	public String getTran_ds_remitto() {
		return tran_ds_remitto;
	}

	public void setTran_ds_remitto(String tran_ds_remitto) {
		this.tran_ds_remitto = tran_ds_remitto;
	}

	public String getTran_ds_contrato() {
		return tran_ds_contrato;
	}

	public void setTran_ds_contrato(String tran_ds_contrato) {
		this.tran_ds_contrato = tran_ds_contrato;
	}

	public String getTran_ds_cockpit() {
		return tran_ds_cockpit;
	}

	public void setTran_ds_cockpit(String tran_ds_cockpit) {
		this.tran_ds_cockpit = tran_ds_cockpit;
	}

	public String getTran_ds_status() {
		return tran_ds_status;
	}

	public void setTran_ds_status(String tran_ds_status) {
		this.tran_ds_status = tran_ds_status;
	}

	public String getTran_id_customer() {
		return tran_id_customer;
	}

	public void setTran_id_customer(String tran_id_customer) {
		this.tran_id_customer = tran_id_customer;
	}

	public int getTran_id_cd_transportadora() {
		return tran_id_cd_transportadora;
	}

	public void setTran_id_cd_transportadora(int tran_id_cd_transportadora) {
		this.tran_id_cd_transportadora = tran_id_cd_transportadora;
	}

	public String getTran_nm_nometransportadora() {
		return tran_nm_nometransportadora;
	}

	public void setTran_nm_nometransportadora(String tran_nm_nometransportadora) {
		this.tran_nm_nometransportadora = tran_nm_nometransportadora;
	}

	public String getTran_ds_codigotransportadora() {
		return tran_ds_codigotransportadora;
	}

	public void setTran_ds_codigotransportadora(String tran_ds_codigotransportadora) {
		this.tran_ds_codigotransportadora = tran_ds_codigotransportadora;
	}

	public String getTran_dh_registro() {
		return tran_dh_registro;
	}

	public void setTran_dh_registro(String tran_dh_registro) {
		this.tran_dh_registro = tran_dh_registro;
	}

	public String getTran_in_inativo() {
		return tran_in_inativo;
	}

	public void setTran_in_inativo(String tran_in_inativo) {
		this.tran_in_inativo = tran_in_inativo;
	}

	public int getTran_id_cd_usuarioatualizacao() {
		return tran_id_cd_usuarioatualizacao;
	}

	public void setTran_id_cd_usuarioatualizacao(int tran_id_cd_usuarioatualizacao) {
		this.tran_id_cd_usuarioatualizacao = tran_id_cd_usuarioatualizacao;
	}

	public String getTran_dh_atualizacao() {
		return tran_dh_atualizacao;
	}

	public void setTran_dh_atualizacao(String tran_dh_atualizacao) {
		this.tran_dh_atualizacao = tran_dh_atualizacao;
	}

	public String getTran_ds_cnpj() {
		return tran_ds_cnpj;
	}

	public void setTran_ds_cnpj(String tran_ds_cnpj) {
		this.tran_ds_cnpj = tran_ds_cnpj;
	}
	
	

}
