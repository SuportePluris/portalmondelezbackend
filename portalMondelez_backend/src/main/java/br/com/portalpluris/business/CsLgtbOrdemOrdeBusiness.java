package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.portalpluris.bean.CsLgtbCustomerCustBean;
import br.com.portalpluris.bean.CsLgtbOrdemOrdeBean;
import br.com.portalpluris.bean.CsLgtbShipmentShipBean;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsLgtbCustomerCustDao;
import br.com.portalpluris.dao.CsLgtbOrdemOrdeDao;

public class CsLgtbOrdemOrdeBusiness {

	/**
	 * Metodo responsavel por realizar leitura e gravacao das informacoes de ordem disponibilizadas via arquivo TXT no banco de dados
	 * @author Caio Fernandes
	 * */
	public void gravarOrdem() {

		Logger log = Logger.getLogger(this.getClass().getName());	
		log.info("[gravarOrdem] - INICIO ");

		try {

			List<CsLgtbOrdemOrdeBean> lstOrdem = new ArrayList<CsLgtbOrdemOrdeBean>();
			CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
			UtilMoveArquivo utilMove = new UtilMoveArquivo();

			// CONFIGURACOES DE DIRETORIOS
			//String dirOrdem = "C:\\Users\\caraujo\\Desktop\\MONDELEZ_IMP\\SHIPMENT\\";
			String dirOrdem = confDao.slctConf("conf.importacao.agendamento.diretorio.ordem");
			String dirOrdemBkp = confDao.slctConf("conf.importacao.agendamento.diretorio.ordem.bkp");
			String dirOrdemErro = confDao.slctConf("conf.importacao.agendamento.diretorio.ordem.erro");

			// LEITURA DOS ARQUIVOS DO DIRETORIO
			File arquivo = new File(dirOrdem);
			String[] arq = arquivo.list();

			if(arq != null){

				CsLgtbCustomerCustBean customerBean = new CsLgtbCustomerCustBean();
				log.info("[gravarOrdem] - CARREGANDO ARQUIVOS TXT ");
				log.info( " ******** "+ arq.length + " ARQUIVOS(S) CARREGADOS(S). ******* ");

				for (int i = 0; i < arq.length; i++) {
					
					// ARQUIVO A SER PROCESSADO
					File arquivoProcessado = new File(dirOrdem + "\\" +  arq[i]);
					
					try {

						if(arquivoProcessado.getName().toUpperCase().endsWith(".TXT")){
							
							String nmArquivo = arquivoProcessado.getName();
							log.info("INICIANDO LEITURA DO ARQUIVO TXT: " + nmArquivo);	
							
							// INSTANCIAR ARQUIVO
							FileInputStream fileInputStream = new FileInputStream(arquivoProcessado);
							BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
							
							// CONTROLES DO ARQUIVO
							String conteudo = "";
							int linha = 0;

							// FOR DAS LINHAS DO ARQUIVO
							while (((conteudo = bufferedReader.readLine()) != null)) {

								try{
									
									linha++;
									log.info("LENDO CONTEUDO DO ARQUIVO ");
									log.info("CONTEUDO DA LINHA: " + linha + "" + conteudo);	

									// SETAR VALORES DO ARQUIVO NO BEAN
									lstOrdem = setListaOrdemBean(lstOrdem, conteudo);
									
								} catch (Exception e) {
									log.info("ERRO NA LEITURA DA LINHA:"+ linha);
									log.info("Erro:"+e);
									e.printStackTrace();				

								}
							}		
							
							// REALIZAR O INSERT DA LISTA NO BANCO
							CsLgtbOrdemOrdeDao ordemOrdeDao = new CsLgtbOrdemOrdeDao();
							ordemOrdeDao.insertOrdem(lstOrdem, nmArquivo);
							
							// MOVER ARQUIVO PARA PASTA BKP
							File arquivoProcessadoBkp = new File(dirOrdemBkp + "\\" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoBkp);
						}
						
					} catch (Exception e) {
						log.info("[gravarOrdem] - ERRO AO LER AQUIVO CUSTOMER ");
						
						// MOVER ARQUIVO PARA PASTA DE ERRO
						if(arq != null) {
							File arquivoProcessadoErro = new File(dirOrdemErro + "\\" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoErro);
						}
						
					}

				}
			}

		} catch (Exception e) {
			log.info("[gravarOrdem] - ERRO AO GRAVAR CUSTOMER ");
		}

	}

	/**
	 * Metodo responsavel por obter os valores do arquivo e retornar a lista de CsLgtbOrdemOrdeBean preenchida.
	 * @author Caio Fernandes
	 * @param List<CsLgtbOrdemOrdeBean>, String, String
	 * @return List<CsLgtbOrdemOrdeBean>
	 * @throws ParseException 
	 * */
	private List<CsLgtbOrdemOrdeBean> setListaOrdemBean(List<CsLgtbOrdemOrdeBean> lstOrdem, String conteudo) throws ParseException {
			
		// LEITURA DOS CAMPOS DO ARQUIVO
		CsLgtbOrdemOrdeBean ordemBean = new CsLgtbOrdemOrdeBean();
		UtilDate utilDate = new UtilDate();
		
		String[] conteudoSplit = conteudo.split(";");
		
		ordemBean.setOrde_ds_plant(conteudoSplit[0]);
		ordemBean.setOrde_ds_salesdocumenttype(conteudoSplit[1]);
		ordemBean.setOrde_ds_salesdocumentnumber(conteudoSplit[2]);
		ordemBean.setOrde_ds_item(conteudoSplit[3]);
		ordemBean.setOrde_ds_overallprocessingstatus(conteudoSplit[4]);
		ordemBean.setOrde_ds_entrydate(conteudoSplit[5]);
		ordemBean.setOrde_ds_soldtoparty(conteudoSplit[6]);
		ordemBean.setOrde_ds_materialnumber(conteudoSplit[7]);
		ordemBean.setOrde_ds_personnelnumber(conteudoSplit[8]);
		ordemBean.setOrde_ds_billingdate(conteudoSplit[9]);		
		ordemBean.setOrde_dh_requestdeliverydate(utilDate.convertStringToDateYYYY_MM_dd_HH_MM_SS(conteudoSplit[10]));
		ordemBean.setOrde_dh_rddfromcustomer(utilDate.convertStringToDateYYYY_MM_dd_HH_MM_SS(conteudoSplit[11]));
		ordemBean.setOrde_dh_deliveryconfirmationdate(utilDate.convertStringToDateYYYY_MM_dd_HH_MM_SS(conteudoSplit[12]));
		ordemBean.setOrde_dh_deliveryscheduledate(utilDate.convertStringToDateYYYY_MM_dd_HH_MM_SS(conteudoSplit[13]));
		ordemBean.setOrde_ds_volumoftheitem(conteudoSplit[14]);
		ordemBean.setOrde_ds_netvalue(conteudoSplit[15]);
		ordemBean.setOrde_ds_discount(conteudoSplit[16]);
		ordemBean.setOrde_ds_icmivalue(conteudoSplit[17]);
		ordemBean.setOrde_ds_ipi3value(conteudoSplit[18]);
		ordemBean.setOrde_ds_ips3value(conteudoSplit[19]);
		ordemBean.setOrde_ds_icn3value(conteudoSplit[20]);
		ordemBean.setOrde_ds_ics3value(conteudoSplit[21]);
		ordemBean.setOrde_ds_customerponumber(conteudoSplit[22]);
		ordemBean.setOrde_ds_percentagediscountitem(conteudoSplit[23]);
		ordemBean.setOrde_ds_maxallowed(conteudoSplit[24]);
		ordemBean.setOrde_ds_minallowed(conteudoSplit[25]);
		ordemBean.setOrde_ds_simulatednumber(conteudoSplit[26]);
		ordemBean.setOrde_ds_sourcenfnumber(conteudoSplit[27]);
		ordemBean.setOrde_ds_sourcenfseries(conteudoSplit[28]);
		ordemBean.setOrde_ds_cancellingreason(conteudoSplit[29]);
		ordemBean.setOrde_ds_commercialpolicy(conteudoSplit[30]);
		ordemBean.setOrde_ds_deliveryoccurance(conteudoSplit[31]);
		ordemBean.setOrde_ds_deliveryoccurancesolndate(conteudoSplit[32]);
		ordemBean.setOrde_ds_delivery(conteudoSplit[33]);
		ordemBean.setOrde_ds_billingdocument(conteudoSplit[34]);
		ordemBean.setOrde_ds_ninedigitnfenumber(conteudoSplit[35]);
		ordemBean.setOrde_ds_series(conteudoSplit[36]);
		ordemBean.setOrde_ds_ordernumber(conteudoSplit[37]);
		ordemBean.setOrde_ds_orderitemreasoncode2(conteudoSplit[38]);
		ordemBean.setOrde_ds_derquantitysalesunits(conteudoSplit[39]);
		ordemBean.setOrde_ds_salesunit(conteudoSplit[40]);
		ordemBean.setOrde_ds_salesorganization(conteudoSplit[41]);
		ordemBean.setOrde_ds_distributionchannel(conteudoSplit[42]);
		ordemBean.setOrde_ds_division(conteudoSplit[43]);
		ordemBean.setOrde_ds_customerpotype(conteudoSplit[44]);
		ordemBean.setOrde_ds_quantidade(conteudoSplit[45]);
		ordemBean.setOrde_ds_orderreason(conteudoSplit[46]);
		ordemBean.setOrde_ds_reasonrejection(conteudoSplit[47]);
		ordemBean.setOrde_ds_backordernumber(conteudoSplit[48]);
		ordemBean.setOrde_ds_termspaymentkey(conteudoSplit[49]);
		
		lstOrdem.add(ordemBean);
		
		return lstOrdem;
	}

}
