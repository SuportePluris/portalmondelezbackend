package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoSkudBean;
import br.com.portalpluris.bean.RetornoSkudXLSBean;
import br.com.portalpluris.bean.XlsCabecalhoBean;

public class CsCdtbSkudDeliveryDao {

	public List<RetornoSkudBean> slcSkudByDelivery(String idDelivery) {
	
		List<RetornoSkudBean> listRetSku = new ArrayList<RetornoSkudBean>();
			
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraEmailDao");
		logsBean.setLogsDsMetodo("slctTransMail");
		logsBean.setLogsInErro("N");
		
		log.info("***** Inicio Consulta na base SKUD *****");	

		try {
		
			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "SELECT";
			cSQL += " SKUD_ID_CD_SHIPMENT,";
			cSQL += " SKUD_ID_CD_DELIVERY,";
			cSQL += " SKUD_DS_PESOBRUTO,";
			cSQL += " SKUD_DS_PESOLIQUIDO,";
			cSQL += " SKUD_DS_CODSKU,";
			cSQL += " SKUD_DS_ITEMSKU,";
			cSQL += " SKUD_DS_VOLUMECUBICO,";
			cSQL += " SKUD_NR_QUANTIDADE,";
			cSQL += " FORMAT(SKUD_DH_DELIVERY, 'dd/MM/yyyy') AS SKUD_DH_DELIVERY, ";
			cSQL += " SKUD_DS_PEDIDOMONDELEZ,";
			cSQL += " SKUD_DS_PEDIDOCLIENTE,";
			cSQL += " SKUD_DS_TIPOVENDA,";
			cSQL += " SKUD_DH_REGISTRO,";
			cSQL += " SKUD_DH_ATUALIZACAO,";
			cSQL += " SKUD_DS_VALOR";
			cSQL += " FROM CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
			cSQL += " WHERE SKUD_ID_CD_DELIVERY =  ?";
			cSQL += " 		AND SKUD_IN_CANCELADO =  'N' ";
			
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setLong(1, Long.parseLong(idDelivery));

			rs = pst.executeQuery();
			
			while(rs.next()){
				
				RetornoSkudBean retSku = new RetornoSkudBean();
				
				retSku = setSku(rs);
				
				
				listRetSku.add(retSku);
				
				
			}
			 
			
			
		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			daoLog.createLog(logsBean);

		}		
		
		return listRetSku;
	}
	
	
	
	public XlsCabecalhoBean slcCabecalhoToXLS(String id_embarque) {
		
		XlsCabecalhoBean cabecalhoBean = new XlsCabecalhoBean();
		
		try {
		
			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += " SELECT ";
			cSQL += " 	CNCL_DS_CNPJ, ";
			cSQL += " 	CNCL_NM_RAZAOSOCIAL, ";
			cSQL += " 	CLIE_DS_NOMECLIENTE, ";
			cSQL += " 	CLIE_DS_CIDADE, ";
			cSQL += " 	CLIE_DS_UF, ";
			cSQL += " 	CLIE_ID_CD_CODIGOCLIENTE, ";
			cSQL += " 	CNCL_DS_CHANEL, ";
			cSQL += " 	TRAN_DS_CNPJ, ";
			cSQL += " 	TRAN_NM_NOMETRANSPORTADORA ";
			cSQL += " FROM  ";
			cSQL += " 	CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) INNER JOIN CS_CDTB_CANALPORCLIENTE_CNCL CNCL (NOLOCK) ";
			cSQL += " 		ON EMBA_ID_CD_CODCLIENTE = CNCL_CD_CUSTUMER ";
			cSQL += " 	INNER JOIN CS_CDTB_TRANSPORTADORA_TRAN TRANS (NOLOCK) ";
			cSQL += " 		ON EMBA_DS_COD_TRANSPORTADORA = TRAN_DS_CODIGOTRANSPORTADORA ";
			cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE  (NOLOCK) ";
			cSQL += " 		ON CNCL_CD_CUSTUMER = CLIE_ID_CD_CODIGOCLIENTE ";
			cSQL += " WHERE  ";
			cSQL += " 	EMBA_ID_CD_EMBARQUE = ? ";
			
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setLong(1, Long.parseLong(id_embarque));

			rs = pst.executeQuery();
			
			while(rs.next()){
				
				cabecalhoBean = setXLSCabecalho(rs);
				
			}
			 
			
			
		} catch (Exception e) {
			

		} 	
		
		return cabecalhoBean;
	}

	private XlsCabecalhoBean setXLSCabecalho(ResultSet rs) throws SQLException {
		
		XlsCabecalhoBean bean = new XlsCabecalhoBean();
		
		bean.setCncl_ds_cnpj(rs.getString("cncl_ds_cnpj"));
		bean.setCncl_nm_razaosocial(rs.getString("cncl_nm_razaosocial"));
		
		bean.setClie_ds_nomecliente(rs.getString("clie_ds_nomecliente"));
		bean.setClie_ds_cidade(rs.getString("clie_ds_cidade"));
		bean.setClie_ds_uf(rs.getString("clie_ds_uf"));
		bean.setClie_id_cd_codigocliente(rs.getString("clie_id_cd_codigocliente"));
		bean.setCncl_ds_chanel(rs.getString("cncl_ds_chanel"));
		bean.setTran_ds_cnpj(rs.getString("tran_ds_cnpj"));
		bean.setTran_nm_nometransportadora(rs.getString("tran_nm_nometransportadora"));
		
		return bean;
	}
	
	
	public List<RetornoSkudXLSBean> slcSkudByDeliveryToXLS(String id_embarque) {
		
		List<RetornoSkudXLSBean> listRetSku = new ArrayList<RetornoSkudXLSBean>();
			
		Logger log = Logger.getLogger(this.getClass().getName());
	
		log.info("***** Inicio Consulta na base SKUD *****");	

		try {
		
			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "SELECT";
			cSQL += " 	SKUD_ID_CD_SHIPMENT,";
			cSQL += " 	SKUD_ID_CD_DELIVERY,";
			cSQL += " 	SKUD_DS_PESOBRUTO,";
			cSQL += " 	SKUD_DS_PESOLIQUIDO,";
			cSQL += " 	SKUD_DS_CODSKU,";
			cSQL += " 	SKUD_DS_ITEMSKU,";
			cSQL += " 	SKUD_DS_VOLUMECUBICO,";
			cSQL += " 	SKUD_NR_QUANTIDADE,";
			cSQL += " 	FORMAT(SKUD_DH_DELIVERY, 'dd/MM/yyyy') AS SKUD_DH_DELIVERY, ";
			cSQL += " 	SKUD_DS_PEDIDOMONDELEZ,";
			cSQL += " 	SKUD_DS_PEDIDOCLIENTE,";
			cSQL += " 	SKUD_DS_TIPOVENDA,";
			cSQL += " 	SKUD_DH_REGISTRO,";
			cSQL += " 	SKUD_DH_ATUALIZACAO,";
			cSQL += " 	SKUD_DS_VALOR,";
			
			cSQL += " 	NOFI_NR_NOTAFISCAL,";
			cSQL += " 	EMBA_DS_TIPOVEICULO";
			
			
			cSQL += " FROM ";
			cSQL += " 	CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK) INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
			cSQL += " 			ON SKUD.SKUD_ID_CD_SHIPMENT = EMBA.EMBA_ID_CD_SHIPMENT  ";
			cSQL += " 	LEFT JOIN CS_CDTB_NOTAFISCAL_NOFI NOFI (NOLOCK)";
			cSQL += " 		ON NOFI_NR_DELIVERYDOCUMENT  = SKUD_ID_CD_DELIVERY"; //558
			cSQL += " 		AND NOFI_NR_NUMPEDIDO  = SKUD_DS_PEDIDOMONDELEZ";    //557			
			cSQL += " WHERE EMBA_ID_CD_EMBARQUE =  ? ";
			cSQL += " 		AND SKUD_IN_CANCELADO =  'N' ";
			
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setLong(1, Long.parseLong(id_embarque));

			rs = pst.executeQuery();
			
			while(rs.next()){
				
				RetornoSkudXLSBean retSku = new RetornoSkudXLSBean();
				
				retSku = setSkuXLSCliente(rs);
				
				listRetSku.add(retSku);
				
			}
			 
			
			
		} catch (Exception e) {
			

		} 	
		
		return listRetSku;
	}

	private RetornoSkudBean setSku(ResultSet rs) throws SQLException {
		
		RetornoSkudBean retSku = new RetornoSkudBean();
		
			retSku.setSkud_dh_delivery(rs.getString("SKUD_DH_DELIVERY"));
			retSku.setSkud_dh_registro(rs.getString("SKUD_DH_REGISTRO"));
			retSku.setSkud_ds_codsku(rs.getString("SKUD_DS_CODSKU"));
			retSku.setSkud_ds_itemsku(rs.getString("SKUD_DS_ITEMSKU"));
			retSku.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE"));
			retSku.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ"));
			retSku.setSkud_ds_pesobruto(rs.getString("SKUD_DS_PESOBRUTO"));
			retSku.setSkud_ds_pesoliquido(rs.getString("SKUD_DS_PESOLIQUIDO"));
			retSku.setSkud_ds_tipovenda(rs.getString("SKUD_DS_TIPOVENDA"));
			retSku.setSkud_ds_valor(rs.getString("SKUD_DS_VALOR"));
			retSku.setSkud_ds_volumecubico(rs.getString("SKUD_DS_VOLUMECUBICO"));
			retSku.setSkud_id_cd_delivery(rs.getString("SKUD_ID_CD_DELIVERY"));
			retSku.setSkud_id_cd_shipment(rs.getString("SKUD_ID_CD_SHIPMENT"));
			retSku.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE"));
		
		
		return retSku;
	}

	private RetornoSkudXLSBean setSkuXLSCliente(ResultSet rs) throws SQLException {
		
		RetornoSkudXLSBean retSku = new RetornoSkudXLSBean();
		
			retSku.setSkud_dh_delivery(rs.getString("SKUD_DH_DELIVERY"));
			retSku.setSkud_dh_registro(rs.getString("SKUD_DH_REGISTRO"));
			retSku.setSkud_ds_codsku(rs.getString("SKUD_DS_CODSKU"));
			retSku.setSkud_ds_itemsku(rs.getString("SKUD_DS_ITEMSKU"));
			retSku.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE"));
			retSku.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ"));
			retSku.setSkud_ds_pesobruto(rs.getString("SKUD_DS_PESOBRUTO"));
			retSku.setSkud_ds_pesoliquido(rs.getString("SKUD_DS_PESOLIQUIDO"));
			retSku.setSkud_ds_tipovenda(rs.getString("SKUD_DS_TIPOVENDA"));
			retSku.setSkud_ds_valor(rs.getString("SKUD_DS_VALOR"));
			retSku.setSkud_ds_volumecubico(rs.getString("SKUD_DS_VOLUMECUBICO"));
			retSku.setSkud_id_cd_delivery(rs.getString("SKUD_ID_CD_DELIVERY"));
			retSku.setSkud_id_cd_shipment(rs.getString("SKUD_ID_CD_SHIPMENT"));
			retSku.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE"));
			
			retSku.setNofi_nr_notafiscal(rs.getString("NOFI_NR_NOTAFISCAL"));
			retSku.setEmba_ds_tipoveiculo(rs.getString("EMBA_DS_TIPOVEICULO"));
		
					
		return retSku;
	}



	public String slctSkudByDelivery(String nrDeliveryDocument,String idShipment) throws SQLException {
					
			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += " SELECT TOP 1 ";
			cSQL += " 	SKUD_ID_CD_SHIPMENT";					
			cSQL += " FROM ";
			cSQL += " 	CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK) ";				
			cSQL += " WHERE SKUD_ID_CD_DELIVERY =  ? ";
			
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1,nrDeliveryDocument);

			rs = pst.executeQuery();
			
			while(rs.next()){
				
				idShipment = rs.getString("SKUD_ID_CD_SHIPMENT");
			
			}
	
		return idShipment;
	}
}
