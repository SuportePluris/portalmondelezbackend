package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.CsCdtbRegiaoRegiBean;

public class CsCdtbRegiaoRegiDao {

	public List<CsCdtbRegiaoRegiBean> getListRegiao(String inativo) throws SQLException {

		List<CsCdtbRegiaoRegiBean> listBean = new ArrayList<CsCdtbRegiaoRegiBean>();

			
			Connection conn = DAO.getConexao();
			ResultSet rs = null;

			PreparedStatement pst;
			String query = "" 
				+ "	SELECT "
				+ "		ID_REGI_CD_REGIAO, "
				+ "		REGI_DS_REGIAO, "
				+ "		REGI_IN_INATIVO, "
				+ "		FORMAT (REGI_DH_REGISTRO, 'dd/MM/yyyy') AS REGI_DH_REGISTRO "
				+ "	FROM "
				+ "		CS_CDTB_REGIAO_REGI (NOLOCK) "
				+ "	WHERE "
				+ "		REGI_IN_INATIVO = ? "
				+ "	ORDER BY "
				+ "		REGI_DS_REGIAO "
				+ "";
			
			pst = conn.prepareStatement(query);
			pst.setString(1, inativo);
			
			rs = pst.executeQuery();

			while (rs.next()) {

				CsCdtbRegiaoRegiBean regiaoBean = setRegiaoBean(rs);

				listBean.add(regiaoBean);
			}

			return listBean;
		}

	private CsCdtbRegiaoRegiBean setRegiaoBean(ResultSet rs) throws SQLException {
		
		CsCdtbRegiaoRegiBean regiaoBean = new CsCdtbRegiaoRegiBean();
		
		regiaoBean.setId_regi_cd_regiao(rs.getInt("ID_REGI_CD_REGIAO"));
		regiaoBean.setRegi_dh_registro(rs.getString("REGI_DH_REGISTRO"));
		regiaoBean.setRegi_ds_regiao(rs.getString("REGI_DS_REGIAO"));
		regiaoBean.setRegi_in_inativo(rs.getString("REGI_IN_INATIVO"));
		
		return regiaoBean;
	}
}
