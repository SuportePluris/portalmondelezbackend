package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsAstbUsuariocanalUscaBean;
import br.com.portalpluris.bean.CsCdtbCanalCanaBean;
import br.com.portalpluris.bean.CsCdtbTipoTipoBean;
import br.com.portalpluris.bean.CsCdtbTransportadoraTranBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsAstbUsuariocanalUscaDao;
import br.com.portalpluris.dao.CsCdtbCanalCanaDao;
import br.com.portalpluris.dao.CsCdtbTipoTipoDao;
import br.com.portalpluris.dao.CsCdtbTransportadoraTranDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbCanalCanaBusiness {

	CsCdtbCanalCanaDao dao = new CsCdtbCanalCanaDao();
	private DAOLog daoLog = new DAOLog();

	public String getListCanal(String inativo, String idUsuario) throws JsonProcessingException {

		CsCdtbCanalCanaDao dao = new CsCdtbCanalCanaDao();
		List<CsCdtbCanalCanaBean> listBean = new ArrayList<CsCdtbCanalCanaBean>();

		String jsonRetorno = "";

		try {

			listBean = dao.getListCanal(inativo, idUsuario);

		} catch (Exception e) {
			e.printStackTrace();

		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listBean);

		return jsonRetorno;
		
	}
	
	public String getListCanal(String inativo) throws JsonProcessingException {

		CsCdtbCanalCanaDao dao = new CsCdtbCanalCanaDao();
		List<CsCdtbCanalCanaBean> listBean = new ArrayList<CsCdtbCanalCanaBean>();

		String jsonRetorno = "";

		try {

			listBean = dao.getListCanal(inativo);

		} catch (Exception e) {
			e.printStackTrace();

		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listBean);

		return jsonRetorno;
		
	}
	
	
	public String getListCanalByUsuario(String idUsuario) throws JsonProcessingException {

		CsAstbUsuariocanalUscaDao dao = new CsAstbUsuariocanalUscaDao();
		List<CsAstbUsuariocanalUscaBean> listBean = new ArrayList<CsAstbUsuariocanalUscaBean>();

		String jsonRetorno = "";

		try {

			listBean = dao.getListUsuariosPorCanal(idUsuario);

		} catch (Exception e) {
			e.printStackTrace();

		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listBean);

		return jsonRetorno;
		
	}
	
	public String createCanal(CsCdtbCanalCanaBean bean) throws JsonProcessingException {

		CsCdtbCanalCanaDao dao = new CsCdtbCanalCanaDao();
		List<CsCdtbCanalCanaBean> listBean = new ArrayList<CsCdtbCanalCanaBean>();

		String jsonRetorno = "";
		RetornoBean retBean = new RetornoBean();
		
		try {

			retBean = dao.createCanal(bean);

		} catch (Exception e) {
			e.printStackTrace();

		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		return jsonRetorno;
		
	}
	
	public String updateCanal(CsCdtbCanalCanaBean bean) throws JsonProcessingException {

		CsCdtbCanalCanaDao dao = new CsCdtbCanalCanaDao();
		List<CsCdtbCanalCanaBean> listBean = new ArrayList<CsCdtbCanalCanaBean>();

		String jsonRetorno = "";
		RetornoBean retBean = new RetornoBean();
		
		try {

			retBean = dao.updateCanal(bean);

		} catch (Exception e) {
			e.printStackTrace();

		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		return jsonRetorno;
		
	}
	
	


}
