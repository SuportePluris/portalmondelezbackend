package br.com.portalpluris.dao;

import java.sql.Connection;
import java.util.logging.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsNgtbEmbarqueAgendamentoEmbaBean;
import br.com.portalpluris.bean.CsNgtbEmbarqueEmbaBean;
import br.com.portalpluris.bean.EmbaShipEmshBean;
import br.com.portalpluris.bean.EmbarqueAgendamentoBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.NotaFiscalBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.RetornoDeliveryBean;
import br.com.portalpluris.bean.RetornoShipmentBean;
import br.com.portalpluris.bean.RetornoShipmentDeliveryBean;
import br.com.portalpluris.bean.StatusEmbarqueBean;

public class CsNgtbEmbarqueEmbaDao {

	public List<RetornoShipmentBean> getEmba(String emba_id_cd_status, String idFuncionario) throws SQLException {

		List<RetornoShipmentBean> retornoShipmentBean = new ArrayList<RetornoShipmentBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		
		String cSQL = "";
		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL,";
		cSQL += " CONVERT(VARCHAR(10), EMBA_DH_REGISTRO,103) AS SKUD_DH_REGISTRO, ";
		cSQL += "CASE ";
		cSQL += "	WHEN (SELECT COUNT(EMBA2.EMBA_ID_CD_CODCLIENTE) FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA2 (NOLOCK) WHERE EMBA2.EMBA_ID_CD_SHIPMENT = EMBA.EMBA_ID_CD_SHIPMENT) > 1 THEN 'S' ";
		cSQL += "	ELSE 'N'";
		cSQL += "END AS 'MULTISTOP' ";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS ";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += " WHERE EMBA.EMBA_ID_CD_STATUS = ?";
		cSQL += " AND EMBA.EMBA_IN_USO = 'N'";
		cSQL += " AND CLIE.CLIE_IN_INATIVO = 'N'";
		
		//SE NÃO POSSUIR REGIÃO E CANAL, VÊ TUDO
		csCdtbUsuarioUserDao userDao = new csCdtbUsuarioUserDao();
		ResultSet result = userDao.getRegiaoCanalByUser(idFuncionario);
		
		if(result.next()) {
			
			cSQL += " AND CLIE.CLIE_ID_CD_CODIGOCLIENTE IN (";
			
				cSQL += " SELECT ";
				cSQL += " 	DISTINCT CNCL_CD_CUSTUMER ";
				cSQL += " FROM ";
				cSQL += " 	CS_CDTB_CANALPORCLIENTE_CNCL CNCL (NOLOCK) ";
				cSQL += " 	INNER JOIN CS_CDTB_REGIAO_REGI REGI (NOLOCK) ";
				cSQL += " 		ON UPPER(LTRIM(RTRIM(CNCL.CNCL_DS_REGIAOCANAL))) = UPPER(LTRIM(RTRIM(REGI.REGI_DS_REGIAO))) ";
				cSQL += " 	INNER JOIN CS_CDTB_CANAL_CANA CANA (NOLOCK) ";
				cSQL += " 		ON UPPER(LTRIM(RTRIM(CNCL.CNCL_DS_CHANEL))) = UPPER(LTRIM(RTRIM(CANA.CANA_DS_CANAL))) ";
				cSQL += " 	INNER JOIN CS_ASTB_USERREGIAOCANAL_URCS URCS (NOLOCK) ";
				cSQL += " 		ON REGI.ID_REGI_CD_REGIAO = URCS.URCS_ID_CD_REGIAO ";
				cSQL += " 		AND CANA.CANA_ID_CD_CANAL = URCS.URCS_ID_CD_CANAL ";
				cSQL += " WHERE ";
				cSQL += " 	URCS.URCS_ID_CD_USUARIO = '" + idFuncionario + "' ";
			 
			cSQL += ")";
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,CONVERT(VARCHAR(10), EMBA_DH_REGISTRO,103),STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";

		pst = conn.prepareStatement(cSQL);
		pst.setString(1, emba_id_cd_status);

		rs = pst.executeQuery();

		while (rs.next()) {

			RetornoShipmentBean retShipmentBean = setRetornoShipmentBean(rs);

			retornoShipmentBean.add(retShipmentBean);
		}

		return retornoShipmentBean;
	}

	private RetornoShipmentBean setRetornoShipmentBean(ResultSet rs) throws SQLException {

		RetornoShipmentBean retShipmentBean = new RetornoShipmentBean();

		retShipmentBean.setEmba_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT"));
		retShipmentBean.setEmba_ds_veiculo(rs.getString("EMBA_DS_VEICULO"));
		retShipmentBean.setEmba_ds_tipoveiculo(rs.getString("EMBA_DS_TIPOVEICULO"));
		retShipmentBean.setEmba_in_transportadoraconfirmada(rs.getString("EMBA_IN_TRANSPORTADORACONFIRMADA"));
		retShipmentBean.setEmba_ds_tipocarga(rs.getString("EMBA_DS_TIPOCARGA"));
		retShipmentBean.setEmba_ds_cod_transportadora(rs.getString("EMBA_DS_COD_TRANSPORTADORA"));
		retShipmentBean.setEmba_id_cd_codcliente(rs.getString("EMBA_ID_CD_CODCLIENTE"));
		retShipmentBean.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
		retShipmentBean.setEmba_dh_leadtime(rs.getString("EMBA_DH_LEADTIME"));
		retShipmentBean.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
		retShipmentBean.setSkud_ds_pesobruto(rs.getString("PESO_BRUTO"));
		retShipmentBean.setSkud_ds_pesoliquido(rs.getString("PESO_LIQUIDO"));
		retShipmentBean.setSkud_ds_volumecubico(rs.getString("VALOR_CUBICO"));
		retShipmentBean.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE"));
		retShipmentBean.setSkud_ds_valor(rs.getString("VALOR_TOTAL"));
		retShipmentBean.setEmba_id_cd_embarque(rs.getString("EMBA_ID_CD_EMBARQUE"));
		retShipmentBean.setSkud_dh_registro(rs.getString("SKUD_DH_REGISTRO"));
		retShipmentBean.setMultistop(rs.getString("MULTISTOP"));
		
		return retShipmentBean;
	}

	public List<CsNgtbEmbarqueEmbaBean> getListEmba(int embaIdCdStatusagendamento) throws SQLException {

		List<CsNgtbEmbarqueEmbaBean> lstEmbaBean = new ArrayList<CsNgtbEmbarqueEmbaBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;

		pst = conn.prepareStatement("" + " SELECT " + " 	EMBA_ID_CD_EMBARQUE, " + " 	EMBA_DH_REGISTRO, "
				+ " 	EMBA_ID_CD_SHIPMENT, " + " 	EMBA_ID_CD_USUARIO, " + " 	EMBA_DH_AGENDAMENTO, "
				+ " 	EMBA_DH_REAGENDAMENTO_I, " + " 	EMBA_DH_REAGENDAMENTO_II, " + " 	EMBA_DH_REAGENDAMENTO_III, "
				+ " 	EMBA_ID_CD_STATUSAGENGAMENTO, " + " 	EMBA_DS_SENHATRANSPORTADORA, " + " 	EMBA_IN_CUTOFF, "
				+ " 	EMBA_DS_PERIODO, " + " 	EMBA_DH_ATUALIZACAO, " + " 	EMBA_ID_CD_USUARIOATUALIZACAO " + " FROM "
				+ " 	CS_NGTB_EMBARQUE_EMBA (NOLOCK) " + "	WHERE " + "		EMBA_ID_CD_STATUSAGENGAMENTO = ? "
				+ "");

		pst.setInt(1, embaIdCdStatusagendamento);

		rs = pst.executeQuery();

		while (rs.next()) {

			CsNgtbEmbarqueEmbaBean embaBean = setEmbaBean(rs);

			lstEmbaBean.add(embaBean);
		}

		return lstEmbaBean;
	}

	private CsNgtbEmbarqueEmbaBean setEmbaBean(ResultSet rs) throws SQLException {

		CsNgtbEmbarqueEmbaBean embaBean = new CsNgtbEmbarqueEmbaBean();

		embaBean.setEmbaDhAgendamento(rs.getTimestamp("EMBA_DH_AGENDAMENTO"));
		embaBean.setEmbaDhReagendamentoI(rs.getTimestamp("EMBA_DH_REAGENDAMENTO_I"));
		embaBean.setEmbaDhReagendamentoII(rs.getTimestamp("EMBA_DH_REAGENDAMENTO_II"));
		embaBean.setEmbaDhReagendamentoIII(rs.getTimestamp("EMBA_DH_REAGENDAMENTO_III"));
		embaBean.setEmbaDhRegistro(rs.getTimestamp("EMBA_DH_REGISTRO"));
		embaBean.setEmbaDsPeriodo(rs.getString("EMBA_DS_PERIODO"));
		embaBean.setEmbaDsSenhatransportadora(rs.getString("EMBA_DS_SENHATRANSPORTADORA"));
		embaBean.setEmbaIdCdEmbarque(rs.getInt("EMBA_ID_CD_EMBARQUE"));
		embaBean.setEmbaIdCdShipment(rs.getInt("EMBA_ID_CD_SHIPMENT"));
		embaBean.setEmbaIdCdStatusagendamento(rs.getInt("EMBA_ID_CD_STATUSAGENGAMENTO"));
		embaBean.setEmbaIdCdUsuario(rs.getInt("EMBA_ID_CD_USUARIO"));
		embaBean.setEmbaIdCdUsuarioatualizacao(rs.getInt("EMBA_ID_CD_USUARIOATUALIZACAO"));
		embaBean.setEmbaInCutoff(rs.getString("EMBA_IN_CUTOFF"));
		embaBean.setEmbaDhAtualizacao(rs.getTimestamp("EMBA_DH_ATUALIZACAO"));

		return embaBean;
	}

	public EmbaShipEmshBean getSomaEmba(String filtro, String criterio, String pa) throws SQLException {

		EmbaShipEmshBean enshBean = new EmbaShipEmshBean();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;

		String query = "" + "  SELECT  " + "  	EMBA.EMBA_ID_CD_EMBARQUE, " + "  	SHIP.SHIP_ID_CD_SHIPMENT, "
				+ "  	SHIP.SHIP_NR_PESO, " + "  	SHIP.SHIP_NR_CUBAGEM, " + "  	SHIP.SHIP_NR_VALOR, "
				+ "  	SHIP.SHIP_NR_VOLUMES, " + "  	SHIP.SHIP_DH_LEADTIME, " + "  	SHIP.SHIP_DS_TIPOCARGA, "
				+ "  	SHIP.SHIP_DS_CODIGOTRANSPORTADORA, " + "  	TRANS.TRAN_NM_NOMETRANSPORTADORA, "
				+ "  	CLIE.CLIE_DS_NOMECLIENTE, " + "		SHIP.SHIP_ID_CD_DELIVERYNOTE " + "  FROM "
				+ "  	CS_NGTB_EMBARQUE_EMBA EMBA (NOLOCK) " + "  	INNER JOIN CS_CDTB_SHIPMENT_SHIP SHIP (NOLOCK) "
				+ "  		ON EMBA.EMBA_ID_CD_SHIPMENT = SHIP.SHIP_ID_CD_SHIPMENT "
				+ "  	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) "
				+ "  		ON SHIP.SHIP_ID_CD_CODIGOCLIENTE = CLIE.CLIE_ID_CD_CODIGOCLIENTE "
				+ "  	INNER JOIN CS_CDTB_TRANSPORTADORA_TRAN TRANS (NOLOCK) "
				+ "  		ON SHIP.SHIP_DS_CODIGOTRANSPORTADORA  = TRANS.TRAN_DS_CODIGOTRANSPORTADORA " + "";

		if (filtro.equalsIgnoreCase("embarque")) {
			query += " WHERE EMBA.EMBA_ID_CD_EMBARQUE = ? ";

		} else if (filtro.equalsIgnoreCase("shipment")) {
			query += " WHERE SHIP.SHIP_ID_CD_SHIPMENT = ? ";

		} else if (filtro.equalsIgnoreCase("status")) {
			query += " WHERE EMBA.EMBA_ID_CD_STATUSAGENGAMENTO = ? ";
		}

		// TODO ATRIBUIR O FILTRO DA PA QUANDO ESTIVER DEFINIDO

		pst = conn.prepareStatement(query);

		pst.setString(1, criterio);

		rs = pst.executeQuery();

		int i = 0;
		while (rs.next()) {

			if (i == 0) {
				enshBean.setClieDsNomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
				enshBean.setEmbaIdCdEmbarque(rs.getInt("EMBA_ID_CD_EMBARQUE"));
				enshBean.setShipDhLeadtime(rs.getTimestamp("SHIP_DH_LEADTIME"));
				enshBean.setShipDsTipocarga(rs.getString("SHIP_DS_TIPOCARGA"));
				enshBean.setShipDsCodigotransportadora(rs.getString("SHIP_DS_CODIGOTRANSPORTADORA"));
				enshBean.setShipIdCdShipment(rs.getInt("SHIP_ID_CD_SHIPMENT"));
				enshBean.setTranNmNometransportadora(rs.getString("TRAN_NM_NOMETRANSPORTADORA"));
				enshBean.setShipIdCdDeliverynote(rs.getString("SHIP_ID_CD_DELIVERYNOTE"));
			}

			enshBean.setShipNrCubagem(enshBean.getShipNrCubagem() + rs.getDouble("SHIP_NR_CUBAGEM"));
			enshBean.setShipNrPeso(enshBean.getShipNrPeso() + rs.getDouble("SHIP_NR_PESO"));
			enshBean.setShipNrValor(enshBean.getShipNrValor() + rs.getDouble("SHIP_NR_VALOR"));
			enshBean.setShipNrVolumes(enshBean.getShipNrVolumes() + rs.getDouble("SHIP_NR_VOLUMES"));

			i++;
		}

		return enshBean;
	}

	public List<EmbaShipEmshBean> getListEmba(String filtro, String criterio, String pa) throws SQLException {

		List<EmbaShipEmshBean> lstEnshBean = new ArrayList<EmbaShipEmshBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;

		String query = "" + "  SELECT  " + "  	EMBA.EMBA_ID_CD_EMBARQUE, " + "  	SHIP.SHIP_ID_CD_SHIPMENT, "
				+ "  	SHIP.SHIP_NR_PESO, " + "  	SHIP.SHIP_NR_CUBAGEM, " + "  	SHIP.SHIP_NR_VALOR, "
				+ "  	SHIP.SHIP_NR_VOLUMES, " + "  	SHIP.SHIP_DH_LEADTIME, " + "  	SHIP.SHIP_DS_TIPOCARGA, "
				+ "  	SHIP.SHIP_DS_CODIGOTRANSPORTADORA, " + "  	TRANS.TRAN_NM_NOMETRANSPORTADORA, "
				+ "  	CLIE.CLIE_DS_NOMECLIENTE, " + "		SHIP.SHIP_ID_CD_DELIVERYNOTE " + "  FROM "
				+ "  	CS_NGTB_EMBARQUE_EMBA EMBA (NOLOCK) " + "  	INNER JOIN CS_CDTB_SHIPMENT_SHIP SHIP (NOLOCK) "
				+ "  		ON EMBA.EMBA_ID_CD_SHIPMENT = SHIP.SHIP_ID_CD_SHIPMENT "
				+ "  	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) "
				+ "  		ON SHIP.SHIP_ID_CD_CODIGOCLIENTE = CLIE.CLIE_ID_CD_CODIGOCLIENTE "
				+ "  	INNER JOIN CS_CDTB_TRANSPORTADORA_TRAN TRANS (NOLOCK) "
				+ "  		ON SHIP.SHIP_DS_CODIGOTRANSPORTADORA  = TRANS.TRAN_DS_CODIGOTRANSPORTADORA " + "";

		if (filtro.equalsIgnoreCase("embarque")) {
			query += " WHERE EMBA.EMBA_ID_CD_EMBARQUE = ? ";

		} else if (filtro.equalsIgnoreCase("shipment")) {
			query += " WHERE SHIP.SHIP_ID_CD_SHIPMENT = ? ";

		} else if (filtro.equalsIgnoreCase("status")) {
			query += " WHERE EMBA.EMBA_ID_CD_STATUSAGENGAMENTO = ? ";
		}

		// TODO ATRIBUIR O FILTRO DA PA QUANDO ESTIVER DEFINIDO

		pst = conn.prepareStatement(query);

		pst.setString(1, criterio);

		rs = pst.executeQuery();

		while (rs.next()) {

			EmbaShipEmshBean enshBEan = getEmshbean(rs);

			lstEnshBean.add(enshBEan);

		}

		return lstEnshBean;
	}

	private EmbaShipEmshBean getEmshbean(ResultSet rs) throws SQLException {

		EmbaShipEmshBean enshBEan = new EmbaShipEmshBean();

		enshBEan.setClieDsNomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
		enshBEan.setEmbaIdCdEmbarque(rs.getInt("EMBA_ID_CD_EMBARQUE"));
		enshBEan.setShipDhLeadtime(rs.getTimestamp("SHIP_DH_LEADTIME"));
		enshBEan.setShipDsTipocarga(rs.getString("SHIP_DS_TIPOCARGA"));
		enshBEan.setShipDsCodigotransportadora(rs.getString("SHIP_DS_CODIGOTRANSPORTADORA"));
		enshBEan.setShipIdCdShipment(rs.getInt("SHIP_ID_CD_SHIPMENT"));
		enshBEan.setTranNmNometransportadora(rs.getString("TRAN_NM_NOMETRANSPORTADORA"));
		enshBEan.setShipNrCubagem(rs.getDouble("SHIP_NR_CUBAGEM"));
		enshBEan.setShipNrPeso(rs.getDouble("SHIP_NR_PESO"));
		enshBEan.setShipNrValor(rs.getDouble("SHIP_NR_VALOR"));
		enshBEan.setShipNrVolumes(rs.getDouble("SHIP_NR_VOLUMES"));
		enshBEan.setShipIdCdDeliverynote(rs.getString("SHIP_ID_CD_DELIVERYNOTE"));

		return enshBEan;

	}

	public List<EmbarqueAgendamentoBean> getLstAgendamento(int embaIdCdEmbarque) throws SQLException {

		List<EmbarqueAgendamentoBean> lstEmbaAgendamento = new ArrayList<EmbarqueAgendamentoBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;

		String query = ""
				+ " SELECT "
				+ " 	DAAG.DAAG_ID_CD_AGENDAMENTO, "
				+ " 	DAAG.DAAG_ID_CD_EMBARQUE, "
				+ " 	FORMAT(DAAG.DAAG_DH_AGENDAMENTO, 'dd/MM/yyyy') AS DAAG_DH_AGENDAMENTO, "
				+ " 	DAAG_ID_CD_MOTIVO, "
				+ " 	DAAG_ID_CD_RESPONSAVEL, "
				+ " 	DAAG.DAAG_DS_TIPOAGENDAMENTO, "
				+ " 	DAAG.DAAG_DS_SENHAAGENDAMENTO, "
				+ " 	DAAG.DAAG_DS_HORAAGENDAMENTO, "
				+ " 	EMBA.EMBA_IN_CUTOFF, "
				+ " 	EMBA.EMBA_ID_CD_MOTIVO, "
				+ " 	EMBA.EMBA_ID_CD_RESPONSAVEL, "
				+ " 	EMBA.EMBA_ID_CD_STATUS, "
				+ "		EMBA.EMBA_ID_CD_CODCLIENTE,"
				+ "		EMBA.EMBA_DS_COD_TRANSPORTADORA,"
				+ " 	STAT.STAT_DS_STATUS,"
				+ "		MOTIDAAG.MOTI_DS_MOTIVO, "
				+ "		RESPDAAG.RESP_NM_RESPONSAVEL "
				+ " FROM "
				+ " 	CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) "
				+ " 	INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) "
				+ " 		ON DAAG.DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE "
				+ " 	LEFT JOIN CS_CDTB_STATUSAGENDAMENTO_STAT STAT (NOLOCK) "
				+ " 		ON EMBA.EMBA_ID_CD_STATUS = STAT.STAT_ID_CD_STATUS "
				+ " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTIEMBA (NOLOCK) "
				+ " 		ON EMBA.EMBA_ID_CD_MOTIVO = MOTIEMBA.MOTI_ID_CD_MOTIVO "
				+ " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTIDAAG (NOLOCK) "
				+ " 		ON DAAG.DAAG_ID_CD_MOTIVO = MOTIDAAG.MOTI_ID_CD_MOTIVO "
				+ " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESPEMBA  (NOLOCK) "
				+ " 		ON EMBA.EMBA_ID_CD_RESPONSAVEL = RESPEMBA.RESP_ID_CD_RESPONSAVEL "
				+ " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESPDAAG  (NOLOCK) "
				+ " 		ON DAAG.DAAG_ID_CD_RESPONSAVEL = RESPDAAG.RESP_ID_CD_RESPONSAVEL "
				+ "	WHERE "
				+ "		EMBA.EMBA_ID_CD_EMBARQUE = ? "
				+ "	ORDER BY "
				+ "		DAAG.DAAG_DH_AGENDAMENTO DESC "; 

		pst = conn.prepareStatement(query);

		pst.setInt(1, embaIdCdEmbarque);

		rs = pst.executeQuery();

		while (rs.next()) {

			EmbarqueAgendamentoBean emagBean = getEmagbean(rs);
			lstEmbaAgendamento.add(emagBean);
		}

		return lstEmbaAgendamento;
	}

	public EmbarqueAgendamentoBean getAgendamento(int daag_id_cd_agendamento) throws SQLException {

		EmbarqueAgendamentoBean emagBean = new EmbarqueAgendamentoBean();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;

		String query = ""
				+ " SELECT "
				+ " 	DAAG.DAAG_ID_CD_AGENDAMENTO, "
				+ " 	DAAG.DAAG_ID_CD_EMBARQUE, "
				+ " 	FORMAT(DAAG.DAAG_DH_AGENDAMENTO, 'dd/MM/yyyy') AS DAAG_DH_AGENDAMENTO, "
				+ " 	DAAG_ID_CD_MOTIVO, "
				+ " 	DAAG_ID_CD_RESPONSAVEL, "
				+ " 	DAAG.DAAG_DS_TIPOAGENDAMENTO, "
				+ " 	DAAG.DAAG_DS_SENHAAGENDAMENTO, "
				+ " 	DAAG.DAAG_DS_HORAAGENDAMENTO, "
				+ " 	EMBA.EMBA_IN_CUTOFF, "
				+ " 	EMBA.EMBA_ID_CD_MOTIVO, "
				+ " 	EMBA.EMBA_ID_CD_RESPONSAVEL, "
				+ " 	EMBA.EMBA_ID_CD_STATUS, "
				+ " 	STAT.STAT_DS_STATUS, "
				+ "		MOTIDAAG.MOTI_DS_MOTIVO, "
				+ "		RESPDAAG.RESP_NM_RESPONSAVEL "
				+ " FROM "
				+ " 	CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK) "
				+ " 	INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) "
				+ " 		ON DAAG.DAAG_ID_CD_EMBARQUE = EMBA.EMBA_ID_CD_EMBARQUE "
				+ " 	LEFT JOIN CS_CDTB_STATUSAGENDAMENTO_STAT STAT (NOLOCK) "
				+ " 		ON EMBA.EMBA_ID_CD_STATUS = STAT.STAT_ID_CD_STATUS "
				+ " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTIEMBA (NOLOCK) "
				+ " 		ON EMBA.EMBA_ID_CD_MOTIVO = MOTIEMBA.MOTI_ID_CD_MOTIVO "
				+ " 	LEFT JOIN CS_CDTB_MOTIVO_MOTI MOTIDAAG (NOLOCK) "
				+ " 		ON DAAG.DAAG_ID_CD_MOTIVO = MOTIDAAG.MOTI_ID_CD_MOTIVO "
				+ " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESPEMBA  (NOLOCK) "
				+ " 		ON EMBA.EMBA_ID_CD_RESPONSAVEL = RESPEMBA.RESP_ID_CD_RESPONSAVEL "
				+ " 	LEFT JOIN CS_CDTB_RESPONSAVEL_RESP RESPDAAG  (NOLOCK) "
				+ " 		ON DAAG.DAAG_ID_CD_RESPONSAVEL = RESPDAAG.RESP_ID_CD_RESPONSAVEL "
				+ "	WHERE "
				+ "		DAAG.DAAG_ID_CD_AGENDAMENTO = ? "
				+ ""; 

		pst = conn.prepareStatement(query);

		pst.setInt(1, daag_id_cd_agendamento);

		rs = pst.executeQuery();

		if (rs.next()) {

			emagBean = getEmagbean(rs);
		}

		return emagBean;
	}

	public RetornoBean createUpdateAgendamento(EmbarqueAgendamentoBean embaBembaAgenBeanean) {

		
		
		RetornoBean retBean = new RetornoBean();

		retBean.setMsg("Agendamento criado com sucesso");
		retBean.setSucesso(true);

		try {

			createBkpEmbarque(embaBembaAgenBeanean.getDaag_id_cd_embarque());
			

			if (embaBembaAgenBeanean.getDaag_id_cd_agendamento() == 0) {
				createAgendamento(embaBembaAgenBeanean);
				updateEmbarque(embaBembaAgenBeanean);
			} else {
				updateAgendamento(embaBembaAgenBeanean);
				updateEmbarque(embaBembaAgenBeanean);
			}

		} catch (Exception e) {
			e.printStackTrace();
			retBean.setMsg(e.getMessage());
			retBean.setSucesso(false);
		}

		return retBean;
	}

	public void createBkpEmbarque(int emba_id_cd_embarque) throws SQLException {

		Connection conn = DAO.getConexao();
		
		String cSQL = "";
		cSQL += " INSERT INTO CS_NGTB_EMBARQUEHISTORICO_EMHI ( ";
		cSQL += " EMBA_ID_CD_EMBARQUE, ";
		cSQL += " EMBA_ID_CD_SHIPMENT, ";
		cSQL += " EMBA_DH_REGISTRO, ";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA, ";
		cSQL += " EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " EMBA_DH_LEADTIME, ";
		cSQL += " EMBA_DS_PLANTA, ";
		cSQL += " EMBA_DH_SHIPMENT, ";
		cSQL += " EMBA_DS_VEICULO, ";
		cSQL += " EMBA_DS_TIPOVEICULO, ";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA, ";
		cSQL += " EMBA_DH_ATUALIZACAO, ";
		cSQL += " EMBA_ID_CD_USUARIOATUALIZACAO, ";
		cSQL += " EMBA_DS_PERIODO, ";
		cSQL += " EMBA_IN_CUTOFF, ";
		cSQL += " EMBA_IN_USO, ";
		cSQL += " EMBA_ID_CD_STATUS, ";
		cSQL += " EMBA_DS_TIPOCARGA, ";
		cSQL += " EMBA_DS_TIPOAGENDAMENTO, ";
		cSQL += " EMBA_ID_CD_MOTIVO, ";
		cSQL += " EMBA_ID_CD_RESPONSAVEL, ";
		cSQL += " EMBA_ID_MOTIVOCUTOFF, ";
		cSQL += " EMBA_ID_RESPONSAVELCUTOFF, ";
		cSQL += " EMBA_IN_POSSUINF) ";
		cSQL += " SELECT * FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA (NOLOCK)	WHERE EMBA_ID_CD_EMBARQUE = ? ";

		
		PreparedStatement pst = conn.prepareStatement(cSQL);

		pst.setInt(1, emba_id_cd_embarque);

		pst.execute();
	}
	
	/**
	 * Metodo responsavel por criar um backup do registro da tabela CS_NGTB_EMBARQUEAGENDAMENTO_EMBA atraves do código de shipment passado via parametro
	 * @author Caio Fernandes
	 * @param int
	 * @throws SQLException
	 * */
	public void createBkpEmbarqueByIdShipment(String emba_id_cd_shipment) throws SQLException {

		Connection conn = DAO.getConexao();
		
		String cSQL = "";
		cSQL += " INSERT INTO CS_NGTB_EMBARQUEHISTORICO_EMHI ( ";
		cSQL += " EMBA_ID_CD_EMBARQUE, ";
		cSQL += " EMBA_ID_CD_SHIPMENT, ";
		cSQL += " EMBA_DH_REGISTRO, ";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA, ";
		cSQL += " EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " EMBA_DH_LEADTIME, ";
		cSQL += " EMBA_DS_PLANTA, ";
		cSQL += " EMBA_DH_SHIPMENT, ";
		cSQL += " EMBA_DS_VEICULO, ";
		cSQL += " EMBA_DS_TIPOVEICULO, ";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA, ";
		cSQL += " EMBA_DH_ATUALIZACAO, ";
		cSQL += " EMBA_ID_CD_USUARIOATUALIZACAO, ";
		cSQL += " EMBA_DS_PERIODO, ";
		cSQL += " EMBA_IN_CUTOFF, ";
		cSQL += " EMBA_IN_USO, ";
		cSQL += " EMBA_ID_CD_STATUS, ";
		cSQL += " EMBA_DS_TIPOCARGA, ";
		cSQL += " EMBA_DS_TIPOAGENDAMENTO, ";
		cSQL += " EMBA_ID_CD_MOTIVO, ";
		cSQL += " EMBA_ID_CD_RESPONSAVEL, ";
		cSQL += " EMBA_ID_MOTIVOCUTOFF, ";
		cSQL += " EMBA_ID_RESPONSAVELCUTOFF, ";
		cSQL += " EMBA_IN_POSSUINF) ";
		cSQL += " SELECT * FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA (NOLOCK)	WHERE EMBA_ID_CD_SHIPMENT = ? ";
		
		PreparedStatement pst = conn.prepareStatement(cSQL);

		pst.setString(1, emba_id_cd_shipment);
		
		pst.execute();
	}

	private void updateEmbarque(EmbarqueAgendamentoBean embaBembaAgenBeanean) throws SQLException, ParseException {

		Connection conn = DAO.getConexao();
		PreparedStatement pst = conn.prepareStatement(""
				+ " UPDATE "
				+ " 	CS_NGTB_EMBARQUEAGENDAMENTO_EMBA "
				+ " SET "
				+ " 	EMBA_IN_CUTOFF = ?, "
				+ " 	EMBA_ID_CD_MOTIVO = ?, "
				+ " 	EMBA_ID_CD_RESPONSAVEL = ?, "
				+ " 	EMBA_ID_CD_STATUS = ? "
				
				//DANILLO *** 29/07/2021 *** RETIRADO ATUALIZAÇÃO DO PERIODO
				//+ "		EMBA_DS_PERIODO = ?"
				+ " WHERE "
				+ " 	EMBA_ID_CD_EMBARQUE = ? "
				+ "");

		pst.setString(1, embaBembaAgenBeanean.getEmba_in_cutoff());
		
		if (embaBembaAgenBeanean.getDaag_id_cd_motivo() == 0) {
			pst.setNull(2, Types.INTEGER);
		} else {
			pst.setInt(2, embaBembaAgenBeanean.getDaag_id_cd_motivo());
		}
		
		if (embaBembaAgenBeanean.getDaag_id_cd_responsavel() == 0) {
			pst.setNull(3, Types.INTEGER);
		} else {
			pst.setInt(3, embaBembaAgenBeanean.getDaag_id_cd_responsavel());
		}
		
//		pst.setInt(2, embaBembaAgenBeanean.getEmba_id_cd_motivo());
//		pst.setInt(3, embaBembaAgenBeanean.getEmba_id_cd_responsavel());
		pst.setInt(4, embaBembaAgenBeanean.getEmba_id_cd_status());
		
		//DANILLO *** 29/07/2021 *** RETIRADO ATUALIZAÇÃO DO PERIODO
		//pst.setString(5, getPeriodo(embaBembaAgenBeanean.getDaag_dh_agendamento()));
		
		pst.setInt(5, embaBembaAgenBeanean.getDaag_id_cd_embarque());

		pst.execute();

	}

	private void createAgendamento(EmbarqueAgendamentoBean embaBembaAgenBeanean) throws SQLException {

		Connection conn = DAO.getConexao();
		PreparedStatement pst = conn.prepareStatement(""
				+ " INSERT INTO CS_NGTB_DATAAGENDAMENTO_DAAG ( "
				+ " 	DAAG_ID_CD_EMBARQUE, "
				+ " 	DAAG_DS_TIPOAGENDAMENTO, "
				+ " 	DAAG_DH_AGENDAMENTO, "
				+ " 	DAAG_ID_CD_MOTIVO, "
				+ " 	DAAG_ID_CD_RESPONSAVEL, "
				+ " 	DAAG_ID_CD_USUARIO, "
				+ " 	DAAG_DS_SENHAAGENDAMENTO, "
				+ " 	DAAG_DS_HORAAGENDAMENTO "
				+ " ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
				+ "");

		pst.setInt(1, embaBembaAgenBeanean.getDaag_id_cd_embarque());
		pst.setString(2, embaBembaAgenBeanean.getDaag_ds_tipoagendamento());
		pst.setString(3, embaBembaAgenBeanean.getDaag_dh_agendamento());
		
		if (embaBembaAgenBeanean.getDaag_id_cd_motivo() == 0) {
			pst.setNull(4, Types.INTEGER);
		} else {
			pst.setInt(4, embaBembaAgenBeanean.getDaag_id_cd_motivo());
		}
		
		if (embaBembaAgenBeanean.getDaag_id_cd_responsavel() == 0) {
			pst.setNull(5, Types.INTEGER);
		} else {
			pst.setInt(5, embaBembaAgenBeanean.getDaag_id_cd_responsavel());
		}
		
		
		
		pst.setInt(6, embaBembaAgenBeanean.getDaag_id_cd_usuario());
		pst.setString(7, embaBembaAgenBeanean.getDaag_ds_senhaagendamento());
		pst.setString(8, embaBembaAgenBeanean.getDaag_ds_horaagendamento());

		pst.execute();

	}

	private void updateAgendamento(EmbarqueAgendamentoBean embaBembaAgenBeanean) throws SQLException {

		Connection conn = DAO.getConexao();
		PreparedStatement pst = conn.prepareStatement(""
				+ " UPDATE "
				+ " 	CS_NGTB_DATAAGENDAMENTO_DAAG "
				+ " SET "
				+ " 	DAAG_DH_AGENDAMENTO = ?, "
				+ " 	DAAG_ID_CD_MOTIVO = ?, "
				+ " 	DAAG_ID_CD_RESPONSAVEL = ?, "
				+ " 	DAAG_DS_TIPOAGENDAMENTO = ?, "
				+ " 	DAAG_DS_SENHAAGENDAMENTO = ?, "
				+ " 	DAAG_ID_CD_USUARIO = ?, "
				+ " 	DAAG_DS_HORAAGENDAMENTO = ? "
				+ " WHERE "
				+ " 	DAAG_ID_CD_AGENDAMENTO = ? "
				+ "");

		pst.setString(1, embaBembaAgenBeanean.getDaag_dh_agendamento());
		
		if (embaBembaAgenBeanean.getDaag_id_cd_motivo() == 0) {
			pst.setNull(2, Types.INTEGER);
		} else {
			pst.setInt(2, embaBembaAgenBeanean.getDaag_id_cd_motivo());
		}
		
		if (embaBembaAgenBeanean.getDaag_id_cd_responsavel() == 0) {
			pst.setNull(3, Types.INTEGER);
		} else {
			pst.setInt(3, embaBembaAgenBeanean.getDaag_id_cd_responsavel());
		}
		
//		pst.setInt(2, embaBembaAgenBeanean.getDaag_id_cd_motivo());
//		pst.setInt(3, embaBembaAgenBeanean.getDaag_id_cd_responsavel());
		pst.setString(4, embaBembaAgenBeanean.getDaag_ds_tipoagendamento());
		pst.setString(5, embaBembaAgenBeanean.getDaag_ds_senhaagendamento());
		pst.setInt(6, embaBembaAgenBeanean.getDaag_id_cd_usuario());
		pst.setString(7, embaBembaAgenBeanean.getDaag_ds_horaagendamento());
		pst.setInt(8, embaBembaAgenBeanean.getDaag_id_cd_agendamento());
		

		pst.execute();

	}

	private EmbarqueAgendamentoBean getEmagbean(ResultSet rs) throws SQLException {

		EmbarqueAgendamentoBean emagBean = new EmbarqueAgendamentoBean();

		emagBean.setDaag_id_cd_agendamento(rs.getInt("DAAG_ID_CD_AGENDAMENTO"));
		emagBean.setDaag_dh_agendamento(rs.getString("DAAG_DH_AGENDAMENTO"));
		emagBean.setDaag_id_cd_motivo(rs.getInt("DAAG_ID_CD_MOTIVO"));
		emagBean.setDaag_id_cd_responsavel(rs.getInt("DAAG_ID_CD_RESPONSAVEL"));
		emagBean.setDaag_ds_senhaagendamento(rs.getString("DAAG_DS_SENHAAGENDAMENTO"));
		emagBean.setDaag_ds_tipoagendamento(rs.getString("DAAG_DS_TIPOAGENDAMENTO"));
		emagBean.setDaag_id_cd_embarque(rs.getInt("DAAG_ID_CD_EMBARQUE"));
		emagBean.setEmba_id_cd_motivo(rs.getInt("EMBA_ID_CD_MOTIVO"));
		emagBean.setEmba_id_cd_responsavel(rs.getInt("EMBA_ID_CD_RESPONSAVEL"));
		emagBean.setEmba_id_cd_status(rs.getInt("EMBA_ID_CD_STATUS"));
		emagBean.setEmba_in_cutoff(rs.getString("EMBA_IN_CUTOFF"));
		emagBean.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
		emagBean.setMoti_ds_motivo(rs.getString("MOTI_DS_MOTIVO"));
		emagBean.setResp_nm_responsavel(rs.getString("RESP_NM_RESPONSAVEL"));
		emagBean.setDaag_ds_horaagendamento(rs.getString("DAAG_DS_HORAAGENDAMENTO"));
		
		return emagBean;
	}

	public static String getPeriodo(String dataString) throws ParseException {

		String periodoMes = dataString.substring(5, 7);

		if (periodoMes.startsWith("0")) {
			periodoMes = periodoMes.substring(1, 2);
		}

		periodoMes = "P" + periodoMes;

		return periodoMes;
	}

	public RetornoShipmentBean slcEmbarqueShipment(String idEmbarque) {

		RetornoShipmentDeliveryBean retShipDeli = new RetornoShipmentDeliveryBean();
		RetornoShipmentBean retShip = new RetornoShipmentBean();

		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsNgtbEmbarqueEmbaDao");
		logsBean.setLogsDsMetodo("slcEmbarqueDelivery");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Consulta na base Embarque x Delivery *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = " ";
			cSQL += " SELECT ";
			cSQL += " 	ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO, ";
			cSQL += " 	ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO, ";
			cSQL += " 	ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO, ";
			cSQL += " 	ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE, ";
			cSQL += " 	ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL, ";
			cSQL += " 	(SELECT TOP 1 CONVERT(VARCHAR, FORMAT(DAAG_DH_AGENDAMENTO,'dd/MM/yyyy')) + ' ' + CONVERT(VARCHAR,DAAG_DS_HORAAGENDAMENTO) FROM CS_NGTB_DATAAGENDAMENTO_DAAG WHERE DAAG_ID_CD_EMBARQUE = EMBA_ID_CD_EMBARQUE ORDER BY DAAG_DH_REGISTRO DESC) AS DAAG_DH_AGENDAMENTO, "; 
			cSQL += " 	(SELECT TOP 1 DAAG_DS_SENHAAGENDAMENTO FROM CS_NGTB_DATAAGENDAMENTO_DAAG WHERE DAAG_ID_CD_EMBARQUE = EMBA_ID_CD_EMBARQUE ORDER BY DAAG_DH_REGISTRO DESC) AS DAAG_DS_SENHAAGENDAMENTO, ";

			cSQL += " 	EMBA_DS_PLANTA, ";
			cSQL += " 	EMBA_ID_CD_SHIPMENT, ";
			cSQL += " 	EMBA.EMBA_ID_CD_EMBARQUE, ";
			cSQL += " 	EMBA_DS_VEICULO, ";
			cSQL += " 	EMBA_DS_TIPOVEICULO, ";
			cSQL += " 	EMBA_IN_TRANSPORTADORACONFIRMADA, ";
			cSQL += " 	EMBA_DS_TIPOCARGA , ";
			cSQL += "  	EMBA_DS_COD_TRANSPORTADORA, ";
			cSQL += " 	EMBA_ID_CD_SHIPMENT, ";
			cSQL += " 	EMBA_ID_CD_STATUS, ";
			cSQL += " 	CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME, ";
			cSQL += " 	EMBA.EMBA_ID_CD_CODCLIENTE, ";
			cSQL += " 	EMBA.EMBA_ID_CD_STATUS ";
			cSQL += " 	CLIE_ID_CD_CODIGOCLIENTE,  ";
			cSQL += " 	CLIE_DS_CNPJ,  ";
			cSQL += " 	CLIE_DS_CIDADE,  ";
			cSQL += " 	CLIE_DS_UF, 	 ";
			cSQL += " 	CNCL_DS_CHANEL, ";
			cSQL += " 	STAT_DS_STATUS, ";
			cSQL += " 	CLIE_DS_NOMECLIENTE  ";
			cSQL += " FROM  ";
			cSQL += " 	CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)  ";
			cSQL += " 	INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) ";
			cSQL += " 		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT ";
			cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
			cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS ";
			cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
			cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE ";
			cSQL += "  	LEFT join CS_CDTB_CANALPORCLIENTE_CNCL CNCL (NOLOCK) ";
			cSQL += " 		ON EMBA_ID_CD_CODCLIENTE = CNCL_CD_CUSTUMER  ";
			cSQL += " WHERE  ";
			//cSQL += " 	SKUD_ID_CD_SHIPMENT = 7000962407 ";
			//cSQL += " 	AND EMBA_ID_CD_EMBARQUE = 20 ";
			cSQL += " 		EMBA_ID_CD_EMBARQUE = ? ";
			cSQL += " 		AND SKUD_IN_CANCELADO =  'N' ";
			cSQL += " GROUP BY  ";
			
			cSQL += " 	EMBA_DS_PLANTA, ";
			cSQL += " 	EMBA_ID_CD_SHIPMENT, ";
			cSQL += " 	EMBA.EMBA_ID_CD_EMBARQUE, ";
			cSQL += " 	EMBA_DS_VEICULO, ";
			cSQL += " 	EMBA_DS_TIPOVEICULO, ";
			cSQL += " 	EMBA_IN_TRANSPORTADORACONFIRMADA, ";
			cSQL += " 	EMBA_DS_TIPOCARGA , ";
			cSQL += "  	EMBA_DS_COD_TRANSPORTADORA, ";
			cSQL += " 	EMBA_ID_CD_SHIPMENT, ";
			cSQL += " 	EMBA_ID_CD_STATUS, ";
			cSQL += " 	CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103), ";
			cSQL += " 	EMBA.EMBA_ID_CD_CODCLIENTE, ";
			cSQL += " 	EMBA.EMBA_ID_CD_STATUS, ";
			cSQL += " 	CLIE_DS_CNPJ,  ";
			cSQL += " 	CLIE_DS_CIDADE,  ";
			cSQL += " 	CLIE_DS_UF, 	 ";
			cSQL += " 	CNCL_DS_CHANEL, ";
			cSQL += " 	STAT_DS_STATUS, ";
			cSQL += " 	CLIE_DS_NOMECLIENTE ";
			 
						
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, idEmbarque);

			rs = pst.executeQuery();

			retShip = setShipment(rs);

		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			log.info("***** Fim o Consulta na base Embarque x Delivery *****");
			daoLog.createLog(logsBean);

		}

		return retShip;
	}
	
	
	public RetornoShipmentBean slcEmbarqueShipmentAgrupado(String idEmbarque) {

		RetornoShipmentDeliveryBean retShipDeli = new RetornoShipmentDeliveryBean();
		RetornoShipmentBean retShip = new RetornoShipmentBean();

		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsNgtbEmbarqueEmbaDao");
		logsBean.setLogsDsMetodo("slcEmbarqueDelivery");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Consulta na base Embarque x Delivery *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "  SELECT ";
			cSQL += "  	ISNULL(SUM(SKUD_DS_PESOBRUTO),0) AS SKUD_DS_PESOBRUTO, ";
			cSQL += "  	ISNULL(SUM(SKUD_DS_PESOLIQUIDO),0) AS SKUD_DS_PESOLIQUIDO, ";
			cSQL += "  	ISNULL(SUM(SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE , ";
			cSQL += "  	ISNULL(SUM(SKUD_DS_VOLUMECUBICO),0) AS SKUD_DS_VOLUMECUBICO, ";
			cSQL += "  	ISNULL(SUM(SKUD_DS_VALOR),0) AS SKUD_DS_VALOR, ";
			cSQL += "  	STAT_DS_STATUS, ";
			cSQL += "  	CLIE_DS_NOMECLIENTE, ";
			cSQL += "  	EMBA_ID_CD_CODCLIENTE, ";
			cSQL += "  	EMBA_ID_CD_SHIPMENT, ";
			cSQL += "  	EMBA.EMBA_ID_CD_EMBARQUE, ";
			cSQL += "  	EMBA_DS_VEICULO, ";
			cSQL += "  	EMBA_DS_TIPOVEICULO, ";
			cSQL += "  	EMBA_IN_TRANSPORTADORACONFIRMADA, ";
			cSQL += "  	EMBA_DS_TIPOCARGA , ";
			cSQL += "  	EMBA_DS_COD_TRANSPORTADORA, ";
			cSQL += "  	EMBA_ID_CD_SHIPMENT, ";
			cSQL += "  	EMBA.EMBA_ID_CD_CODCLIENTE,  ";
			cSQL += "  	EMBA.EMBA_ID_CD_STATUS,  ";
			cSQL += "  	CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) AS EMBA_DH_LEADTIME ";
			cSQL += "  FROM  ";
			cSQL += "  	CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK) INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
			cSQL += "  		ON SKUD_ID_CD_SHIPMENT = EMBA_ID_CD_SHIPMENT ";
			cSQL += "  	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
			cSQL += "   		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS ";
			cSQL += "  	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
			cSQL += "   		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE ";
			cSQL += "  WHERE  ";
			cSQL += "  	EMBA.EMBA_ID_CD_EMBARQUE = ? ";
			cSQL += "  GROUP BY  ";
			cSQL += "  	STAT_DS_STATUS, ";
			cSQL += "  	CLIE_DS_NOMECLIENTE, ";
			cSQL += "  	EMBA_ID_CD_CODCLIENTE, ";
			cSQL += "  	EMBA_ID_CD_SHIPMENT, ";
			cSQL += "  	EMBA.EMBA_ID_CD_EMBARQUE, ";
			cSQL += "  	EMBA_DS_VEICULO, ";
			cSQL += "  	EMBA_DS_TIPOVEICULO, ";
			cSQL += "  	EMBA_IN_TRANSPORTADORACONFIRMADA, ";
			cSQL += "  	EMBA_DS_TIPOCARGA , ";
			cSQL += "  	EMBA_DS_COD_TRANSPORTADORA, ";
			cSQL += "  	EMBA_ID_CD_SHIPMENT, ";
			cSQL += "  	EMBA.EMBA_ID_CD_CODCLIENTE,  ";
			cSQL += "  	EMBA.EMBA_ID_CD_STATUS,  ";
			cSQL += "  	EMBA_DH_LEADTIME ";

						
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, idEmbarque);

			rs = pst.executeQuery();

			retShip = setShipmentAgrupado(rs);

		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
			e.printStackTrace();

		} finally {
			log.info("***** Fim  o Consulta na base Embarque x Delivery *****");
			daoLog.createLog(logsBean);

		}

		return retShip;
	}

	private RetornoShipmentBean setShipment(ResultSet rs) throws SQLException {

		RetornoShipmentBean retShip = new RetornoShipmentBean();

		if (rs.next()) {

			//retShip.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ") == null ? "" : rs.getString("SKUD_DS_PEDIDOMONDELEZ") );
			//retShip.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE") == null ? "" : rs.getString("SKUD_DS_PEDIDOCLIENTE") );
			//retShip.setSkud_id_cd_delivery(rs.getString("SKUD_ID_CD_DELIVERY") == null ? "" : rs.getString("SKUD_ID_CD_DELIVERY") );
			//retShip.setCocl_ds_grupocliente(rs.getString("COCL_DS_GRUPOCLIENTE") == null ? "" : rs.getString("COCL_DS_GRUPOCLIENTE") );
			//retShip.setNofi_nr_notafiscal(rs.getString("NOFI_NR_NOTAFISCAL") == null ? "" : rs.getString("NOFI_NR_NOTAFISCAL") );
			//retShip.setNofi_ds_serienota(rs.getString("NOFI_DS_SERIENOTA") == null ? "" : rs.getString("NOFI_DS_SERIENOTA") );
			
			retShip.setClie_ds_cnpj(rs.getString("CLIE_DS_CNPJ") == null ? "" : rs.getString("CLIE_DS_CNPJ") );
			retShip.setClie_ds_cidade(rs.getString("CLIE_DS_CIDADE") == null ? "" : rs.getString("CLIE_DS_CIDADE") );
			retShip.setClie_ds_uf(rs.getString("CLIE_DS_UF") == null ? "" : rs.getString("CLIE_DS_UF") );
			retShip.setEmba_ds_planta(rs.getString("EMBA_DS_PLANTA") == null ? "" : rs.getString("EMBA_DS_PLANTA") );
			retShip.setDaag_dh_agendamento(rs.getString("DAAG_DH_AGENDAMENTO") == null ? "" : rs.getString("DAAG_DH_AGENDAMENTO") );
			retShip.setDaag_ds_senhaagendamento(rs.getString("DAAG_DS_SENHAAGENDAMENTO") == null ? "" : rs.getString("DAAG_DS_SENHAAGENDAMENTO") );
			retShip.setClie_id_cd_codigocliente(rs.getString("CLIE_ID_CD_CODIGOCLIENTE") == null ? "" : rs.getString("CLIE_ID_CD_CODIGOCLIENTE") );
			retShip.setCncl_ds_chanel(rs.getString("CNCL_DS_CHANEL") == null ? "" : rs.getString("CNCL_DS_CHANEL") );
			
			
			
			retShip.setEmba_ds_tipocarga(rs.getString("EMBA_DS_TIPOCARGA") == null ? "" : rs.getString("EMBA_DS_TIPOCARGA") );
			retShip.setEmba_ds_tipoveiculo(rs.getString("EMBA_DS_TIPOVEICULO") == null ? "" : rs.getString("EMBA_DS_TIPOVEICULO") );
			retShip.setEmba_ds_veiculo(rs.getString("EMBA_DS_VEICULO") == null ? "" : rs.getString("EMBA_DS_VEICULO") );
			retShip.setEmba_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT") == null ? "" : rs.getString("EMBA_ID_CD_SHIPMENT") );
			retShip.setEmba_ds_cod_transportadora(rs.getString("EMBA_DS_COD_TRANSPORTADORA") == null ? "" : rs.getString("EMBA_DS_COD_TRANSPORTADORA") );
			retShip.setEmba_in_transportadoraconfirmada(rs.getString("EMBA_IN_TRANSPORTADORACONFIRMADA") == null ? "" : rs.getString("EMBA_IN_TRANSPORTADORACONFIRMADA") );
			retShip.setSkud_ds_pesobruto(rs.getString("PESO_BRUTO") == null ? "" : rs.getString("PESO_BRUTO") );
			retShip.setSkud_ds_pesoliquido(rs.getString("PESO_LIQUIDO") == null ? "" : rs.getString("PESO_LIQUIDO") );
			retShip.setSkud_ds_valor(rs.getString("VALOR_TOTAL") == null ? "" : rs.getString("VALOR_TOTAL") );
			retShip.setSkud_ds_volumecubico(rs.getString("VALOR_CUBICO") == null ? "" : rs.getString("VALOR_CUBICO") );
			retShip.setSkud_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT") == null ? "" : rs.getString("EMBA_ID_CD_SHIPMENT") );
			retShip.setEmba_dh_leadtime(rs.getString("EMBA_DH_LEADTIME") == null ? "" : rs.getString("EMBA_DH_LEADTIME") );
			retShip.setStat_ds_status(rs.getString("STAT_DS_STATUS") == null ? "" : rs.getString("STAT_DS_STATUS") );
			retShip.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE") == null ? "" : rs.getString("SKUD_NR_QUANTIDADE") );
			retShip.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE") == null ? "" : rs.getString("CLIE_DS_NOMECLIENTE") );
			retShip.setEmba_id_cd_codcliente(rs.getString("EMBA_ID_CD_CODCLIENTE") == null ? "" : rs.getString("EMBA_ID_CD_CODCLIENTE") );
			retShip.setEmba_id_cd_embarque((rs.getString("EMBA_ID_CD_EMBARQUE")) == null ? "" : rs.getString("EMBA_ID_CD_EMBARQUE") );
			retShip.setEmba_id_cd_status(rs.getString("EMBA_ID_CD_STATUS") == null ? "" : rs.getString("EMBA_ID_CD_STATUS") );
			
			
			

		}
		return retShip;
	}

	
	private RetornoShipmentBean setShipmentAgrupado(ResultSet rs) throws SQLException {

		RetornoShipmentBean retShip = new RetornoShipmentBean();

		if (rs.next()) {
			
			retShip.setSkud_ds_pesobruto(rs.getString("SKUD_DS_PESOBRUTO"));
			retShip.setSkud_ds_pesoliquido(rs.getString("SKUD_DS_PESOLIQUIDO"));
			retShip.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE"));
			retShip.setSkud_ds_volumecubico(rs.getString("SKUD_DS_VOLUMECUBICO"));
			retShip.setSkud_ds_valor(rs.getString("SKUD_DS_VALOR"));
			retShip.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
			retShip.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
			retShip.setEmba_id_cd_codcliente(rs.getString("EMBA_ID_CD_CODCLIENTE"));
			retShip.setSkud_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT"));
			retShip.setEmba_id_cd_embarque((rs.getString("EMBA_ID_CD_EMBARQUE")));
			retShip.setEmba_ds_veiculo(rs.getString("EMBA_DS_VEICULO"));
			retShip.setEmba_ds_tipoveiculo(rs.getString("EMBA_DS_TIPOVEICULO"));
			retShip.setEmba_in_transportadoraconfirmada(rs.getString("EMBA_IN_TRANSPORTADORACONFIRMADA"));
			retShip.setEmba_ds_tipocarga(rs.getString("EMBA_DS_TIPOCARGA"));
			retShip.setEmba_ds_cod_transportadora(rs.getString("EMBA_DS_COD_TRANSPORTADORA"));
			retShip.setEmba_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT"));
			retShip.setClie_id_cd_codigocliente(rs.getString("EMBA_ID_CD_CODCLIENTE"));
			retShip.setEmba_id_cd_status(rs.getString("EMBA_ID_CD_STATUS"));
			retShip.setEmba_dh_leadtime(rs.getString("EMBA_DH_LEADTIME"));
			
		}
		return retShip;
	}
	public ArrayList<RetornoDeliveryBean> slcDelivery(String emba_id_cd_shipment, String emba_id_cd_embarque, String tipo) {

		ArrayList<RetornoDeliveryBean> retDeliverylst = new ArrayList<RetornoDeliveryBean>();
		RetornoDeliveryBean retDelivery = new RetornoDeliveryBean();

		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsNgtbEmbarqueEmbaDao");
		logsBean.setLogsDsMetodo("slcEmbarqueDelivery");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Consulta na base  Delivery *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += " SELECT ";
			
			cSQL += " 		CLIE_ID_CD_CODIGOCLIENTE, ";
			cSQL += " 		CLIE_DS_NOMECLIENTE, ";
			cSQL += " 		EMBA_DS_PLANTA, ";
			cSQL += " 		SKUD_DS_PEDIDOMONDELEZ, ";
			cSQL += " 		TRAN_DS_CODIGOTRANSPORTADORA,";
			cSQL += " 		TRAN_NM_NOMETRANSPORTADORA,";
			cSQL += " 		SKUD_IN_CANCELADO,";
			
			cSQL += " 		SKUD_DS_PEDIDOMONDELEZ, ";
			cSQL += " 		SKUD_DS_PEDIDOCLIENTE, ";
			cSQL += " 		SKUD_ID_CD_DELIVERY, ";
			//cSQL += " 		NOFI_NR_NOTAFISCAL, ";
			//cSQL += " 		NOFI_DS_SERIENOTA, ";
			
			cSQL += " 		SKUD_ID_CD_DELIVERY, ";
			cSQL += " 		SKUD_ID_CD_SHIPMENT, ";
			//cSQL += " 		NOFI.NOFI_NR_NOTAFISCAL,";
			cSQL += " 		ISNULL(SUM(SKUD_DS_PESOBRUTO),0) SKUD_DS_PESOBRUTO, ";
			cSQL += " 		ISNULL(SUM(SKUD_DS_PESOLIQUIDO),0) SKUD_DS_PESOLIQUIDO, ";
			cSQL += " 		ISNULL(SUM(SKUD_DS_VALOR),0) SKUD_DS_VALOR, ";
			cSQL += " 		ISNULL(SUM(SKUD_NR_QUANTIDADE),0) SKUD_NR_QUANTIDADE, ";
			cSQL += " 		ISNULL(SUM(SKUD_DS_VOLUMECUBICO),0) SKUD_DS_VOLUMECUBICO ";
			cSQL += " 	FROM ";
			cSQL += " 		CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK) ";
			//cSQL += " 		LEFT JOIN CS_CDTB_NOTAFISCAL_NOFI NOFI (NOLOCK) ";
			//cSQL += " 			ON NOFI.NOFI_NR_DELIVERYDOCUMENT = SKUD.SKUD_ID_CD_DELIVERY ";
			
			cSQL += "		INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)  ";
			cSQL += "			ON EMBA_ID_CD_SHIPMENT = SKUD_ID_CD_SHIPMENT AND EMBA.EMBA_ID_CD_CODCLIENTE = SKUD.SKUD_ID_CD_CODCLIENTE";
			cSQL += "		INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
			cSQL += "			ON  CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE ";
			cSQL += "		INNER JOIN CS_CDTB_TRANSPORTADORA_TRAN TRANS (NOLOCK) ";
			cSQL += "			ON TRAN_DS_CODIGOTRANSPORTADORA = EMBA_DS_COD_TRANSPORTADORA ";
			//cSQL += "		LEFT JOIN CS_NGTB_DATAAGENDAMENTO_DAAG DAAG	";
			//cSQL += "			ON DAAG_ID_CD_EMBARQUE = EMBA_ID_CD_EMBARQUE "; 
			
			cSQL += " WHERE ";
			cSQL += " 	SKUD_ID_CD_SHIPMENT = ? ";
			cSQL += " 	AND EMBA_ID_CD_EMBARQUE = ? "; // ADICIONADO DANILLO
			if(!tipo.equalsIgnoreCase("tela")) {
				cSQL += " 	AND SKUD_IN_CANCELADO =  'N' ";
			}
			
			
			cSQL += " GROUP BY ";
			
			cSQL += " 	CLIE_ID_CD_CODIGOCLIENTE, ";
			cSQL += " 	CLIE_DS_NOMECLIENTE, ";
			cSQL += " 	EMBA_DS_PLANTA, ";
			cSQL += " 	SKUD_DS_PEDIDOMONDELEZ, ";
			cSQL += " 	TRAN_DS_CODIGOTRANSPORTADORA,";
			cSQL += " 	TRAN_NM_NOMETRANSPORTADORA,";
			
			cSQL += " 		SKUD_DS_PEDIDOMONDELEZ, ";
			cSQL += " 		SKUD_DS_PEDIDOCLIENTE, ";
			cSQL += " 		SKUD_ID_CD_DELIVERY, ";
			//cSQL += " 		NOFI_NR_NOTAFISCAL, ";
			//cSQL += " 		NOFI_DS_SERIENOTA, ";
			
			cSQL += " 	SKUD_ID_CD_DELIVERY, "; 
			cSQL += " 	SKUD_ID_CD_SHIPMENT, ";
			//cSQL += " 	NOFI.NOFI_NR_NOTAFISCAL, ";
			cSQL += " 	SKUD_IN_CANCELADO ";
			
			
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, emba_id_cd_shipment);
			pst.setString(2, emba_id_cd_embarque);

			rs = pst.executeQuery();
			
			retDeliverylst = setDelivery2(rs);		
		
			
		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
			e.printStackTrace();

		} finally {
			log.info("***** Fim o Consulta na base Delivery *****");
			daoLog.createLog(logsBean);
		}

		return retDeliverylst;
	}
	
	
	public ArrayList<RetornoDeliveryBean> slcDeliveryToXLS(String emba_id_cd_shipment) {

		ArrayList<RetornoDeliveryBean> retDeliverylst = new ArrayList<RetornoDeliveryBean>();
		RetornoDeliveryBean retDelivery = new RetornoDeliveryBean();

		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsNgtbEmbarqueEmbaDao");
		logsBean.setLogsDsMetodo("slcEmbarqueDelivery");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Consulta na base  Delivery *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
				cSQL += " SELECT ";
				
					cSQL += " 		CLIE_ID_CD_CODIGOCLIENTE, ";
					cSQL += " 		CLIE_DS_NOMECLIENTE, ";
					cSQL += " 		EMBA_DS_PLANTA, ";
					cSQL += " 		SKUD_DS_PEDIDOMONDELEZ, ";
					cSQL += " 		TRAN_DS_CODIGOTRANSPORTADORA,";
					cSQL += " 		TRAN_NM_NOMETRANSPORTADORA,";
					
					cSQL += " 		SKUD_ID_CD_DELIVERY, ";
					cSQL += " 		SKUD_ID_CD_SHIPMENT, ";
					cSQL += " 		NOFI.NOFI_NR_NOTAFISCAL,";
					cSQL += " 		ISNULL(SUM(SKUD_DS_PESOBRUTO),0) SKUD_DS_PESOBRUTO, ";
					cSQL += " 		ISNULL(SUM(SKUD_DS_PESOLIQUIDO),0) SKUD_DS_PESOLIQUIDO, ";
					cSQL += " 		ISNULL(SUM(SKUD_DS_VALOR),0) SKUD_DS_VALOR, ";
					cSQL += " 		ISNULL(SUM(SKUD_NR_QUANTIDADE),0) SKUD_NR_QUANTIDADE, ";
					cSQL += " 		ISNULL(SUM(SKUD_DS_VOLUMECUBICO),0) SKUD_DS_VOLUMECUBICO ";
					cSQL += " 	FROM ";
					cSQL += " 		CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK) ";
					cSQL += " 		LEFT JOIN CS_CDTB_NOTAFISCAL_NOFI NOFI (NOLOCK) ";
					cSQL += " 			ON NOFI.NOFI_NR_DELIVERYDOCUMENT = SKUD.SKUD_ID_CD_DELIVERY ";
					
					cSQL += "		INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) ";
					cSQL += "			ON EMBA_ID_CD_SHIPMENT = SKUD_ID_CD_SHIPMENT ";
					cSQL += "		INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
					cSQL += "			ON  CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE ";
					cSQL += "		INNER JOIN CS_CDTB_TRANSPORTADORA_TRAN TRANS (NOLOCK) ";
					cSQL += "			ON TRAN_DS_CODIGOTRANSPORTADORA = EMBA_DS_COD_TRANSPORTADORA ";
					cSQL += "		LEFT JOIN CS_NGTB_DATAAGENDAMENTO_DAAG DAAG (NOLOCK)	";
					cSQL += "			ON DAAG_ID_CD_EMBARQUE = EMBA_ID_CD_EMBARQUE "; 
					
					cSQL += " WHERE ";
					cSQL += " 	SKUD_ID_CD_SHIPMENT = ? ";
					cSQL += " GROUP BY ";
					
					cSQL += " 	CLIE_ID_CD_CODIGOCLIENTE, ";
					cSQL += " 	CLIE_DS_NOMECLIENTE, ";
					cSQL += " 	EMBA_DS_PLANTA, ";
					cSQL += " 	SKUD_DS_PEDIDOMONDELEZ, ";
					cSQL += " 	TRAN_DS_CODIGOTRANSPORTADORA,";
					cSQL += " 	TRAN_NM_NOMETRANSPORTADORA,";
					
					cSQL += " 	SKUD_ID_CD_DELIVERY, "; 
					cSQL += " 	SKUD_ID_CD_SHIPMENT, ";
					cSQL += " 	NOFI.NOFI_NR_NOTAFISCAL, ";
					cSQL += " 	SKUD_IN_CANCELADO, ";
					cSQL += " 	SKUD_DS_PEDIDOMONDELEZ, ";
					cSQL += " 	SKUD_DS_PEDIDOCLIENTE ";
			
			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, emba_id_cd_shipment);

			rs = pst.executeQuery();
			
			retDeliverylst = setDelivery(rs);		
		
			
		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			log.info("***** Fim o Consulta na base Delivery *****");
			daoLog.createLog(logsBean);
		}

		return retDeliverylst;
	}

	private ArrayList<RetornoDeliveryBean> setDelivery(ResultSet rs) throws SQLException {

		ArrayList<RetornoDeliveryBean> retDeliverylst = new ArrayList<RetornoDeliveryBean>();
		RetornoDeliveryBean retDelivery = new RetornoDeliveryBean();
		
 		while(rs.next()) {
			
			retDelivery = new RetornoDeliveryBean();			
			

			retDelivery.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
			retDelivery.setEmba_ds_planta(rs.getString("EMBA_DS_PLANTA"));
			
			retDelivery.setClie_id_cd_codigocliente(rs.getString("CLIE_ID_CD_CODIGOCLIENTE"));
			retDelivery.setTran_ds_codigotransportadora(rs.getString("TRAN_DS_CODIGOTRANSPORTADORA"));
			retDelivery.setTran_nm_nometransportadora(rs.getString("TRAN_NM_NOMETRANSPORTADORA"));
			
			retDelivery.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ"));
			retDelivery.setSkud_id_cd_delivery(rs.getString("SKUD_ID_CD_DELIVERY"));
			retDelivery.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE"));
			retDelivery.setNofi_ds_serienota(rs.getString("NOFI_DS_SERIENOTA"));
			retDelivery.setNofi_nr_notafiscal(rs.getString("NOFI_NR_NOTAFISCAL"));
			
			
			
			retDelivery.setSkud_ds_pesobruto(rs.getString("SKUD_DS_PESOBRUTO"));
			retDelivery.setSkud_ds_pesoliquido(rs.getString("SKUD_DS_PESOLIQUIDO"));
			retDelivery.setSkud_ds_valor(rs.getString("SKUD_DS_VALOR"));
			retDelivery.setSkud_ds_volumecubico(rs.getString("SKUD_DS_VOLUMECUBICO"));	
			retDelivery.setSkud_id_cd_shipment(rs.getString("SKUD_ID_CD_SHIPMENT"));
			retDelivery.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE"));
			retDelivery.setNofi_nr_notafiscal(rs.getString("NOFI_NR_NOTAFISCAL"));
			retDelivery.setSkud_in_cancelado(rs.getString("SKUD_IN_CANCELADO"));
			retDelivery.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ"));
			retDelivery.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE"));
			
			retDeliverylst.add(retDelivery);

		}
		return retDeliverylst;

	}
	
	private ArrayList<RetornoDeliveryBean> setDelivery2(ResultSet rs) throws SQLException {

		ArrayList<RetornoDeliveryBean> retDeliverylst = new ArrayList<RetornoDeliveryBean>();
		RetornoDeliveryBean retDelivery = new RetornoDeliveryBean();
		
 		while(rs.next()) {
			
			retDelivery = new RetornoDeliveryBean();			
			

			retDelivery.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
			retDelivery.setEmba_ds_planta(rs.getString("EMBA_DS_PLANTA"));
			
			retDelivery.setClie_id_cd_codigocliente(rs.getString("CLIE_ID_CD_CODIGOCLIENTE"));
			retDelivery.setTran_ds_codigotransportadora(rs.getString("TRAN_DS_CODIGOTRANSPORTADORA"));
			retDelivery.setTran_nm_nometransportadora(rs.getString("TRAN_NM_NOMETRANSPORTADORA"));
			
			retDelivery.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ"));
			retDelivery.setSkud_id_cd_delivery(rs.getString("SKUD_ID_CD_DELIVERY"));
			retDelivery.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE"));
//			retDelivery.setNofi_ds_serienota(rs.getString("NOFI_DS_SERIENOTA"));
//			retDelivery.setNofi_nr_notafiscal(rs.getString("NOFI_NR_NOTAFISCAL"));
			
			
			
			retDelivery.setSkud_ds_pesobruto(rs.getString("SKUD_DS_PESOBRUTO"));
			retDelivery.setSkud_ds_pesoliquido(rs.getString("SKUD_DS_PESOLIQUIDO"));
			retDelivery.setSkud_ds_valor(rs.getString("SKUD_DS_VALOR"));
			retDelivery.setSkud_ds_volumecubico(rs.getString("SKUD_DS_VOLUMECUBICO"));	
			retDelivery.setSkud_id_cd_shipment(rs.getString("SKUD_ID_CD_SHIPMENT"));
			retDelivery.setSkud_nr_quantidade(rs.getString("SKUD_NR_QUANTIDADE"));
//			retDelivery.setNofi_nr_notafiscal(rs.getString("NOFI_NR_NOTAFISCAL"));
			retDelivery.setSkud_in_cancelado(rs.getString("SKUD_IN_CANCELADO"));
			retDelivery.setSkud_ds_pedidomondelez(rs.getString("SKUD_DS_PEDIDOMONDELEZ"));
			retDelivery.setSkud_ds_pedidocliente(rs.getString("SKUD_DS_PEDIDOCLIENTE"));
			
			retDeliverylst.add(retDelivery);

		}
		return retDeliverylst;

	}
	
	/**
	 * Metodo responsavel por realizar o update do campo EMBA_DH_LEADTIME atraves da nova data passada via parametro
	 * @author Caio Fernandes
	 * @param String, String
	 * @return 
	 */
	public RetornoBean updateLeadTime(CsNgtbEmbarqueAgendamentoEmbaBean embarqueAgendamentoEmbaBean) throws ParseException {

		Connection conn = DAO.getConexao();
		UtilDate utilDate = new UtilDate();
		RetornoBean retBean = new RetornoBean();
		
		//String strDtLeadTime = utilDate.convertDateToYYYY_MM_dd(embarqueAgendamentoEmbaBean.getEmba_dh_leadtime());
		
		if(conn != null) {
			
			try {
				
				PreparedStatement pst = conn.prepareStatement(""
						+ " UPDATE "
						+ " 	CS_NGTB_EMBARQUEAGENDAMENTO_EMBA "
						+ " SET "
						+ "		EMBA_DH_LEADTIME = ?, "
						+ "		EMBA_DH_ATUALIZACAO = GETDATE(), "
						+ "		EMBA_ID_CD_USUARIOATUALIZACAO = ? "
						+ " WHERE "
						+ " 	EMBA_ID_CD_EMBARQUE = ? "
						+ "");

				pst.setString(1,embarqueAgendamentoEmbaBean.getUpdt_dh_leadtime());
				pst.setString(2, embarqueAgendamentoEmbaBean.getEmba_id_cd_usuarioatualizacao());
				pst.setString(3, embarqueAgendamentoEmbaBean.getEmba_id_cd_embarque());
						
				pst.execute();
				
				// CRIAR REGISTRO DE BKP DA EMBA
				CsNgtbEmbarqueEmbaDao embarqueEmbaDao = new CsNgtbEmbarqueEmbaDao();
				embarqueEmbaDao.createBkpEmbarque(Integer.parseInt(embarqueAgendamentoEmbaBean.getEmba_id_cd_embarque()));
				
				retBean.setSucesso(true);
				retBean.setProcesso("UPDATE");
				retBean.setMsg("LEADTIME ATUALIZADO COM SUCESSO! ");
				
			} catch (Exception e) {
				
				e.printStackTrace();
				retBean.setSucesso(false);
				retBean.setProcesso("UPDATE");
				retBean.setMsg(e.getMessage());
				System.out.println("ERRO AO ATUALIZAR LEADTIME ");
				
			}
			
		} else {
			retBean.setSucesso(false);
			retBean.setMsg("NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		return retBean;
		
	}

	public ArrayList<RetornoShipmentBean> slctByCriteria(String condicao, String id, String status) {
		
		
		ArrayList<RetornoShipmentBean> retShip = new ArrayList<RetornoShipmentBean>();
		Logger log = Logger.getLogger(this.getClass().getName());
		
		String cSQL ="";
		
		try {
			
			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

		if(condicao.equalsIgnoreCase("embarque")){
			
			cSQL = getSlctByEmbarque(id, status);
			
		}else if(condicao.equalsIgnoreCase("delivery")){
			
			cSQL = getSlctByDelivery(id, status);
			
		}else if(condicao.equalsIgnoreCase("shipment")){
			
			cSQL = getSlctByShipment(id, status);
		
		}else if(condicao.equalsIgnoreCase("dhImportacao")){
			
			cSQL = getSlctByDhShipment(id, status);
			
		}else if(condicao.equalsIgnoreCase("cliente")){
			
			cSQL = getSlctByCliente(id, status);
			
		}else if(condicao.equalsIgnoreCase("nf")){
			
			cSQL = getSlctByNF(id, status);
			
		}else if(condicao.equalsIgnoreCase("canal")){
			
			cSQL = getSlctByCanal(id, status);
			
		}else if(condicao.equalsIgnoreCase("pedido")){
			
			cSQL = getSlctByPedido(id, status);
			
		}
		
		PreparedStatement pst = con.prepareStatement(cSQL);
			
		rs = pst.executeQuery();
			
		retShip = setShipmentList(rs);	
		
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			log.info("***** Fim o Consulta na base Delivery *****");
		
		}
		
		return retShip;
	}
	
	private ArrayList<RetornoShipmentBean> setShipmentList(ResultSet rs) throws SQLException {
		
		ArrayList<RetornoShipmentBean> retShipList = new ArrayList <RetornoShipmentBean>();
		
		while (rs.next()) {			
			
			RetornoShipmentBean retShip = new RetornoShipmentBean();
			retShip.setEmba_ds_tipocarga(rs.getString("EMBA_DS_TIPOCARGA"));
			retShip.setEmba_ds_tipoveiculo(rs.getString("EMBA_DS_TIPOVEICULO"));
			retShip.setEmba_ds_veiculo(rs.getString("EMBA_DS_VEICULO"));
			retShip.setEmba_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT"));
			retShip.setEmba_ds_cod_transportadora(rs.getString("EMBA_DS_COD_TRANSPORTADORA"));			
			retShip.setEmba_in_transportadoraconfirmada(rs.getString("EMBA_IN_TRANSPORTADORACONFIRMADA"));
			retShip.setSkud_ds_pesobruto(rs.getString("PESO_BRUTO"));
			retShip.setSkud_ds_pesoliquido(rs.getString("PESO_LIQUIDO"));
			retShip.setSkud_ds_valor(rs.getString("VALOR_TOTAL"));
			retShip.setSkud_ds_volumecubico(rs.getString("VALOR_CUBICO"));			
			retShip.setSkud_id_cd_shipment(rs.getString("EMBA_ID_CD_SHIPMENT"));
			retShip.setEmba_dh_leadtime(rs.getString("EMBA_DH_LEADTIME"));
			retShip.setStat_ds_status(rs.getString("STAT_DS_STATUS"));
			retShip.setEmba_id_cd_embarque(rs.getString("EMBA_ID_CD_EMBARQUE"));
			retShip.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
			retShipList.add(retShip);

		}
		return retShipList;
	}

	private String getSlctByShipment(String id, String status) {
		
		String cSQL ="";

		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK)";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += " WHERE EMBA.EMBA_ID_CD_SHIPMENT = "+id;
		
		if(!status.equals("null")) {
			cSQL += " AND EMBA.EMBA_ID_CD_STATUS = "+status;
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";
		
		
		return cSQL;
	}

	private String getSlctByDelivery(String id, String status) {

		String cSQL ="";
		
		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK)";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK)";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += " WHERE SKUD.SKUD_ID_CD_DELIVERY = "+id;	
		
		if(!status.equals("null")) {
			cSQL += " AND EMBA.EMBA_ID_CD_STATUS = "+status;
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";
		
		return cSQL;
		
	}

	private String getSlctByEmbarque(String id, String status) {
		
		String cSQL ="";

		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += " WHERE EMBA.EMBA_ID_CD_EMBARQUE = "+id;
		
		if(!status.equals("null")) {
			cSQL += " AND EMBA.EMBA_ID_CD_STATUS = "+status;
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";
		
		
		return cSQL;
	}
	
	private String getSlctByDhShipment(String id, String status) {
		
		UtilDate utilDate = new UtilDate();
		
		String cSQL ="";

		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += " WHERE EMBA.EMBA_DH_SHIPMENT BETWEEN '"+utilDate.convertStringToSql(id, "C") + "' AND '"+utilDate.convertStringToSql(id, "F")+"'";
		
		if(!status.equals("null")) {
			cSQL += " AND EMBA.EMBA_ID_CD_STATUS = "+status;
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";
		
		
		return cSQL;
	}
	
	private String getSlctByCliente(String id, String status) {
		
		String cSQL ="";

		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += " WHERE CLIE.CLIE_DS_NOMECLIENTE LIKE '%"+id+"%'";
		
		if(!status.equals("null")) {
			cSQL += " AND EMBA.EMBA_ID_CD_STATUS = "+status;
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";
		
		
		return cSQL;
	}
	
	private String getSlctByNF(String id, String status) {
		
		String cSQL ="";

		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += " 	INNER JOIN CS_CDTB_NOTAFISCAL_NOFI NOFI (NOLOCK) ";
		cSQL += " 			ON NOFI.NOFI_NR_DELIVERYDOCUMENT = SKUD.SKUD_ID_CD_DELIVERY ";
		cSQL += " WHERE NOFI.NOFI_NR_NOTAFISCAL = "+id;
		
		if(!status.equals("null")) {
			cSQL += " AND EMBA.EMBA_ID_CD_STATUS = "+status;
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";
		
		
		return cSQL;
	}
	
	private String getSlctByCanal(String id, String status) {
		
		String cSQL ="";

		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += "	INNER JOIN CS_CDTB_CANALPORCLIENTE_CNCL CNCL (NOLOCK) ";
		cSQL += " 			ON EMBA.EMBA_ID_CD_CODCLIENTE = CNCL.CNCL_CD_CUSTUMER ";
		cSQL += " WHERE CNCL.CNCL_DS_CHANEL LIKE '%"+id+"%'";

		if(!status.equals("null")) {
			cSQL += " AND EMBA.EMBA_ID_CD_STATUS = "+status;
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";
		
		
		return cSQL;
	}
	
	private String getSlctByPedido(String id, String status) {
		
		String cSQL ="";

		cSQL += "	SELECT   ";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " EMBA.EMBA_ID_CD_EMBARQUE,";
		cSQL += " EMBA_DS_VEICULO,";
		cSQL += " EMBA_DS_TIPOVEICULO,";
		cSQL += " EMBA_IN_TRANSPORTADORACONFIRMADA,";
		cSQL += " EMBA_DS_TIPOCARGA ,";
		cSQL += " EMBA_DS_COD_TRANSPORTADORA,";
		cSQL += " EMBA_ID_CD_SHIPMENT,";
		cSQL += " CONVERT(VARCHAR(10),EMBA_DH_LEADTIME,103) EMBA_DH_LEADTIME,";
		cSQL += " STAT_DS_STATUS, ";
		cSQL += " EMBA.EMBA_ID_CD_CODCLIENTE, ";
		cSQL += " CLIE_DS_NOMECLIENTE, ";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOBRUTO),0) AS PESO_BRUTO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_PESOLIQUIDO),0) AS PESO_LIQUIDO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VOLUMECUBICO),0) AS VALOR_CUBICO,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_NR_QUANTIDADE),0) AS SKUD_NR_QUANTIDADE,";
		cSQL += " ISNULL(SUM(SKUD.SKUD_DS_VALOR),0) AS VALOR_TOTAL";
		cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
		cSQL += "	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
		cSQL += "		ON  EMBA.EMBA_ID_CD_SHIPMENT  = SKUD.SKUD_ID_CD_SHIPMENT";
		cSQL += " 	INNER JOIN CS_CDTB_STATUSAGENDAMENTO_STAT ST (NOLOCK) ";
		cSQL += " 		ON ST.STAT_ID_CD_STATUS = EMBA.EMBA_ID_CD_STATUS";
		cSQL += " 	INNER JOIN CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += " 		ON CLIE.CLIE_ID_CD_CODIGOCLIENTE = EMBA.EMBA_ID_CD_CODCLIENTE";
		cSQL += " WHERE SKUD.SKUD_DS_PEDIDOMONDELEZ = '"+id+"'";

		if(!status.equals("null")) {
			cSQL += " AND EMBA.EMBA_ID_CD_STATUS = "+status;
		}
		
		cSQL += " GROUP BY EMBA.EMBA_ID_CD_EMBARQUE,STAT_DS_STATUS,EMBA_ID_CD_SHIPMENT,EMBA_DS_COD_TRANSPORTADORA,EMBA_DS_TIPOVEICULO,EMBA_DS_VEICULO,EMBA_IN_TRANSPORTADORACONFIRMADA,EMBA_DS_TIPOCARGA,EMBA_DH_LEADTIME, EMBA.EMBA_ID_CD_CODCLIENTE, CLIE_DS_NOMECLIENTE";
		
		
		return cSQL;
	}
	
	public void setEmbarqueEmUsoDao(int emba_id_cd_embarque, String in_uso, int emba_id_cd_usuarioatualizacao) throws SQLException {
		
		DAO dao = new DAO();
		Connection con = dao.getConexao();
		
		String cSQL = " UPDATE CS_NGTB_EMBARQUEAGENDAMENTO_EMBA SET EMBA_IN_USO = ?, EMBA_ID_CD_USUARIOATUALIZACAO = ? WHERE EMBA_ID_CD_EMBARQUE = ?"; 
		
		PreparedStatement pst = con.prepareStatement(cSQL);
		pst.setString(1, in_uso);
		pst.setInt(2, emba_id_cd_usuarioatualizacao);
		pst.setInt(3, emba_id_cd_embarque);
		
		
		pst.execute();
		//con.close();
	}
	
	public RetornoBean updateStatusEmbarque(StatusEmbarqueBean statusEmbarqueBean) {
		
		RetornoBean retBean = new RetornoBean();
		DAO dao = new DAO();
		Connection con = dao.getConexao();
		
		try {

			createBkpEmbarque(statusEmbarqueBean.getEmba_id_cd_embarque());
			
			
			String cSQL = " UPDATE CS_NGTB_EMBARQUEAGENDAMENTO_EMBA SET EMBA_ID_CD_STATUS = ?, EMBA_ID_CD_USUARIOATUALIZACAO = ? WHERE EMBA_ID_CD_EMBARQUE = ?"; 
			
			PreparedStatement pst = con.prepareStatement(cSQL);
			pst.setInt(1, statusEmbarqueBean.getStat_id_cd_status());
			pst.setInt(2, statusEmbarqueBean.getEmba_id_cd_usuarioatualizacao());
			pst.setInt(3, statusEmbarqueBean.getEmba_id_cd_embarque());
			
			pst.execute();
			
			retBean.setMsg("Status Atualizado com sucesso!");
			retBean.setSucesso(true);
			retBean.setProcesso("Atualizar Status");

		} catch (Exception e) {
			e.printStackTrace();
			retBean.setMsg(e.getMessage());
			retBean.setSucesso(false);
		}

		
		return retBean;
		
	}
	
	
	//DANILLO *** INCLUSÃO DE PERIODO BASEADO NA DATA DE EMISSÃO DA NOTA FISCAL
	//29/07/2021
	public void updatePeriodoEmbarque(String skudIdCdDelivery, String dataString, String possuiNF) throws SQLException, ParseException {

		CsNgtbEmbarqueEmbaDao embaDao = new CsNgtbEmbarqueEmbaDao();
		Connection conn = DAO.getConexao();
		PreparedStatement pst = conn.prepareStatement(""
				+ " UPDATE "
				+ " 	CS_NGTB_EMBARQUEAGENDAMENTO_EMBA "
				+ " SET "
				+ "		EMBA_DS_PERIODO = ?, "
				+ "		EMBA_IN_POSSUINF = ?  "
				+ " WHERE  "
				+ " 	EMBA_ID_CD_SHIPMENT in " 
				+ " 		( SELECT DISTINCT EMBA_ID_CD_SHIPMENT FROM " 
				+ " 			CS_CDTB_NOTAFISCAL_NOFI NOFI (nolock) INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (nolock) "
				+ " 				ON NOFI.NOFI_NR_DELIVERYDOCUMENT = SKUD.SKUD_ID_CD_DELIVERY " 
				+ " 			INNER JOIN CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (nolock) "
				+ " 				ON EMBA_ID_CD_SHIPMENT = SKUD_ID_CD_SHIPMENT "
				+ " 			WHERE " 
				+ " 				SKUD.SKUD_ID_CD_DELIVERY = ? ) "
				+ "");

		pst.setString(1, embaDao.getPeriodo(dataString));
		pst.setString(2, possuiNF);
		pst.setString(3, skudIdCdDelivery);
		pst.execute();


	}
	public void updatePeriodo(NotaFiscalBean notaFiscalBean) throws SQLException {
		
		
		CsCdtbSkudDeliveryDao skuDao = new CsCdtbSkudDeliveryDao();
		DAO dao = new DAO();
		Connection con = dao.getConexao();
		
		String idShipment = null;
		//VERIFICA SE EXISTE O SKU NA BASE CRIETERIO DELIVERY DOCUMENT
		idShipment = skuDao.slctSkudByDelivery(notaFiscalBean.getNrDeliveryDocument(),idShipment);
		
     	//SE EXISTIR DAR UM UPDATE NO PERIODO DO EMBARQUE
		if(idShipment != null){			
		
			if(notaFiscalBean.getDhEmissaoNota() != null && !notaFiscalBean.getDhEmissaoNota().isEmpty()){
				
				String periodo = "P"+notaFiscalBean.getDhEmissaoNota().substring(6,7);			
				
				String cSQL = " UPDATE CS_NGTB_EMBARQUEAGENDAMENTO_EMBA SET EMBA_DS_PERIODO = ? WHERE EMBA_ID_CD_SHIPMENT = ?"; 
				
				PreparedStatement pst = con.prepareStatement(cSQL);
				
				pst.setString(1, periodo);
				pst.setString(2, idShipment);
				
				pst.execute();
				
			}
		}
		
	}
}
