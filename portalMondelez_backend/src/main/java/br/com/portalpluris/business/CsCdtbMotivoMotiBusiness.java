package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbMotivoMotiBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsCdtbMotivoMotiDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbMotivoMotiBusiness {

	CsCdtbMotivoMotiDao motiDao = new CsCdtbMotivoMotiDao();
	private DAOLog daoLog = new DAOLog();

	public String createUpdateMotivo(CsCdtbMotivoMotiBean motivoBean)
			throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogMoti(motivoBean, "createUpdateMotivo");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = motiDao.createUpdateMotivo(motivoBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getMotivo(int moti_id_cd_motivo) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogMoti(null, "getMotivo");
		logsBean.setLogsTxInfoentrada(moti_id_cd_motivo + "");

		CsCdtbMotivoMotiBean motivoBean = new CsCdtbMotivoMotiBean();

		String jsonRetorno = "";

		try {

			motivoBean = motiDao.getMoti(moti_id_cd_motivo);
		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(moti_id_cd_motivo);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListMotivos(String tipo, String inativo) throws JsonProcessingException {

		List<CsCdtbMotivoMotiBean> list = new ArrayList<CsCdtbMotivoMotiBean>();

		String jsonRetorno = "";

		try {

			list = motiDao.getListMotivo(tipo, inativo);

		} catch (Exception e) {
			e.printStackTrace();

		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(list);

		return jsonRetorno;
	}

	private EsLgtbLogsLogsBean getLogMoti(CsCdtbMotivoMotiBean motivoBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbMotivoMotiBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");
		logsBean.setLogsTxInfoentrada(getAllValues(motivoBean));

		if (motivoBean != null) {
			logsBean.setLogsDsChave(motivoBean.getMoti_id_cd_motivo() + "");
		}

		return logsBean;
	}

	private String getAllValues(CsCdtbMotivoMotiBean motivoBean) {

		String all = "";

		if (motivoBean != null) {
			all = "" + motivoBean.getMoti_id_cd_motivo() + ";" + motivoBean.getMoti_ds_motivo() + ";"
					+ motivoBean.getMoti_in_inativo() + ";" + motivoBean.getMoti_dh_registro() + ";" + "";
		}

		return all;
	}

}
