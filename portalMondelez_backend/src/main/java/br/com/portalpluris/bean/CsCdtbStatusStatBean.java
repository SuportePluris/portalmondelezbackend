package br.com.portalpluris.bean;

import java.sql.Timestamp;

public class CsCdtbStatusStatBean {

	int stat_id_cd_status;
	String status_ds_descricaostatus;
	Timestamp status_dh_registro;
	String status_in_inativo;
	int status_id_cd_usuarioatualizacao;
	Timestamp status_dh_atualizacao;
	public int getStat_id_cd_status() {
		return stat_id_cd_status;
	}
	public void setStat_id_cd_status(int stat_id_cd_status) {
		this.stat_id_cd_status = stat_id_cd_status;
	}
	public String getStatus_ds_descricaostatus() {
		return status_ds_descricaostatus;
	}
	public void setStatus_ds_descricaostatus(String status_ds_descricaostatus) {
		this.status_ds_descricaostatus = status_ds_descricaostatus;
	}
	public Timestamp getStatus_dh_registro() {
		return status_dh_registro;
	}
	public void setStatus_dh_registro(Timestamp status_dh_registro) {
		this.status_dh_registro = status_dh_registro;
	}
	public String getStatus_in_inativo() {
		return status_in_inativo;
	}
	public void setStatus_in_inativo(String status_in_inativo) {
		this.status_in_inativo = status_in_inativo;
	}
	public int getStatus_id_cd_usuarioatualizacao() {
		return status_id_cd_usuarioatualizacao;
	}
	public void setStatus_id_cd_usuarioatualizacao(int status_id_cd_usuarioatualizacao) {
		this.status_id_cd_usuarioatualizacao = status_id_cd_usuarioatualizacao;
	}
	public Timestamp getStatus_dh_atualizacao() {
		return status_dh_atualizacao;
	}
	public void setStatus_dh_atualizacao(Timestamp status_dh_atualizacao) {
		this.status_dh_atualizacao = status_dh_atualizacao;
	}

	

}
