package br.com.portalpluris.business;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbSkudeliverySkudBean;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsLgbtskudeliverySkudDao;

public class CsLgtbSkudeliverySkudBusiness {

	/**
	 * Metodo responsavel por realizar leitura e gravacao das informacoes disponibilizadas via planilha no  banco de dados
	 * @author Caio Fernandes
	 * */
	public void gravarSkuDelivery() {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("[gravarSkuDelivery] - INICIO ");

		try {

			List<CsLgtbSkudeliverySkudBean> lstSkuDelivery = new ArrayList<CsLgtbSkudeliverySkudBean>();
			CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
			UtilMoveArquivo utilMove = new UtilMoveArquivo();

//			String diretorioSkuDelivery = confDao.slctConf("conf.importacao.agendamento.skudelivery");
//			String diretorioSkuDeliveryBkp = confDao.slctConf("conf.importacao.agendamento.skudelivery.bkp");
//			String diretorioSkuDeliveryErro = confDao.slctConf("conf.importacao.agendamento.skudelivery.erro");
			
			
			String diretorioSkuDelivery = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\DNS";
			String diretorioSkuDeliveryBkp = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\DNS\\bkp";
			String diretorioSkuDeliveryErro = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\DNS\\erro";
			// LEITURA DOS ARQUIVOS DO DIRETORIO
			File arquivos = new File(diretorioSkuDelivery);
			String[] arq = arquivos.list();
			
			if(arq != null) {
				
				for (int i = 0; i < arq.length; i++) {

					// ARQUIVO A SER PROCESSADO
					//File arquivoProcessado = new File(diretorioSkuDelivery + "/" +  arq[i]); // UTILIZAR QUANDO FOR LINUX
					File arquivoProcessado = new File(diretorioSkuDelivery + "\\" +  arq[i]); // UTILIZAR QUANDO FOR WINDOWS
					
					try {
						
						if(arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

							String nmArquivo = arquivoProcessado.getName();

							// INSTANCIAR ARQUIVO
							FileInputStream file = new FileInputStream(arquivoProcessado);

							// INSTANCIAR PLANILHA
							Workbook wb = WorkbookFactory.create(file);								
							
							// FOR DAS ABAS DO ARQUIVO
							for(int j = 0; j < wb.getNumberOfSheets(); j++) {

								// CONFIRMAR NOME DA ABA
								if(wb.getSheetName(j).equalsIgnoreCase("Sheet1")) {
									
									// SETAR ABA DA PLANILHA
									Sheet planilha = wb.getSheetAt(j);

									// OBTER VALORES DA PLANILHA E PREENCHER A LISTA 
									lstSkuDelivery = setListaSkuDelivery(lstSkuDelivery, planilha);
									
									// REALIZAR O INSERT DA LISTA NO BANCO
									CsLgbtskudeliverySkudDao skudeliveryPediDao = new CsLgbtskudeliverySkudDao();
									skudeliveryPediDao.insertSkuDelivery(lstSkuDelivery, nmArquivo);
								}

							}
							
							// REALIZAR MOVE DO ARQUIVO
							File arquivoProcessadoBkp = new File(diretorioSkuDeliveryBkp + "/" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoBkp);

						}
						
					} catch (Exception e) {
						log.info("[gravarSkuDelivery] - ERRO AO LER AQUIVO SKU DELIVERY: " + e.getMessage());
						
						// MOVER ARQUIVO PARA PASTA DE ERRO
						if(arq != null) {
							File arquivoProcessadoErro = new File(diretorioSkuDeliveryErro + "/" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoErro);
						}
						
					}

					
					
				}
				
			} else {
				log.info("[gravarSkuDelivery] - NAO FOI POSSIVEL OBTER OS ARQUIVOS DO DIRETORIO ");
			}

		} catch (Exception e){
			log.info("[gravarSkuDelivery] - ERRO AO GRAVAR SKU DELIVERY ");
		}

	}

	/**
	 * Metodo responsavel por obter os valores da planilha e retornar a lista de CsLgtbSkudeliverySkudBean preenchida.
	 * @author Caio Fernandes
	 * @param List<CsLgtbSkudeliverySkudBean>, Sheet
	 * @return List<CsLgtbSkudeliverySkudBean>
	 * @throws ParseException 
	 * */
	private List<CsLgtbSkudeliverySkudBean> setListaSkuDelivery(List<CsLgtbSkudeliverySkudBean> lstSkuDeliveryBean, Sheet planilha) throws ParseException {

		UtilDate util = new UtilDate();
		
		// FOR DAS LINHAS DO ARQUIVO
		for (int i = 1; i < planilha.getLastRowNum()+1 ; i++){
			
			// POPULAR BEAN
			CsLgtbSkudeliverySkudBean skuDeliveryBean = new CsLgtbSkudeliverySkudBean();
			
			if(planilha.getRow(i).getCell(0) != null){
				// SKUD_CD_DELIVERY
				skuDeliveryBean.setSkud_cd_delivery(planilha.getRow(i).getCell(0).toString());
			}
			
			if(planilha.getRow(i).getCell(1) != null){
				// SKUD_CD_REFERENCEDOCUMENT
				skuDeliveryBean.setSkud_cd_referencedocument(planilha.getRow(i).getCell(1).toString());
			}
			
			if(planilha.getRow(i).getCell(2) != null){
				// SKUD_DS_ITEM
				skuDeliveryBean.setSkud_ds_item(planilha.getRow(i).getCell(2).toString());
			}
			
			if(planilha.getRow(i).getCell(3) != null){
				// SKUD_DS_SHIPTOPARTY
				skuDeliveryBean.setSkud_ds_shiptoparty(planilha.getRow(i).getCell(3).toString());
			}
			
			if(planilha.getRow(i).getCell(4) != null){
				// SKUD_DS_NAMESHIPTOPARTY
				skuDeliveryBean.setSkud_ds_nameshiptoparty(planilha.getRow(i).getCell(4).toString());
			}
			
			if(planilha.getRow(i).getCell(5) != null){
				// SKUD_DS_MATERIAL
				skuDeliveryBean.setSkud_ds_material(planilha.getRow(i).getCell(5).toString());
			}
			
			if(planilha.getRow(i).getCell(6) != null){
				// SKUD_DS_DESCRIPTION
				skuDeliveryBean.setSkud_ds_description(planilha.getRow(i).getCell(6).toString());
			}
			
			if(planilha.getRow(i).getCell(7) != null){
				// SKUD_DS_DELIVERYQUANTITY 
				skuDeliveryBean.setSkud_ds_deliveryquantity(planilha.getRow(i).getCell(7).toString());
			}
			
			if(planilha.getRow(i).getCell(8) != null){
				// SKUD_DS_SALESUNIT
				skuDeliveryBean.setSkud_ds_salesunit(planilha.getRow(i).getCell(8).toString());
			}
			
			if(planilha.getRow(i).getCell(9) != null){
				// SKUD_DH_MATERIALAVAILDATE
				skuDeliveryBean.setSkud_dh_materialavaildate(util.convertStringToDate(planilha.getRow(i).getCell(9).toString()));
			}
			
			if(planilha.getRow(i).getCell(10) != null){
				// SKUD_DS_NETWEIGHT 
				skuDeliveryBean.setSkud_ds_netweight(planilha.getRow(i).getCell(10).toString()); 
			}
			
			if(planilha.getRow(i).getCell(11) != null){
				// SKUD_DS_TOTALWEIGHT 
				skuDeliveryBean.setSkud_ds_totalweight(planilha.getRow(i).getCell(11).toString());
			}
			
			if(planilha.getRow(i).getCell(12) != null){
				// SKUD_DS_WEIGHTUNIT
				skuDeliveryBean.setSkud_ds_weightunit(planilha.getRow(i).getCell(12).toString());
			}
			
			if(planilha.getRow(i).getCell(13) != null){
				// SKUD_DS_VOLUME 
				skuDeliveryBean.setSkud_ds_volume(planilha.getRow(i).getCell(13).toString());
			}
			
			if(planilha.getRow(i).getCell(14) != null){
				// SKUD_DS_VOLUMEUNIT 
				skuDeliveryBean.setSkud_ds_volumeunit(planilha.getRow(i).getCell(14).toString());
			}
			
			if(planilha.getRow(i).getCell(15) != null){
				// SKUD_DS_STORAGELOCATION
				skuDeliveryBean.setSkud_ds_storagelocation(planilha.getRow(i).getCell(15).toString());
			}
			
			if(planilha.getRow(i).getCell(16) != null){
				// SKUD_DS_BATCH
				skuDeliveryBean.setSkud_ds_batch(planilha.getRow(i).getCell(16).toString());
			}
			
			if(planilha.getRow(i).getCell(17) != null){
				// SKUD_DS_PLANT
				skuDeliveryBean.setSkud_ds_plant(planilha.getRow(i).getCell(17).toString()); 
			}
			
			if(planilha.getRow(i).getCell(18) != null){
				// SKUD_DH_ACTGDSMVMNTDATE
				skuDeliveryBean.setSkud_dh_actgdsmvmntdate(util.convertStringToDate(planilha.getRow(i).getCell(18).toString()));
			}
			
			if(planilha.getRow(i).getCell(19) != null){
				// SKUD_DH_DELIVERYDATE 
				skuDeliveryBean.setSkud_dh_deliverydate(util.convertStringToDate(planilha.getRow(i).getCell(19).toString()));
			}
			
			if(planilha.getRow(i).getCell(20) != null){
				// SKUD_DS_ROUTE 
				skuDeliveryBean.setSkud_ds_route(planilha.getRow(i).getCell(20).toString());
			}
			
			if(planilha.getRow(i).getCell(21) != null){
				// SKUD_DS_ACTUALDELIVERYQTY 
				skuDeliveryBean.setSkud_ds_actualdeliveryqty(planilha.getRow(i).getCell(21).toString());
			}
			
			if(planilha.getRow(i).getCell(22) != null){
				// SKUD_DS_VENDORLOCATION
				skuDeliveryBean.setSkud_ds_vendorlocation(planilha.getRow(i).getCell(22).toString());
			}
			
			if(planilha.getRow(i).getCell(23) != null){
				// SKUD_DS_LOCATIONSHIPTOPARTY
				skuDeliveryBean.setSkud_ds_locationshiptoparty(planilha.getRow(i).getCell(23).toString());
			}
			
			if(planilha.getRow(i).getCell(24) != null){
				// SKUD_DH_LOADINGDATE
				skuDeliveryBean.setSkud_dh_loadingdate(util.convertStringToDate(planilha.getRow(i).getCell(24).toString()));
			}
			
			if(planilha.getRow(i).getCell(25) != null){
				// SKUD_DS_DELIVERYTYPE
				skuDeliveryBean.setSkud_ds_deliverytype(planilha.getRow(i).getCell(25).toString());
			}
			
			if(planilha.getRow(i).getCell(26) != null){
				// SKUD_DS_SHIPPINPOINT
				skuDeliveryBean.setSkud_ds_shippinpoint(planilha.getRow(i).getCell(26).toString());
			}
			
			if(planilha.getRow(i).getCell(27) != null){
				// SKUD_DS_DELIVERYPRIORITY
				skuDeliveryBean.setSkud_ds_deliverypriority(planilha.getRow(i).getCell(27).toString());
			}
			
			if(planilha.getRow(i).getCell(28) != null){
				// SKUD_DH_GOODSISSUEDATE
				skuDeliveryBean.setSkud_dh_goodsissuedate(util.convertStringToDate(planilha.getRow(i).getCell(28).toString()));
			}
			
			if(planilha.getRow(i).getCell(29) != null){
				// SKUD_DH_TRANSPTNPLANGDATE
				skuDeliveryBean.setSkud_dh_transptnplangdate(util.convertStringToDate(planilha.getRow(i).getCell(29).toString()));
			}
			
			if(planilha.getRow(i).getCell(30) != null){
				// SKUD_DH_PICKINGDATE
				skuDeliveryBean.setSkud_dh_pickingdate(util.convertStringToDate(planilha.getRow(i).getCell(30).toString()));
			}
			
			if(planilha.getRow(i).getCell(31) != null){
				// SKUD_DS_LOCATIONSOLDTOPARTY
				skuDeliveryBean.setSkud_ds_locationsoldtoparty(planilha.getRow(i).getCell(31).toString());
			}
			
			if(planilha.getRow(i).getCell(32) != null){
				// SKUD_DS_VENDOR
				skuDeliveryBean.setSkud_ds_vendor(planilha.getRow(i).getCell(32).toString()); 
			}
			
			if(planilha.getRow(i).getCell(33) != null){
				// SKUD_DS_DISTRIBUTIONCHANNEL
				skuDeliveryBean.setSkud_ds_distributionchannel(planilha.getRow(i).getCell(33).toString());
			}
			
			if(planilha.getRow(i).getCell(34) != null){
				// SKUD_DS_SALESORGANIZATION
				skuDeliveryBean.setSkud_ds_salesorganization(planilha.getRow(i).getCell(34).toString());
			}
			
			if(planilha.getRow(i).getCell(35) != null){
				// SKUD_DS_WAREHOUSENUMBER
				skuDeliveryBean.setSkud_ds_warehousenumber(planilha.getRow(i).getCell(35).toString()); 
			}
			
			if(planilha.getRow(i).getCell(36) != null){
				// SKUD_DS_SHIPMENT
				skuDeliveryBean.setSkud_cd_shipment(planilha.getRow(i).getCell(36).toString()); 
			}

			// ADICIONAR BEAN NA LISTA
			lstSkuDeliveryBean.add(skuDeliveryBean);

		}

		return lstSkuDeliveryBean;
	}

}

