package br.com.portalpluris.bean;

import java.util.Date;

public class CsLgtbResumoDnsRednBean {
	
	String redn_ds_ordereleaseid;
	String redn_ds_dnn;
	Date redn_dh_deliverydate;
	String redn_ds_shipmenttop;
	String redn_ds_shipmentsplit;
	String redn_ds_sourcelocationid;
	String redn_ds_sourcelocationame;
	String redn_ds_destinationlocationid;
	String redn_ds_destinationlocationname;
	String redn_ds_destinationcity;
	String redn_ds_destinationprovincecode;
	String redn_ds_grossweight;
	String redn_ds_grossvolume;
	String redn_ds_cases;
	String redn_ds_projetos;
	String redn_ds_modelplann;
	String redn_ds_equipamentogroup;
	Date redn_dh_registro;
	String redn_ds_statusprocessamento;
	Date redn_dh_processamento;
	String redn_ds_nomearquivo;
	
	
	public String getRedn_ds_ordereleaseid() {
		return redn_ds_ordereleaseid;
	}
	public void setRedn_ds_ordereleaseid(String redn_ds_ordereleaseid) {
		this.redn_ds_ordereleaseid = redn_ds_ordereleaseid;
	}
	public String getRedn_ds_dnn() {
		return redn_ds_dnn;
	}
	public void setRedn_ds_dnn(String redn_ds_dnn) {
		this.redn_ds_dnn = redn_ds_dnn;
	}
	public Date getRedn_dh_deliverydate() {
		return redn_dh_deliverydate;
	}
	public void setRedn_dh_deliverydate(Date redn_dh_deliverydate) {
		this.redn_dh_deliverydate = redn_dh_deliverydate;
	}
	public String getRedn_ds_shipmenttop() {
		return redn_ds_shipmenttop;
	}
	public void setRedn_ds_shipmenttop(String redn_ds_shipmenttop) {
		this.redn_ds_shipmenttop = redn_ds_shipmenttop;
	}
	public String getRedn_ds_shipmentsplit() {
		return redn_ds_shipmentsplit;
	}
	public void setRedn_ds_shipmentsplit(String redn_ds_shipmentsplit) {
		this.redn_ds_shipmentsplit = redn_ds_shipmentsplit;
	}
	public String getRedn_ds_sourcelocationid() {
		return redn_ds_sourcelocationid;
	}
	public void setRedn_ds_sourcelocationid(String redn_ds_sourcelocationid) {
		this.redn_ds_sourcelocationid = redn_ds_sourcelocationid;
	}
	public String getRedn_ds_sourcelocationame() {
		return redn_ds_sourcelocationame;
	}
	public void setRedn_ds_sourcelocationame(String redn_ds_sourcelocationame) {
		this.redn_ds_sourcelocationame = redn_ds_sourcelocationame;
	}
	public String getRedn_ds_destinationlocationid() {
		return redn_ds_destinationlocationid;
	}
	public void setRedn_ds_destinationlocationid(
			String redn_ds_destinationlocationid) {
		this.redn_ds_destinationlocationid = redn_ds_destinationlocationid;
	}
	public String getRedn_ds_destinationlocationname() {
		return redn_ds_destinationlocationname;
	}
	public void setRedn_ds_destinationlocationname(
			String redn_ds_destinationlocationname) {
		this.redn_ds_destinationlocationname = redn_ds_destinationlocationname;
	}
	public String getRedn_ds_destinationcity() {
		return redn_ds_destinationcity;
	}
	public void setRedn_ds_destinationcity(String redn_ds_destinationcity) {
		this.redn_ds_destinationcity = redn_ds_destinationcity;
	}
	public String getRedn_ds_destinationprovincecode() {
		return redn_ds_destinationprovincecode;
	}
	public void setRedn_ds_destinationprovincecode(
			String redn_ds_destinationprovincecode) {
		this.redn_ds_destinationprovincecode = redn_ds_destinationprovincecode;
	}
	public String getRedn_ds_grossweight() {
		return redn_ds_grossweight;
	}
	public void setRedn_ds_grossweight(String redn_ds_grossweight) {
		this.redn_ds_grossweight = redn_ds_grossweight;
	}
	public String getRedn_ds_grossvolume() {
		return redn_ds_grossvolume;
	}
	public void setRedn_ds_grossvolume(String redn_ds_grossvolume) {
		this.redn_ds_grossvolume = redn_ds_grossvolume;
	}
	public String getRedn_ds_cases() {
		return redn_ds_cases;
	}
	public void setRedn_ds_cases(String redn_ds_cases) {
		this.redn_ds_cases = redn_ds_cases;
	}
	public String getRedn_ds_projetos() {
		return redn_ds_projetos;
	}
	public void setRedn_ds_projetos(String redn_ds_projetos) {
		this.redn_ds_projetos = redn_ds_projetos;
	}
	public String getRedn_ds_modelplann() {
		return redn_ds_modelplann;
	}
	public void setRedn_ds_modelplann(String redn_ds_modelplann) {
		this.redn_ds_modelplann = redn_ds_modelplann;
	}
	public String getRedn_ds_equipamentogroup() {
		return redn_ds_equipamentogroup;
	}
	public void setRedn_ds_equipamentogroup(String redn_ds_equipamentogroup) {
		this.redn_ds_equipamentogroup = redn_ds_equipamentogroup;
	}
	public Date getRedn_dh_registro() {
		return redn_dh_registro;
	}
	public void setRedn_dh_registro(Date redn_dh_registro) {
		this.redn_dh_registro = redn_dh_registro;
	}
	public String getRedn_ds_statusprocessamento() {
		return redn_ds_statusprocessamento;
	}
	public void setRedn_ds_statusprocessamento(String redn_ds_statusprocessamento) {
		this.redn_ds_statusprocessamento = redn_ds_statusprocessamento;
	}
	public Date getRedn_dh_processamento() {
		return redn_dh_processamento;
	}
	public void setRedn_dh_processamento(Date redn_dh_processamento) {
		this.redn_dh_processamento = redn_dh_processamento;
	}
	public String getRedn_ds_nomearquivo() {
		return redn_ds_nomearquivo;
	}
	public void setRedn_ds_nomearquivo(String redn_ds_nomearquivo) {
		this.redn_ds_nomearquivo = redn_ds_nomearquivo;
	}
}
