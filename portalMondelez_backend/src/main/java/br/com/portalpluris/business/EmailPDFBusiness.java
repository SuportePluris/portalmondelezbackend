package br.com.portalpluris.business;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.EmailsUtil;
import br.com.catapi.util.UtilLayoutEmail;
import br.com.portalpluris.bean.DetalhesEmailCompletoBean;
import br.com.portalpluris.bean.PDFBean;
import br.com.portalpluris.bean.RetornoBean;

public class EmailPDFBusiness {

	public String getBytes(String id_embarque, String tipo) throws Exception {
		
		String retorno = "";
		
		PDFBean pdfBean = new PDFBean();
		UtilLayoutEmail UtilLayoutEmail = new UtilLayoutEmail();
		
		pdfBean.setBytes(UtilLayoutEmail.getBytePDF(id_embarque, tipo));
		
		ObjectMapper mapper = new ObjectMapper();
		retorno = mapper.writeValueAsString(pdfBean);

		return retorno;
	}

	public String enviaEmail(String destinatario, String id_embarque, String userIdCdUsuario) throws Exception {

		String retorno = "";
		String htmlBody = "";
		
		RetornoBean retBean = new RetornoBean();
		
		UtilLayoutEmail UtilLayoutEmail = new UtilLayoutEmail();
		
		// **** PDF ****
		//byte[] attach = UtilLayoutEmail.getBytePDF(id_embarque, destinatario);
		
	// **** GERAR O XLS ****
		byte[] outputByte = null;
		
		if(destinatario.equalsIgnoreCase("emailCliente")){
			outputByte = UtilLayoutEmail.getByteXLS(id_embarque, destinatario);
		}
		
		DetalhesEmailCompletoBean detalhesEmail = UtilLayoutEmail.getHtmlBody(id_embarque, destinatario);
		
		
		if(userIdCdUsuario.equals("null")) {
			userIdCdUsuario = "1";
		}
		
		//retBean = EmailsUtil.sendMail(Integer.parseInt(id_embarque), Integer.parseInt(userIdCdUsuario), destinatario, attach);
		retBean = EmailsUtil.sendMail(Integer.parseInt(id_embarque), Integer.parseInt(userIdCdUsuario), destinatario, outputByte, detalhesEmail);
		
		ObjectMapper mapper = new ObjectMapper();
		retorno = mapper.writeValueAsString(retBean);
		
		return retorno;
	}

}
