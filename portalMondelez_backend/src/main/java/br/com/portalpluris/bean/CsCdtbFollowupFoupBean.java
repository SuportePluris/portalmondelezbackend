package br.com.portalpluris.bean;

public class CsCdtbFollowupFoupBean {

	int foup_id_cd_followup;
	String foup_ds_followup;
	String foup_dh_registro;
	String foup_in_inativo;
	int foup_id_cd_usuariocriacao;
	int foup_id_cd_usuarioatualizacao;
	String foup_dh_atualizacao;
	int stat_id_cd_status;
	String stat_ds_status;
	
	
	
	public String getStat_ds_status() {
		return stat_ds_status;
	}

	public void setStat_ds_status(String stat_ds_status) {
		this.stat_ds_status = stat_ds_status;
	}

	public int getStat_id_cd_status() {
		return stat_id_cd_status;
	}

	public void setStat_id_cd_status(int stat_id_cd_status) {
		this.stat_id_cd_status = stat_id_cd_status;
	}

	public int getFoup_id_cd_followup() {
		return foup_id_cd_followup;
	}

	public void setFoup_id_cd_followup(int foup_id_cd_followup) {
		this.foup_id_cd_followup = foup_id_cd_followup;
	}

	public String getFoup_ds_followup() {
		return foup_ds_followup;
	}

	public void setFoup_ds_followup(String foup_ds_followup) {
		this.foup_ds_followup = foup_ds_followup;
	}

	public String getFoup_dh_registro() {
		return foup_dh_registro;
	}

	public void setFoup_dh_registro(String foup_dh_registro) {
		this.foup_dh_registro = foup_dh_registro;
	}

	public String getFoup_in_inativo() {
		return foup_in_inativo;
	}

	public void setFoup_in_inativo(String foup_in_inativo) {
		this.foup_in_inativo = foup_in_inativo;
	}

	public int getFoup_id_cd_usuariocriacao() {
		return foup_id_cd_usuariocriacao;
	}

	public void setFoup_id_cd_usuariocriacao(int foup_id_cd_usuariocriacao) {
		this.foup_id_cd_usuariocriacao = foup_id_cd_usuariocriacao;
	}

	public int getFoup_id_cd_usuarioatualizacao() {
		return foup_id_cd_usuarioatualizacao;
	}

	public void setFoup_id_cd_usuarioatualizacao(int foup_id_cd_usuarioatualizacao) {
		this.foup_id_cd_usuarioatualizacao = foup_id_cd_usuarioatualizacao;
	}

	public String getFoup_dh_atualizacao() {
		return foup_dh_atualizacao;
	}

	public void setFoup_dh_atualizacao(String foup_dh_atualizacao) {
		this.foup_dh_atualizacao = foup_dh_atualizacao;
	}

}
