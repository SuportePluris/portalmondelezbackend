package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.portalpluris.bean.ContatosBean;
import br.com.portalpluris.bean.RetornoInsertContatoBean;
import br.com.portalpluris.bean.csCdtbContatosBean;

public class CsCdtbContatosContDao {
//	CRIADO POR RAFA 03/08/2021
	public String getContatos(String tipo, String idEmbarque) throws SQLException, JsonProcessingException {
		
		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		ResultSet rs = null;
		String codigo = "";
		if(tipo.equalsIgnoreCase("TRANSPORTADORA")) {
			//BUSCA QUAL O SCAC DA TRANSPORTADORA PELO EMBARQUE
			codigo = getIdTransportadora(idEmbarque);
		}else if(tipo.equalsIgnoreCase("CLIENTE")) {
			//BUSCA QUAL O CÓDIGO DO CLIENTE PELO EMBARQUE
			codigo = getIdCliente(idEmbarque);
		}else if(tipo.equalsIgnoreCase("FOCAL")) {
			//BUSCA QUAL O CÓDIGO DO CLIENTE PELO EMBARQUE
			codigo = getIdCliente(idEmbarque);
		}
		
		String retorno = "";
		if(!codigo.equals("")) {
			pst = conn.prepareStatement("SELECT CONT_DS_EMAILCONTATO FROM CS_CDTB_CONTATOS_CONT WHERE CONT_DS_TIPOCONTATO = ? AND CONT_DS_CODIGO = ?");
			
			pst.setString(1, tipo);
			pst.setString(2, codigo);
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				retorno += rs.getString("CONT_DS_EMAILCONTATO") + ",";
			}
			if(!retorno.equalsIgnoreCase("")) {
				retorno = retorno.substring(0, retorno.length() - 1);
			}
			
		}
		
		csCdtbContatosBean contatos = new csCdtbContatosBean();
		contatos.setContatos(retorno);
		String jsonRetorno = "";
		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(contatos);
		return jsonRetorno;
	}
	
	
public csCdtbContatosBean getContatosEmails(String tipo, String idEmbarque) throws SQLException, JsonProcessingException {
		
		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		ResultSet rs = null;
		String codigo = "";
		if(tipo.equalsIgnoreCase("TRANSPORTADORA")) {
			//BUSCA QUAL O SCAC DA TRANSPORTADORA PELO EMBARQUE
			codigo = getIdTransportadora(idEmbarque);
		}else if(tipo.equalsIgnoreCase("CLIENTE")) {
			//BUSCA QUAL O CÓDIGO DO CLIENTE PELO EMBARQUE
			codigo = getIdCliente(idEmbarque);
		}else if(tipo.equalsIgnoreCase("FOCAL")) {
			//BUSCA QUAL O CÓDIGO DO CLIENTE PELO EMBARQUE
			codigo = getIdCliente(idEmbarque);
		}
		
		String retorno = "";
		if(!codigo.equals("")) {
			pst = conn.prepareStatement("SELECT CONT_DS_EMAILCONTATO FROM CS_CDTB_CONTATOS_CONT WHERE CONT_DS_TIPOCONTATO = ? AND CONT_DS_CODIGO = ?");
			
			pst.setString(1, tipo);
			pst.setString(2, codigo);
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				retorno += rs.getString("CONT_DS_EMAILCONTATO") + ",";
			}
			if(!retorno.equalsIgnoreCase("")) {
				retorno = retorno.substring(0, retorno.length() - 1);
			}
			
		}
		
		csCdtbContatosBean contatos = new csCdtbContatosBean();
		contatos.setContatos(retorno);
		
		return contatos;
	}
	
	public String getIdCliente(String idEmbarque) throws SQLException {
		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		ResultSet rs = null;
		
		pst = conn.prepareStatement("SELECT TOP 1 EMBA_ID_CD_CODCLIENTE FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA WHERE EMBA_ID_CD_EMBARQUE = ?");
		
		pst.setString(1, idEmbarque);
		
		rs = pst.executeQuery();
		String retorno = "";
		if(rs.next()) {
			retorno = rs.getString("EMBA_ID_CD_CODCLIENTE");
		}
		
		return retorno;
	}
	
	public String getIdTransportadora(String idEmbarque) throws SQLException {
		Connection conn = DAO.getConexao();

		PreparedStatement pst;
		ResultSet rs = null;
		
		pst = conn.prepareStatement("SELECT TOP 1 EMBA_DS_COD_TRANSPORTADORA FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA WHERE EMBA_ID_CD_EMBARQUE = ?");
		
		pst.setString(1, idEmbarque);
		
		rs = pst.executeQuery();
		String retorno = "";
		if(rs.next()) {
			retorno = rs.getString("EMBA_DS_COD_TRANSPORTADORA");
		}
		
		return retorno;
	}


	public RetornoInsertContatoBean insertContato(ContatosBean contBean) throws SQLException {
		
		RetornoInsertContatoBean insertContatoRet = new RetornoInsertContatoBean();		
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dhImportacao = sdf.format(date);
		

		try{
		
				DAO dao = new DAO();
				Connection con = dao.getConexao();
		
				PreparedStatement pst = con.prepareStatement(""
						
				+" 	INSERT INTO CS_CDTB_CONTATOS_CONT "
				+"( "
				+" CONT_DH_REGISTRO, "
				+" CONT_DS_TIPOCONTATO, "
				+" CONT_NM_NOMECONTATO, "
				+" CONT_DS_EMAILCONTATO, "
				+" CONT_DS_TELEFONECONTATO, "
				+" CONT_DS_CODIGO, "
				+" USER_ID_CD_USUARIO,"
				+" CONT_DS_INATIVO"
				+")"
				+"VALUES (?, ?, ?, ?, ?, ?,? ,?)");
		
				pst.setString(1,dhImportacao);
				pst.setString(2, contBean.getCont_ds_tipocontato());
				pst.setString(3, contBean.getCont_nm_nomecontato());
				pst.setString(4, contBean.getCont_ds_emailcontato());
				pst.setString(5, contBean.getCont_ds_telefonecontato());
				pst.setString(6, contBean.getCont_ds_codigocontato());
				pst.setString(7, contBean.getUser_id_cd_usuario());
				pst.setString(8, "N");
				
				boolean sucess = pst.execute();
			
				 if(!sucess){
					 
					 insertContatoRet.setMenssagem("Contato inserido com sucesso");
					 insertContatoRet.setIdErro("0");
					 
				 }else{
					 insertContatoRet.setMenssagem("Erro ao inserir o Contato");
					 insertContatoRet.setIdErro("3");
				 }
				 
				}catch (SQLException e){
					
					 insertContatoRet.setMenssagem("Erro"+e);
					
					return insertContatoRet;
					
				}
				 
				
				 return insertContatoRet;
			}


	public boolean slctContato(ContatosBean contBean) throws SQLException {
		
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst;
		ResultSet rs = null;
		
		pst = conn.prepareStatement("SELECT * FROM CS_CDTB_CONTATOS_CONT WHERE CONT_DS_TIPOCONTATO = ? AND CONT_DS_CODIGO = ? ");
		
		pst.setString(1, contBean.getCont_ds_tipocontato());
		pst.setString(2, contBean.getCont_ds_codigocontato());
		
		rs = pst.executeQuery();
		
		if(rs.next()) {
			
			return true;
			
		}
		
		
		return false;
	}
	

}
