package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.HierarquiaClienteBean;

public class CsCdtbHierarquiaClienteDao {

	public HierarquiaClienteBean slcHierarquiaCliente(int clienteCustumer) {

		HierarquiaClienteBean hierarquiaClieRet = new HierarquiaClienteBean();
		hierarquiaClieRet.setCdCliente(0);

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbHierarquiaClienteDao");
		logsBean.setLogsDsMetodo("slcHierarquiaCliente");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio da Consulta Hierarquia Cliente *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			Connection conn = DAO.getConexao();
			ResultSet rs = null;

			PreparedStatement pst;
			String query = "" + " SELECT " + "		" + "		HQCL_ID_CLIENTE, " + "		HQCL_DS_RAZAOSOCIAL, "
					+ "		HQCL_DS_NIVEL1, " + "		HQCL_DS_NIVEL2, " + "		HQCL_CD_GERENTENACIONAL, "
					+ "		HQCL_DS_GERENTENACIONAL, " + "		HQCL_CD_GERENTEREGIONAL, "
					+ "		HQCL_DS_GERENTEREGIONAL, " + "		HQCL_CD_GERENTEAREA, " + "		HQCL_DS_GERENTEAREA, "
					+ "		HQCL_CD_VENDEDOR, " + "		HQCL_DS_VENDEDOR, " + "		HQCL_CD_SALESDISTRICT " + " FROM "
					+ "		CS_CDTB_HIERARQUIACLIENTE_HQCL (NOLOCK) " + " WHERE " + "		HQCL_ID_CLIENTE = ?";

			pst = conn.prepareStatement(query);
			pst.setInt(1, clienteCustumer);

			rs = pst.executeQuery();

			while (rs.next()) {

				hierarquiaClieRet.setCdCliente(rs.getInt("HQCL_ID_CLIENTE"));
				hierarquiaClieRet.setRazaoSocial(rs.getString("HQCL_DS_RAZAOSOCIAL"));
				hierarquiaClieRet.setNivel1(rs.getString("HQCL_DS_NIVEL1"));
				hierarquiaClieRet.setNivel2(rs.getString("HQCL_DS_NIVEL2"));
				hierarquiaClieRet.setCdGerenteNacional(rs.getString("HQCL_CD_GERENTENACIONAL"));
				hierarquiaClieRet.setDsGerenteNacional(rs.getString("HQCL_DS_GERENTENACIONAL"));
				hierarquiaClieRet.setCdGerenteRegional(rs.getString("HQCL_CD_GERENTEREGIONAL"));
				hierarquiaClieRet.setDsGerenteRegional(rs.getString("HQCL_DS_GERENTEREGIONAL"));
				hierarquiaClieRet.setDsGerenteArea(rs.getString("HQCL_CD_GERENTEAREA"));
				hierarquiaClieRet.setCdGerenteArea(rs.getString("HQCL_DS_GERENTEAREA"));
				hierarquiaClieRet.setCdVendedor(rs.getString("HQCL_CD_VENDEDOR"));
				hierarquiaClieRet.setDsVendedor(rs.getString("HQCL_DS_VENDEDOR"));
				hierarquiaClieRet.setSalesDistrinct(rs.getInt("HQCL_CD_SALESDISTRICT"));

			}

		} catch (Exception e) {

			log.info("***** Erro na Consulta  Hierarquia Cliente " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim da Consulta  Hierarquia Cliente  *****");
			daoLog.createLog(logsBean);

		}

		return hierarquiaClieRet;
	}

	public void insertHierarquiaCliente(HierarquiaClienteBean hierarquiaCliente, int clienteCustumer,
			String dhImportacao, String nmArquivo) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbHierarquiaClienteDao");
		logsBean.setLogsDsMetodo("insertHierarquiaCliente");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Insert na base Hierarquia Cliente  *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst = con.prepareStatement("");

			pst = con.prepareStatement("" + " 	INSERT INTO CS_CDTB_HIERARQUIACLIENTE_HQCL ("
					+ "		HQCL_ID_CLIENTE, " + "		HQCL_DS_RAZAOSOCIAL, " + "		HQCL_DS_NIVEL1, "
					+ "		HQCL_DS_NIVEL2, " + "		HQCL_CD_GERENTENACIONAL, " + "		HQCL_DS_GERENTENACIONAL, "
					+ "		HQCL_CD_GERENTEREGIONAL, " + "		HQCL_DS_GERENTEREGIONAL, "
					+ "		HQCL_CD_GERENTEAREA, " + "		HQCL_DS_GERENTEAREA, " + "		HQCL_CD_VENDEDOR, "
					+ "		HQCL_DS_VENDEDOR, " + "		HQCL_CD_SALESDISTRICT, " + "		HQCL_DH_IMPORTACAO, "
					+ "		HQCL_NM_ARQUIVOPROCESSADO, " + "		HQCL_CD_USUARIOATUALIZACAO "
					+ ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)");

			pst.setInt(1, clienteCustumer);
			pst.setString(2, hierarquiaCliente.getRazaoSocial());
			pst.setString(3, hierarquiaCliente.getNivel1());
			pst.setString(4, hierarquiaCliente.getNivel2());
			pst.setString(5, hierarquiaCliente.getCdGerenteNacional());
			pst.setString(6, hierarquiaCliente.getDsGerenteNacional());
			pst.setString(7, hierarquiaCliente.getCdGerenteRegional());
			pst.setString(8, hierarquiaCliente.getDsGerenteRegional());
			pst.setString(9, hierarquiaCliente.getDsGerenteArea());
			pst.setString(10, hierarquiaCliente.getCdGerenteArea());
			pst.setString(11, hierarquiaCliente.getCdVendedor());
			pst.setString(12, hierarquiaCliente.getDsVendedor());
			pst.setInt(13, hierarquiaCliente.getSalesDistrinct());
			pst.setString(14, dhImportacao);
			pst.setString(15, nmArquivo);
			pst.setString(16, idUser);

			pst.execute();

		} catch (Exception e) {

			log.info("***** Erro no INSERT na base Hierarquia Cliente  " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  INSERT na base Hierarquia Cliente   *****");
			daoLog.createLog(logsBean);

		}
	}

	public void updateHierarquiaCliente(HierarquiaClienteBean hierarquiaCliente, int clienteCustumer,
			String dhImportacao, String nmArquivo) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("insertLeadTime");
		logsBean.setLogsDsMetodo("slcNomePlanta");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio update na base Hierarquia Cliente *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst;

			pst = con.prepareStatement(""

					+ "		UPDATE  CS_CDTB_HIERARQUIACLIENTE_HQCL SET" + "		HQCL_DS_RAZAOSOCIAL = ?, "
					+ "		HQCL_DS_NIVEL1 = ?, " + "		HQCL_DS_NIVEL2 = ?, "
					+ "		HQCL_CD_GERENTENACIONAL = ?, " + "		HQCL_DS_GERENTENACIONAL = ?, "
					+ "		HQCL_CD_GERENTEREGIONAL = ?, " + "		HQCL_DS_GERENTEREGIONAL = ?, "
					+ "		HQCL_CD_GERENTEAREA = ?, " + "		HQCL_DS_GERENTEAREA = ?, "
					+ "		HQCL_CD_VENDEDOR = ?, " + "		HQCL_DS_VENDEDOR = ?, " + "		HQCL_CD_SALESDISTRICT = ?, "
					+ "		HQCL_DH_ATUALIZACAO = ?, " + "		HQCL_NM_ARQUIVOPROCESSADO = ? "
					+ " 	WHERE  HQCL_ID_CLIENTE = ? ");

			pst.setString(1, hierarquiaCliente.getRazaoSocial());
			pst.setString(2, hierarquiaCliente.getNivel1());
			pst.setString(3, hierarquiaCliente.getNivel2());
			pst.setString(4, hierarquiaCliente.getCdGerenteNacional());
			pst.setString(5, hierarquiaCliente.getDsGerenteNacional());
			pst.setString(6, hierarquiaCliente.getCdGerenteRegional());
			pst.setString(7, hierarquiaCliente.getDsGerenteRegional());
			pst.setString(8, hierarquiaCliente.getCdGerenteArea());
			pst.setString(9, hierarquiaCliente.getDsGerenteArea());
			pst.setString(10, hierarquiaCliente.getCdVendedor());
			pst.setString(11, hierarquiaCliente.getDsVendedor());
			pst.setInt(12, hierarquiaCliente.getSalesDistrinct());
			pst.setString(13, dhImportacao);
			pst.setString(14, nmArquivo);
			pst.setInt(15, clienteCustumer);

			pst.executeUpdate();

		} catch (Exception e) {

			log.info("***** Erro no UPDATE na base do Hierarquia Cliente  " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  UPDATE na base do Hierarquia Cliente  *****");
			daoLog.createLog(logsBean);

		}

	}

}
