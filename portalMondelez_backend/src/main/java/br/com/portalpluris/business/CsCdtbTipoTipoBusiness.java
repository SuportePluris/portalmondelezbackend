package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbTipoTipoBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsCdtbTipoTipoDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbTipoTipoBusiness {

	CsCdtbTipoTipoDao tipoDao = new CsCdtbTipoTipoDao();
	private DAOLog daoLog = new DAOLog();

	public String createUpdateTipo(CsCdtbTipoTipoBean tipoBean) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogTipo(tipoBean, "createUpdateTipo");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = tipoDao.createUpdateTipo(tipoBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getListTipo(String tipoInAtivo) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogTipo(null, "getListTipo");
		logsBean.setLogsTxInfoentrada(tipoInAtivo);

		List<CsCdtbTipoTipoBean> listTipoBean = new ArrayList<CsCdtbTipoTipoBean>();

		String jsonRetorno = "";

		try {

			listTipoBean = tipoDao.getListTipo(tipoInAtivo);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(listTipoBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;

	}

	private EsLgtbLogsLogsBean getLogTipo(CsCdtbTipoTipoBean tipoBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTipoTipoBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");
		logsBean.setLogsTxInfoentrada(getAllValues(tipoBean));

		if (tipoBean != null) {
			logsBean.setLogsDsChave(tipoBean.getTipo_id_cd_tipo() + "");
		}

		return logsBean;
	}

	private String getAllValues(CsCdtbTipoTipoBean tipoBean) {

		String all = "";

		if (tipoBean != null) {
			all = "" + tipoBean.getTipo_id_cd_tipo() + ";" + tipoBean.getTipo_ds_tipo() + ";"
					+ tipoBean.getTipo_in_inativo() + ";" + tipoBean.getTipo_dh_registro() + ";" + "";
		}

		return all;
	}

}
