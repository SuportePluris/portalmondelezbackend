package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbSkudeliverySkudBean;
import br.com.portalpluris.bean.CsNgtbEmbarqueAgendamentoEmbaBean;

public class ProcessaCargaDao {
	
	/**
	 * Metodo responsavel por coletar as informacoes das tabelas LGTB necessarias para criar os registros de embarque e skud
	 * @author Caio Fernandes
	 * @param 
	 * */
	public void selectLgtbsCarga() throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		ResultSet ret = null;
		ResultSet retEmba = null;
		List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryBean = new ArrayList<CsCdtbSkudeliverySkudBean>();
		List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryRecriarBean = new ArrayList<CsCdtbSkudeliverySkudBean>();
		List<CsNgtbEmbarqueAgendamentoEmbaBean> lstEmbarqueAgendamentoBean = new ArrayList<CsNgtbEmbarqueAgendamentoEmbaBean>();
		List<CsNgtbEmbarqueAgendamentoEmbaBean> lstEmbarqueAgendamentoUpdateBean = new ArrayList<CsNgtbEmbarqueAgendamentoEmbaBean>();
		List<String> lstEmbarqueAgendamentoUpdateLgtbsBean = new ArrayList<String>();
		UtilDate utilDate = new UtilDate();
		
		try {
			
			if(conn != null) {
				
				// REALIZAR O SELECT
				log.info("[selectLgtbsCarga] - REALIZANDO SELECT ");
				
				// QUERY DA SKUD
				String cSQL = "";
				cSQL += " SELECT DISTINCT ";
				cSQL += " 	SKUD.SKUD_CD_DELIVERY,   ";
				cSQL += " 	SHIP.SHIP_CD_SHIPMENTID, 	  ";
				cSQL += " 	SKUD.SKUD_CD_REFERENCEDOCUMENT, 	  ";
				cSQL += " 	SKUD.SKUD_DS_TOTALWEIGHT, 	  ";
				cSQL += " 	SKUD.SKUD_DS_NETWEIGHT,	  ";
				cSQL += " 	SKUD.SKUD_DS_MATERIAL,	  ";
				cSQL += " 	SKUD.SKUD_DS_DESCRIPTION,	  ";
				cSQL += " 	SKUD.SKUD_DS_VOLUME,   ";
				cSQL += " 	SKUD.SKUD_DS_DELIVERYQUANTITY,   ";
				cSQL += " 	SKUD.SKUD_DH_DELIVERYDATE,  ";
				cSQL += " 	PEDI.PEDI_DS_PURCHASEORDERNUMBER,  ";
				cSQL += " 	PEDI.PEDI_DS_NETVALUE,  ";
				cSQL += " 	PEDI.PEDI_DS_SALESDOCUMENTTYPE  ";
				cSQL += " FROM   ";
				cSQL += " 	CS_LGTB_SHIPMENT_SHIP SHIP (NOLOCK) ";
				cSQL += " 		INNER JOIN CS_LGTB_RESUMODNS_REDN REDN  (NOLOCK) ";
				cSQL += " 			ON SHIP.SHIP_CD_SHIPMENTID = REDN.REDN_DS_SHIPMENTTOP 			  ";
				cSQL += " 		INNER JOIN CS_LGTB_SKUDELIVERY_SKUD SKUD (NOLOCK) ";
				cSQL += " 			ON SKUD.SKUD_CD_DELIVERY = REDN.REDN_DS_DNN   ";
				cSQL += " 		INNER JOIN CS_LGTB_PEDIDOSDELIVERY_PEDI PEDI  (NOLOCK) ";
				cSQL += " 			ON SKUD.SKUD_CD_REFERENCEDOCUMENT = PEDI.PEDI_CD_SALESDOCUMENT  ";
				cSQL += " WHERE   ";
				cSQL += " 	(SKUD.SKUD_DS_STATUSPROCESSAMENTO IS NULL   ";
				cSQL += " 	OR PEDI.PEDI_DS_STATUSPROCESSAMENTO IS NULL   ";
				cSQL += " 	OR SHIP.SHIP_DS_STATUSPROCESSAMENTO IS NULL  ";
				cSQL += " 	OR REDN.REDN_DS_STATUSPROCESSAMENTO IS NULL)   ";
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(cSQL);
				
				ret = pst.executeQuery();
				
				while(ret.next()){
					
					// PREENCHER BEAN CDTB_SKUD
					CsCdtbSkudeliverySkudBean skuDeliveryBean = new CsCdtbSkudeliverySkudBean();
					
					skuDeliveryBean.setSkud_id_cd_shipment(ret.getString("SHIP_CD_SHIPMENTID"));
					skuDeliveryBean.setSkud_id_cd_delivery(ret.getString("SKUD_CD_DELIVERY"));
					skuDeliveryBean.setSkud_ds_pesobruto(ret.getString("SKUD_DS_TOTALWEIGHT"));
					skuDeliveryBean.setSkud_ds_pesoliquido(ret.getString("SKUD_DS_NETWEIGHT"));
					skuDeliveryBean.setSkud_ds_codsku(ret.getString("SKUD_DS_MATERIAL"));
					skuDeliveryBean.setSkud_ds_itemsku(ret.getString("SKUD_DS_DESCRIPTION"));
					skuDeliveryBean.setSkud_ds_volumecubico(ret.getString("SKUD_DS_VOLUME"));
					skuDeliveryBean.setSkud_nr_quantidade(ret.getString("SKUD_DS_DELIVERYQUANTITY"));
					skuDeliveryBean.setSkud_dh_delivery(ret.getDate("SKUD_DH_DELIVERYDATE"));
					skuDeliveryBean.setSkud_ds_pedidomondelez(ret.getString("SKUD_CD_REFERENCEDOCUMENT"));
					// **RAFA** - ATRIBUINDO O PEDIDO MONDELEZ A UMA VARIAVEL PARA USAR NA VALIDAÇÃO DE STATUS
					String pedidoMondelez = ret.getString("SKUD_CD_REFERENCEDOCUMENT");
					skuDeliveryBean.setSkud_ds_pedidocliente(ret.getString("PEDI_DS_PURCHASEORDERNUMBER"));
					skuDeliveryBean.setSkud_ds_valor(ret.getString("PEDI_DS_NETVALUE"));
					skuDeliveryBean.setSkud_ds_tipovenda(ret.getString("PEDI_DS_SALESDOCUMENTTYPE"));
					
					// VERIFICACAO PARA IDENTIFICAR DELIVERY EXISTENTE NA CDTB (chaves: id_delivery/id_shipment)
					if(!existeDelivery(skuDeliveryBean.getSkud_id_cd_shipment(), skuDeliveryBean.getSkud_id_cd_delivery())) {
						// CASO NAO EXISTIR INSERIR UM NOVO
						// ADICIONAR BEAN NA LISTA DE INSERT
						lstSkuDeliveryBean.add(skuDeliveryBean);
						
					} else {
						// CASO EXISTIR EXCLUIR REGISTRO E INSERIR UM NOVO
						// ADICIONAR BEAN NA LISTA DE DELETE E INSERT
						lstSkuDeliveryRecriarBean.add(skuDeliveryBean);
					}
					
					lstEmbarqueAgendamentoUpdateLgtbsBean.add(skuDeliveryBean.getSkud_id_cd_shipment());
				}
				
				// CRIAR REGISTROS SKUD
				if (lstSkuDeliveryBean.size() > 0) {
					insertSkudDelivery(lstSkuDeliveryBean);
				}
				
				// PARA SKUD JA EXISTENTES EXCLUIR E CRIAR NOVOS REGISTROS SKUD 
				if(lstSkuDeliveryRecriarBean.size() > 0) {
					excluirSkudDelivery(lstSkuDeliveryRecriarBean);
					insertSkudDelivery(lstSkuDeliveryRecriarBean);
				}
				
				

				// QUERY DO EMBARQUE
				String cSQLEmba = "";
				cSQLEmba += " SELECT DISTINCT ";
				cSQLEmba += " 	SHIP.SHIP_CD_SHIPMENTID, 	 ";
				cSQLEmba += "  	SHIP.SHIP_DS_STATUSOFERTA,  ";
				cSQLEmba += "  	ISNULL(SHIP.SHIP_DS_PLANNEDSCAC, '') AS SHIP_DS_PLANNEDSCAC,  ";
				cSQLEmba += "  	SHIP.SHIP_CD_CODCLIENTE,  ";
				cSQLEmba += "  	SHIP.SHIP_DS_SOURCELOCATIONID,  ";
				cSQLEmba += "  	SHIP.SHIP_DS_STARTTIME,  ";
				cSQLEmba += "  	SHIP.SHIP_DS_TRUCKOUCARRETA,  ";
				cSQLEmba += "  	SHIP.SHIP_DS_EQUIPMENTGROUP,  ";
				cSQLEmba += "  	SHIP.SHIP_DS_DESTCITY,  ";
				cSQLEmba += " 	SHIP.SHIP_DS_CLIENTEAGENDADO,  ";
				cSQLEmba += "  	SHIP.SHIP_DS_TIPO  ";
				cSQLEmba += "  FROM  ";
				cSQLEmba += " 		CS_LGTB_SHIPMENT_SHIP SHIP (NOLOCK) ";
				cSQLEmba += " 			INNER JOIN CS_LGTB_RESUMODNS_REDN REDN (NOLOCK) ";
				cSQLEmba += " 				ON SHIP.SHIP_CD_SHIPMENTID = REDN.REDN_DS_SHIPMENTTOP 			 ";
				cSQLEmba += " 			INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK) ";
				cSQLEmba += " 				ON REDN.REDN_DS_DNN = SKUD.SKUD_ID_CD_DELIVERY 			 ";
				cSQLEmba += "  WHERE  ";
				cSQLEmba += " 		SHIP.SHIP_DS_STATUSPROCESSAMENTO IS NULL ";
				cSQLEmba += " 		AND REDN.REDN_DS_STATUSPROCESSAMENTO IS NULL  ";
				
				PreparedStatement pstEmba;	
				pstEmba = conn.prepareStatement(cSQLEmba);
				
				retEmba = pstEmba.executeQuery();
				
				while(retEmba.next()){

					// PREENCHER BEAN CDTB_EMBARQUE 
					CsNgtbEmbarqueAgendamentoEmbaBean embarqueAgendamentoBean = new CsNgtbEmbarqueAgendamentoEmbaBean();

					embarqueAgendamentoBean.setEmba_id_cd_shipment(retEmba.getString("SHIP_CD_SHIPMENTID"));
					embarqueAgendamentoBean.setEmba_ds_cod_transportadora(retEmba.getString("SHIP_DS_PLANNEDSCAC"));
					embarqueAgendamentoBean.setEmba_id_cd_codcliente(retEmba.getString("SHIP_CD_CODCLIENTE"));
					String emba_ds_planta = retEmba.getString("SHIP_DS_SOURCELOCATIONID");
					if(emba_ds_planta.equalsIgnoreCase("") || emba_ds_planta != null) {
						emba_ds_planta.replace(".0", "");
					}
					embarqueAgendamentoBean.setEmba_ds_planta(emba_ds_planta);
					embarqueAgendamentoBean.setEmba_ds_veiculo(retEmba.getString("SHIP_DS_TRUCKOUCARRETA"));
					String statusOferta = retEmba.getString("SHIP_DS_STATUSOFERTA");
					embarqueAgendamentoBean.setEmba_in_transportadoraconfirmada(executarRegraStatusOferta(embarqueAgendamentoBean.getEmba_ds_cod_transportadora(), statusOferta));
					embarqueAgendamentoBean.setEmba_ds_tipoagendamento(calcularTipoAgendamento(embarqueAgendamentoBean.getEmba_id_cd_codcliente()));

					String clienteAgendado = retEmba.getString("SHIP_DS_CLIENTEAGENDADO");

					// **RAFA** - ATRIBUINDO O ID SHIPMENT A UMA VARIAVEL PARA USAR NA VALIDAÇÃO DE STATUS
					String shipmentId = retEmba.getString("SHIP_CD_SHIPMENTID");
					
					//ALTERADO POR RAFA - ENVIANDO TB PARAMETRO DO COD CLIENTE PARA SABER SE ESSE CLIENTE NECESSITA DE NF
					String codCliente = retEmba.getString("SHIP_CD_CODCLIENTE");
					
					embarqueAgendamentoBean.setEmba_id_cd_status(executarRegraStatus(clienteAgendado, codCliente, shipmentId));
					embarqueAgendamentoBean.setEmba_ds_tipoveiculo(retEmba.getString("SHIP_DS_EQUIPMENTGROUP"));
					embarqueAgendamentoBean.setEmba_ds_tipocarga(retEmba.getString("SHIP_DS_TIPO"));
					embarqueAgendamentoBean.setEmba_ds_destcity(retEmba.getString("SHIP_DS_DESTCITY"));
					embarqueAgendamentoBean.setEmba_dh_shipment(utilDate.convertStringToDateYYYY_MM_dd_HH_MM_SS_2(retEmba.getString("SHIP_DS_STARTTIME")));	
					Date leadTime = new Date();
					leadTime = calcularLeadTime(embarqueAgendamentoBean.getEmba_dh_shipment(), embarqueAgendamentoBean.getEmba_ds_planta(), embarqueAgendamentoBean.getEmba_ds_tipocarga(), embarqueAgendamentoBean.getEmba_ds_destcity());
					embarqueAgendamentoBean.setEmba_dh_leadtime(leadTime); 
					
					//DANILLO *** 29/07/2021 *** INCLUSÃO DE PERIODO BASEADO NA DATA DE EMISSÃO DA NOTA FISCAL CASO NÃO EXISTA PEGAR O MES VIGENTE
					//embarqueAgendamentoBean.setEmba_ds_periodo(calcularPeriodo(leadTime));
					//AGORA CONSULTAR NOTAFISCAL PELA SHIPMENT
					CsLgtbNotaFiscalNofiDao nofiDao = new CsLgtbNotaFiscalNofiDao();
					String dataPeriodo = nofiDao.slctNotaFiscalByShipment(retEmba.getString("SHIP_CD_SHIPMENTID"));
					if(!dataPeriodo.equalsIgnoreCase("")){
						CsNgtbEmbarqueEmbaDao embaDao = new CsNgtbEmbarqueEmbaDao();
						embarqueAgendamentoBean.setEmba_ds_periodo(embaDao.getPeriodo(dataPeriodo));
					}else{
						embarqueAgendamentoBean.setEmba_ds_periodo(calcularPeriodo()); 
					}
						

					if(!existeEmbarque(embarqueAgendamentoBean.getEmba_id_cd_shipment())) {
						// CASO NAO EXISTIR INSERIR UM NOVO
						// ADICIONAR BEAN NA LISTA DE INSERT
						lstEmbarqueAgendamentoBean.add(embarqueAgendamentoBean);
					} else {
						// CASO EXISTIR REALIZAR O UPDATE DO REGISTRO 
						lstEmbarqueAgendamentoUpdateBean.add(embarqueAgendamentoBean);
					}
					
					

				}
				
				// CRIAR REGISTROS EMBA
				if(lstEmbarqueAgendamentoBean.size() > 0) {
					insertEmbarqueAgendamento(lstEmbarqueAgendamentoBean);
				}
				
				// UPDATE REGISTROS EMBA
				if(lstEmbarqueAgendamentoUpdateBean.size() > 0) {
					updateEmbarqueAgendamento(lstEmbarqueAgendamentoUpdateBean);
				}	
				
				updateLgtbs2(lstEmbarqueAgendamentoUpdateLgtbsBean);
				
				log.info("[selectLgtbsCarga] - INSERT REALIZADO COM SUCESSO ");
				
			
		} else {
			log.info("[selectLgtbsCarga] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
			
		} catch (Exception e) {
			log.info("[selectLgtbsCarga] - ERRO AO PROCESSAR CARGA: " + e.getMessage());			
		}
		
		log.info("[selectLgtbsCarga] - FIM ");
		
	}
	
	
	/**
	 * Metodo responsavel por realizar o calculo do Lead Time 
	 * @param Date, String, String, String 
	 * @return Date dtLeadtime
	 * */
	public Date calcularLeadTime(Date dt, String planta, String tipoCarga, String shipDestCity) throws SQLException, ParseException {

		Logger log = Logger.getLogger(this.getClass().getName());	
		ResultSet retDtju = null;
		ResultSet retLdtm = null;
		ResultSet ret = null;
		Connection conn = DAO.getConexao();
		UtilDate dtUtil = new UtilDate();

		 String strDt = dtUtil.convertDateToYYYY_MM_dd(dt);
		 int nrDiaUtil = 0;
		 int nrLtl = 0;
		 int nrTl = 0;
		 int nrTlOrLtl = 0;
		 Date dtLeadtime = new Date();

		// REALIZAR A CONSULTA
		log.info("[calcularLeadTime] - REALIZANDO CONSULTA ");
		
		try {
			
			if(conn != null) {

				PreparedStatement pstDtju;	
				pstDtju = conn.prepareStatement(""
						+" SELECT "
						+ " 	NR_DIAUTIL "
						+ " FROM "
						+ "		CS_CDTB_DTJULIANA_DTJU (NOLOCK) "
						+ " WHERE "
						+ "		DH_DATAJULIANA = ? ");

				pstDtju.setString(1, strDt);

				retDtju = pstDtju.executeQuery();

				if(retDtju.next()) {
					nrDiaUtil = retDtju.getInt("NR_DIAUTIL");
				}
				
				PreparedStatement pstLdtm;
				pstLdtm = conn.prepareStatement(""
						+" SELECT "
						+ " 	LDTM_DS_LTL, "
						+ " 	LDTM_DS_TL "
						+ " FROM "
						+ "		CS_CDTB_LEADTIME_LDTM (NOLOCK) "
						+ " WHERE "
						+ "		LDTM_CD_SOURCELOCATION = ? "
						+ "		AND LDTM_DS_DESTINATIONCITY = ? "
						+ "");

				pstLdtm.setString(1, planta);
				pstLdtm.setString(2, shipDestCity);
				
				retLdtm = pstLdtm.executeQuery();
				
				if(retLdtm.next()) {
					nrLtl = retLdtm.getInt("LDTM_DS_LTL");
					nrTl = retLdtm.getInt("LDTM_DS_TL");
				}
				
				// VERIFICAR TIPO DE CARGA PARA REALIZAR A SOMA
				if (tipoCarga.equalsIgnoreCase("LTL")) {
					nrTlOrLtl = nrLtl + 1;
					
				} else {
					nrTlOrLtl = nrTl + 1;
				}
				
				nrDiaUtil = nrTlOrLtl + nrDiaUtil;
				
				
				PreparedStatement pst;
				pst = conn.prepareStatement(""
						+" SELECT "
						+ " 	DH_DATAJULIANA "
						+ " FROM "
						+ "		CS_CDTB_DTJULIANA_DTJU (NOLOCK) "
						+ " WHERE "
						+ "		NR_DIAUTIL = ? "
						+ "");
				
				pst.setInt(1, nrDiaUtil);
				ret = pst.executeQuery();
				
				if(ret.next()) {
					dtLeadtime = ret.getDate("DH_DATAJULIANA");
				}
				
			}
			
		} catch (Exception e) {
			log.info("[calcularLeadTime] - ERRO AO CALCULAR LEADTIME: " + e.getMessage());
		}

		return dtLeadtime;

	}
	
	/**
	 * Metodo responsavel por realizar o update das informacoes de "REDN_DS_STATUSPROCESSAMENTO" e "REDN_DH_PROCESSAMENTO" na tabela CS_LGTB_RESUMODNS_REDN
	 * @author Caio Fernandes
	 * @param String
	 * */
	public void updateResumoDnsProcessados(String idDelivery) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[updateResumoDnsProcessados] - INICIO ");
		
		if(conn != null) {
			
			// REALIZAR O UPDATE
			log.info("[updateResumoDnsProcessados] - REALIZANDO UPDATE ");
			
			PreparedStatement pst;	
			pst = conn.prepareStatement(""
					+" UPDATE "
					+ "		CS_LGTB_RESUMODNS_REDN "
					+ "SET "
					+ "		REDN_DS_STATUSPROCESSAMENTO = 'S', "
					+ "		REDN_DH_PROCESSAMENTO = GETDATE() "
					+ "WHERE "
					+ "		REDN_DS_DNN = ? "
					+ "		AND REDN_DS_STATUSPROCESSAMENTO IS NULL "
					+ "");
			
			pst.setString(1, idDelivery);
			pst.execute();
			
			log.info("[updateResumoDnsProcessados] - UPDATE REALIZADO COM SUCESSO ");
			
		} else {
			log.info("[updateResumoDnsProcessados] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updateResumoDnsProcessados] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel por realizar o update das informacoes de "PEDI_DS_STATUSPROCESSAMENTO" e "PEDI_DH_PROCESSAMENTO" na tabela CS_LGTB_PEDIDOSDELIVERY_PEDI
	 * @author Caio Fernandes
	 * @param String 
	 * */
	public void updatePedidosDeliveryProcessados(String idSalesdocument) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[updatePedidosDeliveryProcessados] - INICIO ");
		
		if(conn != null) {
			
			// REALIZAR O UPDATE
			log.info("[updatePedidosDeliveryProcessados] - REALIZANDO UPDATE ");
			
			PreparedStatement pst;	
			pst = conn.prepareStatement(""
					+" UPDATE "
					+ "		CS_LGTB_PEDIDOSDELIVERY_PEDI "
					+ "SET "
					+ "		PEDI_DS_STATUSPROCESSAMENTO = 'S', "
					+ "		PEDI_DH_PROCESSAMENTO = GETDATE() "
					+ "WHERE "
					+ "		PEDI_CD_SALESDOCUMENT = ? "
					+ "		AND PEDI_DS_STATUSPROCESSAMENTO IS NULL "
					+ "");
			
			pst.setString(1, idSalesdocument);
			pst.execute();
			
			log.info("[updatePedidosDeliveryProcessados] - UPDATE REALIZADO COM SUCESSO ");
			
		} else {
			log.info("[updatePedidosDeliveryProcessados] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updatePedidosDeliveryProcessados] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel por realizar o update das informacoes de "SHIP_DS_STATUSPROCESSAMENTO" e "SHIP_DH_PROCESSAMENTO" na tabela CS_LGTB_SHIPMENT_SHIP
	 * @author Caio Fernandes
	 * @param String
	 * */
	public void updateShipmentProcessados(String idShipment) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[updateShipmentProcessados] - INICIO ");
		
		if(conn != null) {
			
			// REALIZAR O UPDATE
			log.info("[updateShipmentProcessados] - REALIZANDO UPDATE ");
			
			PreparedStatement pst;	
			pst = conn.prepareStatement(""
					+" UPDATE "
					+ "		CS_LGTB_SHIPMENT_SHIP "
					+ "SET "
					+ "		SHIP_DS_STATUSPROCESSAMENTO = 'S', "
					+ "		SHIP_DH_PROCESSAMENTO = GETDATE() "
					+ "WHERE "
					+ "		SHIP_CD_SHIPMENTID = ? "
					+ "		AND SHIP_DS_STATUSPROCESSAMENTO IS NULL "
					+ "");
			
			pst.setString(1, idShipment);
			pst.execute();
			
			log.info("[updateShipmentProcessados] - UPDATE REALIZADO COM SUCESSO ");
			
		} else {
			log.info("[updateShipmentProcessados] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updateShipmentProcessados] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel por realizar o update das informacoes de "SKUD_DS_STATUSPROCESSAMENTO" e "SKUD_DH_PROCESSAMENTO" na tabela CS_LGTB_SKUDELIVERY_SKUD
	 * @author Caio Fernandes
	 * @param List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryBean
	 * */
	public void updateSkuDeliveryProcessados(String idReferenceDocument) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		Connection conn = DAO.getConexao();
		
		log.info("[updateSkuDeliveryProcessados] - INICIO ");
		
		if(conn != null) {
			
			// REALIZAR O UPDATE
			log.info("[updateSkuDeliveryProcessados] - REALIZANDO UPDATE ");
			
			PreparedStatement pst;	
			pst = conn.prepareStatement(""
					+" UPDATE "
					+ "		CS_LGTB_SKUDELIVERY_SKUD "
					+ "SET "
					+ "		SKUD_DS_STATUSPROCESSAMENTO = 'S',  "
					+ "		SKUD_DH_PROCESSAMENTO = GETDATE() "
					+ "WHERE "
					+ "		SKUD_CD_REFERENCEDOCUMENT = ? "
					+ "		AND SKUD_DS_STATUSPROCESSAMENTO IS NULL "
					+ "");
			
			pst.setString(1, idReferenceDocument);
			pst.execute();
			
			log.info("[updateSkuDeliveryProcessados] - UPDATE REALIZADO COM SUCESSO ");
			
		} else {
			log.info("[updateSkuDeliveryProcessados] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updateSkuDeliveryProcessados] - FIM ");
		
	}

	/**
	 * Metodo responsavel por realizar o insert das informacoes na tabela CS_CDTB_SKUDELIVERY_SKUD
	 * @author Caio Fernandes
	 * @param List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryBean
	 * */
	public void insertSkudDelivery(List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryBean) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[insertSkudDelivery] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstSkuDeliveryBean.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertPedidosDelivery] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" INSERT CS_CDTB_SKUDELIVERY_SKUD ( "
						+ "		SKUD_ID_CD_SHIPMENT, "
						+ "		SKUD_ID_CD_DELIVERY, 	"
						+ "		SKUD_DS_PESOBRUTO, 	"
						+ " 	SKUD_DS_PESOLIQUIDO, 	"
						+ " 	SKUD_DS_CODSKU,	"
						+ " 	SKUD_DS_ITEMSKU,	"
						+ " 	SKUD_DS_VOLUMECUBICO, "
						+ " 	SKUD_NR_QUANTIDADE, "
						+ " 	SKUD_DH_DELIVERY, "
						+ " 	SKUD_DS_PEDIDOMONDELEZ, "
						+ " 	SKUD_DS_PEDIDOCLIENTE, "
						+ " 	SKUD_DS_VALOR, "
						+ " 	SKUD_DS_TIPOVENDA )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?,?,?,?,?)	"
						+ "");
				
				pst.setLong(1, Long.parseLong(lstSkuDeliveryBean.get(i).getSkud_id_cd_shipment()));
				pst.setLong(2, Long.parseLong(lstSkuDeliveryBean.get(i).getSkud_id_cd_delivery()));
				pst.setDouble(3, Double.parseDouble(lstSkuDeliveryBean.get(i).getSkud_ds_pesobruto()));
				pst.setDouble(4, Double.parseDouble(lstSkuDeliveryBean.get(i).getSkud_ds_pesoliquido()));
				pst.setString(5, lstSkuDeliveryBean.get(i).getSkud_ds_codsku());
				pst.setString(6, lstSkuDeliveryBean.get(i).getSkud_ds_itemsku());
				pst.setDouble(7, Double.parseDouble(lstSkuDeliveryBean.get(i).getSkud_ds_volumecubico()));
				pst.setDouble(8, Double.parseDouble(lstSkuDeliveryBean.get(i).getSkud_nr_quantidade()));
				pst.setString(9, lstSkuDeliveryBean.get(i).getSkud_dh_delivery().toString());
				pst.setString(10, lstSkuDeliveryBean.get(i).getSkud_ds_pedidomondelez());
				pst.setString(11, lstSkuDeliveryBean.get(i).getSkud_ds_pedidocliente());
				pst.setString(12, lstSkuDeliveryBean.get(i).getSkud_ds_valor());
				pst.setString(13, lstSkuDeliveryBean.get(i).getSkud_ds_tipovenda());
				
				pst.execute();
				
				log.info("[insertSkudDelivery] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
			
		} else {
			log.info("[insertSkudDelivery] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[insertSkudDelivery] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel por realizar o update das informacoes na tabela CS_NGTB_EMBARQUEAGENDAMENTO_EMBA
	 * @author Caio Fernandes
	 * @param List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryBean
	 * */
	public void updateEmbarqueAgendamento(List<CsNgtbEmbarqueAgendamentoEmbaBean> lstEmbarqueAgendamentoUpdateBean) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		UtilDate util = new UtilDate();
		
		log.info("[updateEmbarqueAgendamento] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstEmbarqueAgendamentoUpdateBean.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[updateEmbarqueAgendamento] - REALIZANDO UPDATE ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" UPDATE "
						+ "		CS_NGTB_EMBARQUEAGENDAMENTO_EMBA "
						+ "SET "
						+ "		EMBA_DS_COD_TRANSPORTADORA = ?, "
						+ "		EMBA_ID_CD_CODCLIENTE = ?, "
						+ "		EMBA_DH_LEADTIME = ?, "
						+ "		EMBA_DS_PLANTA = ?, "
						+ "		EMBA_DH_SHIPMENT = ?, "
						+ "		EMBA_DS_VEICULO = ?, "
						+ "		EMBA_DS_TIPOVEICULO = ?, "
						+ "		EMBA_IN_TRANSPORTADORACONFIRMADA = ?, "
						+ "		EMBA_DS_PERIODO = ?, "
						+ "		EMBA_DS_TIPOCARGA = ?, "
						+ "		EMBA_ID_CD_STATUS = ?, "
						+ "		EMBA_DS_TIPOAGENDAMENTO = ? "
						+ "WHERE "
						+ "		EMBA_ID_CD_SHIPMENT = ?	"
						+ "");
				
				pst.setString(1, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_ds_cod_transportadora());
				pst.setLong(2, Long.parseLong(lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_id_cd_codcliente()));
				pst.setString(3, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_dh_leadtime()));
				pst.setString(4, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_ds_planta());
				pst.setString(5, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_dh_shipment()));
				pst.setString(6, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_ds_veiculo());
				pst.setString(7, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_ds_tipoveiculo());
				pst.setString(8, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_in_transportadoraconfirmada());
				pst.setString(9, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_ds_periodo());
				pst.setString(10, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_ds_tipocarga());
				pst.setString(11, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_id_cd_status());
				pst.setString(12, lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_ds_tipoagendamento());				
				pst.setLong(13, Long.parseLong(lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_id_cd_shipment()));
				
				pst.execute();
				log.info("[updateEmbarqueAgendamento] - UPDATE REALIZADO COM SUCESSO ");
				
				// ATUALIZAR FLAGS DE PROCESSADO
				//updateLgtbs(lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_id_cd_shipment());
				
				// CRIAR BACKUP DE REGISTRO EMBA
				log.info("[updateEmbarqueAgendamento] - CRIANDO REGISTRO DE BACKUP ");
				CsNgtbEmbarqueEmbaDao embarqueEmbaDao = new CsNgtbEmbarqueEmbaDao();
				embarqueEmbaDao.createBkpEmbarqueByIdShipment(lstEmbarqueAgendamentoUpdateBean.get(i).getEmba_id_cd_shipment());
				log.info("[updateEmbarqueAgendamento] - REGISTRO DE BACKUP CRIADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[updateEmbarqueAgendamento] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updateEmbarqueAgendamento] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel por realizar o insert das informacoes na tabela CS_NGTB_EMBARQUEAGENDAMENTO_EMBA
	 * @author Caio Fernandes
	 * @param List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryBean
	 * */
	public void insertEmbarqueAgendamento(List<CsNgtbEmbarqueAgendamentoEmbaBean> lstEmbarqueAgendamentoBean) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		UtilDate util = new UtilDate();
		
		log.info("[insertEmbarqueAgendamento] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstEmbarqueAgendamentoBean.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertEmbarqueAgendamento] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" INSERT CS_NGTB_EMBARQUEAGENDAMENTO_EMBA ( "
						+ "		EMBA_ID_CD_SHIPMENT, "
						+ "		EMBA_DS_COD_TRANSPORTADORA, 	"
						+ "		EMBA_ID_CD_CODCLIENTE, 	"
						+ " 	EMBA_DH_LEADTIME, 	"
						+ " 	EMBA_DS_PLANTA,	"
						+ " 	EMBA_DH_SHIPMENT,	"
						+ " 	EMBA_DS_VEICULO, "
						+ " 	EMBA_DS_TIPOVEICULO, "
						+ " 	EMBA_IN_TRANSPORTADORACONFIRMADA, "
						+ " 	EMBA_DS_PERIODO, "
						+ " 	EMBA_DS_TIPOCARGA, "
						+ " 	EMBA_ID_CD_STATUS, "
						+ " 	EMBA_DS_TIPOAGENDAMENTO )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?,?,?,?,?)	"
						+ "");
				
				pst.setLong(1, Long.parseLong(lstEmbarqueAgendamentoBean.get(i).getEmba_id_cd_shipment()));
				pst.setString(2, lstEmbarqueAgendamentoBean.get(i).getEmba_ds_cod_transportadora());
				pst.setLong(3, Long.parseLong(lstEmbarqueAgendamentoBean.get(i).getEmba_id_cd_codcliente()));
				pst.setString(4, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstEmbarqueAgendamentoBean.get(i).getEmba_dh_leadtime()));
				pst.setString(5, lstEmbarqueAgendamentoBean.get(i).getEmba_ds_planta());
				pst.setString(6, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstEmbarqueAgendamentoBean.get(i).getEmba_dh_shipment()));
				pst.setString(7, lstEmbarqueAgendamentoBean.get(i).getEmba_ds_veiculo());
				pst.setString(8, lstEmbarqueAgendamentoBean.get(i).getEmba_ds_tipoveiculo());
				pst.setString(9, lstEmbarqueAgendamentoBean.get(i).getEmba_in_transportadoraconfirmada());
				pst.setString(10, lstEmbarqueAgendamentoBean.get(i).getEmba_ds_periodo());
				pst.setString(11, lstEmbarqueAgendamentoBean.get(i).getEmba_ds_tipocarga());
				pst.setString(12, lstEmbarqueAgendamentoBean.get(i).getEmba_id_cd_status());
				pst.setString(13, lstEmbarqueAgendamentoBean.get(i).getEmba_ds_tipoagendamento());
				
				pst.execute();
				
				// ATUALIZAR FLAGS DE PROCESSADO
				//updateLgtbs(lstEmbarqueAgendamentoBean.get(i).getEmba_id_cd_shipment());
				
				log.info("[insertEmbarqueAgendamento] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
			
		} else {
			log.info("[insertEmbarqueAgendamento] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[insertEmbarqueAgendamento] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel por realizar o UPDATE das tabelas de logs (LGTBs)
	 * @author Caio Fernandes
	 * @param String
	 * */
	public void updateLgtbs2(List<String> lstEmbarqueAgendamentoUpdateLgtbsBean) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[updateLgtbs] - INICIO ");
		
		if(conn != null) {
			
			for(int i = 0; i < lstEmbarqueAgendamentoUpdateLgtbsBean.size(); i++) {
				
				ResultSet ret = null;
				
				String cSQL = "";
				cSQL += " SELECT DISTINCT ";
				cSQL += " 	EMBA.EMBA_ID_CD_SHIPMENT, ";
				cSQL += " 	SKUD.SKUD_DS_PEDIDOMONDELEZ, ";
				cSQL += " 	SKUD.SKUD_ID_CD_DELIVERY ";
				cSQL += " FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
				cSQL += " 	INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
				cSQL += " 		ON SKUD.SKUD_ID_CD_SHIPMENT = EMBA.EMBA_ID_CD_SHIPMENT ";
				cSQL += " WHERE EMBA_ID_CD_SHIPMENT = ? ";

				
				PreparedStatement pst;	
				pst = conn.prepareStatement(cSQL);
				
				pst.setString(1, lstEmbarqueAgendamentoUpdateLgtbsBean.get(i));

				ret = pst.executeQuery();
				
				int j = 0;
				while(ret.next()) {
					
					String idShipmnet = ret.getString("EMBA_ID_CD_SHIPMENT");
					String idDelivery = ret.getString("SKUD_ID_CD_DELIVERY");
					String pedidoMondelez = ret.getString("SKUD_DS_PEDIDOMONDELEZ");
					
					// ATUALIZAR LGTB SHIP
					if(j == 0) {
						updateShipmentProcessados(idShipmnet);
					}				
					
					// ATUALIZAR LGTB REDN
					updateResumoDnsProcessados(idDelivery);
					
					// ATUALIZAR LGTB SKUD
					updateSkuDeliveryProcessados(pedidoMondelez);
					
					// ATUALIZAR LGTB PEDI
					updatePedidosDeliveryProcessados(pedidoMondelez);
					
					j++;
					
				}
				log.info("[updateLgtbs] - UPDATE REALIZADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[updateLgtbs] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updateLgtbs] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel por realizar o UPDATE das tabelas de logs (LGTBs)
	 * @author Caio Fernandes
	 * @param String
	 * */
	public void updateLgtbs(String idShipment) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		ResultSet ret = null;
		
		log.info("[updateLgtbs] - INICIO ");
		
		if(conn != null) {
			
			String cSQL = "";
			cSQL += " SELECT DISTINCT ";
			cSQL += " 	SHIP.SHIP_CD_SHIPMENTID,  ";
			cSQL += " 	REDN.REDN_DS_DNN, ";
			cSQL += " 	SKUD.SKUD_CD_REFERENCEDOCUMENT, ";
			cSQL += " 	PEDI.PEDI_CD_SALESDOCUMENT ";
			cSQL += " FROM ";
			cSQL += " 	CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK)";
			cSQL += " 		INNER JOIN CS_LGTB_SHIPMENT_SHIP SHIP (NOLOCK)";
			cSQL += " 			ON EMBA.EMBA_ID_CD_SHIPMENT = SHIP.SHIP_CD_SHIPMENTID ";
			cSQL += " 		INNER JOIN CS_LGTB_RESUMODNS_REDN REDN (NOLOCK)";
			cSQL += " 			ON SHIP.SHIP_CD_SHIPMENTID = REDN.REDN_DS_SHIPMENTTOP ";
			cSQL += " 		INNER JOIN CS_LGTB_SKUDELIVERY_SKUD SKUD (NOLOCK)";
			cSQL += " 			ON REDN.REDN_DS_DNN = SKUD.SKUD_CD_DELIVERY ";
			cSQL += " 		INNER JOIN CS_LGTB_PEDIDOSDELIVERY_PEDI PEDI (NOLOCK)";
			cSQL += " 			ON SKUD.SKUD_CD_REFERENCEDOCUMENT = PEDI.PEDI_CD_SALESDOCUMENT ";
			cSQL += " WHERE ";
			cSQL += " 	EMBA.EMBA_ID_CD_SHIPMENT = ? ";
			
			PreparedStatement pst;	
			pst = conn.prepareStatement(cSQL);
			
			pst.setString(1, idShipment);

			ret = pst.executeQuery();
			
			int i = 0;
			while(ret.next()) {
				
				String idShipmnet = ret.getString("SHIP_CD_SHIPMENTID");
				String idDelivery = ret.getString("REDN_DS_DNN");
				String idReferenceDocument = ret.getString("SKUD_CD_REFERENCEDOCUMENT");
				String idSalesdocument = ret.getString("PEDI_CD_SALESDOCUMENT");
				
				// ATUALIZAR LGTB SHIP
				if(i == 0) {
					updateShipmentProcessados(idShipmnet);
				}				
				
				// ATUALIZAR LGTB REDN
				updateResumoDnsProcessados(idDelivery);
				
				// ATUALIZAR LGTB SKUD
				updateSkuDeliveryProcessados(idReferenceDocument);
				
				// ATUALIZAR LGTB PEDI
				updatePedidosDeliveryProcessados(idSalesdocument);
				
				i++;
				
			}
			log.info("[updateLgtbs] - INSERT REALIZADO COM SUCESSO ");
				
			
			
		} else {
			log.info("[updateLgtbs] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updateLgtbs] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel por realizar o delete dos registros da tabela CS_CDTB_SKUDELIVERY_SKUD
	 * @author Caio Fernandes
	 * @param List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryRecriarBean
	 * */
	public void excluirSkudDelivery(List<CsCdtbSkudeliverySkudBean> lstSkuDeliveryRecriarBean) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[excluirSkudDelivery] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstSkuDeliveryRecriarBean.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[excluirSkudDelivery] - REALIZANDO DELETE ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" DELETE "
						+ "		CS_CDTB_SKUDELIVERY_SKUD "
						+ "WHERE "
						+ "		SKUD_ID_CD_SHIPMENT = ?	"
						+ "		AND SKUD_ID_CD_DELIVERY = ? "
						+ "");
				
				pst.setLong(1, Long.parseLong(lstSkuDeliveryRecriarBean.get(i).getSkud_id_cd_shipment()));
				pst.setLong(2, Long.parseLong(lstSkuDeliveryRecriarBean.get(i).getSkud_id_cd_delivery()));
				
				pst.execute();
				
				log.info("[excluirSkudDelivery] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
			
		} else {
			log.info("[excluirSkudDelivery] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[excluirSkudDelivery] - FIM ");
		
	}

	/**
	 * Metodo responsavel por executar as regras do campo EMBA_IN_TRANSPORTADORACONFIRMADA 
	 * @author Caio Fernandes
	 * @param String
	 * @return String tipoAgendamentoRetorno
	 * */
	private String executarRegraStatusOferta(String codTransportadora, String statusOferta) {
		
		// REGRA CAMPO TRANSPORTADORA CONFIRMADA 
		// Status Oferta = Ofertado e contém SCAC = CONFIRMADO
		// Status Oferta = Nao Ofertado e não contém SCAC = NAO CONFIRMADO
		
		String inTransConfirmada = "";
		
		if(statusOferta.equalsIgnoreCase("Ofertado") && !codTransportadora.equalsIgnoreCase("")) {
			// CONFIRMADO
			inTransConfirmada = "CONFIRMADO";
		} else {
			// NAO CONFIRMADO
			inTransConfirmada = "NÃO CONFIRMADO";
		}
		
		return inTransConfirmada;
	}
	
	/**
	 * Metodo responsavel por verificar se ja existe EMBA cadastrada para as chaves idShipment passadas via parametro
	 * @author Caio Fernandes
	 * @param String, String
	 * @return boolean
	 * */
	public boolean existeEmbarque(String idShipment) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		ResultSet ret = null;
		boolean retorno = false;
		
		// REALIZAR O INSERT
		log.info("[existeDelivery] - REALIZANDO CONSULTA ");
		
		if(conn != null) {
			
			PreparedStatement pst;	
			pst = conn.prepareStatement(""
					+" SELECT "
					+ " 	EMBA_ID_CD_SHIPMENT "
					+ " FROM "
					+ "		CS_NGTB_EMBARQUEAGENDAMENTO_EMBA (NOLOCK)"
					+ " WHERE "
					+ "		EMBA_ID_CD_SHIPMENT = ? ");
			
			pst.setString(1, idShipment);
			
			ret = pst.executeQuery();
			
			if(ret.next()) {
				retorno = true;
			}
			
		}
		
		return retorno;
		
	}
	
	/**
	 * Metodo responsavel por verificar se ja existe SKUD cadastrada para as chaves idShipment e idDelivery passadas via parametro
	 * @author Caio Fernandes
	 * @param String, String
	 * @return boolean
	 * */
	public boolean existeDelivery(String idShipment, String idDelivery) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		ResultSet ret = null;
		boolean retorno = false;
		
		// REALIZAR O SELECT
		log.info("[existeDelivery] - REALIZANDO CONSULTA ");
		
		if(conn != null) {
			
			PreparedStatement pst;	
			pst = conn.prepareStatement(""
					+" SELECT "
					+ " 	SKUD_ID_CD_SHIPMENT "
					+ " FROM "
					+ "		CS_CDTB_SKUDELIVERY_SKUD (NOLOCK)"
					+ " WHERE "
					+ "		SKUD_ID_CD_SHIPMENT = ? "
					+ "		AND SKUD_ID_CD_DELIVERY = ? ");
			
			pst.setString(1, idShipment);
			pst.setString(2, idDelivery);
			
			ret = pst.executeQuery();
			
			if(ret.next()) {
				retorno = true;
			}
			
		}
		
		return retorno;
		
	}
	
	/**
	 * Metodo responsavel por executar as regras do campo EMBA_ID_CD_STATUS 
	 * @author Caio Fernandes
	 * @param String
	 * @return String clienteAgendado
	 * */
	public String executarRegraStatus(String clienteAgendado, String codCliente, String shipmentId) throws SQLException {
		
		// REGRA DO CAMPO STATUS:
		// CASO FOR "NÃO AGENDADO" STATUS = 1
		// CASO FOR "AGENDADO" E NAO NECESSITAR DE NF STATUS = 2
		// CASO O CLIENTE SEJA UM CLIENTE AGENDADO, VERIFICA SE ELE NECESSITA DE NF,
		// CASO NECESSITE DE NF PARA AGENDAR SETAR O STATUS DE AGENDAMENTO PARA AGUARDANDO NF STATUS = 3
		String status;
		
		
		
		if(clienteAgendado.equalsIgnoreCase("Agendado")) {
			String nfObrigatoria = buscarNecessidadeNfCliente(codCliente);
			boolean nfExistente = buscarNf(shipmentId);
			if(nfObrigatoria.toUpperCase().trim().equals("SIM") && !nfExistente) {
				status = "3";
			}else {
				status = "2";
			}
			
		} else {
			status = "1";
		}
		
		return status;
		
	}
	
	public boolean buscarNf(String shipmentId) throws SQLException {
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		ResultSet ret = null;
		
		// REALIZAR O INSERT
		log.info("[buscarNf] - REALIZANDO CONSULTA ");
		
		String cSQL = "";
		cSQL += " SELECT TOP 1  ";
		cSQL += " 	NOFI_NR_NOTAFISCAL  ";
		cSQL += " FROM  ";
		cSQL += " 	CS_CDTB_NOTAFISCAL_NOFI NOFI (NOLOCK) ";
		cSQL += " 		INNER JOIN CS_CDTB_SKUDELIVERY_SKUD SKUD (NOLOCK) ";
		cSQL += " 			ON NOFI.NOFI_NR_NUMPEDIDO = SKUD.SKUD_DS_PEDIDOMONDELEZ ";
		cSQL += " WHERE  ";
		cSQL += " 	SKUD.SKUD_ID_CD_SHIPMENT = ? ";
		
		PreparedStatement pst;	
		pst = conn.prepareStatement(cSQL);
		pst.setString(1, shipmentId);
		ret = pst.executeQuery();
		boolean retorno = false;
		if(ret.next()) {
			retorno = true;
		}
		return retorno;
	}
	
	public String buscarNecessidadeNfCliente(String codCliente) throws SQLException {
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		ResultSet ret = null;
		
		// REALIZAR O INSERT
		log.info("[buscarNecessidadeNfCliente] - REALIZANDO CONSULTA ");
		
		String cSQL = "SELECT TOP 1 CLIE_DS_NFOBRIGATORIA FROM CS_CDTB_CLIENTE_CLIE (NOLOCK) WHERE CLIE_ID_CD_CODIGOCLIENTE = ?";
		PreparedStatement pst;	
		pst = conn.prepareStatement(cSQL);
		pst.setString(1, codCliente);
		ret = pst.executeQuery();
		String retorno = "";
		if(ret.next()) {
			retorno = ret.getString("CLIE_DS_NFOBRIGATORIA");
		}
		return retorno;
	}
	
	/**
	 * Metodo responsavel por executar as regras do campo EMBA_DS_TIPOAGENDAMENTO
	 * @author Caio Fernandes
	 * @param String
	 * @return String tipoAgendamentoRetorno
	 * */
	public String calcularTipoAgendamento(String codCliente) throws SQLException {
		
		// SE NA CS_CDTB_CLIENTE_CLIE O CAMPO CLIE_DS_TIPOAGENDAMENTO = 'sem agenda' - MDLZ_LOC_APPT_NOT_REQUIRED
		// SE NA CS_CDTB_CLIENTE_CLIE O CAMPO CLIE_DS_TIPOAGENDAMENTO = 'Agendamento em trânsito' - MDLZ_LOC_APPT_PARALLEL_TRANSIT
		// SE NA CS_CDTB_CLIENTE_CLIE O CAMPO CLIE_DS_TIPOAGENDAMENTO = 'Ofertado após confirmação agenda' - MDLZ_LOC_APPT_REQUIRED
		// SE NA CS_CDTB_CLIENTE_CLIE O CAMPO CLIE_DS_TIPOAGENDAMENTO = 'Agendamento somente com NF' - MDLZ_LOC_APPT_WITH_NF
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		ResultSet ret = null;
		String tipoAgendamento = "";
		String tipoAgendamentoRetorno = "";
		
		// REALIZAR O INSERT
		log.info("[calcularTipoAgendamento] - REALIZANDO CONSULTA ");
		
		if(conn != null) {
			
			PreparedStatement pst;	
			pst = conn.prepareStatement(""
					+" SELECT "
					+ " 	CLIE_DS_TIPOAGENDAMENTO "
					+ " FROM "
					+ "		CS_CDTB_CLIENTE_CLIE (NOLOCK) "
					+ " WHERE "
					+ "		CLIE_ID_CD_CODIGOCLIENTE = ? ");
			
			pst.setString(1, codCliente);
			
			ret = pst.executeQuery();
			
			if(ret.next()) {
				
				tipoAgendamento = ret.getString("CLIE_DS_TIPOAGENDAMENTO");
				
				if(tipoAgendamento.equalsIgnoreCase("sem agenda")) {
					tipoAgendamentoRetorno = "MDLZ_LOC_APPT_NOT_REQUIRED";
				} else if(tipoAgendamento.equalsIgnoreCase("Agendamento em trânsito")) {
					tipoAgendamentoRetorno = "MDLZ_LOC_APPT_PARALLEL_TRANSIT";
				} else if(tipoAgendamento.equalsIgnoreCase("Ofertado após confirmação agenda")) {
					tipoAgendamentoRetorno = "MDLZ_LOC_APPT_REQUIRED";
				} else if(tipoAgendamento.equalsIgnoreCase("Agendamento somente com NF")) {
					tipoAgendamentoRetorno = "MDLZ_LOC_APPT_WITH_NF";
				}
				
			}
			
		}
		
		return tipoAgendamentoRetorno;
		
	}
	
	
	/**
	 * Metodo responsavel por realizar a regra do campo periodo
	 * @author Caio Fernandes
	 * @param Date
	 * @return String 
	 * */
	public String calcularPeriodo() {
		
		// IDENTIFICAR O MES DA DATA PASSADA E RETORNAR P1, P2, P3...
		String retorno = "";
		Date dt = new Date();
		GregorianCalendar dataCal = new GregorianCalendar();
		dataCal.setTime(dt);
		int mes = dataCal.get(Calendar.MONTH);
		
		if(mes == 0) {
			retorno = "P1"; // JAN
		} else if (mes == 1) {
			retorno = "P2"; // FEV
		} else if (mes == 2) {
			retorno = "P3"; // MAR
		} else if (mes == 3) {
			retorno = "P4"; // ABR
		} else if (mes == 4) {
			retorno = "P5"; // MAI
		} else if (mes == 5) {
			retorno = "P6"; // JUN
		} else if (mes == 6) {
			retorno = "P7"; // JUL
		} else if (mes == 7) {
			retorno = "P8"; // AGO
		} else if (mes == 8) {
			retorno = "P9"; // SET
		} else if (mes == 9) {
			retorno = "P10"; // OUT
		} else if (mes == 10) {
			retorno = "P11"; // NOV
		} else if (mes == 11) {
			retorno = "P12"; // DEZ
		}
		
		return retorno;
		
	}
}

