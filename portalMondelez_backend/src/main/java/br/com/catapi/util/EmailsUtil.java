package br.com.catapi.util;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.portalpluris.bean.CsCdtbUsuarioUserBean;
import br.com.portalpluris.bean.CsNgtbEmailsEmaiBean;
import br.com.portalpluris.bean.DetalhesEmailCompletoBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.csCdtbContatosBean;
import br.com.portalpluris.business.CsCdtbContatosContBusiness;
import br.com.portalpluris.dao.CsCdtbContatosContDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConf;
import br.com.portalpluris.dao.CsNgtbEmailsEmaiDao;
import br.com.portalpluris.dao.csCdtbUsuarioUserDao;


 
public class EmailsUtil{

	public EmailsUtil(){
		System.setProperty("line.separator", "\r\n");
	}

	/**
	 * @author Rogério Nunes
	 * @return RetornoBean
	 * @throws SQLException 
	 * @throws MessagingException 
	 * @throws JsonProcessingException 
	 */
	public static RetornoBean sendMail(int embaIdCdEmbarque, int userIdCdUsuario, String destinatario, byte[] attach, DetalhesEmailCompletoBean detalhesArquivoEmail) throws SQLException, MessagingException, JsonProcessingException {
	   
		RetornoBean retBean = new RetornoBean();
		
		Properties props = new Properties();
	    
	    props.put("mail.smtp.host", CsDmtbConfiguracaoConf.getConfiguracao("mail.smtp.host@1"));
	    props.put("mail.smtp.auth", CsDmtbConfiguracaoConf.getConfiguracao("mail.smtp.auth@1"));
	    props.put("mail.smtp.ssl.enable", CsDmtbConfiguracaoConf.getConfiguracao("mail.smtp.ssl.enable@1"));
	    props.put("mail.smtp.starttls.enable", CsDmtbConfiguracaoConf.getConfiguracao("mail.smtp.starttls.enable@1"));
	    props.put("mail.smtp.socketFactory.class", CsDmtbConfiguracaoConf.getConfiguracao("mail.smtp.socketFactory.class@1"));
	    props.put("mail.smtp.port" , CsDmtbConfiguracaoConf.getConfiguracao("mail.smtp.port@1"));
	    
	    Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
	    	protected PasswordAuthentication getPasswordAuthentication(){
            	return new PasswordAuthentication(CsDmtbConfiguracaoConf.getConfiguracao("mail.user@1"), CsDmtbConfiguracaoConf.getConfiguracao("mail.pass@1"));
	        }
	    });
	 
	    session.setDebug(false);
	 
	    CsNgtbEmailsEmaiDao emaiDao = new CsNgtbEmailsEmaiDao();
	    List<CsNgtbEmailsEmaiBean> emailsBeanList = emaiDao.getEmails(embaIdCdEmbarque, destinatario, userIdCdUsuario);
	    CsNgtbEmailsEmaiBean emailsBean = new CsNgtbEmailsEmaiBean();
	    
	    //TODO TESTE
	    //emailsBeanList = emaiDao.teste();
	    	    
	    Address[] toUser;
	    String emailsPara ="";
	    for (CsNgtbEmailsEmaiBean emaiBean : emailsBeanList) {
	    	emailsPara += emaiBean.getEmaiDsDestinatario()+",";
	    	
	    }
	    String idEmbarque = Integer.toString(embaIdCdEmbarque);
	    
	    //ALTERAÇÃO RAFA 03/08/2021
	    //INCLUSAO DE E-MAILS DOS FOCAIS QUANDO O DESTINATARIO FOR A TRANSPORTADORA
	    if(destinatario.equalsIgnoreCase("emailTransportadora")) {
	    	CsCdtbContatosContDao contatosDao = new CsCdtbContatosContDao();
	    	
	    	csCdtbContatosBean contatos = contatosDao.getContatosEmails("FOCAL", idEmbarque);
	    	emailsPara += contatos.getContatos() + ",";
	    }
	    //emailsPara += "danillocosta@plurismidia.com.br,rafaelsilva@plurismidia.com.br,";
	    emailsPara = emailsPara.substring(0, emailsPara.length() -1);
	    
	    //emailsPara = "suportecrm@plurismidia.com.br, rafaelsilva@plurismidia.com.br, caioaraujo@plurismidia.com.br";
	    emailsBean.setEmaiDsDestinatario(emailsPara);
	    
	    toUser = InternetAddress.parse(emailsPara);
	    	
    	Message message = new MimeMessage(session);
    	
    	//message.setFrom(new InternetAddress(CsDmtbConfiguracaoConf.getConfiguracao("mail.from@1")));
    	
    	csCdtbUsuarioUserDao userDao = new csCdtbUsuarioUserDao();
    	CsCdtbUsuarioUserBean beanUser = userDao.findUserByUser(userIdCdUsuario);
    	message.setFrom(new InternetAddress(beanUser.getUser_ds_email()));
    	
    	
    	//Address[] toUser = InternetAddress.parse(emaiBean.getEmaiDsDestinatario());  
	 
	    message.setRecipients(Message.RecipientType.TO, toUser);

	    String subject;
	    
	    if(embaIdCdEmbarque > 0 && detalhesArquivoEmail.getListDetalhesDoDelivery().size() > 0) {
	    
		    if(destinatario.equalsIgnoreCase("emailCliente")){
		    	subject = "Solicitação de Agendamento Mondelez - [" + embaIdCdEmbarque + "] - "
		    			+ "Cliente: "+ detalhesArquivoEmail.getDetalhesShip().getEmba_id_cd_codcliente() +" - "
		    			+ detalhesArquivoEmail.getDetalhesShip().getClie_ds_nomecliente()
		    			+ "Cidade: "+detalhesArquivoEmail.getDetalhesShip().getClie_ds_cidade()+"/"+detalhesArquivoEmail.getDetalhesShip().getClie_ds_uf()+" - "
		    			+ "Grupo: "+detalhesArquivoEmail.getDetalhesShip().getCncl_ds_chanel()+" - "
		    			+ "Data solicitação: "+detalhesArquivoEmail.getDetalhesShip().getDaag_dh_agendamento();
		    	
		    }else if(destinatario.equalsIgnoreCase("emailTransportadora")){
		    	subject = "Formalização de agendamento Mondelez - [" + embaIdCdEmbarque + "] - "
		    			+ "Transportadora: "+detalhesArquivoEmail.getListDetalhesDoDelivery().get(0).getTran_ds_codigotransportadora()+" - "+detalhesArquivoEmail.getListDetalhesDoDelivery().get(0).getTran_nm_nometransportadora()+ " - "
		    			+ "Cliente: "+detalhesArquivoEmail.getDetalhesShip().getEmba_id_cd_codcliente()+" - " +detalhesArquivoEmail.getListDetalhesDoDelivery().get(0).getClie_ds_nomecliente();
		    	
		    }else{
		    	subject = "Informações sobre o embarque "+embaIdCdEmbarque;
		    	
		    }
		    
	    }else{
	    	emailsBean.setEmaiInEnviado("N");
	    	emailsBean.setEmaiDsErro("INFORMAÇÕES PARA ENVIO NÃO LOCALIZADAS");
	    	return retBean;
	    }
	    
	    
	    
	    message.setSubject(subject);

	    String content = "Ficha anexa";
	    //message.setContent(content, "text/html; charset=utf-8");
	    
	    MimeBodyPart mbText = new MimeBodyPart();
	    //mbText.setText(content);
	    mbText.setContent(detalhesArquivoEmail.getLayoutEmail(),"text/html; charset=UTF-8");
	    
	    Multipart mp = new MimeMultipart();
	    mp.addBodyPart(mbText);
	   
	//ENVIAR ANEXO XLS PARA O CLIENTE
	    if(destinatario.equalsIgnoreCase("emailCliente") && attach != null){
	    	mp = AnexarArquivoXLS(embaIdCdEmbarque, attach, mp);
	    }
	    
	    message.setContent(mp);
	    
	    emailsBean.setEmaiDsAssunto(subject);
	    //emaiBean.setEmaiTxConteudo(content);
	    emailsBean.setEmaiTxConteudo(detalhesArquivoEmail.getLayoutEmail());
	    emailsBean.setEmbaIdCdEmbarque(embaIdCdEmbarque);
	    emailsBean.setUserIdCdUsuario(userIdCdUsuario);
	    emailsBean.setEmaiInEnviado("S");
	    
	    try {
		    	
	    	Transport.send(message);
	    	emaiDao.gravaLogEnvio(emailsBean);
		    	
	     } catch (MessagingException e) {
	    	 e.printStackTrace();
	    	 
	    	 emailsBean.setEmaiInEnviado("N");
	    	 emailsBean.setEmaiDsErro(e.getMessage());		    	 
	    	 emaiDao.gravaLogEnvio(emailsBean);
	     }
		//}
	    
	    retBean.setMsg("Solicitação enviados com sucesso");
    	retBean.setSucesso(true);
	    
	    System.gc();
	    return retBean;
	}

	private static Multipart AnexarArquivoXLS(int embaIdCdEmbarque, byte[] attach, Multipart mp) throws MessagingException {
		
		MimeBodyPart mbFile = new MimeBodyPart();
		ByteArrayDataSource bds = new ByteArrayDataSource(attach, "application/pdf");
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String dataFormatada = formatter.format(date);
		
		bds.setName("Embarque["+embaIdCdEmbarque+"] "+dataFormatada+".xlsx");
		
		mbFile.setDataHandler(new DataHandler(bds)); 
		mbFile.setFileName(bds.getName()); 
		mp.addBodyPart(mbFile);
		
		return mp;
	}
}