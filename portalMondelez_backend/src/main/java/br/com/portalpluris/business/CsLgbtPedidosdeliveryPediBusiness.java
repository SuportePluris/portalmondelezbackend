package br.com.portalpluris.business;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgbtPedidosdeliveryPediBean;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsLgbtPedidosdeliveryPediDao;

public class CsLgbtPedidosdeliveryPediBusiness {

	/**
	 * Metodo responsavel por realizar leitura e gravacao das informacoes disponibilizadas via planilha no  banco de dados
	 * @author Caio Fernandes
	 * */
	public void gravarPedidosDelivery() {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("[gravarPedidosDelivery] - INICIO ");
		
		try {
			
			List<CsLgbtPedidosdeliveryPediBean> lstPedidoDeliveryBean = new ArrayList<CsLgbtPedidosdeliveryPediBean>();
			CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
			UtilMoveArquivo utilMove = new UtilMoveArquivo();
			
			//String diretorioPedidosDelivery = "C:\\Users\\caraujo\\Desktop\\MONDELEZ_IMP\\PEDIDOS_DELIVERY\\";
//			String diretorioPedidosDelivery = confDao.slctConf("conf.importacao.agendamento.pedidosdelivery");
//			String diretorioPedidosDeliveryBkp = confDao.slctConf("conf.importacao.agendamento.pedidosdelivery.bkp");
//			String diretorioPedidosDeliveryErro = confDao.slctConf("conf.importacao.agendamento.pedidosdelivery.erro");
			
			String diretorioPedidosDelivery = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\PEDIDO";
			String diretorioPedidosDeliveryBkp = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\PEDIDO\\bkp";
			String diretorioPedidosDeliveryErro = "C:\\Users\\rnunes\\Desktop\\Mondelez\\05-08\\PEDIDO\\erro";
			
			// LEITURA DOS ARQUIVOS DO DIRETORIO
			File arquivos = new File(diretorioPedidosDelivery);
			String[] arq = arquivos.list();
			
			if (arq != null) {
				
				for (int i = 0; i < arq.length; i++) {
					
					// ARQUIVO A SER PROCESSADO
					//File arquivoProcessado = new File(diretorioPedidosDelivery + "/" +  arq[i]); // UTILIZAR QUANDO FOR LINUX
					File arquivoProcessado = new File(diretorioPedidosDelivery + "\\" +  arq[i]); // UTILIZAR QUANDO FOR WINDOWS
					try {
						
						if(arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {
							
							String nmArquivo = arquivoProcessado.getName();
							
							// INSTANCIAR ARQUIVO
							FileInputStream file = new FileInputStream(arquivoProcessado);
							
							// INSTANCIAR PLANILHA
							Workbook wb = WorkbookFactory.create(file);								
							
							// FOR DAS ABAS DO ARQUIVO
							for(int j = 0; j < wb.getNumberOfSheets(); j++) {
								
								// CONFIRMAR NOME DA ABA
								if(wb.getSheetName(j).equalsIgnoreCase("Sheet1")) {
									
									// SETAR ABA DA PLANILHA
									Sheet planilha = wb.getSheetAt(j);
									
									// OBTER VALORES DA PLANILHA E PREENCHER A LISTA 
									lstPedidoDeliveryBean = setListaPedidoDelivery(lstPedidoDeliveryBean, planilha);
									
									// REALIZAR O INSERT DA LISTA NO BANCO
									CsLgbtPedidosdeliveryPediDao pedidosdeliveryPediDao = new CsLgbtPedidosdeliveryPediDao();
									pedidosdeliveryPediDao.insertPedidosDelivery(lstPedidoDeliveryBean, nmArquivo);
								}
								
							}
							
							// MOVER ARQUIVO PARA PASTA BKP
							File arquivoProcessadoBkp = new File(diretorioPedidosDeliveryBkp + "/" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoBkp);
							
						}
						
					} catch (Exception e) {
						log.info("[gravarPedidosDelivery] - ERRO AO LER AQUIVO PEDIDOS DELIVERY: " + e.getMessage());
						
						// MOVER ARQUIVO PARA PASTA DE ERRO
						if(arq != null) {
							File arquivoProcessadoErro = new File(diretorioPedidosDeliveryErro + "/" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoErro);
						}
					}
						
				}
				
			} else {
				log.info("[gravarPedidosDelivery] - NAO FOI POSSIVEL OBTER OS ARQUIVOS DO DIRETORIO ");
			}
			
		} catch (Exception e){
			log.info("[gravarPedidosDelivery] - ERRO AO GRAVAR PEDIDO DELIVERY: " + e.getMessage());
		}
		
	}

	/**
	 * Metodo responsavel por obter os valores da planilha e retornar a lista de CsLgbtPedidosdeliveryPediBean preenchida.
	 * @author Caio Fernandes
	 * @param List<CsLgbtPedidosdeliveryPediBean>, Sheet
	 * @return List<CsLgbtPedidosdeliveryPediBean>
	 * @throws ParseException 
	 * */
	private static List<CsLgbtPedidosdeliveryPediBean> setListaPedidoDelivery(List<CsLgbtPedidosdeliveryPediBean> lstPedidoDeliveryBean, Sheet planilha) throws ParseException {
		
		UtilDate util = new UtilDate();
		
		// FOR DAS LINHAS DO ARQUIVO
		for (int i = 1; i < planilha.getLastRowNum()+1 ; i++){
			
			// POPULAR BEAN
			CsLgbtPedidosdeliveryPediBean pedidoDeliveryBean = new CsLgbtPedidosdeliveryPediBean();
			
			if(planilha.getRow(i).getCell(0) != null){
				// PEDI_CD_SALESDOCUMENT
				pedidoDeliveryBean.setPedi_cd_salesdocument(planilha.getRow(i).getCell(0).toString());
			}
			
			if(planilha.getRow(i).getCell(1) != null){
				// PEDI_DS_NETVALUE
				pedidoDeliveryBean.setPedi_ds_netvalue(planilha.getRow(i).getCell(1).toString());
			}
			
			if(planilha.getRow(i).getCell(2) != null){
				// PEDI_DS_PURCHASEORDERNUMBER
				pedidoDeliveryBean.setPedi_ds_purchaseordernumber(planilha.getRow(i).getCell(2).toString());
			}
			
			if(planilha.getRow(i).getCell(3) != null){
				// PEDI_DS_CREATEDON
				pedidoDeliveryBean.setPedi_dh_createdon(util.convertStringToDate(planilha.getRow(i).getCell(3).toString()));
			}
			
			if(planilha.getRow(i).getCell(4) != null){
				// PEDI_DS_CREATEDBY
				pedidoDeliveryBean.setPedi_ds_createdby(planilha.getRow(i).getCell(4).toString());
			}
			
			if(planilha.getRow(i).getCell(5) != null){
				// PEDI_DS_SOLDTOPARTY
				pedidoDeliveryBean.setPedi_ds_soldtoparty(planilha.getRow(i).getCell(5).toString());
			}
			
			if(planilha.getRow(i).getCell(6) != null){
				// PEDI_DS_SALESDOCUMENTTYPE
				pedidoDeliveryBean.setPedi_ds_salesdocumenttype(planilha.getRow(i).getCell(6).toString());	
			}
			
			if(planilha.getRow(i).getCell(7) != null){
				// PEDI_DH_DOCUMENTDATE
				pedidoDeliveryBean.setPedi_dh_documentdate(util.convertStringToDate(planilha.getRow(i).getCell(7).toString()));
			}
			
			if(planilha.getRow(i).getCell(8) != null){
				// PEDI_DS_DISTRIBUTIONCHANNEL
				pedidoDeliveryBean.setPedi_ds_distributionchannel(planilha.getRow(i).getCell(8).toString());
			}
			
			if(planilha.getRow(i).getCell(9) != null){
				// PEDI_DS_DOCUMENTCURRENCY
				pedidoDeliveryBean.setPedi_ds_documentcurrency(planilha.getRow(i).getCell(9).toString());
			}
			
			if(planilha.getRow(i).getCell(10) != null){
				// PEDI_DS_SALESORGANIZATION
				pedidoDeliveryBean.setPedi_ds_salesorganization(planilha.getRow(i).getCell(10).toString());
			}

			// ADICIONAR BEAN NA LISTA
			lstPedidoDeliveryBean.add(pedidoDeliveryBean);
			
		}
		
		return lstPedidoDeliveryBean;
	}
	
}
