package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.CsCdtbTipoTipoBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsCdtbTipoTipoDao {

	public RetornoBean createUpdateTipo(CsCdtbTipoTipoBean tipoBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		if (tipoBean.getTipo_id_cd_tipo() > 0) {
			retBean = updateTipo(tipoBean);

			return retBean;
		}

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		//if (tipoBean.getTipo_in_inativo().equalsIgnoreCase("S")) {
			pst = conn.prepareStatement("" 
					+ " INSERT INTO CS_CDTB_TIPO_TIPO ( " 
					+ "		TIPO_DS_TIPO, "
					+ "		TIPO_IN_INATIVO " + " ) VALUES ( ?, ? ) ");

			pst.setString(1, tipoBean.getTipo_ds_tipo());
			pst.setString(2, tipoBean.getTipo_in_inativo());
/*
		} else {
			pst = conn.prepareStatement("" 
					+ " 	INSERT INTO CS_CDTB_TIPO_TIPO ( " 
					+ " 	TIPO_DS_TIPO " + " ) VALUES ( ? ) ");

			pst.setString(1, tipoBean.getTipo_ds_tipo());
		}
*/
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Tipo criado com sucesso");

		return retBean;
	}

	private RetornoBean updateTipo(CsCdtbTipoTipoBean tipoBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		String queryUpdate = " UPDATE CS_CDTB_TIPO_TIPO SET ";

		if (tipoBean.getTipo_ds_tipo() != null && tipoBean.getTipo_ds_tipo() != "") {
			queryUpdate += " TIPO_DS_TIPO = '" + tipoBean.getTipo_ds_tipo() + "'";
		}

		if (tipoBean.getTipo_in_inativo() != null && tipoBean.getTipo_in_inativo() != "") {
			queryUpdate += ", TIPO_IN_INATIVO = '" + tipoBean.getTipo_in_inativo() + "'";
		}

		queryUpdate += " WHERE TIPO_ID_CD_TIPO = " + tipoBean.getTipo_id_cd_tipo();

		pst = conn.prepareStatement(queryUpdate);
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Tipo atualizado com sucesso");

		return retBean;
	}

	public List<CsCdtbTipoTipoBean> getListTipo(String tipoInAtivo) throws SQLException {

		List<CsCdtbTipoTipoBean> listTipoBean = new ArrayList<CsCdtbTipoTipoBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " 
		+ "		TIPO_ID_CD_TIPO, " 
				+ "		TIPO_DS_TIPO, " + "		TIPO_DH_REGISTRO, "
				+ "		TIPO_IN_INATIVO " + " FROM " + "		CS_CDTB_TIPO_TIPO (NOLOCK) " + "";

		// COMENTADO PARA TRAZER TODOS OS TIPOS E NAO SOMENTE OS INATIVOS
		if (tipoInAtivo.equalsIgnoreCase("S") || tipoInAtivo.equalsIgnoreCase("N")) {
			query += " WHERE TIPO_IN_INATIVO = '" + tipoInAtivo + "'";
		}

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbTipoTipoBean tipoBean = setTipoBean(rs);

			listTipoBean.add(tipoBean);
		}

		return listTipoBean;
	}

	private CsCdtbTipoTipoBean setTipoBean(ResultSet rs) throws SQLException {

		CsCdtbTipoTipoBean tipoBean = new CsCdtbTipoTipoBean();

		tipoBean.setTipo_id_cd_tipo(rs.getInt("TIPO_ID_CD_TIPO"));
		tipoBean.setTipo_ds_tipo(rs.getString("TIPO_DS_TIPO"));
		tipoBean.setTipo_in_inativo(rs.getString("TIPO_IN_INATIVO"));
		tipoBean.setTipo_dh_registro(rs.getString("TIPO_DH_REGISTRO"));

		return tipoBean;
	}

}
