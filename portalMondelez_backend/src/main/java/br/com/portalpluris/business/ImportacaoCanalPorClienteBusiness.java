package br.com.portalpluris.business;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilParseToInt;
import br.com.portalpluris.bean.CanalPorClienteBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.dao.CsCdtbCanalPorClienteDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.DAOLog;

public class ImportacaoCanalPorClienteBusiness {
	
	
	public void importadorCanalCliente(){		
		
		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("*** INICIO DO SERVIÇO DE IMPORTAÇÃO DE CANAL CLIENTE ******");

		// DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoCanalPorClienteBusiness");
		logsBean.setLogsDsMetodo("importadorCanalCliente");
		logsBean.setLogsInErro("N");
		
		UtilMoveArquivo utilMove = new UtilMoveArquivo();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		UtilParseToInt parseInt = new UtilParseToInt();
		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		CanalPorClienteBean canalCli = new CanalPorClienteBean();
		CsCdtbCanalPorClienteDao canalDao = new CsCdtbCanalPorClienteDao();
		
		String diretorio = confDao.slctConf("conf.importacao.canal.cliente");
		String diretorioBKP = confDao.slctConf("conf.importacao.canal.cliente.bkp");	
		String diretorioErro = confDao.slctConf("conf.importacao.canal.cliente.erro");
		
		File arquivoProcessado = null;
		File arquivoProcessadobkp = null;
		File arquivoProcessadoerro = new File(diretorioErro+ "/");

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dhImportacao = sdf.format(date);
		
		int linha = 0;
		
		try {			

			File arquivo = new File(diretorio);
			String[] arq = arquivo.list();

			if(arq != null){				
				
				for (int i = 0; i < arq.length; i++) {

					 arquivoProcessado = new File(diretorio + "/" + arq[i]);
					 arquivoProcessadobkp = new File(diretorioBKP + "/" + arq[i]);				

					String nmArquivo = arq[i] + "_" + dhImportacao;
					
					if (arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

						log.info("*** INICIO DA LEITURA DO ARQUIVO " + arquivoProcessado.getName() + "******");
						FileInputStream file = new FileInputStream(arquivoProcessado);

						Workbook wb = WorkbookFactory.create(file);
						Sheet sheet = wb.getSheetAt(0);

							for (int j = 10; j < sheet.getLastRowNum(); j++) {
								
								linha =j;
								
								canalCli = new CanalPorClienteBean();
								
								canalCli = setCanal(sheet,j,dhImportacao,nmArquivo,idUser);
								
								boolean ifExists = canalDao.slctCanal(canalCli.getCncl_cd_custumer());
								
								if(!ifExists){
									
									canalDao.insert(canalCli);
									
								}else{
									canalDao.update(canalCli);
									
								}								
							}
						
						
						boolean movido = utilMove.moveArquivo(arquivoProcessado,arquivoProcessadobkp);
						
						if(movido){
							log.info("Arquivo movido com sucesso");
						}else{
							log.info("Não foi possivel mover o arquivo");
						}
					}
				}			
			}
			
		} catch (Exception e) {			
				log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO CANAL CLIENTE" + e + " ******");		
				logsBean.setLogsInErro("S");
				logsBean.setLogsTxErro(e.getMessage());
				log.info("*** LINHA:" + linha + "************");
				logsBean.setLogsTxErro( "LINHA DE ERRO:" +linha +e.getMessage());
				utilMove.moveArquivo(arquivoProcessado,arquivoProcessadoerro);

		} finally {
				daoLog.createLog(logsBean);
				log.info("**** FIM SERVIÇO DE IMPORTAÇÃO DE ARQUIVO CANAL CLIENTE ****");
		}	
	}

	private CanalPorClienteBean setCanalOld(Sheet sheet, int j,
			String dhImportacao, String nmArquivo, String idUser) {
		
		CanalPorClienteBean canalCli = new CanalPorClienteBean();

		if(sheet.getRow(j).getCell(1) != null){
		Cell CUSTUMER = sheet.getRow(j).getCell(1);
		CUSTUMER.setCellType(CellType.STRING);
		canalCli.setCncl_cd_custumer(CUSTUMER.toString());
		}
		if(sheet.getRow(j).getCell(2) != null){
		canalCli.setCncl_ds_chanel(sheet.getRow(j).getCell(2).toString());
		}
		if(sheet.getRow(j).getCell(3) != null){
		Cell cdHolding = sheet.getRow(j).getCell(3);
		cdHolding.setCellType(CellType.STRING);
		canalCli.setCncl_cd_holding(cdHolding.toString());
		}
		if(sheet.getRow(j).getCell(4) != null){
		canalCli.setCncl_nm_holding(sheet.getRow(j).getCell(4).toString());
		}
		if(sheet.getRow(j).getCell(5) != null){
		canalCli.setCncl_ds_country(sheet.getRow(j).getCell(5).toString());
		}
		if(sheet.getRow(j).getCell(6) != null){
		canalCli.setCncl_nm_razaosocial(sheet.getRow(j).getCell(6).toString());
		}
		if(sheet.getRow(j).getCell(7) != null){
		canalCli.setCncl_ds_fantasia(sheet.getRow(j).getCell(7).toString());
		}
		if(sheet.getRow(j).getCell(8) != null){
		canalCli.setCncl_ds_cidade(sheet.getRow(j).getCell(8).toString());
		}
		if(sheet.getRow(j).getCell(9) != null){
		canalCli.setCncl_ds_cep(sheet.getRow(j).getCell(9).toString());
		}
		if(sheet.getRow(j).getCell(10) != null){
		canalCli.setCncl_ds_estado(sheet.getRow(j).getCell(10).toString());
		}
		if(sheet.getRow(j).getCell(11) != null){
		canalCli.setCncl_ds_rua(sheet.getRow(j).getCell(11).toString());
		}
		if(sheet.getRow(j).getCell(12) != null){
		canalCli.setCncl_ds_telefone(sheet.getRow(j).getCell(12).toString());
		}
		if(sheet.getRow(j).getCell(13) != null){
		Cell industria = sheet.getRow(j).getCell(13);
		industria.setCellType(CellType.STRING);
		canalCli.setCncl_ds_industria(industria.toString());
		}
		if(sheet.getRow(j).getCell(14) != null){
		canalCli.setCncl_cd_nielsen(sheet.getRow(j).getCell(14).toString());
		}
		if(sheet.getRow(j).getCell(15) != null){
		canalCli.setCncl_ds_district(sheet.getRow(j).getCell(15).toString());
		}
		if(sheet.getRow(j).getCell(16) != null){
		canalCli.setCncl_ds_cnpj(sheet.getRow(j).getCell(16).toString());
		}
		if(sheet.getRow(j).getCell(17) != null){
		canalCli.setCncl_ds_transportzone(sheet.getRow(j).getCell(17).toString());
		}
		if(sheet.getRow(j).getCell(18) != null){
		Cell inscricao = sheet.getRow(j).getCell(18);
		inscricao.setCellType(CellType.STRING);
		canalCli.setCncl_ds_inscricaoestadual(inscricao.toString());
		}
		if(sheet.getRow(j).getCell(19) != null){
		canalCli.setCncl_ds_subtrib(sheet.getRow(j).getCell(19).toString());
		}
		if(sheet.getRow(j).getCell(20) != null){
		canalCli.setCncl_ds_taxajurd(sheet.getRow(j).getCell(20).toString());
		}
		if(sheet.getRow(j).getCell(21) != null){
		Cell HIERAR = sheet.getRow(j).getCell(21);
		HIERAR.setCellType(CellType.STRING);
		canalCli.setCncl_ds_hiearchyassignment(HIERAR.toString());
		}
		if(sheet.getRow(j).getCell(22) != null){
		Cell pai = sheet.getRow(j).getCell(22);
		pai.setCellType(CellType.STRING);
		canalCli.setCncl_cd_codigopai(pai.toString());
		}
		if(sheet.getRow(j).getCell(23) != null){
		canalCli.setCncl_ds_condpagamento(sheet.getRow(j).getCell(23).toString());
		}
		if(sheet.getRow(j).getCell(24) != null){
			
			canalCli.setCncl_ds_aceitasaldo(sheet.getRow(j).getCell(24).toString());
		}
		
		if(sheet.getRow(j).getCell(25) != null){
	
			canalCli.setCncl_ds_rebate(sheet.getRow(j).getCell(25).toString());
		}
		if(sheet.getRow(j).getCell(26) != null){
		Cell distrinct = sheet.getRow(j).getCell(26);
		distrinct.setCellType(CellType.STRING);
		canalCli.setCncl_cd_salesdistrict(distrinct.toString());
		}
		
		if(sheet.getRow(j).getCell(27) != null){
		
			canalCli.setCncl_ds_ordercombinat(sheet.getRow(j).getCell(27).toString());
		}
		if(sheet.getRow(j).getCell(28) != null){
		Cell deliverypr = sheet.getRow(j).getCell(28);
		deliverypr.setCellType(CellType.STRING);
		canalCli.setCncl_cd_deliveryprior(deliverypr.toString());
		}
		if(sheet.getRow(j).getCell(29) != null){
		canalCli.setCncl_ds_paytterms(sheet.getRow(j).getCell(29).toString());
		}
		if(sheet.getRow(j).getCell(30) != null){
		canalCli.setCncl_ds_pricedetermination(sheet.getRow(j).getCell(30).toString());
		}
		if(sheet.getRow(j).getCell(31) != null){
		canalCli.setCncl_ds_typeclass(sheet.getRow(j).getCell(31).toString());
		}
		canalCli.setCncl_id_usuarioatualizacao(idUser);
		canalCli.setCncl_dh_importacao(dhImportacao);
		canalCli.setCncl_ds_nomearquivo(nmArquivo);
		canalCli.setCncl_ds_inativo("N");				
		
		return canalCli;
	}
	
	private CanalPorClienteBean setCanal(Sheet sheet, int j,
			String dhImportacao, String nmArquivo, String idUser) {
		
		CanalPorClienteBean canalCli = new CanalPorClienteBean();

		if(sheet.getRow(j).getCell(1) != null){
		Cell CUSTUMER = sheet.getRow(j).getCell(1);
		CUSTUMER.setCellType(CellType.STRING);
		canalCli.setCncl_cd_custumer(CUSTUMER.toString());
		}
		if(sheet.getRow(j).getCell(2) != null){
		canalCli.setCncl_nm_razaosocial(sheet.getRow(j).getCell(2).toString());
		}
		if(sheet.getRow(j).getCell(3) != null){
			Cell celCNPJ = sheet.getRow(j).getCell(3);
			celCNPJ.setCellType(CellType.STRING);		
			canalCli.setCncl_ds_cnpj(celCNPJ.toString());
		}
		
		if(sheet.getRow(j).getCell(4) != null){
			canalCli.setCncl_ds_cidade(sheet.getRow(j).getCell(4).toString());
		}
		if(sheet.getRow(j).getCell(5) != null){
			canalCli.setCncl_ds_estado(sheet.getRow(j).getCell(5).toString());
		}
		if(sheet.getRow(j).getCell(6) != null){
			Cell cdHolding = sheet.getRow(j).getCell(6);
			cdHolding.setCellType(CellType.STRING);
			canalCli.setCncl_cd_holding(cdHolding.toString());
		}
		if(sheet.getRow(j).getCell(7) != null){
			canalCli.setCncl_ds_level1node(sheet.getRow(j).getCell(7).toString());
		}
		if(sheet.getRow(j).getCell(8) != null){
			canalCli.setCncl_ds_grupocanal(sheet.getRow(j).getCell(8).toString());
		}
		if(sheet.getRow(j).getCell(9) != null){
			canalCli.setCncl_ds_level2node(sheet.getRow(j).getCell(9).toString());
		}
		if(sheet.getRow(j).getCell(19) != null){
			Cell cdChanel = sheet.getRow(j).getCell(19);
			cdChanel.setCellType(CellType.STRING);
			canalCli.setCncl_ds_chanel(cdChanel.toString());
		}
		if(sheet.getRow(j).getCell(11) != null){
			canalCli.setCncl_ds_level3node(sheet.getRow(j).getCell(11).toString());
		}
		if(sheet.getRow(j).getCell(12) != null){
			canalCli.setCncl_ds_segmento(sheet.getRow(j).getCell(12).toString());
		}
		if(sheet.getRow(j).getCell(13) != null){
			canalCli.setCncl_ds_level4node(sheet.getRow(j).getCell(13).toString());
		}
		if(sheet.getRow(j).getCell(14) != null){
			canalCli.setCncl_ds_rede(sheet.getRow(j).getCell(14).toString());
		}
		if(sheet.getRow(j).getCell(15) != null){
			canalCli.setCncl_ds_level5node(sheet.getRow(j).getCell(15).toString());
		}
		if(sheet.getRow(j).getCell(16) != null){
			canalCli.setCncl_ds_regiaovendas(sheet.getRow(j).getCell(16).toString());
		}
		
		if(sheet.getRow(j).getCell(17) != null){
			canalCli.setCncl_ds_level6node(sheet.getRow(j).getCell(17).toString());
		}
		if(sheet.getRow(j).getCell(18) != null){
			canalCli.setCncl_ds_areavenda(sheet.getRow(j).getCell(18).toString());
		}
	
		if(sheet.getRow(j).getCell(20) != null){
			canalCli.setCncl_ds_regiaocanal(sheet.getRow(j).getCell(20).toString());
		}
		canalCli.setCncl_id_usuarioatualizacao(idUser);
		canalCli.setCncl_dh_importacao(dhImportacao);
		canalCli.setCncl_ds_nomearquivo(nmArquivo);
		canalCli.setCncl_ds_inativo("N");				
		
		return canalCli;
	}
}
