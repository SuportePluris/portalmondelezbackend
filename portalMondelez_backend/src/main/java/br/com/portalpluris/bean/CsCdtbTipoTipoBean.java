package br.com.portalpluris.bean;

public class CsCdtbTipoTipoBean {

	int tipo_id_cd_tipo;
	private String tipo_ds_tipo;
	private String tipo_in_inativo;
	private String tipo_dh_registro;

	public int getTipo_id_cd_tipo() {
		return tipo_id_cd_tipo;
	}

	public void setTipo_id_cd_tipo(int tipo_id_cd_tipo) {
		this.tipo_id_cd_tipo = tipo_id_cd_tipo;
	}

	public String getTipo_ds_tipo() {
		return tipo_ds_tipo;
	}

	public void setTipo_ds_tipo(String tipo_ds_tipo) {
		this.tipo_ds_tipo = tipo_ds_tipo;
	}

	public String getTipo_in_inativo() {
		return tipo_in_inativo;
	}

	public void setTipo_in_inativo(String tipo_in_inativo) {
		this.tipo_in_inativo = tipo_in_inativo;
	}

	public String getTipo_dh_registro() {
		return tipo_dh_registro;
	}

	public void setTipo_dh_registro(String tipo_dh_registro) {
		this.tipo_dh_registro = tipo_dh_registro;
	}

}
