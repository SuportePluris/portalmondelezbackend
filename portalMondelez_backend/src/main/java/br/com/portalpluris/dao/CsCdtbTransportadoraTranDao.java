package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbTransportadoraEmail;
import br.com.portalpluris.bean.CsCdtbTransportadoraTranBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsCdtbTransportadoraTranDao {

	public RetornoBean createUpdateTran(CsCdtbTransportadoraTranBean tranBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		if (tranBean.getTran_id_cd_transportadora() > 0) {
			retBean = updateTran(tranBean);

			return retBean;
		}

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		pst = conn.prepareStatement("" + " INSERT INTO CS_CDTB_TRANSPORTADORA_TRAN ( "
				+ " 		TRAN_NM_NOMETRANSPORTADORA, " 
				+ "			TRAN_DS_CODIGOTRANSPORTADORA, "
				// + " TRAN_DS_TIPOCARGATRANSPORTADORA, "
				+ " 		TRAN_DS_CNPJ, "
				+ "			TRAN_ID_CD_USUARIOATUALIZACAO" + " ) "
				+ "	VALUES (?,?,?,?)");

		pst.setString(1, tranBean.getTran_nm_nometransportadora());
		pst.setString(2, tranBean.getTran_ds_codigotransportadora());
		pst.setString(3, tranBean.getTran_ds_cnpj());
		
		// pst.setString(3, tranBean.getTranDsTipocargatransportadora());
		pst.setLong(4, tranBean.getTran_id_cd_usuarioatualizacao());
		

		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Transportadora criada com sucesso");

		return retBean;
	}

	private RetornoBean updateTran(CsCdtbTransportadoraTranBean tranBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		String queryUpdate = " UPDATE CS_CDTB_TRANSPORTADORA_TRAN SET " + " TRAN_ID_CD_USUARIOATUALIZACAO = "
				+ tranBean.getTran_id_cd_usuarioatualizacao() + ", " + " TRAN_DH_ATUALIZACAO = GETDATE(), TRAN_DS_CNPJ = " + tranBean.getTran_ds_cnpj();

		if (tranBean.getTran_nm_nometransportadora() != null && tranBean.getTran_nm_nometransportadora() != "") {
			queryUpdate += ", TRAN_NM_NOMETRANSPORTADORA = '" + tranBean.getTran_nm_nometransportadora() + "'";
		}

		if (tranBean.getTran_ds_codigotransportadora() != null && tranBean.getTran_ds_codigotransportadora() != "") {
			queryUpdate += ", TRAN_DS_CODIGOTRANSPORTADORA = '" + tranBean.getTran_ds_codigotransportadora() + "'";
		}

		/*
		 * if (tranBean.getTranDsTipocargatransportadora() != null &&
		 * tranBean.getTranDsTipocargatransportadora() != "") { queryUpdate +=
		 * ", TRAN_DS_TIPOCARGATRANSPORTADORA = '" +
		 * tranBean.getTranDsTipocargatransportadora() + "'"; }
		 */

		if (tranBean.getTran_in_inativo() != null && !tranBean.getTran_in_inativo().equals("")) {
			queryUpdate += ", TRAN_IN_INATIVO = '" + tranBean.getTran_in_inativo() + "'";
		}

		queryUpdate += " WHERE TRAN_ID_CD_TRANSPORTADORA = " + tranBean.getTran_id_cd_transportadora();

		pst = conn.prepareStatement(queryUpdate);
		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Transportadora atualizada com sucesso");

		return retBean;
	}

	public List<CsCdtbTransportadoraTranBean> getListTran(String statInAtivo) throws SQLException {

		List<CsCdtbTransportadoraTranBean> listTranBean = new ArrayList<CsCdtbTransportadoraTranBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " + "		TRAN_ID_CD_TRANSPORTADORA, " 
				+ "		TRAN_NM_NOMETRANSPORTADORA, "
				+ "		TRAN_DS_CODIGOTRANSPORTADORA, "
				+ " 	FORMAT (TRAN_DH_REGISTRO, 'dd/MM/yyyy ') as TRAN_DH_REGISTRO,  "
				// + " TRAN_DS_TIPOCARGATRANSPORTADORA, "
				+ " 	CASE WHEN TRAN_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS TRAN_IN_INATIVO, "
				+ "		TRAN_ID_CD_USUARIOATUALIZACAO, "
				+ "		FORMAT (TRAN_DH_ATUALIZACAO, 'dd/MM/yyyy ') as TRAN_DH_ATUALIZACAO,"
				+ "		TRAN_DS_CNPJ  " 
				+ " FROM "
				+ "		CS_CDTB_TRANSPORTADORA_TRAN (NOLOCK) ";

		if (statInAtivo.equalsIgnoreCase("S") || statInAtivo.equalsIgnoreCase("N")) {
			query += " WHERE TRAN_IN_INATIVO = '" + statInAtivo + "'";
		}

		query += " ORDER BY TRAN_NM_NOMETRANSPORTADORA ";

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbTransportadoraTranBean tranBean = setTranBean(rs);

			listTranBean.add(tranBean);
		}

		return listTranBean;
	}

	
	
	public List<CsCdtbTransportadoraEmail> getListContatosTran(String codTransportadora) throws SQLException {

		List<CsCdtbTransportadoraEmail> listContatosTranBean = new ArrayList<CsCdtbTransportadoraEmail>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " 
				+ "		TRML_ID_TRANSPORTADORAEMAIL, " 
				+ "		TRML_DS_CODIGOTRANSPORTADORA, "
				+ "		TRML_DS_RAZAOSCOAIL, "
				+ " 	TRML_ID_USUARIOATUALIZACAO,  "
				+ " 	CASE WHEN TRML_DS_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS TRML_DS_INATIVO, "
				+ "		TRML_DS_CNPJ, "
				+ "		FORMAT (TRML_DH_IMPORTACAO, 'dd/MM/yyyy ') as TRML_DH_IMPORTACAO,"
				+ "		TRML_DS_EMAIL,"
				+ "		TRML_NM_ARQUIVOPROCESSADO  " 
				+ " FROM "
				+ "		CS_CDTB_TRANSPORTADORAEMAIL_TRML (NOLOCK) ";
		
		if (!codTransportadora.equalsIgnoreCase("")) {
			query += " WHERE TRML_DS_CODIGOTRANSPORTADORA = '" + codTransportadora + "'";
		}
		codTransportadora = "10A5";
		query += " ORDER BY TRML_DS_EMAIL ";

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsCdtbTransportadoraEmail tranBean = setContatosTranBean(rs);

			listContatosTranBean.add(tranBean);
		}

		return listContatosTranBean;
	}
	
	
	
	public CsCdtbTransportadoraTranBean getTran(String tranIdCdTransportadora) throws SQLException {

		CsCdtbTransportadoraTranBean tranBean = new CsCdtbTransportadoraTranBean();
		tranBean.setTran_nm_nometransportadora("null");

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " 
				+ "		TRAN_ID_CD_TRANSPORTADORA, " 
				+ "		TRAN_NM_NOMETRANSPORTADORA, "
				+ "		TRAN_DS_CODIGOTRANSPORTADORA, " 
				+ "		TRAN_DH_REGISTRO, " 
				+ "		TRAN_IN_INATIVO, "
				+ "		TRAN_ID_CD_USUARIOATUALIZACAO, " 
				+ "		TRAN_DH_ATUALIZACAO,"
				+ "		TRAN_DS_CNPJ " 
				+ " FROM "
				+ "		CS_CDTB_TRANSPORTADORA_TRAN (NOLOCK) " 
				+ " WHERE " 
				+ "		TRAN_DS_CODIGOTRANSPORTADORA = ?";

		pst = conn.prepareStatement(query);
		pst.setString(1, tranIdCdTransportadora);

		rs = pst.executeQuery();

		while (rs.next()) {

			tranBean = setTranBean(rs);
		}

		return tranBean;
	}

	private CsCdtbTransportadoraTranBean setTranBean(ResultSet rs) throws SQLException {

		CsCdtbTransportadoraTranBean tranBean = new CsCdtbTransportadoraTranBean();

		tranBean.setTran_id_cd_transportadora(rs.getInt("TRAN_ID_CD_TRANSPORTADORA"));
		tranBean.setTran_nm_nometransportadora(rs.getString("TRAN_NM_NOMETRANSPORTADORA"));
		tranBean.setTran_ds_codigotransportadora(rs.getString("TRAN_DS_CODIGOTRANSPORTADORA"));
		tranBean.setTran_dh_registro(rs.getString("TRAN_DH_REGISTRO"));
		// tranBean.setTranDsTipocargatransportadora(rs.getString("TRAN_DS_TIPOCARGATRANSPORTADORA"));
		tranBean.setTran_in_inativo(rs.getString("TRAN_IN_INATIVO"));
		tranBean.setTran_id_cd_usuarioatualizacao(rs.getInt("TRAN_ID_CD_USUARIOATUALIZACAO"));
		tranBean.setTran_dh_atualizacao(rs.getString("TRAN_DH_ATUALIZACAO"));
		tranBean.setTran_ds_cnpj(rs.getString("TRAN_DS_CNPJ"));
		

		return tranBean;
	}
	
	
	private CsCdtbTransportadoraEmail setContatosTranBean(ResultSet rs) throws SQLException {

		CsCdtbTransportadoraEmail tranBean = new CsCdtbTransportadoraEmail();

		tranBean.setTrml_id_transportadoraemail(rs.getString("TRML_ID_TRANSPORTADORAEMAIL"));
		tranBean.setTrml_ds_codigotransportadora(rs.getString("TRML_DS_CODIGOTRANSPORTADORA"));
		tranBean.setTrml_ds_razaoscoail(rs.getString("TRML_DS_RAZAOSCOAIL"));
		tranBean.setTrml_ds_cnpj(rs.getString("TRML_DS_CNPJ"));
		tranBean.setTrml_ds_email(rs.getString("TRML_DS_EMAIL"));
		tranBean.setTrml_ds_inativo(rs.getString("TRML_DS_INATIVO"));
		tranBean.setTrml_id_usuarioatualizacao(rs.getString("TRML_ID_USUARIOATUALIZACAO"));
		tranBean.setTrml_nm_arquivoprocessado(rs.getString("TRML_NM_ARQUIVOPROCESSADO"));
		
		return tranBean;
	}

	public boolean slcTran(String tranDsCodigotransportadora) throws SQLException {

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " + "		" + "TRAN_ID_CD_TRANSPORTADORA, "
				+ "		TRAN_NM_NOMETRANSPORTADORA, " + "		TRAN_DS_CODIGOTRANSPORTADORA, "
				+ "		TRAN_DH_REGISTRO, " + "		TRAN_IN_INATIVO, " + "		TRAN_ID_CD_USUARIOATUALIZACAO, "
				+ "		TRAN_DH_ATUALIZACAO " + " FROM " + "		CS_CDTB_TRANSPORTADORA_TRAN (NOLOCK) " + " WHERE "
				+ "		TRAN_DS_CODIGOTRANSPORTADORA = ?";

		pst = conn.prepareStatement(query);
		pst.setString(1, tranDsCodigotransportadora);

		rs = pst.executeQuery();

		while (rs.next()) {

			System.out.println("Já existe uma transportadora");

			return false;

		}

		return true;
	}

	public void updateTrans(CsCdtbTransportadoraTranBean transBean, String dhImportacao, String nmArquivo) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraTranDao");
		logsBean.setLogsDsMetodo("updateTrans");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Insert na base TRANSPORTADORA *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst;

			pst = con.prepareStatement("" + "UPDATE CS_CDTB_TRANSPORTADORA_TRAN SET"
					+ "  TRAN_NM_NOMETRANSPORTADORA = ? ,"
					+ "  TRAN_DH_ATUALIZACAO = ?, "
					+ "	TRAN_NM_ARQUIVOPROCESSADO = ?, "
					+ " TRAN_ID_CUSTOMER = ?,"
					+ " TRAN_DS_CNPJ =?, "
					+ " TRAN_DS_COCKPIT = ?, "
					+ " TRAN_DS_STATUS = ?, "
					+ " TRAN_DS_CONTRATO = ?,"
					+ " TRAN_DS_REMITTO = ?,"
					+ " TRAN_DS_CONTATO = ?,"
					+ " TRAN_DS_EMAIL = ?,"
					+ " TRAN_DS_TELELEFONE = ?"
					+ " WHERE TRAN_DS_CODIGOTRANSPORTADORA = ?");

			pst.setString(1, transBean.getTran_nm_nometransportadora());
			pst.setString(2, dhImportacao);
			pst.setString(3, nmArquivo);
			pst.setString(4, transBean.getTran_id_customer());
			pst.setString(5, transBean.getTran_ds_cnpj());
			pst.setString(6, transBean.getTran_ds_cockpit());
			pst.setString(7, transBean.getTran_ds_status());
			pst.setString(8, transBean.getTran_ds_contrato());
			pst.setString(9, transBean.getTran_ds_remitto());		
			pst.setString(10, transBean.getTran_ds_contato());	
			pst.setString(11, transBean.getTran_ds_email());	
			pst.setString(12, transBean.getTran_ds_telefone());	
			pst.setString(13, transBean.getTran_ds_codigotransportadora());

			pst.executeUpdate();

		} catch (Exception e) {

			log.info("***** Erro no UPDATE na base do TRANSPORTADORA " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  UPDATE na base do TRANSPORTADORA  *****");
			daoLog.createLog(logsBean);

		}

	}

	public void insertTrans(CsCdtbTransportadoraTranBean transBean, String dhImportacao, String nmArquivo) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraTranDao");
		logsBean.setLogsDsMetodo("insertTrans");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Insert na base TRANSPORTADORA *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst = con.prepareStatement("");

			pst = con.prepareStatement("" + " 	INSERT INTO CS_CDTB_TRANSPORTADORA_TRAN ("
					+ "  TRAN_NM_NOMETRANSPORTADORA ,"
					+ "  TRAN_DS_CODIGOTRANSPORTADORA ,"
					+ "  TRAN_DH_REGISTRO  ,"
					+ "  TRAN_IN_INATIVO ,"
					+ "  TRAN_NM_ARQUIVOPROCESSADO, "
					+ "  TRAN_ID_CD_USUARIOATUALIZACAO,"
					+ " TRAN_DS_CNPJ, "
					+ " TRAN_DS_COCKPIT, "
					+ " TRAN_DS_STATUS, "
					+ " TRAN_DS_CONTRATO, ,"
					+ " TRAN_DS_REMITTO, "
					+ " TRAN_DS_CONTATO ,"
					+ " TRAN_DS_EMAIL ,"
					+ " TRAN_DS_TELELEFONE"
					+ ") VALUES ( ?, ?, ?, ?, ?, ?,? ,?, ?, ?, ?,? ,? ,?)");

			pst.setString(1, transBean.getTran_nm_nometransportadora());
			pst.setString(2, transBean.getTran_ds_codigotransportadora());
			pst.setString(3, dhImportacao);
			pst.setString(4, "N");
			pst.setString(5, nmArquivo);
			pst.setString(6, idUser);
			pst.setString(7,  transBean.getTran_ds_cnpj());
			pst.setString(8,  transBean.getTran_ds_cockpit());
			pst.setString(9,  transBean.getTran_ds_status());
			pst.setString(10,  transBean.getTran_ds_contrato());
			pst.setString(11,  transBean.getTran_ds_remitto());
			pst.setString(12,  transBean.getTran_ds_contato());
			pst.setString(13,  transBean.getTran_ds_email());
			pst.setString(14,  transBean.getTran_ds_telefone());


			pst.execute();

		} catch (Exception e) {

			log.info("***** Erro no INSERT na base do TRANSPORTADORA " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  INSERT na base do TRANSPORTADORA  *****");
			daoLog.createLog(logsBean);

		}

	}

}
