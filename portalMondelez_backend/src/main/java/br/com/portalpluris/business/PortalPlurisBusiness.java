package br.com.portalpluris.business;


import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.ArquivoLeadTimeBean;
import br.com.portalpluris.bean.BaseClienteConsolidadaBean;
import br.com.portalpluris.bean.CsAstbEmbarqueFoupBean;
import br.com.portalpluris.bean.CsAstbUsuariocanalUscaBean;
import br.com.portalpluris.bean.CsAstbUsuariocsadasdanalUscaBean;
import br.com.portalpluris.bean.CsCdtbCanalCanaBean;
import br.com.portalpluris.bean.CsCdtbContatoclienteCoclBean;
import br.com.portalpluris.bean.CsCdtbFollowupFoupBean;
import br.com.portalpluris.bean.CsCdtbTransportadoraTranBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.HierarquiaClienteBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.CsCdtbUsuarioUserBean;
import br.com.portalpluris.dao.CsAstbEmbarqueFoupDao;
import br.com.portalpluris.dao.CsCdtbClienteClieDao;
import br.com.portalpluris.dao.CsCdtbHierarquiaClienteDao;
import br.com.portalpluris.dao.CsCdtbLeadTimeLdtmDao;
import br.com.portalpluris.dao.CsCdtbTransportadoraTranDao;
import br.com.portalpluris.dao.DAOLog;
import br.com.portalpluris.dao.csCdtbUsuarioUserDao;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PortalPlurisBusiness {

	csCdtbUsuarioUserDao funcDao = new csCdtbUsuarioUserDao();
	
	
	public RetornoBean validaUsuario(CsCdtbUsuarioUserBean usuarioBean){
		
		RetornoBean retbean = new RetornoBean();
		retbean.setProcesso("validacao");
		
		String txt = "";
		
		if(usuarioBean.getUser_ds_nomeusuario() == null || usuarioBean.getUser_ds_nomeusuario().equalsIgnoreCase("")){txt += "Nome\n";}
		if(usuarioBean.getUser_ds_login() == null || usuarioBean.getUser_ds_login().equalsIgnoreCase("")){txt += "Login\n";}
		if(usuarioBean.getUser_ds_senha() == null || usuarioBean.getUser_ds_senha().equalsIgnoreCase("")){txt += "Senha\n";}
		//if(usuarioBean.getUser_ds_email() == null || usuarioBean.getUser_ds_email().equalsIgnoreCase("")){txt += "Email\n";}
		//if(usuarioBean.getUser_ds_pa() == null || usuarioBean.getUser_ds_pa().equalsIgnoreCase("")){txt += "Pa\n";}
		if(usuarioBean.getUser_ds_permissionamento() == null || usuarioBean.getUser_ds_permissionamento().equalsIgnoreCase("")){txt += "Permissão\n";}
		if(usuarioBean.getUser_in_inativo() == null || usuarioBean.getUser_in_inativo().equalsIgnoreCase("")){txt += "Inativo";}
		
		if(txt.equalsIgnoreCase("")){
			retbean.setMsg("ok");
			retbean.setSucesso(true);
		}else{
			retbean.setMsg(txt);
			retbean.setSucesso(false);
		}
		
		return retbean;
		
	}
	
	
	public RetornoBean validaUsuarioCanal(CsAstbUsuariocanalUscaBean bean){
		
		RetornoBean retbean = new RetornoBean();
		retbean.setProcesso("validacao");
		
		String txt = "";
		
		if(bean.getCsCdtbUsuarioUserBean().getUser_id_cd_usuario() == null || bean.getCsCdtbUsuarioUserBean().getUser_id_cd_usuario().equalsIgnoreCase("")){txt += "Usuario\n";}
		//if(bean.getCsCdtbCanalCanaBean().getCana_id_cd_canal() == null || bean.getCsCdtbCanalCanaBean().getCana_id_cd_canal().equalsIgnoreCase("")){txt += "Canal\n";}
		
		if(txt.equalsIgnoreCase("")){
			retbean.setMsg("ok");
			retbean.setSucesso(true);
		}else{
			retbean.setMsg(txt);
			retbean.setSucesso(false);
		}
		
		return retbean;
		
	}
	
	
	public RetornoBean validaCanal(CsCdtbCanalCanaBean bean){
		
		RetornoBean retbean = new RetornoBean();
		retbean.setProcesso("validacao");
		
		String txt = "";
		
//		if(bean.getCana_ds_canal() == null || bean.getCana_ds_canal().equalsIgnoreCase("")){txt += "Canal\n";}
		
		if(txt.equalsIgnoreCase("")){
			retbean.setMsg("ok");
			retbean.setSucesso(true);
		}else{
			retbean.setMsg(txt);
			retbean.setSucesso(false);
		}
		
		return retbean;
		
	}




	public RetornoBean validaEmbarqueFoup(CsAstbEmbarqueFoupBean bean){
	
	RetornoBean retbean = new RetornoBean();
	retbean.setProcesso("validacao");
	
	String txt = "";
	
	if(bean.getEmfo_id_cd_embarque() == null ||  bean.getEmfo_id_cd_embarque().equalsIgnoreCase("0")){txt += "Embarque\n";};
	if(bean.getEmfo_id_cd_followup() == null ||  bean.getEmfo_id_cd_followup().equalsIgnoreCase("0")){txt += "FollowUp\n";};
	//if(bean.getEmfo_id_cd_motivo() == null ||  bean.getEmfo_id_cd_motivo().equalsIgnoreCase("0")){txt += "Motivo\n";};
	//if(bean.getEmfo_id_cd_responsavel() == null || bean.getEmfo_id_cd_responsavel().equalsIgnoreCase("0")){txt += "Responsavel\n";};
	//if(bean.getEmfo_tx_followup() == null || bean.getEmfo_tx_followup().equalsIgnoreCase("")){txt += "Observação\n";};
	
	if(txt.equalsIgnoreCase("")){
		retbean.setMsg("ok");
		retbean.setSucesso(true);
	}else{
		retbean.setMsg(txt);
		retbean.setSucesso(false);
	}
	
	return retbean;
	
}



	public RetornoBean validaCliente(BaseClienteConsolidadaBean clienteBbean){
		
		RetornoBean retbean = new RetornoBean();
		retbean.setProcesso("validacao");
		
		String txt = "";
		
		if(clienteBbean.getClie_id_cd_codigocliente() == null || clienteBbean.getClie_id_cd_codigocliente().equalsIgnoreCase("")){txt += "Código\n";}
		if(clienteBbean.getClie_ds_nomecliente() == null || clienteBbean.getClie_ds_nomecliente().equalsIgnoreCase("")){txt += "Nome\n";}
		if(clienteBbean.getClie_ds_cnpj() == null || clienteBbean.getClie_ds_cnpj().equalsIgnoreCase("")){txt += "Cnpj\n";}
		
		if(txt.equalsIgnoreCase("")){
			retbean.setMsg("ok");
			retbean.setSucesso(true);
		}else{
			retbean.setMsg(txt);
		}
		
		return retbean;
		
	}
	
	
public RetornoBean validaCadastroFollow(CsCdtbFollowupFoupBean foupBean){
		
		RetornoBean retbean = new RetornoBean();
		retbean.setProcesso("validacao");
		
		String txt = "";
		
		if(foupBean.getFoup_ds_followup() == null || foupBean.getFoup_ds_followup().equalsIgnoreCase("")){txt += "FOLLOWUP\n";}
		if(foupBean.getFoup_in_inativo() == null || foupBean.getFoup_in_inativo().equalsIgnoreCase("")){txt += "INATIVO\n";}
		
		if(txt.equalsIgnoreCase("")){
			retbean.setMsg("ok");
			retbean.setSucesso(true);
		}else{
			retbean.setMsg(txt);
			retbean.setSucesso(false);
		}
		
		return retbean;
		
	}

	public RetornoBean validaCadastroCliente(CsCdtbContatoclienteCoclBean coceBean){
		
		RetornoBean retbean = new RetornoBean();
		retbean.setProcesso("validacao");
		
		String txt = "";
		
		if(coceBean.getCocl_ds_emailcontatocliente() == null || coceBean.getCocl_ds_emailcontatocliente().equalsIgnoreCase("")){txt += "EMAIL\n";}
		
		if(txt.equalsIgnoreCase("")){
			retbean.setMsg("ok");
			retbean.setSucesso(true);
		}else{
			retbean.setMsg(txt);
			retbean.setSucesso(false);
		}
		
		return retbean;
		
	}
	
	
	
	public RetornoBean validaCadastroTransportadora(CsCdtbTransportadoraTranBean tranBean){
		
		RetornoBean retbean = new RetornoBean();
		retbean.setProcesso("validacao");
		
		String txt = "";
		
		if(tranBean.getTran_ds_codigotransportadora() == null ||tranBean.getTran_ds_codigotransportadora().equalsIgnoreCase("")){txt += "Código transportadora\n";}
		if(tranBean.getTran_nm_nometransportadora() == null ||tranBean.getTran_nm_nometransportadora().equalsIgnoreCase("")){txt += "Nome transportadora\n";}
		//if(tranBean.getTran_ds_cnpj() == null ||tranBean.getTran_ds_cnpj().equalsIgnoreCase("")){txt += "Cnpj\n";}
		if(tranBean.getTran_in_inativo() == null ||tranBean.getTran_in_inativo().equalsIgnoreCase("")){txt += "INATIVO\n";}
		
		if(txt.equalsIgnoreCase("")){
			retbean.setMsg("ok");
			retbean.setSucesso(true);
		}else{
			retbean.setMsg(txt);
			retbean.setSucesso(false);
		}
		
		return retbean;
		
	}
	
	/**
	 * Metodo responsavel por obter informacoes do login 
	 * @author Danillo COsta
	 * @param LoginBean loginBean
	 * @return String jsonRetorno
	 * */
	public String getLogin(CsCdtbUsuarioUserBean loginBean)  {

		String jsonRetorno = "";

		csCdtbUsuarioUserDao func = new csCdtbUsuarioUserDao();

		loginBean = func.getLogin(loginBean);

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(loginBean);

		} catch (Exception e) {
			System.out.println("[getLogin] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}
	
	
	/**
	 * Metodo responsavel por obter informacoes do login 
	 * @author Danillo Costa
	 * @param LoginBean loginBean
	 * @return String jsonRetorno
	 * */
	public String getAllUsers()  {

		String jsonRetorno = "";

		csCdtbUsuarioUserDao func = new csCdtbUsuarioUserDao();

		List<CsCdtbUsuarioUserBean> loginBean = func.getAllUser();

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(loginBean);

		} catch (Exception e) {
			System.out.println("[getLogin] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}
	
	
	
	/**
	 * Metodo responsavel por obter informacoes do login 
	 * @author Danillo Costa
	 * @param LoginBean loginBean
	 * @return String jsonRetorno
	 * */
	public String createUser( CsCdtbUsuarioUserBean loginBean)  {

		String jsonRetorno = "";

		csCdtbUsuarioUserDao func = new csCdtbUsuarioUserDao();

		RetornoBean ret = func.getCreateUser(loginBean);

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(ret);

		} catch (Exception e) {
			System.out.println("[getLogin] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}
	
	
	public String updateUser( CsCdtbUsuarioUserBean loginBean)  {

		String jsonRetorno = "";

		csCdtbUsuarioUserDao func = new csCdtbUsuarioUserDao();

		RetornoBean ret = func.getUpdateUser(loginBean);

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(ret);

		} catch (Exception e) {
			System.out.println("[createEmbarqueFoup] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}



	public String searchByCriteria(String criteria, String id) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());		
		log.info("*** INICIO DO SERVIÇO DE BUSCA POR CRITERIA ******");
		
		//DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("PortalPlurisBusiness");
		logsBean.setLogsDsMetodo("searchByCriteria");
		logsBean.setLogsInErro("N");	

		String jsonRetorno = "";

		try {
			
		
		if(criteria.equalsIgnoreCase("transportadora")){			
					
			CsCdtbTransportadoraTranDao tranDao = new CsCdtbTransportadoraTranDao();
			CsCdtbTransportadoraTranBean tranBean = new CsCdtbTransportadoraTranBean();			
		
			tranBean = tranDao.getTran(id);
			
			if (!tranBean.getTran_nm_nometransportadora().equalsIgnoreCase("null")){
				
				log.info("*** Transportadora Encontrada ******");
				ObjectMapper mapper = new ObjectMapper();
				jsonRetorno = mapper.writeValueAsString(tranBean);
			
				return jsonRetorno;	
			
			}else{
				
				jsonRetorno = "Transportadora Não encontrada";
				
				log.info("*** Transportadora não Encontrada ******");
			}
			
		}else if(criteria.equalsIgnoreCase("leadtime")){
			
			ArquivoLeadTimeBean leadTime = new ArquivoLeadTimeBean();
			CsCdtbLeadTimeLdtmDao leadDao = new CsCdtbLeadTimeLdtmDao();
			
			leadTime = leadDao.slcLeadTime(id);
			
			if(!leadTime.getPlantaCidade().equalsIgnoreCase("null")){
				
				log.info("*** Lead Time Encontrado ******");
				ObjectMapper mapper = new ObjectMapper();
				jsonRetorno = mapper.writeValueAsString(leadTime);
				
				return jsonRetorno;
				
			}else{
				log.info("*** Lead Time Não Encontrado ******");
				jsonRetorno = "Lead Time Não Encontrado";
				return jsonRetorno;
			}
			
			
		}else if(criteria.equalsIgnoreCase("cliente")){
			
			BaseClienteConsolidadaBean cliBean = new BaseClienteConsolidadaBean ();
			CsCdtbClienteClieDao cliDao = new CsCdtbClienteClieDao();
			
			cliBean = cliDao.slcIdCliente(id);
			
			if(cliBean.getClie_id_cd_codigocliente().equalsIgnoreCase("0")){
				
				log.info("***Cliente não Encontrado ******");
				
				jsonRetorno = "Cliente Não encontrado";
				
			}else{
			
				ObjectMapper mapper = new ObjectMapper();
				jsonRetorno = mapper.writeValueAsString(cliBean);
				
				return jsonRetorno;	
			
			}
			
		}else if(criteria.equalsIgnoreCase("hierarquia")){
			
			HierarquiaClienteBean hierarBean = new HierarquiaClienteBean();
			CsCdtbHierarquiaClienteDao hierarDao = new CsCdtbHierarquiaClienteDao();
			
			
			int idHierar = Integer.parseInt(id);
			hierarBean = hierarDao.slcHierarquiaCliente(idHierar);
			
			if(hierarBean.getCdCliente() != 0){
				
				
				ObjectMapper mapper = new ObjectMapper();
				jsonRetorno = mapper.writeValueAsString(hierarBean);
				
				return jsonRetorno;	
				
			}else{
				

				log.info("*** Hierarquia de Cliente Não encontrado ******");
				
				jsonRetorno = "Hierarquia de Cliente Não encontrado";
				
			}
			
			
			
		}
		
		} catch (Exception e) {
			log.info("Erro na consulta por Criteria:" +e);
			
		}finally{
			
			daoLog.createLog(logsBean);
			log.info("**** Fim de Serviço de Consulta por Criteria.****");
		}
		
		return jsonRetorno;
	}

}
