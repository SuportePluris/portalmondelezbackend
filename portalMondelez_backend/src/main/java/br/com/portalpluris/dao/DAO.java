package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import br.com.portalpluris.bean.LoginBean;

public class DAO {
	
	private static HikariDataSource dsCatApi;
	private static Connection con;
	private static HikariDataSource dsCatApiEmpresa;
	private static Connection conEmpresa;

	/**
	 * Método responsável por realizar a conexão com o banco de dados.
	 * @author Caio Fernandes	
	 * @return con
	 * 
	 * */
	public static Connection getConexao() {

		try {
			if (dsCatApi == null || con.isClosed()) {
				ConfiguracaoDataSourcer confDataSoucer = new ConfiguracaoDataSourcer("datasource.properties");

				HikariConfig confCatapi = new HikariConfig();
				confCatapi.setJdbcUrl(confDataSoucer.getUrl());
				confCatapi.setUsername(confDataSoucer.getUserName());
				confCatapi.setPassword(confDataSoucer.getPassword());
				confCatapi.setDriverClassName(confDataSoucer.getDriveClasse());
				dsCatApi = new HikariDataSource(confCatapi);
				con = dsCatApi.getConnection();
			}

			return con;

		} catch (Throwable e) {

			System.out.println(e);

		}
		return con;
	}
	
}
