package br.com.portalpluris.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.mapstruct.Context;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilLayoutEmail;
import br.com.portalpluris.bean.BaseClienteConsolidadaBean;
import br.com.portalpluris.bean.ContatosBean;
import br.com.portalpluris.bean.CsAstbEmbarqueFoupBean;
import br.com.portalpluris.bean.CsAstbUsuariocanalUscaBean;
import br.com.portalpluris.bean.CsCdtbCanalCanaBean;
import br.com.portalpluris.bean.CsCdtbContatoclienteCoclBean;
import br.com.portalpluris.bean.CsCdtbFollowupFoupBean;
import br.com.portalpluris.bean.CsCdtbMotivoMotiBean;
import br.com.portalpluris.bean.CsCdtbResponsavelRespBean;
import br.com.portalpluris.bean.CsCdtbStatusStatBean;
import br.com.portalpluris.bean.CsCdtbTipoTipoBean;
import br.com.portalpluris.bean.CsCdtbTransportadoraEmail;
import br.com.portalpluris.bean.CsCdtbTransportadoraTranBean;
import br.com.portalpluris.bean.CsCdtbUsuarioUserBean;
import br.com.portalpluris.bean.CsNgtbEmbarqueAgendamentoEmbaBean;
import br.com.portalpluris.bean.DetalhesEmailCompletoBean;
import br.com.portalpluris.bean.EmbarqueAgendamentoBean;
import br.com.portalpluris.bean.ImportacaoCancelarPedidoBean;
import br.com.portalpluris.bean.ImportacaoCodTranspBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.RetornoInsertContatoBean;
import br.com.portalpluris.bean.StatusCanalBean;
import br.com.portalpluris.bean.StatusEmbarqueBean;
import br.com.portalpluris.bean.TransportadoraEmailRet;
import br.com.portalpluris.business.AgendamentoBusiness;
import br.com.portalpluris.business.CsAstbEmbarqueFoupBusiness;
import br.com.portalpluris.business.CsCdtbCanalCanaBusiness;
import br.com.portalpluris.business.CsCdtbClienteClieBusiness;
import br.com.portalpluris.business.CsCdtbContatoclienteCoclBusiness;
import br.com.portalpluris.business.CsCdtbContatosContBusiness;
import br.com.portalpluris.business.CsCdtbFollowupFoupBusiness;
import br.com.portalpluris.business.CsCdtbMotivoMotiBusiness;
import br.com.portalpluris.business.CsCdtbRegiaoRegiBusiness;
import br.com.portalpluris.business.CsCdtbResponsavelRespBusiness;
import br.com.portalpluris.business.CsCdtbStatusStatBusiness;
import br.com.portalpluris.business.CsCdtbTipoTipoBusiness;
import br.com.portalpluris.business.CsCdtbTransportadoraEmailTrmlBusiness;
import br.com.portalpluris.business.CsCdtbTransportadoraTranBusiness;
import br.com.portalpluris.business.CsLgtbNotaFiscalNofiBusiness;
import br.com.portalpluris.business.CsLgtbShipmentShipBusiness;
import br.com.portalpluris.business.CsNgtbEmbarqueEmbaBusiness;
import br.com.portalpluris.business.EmailPDFBusiness;
import br.com.portalpluris.business.ImportacaoPortalBusiness;
import br.com.portalpluris.business.PortalPlurisBusiness;
import br.com.portalpluris.business.RelatorioBusiness;
import br.com.portalpluris.dao.CsAstbUsuariocanalUscaDao;

@RestController
@RequestMapping("/portalpluris")
@EnableAutoConfiguration
public class ControllerPortalPluris {

	private PortalPlurisBusiness portalPlurisBusiness = new PortalPlurisBusiness();
	private CsCdtbFollowupFoupBusiness foupBusiness = new CsCdtbFollowupFoupBusiness();
	private CsNgtbEmbarqueEmbaBusiness embaBusiness = new CsNgtbEmbarqueEmbaBusiness();
	private CsCdtbContatoclienteCoclBusiness coclBusiness = new CsCdtbContatoclienteCoclBusiness();
	private CsAstbEmbarqueFoupBusiness emfoBusiness = new CsAstbEmbarqueFoupBusiness();

	// GET TESTE INTEGRIDADE DA API
	private CsCdtbStatusStatBusiness statusBusiness = new CsCdtbStatusStatBusiness();
	private CsCdtbTransportadoraTranBusiness tranBusiness = new CsCdtbTransportadoraTranBusiness();
	private CsCdtbClienteClieBusiness clieBusiness = new CsCdtbClienteClieBusiness();
	private RelatorioBusiness relatorioBusiness = new RelatorioBusiness();
	private AgendamentoBusiness agenBusiness = new AgendamentoBusiness();

	private CsCdtbTipoTipoBusiness tipoBusiness = new CsCdtbTipoTipoBusiness();
	private CsCdtbMotivoMotiBusiness motivoBusiness = new CsCdtbMotivoMotiBusiness();
	private CsCdtbResponsavelRespBusiness responsavelBusiness = new CsCdtbResponsavelRespBusiness();
	
	private ImportacaoPortalBusiness importacaoPortalBusiness = new ImportacaoPortalBusiness();

	
	private CsCdtbCanalCanaBusiness  canalBusiness = new CsCdtbCanalCanaBusiness();
	private EmailPDFBusiness emailPDFBusiness = new EmailPDFBusiness();
	
	private CsCdtbRegiaoRegiBusiness regiaoBusiness = new CsCdtbRegiaoRegiBusiness();
	
	private CsCdtbContatosContBusiness contatoBusiness = new CsCdtbContatosContBusiness(); 
	
	

	// GET TESTE INTEGRIDADE DA API

	@CrossOrigin
	@GetMapping("getstatusapp")
	public String getStatusApp() {
		
		System.gc();
		
		return "{\"status\":\"up\"}";
	}

	// POST TESTE INTEGRIDADE DA API
	@CrossOrigin
	@RequestMapping(value = "poststatusapp", method = RequestMethod.POST)
	public String postStatusApp() {
		
		System.gc();
		
		return "{\"status\":\"up\"}";
	}

	// VALIDAR LOGIN ATRAVES DO USUARIO DO CRM
	@CrossOrigin
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String login(@RequestBody CsCdtbUsuarioUserBean loginBean, @Context HttpServletRequest request)
			throws Exception {

		System.gc();
		
		String retorno = portalPlurisBusiness.getLogin(loginBean);

		return retorno;
	}

	@RequestMapping(value = "getallusers", method = { RequestMethod.GET }, produces = {
			"application/xml;charset=UTF-8" })
	public String findEmpresa() throws Exception {

		System.gc();
		String retorno = portalPlurisBusiness.getAllUsers();

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "createupdateusers", method = RequestMethod.POST)
	public String createUpdateUsers(@RequestBody CsCdtbUsuarioUserBean usuarioBean, @Context HttpServletRequest request)
			throws Exception {

		System.gc();
		
		String retorno = "";

		RetornoBean retValidado = portalPlurisBusiness.validaUsuario(usuarioBean);

		if (retValidado.isSucesso()) {

			if (usuarioBean.getUser_id_cd_usuario() == null
					|| (usuarioBean.getUser_id_cd_usuario().equalsIgnoreCase("0")
							|| usuarioBean.getUser_id_cd_usuario().equalsIgnoreCase(""))) {
				retorno = portalPlurisBusiness.createUser(usuarioBean);

			} else if (Long.parseLong(usuarioBean.getUser_id_cd_usuario()) > 0) {
				retorno = portalPlurisBusiness.updateUser(usuarioBean);
			}

		} else {
			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retValidado);
		}

		return retorno;
	}

	// *** BE Cadastro de Foups ***//

	@CrossOrigin
	@RequestMapping(value = "createupdatefoup", method = RequestMethod.POST)
	public String createUpdateFoup(@RequestBody CsCdtbFollowupFoupBean foupBean, @Context HttpServletRequest request)
			throws Exception {

		System.gc();
		
		String retorno = "";

		RetornoBean retValidado = portalPlurisBusiness.validaCadastroFollow(foupBean);

		if (retValidado.isSucesso()) {

			retorno = foupBusiness.createUpdateFoup(foupBean);

		} else {
			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retValidado);
		}

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getlistfoup/{foupInAtivo}", method = RequestMethod.GET)
	public String getListFoup(@PathVariable("foupInAtivo") String foupInAtivo) throws Exception {

		System.gc();
		
		String retorno = foupBusiness.getListCadastroFoup(foupInAtivo);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getfoup/{foupIdCdFollowup}", method = RequestMethod.GET)
	public String getFoup(@PathVariable("foupIdCdFollowup") int foupIdCdFollowup) throws Exception {

		System.gc();
		
		String retorno = foupBusiness.getFoup(foupIdCdFollowup);

		return retorno;
	}

	// *** BE Lista de Embarque por status ***//

	@CrossOrigin
	// @RequestMapping(value = "getembarquebystatus/{emba_id_cd_status}/{canal}",
	// method = RequestMethod.GET)
	@RequestMapping(value = "getembarquebystatus", method = RequestMethod.POST)
	// public String getEmbarqueByStatus(@PathVariable("emba_id_cd_status") int
	// emba_id_cd_status, @PathVariable("canal") String canal) throws Exception {
	public String getEmbarqueByStatus(@RequestBody StatusCanalBean statuscanal) throws Exception {

		System.gc();
		
		String emba_id_cd_status = statuscanal.getEmba_id_cd_status();
		String idFuncionario = statuscanal.getIdFuncionario();

		String retorno = embaBusiness.getEmbarqueByStatus(emba_id_cd_status, idFuncionario);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getlistembarque/{embaIdCdStatusagendamento}", method = RequestMethod.GET)
	public String getListEmbarque(@PathVariable("embaIdCdStatusagendamento") int embaIdCdStatusagendamento)
			throws Exception {

		System.gc();
		
		String retorno = embaBusiness.getListEmba(embaIdCdStatusagendamento);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getsomaembarque/{filtro}/{criterio}/{pa}", method = RequestMethod.GET)
	public String getSomaEmbarque(@PathVariable("filtro") String filtro, @PathVariable("criterio") String criterio,
			@PathVariable("pa") String pa) throws Exception {

		System.gc();
		
		String retorno = embaBusiness.getSomaEmba(filtro, criterio, pa);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getlistembarque/{filtro}/{criterio}/{pa}", method = RequestMethod.GET)
	public String getListEmbarque(@PathVariable("filtro") String filtro, @PathVariable("criterio") String criterio,
			@PathVariable("pa") String pa) throws Exception {
		
		System.gc();
		
		String retorno = embaBusiness.getListEmba(filtro, criterio, pa);

		return retorno;

	}

	@CrossOrigin
	@RequestMapping(value = "updateleadtime", method = RequestMethod.POST)
	public String updateLeadTime(@RequestBody CsNgtbEmbarqueAgendamentoEmbaBean embarqueAgendamentoBean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = "";

		retorno = embaBusiness.updateLeadTime(embarqueAgendamentoBean);

		return retorno;
	}

	// CRIAR ASSOCIATIVA ENTRE EMBARQUE E FOLLOW-UP
	@CrossOrigin
	@RequestMapping(value = "createembarquefoup", method = RequestMethod.POST)
	public String createEmbarqueFoup(@RequestBody CsAstbEmbarqueFoupBean embarqueFoupBean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = "";

		RetornoBean retValidado = portalPlurisBusiness.validaEmbarqueFoup(embarqueFoupBean);

		if (retValidado.isSucesso()) {

			retorno = emfoBusiness.createEmbarqueFoup(embarqueFoupBean);

		} else {
			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retValidado);
		}

		return retorno;
	}

	// OBTER FOLLOWUPS DE UM EMBARQUE ESPECIFICO
	@CrossOrigin
	@RequestMapping(value = "getembarquefoup/{idembarque}", method = RequestMethod.GET)
	public String getEmbarqueFoup(@PathVariable("idembarque") String idEmbarque) throws Exception {

		System.gc();
		
		String retorno = emfoBusiness.getEmbarqueFoup(idEmbarque);

		return retorno;
	}

	// *** BE Contato Clientes ***//

	@CrossOrigin
	@RequestMapping(value = "createupdatecontatocliente", method = RequestMethod.POST)
	public String createUpdateContatocliente(@RequestBody CsCdtbContatoclienteCoclBean coceBean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = "";

		RetornoBean retValidado = portalPlurisBusiness.validaCadastroCliente(coceBean);

		if (retValidado.isSucesso()) {

			retorno = coclBusiness.createUpdateContatocliente(coceBean);

		} else {
			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retValidado);
		}

		return retorno;
	}
	//brian
	@CrossOrigin
	@RequestMapping(value = "getcontatocliente/{coclIdCdCodigocliente}", method = RequestMethod.GET)
	public String getContatoCliente(@PathVariable("coclIdCdCodigocliente") int coclIdCdCodigocliente) throws Exception {

		System.gc();
		
		String retorno = coclBusiness.getContatoCliente(coclIdCdCodigocliente);

		return retorno;
	}
	
	//brian
	@CrossOrigin
	@RequestMapping(value = "getnotafiscal/{idpedidomond}", method = RequestMethod.GET)
	public String getNotaFiscal(@PathVariable("idpedidomond") String idpedidomond) throws Exception {

		
		CsLgtbNotaFiscalNofiBusiness nofiBusiness = new CsLgtbNotaFiscalNofiBusiness();
		
		System.gc();
		
		String retorno = nofiBusiness.getNotaFiscal(idpedidomond);

		return retorno;
	}
	
//	@CrossOrigin
//	@RequestMapping(value = "insertupdatetransportadoraemail/{iduser}/{codtransportadora}/{razaosocial}/{cnpj}/{email}", method = RequestMethod.GET)
//	public String insertUpdateTransportadoraMail(@PathVariable("iduser") String iduser,@PathVariable("codtransportadora") String codtransportadora,@PathVariable("razaosocial") String razaosocial,@PathVariable("cnpj") String cnpj,@PathVariable("email") String email) throws Exception {
//
//		System.gc();
//		String retorno = null;
//		
//		CsCdtbTransportadoraEmailTrmlBusiness tranMail = new CsCdtbTransportadoraEmailTrmlBusiness();
//
//		if(codtransportadora.equals("null") || codtransportadora.equals("") || codtransportadora.equals(" ")){
//			
//			retorno = "É obrigatorio o preenchimento do Codigo Transportadora";
//				
//			return retorno;
//			
//		}else{
//		
//		 retorno = tranMail.insertTransEmail(iduser,codtransportadora,razaosocial,cnpj,email);	
//		
//		System.gc();
//
//		return retorno;
//		}
		
	
//	//brian
	@CrossOrigin
	@RequestMapping(value = "validacarga", method = RequestMethod.GET)
	public String validaCarga() throws Exception {

		System.gc();
		
		CsLgtbShipmentShipBusiness shipBusiness = new CsLgtbShipmentShipBusiness();		
		
		String retorno = shipBusiness.validaCarga();		
		
		System.gc();

		return retorno;
	}
	

	
	//BRIAN	
	@RequestMapping(value = "insertcontato", method = RequestMethod.POST)
	public String insertContatoCliente(@RequestBody ContatosBean contBean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = null;
		
		ObjectMapper mapper = new ObjectMapper();
		
		RetornoInsertContatoBean insertContatoRet = new RetornoInsertContatoBean();		
		
		if(contBean.getCont_ds_tipocontato() == null || contBean.getCont_ds_tipocontato().equals("null") ){
			
			insertContatoRet.setMenssagem("É obrigatorio o preenchimento do tipo de contato");
			insertContatoRet.setIdErro("1");
				
			retorno = mapper.writeValueAsString(insertContatoRet);
			
			return retorno;
			
		}else{
		
		 retorno = contatoBusiness.insertContatoCliente(contBean);
		 
		}
		System.gc();

		return retorno;
	}

	// *** CADASTRO OU ATUALIZAÇÃO DE STATUS ***//

	@CrossOrigin
	@RequestMapping(value = "createupdatestatus", method = RequestMethod.POST)
	public String createUpdateStatus(@RequestBody CsCdtbStatusStatBean statusBean, @Context HttpServletRequest request)
			throws Exception {

		System.gc();
		
		String retorno = statusBusiness.createUpdateStat(statusBean);
		
		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getliststatus/{statusInAtivo}", method = RequestMethod.GET)
	public String getListStatus(@PathVariable("statusInAtivo") String statusInAtivo) throws Exception {

		System.gc();
		
		String retorno = statusBusiness.getListStatus(statusInAtivo);
		
		return retorno;
	}
	
	@CrossOrigin
	@RequestMapping(value = "getliststatusqtd/{statusInAtivo}/{idFuncionario}", method = RequestMethod.GET)
	public String getListStatusQtd(@PathVariable("statusInAtivo") String statusInAtivo, @PathVariable("idFuncionario") String idFuncionario) throws Exception {

		System.gc();
		
		String retorno = statusBusiness.getListStatusQtd(statusInAtivo, idFuncionario);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getstatus/{statIdCdStatus}", method = RequestMethod.GET)
	public String getStatus(@PathVariable("statIdCdStatus") int statIdCdStatus) throws Exception {

		System.gc();
		
		String retorno = statusBusiness.getStatus(statIdCdStatus);
		
		return retorno;
	}

	// *** CADASTRO OU ATUALIZAÇÃO DE TRANSPORTADORAS ***//

	@CrossOrigin
	@RequestMapping(value = "createupdatetran", method = RequestMethod.POST)
	public String createUpdateTran(@RequestBody CsCdtbTransportadoraTranBean tranBean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = "";

		RetornoBean retValidado = portalPlurisBusiness.validaCadastroTransportadora(tranBean);

		if (retValidado.isSucesso()) {

			retorno = tranBusiness.createUpdateTran(tranBean);

		} else {
			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retValidado);
		}

		return retorno;

	}

	@CrossOrigin
	@RequestMapping(value = "getlisttransportadora/{tranInAtivo}", method = RequestMethod.GET)
	public String getListTransportadora(@PathVariable("tranInAtivo") String tranInAtivo) throws Exception {

		System.gc();
		
		String retorno = tranBusiness.getListTransportadora(tranInAtivo);

		return retorno;
	}

	@RequestMapping(value = "gettransportadora/{tranIdCdTransportadora}", method = RequestMethod.GET)
	public String getTransportadora(@PathVariable("tranIdCdTransportadora") String tranIdCdTransportadora)
			throws Exception {

		System.gc();
		
		String retorno = tranBusiness.getTransportadora(tranIdCdTransportadora);

		return retorno;
	}

	@RequestMapping(value = "getlistcontatostransportadora/{codTransportadora}", method = RequestMethod.GET)
	public String getListContatosTransportadora(@PathVariable("codTransportadora") String codTransportadora)
			throws Exception {

		System.gc();
		
		String retorno = tranBusiness.getListTransportadoraByCodTransp(codTransportadora);

		return retorno;
	}

	// *** CADASTRO OU ATUALIZAÇÃO DE CLIENTES ***//

	@CrossOrigin
	@RequestMapping(value = "createclie", method = RequestMethod.POST)
	public String createClie(@RequestBody BaseClienteConsolidadaBean clieBean, @Context HttpServletRequest request)
			throws Exception {

		System.gc();
		
		String retorno = clieBusiness.createClie(clieBean);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "updateclie", method = RequestMethod.POST)
	public String updateClie(@RequestBody BaseClienteConsolidadaBean clieBean, String dhImportacao, String idUser,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = clieBusiness.updateClie(clieBean);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "createupdatecliente", method = RequestMethod.POST)
	public String createUpdateCliente(@RequestBody BaseClienteConsolidadaBean clieBean, String dhImportacao,
			String idUser, @Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = "";

		RetornoBean retValidado = portalPlurisBusiness.validaCliente(clieBean);

		if (retValidado.isSucesso()) {

			if (clieBean.getClie_id_cd_cliente() == null || (clieBean.getClie_id_cd_cliente().equalsIgnoreCase("0")
					|| clieBean.getClie_id_cd_cliente().equalsIgnoreCase(""))) {

				retorno = clieBusiness.createClie(clieBean);

			} else if (Long.parseLong(clieBean.getClie_id_cd_cliente()) > 0) {

				retorno = clieBusiness.updateClie(clieBean);

			}

		} else {
			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retValidado);
		}

		return retorno;

	}

	@CrossOrigin
	@RequestMapping(value = "getlistcliente/{clieInAtivo}", method = RequestMethod.GET)
	public String getListCliente(@PathVariable("clieInAtivo") String clieInAtivo) throws Exception {

		System.gc();
		
		String retorno = clieBusiness.getListCliente(clieInAtivo);

		return retorno;
	}
	
	@CrossOrigin
	@RequestMapping(value = "getlistclientefiltro/{criterio}/{valor}", method = RequestMethod.GET)
	public String getListClienteFiltro(@PathVariable("criterio") String criterio, @PathVariable("valor") String valor) throws Exception {

		System.gc();
		
		String retorno = clieBusiness.getListClienteFiltro(criterio, valor);

		return retorno;
	}

	@RequestMapping(value = "getcliente/{codCliente}", method = RequestMethod.GET)
	public String getCliente(@PathVariable("codCliente") String codCliente) throws Exception {

		System.gc();
		
		String retorno = clieBusiness.getCliente(codCliente);

		return retorno;
	}

	@RequestMapping(value = "getclientebyembarque/{idEmbarque}", method = RequestMethod.GET)
	public String getClienteByEmbarque(@PathVariable("idEmbarque") String idEmbarque) throws Exception {

		System.gc();
		
		String retorno = clieBusiness.getClienteByEmbarque(idEmbarque);

		return retorno;
	}

	// END POINT QUE FAZ CONSULTA GENERICA.
	// Ex: Busca de embarque ou de delevery.
	// 1 parametro recebe qual dado quer buscar ( embarque ou delevery ou etc.)
	// 2 parametro recebe o id a ser buscado.
	@RequestMapping(value = "searchbycriteria/{criteria}/{id}", method = RequestMethod.GET)
	public String getbycriteria(@PathVariable("criteria") String criteria, @PathVariable("id") String id)
			throws Exception {

		System.gc();
		
		String retorno = portalPlurisBusiness.searchByCriteria(criteria, id);

		return retorno;
	}

	// RETORNO PARA PREENCHER LISTA DE SKUD
	// BRIAN
	@CrossOrigin
	@RequestMapping(value = "getskuds/{delivery}", method = RequestMethod.GET)
	public String getSkud(@PathVariable("delivery") String idDelivery) throws Exception {

		System.gc();
		
		String retorno = agenBusiness.getSkus(idDelivery);

		return retorno;
	}

	// RETORNO PARA PREENCHER LISTA DE SKUD
	// BRIAN
	@CrossOrigin
	@RequestMapping(value = "getstatusdelivery/{condicao}/{id}/{status}", method = RequestMethod.GET)
	public String getstatusdelivery(@PathVariable("condicao") String condicao, @PathVariable("id") String id, @PathVariable("status") String status)
			throws Exception {

		System.gc();
		
		id = id.replace("-", "/");
		
		String retorno = agenBusiness.getDeliveryByStatus(condicao, id, status);

		return retorno;
	}

	// RETORNO PARA PREENCHER LISTA DE SHIPMENT E DELIVERY
	// BRIAN
	@CrossOrigin
	@RequestMapping(value = "getshipmentdelivery/{embarque}", method = RequestMethod.GET)
	public String getshipmentdelivery(@PathVariable("embarque") String embarque) throws Exception {

		System.gc();
		
		String retorno = agenBusiness.getShipmentDelivery(embarque);

		return retorno;
	}

	// RETORNO PARA PREENCHER LISTA DE FOUP POR STATUS
	// BRIAN
	@CrossOrigin
	@RequestMapping(value = "getfoupbystatus/{idStatus}", method = RequestMethod.GET)
	public String getfoupbystatus(@PathVariable("idStatus") String idStatus) throws Exception {

		System.gc();
		
		String retorno = foupBusiness.getFoupbyStatus(idStatus);
		
		return retorno;
	}


	// *** BE Formalizar Embarque ***//

	@Deprecated
	@CrossOrigin
	@RequestMapping(value = "envioembarque/{embaIdCdEmbarque}/{userIdCdUsuario}", method = RequestMethod.GET)
	public String envioEmbarque(@PathVariable("embaIdCdEmbarque") int embaIdCdEmbarque,
			@PathVariable("userIdCdUsuario") int userIdCdUsuario) throws Exception {

		System.gc();
		
		String retorno = embaBusiness.envioEmbarque(embaIdCdEmbarque, userIdCdUsuario);

		return retorno;
	}

	@RequestMapping(value = "getlisteventosfoup/{inativo}", method = RequestMethod.GET)
	public String getListEventosFoup(@PathVariable("inativo") String inativo) throws Exception {

		System.gc();
		
		String retorno = foupBusiness.getListCadastroFoup(inativo);

		return retorno;
	}

	

	// *** BE Agendamento ***//

	@CrossOrigin
	@RequestMapping(value = "getlstagendamento/{embaIdCdEmbarque}", method = RequestMethod.GET)
	public String getLstAgendamento(@PathVariable("embaIdCdEmbarque") int embaIdCdEmbarque) throws Exception {

		System.gc();
		
		String retorno = embaBusiness.getLstAgendamento(embaIdCdEmbarque);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getagendamento/{daag_id_cd_agendamento}", method = RequestMethod.GET)
	public String getAgendamento(@PathVariable("daag_id_cd_agendamento") int daag_id_cd_agendamento) throws Exception {

		System.gc();
		
		String retorno = embaBusiness.getAgendamento(daag_id_cd_agendamento);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "createupdateagendamento", method = RequestMethod.POST)
	public String createUpdateAgendamento(@RequestBody EmbarqueAgendamentoBean embaBean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = embaBusiness.createUpdateAgendamento(embaBean);

		return retorno;
	}

	// FLAG DESFLAG EMBARQUE EM USO
	@CrossOrigin
	@RequestMapping(value = "setembarqueemuso/{emba_id_cd_embarque}/{in_uso}/{emba_id_cd_usuarioatualizacao}", method = RequestMethod.GET)
	public void setEmbarqueEmUso(@PathVariable("emba_id_cd_embarque") int emba_id_cd_embarque,
			@PathVariable("in_uso") String in_uso,
			@PathVariable("emba_id_cd_usuarioatualizacao") int emba_id_cd_usuarioatualizacao) throws Exception {

		System.gc();
		
		embaBusiness.setEmbarqueEmUsoBusiness(emba_id_cd_embarque, in_uso, emba_id_cd_usuarioatualizacao);

	}

	@CrossOrigin
	@RequestMapping(value = "getlstcanalbyusuario/{idUsuario}", method = RequestMethod.GET)
	public String getlstCanalByUsuario(@PathVariable("idUsuario") String idUsuario) throws Exception {

		System.gc();
		
		String retorno = canalBusiness.getListCanalByUsuario(idUsuario);
		
		return retorno;

	}

	@CrossOrigin
	@RequestMapping(value = "getlistcanal/{inativo}/{idUsuario}", method = RequestMethod.GET)
	public String getListCanal(@PathVariable("inativo") String inativo, @PathVariable("idUsuario") String idUsuario)
			throws Exception {

		System.gc();
		
		String retorno = canalBusiness.getListCanal(inativo, idUsuario);
		
		return retorno;

	}
	
	@CrossOrigin
	@RequestMapping(value = "getlistcanal/{inativo}", method = RequestMethod.GET)
	public String getListCanal(@PathVariable("inativo") String inativo) throws Exception {

		System.gc();
		
		String retorno = canalBusiness.getListCanal(inativo);

		return retorno;
		
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "getcreateupdatecanal", method = RequestMethod.POST)
	public String getCreateUpdateCanal(@RequestBody CsCdtbCanalCanaBean bean, @Context HttpServletRequest request)
			throws Exception {

		System.gc();
		
		String retorno = "";

		RetornoBean retValidado = portalPlurisBusiness.validaCanal(bean);

		if (retValidado.isSucesso()) {

			if (bean.getCana_id_cd_canal() == null || (bean.getCana_id_cd_canal().equalsIgnoreCase("0")
					|| bean.getCana_id_cd_canal().equalsIgnoreCase(""))) {
				retorno = canalBusiness.createCanal(bean);

			} else if (Long.parseLong(bean.getCana_id_cd_canal()) > 0) {
				retorno = canalBusiness.updateCanal(bean);
			}

		} else {
			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retValidado);
		}

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "postcreatecanalbyusuario", method = RequestMethod.POST)
	public String postCreateCanalByUsuario(@RequestBody CsAstbUsuariocanalUscaBean bean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = "";

		RetornoBean retValidado = portalPlurisBusiness.validaUsuarioCanal(bean);

		if (retValidado.isSucesso()) {

			CsAstbUsuariocanalUscaDao dao = new CsAstbUsuariocanalUscaDao();
			List<CsAstbUsuariocanalUscaBean> listBean = new ArrayList<CsAstbUsuariocanalUscaBean>();

			RetornoBean retBean = new RetornoBean();

			try {

				retBean = dao.createUsuarioCanal(bean);

				retBean.setSucesso(true);
				retBean.setProcesso("INSERT");
				retBean.setMsg("Vinculo efetuado com sucesso!");

			} catch (Exception e) {
				e.printStackTrace();
				retBean.setSucesso(false);
				retBean.setProcesso("INSERT");
				retBean.setMsg(e.getMessage());

			}

			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retBean);

		} else {
			ObjectMapper mapper = new ObjectMapper();
			retorno = mapper.writeValueAsString(retValidado);
		}

		return retorno;
	}

	// *** BE Cadastro Tipo ***//

	@CrossOrigin
	@RequestMapping(value = "createupdatetipo", method = RequestMethod.POST)
	public String createUpdateTipo(@RequestBody CsCdtbTipoTipoBean tipoBean, @Context HttpServletRequest request)
			throws Exception {

		System.gc();
		
		String retorno = tipoBusiness.createUpdateTipo(tipoBean);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getlistipo/{tipoInAtivo}", method = RequestMethod.GET)
	public String getListTipo(@PathVariable("tipoInAtivo") String tipoInAtivo) throws Exception {

		System.gc();
		
		String retorno = tipoBusiness.getListTipo(tipoInAtivo);

		return retorno;
	}

	// *** BE Cadastro Motivo ***//

	@CrossOrigin
	@RequestMapping(value = "createupdatemotivo", method = RequestMethod.POST)
	public String createUpdateMotivo(@RequestBody CsCdtbMotivoMotiBean motivoBean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = motivoBusiness.createUpdateMotivo(motivoBean);

		return retorno;
	}

	@RequestMapping(value = "getlistmotivos/{tipodetela}/{inativo}", method = RequestMethod.GET)
	public String getListMotivos(@PathVariable("tipodetela") String tipodetela, @PathVariable("inativo") String inativo)
			throws Exception {

		System.gc();
		
		String retorno = motivoBusiness.getListMotivos(tipodetela, inativo);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getmotivo/{moti_id_cd_motivo}", method = RequestMethod.GET)
	public String getMotivo(@PathVariable("moti_id_cd_motivo") int moti_id_cd_motivo) throws Exception {

		System.gc();
		
		String retorno = motivoBusiness.getMotivo(moti_id_cd_motivo);

		return retorno;
	}

	// *** BE Cadastro Responsável ***//

	@CrossOrigin
	@RequestMapping(value = "createupdateresponsavel", method = RequestMethod.POST)
	public String createUpdateResponsavel(@RequestBody CsCdtbResponsavelRespBean respBean,
			@Context HttpServletRequest request) throws Exception {

		System.gc();
		
		String retorno = responsavelBusiness.createUpdateResp(respBean);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "getresponsavel/{resp_id_cd_responsavel}", method = RequestMethod.GET)
	public String getResponsavel(@PathVariable("resp_id_cd_responsavel") int resp_id_cd_responsavel) throws Exception {

		System.gc();
		
		String retorno = responsavelBusiness.getResponsavel(resp_id_cd_responsavel);

		return retorno;
	}
	
	@CrossOrigin
	@RequestMapping(value = "getlistresponsaveis/{inativo}", method = RequestMethod.GET)
	public String getListResponsaveisFoup(@PathVariable("inativo") String inativo) throws Exception {

		System.gc();
		
		String retorno = responsavelBusiness.getListResponsaveis(inativo);

		return retorno;

	}
	
	@CrossOrigin
	@RequestMapping(value = "getrelatorio/{periodo}/{ano}/{transp}", method = RequestMethod.GET)
	public String getRelatorio(@PathVariable("periodo") String periodo, @PathVariable("ano") String ano, @PathVariable("transp") String transp) throws Exception {
		
		System.gc();
		
		//String retorno = relatorioBusiness.getRelatorioBusiness(periodo, ano, transp);
		String retorno = relatorioBusiness.getRelatorioBusinessNovo(periodo, ano, transp);

		return retorno;
	}

	@CrossOrigin
	@RequestMapping(value = "updatestatusembarque", method = RequestMethod.POST)
	public String updateStatusEmbarque(@RequestBody StatusEmbarqueBean statusEmbarqueBean,
			@Context HttpServletRequest request) throws Exception {
		
		System.gc();
		
		String retorno = embaBusiness.updateStatusEmbarque(statusEmbarqueBean);

		return retorno;
	}
	
/*	
    @CrossOrigin
	@RequestMapping(value = "testebyte", method = RequestMethod.GET)
	public String testebyte() throws Exception {

		String retorno = "";
		
		PDFBean pdfean = new PDFBean();
		UtilLayoutEmail UtilLayoutEmail = new UtilLayoutEmail();
		
		pdfean.setBytes(UtilLayoutEmail.getBytePDF());
		
		ObjectMapper mapper = new ObjectMapper();
		retorno = mapper.writeValueAsString(pdfean);

		return retorno;

	}
	*/
	
	@CrossOrigin
	@RequestMapping(value = "getpdf/{id_embarque}", method = RequestMethod.GET)
	public String getPDF(@PathVariable("id_embarque") String id_embarque) throws Exception {
		
		System.gc();
		
		String retorno = "";
		
		retorno = emailPDFBusiness.getBytes(id_embarque, "download");

		return retorno;

	}
	
	
	
	@CrossOrigin
	@RequestMapping(value = "getdetalhesagendacopypast/{id_embarque}", method = RequestMethod.GET)
	public String getDetalhesAgendaCopyPast(@PathVariable("id_embarque") String id_embarque) throws Exception {

		System.gc();
		
		String retorno = "";
		
		UtilLayoutEmail utilLayout = new UtilLayoutEmail();
		DetalhesEmailCompletoBean detalhesEmail = utilLayout.getHtmlBody(id_embarque, "emailCliente");
		
		ObjectMapper mapper = new ObjectMapper();
		retorno = mapper.writeValueAsString(detalhesEmail);
		
		return retorno;

	}
	
	
	@CrossOrigin
	@RequestMapping(value = "enviaemail/{destinatario}/{id_embarque}/{userIdCdUsuario}", method = RequestMethod.GET)
	public String enviaEmail(@PathVariable("destinatario") String destinatario, @PathVariable("id_embarque") String id_embarque, @PathVariable("userIdCdUsuario") String userIdCdUsuario) throws Exception {

		System.gc();
		String retorno = "";
		
		retorno = emailPDFBusiness.enviaEmail(destinatario, id_embarque, userIdCdUsuario);
		
		return retorno;

	}
	
	// ATUALIZAR COD. TRANSPORTADORA DE ACORDO COM PLANILHA IMPORTADA VIA PORTAL
	@CrossOrigin
	@RequestMapping(value = "updatecodtranspbyimport", method = RequestMethod.POST)
	public RetornoBean updateCodTranspByImport(@RequestBody List<ImportacaoCodTranspBean> lstCodTranspBean, @Context HttpServletRequest request) throws Exception {

		System.gc();
		
		RetornoBean retBean = new RetornoBean();
		retBean = importacaoPortalBusiness.updateCodTransp(lstCodTranspBean);
		
		return retBean;
	}
	
	// CANCELAR PEDIDOS DE ACORDO COM PLANILHA IMPORTADA VIA PORTAL
	@CrossOrigin
	@RequestMapping(value = "updatecancelarpedidobyimport", method = RequestMethod.POST)
	public RetornoBean updateCancelarPedidoByImport(@RequestBody List<ImportacaoCancelarPedidoBean> lstCancelarPedidoBean, @Context HttpServletRequest request) throws Exception {

		System.gc();
		
		RetornoBean retBean = new RetornoBean();
		retBean = importacaoPortalBusiness.updateCancelarPedido(lstCancelarPedidoBean);
		
		return retBean;
	}
	
	@CrossOrigin
	@RequestMapping(value = "getlistregiao/{inativo}", method = RequestMethod.GET)
	public String getListRegiao(@PathVariable("inativo") String inativo) throws Exception {

		System.gc();
		
		String retorno = regiaoBusiness.getListRegiao(inativo);

		return retorno;
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "getcontatostransportadora/{idEmbarque}", method = RequestMethod.GET)
	public String getContatosTransportadora(@PathVariable("idEmbarque") String idEmbarque) throws SQLException {
		
		System.gc();
		String retorno = contatoBusiness.getContatosTransportadora(idEmbarque);
		
		return retorno;
	}
	
	@CrossOrigin
	@RequestMapping(value = "getcontatoscliente/{idEmbarque}", method = RequestMethod.GET)
	public String getContatosCliente(@PathVariable("idEmbarque") String idEmbarque) throws SQLException {
		
		System.gc();
		String retorno = contatoBusiness.getContatosCliente(idEmbarque);
		
		return retorno;
	}
	
	@CrossOrigin
	@RequestMapping(value = "getcontatosfocal/{idEmbarque}", method = RequestMethod.GET)
	public String getContatosFocal(@PathVariable("idEmbarque") String idEmbarque) throws SQLException {
		
		System.gc();
		String retorno = contatoBusiness.getContatosFocal(idEmbarque);
		
		return retorno;
	}
}
