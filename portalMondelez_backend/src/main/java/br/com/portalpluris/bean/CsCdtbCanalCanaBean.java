package br.com.portalpluris.bean;

public class CsCdtbCanalCanaBean {

	String cana_id_cd_canal;
	String cana_ds_canal;
	String cana_in_inativo;
	String cana_dh_registro;
	
	
	public String getCana_id_cd_canal() {
		return cana_id_cd_canal;
	}
	public void setCana_id_cd_canal(String cana_id_cd_canal) {
		this.cana_id_cd_canal = cana_id_cd_canal;
	}
	public String getCana_ds_canal() {
		return cana_ds_canal;
	}
	public void setCana_ds_canal(String cana_ds_canal) {
		this.cana_ds_canal = cana_ds_canal;
	}
	public String getCana_in_inativo() {
		return cana_in_inativo;
	}
	public void setCana_in_inativo(String cana_in_inativo) {
		this.cana_in_inativo = cana_in_inativo;
	}
	public String getCana_dh_registro() {
		return cana_dh_registro;
	}
	public void setCana_dh_registro(String cana_dh_registro) {
		this.cana_dh_registro = cana_dh_registro;
	}

	

}
