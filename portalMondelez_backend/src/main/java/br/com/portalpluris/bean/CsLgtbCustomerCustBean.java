package br.com.portalpluris.bean;

public class CsLgtbCustomerCustBean {
	
	String cust_cd_customercode;
	String cust_ds_customername;
	String cust_ds_addressship;
	String cust_ds_adresscomplement;
	String cust_ds_adresscomplement2;
	String cust_ds_citymunicipio;
	String cust_ds_cep;
	String cust_ds_state;
	String cust_ds_city;
	String cust_ds_codddphone;
	String cust_ds_phonenumber;
	String cust_ds_customernumber;
	String cust_ds_impadicind;
	String cust_ds_ibgecode;
	String cust_ds_statecode;
	String cust_ds_codcitycustomer;
	String cust_ds_citydescription;	
	String cust_ds_statusprocessamento;
	String cust_dh_processamento;
	String cust_ds_nomearquivo;
	
	public String getCust_cd_customercode() {
		return cust_cd_customercode;
	}
	public void setCust_cd_customercode(String cust_cd_customercode) {
		this.cust_cd_customercode = cust_cd_customercode;
	}
	public String getCust_ds_customername() {
		return cust_ds_customername;
	}
	public void setCust_ds_customername(String cust_ds_customername) {
		this.cust_ds_customername = cust_ds_customername;
	}
	public String getCust_ds_addressship() {
		return cust_ds_addressship;
	}
	public void setCust_ds_addressship(String cust_ds_addressship) {
		this.cust_ds_addressship = cust_ds_addressship;
	}
	public String getCust_ds_adresscomplement() {
		return cust_ds_adresscomplement;
	}
	public void setCust_ds_adresscomplement(String cust_ds_adresscomplement) {
		this.cust_ds_adresscomplement = cust_ds_adresscomplement;
	}
	public String getCust_ds_adresscomplement2() {
		return cust_ds_adresscomplement2;
	}
	public void setCust_ds_adresscomplement2(String cust_ds_adresscomplement2) {
		this.cust_ds_adresscomplement2 = cust_ds_adresscomplement2;
	}
	public String getCust_ds_citymunicipio() {
		return cust_ds_citymunicipio;
	}
	public void setCust_ds_citymunicipio(String cust_ds_citymunicipio) {
		this.cust_ds_citymunicipio = cust_ds_citymunicipio;
	}
	public String getCust_ds_cep() {
		return cust_ds_cep;
	}
	public void setCust_ds_cep(String cust_ds_cep) {
		this.cust_ds_cep = cust_ds_cep;
	}
	public String getCust_ds_state() {
		return cust_ds_state;
	}
	public void setCust_ds_state(String cust_ds_state) {
		this.cust_ds_state = cust_ds_state;
	}
	public String getCust_ds_city() {
		return cust_ds_city;
	}
	public void setCust_ds_city(String cust_ds_city) {
		this.cust_ds_city = cust_ds_city;
	}
	public String getCust_ds_codddphone() {
		return cust_ds_codddphone;
	}
	public void setCust_ds_codddphone(String cust_ds_codddphone) {
		this.cust_ds_codddphone = cust_ds_codddphone;
	}
	public String getCust_ds_phonenumber() {
		return cust_ds_phonenumber;
	}
	public void setCust_ds_phonenumber(String cust_ds_phonenumber) {
		this.cust_ds_phonenumber = cust_ds_phonenumber;
	}
	public String getCust_ds_customernumber() {
		return cust_ds_customernumber;
	}
	public void setCust_ds_customernumber(String cust_ds_customernumber) {
		this.cust_ds_customernumber = cust_ds_customernumber;
	}
	public String getCust_ds_impadicind() {
		return cust_ds_impadicind;
	}
	public void setCust_ds_impadicind(String cust_ds_impadicind) {
		this.cust_ds_impadicind = cust_ds_impadicind;
	}
	public String getcust_ds_ibgecode() {
		return cust_ds_ibgecode;
	}
	public void setcust_ds_ibgecode(String cust_ds_ibgecode) {
		this.cust_ds_ibgecode = cust_ds_ibgecode;
	}
	public String getCust_ds_statecode() {
		return cust_ds_statecode;
	}
	public void setCust_ds_statecode(String cust_ds_statecode) {
		this.cust_ds_statecode = cust_ds_statecode;
	}
	public String getCust_ds_codcitycustomer() {
		return cust_ds_codcitycustomer;
	}
	public void setCust_ds_codcitycustomer(String cust_ds_codcitycustomer) {
		this.cust_ds_codcitycustomer = cust_ds_codcitycustomer;
	}
	public String getCust_ds_citydescription() {
		return cust_ds_citydescription;
	}
	public void setCust_ds_citydescription(String cust_ds_citydescription) {
		this.cust_ds_citydescription = cust_ds_citydescription;
	}
	public String getCust_ds_statusprocessamento() {
		return cust_ds_statusprocessamento;
	}
	public void setCust_ds_statusprocessamento(String cust_ds_statusprocessamento) {
		this.cust_ds_statusprocessamento = cust_ds_statusprocessamento;
	}
	public String getCust_dh_processamento() {
		return cust_dh_processamento;
	}
	public void setCust_dh_processamento(String cust_dh_processamento) {
		this.cust_dh_processamento = cust_dh_processamento;
	}
	public String getCust_ds_nomearquivo() {
		return cust_ds_nomearquivo;
	}
	public void setCust_ds_nomearquivo(String cust_ds_nomearquivo) {
		this.cust_ds_nomearquivo = cust_ds_nomearquivo;
	}

}
