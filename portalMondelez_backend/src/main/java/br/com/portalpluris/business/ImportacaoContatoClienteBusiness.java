package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.catapi.util.UtilParseToInt;
import br.com.portalpluris.bean.CsCdtbContatoclienteCoclBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.dao.CsCdtbContatoclienteCoclDao;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.DAOLog;

public class ImportacaoContatoClienteBusiness {

	
	public void importadorContatoCliente(){
		
		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("*** INICIO DO SERVIÇO DE IMPORTAÇÃO DE ARQUIVO CONTATO CLIENTE ******");

		// DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ImportacaoLeadTimeBusiness");
		logsBean.setLogsDsMetodo("importadorArquivoLeadTime");
		logsBean.setLogsInErro("N");
		
		UtilMoveArquivo utilMove = new UtilMoveArquivo();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		UtilParseToInt parseInt = new UtilParseToInt();
		CsCdtbContatoclienteCoclBean contatoCliBean = new CsCdtbContatoclienteCoclBean();
		CsCdtbContatoclienteCoclDao contatoCliDao = new CsCdtbContatoclienteCoclDao();
		
		String diretorio = confDao.slctConf("conf.importacao.contato.cliente");
		String diretorioBKP = confDao.slctConf("conf.importacao.contato.cliente.bkp");		
		String diretorioErro = confDao.slctConf("conf.importacao.contato.cliente.erro");
		
		File arquivoProcessado = null;
		File arquivoProcessadobkp = null;
		File arquivoProcessadoerro = new File(diretorioErro+ "/");
		
		
//		String diretorio = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond";
//		String diretorioBKP = "C:\\Users\\bcamargo\\Desktop\\bkp\\importação\\mond\\bkp";	
//		
		int linha = 0;
	
		
		try {

			String conteudo = "";
			File arquivo = new File(diretorio);
			String[] arq = arquivo.list();

			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String dhImportacao = sdf.format(date);
			
			if(arq != null){

			for (int i = 0; i < arq.length; i++) {

				 arquivoProcessado = new File(diretorio + "/" + arq[i]);
				 arquivoProcessadobkp = new File(diretorioBKP + "/" + arq[i]);
				
				String nmArquivo = arq[i] + "_" + dhImportacao;
				
				if (arquivoProcessado.getName().toUpperCase().endsWith(".XLSX")) {

					log.info("*** INICIO DA LEITURA DO ARQUIVO " + arquivoProcessado.getName() + "******");
					FileInputStream file = new FileInputStream(arquivoProcessado);	
					
					
					Workbook wb = WorkbookFactory.create(file);
					Sheet sheet = wb.getSheetAt(0);		
					
					for (int j = 1; j < sheet.getLastRowNum(); j++) {
						
						linha = j;
					
					Cell celidcliente = sheet.getRow(j).getCell(1);
					celidcliente.setCellType(CellType.STRING);	
					String idCliente = celidcliente.toString();;		

						contatoCliDao.insertContatoCliente(sheet,dhImportacao,nmArquivo,contatoCliBean,idCliente,j);
					}

					boolean movido = utilMove.moveArquivo(arquivoProcessado,arquivoProcessadobkp);
					
					if(movido){
						log.info("Arquivo movido com sucesso");
					}else{
						log.info("Não foi possivel mover o arquivo");
					}
	
					
				}
				
			}
		}

			} catch (Exception e) {
				log.info("*** ERRO NO SERVIÇO DE IMPORTAÇÃO CONTATO CLIENTE" + e + " ******");
				log.info("*** LINHA:" + linha + "************");
				logsBean.setLogsInErro("S");
				logsBean.setLogsTxErro( "LINHA DE ERRO:" +linha +e.getMessage());
				utilMove.moveArquivo(arquivoProcessado,arquivoProcessadoerro);

			} finally {
				daoLog.createLog(logsBean);
				log.info("**** FIM SERVIÇO DE IMPORTAÇÃO CONTATO CLIENTE ****");
			}
	}

	
}
