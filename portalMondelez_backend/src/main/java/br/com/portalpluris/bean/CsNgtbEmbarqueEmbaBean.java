package br.com.portalpluris.bean;

import java.sql.Timestamp;

public class CsNgtbEmbarqueEmbaBean {

	int embaIdCdEmbarque;
	Timestamp embaDhRegistro;
	int embaIdCdShipment;
	int embaIdCdUsuario;
	Timestamp embaDhAgendamento;
	Timestamp embaDhReagendamentoI;
	Timestamp embaDhReagendamentoII;
	Timestamp embaDhReagendamentoIII;
	int embaIdCdStatusagendamento;
	String embaDsSenhatransportadora;
	String embaInCutoff;
	String embaDsPeriodo;
	Timestamp embaDhAtualizacao;
	int embaIdCdUsuarioatualizacao;

	//DANILLO *** 29/07/2021 *** INCLUSÃO DE PERIODO BASEADO NA DATA DE EMISSÃO DA NOTA FISCAL
	private String emba_in_possuinf;

	public int getEmbaIdCdEmbarque() {
		return embaIdCdEmbarque;
	}

	public void setEmbaIdCdEmbarque(int embaIdCdEmbarque) {
		this.embaIdCdEmbarque = embaIdCdEmbarque;
	}

	public Timestamp getEmbaDhRegistro() {
		return embaDhRegistro;
	}

	public void setEmbaDhRegistro(Timestamp embaDhRegistro) {
		this.embaDhRegistro = embaDhRegistro;
	}

	public int getEmbaIdCdShipment() {
		return embaIdCdShipment;
	}

	public void setEmbaIdCdShipment(int embaIdCdShipment) {
		this.embaIdCdShipment = embaIdCdShipment;
	}

	public int getEmbaIdCdUsuario() {
		return embaIdCdUsuario;
	}

	public void setEmbaIdCdUsuario(int embaIdCdUsuario) {
		this.embaIdCdUsuario = embaIdCdUsuario;
	}

	public Timestamp getEmbaDhAgendamento() {
		return embaDhAgendamento;
	}

	public void setEmbaDhAgendamento(Timestamp embaDhAgendamento) {
		this.embaDhAgendamento = embaDhAgendamento;
	}

	public Timestamp getEmbaDhReagendamentoI() {
		return embaDhReagendamentoI;
	}

	public void setEmbaDhReagendamentoI(Timestamp embaDhReagendamentoI) {
		this.embaDhReagendamentoI = embaDhReagendamentoI;
	}

	public Timestamp getEmbaDhReagendamentoII() {
		return embaDhReagendamentoII;
	}

	public void setEmbaDhReagendamentoII(Timestamp embaDhReagendamentoII) {
		this.embaDhReagendamentoII = embaDhReagendamentoII;
	}

	public Timestamp getEmbaDhReagendamentoIII() {
		return embaDhReagendamentoIII;
	}

	public void setEmbaDhReagendamentoIII(Timestamp embaDhReagendamentoIII) {
		this.embaDhReagendamentoIII = embaDhReagendamentoIII;
	}

	public int getEmbaIdCdStatusagendamento() {
		return embaIdCdStatusagendamento;
	}

	public void setEmbaIdCdStatusagendamento(int embaIdCdStatusagendamento) {
		this.embaIdCdStatusagendamento = embaIdCdStatusagendamento;
	}

	public String getEmbaDsSenhatransportadora() {
		return embaDsSenhatransportadora;
	}

	public void setEmbaDsSenhatransportadora(String embaDsSenhatransportadora) {
		this.embaDsSenhatransportadora = embaDsSenhatransportadora;
	}

	public String getEmbaInCutoff() {
		return embaInCutoff;
	}

	public void setEmbaInCutoff(String embaInCutoff) {
		this.embaInCutoff = embaInCutoff;
	}

	public String getEmbaDsPeriodo() {
		return embaDsPeriodo;
	}

	public void setEmbaDsPeriodo(String embaDsPeriodo) {
		this.embaDsPeriodo = embaDsPeriodo;
	}

	public Timestamp getEmbaDhAtualizacao() {
		return embaDhAtualizacao;
	}

	public void setEmbaDhAtualizacao(Timestamp embaDhAtualizacao) {
		this.embaDhAtualizacao = embaDhAtualizacao;
	}

	public int getEmbaIdCdUsuarioatualizacao() {
		return embaIdCdUsuarioatualizacao;
	}

	public void setEmbaIdCdUsuarioatualizacao(int embaIdCdUsuarioatualizacao) {
		this.embaIdCdUsuarioatualizacao = embaIdCdUsuarioatualizacao;
	}

	public String getEmba_in_possuinf() {
		return emba_in_possuinf;
	}

	public void setEmba_in_possuinf(String emba_in_possuinf) {
		this.emba_in_possuinf = emba_in_possuinf;
	}
	

}
