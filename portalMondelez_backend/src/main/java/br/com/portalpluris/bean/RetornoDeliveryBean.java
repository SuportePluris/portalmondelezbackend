package br.com.portalpluris.bean;

public class RetornoDeliveryBean {
	
	
	private String emba_ds_planta;
	private String clie_ds_nomecliente;
	private String clie_id_cd_codigocliente;
	private String tran_ds_codigotransportadora;
	private String tran_nm_nometransportadora;
	 
	 private String skud_id_cd_shipment;
	 private String skud_id_cd_delivery;
	 private String nofi_nr_notafiscal;
	 private String skud_ds_pesobruto;
	 private String skud_ds_pesoliquido;
	 private String skud_ds_volumecubico;
	 private String skud_ds_valor;
	 private String skud_nr_quantidade;
	 private String skud_in_cancelado;
	 private String skud_ds_pedidomondelez;
	 private String skud_ds_pedidocliente;
	 
	 

	 private String nofi_ds_serienota;
	 

	 
	 
	 
	 
	 
	
	public String getSkud_ds_pedidocliente() {
		return skud_ds_pedidocliente;
	}
	public void setSkud_ds_pedidocliente(String skud_ds_pedidocliente) {
		this.skud_ds_pedidocliente = skud_ds_pedidocliente;
	}
	public String getNofi_ds_serienota() {
		return nofi_ds_serienota;
	}
	public void setNofi_ds_serienota(String nofi_ds_serienota) {
		this.nofi_ds_serienota = nofi_ds_serienota;
	}
	public String getTran_ds_codigotransportadora() {
		return tran_ds_codigotransportadora;
	}
	public void setTran_ds_codigotransportadora(String tran_ds_codigotransportadora) {
		this.tran_ds_codigotransportadora = tran_ds_codigotransportadora;
	}
	public String getTran_nm_nometransportadora() {
		return tran_nm_nometransportadora;
	}
	public void setTran_nm_nometransportadora(String tran_nm_nometransportadora) {
		this.tran_nm_nometransportadora = tran_nm_nometransportadora;
	}
	public String getClie_id_cd_codigocliente() {
		return clie_id_cd_codigocliente;
	}
	public void setClie_id_cd_codigocliente(String clie_id_cd_codigocliente) {
		this.clie_id_cd_codigocliente = clie_id_cd_codigocliente;
	}
	public String getEmba_ds_planta() {
		return emba_ds_planta;
	}
	public void setEmba_ds_planta(String emba_ds_planta) {
		this.emba_ds_planta = emba_ds_planta;
	}
	public String getClie_ds_nomecliente() {
		return clie_ds_nomecliente;
	}
	public void setClie_ds_nomecliente(String clie_ds_nomecliente) {
		this.clie_ds_nomecliente = clie_ds_nomecliente;
	}
	public String getSkud_ds_pedidomondelez() {
		return skud_ds_pedidomondelez;
	}
	public void setSkud_ds_pedidomondelez(String skud_ds_pedidomondelez) {
		this.skud_ds_pedidomondelez = skud_ds_pedidomondelez;
	}
	public String getSkud_in_cancelado() {
		return skud_in_cancelado;
	}
	public void setSkud_in_cancelado(String skud_in_cancelado) {
		this.skud_in_cancelado = skud_in_cancelado;
	}
	public String getNofi_nr_notafiscal() {
		return nofi_nr_notafiscal;
	}
	public void setNofi_nr_notafiscal(String nofi_nr_notafiscal) {
		this.nofi_nr_notafiscal = nofi_nr_notafiscal;
	}
	
	public String getSkud_nr_quantidade() {
		return skud_nr_quantidade;
	}
	public void setSkud_nr_quantidade(String skud_nr_quantidade) {
		this.skud_nr_quantidade = skud_nr_quantidade;
	}
	public String getSkud_id_cd_shipment() {
		return skud_id_cd_shipment;
	}
	public void setSkud_id_cd_shipment(String skud_id_cd_shipment) {
		this.skud_id_cd_shipment = skud_id_cd_shipment;
	}
	public String getSkud_id_cd_delivery() {
		return skud_id_cd_delivery;
	}
	public void setSkud_id_cd_delivery(String skud_id_cd_delivery) {
		this.skud_id_cd_delivery = skud_id_cd_delivery;
	}
	public String getSkud_ds_pesobruto() {
		return skud_ds_pesobruto;
	}
	public void setSkud_ds_pesobruto(String skud_ds_pesobruto) {
		this.skud_ds_pesobruto = skud_ds_pesobruto;
	}
	public String getSkud_ds_pesoliquido() {
		return skud_ds_pesoliquido;
	}
	public void setSkud_ds_pesoliquido(String skud_ds_pesoliquido) {
		this.skud_ds_pesoliquido = skud_ds_pesoliquido;
	}
	public String getSkud_ds_volumecubico() {
		return skud_ds_volumecubico;
	}
	public void setSkud_ds_volumecubico(String skud_ds_volumecubico) {
		this.skud_ds_volumecubico = skud_ds_volumecubico;
	}
	public String getSkud_ds_valor() {
		return skud_ds_valor;
	}
	public void setSkud_ds_valor(String skud_ds_valor) {
		this.skud_ds_valor = skud_ds_valor;
	}
	
	
	 
	 
	

}
