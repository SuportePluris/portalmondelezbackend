package br.com.portalpluris.bean;

public class FuncionarioBean {

	long id = 0;
	String nome;
	String primeiroAcesso;
	String user;
	String password;
	String logado;
	String ativo;
	long idFuncResp;
	String permissao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPrimeiroAcesso() {
		return primeiroAcesso;
	}

	public void setPrimeiroAcesso(String primeiroAcesso) {
		this.primeiroAcesso = primeiroAcesso;
	}

	public String getLogado() {
		return logado;
	}

	public void setLogado(String logado) {
		this.logado = logado;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	public long getIdFuncResp() {
		return idFuncResp;
	}

	public void setIdFuncResp(long idFuncResp) {
		this.idFuncResp = idFuncResp;
	}

	public String getPermissao() {
		return permissao;
	}

	public void setPermissao(String permissao) {
		this.permissao = permissao;
	}

}
