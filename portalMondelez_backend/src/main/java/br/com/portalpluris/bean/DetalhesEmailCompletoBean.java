package br.com.portalpluris.bean;

import java.util.ArrayList;
import java.util.List;

public class DetalhesEmailCompletoBean {
	
	private String layoutEmail;
	
	private RetornoShipmentBean detalhesShip = new RetornoShipmentBean(); 
	private List<EmbarqueAgendamentoBean> listDetalhesDoEmbarque = new ArrayList<EmbarqueAgendamentoBean>();
	private List<RetornoDeliveryBean> listDetalhesDoDelivery = new ArrayList<RetornoDeliveryBean>();
	
	public String getLayoutEmail() {
		return layoutEmail;
	}
	public void setLayoutEmail(String layoutEmail) {
		this.layoutEmail = layoutEmail;
	}
	public RetornoShipmentBean getDetalhesShip() {
		return detalhesShip;
	}
	public void setDetalhesShip(RetornoShipmentBean detalhesShip) {
		this.detalhesShip = detalhesShip;
	}
	public List<EmbarqueAgendamentoBean> getListDetalhesDoEmbarque() {
		return listDetalhesDoEmbarque;
	}
	public void setListDetalhesDoEmbarque(
			List<EmbarqueAgendamentoBean> listDetalhesDoEmbarque) {
		this.listDetalhesDoEmbarque = listDetalhesDoEmbarque;
	}
	public List<RetornoDeliveryBean> getListDetalhesDoDelivery() {
		return listDetalhesDoDelivery;
	}
	public void setListDetalhesDoDelivery(
			List<RetornoDeliveryBean> listDetalhesDoDelivery) {
		this.listDetalhesDoDelivery = listDetalhesDoDelivery;
	}
	
}
