package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;
import br.com.portalpluris.bean.ImportacaoCancelarPedidoBean;
import br.com.portalpluris.bean.ImportacaoCodTranspBean;

public class ImportacaoPortalDao {
	
	/**
	 * Metodo responsavel atualizar as informações de Cod. Transp/Status/Flag Transportadora Confirmada e realizar o backup da tabela EMBA
	 * @author Caio Fernandes
	 * @param List<ImportacaoCodTranspBean>
	 * @throws SQLException
	 * */
	public void updateCodTransp(List<ImportacaoCodTranspBean> lstCodTranspBean) throws SQLException {
		
		// UPDATE "EMBA_DS_COD_TRANSPORTADORA" ATRAVÉS DO ID SHIPMENT
		// UPDATE "EMBA_ID_CD_STATUS" = 17
		// UPDATE "EMBA_IN_TRANSPORTADORACONFIRMADA" = CONFIRMADO
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[updateCodTransp] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE COD. TRANSP.		
			for(int i = 0; i < lstCodTranspBean.size(); i++) {
				
				String idShipment = lstCodTranspBean.get(i).getId_shipment();
				String codTransp = lstCodTranspBean.get(i).getCod_transp();	
				
				// CRIAR BKP EMBA
				log.info("[updateCodTransp] - CRIANDO REGISTRO DE BACKUP EMBA ");
				CsNgtbEmbarqueEmbaDao embarqueEmbaDao = new CsNgtbEmbarqueEmbaDao();
				embarqueEmbaDao.createBkpEmbarqueByIdShipment(idShipment);
				log.info("[updateCodTransp] - REGISTRO DE BACKUP CRIADO COM SUCESSO ");
				
				// UPDATE EMBA
				String cSQL = "";
				cSQL += " UPDATE ";
				cSQL += " 	CS_NGTB_EMBARQUEAGENDAMENTO_EMBA ";
				cSQL += " SET ";
				cSQL += " 	EMBA_DS_COD_TRANSPORTADORA = ?,  ";
				cSQL += " 	EMBA_ID_CD_STATUS = 17, ";
				cSQL += " 	EMBA_IN_TRANSPORTADORACONFIRMADA = 'CONFIRMADO' ";
				cSQL += " WHERE  ";
				cSQL += " 	EMBA_ID_CD_SHIPMENT = ? ";
				
				PreparedStatement pst = conn.prepareStatement(cSQL);
				pst.setString(1, codTransp);
				pst.setString(2, idShipment);
				pst.execute();
				
				log.info("[updateCodTransp] - UPDATE REALIZADO COM SUCESSO ");
							
				
			}
			
		} else {
			log.info("[updateCodTransp] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updateCodTransp] - FIM ");
		
	}
	
	/**
	 * Metodo responsavel atualizar flag do pedido recebido para "S"
	 * @author Caio Fernandes
	 * @param List<ImportacaoPedidoCanceladoBean>
	 * @throws SQLException
	 * */
	public void updatePedidoCancelado(List<ImportacaoCancelarPedidoBean> lstPedidoCancelado) throws SQLException {
		
		// UPDATE NO CAMPO FLAG PEDIDO CANCELADO
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		
		log.info("[updatePedidoCancelado] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE COD. TRANSP.		
			for(int i = 0; i < lstPedidoCancelado.size(); i++) {
				
				String idPedido = lstPedidoCancelado.get(i).getPedido_mondelez();
				
				// UPDATE EMBA
				String cSQL = "";
				cSQL += " UPDATE ";
				cSQL += " 	CS_CDTB_SKUDELIVERY_SKUD ";
				cSQL += " SET ";
				cSQL += " 	SKUD_IN_CANCELADO = 'S' ";
				cSQL += " WHERE  ";
				cSQL += " 	SKUD_DS_PEDIDOMONDELEZ = ? ";
				
				PreparedStatement pst = conn.prepareStatement(cSQL);
				pst.setString(1, idPedido);
				pst.execute();
				
				log.info("[updatePedidoCancelado] - UPDATE REALIZADO COM SUCESSO ");				
			
			}
			
		} else {
			log.info("[updatePedidoCancelado] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		log.info("[updatePedidoCancelado] - FIM ");
		
	}

}
