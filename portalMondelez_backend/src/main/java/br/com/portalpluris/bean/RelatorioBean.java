package br.com.portalpluris.bean;

public class RelatorioBean {
	
	
	private String emba_id_cd_embarque; //ok
	private String emba_ds_tipoagendamento; //ok
	private String skud_ds_pedidomondelez; //ok
	private String skud_ds_pedidocliente; //ok
	private String skud_id_cd_delivery; //ok
	private String skud_ds_tipovenda; //ok
	private String nofi_nr_notafiscal; //ok
	private String nofi_ds_serienota; //ok
	private String nofi_dh_emissaonota; //ok
	private String emba_ds_cod_transportadora; //ok
	private String tran_nm_nometransportadora; //ok
	private String emba_in_transportadoraconfirmada; //ok
	private String emba_ds_tipocarga; //ok
	private String emba_ds_veiculo; //ok
	private String emba_ds_tipoveiculo; //ok
	private String emba_id_cd_shipment; //ok
	private String emba_dh_shipment; //ok
	private String emba_id_cd_codcliente; //ok
	private String clie_ds_cnpj; //ok
	private String clie_ds_nomecliente; //ok
	private String clie_ds_cidade; //ok
	private String clie_ds_uf; //ok
	private String emba_ds_planta; //ok
	private String canal;  //ok
	private String regional; //verificar de onde iremos trazer a reginal
	private String skud_ds_pesoliquido;  //ok
	private String skud_ds_volumecubico; //ok
	private String skud_ds_valor;  //ok
	private String skud_nr_quantidade;  //ok
	private String dias_leadtime; //ok
	private String emba_dh_leadtime; //ok
	private String calc_prazo_realizar_agendamento; // campo calculado
	private int calc_aging_agendamento; // campo calculado
	private String calc_ontime_scheduling; // campo calculado
	private String calc_primeiro_contato; // campo calculado
	private String stat_ds_status;  //ok
	private String calc_input_dhl_agendamento; // campo calculado
	private String data_agendamento; //DAAG_DH_AGENDAMENTO onde DAAG_DS_TIPOAGENDAMENTO seja = agendamento
	private String senha_agendamento; //Senha de agendamento onde DAAG_DS_TIPOAGENDAMENTO seja = agendamento
	private String data_retificacao_agenda; //DAAG_DH_AGENDAMENTO onde DAAG_DS_TIPOAGENDAMENTO seja = retificacao de agenda
	private String data_solicit_nova_agenda; //DAAG_DH_REGISTRO DE RETIFICAÇÃO
	private String senha_retificacao_agenda; //Senha de agendamento onde DAAG_DS_TIPOAGENDAMENTO seja = retificação de agenda
	private String motivo_retificacao_agenda; //coluna DAAG_DS_MOTIVO onde DAAG_DS_TIPOAGENDAMENTO seja = retificação de agenda
	private String responsavel_retificacao_agenda; //coluna DAAG_DS_RESPONSAVEL onde DAAG_DS_TIPOAGENDAMENTO seja = retificação de agenda
	private String data_reagendamento; //DAAG_DH_AGENDAMENTO onde DAAG_DS_TIPOAGENDAMENTO seja = reagendamento
	private String data_solicit_reagendamento; //DAAG_DH_REGISTRO DE REAGENDAMENTO
	private String senha_reagendamento; //Senha de agendamento onde DAAG_DS_TIPOAGENDAMENTO seja = reagendamento
	private String motivo_reagendamento; //coluna DAAG_DS_MOTIVO onde DAAG_DS_TIPOAGENDAMENTO seja = reagendamento
	private String responsavel_reagendamento; //coluna DAAG_DS_RESPONSAVEL onde DAAG_DS_TIPOAGENDAMENTO seja = reagendamento
	private int calc_aging_reagendamento; //caso tenha um reagendamento, calcular o aging
	private String calc_desvio_leadtime; //campo calculado entre a data do leadtime e a data efetiva da entrega. Caso tenha um reagendamento ou retificação, considerar a data do reagendamento/retificação
	private String foup_motivo_desvio_leadtime; // follow-up de motivo de desvio do leadtime
	private String foup_responsavel_desvio_leadtime; //responsavel do foup de desvio do leadtime
	private String emba_in_cutoff;  //ok
	private String emba_ds_motivocutoff;
	private String emba_ds_responsavelcutoff;
	private String emba_ds_periodo;  //ok
	private String hqcl_ds_gerentenacional;  //ok
	private String hqcl_ds_gerenteregional;  //ok
	private String hqcl_ds_gerentearea; //ok
	private String hqcl_ds_vendedor; //ok
	private String skud_dh_delivery; //ok
	private int tmca;
	
	//INCLUSAO HORA AGENDAMENTO, REAGENDAMENTO, RETIFICAÇÃO DE AGENDA
	private String daag_ds_horaagendamento;
	private String daag_ds_horaretificacaoagenda;
	private String daag_ds_horareagendamento;
	
	//INCLUSAO DE OCORRENCIA E MOTIVO DE OCORRENCIA
	public String dt_ocr_agendamento;
	public String motivo_ocr_agend;
	public String responsavel_pendencia;
	
	
	
	public int getTmca() {
		return tmca;
	}
	public void setTmca(int tmca) {
		this.tmca = tmca;
	}
	public String getDt_ocr_agendamento() {
		return dt_ocr_agendamento;
	}
	public void setDt_ocr_agendamento(String dt_ocr_agendamento) {
		this.dt_ocr_agendamento = dt_ocr_agendamento;
	}
	public String getMotivo_ocr_agend() {
		return motivo_ocr_agend;
	}
	public void setMotivo_ocr_agend(String motivo_ocr_agend) {
		this.motivo_ocr_agend = motivo_ocr_agend;
	}
	public String getResponsavel_pendencia() {
		return responsavel_pendencia;
	}
	public void setResponsavel_pendencia(String responsavel_pendencia) {
		this.responsavel_pendencia = responsavel_pendencia;
	}
	public String getData_solicit_nova_agenda() {
		return data_solicit_nova_agenda;
	}
	public void setData_solicit_nova_agenda(String data_solicit_nova_agenda) {
		this.data_solicit_nova_agenda = data_solicit_nova_agenda;
	}
	public String getData_solicit_reagendamento() {
		return data_solicit_reagendamento;
	}
	public void setData_solicit_reagendamento(String data_solicit_reagendamento) {
		this.data_solicit_reagendamento = data_solicit_reagendamento;
	}
	public String getDaag_ds_horaagendamento() {
		return daag_ds_horaagendamento;
	}
	public void setDaag_ds_horaagendamento(String daag_ds_horaagendamento) {
		this.daag_ds_horaagendamento = daag_ds_horaagendamento;
	}
	public String getDaag_ds_horaretificacaoagenda() {
		return daag_ds_horaretificacaoagenda;
	}
	public void setDaag_ds_horaretificacaoagenda(String daag_ds_horaretificacaoagenda) {
		this.daag_ds_horaretificacaoagenda = daag_ds_horaretificacaoagenda;
	}
	public String getDaag_ds_horareagendamento() {
		return daag_ds_horareagendamento;
	}
	public void setDaag_ds_horareagendamento(String daag_ds_horareagendamento) {
		this.daag_ds_horareagendamento = daag_ds_horareagendamento;
	}
	public String getEmba_id_cd_embarque() {
		return emba_id_cd_embarque;
	}
	public void setEmba_id_cd_embarque(String emba_id_emba_cd_embarque) {
		this.emba_id_cd_embarque = emba_id_emba_cd_embarque;
	}
	public String getEmba_ds_tipoagendamento() {
		return emba_ds_tipoagendamento;
	}
	public void setEmba_ds_tipoagendamento(String emba_ds_tipoagendamento) {
		this.emba_ds_tipoagendamento = emba_ds_tipoagendamento;
	}
	public String getSkud_ds_pedidomondelez() {
		return skud_ds_pedidomondelez;
	}
	public void setSkud_ds_pedidomondelez(String skud_ds_pedidomondelez) {
		this.skud_ds_pedidomondelez = skud_ds_pedidomondelez;
	}
	public String getSkud_id_cd_delivery() {
		return skud_id_cd_delivery;
	}
	public void setSkud_id_cd_delivery(String skud_id_cd_delivery) {
		this.skud_id_cd_delivery = skud_id_cd_delivery;
	}
	public String getSkud_ds_pedidocliente() {
		return skud_ds_pedidocliente;
	}
	public void setSkud_ds_pedidocliente(String skud_ds_pedidocliente) {
		this.skud_ds_pedidocliente = skud_ds_pedidocliente;
	}
	public String getSkud_ds_tipovenda() {
		return skud_ds_tipovenda;
	}
	public void setSkud_ds_tipovenda(String skud_ds_tipovenda) {
		this.skud_ds_tipovenda = skud_ds_tipovenda;
	}
	public String getNofi_nr_notafiscal() {
		return nofi_nr_notafiscal;
	}
	public void setNofi_nr_notafiscal(String nofi_nr_notafiscal) {
		this.nofi_nr_notafiscal = nofi_nr_notafiscal;
	}
	public String getNofi_ds_serienota() {
		return nofi_ds_serienota;
	}
	public void setNofi_ds_serienota(String nofi_ds_serienota) {
		this.nofi_ds_serienota = nofi_ds_serienota;
	}
	public String getNofi_dh_emissaonota() {
		return nofi_dh_emissaonota;
	}
	public void setNofi_dh_emissaonota(String nofi_dh_emissaonota) {
		this.nofi_dh_emissaonota = nofi_dh_emissaonota;
	}
	public String getEmba_ds_cod_transportadora() {
		return emba_ds_cod_transportadora;
	}
	public void setEmba_ds_cod_transportadora(String emba_ds_cod_transportadora) {
		this.emba_ds_cod_transportadora = emba_ds_cod_transportadora;
	}
	public String getTran_nm_nometransportadora() {
		return tran_nm_nometransportadora;
	}
	public void setTran_nm_nometransportadora(String tran_nm_nometransportadora) {
		this.tran_nm_nometransportadora = tran_nm_nometransportadora;
	}
	public String getEmba_in_transportadoraconfirmada() {
		return emba_in_transportadoraconfirmada;
	}
	public void setEmba_in_transportadoraconfirmada(String emba_in_transportadoraconfirmada) {
		this.emba_in_transportadoraconfirmada = emba_in_transportadoraconfirmada;
	}
	public String getEmba_ds_tipocarga() {
		return emba_ds_tipocarga;
	}
	public void setEmba_ds_tipocarga(String emba_ds_tipocarga) {
		this.emba_ds_tipocarga = emba_ds_tipocarga;
	}
	public String getEmba_ds_veiculo() {
		return emba_ds_veiculo;
	}
	public void setEmba_ds_veiculo(String emba_ds_veiculo) {
		this.emba_ds_veiculo = emba_ds_veiculo;
	}
	public String getEmba_ds_tipoveiculo() {
		return emba_ds_tipoveiculo;
	}
	public void setEmba_ds_tipoveiculo(String emba_ds_tipoveiculo) {
		this.emba_ds_tipoveiculo = emba_ds_tipoveiculo;
	}
	public String getEmba_id_cd_shipment() {
		return emba_id_cd_shipment;
	}
	public void setEmba_id_cd_shipment(String emba_id_cd_shipment) {
		this.emba_id_cd_shipment = emba_id_cd_shipment;
	}
	public String getEmba_dh_shipment() {
		return emba_dh_shipment;
	}
	public void setEmba_dh_shipment(String emba_dh_shipment) {
		this.emba_dh_shipment = emba_dh_shipment;
	}
	public String getEmba_id_cd_codcliente() {
		return emba_id_cd_codcliente;
	}
	public void setEmba_id_cd_codcliente(String emba_id_cd_codcliente) {
		this.emba_id_cd_codcliente = emba_id_cd_codcliente;
	}
	public String getClie_ds_cnpj() {
		return clie_ds_cnpj;
	}
	public void setClie_ds_cnpj(String clie_ds_cnpj) {
		this.clie_ds_cnpj = clie_ds_cnpj;
	}
	public String getClie_ds_nomecliente() {
		return clie_ds_nomecliente;
	}
	public void setClie_ds_nomecliente(String clie_ds_nomecliente) {
		this.clie_ds_nomecliente = clie_ds_nomecliente;
	}
	public String getClie_ds_cidade() {
		return clie_ds_cidade;
	}
	public void setClie_ds_cidade(String clie_ds_cidade) {
		this.clie_ds_cidade = clie_ds_cidade;
	}
	public String getClie_ds_uf() {
		return clie_ds_uf;
	}
	public void setClie_ds_uf(String clie_ds_uf) {
		this.clie_ds_uf = clie_ds_uf;
	}
	public String getEmba_ds_planta() {
		return emba_ds_planta;
	}
	public void setEmba_ds_planta(String emba_ds_planta) {
		this.emba_ds_planta = emba_ds_planta;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getRegional() {
		return regional;
	}
	public void setRegional(String regional) {
		this.regional = regional;
	}
	public String getSkud_ds_pesoliquido() {
		return skud_ds_pesoliquido;
	}
	public void setSkud_ds_pesoliquido(String skud_ds_pesoliquido) {
		this.skud_ds_pesoliquido = skud_ds_pesoliquido;
	}
	public String getSkud_ds_volumecubico() {
		return skud_ds_volumecubico;
	}
	public void setSkud_ds_volumecubico(String skud_ds_volumecubico) {
		this.skud_ds_volumecubico = skud_ds_volumecubico;
	}
	public String getSkud_ds_valor() {
		return skud_ds_valor;
	}
	public void setSkud_ds_valor(String skud_ds_valor) {
		this.skud_ds_valor = skud_ds_valor;
	}
	public String getSkud_nr_quantidade() {
		return skud_nr_quantidade;
	}
	public void setSkud_nr_quantidade(String skud_nr_quantidade) {
		this.skud_nr_quantidade = skud_nr_quantidade;
	}
	public String getDias_leadtime() {
		return dias_leadtime;
	}
	public void setDias_leadtime(String dias_leadtime) {
		this.dias_leadtime = dias_leadtime;
	}
	public String getEmba_dh_leadtime() {
		return emba_dh_leadtime;
	}
	public void setEmba_dh_leadtime(String emba_dh_leadtime) {
		this.emba_dh_leadtime = emba_dh_leadtime;
	}
	public String getCalc_prazo_realizar_agendamento() {
		return calc_prazo_realizar_agendamento;
	}
	public void setCalc_prazo_realizar_agendamento(String calc_prazo_realizar_agendamento) {
		this.calc_prazo_realizar_agendamento = calc_prazo_realizar_agendamento;
	}
	public int getCalc_aging_agendamento() {
		return calc_aging_agendamento;
	}
	public void setCalc_aging_agendamento(int calc_aging_agendamento) {
		this.calc_aging_agendamento = calc_aging_agendamento;
	}
	public String getCalc_ontime_scheduling() {
		return calc_ontime_scheduling;
	}
	public void setCalc_ontime_scheduling(String calc_ontime_scheduling) {
		this.calc_ontime_scheduling = calc_ontime_scheduling;
	}
	public String getCalc_primeiro_contato() {
		return calc_primeiro_contato;
	}
	public void setCalc_primeiro_contato(String calc_primeiro_contato) {
		this.calc_primeiro_contato = calc_primeiro_contato;
	}
	public String getStat_ds_status() {
		return stat_ds_status;
	}
	public void setStat_ds_status(String stat_ds_status) {
		this.stat_ds_status = stat_ds_status;
	}
	public String getCalc_input_dhl_agendamento() {
		return calc_input_dhl_agendamento;
	}
	public void setCalc_input_dhl_agendamento(String calc_input_dhl_agendamento) {
		this.calc_input_dhl_agendamento = calc_input_dhl_agendamento;
	}
	public String getData_agendamento() {
		return data_agendamento;
	}
	public void setData_agendamento(String data_agendamento) {
		this.data_agendamento = data_agendamento;
	}
	public String getSenha_agendamento() {
		return senha_agendamento;
	}
	public void setSenha_agendamento(String senha_agendamento) {
		this.senha_agendamento = senha_agendamento;
	}
	public String getData_retificacao_agenda() {
		return data_retificacao_agenda;
	}
	public void setData_retificacao_agenda(String data_retificacao_agenda) {
		this.data_retificacao_agenda = data_retificacao_agenda;
	}
	public String getSenha_retificacao_agenda() {
		return senha_retificacao_agenda;
	}
	public void setSenha_retificacao_agenda(String senha_retificacao_agenda) {
		this.senha_retificacao_agenda = senha_retificacao_agenda;
	}
	public String getMotivo_retificacao_agenda() {
		return motivo_retificacao_agenda;
	}
	public void setMotivo_retificacao_agenda(String motivo_retificacao_agenda) {
		this.motivo_retificacao_agenda = motivo_retificacao_agenda;
	}
	public String getResponsavel_retificacao_agenda() {
		return responsavel_retificacao_agenda;
	}
	public void setResponsavel_retificacao_agenda(String responsavel_retificacao_agenda) {
		this.responsavel_retificacao_agenda = responsavel_retificacao_agenda;
	}
	public String getData_reagendamento() {
		return data_reagendamento;
	}
	public void setData_reagendamento(String data_reagendamento) {
		this.data_reagendamento = data_reagendamento;
	}
	public String getSenha_reagendamento() {
		return senha_reagendamento;
	}
	public void setSenha_reagendamento(String senha_reagendamento) {
		this.senha_reagendamento = senha_reagendamento;
	}
	public String getMotivo_reagendamento() {
		return motivo_reagendamento;
	}
	public void setMotivo_reagendamento(String motivo_reagendamento) {
		this.motivo_reagendamento = motivo_reagendamento;
	}
	public String getResponsavel_reagendamento() {
		return responsavel_reagendamento;
	}
	public void setResponsavel_reagendamento(String responsavel_reagendamento) {
		this.responsavel_reagendamento = responsavel_reagendamento;
	}
	public int getCalc_aging_reagendamento() {
		return calc_aging_reagendamento;
	}
	public void setCalc_aging_reagendamento(int calc_aging_reagendamento) {
		this.calc_aging_reagendamento = calc_aging_reagendamento;
	}
	public String getCalc_desvio_leadtime() {
		return calc_desvio_leadtime;
	}
	public void setCalc_desvio_leadtime(String calc_desvio_leadtime) {
		this.calc_desvio_leadtime = calc_desvio_leadtime;
	}
	public String getFoup_motivo_desvio_leadtime() {
		return foup_motivo_desvio_leadtime;
	}
	public void setFoup_motivo_desvio_leadtime(String foup_motivo_desvio_leadtime) {
		this.foup_motivo_desvio_leadtime = foup_motivo_desvio_leadtime;
	}
	public String getFoup_responsavel_desvio_leadtime() {
		return foup_responsavel_desvio_leadtime;
	}
	public void setFoup_responsavel_desvio_leadtime(String foup_responsavel_desvio_leadtime) {
		this.foup_responsavel_desvio_leadtime = foup_responsavel_desvio_leadtime;
	}
	public String getEmba_in_cutoff() {
		return emba_in_cutoff;
	}
	public void setEmba_in_cutoff(String emba_in_cutoff) {
		this.emba_in_cutoff = emba_in_cutoff;
	}
	public String getEmba_ds_motivocutoff() {
		return emba_ds_motivocutoff;
	}
	public void setEmba_ds_motivocutoff(String emba_ds_motivocutoff) {
		this.emba_ds_motivocutoff = emba_ds_motivocutoff;
	}
	public String getEmba_ds_responsavelcutoff() {
		return emba_ds_responsavelcutoff;
	}
	public void setEmba_ds_responsavelcutoff(String emba_ds_responsavelcutoff) {
		this.emba_ds_responsavelcutoff = emba_ds_responsavelcutoff;
	}
	public String getEmba_ds_periodo() {
		return emba_ds_periodo;
	}
	public void setEmba_ds_periodo(String emba_ds_periodo) {
		this.emba_ds_periodo = emba_ds_periodo;
	}
	public String getHqcl_ds_gerentenacional() {
		return hqcl_ds_gerentenacional;
	}
	public void setHqcl_ds_gerentenacional(String hqcl_ds_gerentenacional) {
		this.hqcl_ds_gerentenacional = hqcl_ds_gerentenacional;
	}
	public String getHqcl_ds_gerenteregional() {
		return hqcl_ds_gerenteregional;
	}
	public void setHqcl_ds_gerenteregional(String hqcl_ds_gerenteregional) {
		this.hqcl_ds_gerenteregional = hqcl_ds_gerenteregional;
	}
	public String getHqcl_ds_gerentearea() {
		return hqcl_ds_gerentearea;
	}
	public void setHqcl_ds_gerentearea(String hqcl_ds_gerentearea) {
		this.hqcl_ds_gerentearea = hqcl_ds_gerentearea;
	}
	public String getHqcl_ds_vendedor() {
		return hqcl_ds_vendedor;
	}
	public void setHqcl_ds_vendedor(String hqcl_ds_vendedor) {
		this.hqcl_ds_vendedor = hqcl_ds_vendedor;
	}
	public String getSkud_dh_delivery() {
		return skud_dh_delivery;
	}
	public void setSkud_dh_delivery(String skud_dh_delivery) {
		this.skud_dh_delivery = skud_dh_delivery;
	}
	
	
}
