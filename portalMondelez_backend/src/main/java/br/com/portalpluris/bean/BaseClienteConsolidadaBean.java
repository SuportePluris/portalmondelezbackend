package br.com.portalpluris.bean;

import java.util.ArrayList;
import java.util.List;

public class BaseClienteConsolidadaBean {

	public String clie_id_cd_cliente;
    public String clie_ds_nomecliente;
    public String clie_id_cd_codigocliente;
    public String clie_ds_cnpj;
    public String clie_dh_registro;
    public String clie_in_inativo;
    public String clie_id_cd_usuarioatualizacao;
    public String clie_dh_atualizacao;
    public String clie_ds_pa;
    public String clie_ds_cidade;
    public String clie_ds_uf;
    public String clie_ds_regiao;
    public String clie_ds_agrupamento;
    public String clie_ds_endereco;
    public String clie_ds_cep;
    public String clie_ds_nfobrigatoria;
    public String clie_ds_agendamentoobrigatorio;
    public String clie_ds_diasrecebimento;
    public String clie_ds_horariorecebimento;
    public String clie_ds_veiculodedicado;
    public String clie_ds_taxadescarga;
    public String clie_ds_fefoespecial;
    public String clie_ds_paletizacaoespecial;
    public String clie_ds_paletizacaomondelez;
    public String clie_ds_tipopaletizacao;
    public String clie_ds_codveiculomax;
    public String clie_ds_tipoveiculomax;
    public String clie_ds_pesoveiculo;
    public String clie_ds_temagendamento;
    public String clie_ds_tipoagendamento;
    public String clie_ds_esperacliente;
    public String clie_ds_pagataxaveiculodedicado;
    public String clie_ds_tipodescarga;
    public String clie_ds_clienteexigepalete;
    public String clie_ds_alturpalete;
    public String clie_cd_veiculomax;
    public String clie_ds_datarevisao;
    public String clie_ds_resprevisao;
    public String clie_ds_observacao;
    public String cli_ds_restricaocirculacao;
    public String clie_ds_tiporestricao;
    public String clie_ds_horariorestricao;
    public String clie_nm_arquivoprocessado;
    
    public List<CsCdtbContatoclienteCoclBean> csCdtbContatoclienteCoclBean = new ArrayList<CsCdtbContatoclienteCoclBean>(); 
    
    
    
    
	
	public List<CsCdtbContatoclienteCoclBean> getCsCdtbContatoclienteCoclBean() {
		return csCdtbContatoclienteCoclBean;
	}
	public void setCsCdtbContatoclienteCoclBean(List<CsCdtbContatoclienteCoclBean> csCdtbContatoclienteCoclBean) {
		this.csCdtbContatoclienteCoclBean = csCdtbContatoclienteCoclBean;
	}
	public String getClie_id_cd_cliente() {
		return clie_id_cd_cliente;
	}
	public void setClie_id_cd_cliente(String clie_id_cd_cliente) {
		this.clie_id_cd_cliente = clie_id_cd_cliente;
	}
	public String getClie_ds_nomecliente() {
		return clie_ds_nomecliente;
	}
	public void setClie_ds_nomecliente(String clie_ds_nomecliente) {
		this.clie_ds_nomecliente = clie_ds_nomecliente;
	}
	public String getClie_id_cd_codigocliente() {
		return clie_id_cd_codigocliente;
	}
	public void setClie_id_cd_codigocliente(String clie_id_cd_codigocliente) {
		this.clie_id_cd_codigocliente = clie_id_cd_codigocliente;
	}
	public String getClie_ds_cnpj() {
		return clie_ds_cnpj;
	}
	public void setClie_ds_cnpj(String clie_ds_cnpj) {
		this.clie_ds_cnpj = clie_ds_cnpj;
	}
	public String getClie_dh_registro() {
		return clie_dh_registro;
	}
	public void setClie_dh_registro(String clie_dh_registro) {
		this.clie_dh_registro = clie_dh_registro;
	}
	public String getClie_in_inativo() {
		return clie_in_inativo;
	}
	public void setClie_in_inativo(String clie_in_inativo) {
		this.clie_in_inativo = clie_in_inativo;
	}
	public String getClie_id_cd_usuarioatualizacao() {
		return clie_id_cd_usuarioatualizacao;
	}
	public void setClie_id_cd_usuarioatualizacao(
			String clie_id_cd_usuarioatualizacao) {
		this.clie_id_cd_usuarioatualizacao = clie_id_cd_usuarioatualizacao;
	}
	public String getClie_dh_atualizacao() {
		return clie_dh_atualizacao;
	}
	public void setClie_dh_atualizacao(String clie_dh_atualizacao) {
		this.clie_dh_atualizacao = clie_dh_atualizacao;
	}
	public String getClie_ds_pa() {
		return clie_ds_pa;
	}
	public void setClie_ds_pa(String clie_ds_pa) {
		this.clie_ds_pa = clie_ds_pa;
	}
	public String getClie_ds_cidade() {
		return clie_ds_cidade;
	}
	public void setClie_ds_cidade(String clie_ds_cidade) {
		this.clie_ds_cidade = clie_ds_cidade;
	}
	public String getClie_ds_uf() {
		return clie_ds_uf;
	}
	public void setClie_ds_uf(String clie_ds_uf) {
		this.clie_ds_uf = clie_ds_uf;
	}
	public String getClie_ds_regiao() {
		return clie_ds_regiao;
	}
	public void setClie_ds_regiao(String clie_ds_regiao) {
		this.clie_ds_regiao = clie_ds_regiao;
	}
	public String getClie_ds_agrupamento() {
		return clie_ds_agrupamento;
	}
	public void setClie_ds_agrupamento(String clie_ds_agrupamento) {
		this.clie_ds_agrupamento = clie_ds_agrupamento;
	}
	public String getClie_ds_endereco() {
		return clie_ds_endereco;
	}
	public void setClie_ds_endereco(String clie_ds_endereco) {
		this.clie_ds_endereco = clie_ds_endereco;
	}
	public String getClie_ds_cep() {
		return clie_ds_cep;
	}
	public void setClie_ds_cep(String clie_ds_cep) {
		this.clie_ds_cep = clie_ds_cep;
	}
	public String getClie_ds_nfobrigatoria() {
		return clie_ds_nfobrigatoria;
	}
	public void setClie_ds_nfobrigatoria(String clie_ds_nfobrigatoria) {
		this.clie_ds_nfobrigatoria = clie_ds_nfobrigatoria;
	}
	public String getClie_ds_agendamentoobrigatorio() {
		return clie_ds_agendamentoobrigatorio;
	}
	public void setClie_ds_agendamentoobrigatorio(
			String clie_ds_agendamentoobrigatorio) {
		this.clie_ds_agendamentoobrigatorio = clie_ds_agendamentoobrigatorio;
	}
	public String getClie_ds_diasrecebimento() {
		return clie_ds_diasrecebimento;
	}
	public void setClie_ds_diasrecebimento(String clie_ds_diasrecebimento) {
		this.clie_ds_diasrecebimento = clie_ds_diasrecebimento;
	}
	public String getClie_ds_horariorecebimento() {
		return clie_ds_horariorecebimento;
	}
	public void setClie_ds_horariorecebimento(String clie_ds_horariorecebimento) {
		this.clie_ds_horariorecebimento = clie_ds_horariorecebimento;
	}
	public String getClie_ds_veiculodedicado() {
		return clie_ds_veiculodedicado;
	}
	public void setClie_ds_veiculodedicado(String clie_ds_veiculodedicado) {
		this.clie_ds_veiculodedicado = clie_ds_veiculodedicado;
	}
	public String getClie_ds_taxadescarga() {
		return clie_ds_taxadescarga;
	}
	public void setClie_ds_taxadescarga(String clie_ds_taxadescarga) {
		this.clie_ds_taxadescarga = clie_ds_taxadescarga;
	}
	public String getClie_ds_fefoespecial() {
		return clie_ds_fefoespecial;
	}
	public void setClie_ds_fefoespecial(String clie_ds_fefoespecial) {
		this.clie_ds_fefoespecial = clie_ds_fefoespecial;
	}
	public String getClie_ds_paletizacaoespecial() {
		return clie_ds_paletizacaoespecial;
	}
	public void setClie_ds_paletizacaoespecial(String clie_ds_paletizacaoespecial) {
		this.clie_ds_paletizacaoespecial = clie_ds_paletizacaoespecial;
	}
	public String getClie_ds_paletizacaomondelez() {
		return clie_ds_paletizacaomondelez;
	}
	public void setClie_ds_paletizacaomondelez(String clie_ds_paletizacaomondelez) {
		this.clie_ds_paletizacaomondelez = clie_ds_paletizacaomondelez;
	}
	public String getClie_ds_tipopaletizacao() {
		return clie_ds_tipopaletizacao;
	}
	public void setClie_ds_tipopaletizacao(String clie_ds_tipopaletizacao) {
		this.clie_ds_tipopaletizacao = clie_ds_tipopaletizacao;
	}
	public String getClie_ds_codveiculomax() {
		return clie_ds_codveiculomax;
	}
	public void setClie_ds_codveiculomax(String clie_ds_codveiculomax) {
		this.clie_ds_codveiculomax = clie_ds_codveiculomax;
	}
	public String getClie_ds_tipoveiculomax() {
		return clie_ds_tipoveiculomax;
	}
	public void setClie_ds_tipoveiculomax(String clie_ds_tipoveiculomax) {
		this.clie_ds_tipoveiculomax = clie_ds_tipoveiculomax;
	}
	public String getClie_ds_pesoveiculo() {
		return clie_ds_pesoveiculo;
	}
	public void setClie_ds_pesoveiculo(String clie_ds_pesoveiculo) {
		this.clie_ds_pesoveiculo = clie_ds_pesoveiculo;
	}
	public String getClie_ds_temagendamento() {
		return clie_ds_temagendamento;
	}
	public void setClie_ds_temagendamento(String clie_ds_temagendamento) {
		this.clie_ds_temagendamento = clie_ds_temagendamento;
	}
	public String getClie_ds_tipoagendamento() {
		return clie_ds_tipoagendamento;
	}
	public void setClie_ds_tipoagendamento(String clie_ds_tipoagendamento) {
		this.clie_ds_tipoagendamento = clie_ds_tipoagendamento;
	}
	public String getClie_ds_esperacliente() {
		return clie_ds_esperacliente;
	}
	public void setClie_ds_esperacliente(String clie_ds_esperacliente) {
		this.clie_ds_esperacliente = clie_ds_esperacliente;
	}
	public String getClie_ds_pagataxaveiculodedicado() {
		return clie_ds_pagataxaveiculodedicado;
	}
	public void setClie_ds_pagataxaveiculodedicado(
			String clie_ds_pagataxaveiculodedicado) {
		this.clie_ds_pagataxaveiculodedicado = clie_ds_pagataxaveiculodedicado;
	}
	public String getClie_ds_tipodescarga() {
		return clie_ds_tipodescarga;
	}
	public void setClie_ds_tipodescarga(String clie_ds_tipodescarga) {
		this.clie_ds_tipodescarga = clie_ds_tipodescarga;
	}
	public String getClie_ds_clienteexigepalete() {
		return clie_ds_clienteexigepalete;
	}
	public void setClie_ds_clienteexigepalete(String clie_ds_clienteexigepalete) {
		this.clie_ds_clienteexigepalete = clie_ds_clienteexigepalete;
	}
	public String getClie_ds_alturpalete() {
		return clie_ds_alturpalete;
	}
	public void setClie_ds_alturpalete(String clie_ds_alturpalete) {
		this.clie_ds_alturpalete = clie_ds_alturpalete;
	}
	public String getClie_cd_veiculomax() {
		return clie_cd_veiculomax;
	}
	public void setClie_cd_veiculomax(String clie_cd_veiculomax) {
		this.clie_cd_veiculomax = clie_cd_veiculomax;
	}
	public String getClie_ds_datarevisao() {
		return clie_ds_datarevisao;
	}
	public void setClie_ds_datarevisao(String clie_ds_datarevisao) {
		this.clie_ds_datarevisao = clie_ds_datarevisao;
	}
	public String getClie_ds_resprevisao() {
		return clie_ds_resprevisao;
	}
	public void setClie_ds_resprevisao(String clie_ds_resprevisao) {
		this.clie_ds_resprevisao = clie_ds_resprevisao;
	}
	public String getClie_ds_observacao() {
		return clie_ds_observacao;
	}
	public void setClie_ds_observacao(String clie_ds_observacao) {
		this.clie_ds_observacao = clie_ds_observacao;
	}
	public String getCli_ds_restricaocirculacao() {
		return cli_ds_restricaocirculacao;
	}
	public void setCli_ds_restricaocirculacao(String cli_ds_restricaocirculacao) {
		this.cli_ds_restricaocirculacao = cli_ds_restricaocirculacao;
	}
	public String getClie_ds_tiporestricao() {
		return clie_ds_tiporestricao;
	}
	public void setClie_ds_tiporestricao(String clie_ds_tiporestricao) {
		this.clie_ds_tiporestricao = clie_ds_tiporestricao;
	}
	public String getClie_ds_horariorestricao() {
		return clie_ds_horariorestricao;
	}
	public void setClie_ds_horariorestricao(String clie_ds_horariorestricao) {
		this.clie_ds_horariorestricao = clie_ds_horariorestricao;
	}
	public String getClie_nm_arquivoprocessado() {
		return clie_nm_arquivoprocessado;
	}
	public void setClie_nm_arquivoprocessado(String clie_nm_arquivoprocessado) {
		this.clie_nm_arquivoprocessado = clie_nm_arquivoprocessado;
	}
	

    
    

}
