package br.com.portalpluris.bean;

public class NotaFiscalBean {

	private String nrNotaFiscal;
	private String codEmpresa;
	private String nrPedido;
	private String nrPedidoCliente;
	private String nrDeliveryDocument;
	private String dhDelivery;
	private String serieNota;
	private String codCliente;
	private String razaoSocial;
	private String cidadeCliente;
	private String ufCliente;
	private String dhEmissaoNota;
	private String volumeNetSalesKg;
	private String valorNota;
	private String valorVendaLiquida;
	private String ltHalf;
	private String ltFull;
	private String canalVendas;
	private String canal;
	private String volumeNetSalesCx;
	private String nomeVendedor;
	private String nomeFantasia;
	private String planta;
	private String tipoPedido;
	private String inativo;
	private String userAtualizacacao;
	private String dhImportacao;
	private String dhAtualizacao;

	public String getNrDeliveryDocument() {
		return nrDeliveryDocument;
	}

	public void setNrDeliveryDocument(String nrDeliveryDocument) {
		this.nrDeliveryDocument = nrDeliveryDocument;
	}

	public String getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	public String getVolumeNetSalesKg() {
		return volumeNetSalesKg;
	}

	public void setVolumeNetSalesKg(String volumeNetSalesKg) {
		this.volumeNetSalesKg = volumeNetSalesKg;
	}

	public String getValorNota() {
		return valorNota;
	}

	public void setValorNota(String valorNota) {
		this.valorNota = valorNota;
	}

	public String getValorVendaLiquida() {
		return valorVendaLiquida;
	}

	public void setValorVendaLiquida(String valorVendaLiquida) {
		this.valorVendaLiquida = valorVendaLiquida;
	}

	public String getVolumeNetSalesCx() {
		return volumeNetSalesCx;
	}

	public void setVolumeNetSalesCx(String volumeNetSalesCx) {
		this.volumeNetSalesCx = volumeNetSalesCx;
	}

	public String getNrNotaFiscal() {
		return nrNotaFiscal;
	}

	public void setNrNotaFiscal(String nrNotaFiscal) {
		this.nrNotaFiscal = nrNotaFiscal;
	}

	public String getCodEmpresa() {
		return codEmpresa;
	}

	public void setCodEmpresa(String codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	public String getNrPedido() {
		return nrPedido;
	}

	public void setNrPedido(String nrPedido) {
		this.nrPedido = nrPedido;
	}

	public String getNrPedidoCliente() {
		return nrPedidoCliente;
	}

	public void setNrPedidoCliente(String nrPedidoCliente) {
		this.nrPedidoCliente = nrPedidoCliente;
	}

	public String getDhDelivery() {
		return dhDelivery;
	}

	public void setDhDelivery(String dhDelivery) {
		this.dhDelivery = dhDelivery;
	}

	public String getSerieNota() {
		return serieNota;
	}

	public void setSerieNota(String serieNota) {
		this.serieNota = serieNota;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCidadeCliente() {
		return cidadeCliente;
	}

	public void setCidadeCliente(String cidadeCliente) {
		this.cidadeCliente = cidadeCliente;
	}

	public String getUfCliente() {
		return ufCliente;
	}

	public void setUfCliente(String ufCliente) {
		this.ufCliente = ufCliente;
	}

	public String getDhEmissaoNota() {
		return dhEmissaoNota;
	}

	public void setDhEmissaoNota(String dhEmissaoNota) {
		this.dhEmissaoNota = dhEmissaoNota;
	}

	public String getLtHalf() {
		return ltHalf;
	}

	public void setLtHalf(String ltHalf) {
		this.ltHalf = ltHalf;
	}

	public String getLtFull() {
		return ltFull;
	}

	public void setLtFull(String ltFull) {
		this.ltFull = ltFull;
	}

	public String getCanalVendas() {
		return canalVendas;
	}

	public void setCanalVendas(String canalVendas) {
		this.canalVendas = canalVendas;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getNomeVendedor() {
		return nomeVendedor;
	}

	public void setNomeVendedor(String nomeVendedor) {
		this.nomeVendedor = nomeVendedor;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getPlanta() {
		return planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	public String getTipoPedido() {
		return tipoPedido;
	}

	public void setTipoPedido(String tipoPedido) {
		this.tipoPedido = tipoPedido;
	}

	public String getInativo() {
		return inativo;
	}

	public void setInativo(String inativo) {
		this.inativo = inativo;
	}

	public String getUserAtualizacacao() {
		return userAtualizacacao;
	}

	public void setUserAtualizacacao(String userAtualizacacao) {
		this.userAtualizacacao = userAtualizacacao;
	}

	public String getDhImportacao() {
		return dhImportacao;
	}

	public void setDhImportacao(String dhImportacao) {
		this.dhImportacao = dhImportacao;
	}

	public String getDhAtualizacao() {
		return dhAtualizacao;
	}

	public void setDhAtualizacao(String dhAtualizacao) {
		this.dhAtualizacao = dhAtualizacao;
	}

}
