package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsCdtbContatoclienteCoclBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsCdtbContatoclienteCoclDao;
import br.com.portalpluris.dao.DAOLog;

public class CsCdtbContatoclienteCoclBusiness {

	CsCdtbContatoclienteCoclDao coclDao = new CsCdtbContatoclienteCoclDao();
	private DAOLog daoLog = new DAOLog();

	public String createUpdateContatocliente(CsCdtbContatoclienteCoclBean coclBean) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogCocl(coclBean, "createUpdateContatocliente");
		RetornoBean retBean = new RetornoBean();

		String jsonRetorno = "";

		try {

			retBean = coclDao.createUpdateContatocliente(coclBean);

		} catch (Exception e) {
			e.printStackTrace();

			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	public String getContatoCliente(int coclIdCdCodigocliente) throws JsonProcessingException {

		EsLgtbLogsLogsBean logsBean = getLogCocl(null, "createUpdateContatocliente");
		logsBean.setLogsDsChave(coclIdCdCodigocliente + "");

		List<CsCdtbContatoclienteCoclBean> lstCoclBean = new ArrayList<CsCdtbContatoclienteCoclBean>();

		String jsonRetorno = "";

		try {

			lstCoclBean = coclDao.getContatoCliente(coclIdCdCodigocliente);

		} catch (Exception e) {
			e.printStackTrace();

			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());
		}

		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(lstCoclBean);

		logsBean.setLogsTxInfosaida(jsonRetorno);
		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());

		daoLog.createLog(logsBean);

		return jsonRetorno;
	}

	private EsLgtbLogsLogsBean getLogCocl(CsCdtbContatoclienteCoclBean coclBean, String metodo) {

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbContatoclienteCoclBusiness");
		logsBean.setLogsDsMetodo(metodo);
		logsBean.setLogsInErro("N");
		logsBean.setLogsTxInfoentrada(getAllValues(coclBean));

		return logsBean;
	}

	private String getAllValues(CsCdtbContatoclienteCoclBean coclBean) {

		String all = "";

		if (coclBean != null) {
			all += coclBean.getCocl_id_cd_contatocliente() + ";";
			all += coclBean.getCocl_id_cd_codigocliente() + ";";
			all += coclBean.getCocl_ds_emailcontatocliente() + ";";
			all += coclBean.getCocl_in_inativo() + ";";
		}

		return all;
	}

}
