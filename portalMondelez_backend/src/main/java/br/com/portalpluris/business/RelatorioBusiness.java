package br.com.portalpluris.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsNgtbDataagendamentoDaagBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.MotivoResponsavelCutoffBean;
import br.com.portalpluris.bean.MotivoResponsavelDesvioLTBean;
import br.com.portalpluris.bean.RelatorioBean;
import br.com.portalpluris.bean.relatorioOcrBean;
import br.com.portalpluris.dao.DAOLog;
import br.com.portalpluris.dao.RelatorioDao;

public class RelatorioBusiness {
	private DAOLog daoLog = new DAOLog();
	RelatorioDao relatorioDao = new RelatorioDao();
	
	public String getRelatorioBusinessNovo(String periodo, String ano, String transp) throws SQLException, JsonProcessingException, InterruptedException {
		List<RelatorioBean> relatorioRetorno = new ArrayList<RelatorioBean>();
		
		String jsonRetorno = "";
		relatorioRetorno = relatorioDao.getRelatorioDaoNovo(periodo, ano, transp);
		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(relatorioRetorno);
		

		return jsonRetorno;
	}
	
	public String getRelatorioBusiness(String periodo, String ano, String transp) throws SQLException, JsonProcessingException, InterruptedException {
		
		List<RelatorioBean> relatorioRetorno = new ArrayList<RelatorioBean>();
		
		
//		EsLgtbLogsLogsBean logsBean = getLogStatus(relatorioRetorno, "createUpdateStat");
//		
//		try {
//
//			relatorioRetorno = relatorioDao.getRelatorioDao(periodo);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//
//
//			logsBean.setLogsInErro("S");
//			logsBean.setLogsTxErro(e.getMessage());
//		}
		Date agora = new Date();
		System.out.println("antes - " + agora);
		relatorioRetorno = relatorioDao.getRelatorioDao(periodo, ano, transp);
		Date jaja = new Date();
		System.out.println("depois - " + jaja);
		//FAZ UM LOOPING NO RETORNO DO BANCO PRA TRAZER OS CAMPOS CALCULADOS
		for(int i=0; i < relatorioRetorno.size(); i++) {
			//busca dados do ultimo agendamento do tipo "AGENDAMENTO"
			
			CsNgtbDataagendamentoDaagBean agendamento = new CsNgtbDataagendamentoDaagBean();
			agendamento = relatorioDao.getDataAgendamentoDao(relatorioRetorno.get(i).getEmba_id_cd_embarque(), "Agendamento");
			if(agendamento.getDaag_dh_agendamento() != null) {
				relatorioRetorno.get(i).setData_agendamento(agendamento.getDaag_dh_agendamento());
				relatorioRetorno.get(i).setSenha_agendamento(agendamento.getDaag_ds_senhaagendamento());
				relatorioRetorno.get(i).setCalc_input_dhl_agendamento(agendamento.getDaag_dh_registro());
				relatorioRetorno.get(i).setDaag_ds_horaagendamento(agendamento.getDaag_ds_horaagendamento());
			}else {
				relatorioRetorno.get(i).setData_agendamento("");
				relatorioRetorno.get(i).setSenha_agendamento("");
				relatorioRetorno.get(i).setCalc_input_dhl_agendamento("");
				relatorioRetorno.get(i).setDaag_ds_horaagendamento("");
			}
			
			
			//REAGENDAMENTO
			CsNgtbDataagendamentoDaagBean reagendamento = new CsNgtbDataagendamentoDaagBean();
			reagendamento = relatorioDao.getDataAgendamentoDao(relatorioRetorno.get(i).getEmba_id_cd_embarque(), "Reagendamento");
			if(reagendamento.getDaag_dh_agendamento() != null) {
				relatorioRetorno.get(i).setData_reagendamento(reagendamento.getDaag_dh_agendamento());
				relatorioRetorno.get(i).setSenha_reagendamento(reagendamento.getDaag_ds_senhaagendamento());
				relatorioRetorno.get(i).setMotivo_reagendamento(reagendamento.getMoti_ds_motivo());
				relatorioRetorno.get(i).setResponsavel_reagendamento(reagendamento.getResp_nm_responsavel());
				relatorioRetorno.get(i).setDaag_ds_horareagendamento(reagendamento.getDaag_ds_horaagendamento());
				relatorioRetorno.get(i).setData_solicit_reagendamento(reagendamento.getDaag_dh_registro());
			}else {
				relatorioRetorno.get(i).setData_reagendamento("");
				relatorioRetorno.get(i).setSenha_reagendamento("");
				relatorioRetorno.get(i).setMotivo_reagendamento("");
				relatorioRetorno.get(i).setResponsavel_reagendamento("");
				relatorioRetorno.get(i).setDaag_ds_horareagendamento("");
				relatorioRetorno.get(i).setData_solicit_reagendamento("");
			}
			
			
			//RETIFICAÇÃO DE AGENDA
			CsNgtbDataagendamentoDaagBean retificacao = new CsNgtbDataagendamentoDaagBean();
			retificacao = relatorioDao.getDataAgendamentoDao(relatorioRetorno.get(i).getEmba_id_cd_embarque(), "Retificação");
			if(retificacao.getDaag_dh_agendamento() != null) {
				relatorioRetorno.get(i).setData_retificacao_agenda(retificacao.getDaag_dh_agendamento());
				relatorioRetorno.get(i).setSenha_retificacao_agenda(retificacao.getDaag_ds_senhaagendamento());
				relatorioRetorno.get(i).setMotivo_retificacao_agenda(retificacao.getMoti_ds_motivo());
				relatorioRetorno.get(i).setResponsavel_retificacao_agenda(retificacao.getResp_nm_responsavel());
				relatorioRetorno.get(i).setDaag_ds_horaretificacaoagenda(retificacao.getDaag_ds_horaagendamento());
				relatorioRetorno.get(i).setData_solicit_nova_agenda(retificacao.getDaag_dh_registro());
			}else {
				relatorioRetorno.get(i).setData_retificacao_agenda("");
				relatorioRetorno.get(i).setSenha_retificacao_agenda("");
				relatorioRetorno.get(i).setMotivo_retificacao_agenda("");
				relatorioRetorno.get(i).setResponsavel_retificacao_agenda("");
				relatorioRetorno.get(i).setDaag_ds_horaretificacaoagenda("");
				relatorioRetorno.get(i).setData_solicit_nova_agenda("");
			}
			
			//OCR
			relatorioOcrBean ocr = new relatorioOcrBean();
			ocr = relatorioDao.getOcr(relatorioRetorno.get(i).getEmba_id_cd_embarque());
			relatorioRetorno.get(i).setDt_ocr_agendamento(ocr.getDt_ocr_agendamento());
			relatorioRetorno.get(i).setMotivo_ocr_agend(ocr.getMotivo_ocr_agend());
			relatorioRetorno.get(i).setResponsavel_pendencia(ocr.getResponsavel_pendencia());
			
			
			//AGING DE AGENDAMENTO E REAGENDAMENTO
			//CALCULO - DATEDIFF ENTRE DATA DE REGISTRO DO FOLLOW DE PRIMEIRO CONTATO E O FOLLOW DE CONFIRMAÇÃO
			//FOLLOWS PRIMEIRO CONTATO = 7,8,9
			//FOLLOWS CONFIRMAÇÃO = 19,21
			relatorioRetorno.get(i).setCalc_aging_agendamento(calculaAging("agendamento", relatorioRetorno.get(i).getEmba_id_cd_embarque()));
			relatorioRetorno.get(i).setCalc_aging_reagendamento(calculaAging("reagendamento", relatorioRetorno.get(i).getEmba_id_cd_embarque()));
			
			//TMCA - DATEDIFF ENTRE DATA DE REGISTRO DE PRIMEIRO CONTATO E DH_REGISTRO DA DAAG ONDE TIPO = AGENDAMENTO
			//FOLLOWS PRIMEIRO CONTATO = 7,8,9
			relatorioRetorno.get(i).setTmca(relatorioDao.calculaTmca(relatorioRetorno.get(i).getEmba_id_cd_embarque()));
			
			//ON TIME SCHEDULING - VERIFICA SE O AGENDAMENTO FOI FEITO DENTRO DO PRAZO PELO AGENDADOR
			//CALCULO - DATA DA SHIPMENT E DH REGISTRO QUE FOI FEITO O PRIMEIRO AGENDAMENTO
			relatorioRetorno.get(i).setCalc_ontime_scheduling(ontimeScheduling(relatorioRetorno.get(i).getEmba_id_cd_embarque()));
			
			//PRIMEIRO CONTATO
			//FOLLOWS PRIMEIRO CONTATO = 7,8,9
			relatorioRetorno.get(i).setCalc_primeiro_contato(getPrimeiroContato(relatorioRetorno.get(i).getEmba_id_cd_embarque()));
			
			//DESVIO LEADTIME
			//CALCULO - DATA DE LEADTIME E DATA DE ENTREGA(CONSIDERAR A ULTIMA DATA DE AGENDAMENTO NA DAAG)
			relatorioRetorno.get(i).setCalc_desvio_leadtime(getDesvioLeadTime(relatorioRetorno.get(i).getEmba_id_cd_embarque()));
			
			//MOTIVO E RESPONSAVEL PELO DESVIO DE LEADTIME
			MotivoResponsavelDesvioLTBean motiResp = new MotivoResponsavelDesvioLTBean();
			motiResp = motivoResponsavelDesvioLT(relatorioRetorno.get(i).getEmba_id_cd_embarque());
			relatorioRetorno.get(i).setFoup_motivo_desvio_leadtime(motiResp.getMoti_ds_motivo());
			relatorioRetorno.get(i).setFoup_responsavel_desvio_leadtime(motiResp.getResp_nm_responsavel());
			
			//MOTIVO E RESPONSAVEL DO CUTOFF
			//BUSCA CASO EMBA_IN_CUTOFF = S
			if(relatorioRetorno.get(i).getEmba_in_cutoff() != null) {
				if(relatorioRetorno.get(i).getEmba_in_cutoff().equals("S")) {
					MotivoResponsavelCutoffBean motiRespCutoff = new MotivoResponsavelCutoffBean();
					relatorioRetorno.get(i).setEmba_ds_motivocutoff(motiRespCutoff.getMoti_ds_motivo());
					relatorioRetorno.get(i).setEmba_ds_responsavelcutoff(motiRespCutoff.getResp_nm_responsavel());
				}
			}
			
			
			
		}
		
		String jsonRetorno = "";
		
		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(relatorioRetorno);
		
//		logsBean.setLogsTxInfosaida(jsonRetorno);
//		logsBean.setLogsDhFim(UtilDate.getSqlDateTimeAtual());
//
//		daoLog.createLog(logsBean);
		
		
		return jsonRetorno;
	}
	
	
	
	public MotivoResponsavelCutoffBean motivoResponsavelCutoff(String idEmbarque) throws SQLException {
		return relatorioDao.getMotivoResponsavelCutoff(idEmbarque);
	}
	
	public MotivoResponsavelDesvioLTBean motivoResponsavelDesvioLT(String idEmbarque) throws SQLException{
		return relatorioDao.getMotivoResponsavelDesvioLt(idEmbarque);
	}
	
	public String getDesvioLeadTime(String idEmbarque) throws SQLException {
		return relatorioDao.desvioLeadTime(idEmbarque);
		
	}
	
	public String getPrimeiroContato(String idEmbarque) throws SQLException {
		return relatorioDao.primeiroContato(idEmbarque);
	}
	
	public String ontimeScheduling(String idEmbarque) throws SQLException {
		return relatorioDao.onTimeScheduling(idEmbarque);
	}
	
	public int calculaAging(String tipo, String idEmbarque) throws SQLException {
		
		return relatorioDao.calculaAging(tipo, idEmbarque);
	}
	
	public CsNgtbDataagendamentoDaagBean getDataAgendamento(String id_embarque, String tipo_agendamento) {
		CsNgtbDataagendamentoDaagBean ret = new CsNgtbDataagendamentoDaagBean();
		
		
		
		return ret;
		
	}

}
