package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CanalPorClienteBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;

public class CsCdtbCanalPorClienteDao {

	public boolean slctCanal(String cncl_cd_custumer) {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbCanalPorClienteDao");
		logsBean.setLogsDsMetodo("slctCanal");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Consulta na base Canal por Cliente *****");
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			
			cSQL += "	SELECT   ";
			cSQL += "	* ";
			cSQL += " 		FROM CS_CDTB_CANALPORCLIENTE_CNCL (NOLOCK)";
			cSQL += " 	WHERE  CNCL_CD_CUSTUMER = ? ";

			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, cncl_cd_custumer);

			rs = pst.executeQuery();

			if (rs.next()) {

				log.info(" ******** O contato cliente :" + cncl_cd_custumer + " Já existe na base *******");

				return true;
			}
		
		} catch (Exception e) {
			log.info("****Fim Consulta na base Canal por Cliente *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			daoLog.createLog(logsBean);

		}

		
		return false;
	}

public void insert(CanalPorClienteBean canalCli) {		
		
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbCanalPorClienteDao");
		logsBean.setLogsDsMetodo("insert");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio insert na base Canal por Cliente *****");
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();
			
			PreparedStatement pst = con.prepareStatement("");

			pst = con.prepareStatement("" 
			+ " INSERT INTO CS_CDTB_CANALPORCLIENTE_CNCL ( "
			+" CNCL_CD_CUSTUMER,"
			+" CNCL_NM_RAZAOSOCIAL,"
			+" CNCL_DS_CNPJ,"
			+" CNCL_DS_CIDADE,"
			+" CNCL_DS_ESTADO,"
			+" CNCL_CD_HOLDING,"
			+" CNCL_DS_LEVEL1NODE,"
			+" CNCL_DS_GRUPOCANAL,"
			+" CNCL_DS_LEVEL2NODE,"
			+" CNCL_DS_CHANEL,"
			+" CNCL_DS_LEVEL3NODE,"
			+" CNCL_DS_SEGMENTO,"
			+" CNCL_DS_LEVEL4NODE,"
			+" CNCL_DS_REDE,"
			+" CNCL_DS_LEVEL5NODE,"
			+" CNCL_DS_REGIAOVENDAS,"
			+" CNCL_DS_LEVEL6NODE,"
			+" CNCL_DS_CANALCUSTUMER,"
			+" CNCL_DS_REGIAOCANAL,"
			+" CNCL_DS_AREAVENDA,"			
			+ " CNCL_ID_USUARIOATUALIZACAO,"
			+ " CNCL_DH_IMPORTACAO,"
			+ " CNCL_DS_NOMEARQUIVO,"
			+ " CNCL_DS_INATIVO"
			+ ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,? )");//14
		
			pst.setString(1,  canalCli.getCncl_cd_custumer());
			pst.setString(2,  canalCli.getCncl_nm_razaosocial());
			pst.setString(3,  canalCli.getCncl_ds_cnpj());
			pst.setString(4,  canalCli.getCncl_ds_cidade());
			pst.setString(5,  canalCli.getCncl_ds_estado());
			pst.setString(6,  canalCli.getCncl_cd_holding());
			pst.setString(7,  canalCli.getCncl_ds_level1node());
			pst.setString(8,  canalCli.getCncl_ds_grupocanal());
			pst.setString(9,  canalCli.getCncl_ds_level2node());
			pst.setString(10,  canalCli.getCncl_ds_chanel());
			pst.setString(11,  canalCli.getCncl_ds_level3node());
			pst.setString(12,  canalCli.getCncl_ds_segmento());
			pst.setString(13,  canalCli.getCncl_ds_level4node());
			pst.setString(14,  canalCli.getCncl_ds_rede());
			pst.setString(15,  canalCli.getCncl_ds_level5node());
			pst.setString(16,  canalCli.getCncl_ds_regiaovendas());
			pst.setString(17,  canalCli.getCncl_ds_level6node());
			pst.setString(18,  canalCli.getCncl_ds_areavenda());
			pst.setString(19,  canalCli.getCncl_ds_canalcustumer());
			pst.setString(20,  canalCli.getCncl_ds_regiaocanal());
			pst.setString(21,  canalCli.getCncl_id_usuarioatualizacao());
			pst.setString(22,  canalCli.getCncl_dh_importacao());
			pst.setString(23,  canalCli.getCncl_ds_nomearquivo());
			pst.setString(24,  canalCli.getCncl_ds_inativo());		
	
		
			pst.execute();
			
		} catch (Exception e) {
			
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			log.info("****Fim insert na base Canal por Cliente *****");
			daoLog.createLog(logsBean);

		}
		
	}
	
	
	public void insertOld(CanalPorClienteBean canalCli) {		
		
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbCanalPorClienteDao");
		logsBean.setLogsDsMetodo("insert");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio insert na base Canal por Cliente *****");
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();
			
			PreparedStatement pst = con.prepareStatement("");

			pst = con.prepareStatement("" 
			+ " INSERT INTO CS_CDTB_CANALPORCLIENTE_CNCL ( "
			+ " CNCL_CD_CUSTUMER,"
			+ " CNCL_DS_CHANEL ,"
			+ " CNCL_CD_HOLDING,"
			+ " CNCL_NM_HOLDING,"
			+ " CNCL_DS_COUNTRY,"
			+ " CNCL_NM_RAZAOSOCIAL,"
			+ " CNCL_DS_FANTASIA,"
			+ " CNCL_DS_CIDADE ,"
			+ " CNCL_DS_CEP    ,"
			+ " CNCL_DS_ESTADO ,"
			+ " CNCL_DS_RUA    ,"
			+ " CNCL_DS_TELEFONE,"
			+ " CNCL_DS_INDUSTRIA,"
			+ " CNCL_CD_NIELSEN,"
			+ " CNCL_DS_DISTRICT,"
			+ " CNCL_DS_CNPJ   ,"
			+ " CNCL_DS_TRANSPORTZONE,"
			+ " CNCL_DS_INSCRICAOESTADUAL,"
			+ " CNCL_DS_SUBTRIB,"
			+ " CNCL_DS_TAXAJURD,"
			+ " CNCL_DS_HIEARCHYASSIGNMENT,"
			+ " CNCL_CD_CODIGOPAI,"
			+ " CNCL_DS_CONDPAGAMENTO,"
			+ " CNCL_DS_ACEITASALDO,"
			+ " CNCL_DS_REBATE ,"
			+ " CNCL_CD_SALESDISTRICT,"
			+ " CNCL_DS_ORDERCOMBINAT,"
			+ " CNCL_CD_DELIVERYPRIOR,"
			+ " CNCL_DS_PAYTTERMS,"
			+ " CNCL_DS_PRICEDETERMINATION,"
			+ " CNCL_DS_TYPECLASS,"
			+ " CNCL_ID_USUARIOATUALIZACAO,"
			+ " CNCL_DH_IMPORTACAO,"
			+ " CNCL_DS_NOMEARQUIVO,"
			+ " CNCL_DS_INATIVO"
			+ ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?)");
		
			pst.setString(1,  canalCli.getCncl_cd_custumer());
			pst.setString(2,  canalCli.getCncl_ds_chanel());
			pst.setString(3,  canalCli.getCncl_cd_holding());
			pst.setString(4,  canalCli.getCncl_nm_holding());
			pst.setString(5,  canalCli.getCncl_ds_country());
			pst.setString(6,  canalCli.getCncl_nm_razaosocial());
			pst.setString(7,  canalCli.getCncl_ds_fantasia());
			pst.setString(8,  canalCli.getCncl_ds_cidade());
			pst.setString(9,  canalCli.getCncl_ds_cep());
			pst.setString(10, canalCli.getCncl_ds_estado());		
			pst.setString(11, canalCli.getCncl_ds_rua());
			pst.setString(12, canalCli.getCncl_ds_telefone());
			pst.setString(13, canalCli.getCncl_ds_industria());
			pst.setString(14, canalCli.getCncl_cd_nielsen());
			pst.setString(15, canalCli.getCncl_ds_district());		
			pst.setString(16, canalCli.getCncl_ds_cnpj());
			pst.setString(17, canalCli.getCncl_ds_transportzone());
			pst.setString(18, canalCli.getCncl_ds_inscricaoestadual());
			pst.setString(19, canalCli.getCncl_ds_subtrib());
			pst.setString(20, canalCli.getCncl_ds_taxajurd());
			pst.setString(21, canalCli.getCncl_ds_hiearchyassignment());
			pst.setString(22, canalCli.getCncl_cd_codigopai());
			pst.setString(23, canalCli.getCncl_ds_condpagamento());
			pst.setString(24, canalCli.getCncl_ds_aceitasaldo());
			pst.setString(25, canalCli.getCncl_ds_rebate());
			pst.setString(26, canalCli.getCncl_cd_salesdistrict());
			pst.setString(27, canalCli.getCncl_ds_ordercombinat());
			pst.setString(28, canalCli.getCncl_cd_deliveryprior());
			pst.setString(29, canalCli.getCncl_ds_paytterms());
			pst.setString(30, canalCli.getCncl_ds_pricedetermination());
			pst.setString(31, canalCli.getCncl_ds_typeclass());
			pst.setString(32, canalCli.getCncl_id_usuarioatualizacao());
			pst.setString(33, canalCli.getCncl_dh_importacao());
			pst.setString(34, canalCli.getCncl_ds_nomearquivo());
			pst.setString(35, canalCli.getCncl_ds_inativo());
	
		
			pst.execute();
			
		} catch (Exception e) {
			
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			log.info("****Fim insert na base Canal por Cliente *****");
			daoLog.createLog(logsBean);

		}
		
	}

	public void updateOld(CanalPorClienteBean canalCli) {
	
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbCanalPorClienteDao");
		logsBean.setLogsDsMetodo("update");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio update na base Canal por Cliente *****");
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();
		
			PreparedStatement pst;

			pst = con.prepareStatement(""
				+ " UPDATE CS_CDTB_CANALPORCLIENTE_CNCL  SET"
				+" CNCL_CD_CUSTUMER= ?, "
				+" CNCL_DS_CHANEL= ?, "
				+" CNCL_CD_HOLDING= ?, "
				+" CNCL_NM_HOLDING= ?, "
				+" CNCL_DS_COUNTRY= ?, "
				+" CNCL_NM_RAZAOSOCIAL= ?, "
				+" CNCL_DS_FANTASIA= ?, "
				+" CNCL_DS_CIDADE= ?, "
				+" CNCL_DS_CEP = ?, "
				+" CNCL_DS_ESTADO= ?, "
				+" CNCL_DS_RUA = ?, "
				+" CNCL_DS_TELEFONE= ?, "
				+" CNCL_DS_INDUSTRIA= ?, "
				+" CNCL_CD_NIELSEN= ?, "
				+" CNCL_DS_DISTRICT= ?, "
				+" CNCL_DS_CNPJ= ?, "
				+" CNCL_DS_TRANSPORTZONE= ?, "
				+" CNCL_DS_INSCRICAOESTADUAL= ?, "
				+" CNCL_DS_SUBTRIB= ?, "
				+" CNCL_DS_TAXAJURD= ?, "
				+" CNCL_DS_HIEARCHYASSIGNMENT= ?, "
				+" CNCL_CD_CODIGOPAI= ?, "
				+" CNCL_DS_CONDPAGAMENTO= ?, "
				+" CNCL_DS_ACEITASALDO= ?, "
				+" CNCL_DS_REBATE= ?, "
				+" CNCL_CD_SALESDISTRICT= ?, "
				+" CNCL_DS_ORDERCOMBINAT= ?, "
				+" CNCL_CD_DELIVERYPRIOR= ?, "
				+" CNCL_DS_PAYTTERMS= ?, "
				+" CNCL_DS_PRICEDETERMINATION= ?, "
				+" CNCL_DS_TYPECLASS= ?, "
				+" CNCL_ID_USUARIOATUALIZACAO= ?, "
				+" CNCL_DH_ATUALIZACAO= ?, "
				+" CNCL_DS_NOMEARQUIVO= ?, "
				+" CNCL_DS_INATIVO= ? "
				+ " WHERE CNCL_CD_CUSTUMER= ? " );
			
			pst.setString(1,  canalCli.getCncl_cd_custumer());
			pst.setString(2,  canalCli.getCncl_ds_chanel());
			pst.setString(3,  canalCli.getCncl_cd_holding());
			pst.setString(4,  canalCli.getCncl_nm_holding());
			pst.setString(5,  canalCli.getCncl_ds_country());
			pst.setString(6,  canalCli.getCncl_nm_razaosocial());
			pst.setString(7,  canalCli.getCncl_ds_fantasia());
			pst.setString(8,  canalCli.getCncl_ds_cidade());
			pst.setString(9,  canalCli.getCncl_ds_cep());
			pst.setString(10, canalCli.getCncl_ds_estado());		
			pst.setString(11, canalCli.getCncl_ds_rua());
			pst.setString(12, canalCli.getCncl_ds_telefone());
			pst.setString(13, canalCli.getCncl_ds_industria());
			pst.setString(14, canalCli.getCncl_cd_nielsen());
			pst.setString(15, canalCli.getCncl_ds_district());		
			pst.setString(16, canalCli.getCncl_ds_cnpj());
			pst.setString(17, canalCli.getCncl_ds_transportzone());
			pst.setString(18, canalCli.getCncl_ds_inscricaoestadual());
			pst.setString(19, canalCli.getCncl_ds_subtrib());
			pst.setString(20, canalCli.getCncl_ds_taxajurd());
			pst.setString(21, canalCli.getCncl_ds_hiearchyassignment());
			pst.setString(22, canalCli.getCncl_cd_codigopai());
			pst.setString(23, canalCli.getCncl_ds_condpagamento());
			pst.setString(24, canalCli.getCncl_ds_aceitasaldo());
			pst.setString(25, canalCli.getCncl_ds_rebate());
			pst.setString(26, canalCli.getCncl_cd_salesdistrict());
			pst.setString(27, canalCli.getCncl_ds_ordercombinat());
			pst.setString(28, canalCli.getCncl_cd_deliveryprior());
			pst.setString(29, canalCli.getCncl_ds_paytterms());
			pst.setString(30, canalCli.getCncl_ds_pricedetermination());
			pst.setString(31, canalCli.getCncl_ds_typeclass());
			pst.setString(32, canalCli.getCncl_id_usuarioatualizacao());
			pst.setString(33, canalCli.getCncl_dh_importacao());
			pst.setString(34, canalCli.getCncl_ds_nomearquivo());
			pst.setString(35, canalCli.getCncl_ds_inativo());
			pst.setString(36, canalCli.getCncl_cd_custumer());
			
			pst.executeUpdate();
			
			
		} catch (Exception e) {
			log.info("****Fim update na base Canal por Cliente *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			
			daoLog.createLog(logsBean);
		}
		
	}
	
	public void update(CanalPorClienteBean canalCli) {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbCanalPorClienteDao");
		logsBean.setLogsDsMetodo("update");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio update na base Canal por Cliente *****");
		
		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();
		
			PreparedStatement pst;

			pst = con.prepareStatement(""
				+ " UPDATE CS_CDTB_CANALPORCLIENTE_CNCL  SET"
				+" CNCL_NM_RAZAOSOCIAL = ?, "
				+" CNCL_DS_CNPJ = ?, "
				+" CNCL_DS_CIDADE = ?, "
				+" CNCL_DS_ESTADO = ?, "
				+" CNCL_CD_HOLDING = ?, "
				+" CNCL_DS_LEVEL1NODE = ?, "
				+" CNCL_DS_GRUPOCANAL = ?, "
				+" CNCL_DS_LEVEL2NODE = ?, "
				+" CNCL_DS_CHANEL = ?, "
				+" CNCL_DS_LEVEL3NODE = ?, "
				+" CNCL_DS_SEGMENTO = ?, "
				+" CNCL_DS_LEVEL4NODE = ?, "
				+" CNCL_DS_REDE = ?, "
				+" CNCL_DS_LEVEL5NODE = ?, "
				+" CNCL_DS_REGIAOVENDAS = ?, "
				+" CNCL_DS_LEVEL6NODE = ?, " // 16
				+" CNCL_DS_CANALCUSTUMER = ?, " // 17
				+" CNCL_DS_REGIAOCANAL = ?, " //18
				+" CNCL_DS_AREAVENDA = ?, "			
				+ " CNCL_ID_USUARIOATUALIZACAO = ?, "
				+ " CNCL_DH_ATUALIZACAO = ?, "
				+ " CNCL_DS_NOMEARQUIVO = ?, "
				+ " CNCL_DS_INATIVO = ?"				
				+ " WHERE CNCL_CD_CUSTUMER= ? " );
			
			
			pst.setString(1,  canalCli.getCncl_nm_razaosocial());
			pst.setString(2,  canalCli.getCncl_ds_cnpj());
			pst.setString(3,  canalCli.getCncl_ds_cidade());
			pst.setString(4,  canalCli.getCncl_ds_estado());
			pst.setString(5,  canalCli.getCncl_cd_holding());
			pst.setString(6,  canalCli.getCncl_ds_level1node());
			pst.setString(7,  canalCli.getCncl_ds_grupocanal());
			pst.setString(8,  canalCli.getCncl_ds_level2node());
			pst.setString(9,  canalCli.getCncl_ds_chanel());
			pst.setString(10,  canalCli.getCncl_ds_level3node());
			pst.setString(11,  canalCli.getCncl_ds_segmento());
			pst.setString(12,  canalCli.getCncl_ds_level4node());
			pst.setString(13,  canalCli.getCncl_ds_rede());
			pst.setString(14,  canalCli.getCncl_ds_level5node());
			pst.setString(15,  canalCli.getCncl_ds_regiaovendas());
			pst.setString(16,  canalCli.getCncl_ds_level6node());
			pst.setString(17,  canalCli.getCncl_ds_canalcustumer());
			pst.setString(18,  canalCli.getCncl_ds_regiaocanal());
			pst.setString(19,  canalCli.getCncl_ds_areavenda());
			pst.setString(20,  canalCli.getCncl_id_usuarioatualizacao());
			pst.setString(21,  canalCli.getCncl_dh_importacao());
			pst.setString(22,  canalCli.getCncl_ds_nomearquivo());
			pst.setString(23,  canalCli.getCncl_ds_inativo());		
			pst.setString(24,  canalCli.getCncl_cd_custumer());
			
			
			
			pst.executeUpdate();
			
			
		} catch (Exception e) {
			log.info("****Fim update na base Canal por Cliente *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			
			daoLog.createLog(logsBean);
		}
		
	}
	

}
