package br.com.portalpluris.business;

import java.util.List;

import br.com.portalpluris.bean.AuxiliarCsCdtbFollowupFoupBean;
import br.com.portalpluris.bean.CsAstbEmbarqueFoupBean;
import br.com.portalpluris.bean.CsCdtbFollowupFoupBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.dao.CsAstbEmbarqueFoupDao;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CsAstbEmbarqueFoupBusiness {

	/**
	 * Metodo responsavel criar a associação entre o cadastro do follow-up e o
	 * cadastro do embarque
	 * 
	 * @author Caio Fernandes
	 * @param CsAstbEmbarqueFoupBean embarqueFoupBean
	 * @return String jsonRetorno
	 */
	public String createEmbarqueFoup(CsAstbEmbarqueFoupBean embarqueFoupBean) {

		String jsonRetorno = "";

		CsAstbEmbarqueFoupDao embarqueFoup = new CsAstbEmbarqueFoupDao();

		RetornoBean ret = embarqueFoup.createEmbarqueFoup(embarqueFoupBean);

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(ret);

		} catch (Exception e) {
			System.out.println("[createEmbarqueFoup]  ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}

	/**
	 * Metodo responsavel buscar as informacoes de follow-up atraves do codigo do
	 * embarque
	 * 
	 * @author Caio Fernandes
	 * @param String
	 * @return String jsonRetorno
	 */
	public String getEmbarqueFoup(String idEmbarque) {

		String jsonRetorno = "";

		CsAstbEmbarqueFoupDao embarqueFoup = new CsAstbEmbarqueFoupDao();

		List<AuxiliarCsCdtbFollowupFoupBean> lstFoups = embarqueFoup.getEmbarqueFoup(idEmbarque);

		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonRetorno = mapper.writeValueAsString(lstFoups);

		} catch (Exception e) {
			System.out.println("[createEmbarqueFoup] - ERRO AO CONVERTER BEAN EM JSON");
		}

		return jsonRetorno;
	}

}
