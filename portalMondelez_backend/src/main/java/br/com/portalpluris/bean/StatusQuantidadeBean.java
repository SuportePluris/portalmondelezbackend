package br.com.portalpluris.bean;

public class StatusQuantidadeBean {
	
	private long stat_id_cd_status;
	private String stat_ds_status;
	private int qtd;
	
	
	public long getStat_id_cd_status() {
		return stat_id_cd_status;
	}
	public void setStat_id_cd_status(long stat_id_cd_status) {
		this.stat_id_cd_status = stat_id_cd_status;
	}
	public String getStat_ds_status() {
		return stat_ds_status;
	}
	public void setStat_ds_status(String stat_ds_status) {
		this.stat_ds_status = stat_ds_status;
	}
	public int getQtd() {
		return qtd;
	}
	public void setQtd(int qtd) {
		this.qtd = qtd;
	}
	
	

}
