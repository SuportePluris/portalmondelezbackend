package br.com.catapi.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class UtilDate {
		
	/**
	 * Metodo responsavel por realizar a conversao de String no formato dd/MM/yyyy ou dd/MM/yy para String no formato yyyy-MM-dd HH:mm:ss definindo "C" para 00:00:00.000 e F para 23:59:59.000
	 * @author Rogério Nunes
	 * @param String
	 * @return Date dtFormatada
	 *  */
	public String convertStringToSql(String dtNoFormat, String comecoOuFim) {
		
		String dhFinal = "";
		String dhTemp[];
		
		if(dtNoFormat == null || dtNoFormat.equalsIgnoreCase("")) {
			return null;
		}
		
		dhTemp = dtNoFormat.split("/");
		
		if(dtNoFormat.length() == 10) {
			dhFinal = dhTemp[2]+"-"+dhTemp[1]+"-"+dhTemp[0];
			
		}else {
			dhFinal = "20"+dhTemp[2]+"-"+dhTemp[1]+"-"+dhTemp[0];
		}
		
		if(comecoOuFim.equals("C")) {
			dhFinal += " 00:00:00.000";
			
		} else if(comecoOuFim.equals("F")) {
			dhFinal += " 23:59:59.000";
		}
		
		return dhFinal;
	}
	
	/**
	 * Metodo responsavel por realizar a conversao de String no formato dd-MMM-yyyy para Date no formato yyyy-MM-dd HH:mm:ss 
	 * @author Caio Fernandes
	 * @param String
	 * @return Date dtFormatada
	 *  */
	public Date convertStringToDate(String dtNoFormat) throws ParseException {
		
		if(dtNoFormat == null || dtNoFormat.equalsIgnoreCase("")) {
			return null;
		}
		
		SimpleDateFormat formatDe = new SimpleDateFormat("dd-MMM-yy");
		SimpleDateFormat formatPara = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	    
	    Date date = formatDe.parse(dtNoFormat);
	    formatPara.format(date);
		
		return date;
	}
	
	/**
	 * Metodo responsavel por realizar a conversao de Date para String 
	 * @author Caio Fernandes
	 * @param Date
	 * @return String
	 *  */
	public String convertDateToYYYY_MM_dd_HH_MM_SS(Date date){
		
		if(date == null){
			return null;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		 String retorno = dateFormat.format(date);
		
		 return retorno;
	}
	
	/**
	 * Metodo responsavel por realizar a conversao de String no formato yyyyMMdd para Date no formato yyyy-MM-dd HH:mm:ss
	 * @author Caio Fernandes
	 * @param String
	 * @return Date dtFormatada
	 *  */
	public Date convertStringToDateYYYY_MM_dd_HH_MM_SS(String dtNoFormat) throws ParseException {
		
		if(dtNoFormat == null || dtNoFormat.equalsIgnoreCase("") || dtNoFormat.startsWith("0")) {
			return null;
		}
		
		SimpleDateFormat formatDe = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat formatPara = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	    
	    Date date = formatDe.parse(dtNoFormat);
	    formatPara.format(date);
		
		return date;
	}
	
	/**
	 * Metodo responsavel por realizar a conversao de Date para String 
	 * @author Caio Fernandes
	 * @param Date
	 * @return String
	 *  */
	public String convertDateToYYYY_MM_dd(Date date){
		
		if(date == null){
			return null;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		return dateFormat.format(date);
		
	}
	
	/**
	 * Metodo responsavel por realizar a conversao de String no formato yyyy-MM-dd HH:mm:ss para Date no formato yyyy-MM-dd HH:mm:ss
	 * @author Caio Fernandes
	 * @param String
	 * @return Date
	 *  */
	public Date convertStringToDateYYYY_MM_dd_HH_MM_SS_2(String dtNoFormat) throws ParseException {
		
		if(dtNoFormat == null || dtNoFormat.equalsIgnoreCase("") || dtNoFormat.startsWith("0")) {
			return null;
		}
		
		SimpleDateFormat formatDe = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatPara = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	    
	    Date date = formatDe.parse(dtNoFormat);
	    formatPara.format(date);
		
		return date;
	}

	public static Timestamp getSqlDateTimeAtual(){
		
		Timestamp sqlDate = null;
		
		try {
		
			TimeZone tz = TimeZone.getTimeZone("UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			df.setTimeZone(tz);
			String timeString = df.format(System.currentTimeMillis());
			Date date = df.parse(timeString);
			sqlDate = new Timestamp(date.getTime());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sqlDate;
	}
	
}
