package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbResumoDnsRednBean;
import br.com.portalpluris.bean.CsLgtbShipmentShipBean;

public class CsLgtbResumoDnsRednDao {
	
	/**
	 * Metodo responsavel por realizar o insert das informacoes da planilha de shipment na tabela CS_LGTB_SHIPMENT_SHIP
	 * @author Caio Fernandes
	 * @param List<CsLgtbResumoDnsRednBean> 
	 * */
	public void insertResumoDns(List<CsLgtbResumoDnsRednBean> lstResumoDnsBean, String nmArquivo) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		UtilDate util = new UtilDate();
		
		log.info("[insertResumoDns] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstResumoDnsBean.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertResumoDns] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" INSERT CS_LGTB_RESUMODNS_REDN ( "
						+ "		REDN_DS_ORDERELEASEID, " 	
						+ "		REDN_DS_DNN, 	" 	
						+ "		REDN_DH_DELIVERYDATE, 	"				
						+ "		REDN_DS_SHIPMENTTOP, 	"					
						+ "		REDN_DS_SHIPMENTSPLIT, 	"
						+ "		REDN_DS_SOURCELOCATIONID, 	"
						+ "		REDN_DS_SOURCELOCATIONAME, 	"
						+ "		REDN_DS_DESTINATIONLOCATIONID, 	"
						+ "		REDN_DS_DESTINATIONLOCATIONNAME, 	"
						+ "		REDN_DS_DESTINATIONCITY, 	"
						+ "		REDN_DS_DESTINATIONPROVINCECODE, 	"
						+ "		REDN_DS_GROSSWEIGHT, 	"
						+ "		REDN_DS_GROSSVOLUME, 	"
						+ "		REDN_DS_CASES, 	"
						+ "		REDN_DS_PROJETOS, 	"
						+ "		REDN_DS_MODELPLANN, 	"
						+ "		REDN_DS_EQUIPAMENTOGROUP, 	"
						+ " 	REDN_DS_NOMEARQUIVO )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)	"
						+ "");
				
				pst.setString(1, lstResumoDnsBean.get(i).getRedn_ds_ordereleaseid());
				pst.setString(2, lstResumoDnsBean.get(i).getRedn_ds_dnn());
				pst.setString(3, util.convertDateToYYYY_MM_dd(lstResumoDnsBean.get(i).getRedn_dh_deliverydate()));
				pst.setString(4, lstResumoDnsBean.get(i).getRedn_ds_shipmenttop());
				pst.setString(5, lstResumoDnsBean.get(i).getRedn_ds_shipmentsplit());
				pst.setString(6, lstResumoDnsBean.get(i).getRedn_ds_sourcelocationid());
				pst.setString(7, lstResumoDnsBean.get(i).getRedn_ds_sourcelocationame());
				pst.setString(8, lstResumoDnsBean.get(i).getRedn_ds_destinationlocationid());
				pst.setString(9, lstResumoDnsBean.get(i).getRedn_ds_destinationlocationname());
				pst.setString(10, lstResumoDnsBean.get(i).getRedn_ds_destinationcity());
				pst.setString(11, lstResumoDnsBean.get(i).getRedn_ds_destinationprovincecode());
				pst.setString(12, lstResumoDnsBean.get(i).getRedn_ds_grossweight());
				pst.setString(13, lstResumoDnsBean.get(i).getRedn_ds_grossvolume());
				pst.setString(14, lstResumoDnsBean.get(i).getRedn_ds_cases());
				pst.setString(15, lstResumoDnsBean.get(i).getRedn_ds_projetos());
				pst.setString(16, lstResumoDnsBean.get(i).getRedn_ds_modelplann());
				pst.setString(17, lstResumoDnsBean.get(i).getRedn_ds_equipamentogroup());
				pst.setString(18, nmArquivo);
				
				pst.execute();
				pst.close();
				
				log.info("[insertResumoDns] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[insertResumoDns] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		//conn.close();
		
		log.info("[insertResumoDns] - FIM ");
		
	}
}
