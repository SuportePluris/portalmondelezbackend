package br.com.portalpluris.bean;

public class EmbarqueAgendamentoBean {
	
	int daag_id_cd_agendamento;
    int daag_id_cd_embarque;
    int emba_id_cd_status;
    String daag_dh_agendamento;
    int daag_id_cd_motivo;
    int daag_id_cd_responsavel;
    String daag_ds_tipoagendamento;
    String daag_ds_senhaagendamento;
    String emba_in_cutoff;
    int emba_id_cd_motivo;
    int emba_id_cd_responsavel;
    String stat_ds_status;
    int daag_id_cd_usuario;
    String emba_dh_leadtime;
    String moti_ds_motivo;
    String resp_nm_responsavel;
    String daag_ds_horaagendamento;
    
    
	public String getDaag_ds_horaagendamento() {
		return daag_ds_horaagendamento;
	}
	public void setDaag_ds_horaagendamento(String daag_ds_horaagendamento) {
		this.daag_ds_horaagendamento = daag_ds_horaagendamento;
	}
	public String getMoti_ds_motivo() {
		return moti_ds_motivo;
	}
	public void setMoti_ds_motivo(String moti_ds_motivo) {
		this.moti_ds_motivo = moti_ds_motivo;
	}
	public String getResp_nm_responsavel() {
		return resp_nm_responsavel;
	}
	public void setResp_nm_responsavel(String resp_nm_responsavel) {
		this.resp_nm_responsavel = resp_nm_responsavel;
	}
	public int getDaag_id_cd_agendamento() {
		return daag_id_cd_agendamento;
	}
	public void setDaag_id_cd_agendamento(int daag_id_cd_agendamento) {
		this.daag_id_cd_agendamento = daag_id_cd_agendamento;
	}
	public int getDaag_id_cd_embarque() {
		return daag_id_cd_embarque;
	}
	public void setDaag_id_cd_embarque(int daag_id_cd_embarque) {
		this.daag_id_cd_embarque = daag_id_cd_embarque;
	}
	public int getEmba_id_cd_status() {
		return emba_id_cd_status;
	}
	public void setEmba_id_cd_status(int emba_id_cd_status) {
		this.emba_id_cd_status = emba_id_cd_status;
	}
	public String getDaag_dh_agendamento() {
		return daag_dh_agendamento;
	}
	public void setDaag_dh_agendamento(String daag_dh_agendamento) {
		this.daag_dh_agendamento = daag_dh_agendamento;
	}
	public int getDaag_id_cd_motivo() {
		return daag_id_cd_motivo;
	}
	public void setDaag_id_cd_motivo(int daag_id_cd_motivo) {
		this.daag_id_cd_motivo = daag_id_cd_motivo;
	}
	public int getDaag_id_cd_responsavel() {
		return daag_id_cd_responsavel;
	}
	public void setDaag_id_cd_responsavel(int daag_id_cd_responsavel) {
		this.daag_id_cd_responsavel = daag_id_cd_responsavel;
	}
	public String getDaag_ds_tipoagendamento() {
		return daag_ds_tipoagendamento;
	}
	public void setDaag_ds_tipoagendamento(String daag_ds_tipoagendamento) {
		this.daag_ds_tipoagendamento = daag_ds_tipoagendamento;
	}
	public String getDaag_ds_senhaagendamento() {
		return daag_ds_senhaagendamento;
	}
	public void setDaag_ds_senhaagendamento(String daag_ds_senhaagendamento) {
		this.daag_ds_senhaagendamento = daag_ds_senhaagendamento;
	}
	public String getEmba_in_cutoff() {
		return emba_in_cutoff;
	}
	public void setEmba_in_cutoff(String emba_in_cutoff) {
		this.emba_in_cutoff = emba_in_cutoff;
	}
	public int getEmba_id_cd_motivo() {
		return emba_id_cd_motivo;
	}
	public void setEmba_id_cd_motivo(int emba_id_cd_motivo) {
		this.emba_id_cd_motivo = emba_id_cd_motivo;
	}
	public int getEmba_id_cd_responsavel() {
		return emba_id_cd_responsavel;
	}
	public void setEmba_id_cd_responsavel(int emba_id_cd_responsavel) {
		this.emba_id_cd_responsavel = emba_id_cd_responsavel;
	}
	public String getStat_ds_status() {
		return stat_ds_status;
	}
	public void setStat_ds_status(String stat_ds_status) {
		this.stat_ds_status = stat_ds_status;
	}
	public String getEmba_dh_leadtime() {
		return emba_dh_leadtime;
	}
	public void setEmba_dh_leadtime(String emba_dh_leadtime) {
		this.emba_dh_leadtime = emba_dh_leadtime;
	}
	public int getDaag_id_cd_usuario() {
		return daag_id_cd_usuario;
	}
	public void setDaag_id_cd_usuario(int daag_id_cd_usuario) {
		this.daag_id_cd_usuario = daag_id_cd_usuario;
	}
    
}
