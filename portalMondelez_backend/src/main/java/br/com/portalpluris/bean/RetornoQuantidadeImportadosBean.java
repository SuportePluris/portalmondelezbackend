package br.com.portalpluris.bean;

public class RetornoQuantidadeImportadosBean {
	
	private String qntdNotaFiscal;
	private String qntdShipment;
	private String qtndOrde;
	private String qtdSkud;
	private String qntdEmba;
	
	
	
	
	public String getQtdSkud() {
		return qtdSkud;
	}
	public void setQtdSkud(String qtdSkud) {
		this.qtdSkud = qtdSkud;
	}
	public String getQntdEmba() {
		return qntdEmba;
	}
	public void setQntdEmba(String qntdEmba) {
		this.qntdEmba = qntdEmba;
	}
	public String getQntdNotaFiscal() {
		return qntdNotaFiscal;
	}
	public void setQntdNotaFiscal(String qntdNotaFiscal) {
		this.qntdNotaFiscal = qntdNotaFiscal;
	}
	public String getQntdShipment() {
		return qntdShipment;
	}
	public void setQntdShipment(String qntdShipment) {
		this.qntdShipment = qntdShipment;
	}
	public String getQtndOrde() {
		return qtndOrde;
	}
	public void setQtndOrde(String qtndOrde) {
		this.qtndOrde = qtndOrde;
	}
	
	

}
