package br.com.portalpluris.bean;

public class ContatosBean {
	
	
	private String cont_ds_tipocontato; 
	private String cont_nm_nomecontato;
	private String cont_ds_emailcontato;
	private String cont_ds_telefonecontato;
	private String cont_ds_codigocontato;
	private String user_id_cd_usuario;
	private String cont_ds_inativo;
	private String cont_id_cd_usuarioatualizacao;
	private String cont_dh_atualizacao;
	
	
	public String getCont_ds_tipocontato() {
		return cont_ds_tipocontato;
	}
	public void setCont_ds_tipocontato(String cont_ds_tipocontato) {
		this.cont_ds_tipocontato = cont_ds_tipocontato;
	}
	public String getCont_nm_nomecontato() {
		return cont_nm_nomecontato;
	}
	public void setCont_nm_nomecontato(String cont_nm_nomecontato) {
		this.cont_nm_nomecontato = cont_nm_nomecontato;
	}
	public String getCont_ds_emailcontato() {
		return cont_ds_emailcontato;
	}
	public void setCont_ds_emailcontato(String cont_ds_emailcontato) {
		this.cont_ds_emailcontato = cont_ds_emailcontato;
	}
	public String getCont_ds_telefonecontato() {
		return cont_ds_telefonecontato;
	}
	public void setCont_ds_telefonecontato(String cont_ds_telefonecontato) {
		this.cont_ds_telefonecontato = cont_ds_telefonecontato;
	}
	public String getCont_ds_codigocontato() {
		return cont_ds_codigocontato;
	}
	public void setCont_ds_codigocontato(String cont_ds_codigocontato) {
		this.cont_ds_codigocontato = cont_ds_codigocontato;
	}
	public String getUser_id_cd_usuario() {
		return user_id_cd_usuario;
	}
	public void setUser_id_cd_usuario(String user_id_cd_usuario) {
		this.user_id_cd_usuario = user_id_cd_usuario;
	}
	public String getCont_ds_inativo() {
		return cont_ds_inativo;
	}
	public void setCont_ds_inativo(String cont_ds_inativo) {
		this.cont_ds_inativo = cont_ds_inativo;
	}
	public String getCont_id_cd_usuarioatualizacao() {
		return cont_id_cd_usuarioatualizacao;
	}
	public void setCont_id_cd_usuarioatualizacao(
			String cont_id_cd_usuarioatualizacao) {
		this.cont_id_cd_usuarioatualizacao = cont_id_cd_usuarioatualizacao;
	}
	public String getCont_dh_atualizacao() {
		return cont_dh_atualizacao;
	}
	public void setCont_dh_atualizacao(String cont_dh_atualizacao) {
		this.cont_dh_atualizacao = cont_dh_atualizacao;
	}
	
	
	
	

}
