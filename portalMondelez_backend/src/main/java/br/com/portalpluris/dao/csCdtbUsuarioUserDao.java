package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.security.config.annotation.web.configurers.AbstractConfigAttributeRequestMatcherRegistry;

import br.com.portalpluris.bean.FuncionarioBean;
import br.com.portalpluris.bean.LoginBean;
import br.com.portalpluris.bean.RegiaoCanalBean;
import br.com.portalpluris.bean.RetornoBean;
import br.com.portalpluris.bean.CsCdtbCanalCanaBean;
import br.com.portalpluris.bean.CsCdtbUsuarioUserBean;

public class csCdtbUsuarioUserDao {

	public CsCdtbUsuarioUserBean findUserByUser(int userIdCdUsuario) {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		ResultSet ret = null;

		CsCdtbUsuarioUserBean loginBean = new CsCdtbUsuarioUserBean();
		
		try {
			PreparedStatement pst;

			pst = conn.prepareStatement("" 
					+ " SELECT "
					+ " 	USUA.USER_ID_CD_USUARIO, "
					+ "		FORMAT (USUA.USER_DH_REGISTRO, 'dd/MM/yyyy ') AS USER_DH_REGISTRO,"
					+ " 	USUA.USER_DS_NOMEUSUARIO, "
					+ " 	USUA.USER_DS_LOGIN, "
					+ " 	USUA.USER_DS_SENHA, "
					+ " 	USUA.USER_DS_EMAIL, "
					+ " 	USUA.USER_DS_PERMISSIONAMENTO, "
					+ " 	CASE WHEN USUA.USER_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS USER_IN_INATIVO, "
					+ " 	USUA.USER_ID_CD_USUARIOATUALIZACAO, "
					+ " 	FORMAT (USUA.USER_DH_ATUALIZACAO, 'dd/MM/yyyy ') AS USER_DH_ATUALIZACAO "
					+ " FROM "
					+ " 	CS_CDTB_USUARIO_USER USUA (NOLOCK) "
					+ " WHERE "
					+ " 	USUA.USER_ID_CD_USUARIO = ? "
					+ " ");

			pst.setInt(1, userIdCdUsuario);

			ret = pst.executeQuery();
			
			

			if (ret.next()) {

				loginBean = populaBeanLogin(ret);
			}

			//conn.close();
			pst.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return loginBean;
	}
	
	public CsCdtbUsuarioUserBean getLogin(CsCdtbUsuarioUserBean loginBean) {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		ResultSet ret = null;

		try {
			PreparedStatement pst;

			pst = conn.prepareStatement("" 
					+ " SELECT "
					+ " 	USUA.USER_ID_CD_USUARIO, "
					+ "		FORMAT (USUA.USER_DH_REGISTRO, 'dd/MM/yyyy ') AS USER_DH_REGISTRO,"
					+ " 	USUA.USER_DS_NOMEUSUARIO, "
					+ " 	USUA.USER_DS_LOGIN, "
					+ " 	USUA.USER_DS_SENHA, "
					+ " 	USUA.USER_DS_EMAIL, "
					+ " 	USUA.USER_DS_PERMISSIONAMENTO, "
					+ " 	CASE WHEN USUA.USER_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS USER_IN_INATIVO, "
					+ " 	USUA.USER_ID_CD_USUARIOATUALIZACAO, "
					+ " 	FORMAT (USUA.USER_DH_ATUALIZACAO, 'dd/MM/yyyy ') AS USER_DH_ATUALIZACAO "
					+ " FROM "
					+ " 	CS_CDTB_USUARIO_USER USUA (NOLOCK) "
					+ " WHERE "
					+ " 	USUA.USER_DS_LOGIN = ? "
					+ " 	AND USUA.USER_DS_SENHA = ? "
					+ " 	AND USUA.USER_IN_INATIVO = 'N' "
					+ " ");

			pst.setString(1, loginBean.getUser_ds_login());
			pst.setString(2, loginBean.getUser_ds_senha());

			ret = pst.executeQuery();

			if(ret.next()) {
				loginBean = populaBeanLogin(ret);				
			}

			//conn.close();
			pst.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return loginBean;
	}

	public List<CsCdtbUsuarioUserBean> getAllUser() {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		ResultSet ret = null;
		List<CsCdtbUsuarioUserBean> usuariosList = new ArrayList<CsCdtbUsuarioUserBean>();
		try {

			PreparedStatement pst;

			pst = conn.prepareStatement("" 
					+ " SELECT "
					+ " 	USUA.USER_ID_CD_USUARIO, "
					+ "		FORMAT (USUA.USER_DH_REGISTRO, 'dd/MM/yyyy ') AS USER_DH_REGISTRO,"
					+ " 	USUA.USER_DS_NOMEUSUARIO, "
					+ " 	USUA.USER_DS_LOGIN, "
					+ " 	USUA.USER_DS_SENHA, "
					+ " 	USUA.USER_DS_EMAIL, "
					+ " 	USUA.USER_DS_PERMISSIONAMENTO, "
					+ " 	CASE WHEN USUA.USER_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS USER_IN_INATIVO, "
					+ " 	USUA.USER_ID_CD_USUARIOATUALIZACAO, "
					+ " 	FORMAT (USUA.USER_DH_ATUALIZACAO, 'dd/MM/yyyy ') AS USER_DH_ATUALIZACAO "
					+ " FROM "
					+ " 	CS_CDTB_USUARIO_USER USUA (NOLOCK) "
					+ "	ORDER BY USUA.USER_DS_NOMEUSUARIO ");

			ret = pst.executeQuery();

			while (ret.next()) {

				usuariosList.add(populaBeanLogin(ret));
			}

			//conn.close();
			pst.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return usuariosList;
	}

	public CsCdtbUsuarioUserBean selectUserByUserName(String username) {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		ResultSet ret = null;

		CsCdtbUsuarioUserBean loginBean = new CsCdtbUsuarioUserBean();

		try {
			PreparedStatement pst;

			pst = conn.prepareStatement("" 
					+ " SELECT "
					+ " 	USUA.USER_ID_CD_USUARIO, "
					+ "		FORMAT (USUA.USER_DH_REGISTRO, 'dd/MM/yyyy ') AS USER_DH_REGISTRO,"
					+ " 	USUA.USER_DS_NOMEUSUARIO, "
					+ " 	USUA.USER_DS_LOGIN, "
					+ " 	USUA.USER_DS_SENHA, "
					+ " 	USUA.USER_DS_EMAIL, "
					+ " 	USUA.USER_DS_PERMISSIONAMENTO, "
					+ " 	CASE WHEN USUA.USER_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS USER_IN_INATIVO, "
					+ " 	USUA.USER_ID_CD_USUARIOATUALIZACAO, "
					+ " 	FORMAT (USUA.USER_DH_ATUALIZACAO, 'dd/MM/yyyy ') AS USER_DH_ATUALIZACAO "
					+ " FROM "
					+ " 	CS_CDTB_USUARIO_USER USUA (NOLOCK) "
					+ "  WHERE  " 
					+ "  	USUA.USER_DS_LOGIN = ? "
					// +" AND USER_IN_INATIVO = 'N' "
					+ "  ORDER BY USUA.USER_DS_NOMEUSUARIO " + " ");

			pst.setString(1, username);
			// pst.setString(2, "S");

			ret = pst.executeQuery();

			if (ret.next()) {

				loginBean = populaBeanLogin(ret);
			}

			//conn.close();
			pst.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return loginBean;
	}

	private CsCdtbUsuarioUserBean populaBeanLogin(ResultSet retUser) throws SQLException {

		CsCdtbUsuarioUserBean funcBean = new CsCdtbUsuarioUserBean();

		funcBean.setUser_id_cd_usuario(retUser.getString("USER_ID_CD_USUARIO") == null ? "0" : retUser.getString("USER_ID_CD_USUARIO"));
		funcBean.setUser_ds_nomeusuario(retUser.getString("USER_DS_NOMEUSUARIO"));
		funcBean.setUser_ds_login(retUser.getString("USER_DS_LOGIN"));
		funcBean.setUser_ds_senha(retUser.getString("USER_DS_SENHA"));
		funcBean.setUser_ds_email(retUser.getString("USER_DS_EMAIL"));
		funcBean.setUser_dh_registro(retUser.getString("USER_DH_REGISTRO"));
		funcBean.setUser_ds_permissionamento(retUser.getString("USER_DS_PERMISSIONAMENTO"));
		funcBean.setUser_in_inativo(retUser.getString("USER_IN_INATIVO"));
		
		List<RegiaoCanalBean> lstRegiaoCanalBean = new ArrayList<RegiaoCanalBean>();
		
		ResultSet retRegiaoCanal = getRegiaoCanalByUser(retUser.getString("USER_ID_CD_USUARIO"));
		
		while(retRegiaoCanal.next()) {
			RegiaoCanalBean regiaoCanalBean = new RegiaoCanalBean();

			regiaoCanalBean.setUrcs_id_regi_cd_regiao(retRegiaoCanal.getString("ID_REGI_CD_REGIAO"));
			regiaoCanalBean.setUrcs_id_cd_canal(retRegiaoCanal.getString("CANA_ID_CD_CANAL"));
			
			try {
				regiaoCanalBean.setRegi_ds_regiao(retRegiaoCanal.getString("REGI_DS_REGIAO") == null ? "" : retRegiaoCanal.getString("REGI_DS_REGIAO"));
				regiaoCanalBean.setCana_ds_canal(retRegiaoCanal.getString("CANA_DS_CANAL") == null ? "" : retRegiaoCanal.getString("CANA_DS_CANAL"));
			}catch (Exception e) {
				regiaoCanalBean.setRegi_ds_regiao("");
				regiaoCanalBean.setCana_ds_canal("");
			}

			lstRegiaoCanalBean.add(regiaoCanalBean);
		}
		
		
		funcBean.setLstRegiaoCanal(lstRegiaoCanalBean);
		
		return funcBean;

	}

	public ResultSet getRegiaoCanalByUser(String urcs_id_cd_usuario) throws SQLException {

		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = conn.prepareStatement(""
				+ " SELECT "
				+ " 	CANA.CANA_ID_CD_CANAL, "
				+ "  	CANA.CANA_DS_CANAL, "
				+ " 	REGI.ID_REGI_CD_REGIAO, "
				+ "  	REGI.REGI_DS_REGIAO "
				+ "  FROM "
				+ "  	CS_ASTB_USERREGIAOCANAL_URCS URCS (NOLOCK) "
				+ "  	LEFT JOIN CS_CDTB_CANAL_CANA CANA (NOLOCK) "
				+ "  		ON URCS.URCS_ID_CD_CANAL = CANA.CANA_ID_CD_CANAL "
				+ "  	LEFT JOIN CS_CDTB_REGIAO_REGI REGI (NOLOCK) "
				+ "  		ON URCS.URCS_ID_CD_REGIAO = REGI.ID_REGI_CD_REGIAO "
				+ "	WHERE"
				+ "		URCS.URCS_ID_CD_USUARIO = ?"
				+ "");
		
		pst.setString(1, urcs_id_cd_usuario);
		
		ResultSet rs = pst.executeQuery();
		
		return rs;
	}

	public RetornoBean getCreateUser(CsCdtbUsuarioUserBean funcBean) {

		RetornoBean retBean = new RetornoBean();
		retBean.setProcesso("insert");

		CsCdtbUsuarioUserBean funcBeanRet = getLogin(funcBean);

		if (funcBeanRet.getUser_id_cd_usuario() == null) {

			DAO dao = new DAO();
			Connection conn = dao.getConexao();

			try {
				PreparedStatement pst;

				pst = conn.prepareStatement("" 
				+ " INSERT INTO CS_CDTB_USUARIO_USER (" 
						+ " 	USER_DS_NOMEUSUARIO, "
						+ " 	USER_DS_LOGIN, " 
						+ " 	USER_DS_SENHA, " 
						+ " 	USER_DS_EMAIL, " 
						+ " 	USER_DS_PERMISSIONAMENTO, " 
						+ " 	USER_IN_INATIVO, "
						+ " 	USER_ID_CD_USUARIOATUALIZACAO," 
						+ " )VALUES (?, ?, ?, ?, ?, ?, ?)" 
						+ "");

				pst.setString(1, funcBean.getUser_ds_nomeusuario().toUpperCase());
				pst.setString(2, funcBean.getUser_ds_login().toUpperCase());
				pst.setString(3, funcBean.getUser_ds_senha().toUpperCase());
				pst.setString(4, funcBean.getUser_ds_email());
				pst.setString(5, funcBean.getUser_ds_permissionamento().toUpperCase());
				pst.setString(6, funcBean.getUser_in_inativo().toUpperCase());
				pst.setLong(7, funcBean.getUser_id_cd_usuarioatualizacao() == "" ? 0L
						: Long.parseLong(funcBean.getUser_id_cd_usuarioatualizacao()));
				
				pst.execute();

				funcBean.setUser_id_cd_usuario(getIdByUser(funcBean.getUser_ds_login()));

				//conn.close();
				pst.close();
				
				criaUsuarioRegiaoCanal(funcBean);

				retBean.setSucesso(true);
				retBean.setMsg("Usuário cadastrado com sucesso");

			} catch (Exception e) {
				e.printStackTrace();
				retBean.setSucesso(false);
				retBean.setMsg(e.getMessage());
			}

		} else {
			retBean.setSucesso(false);
			retBean.setMsg("Usuário já cadastrado");
		}

		return retBean;
	}

	private void criaUsuarioRegiaoCanal(CsCdtbUsuarioUserBean funcBean) throws SQLException {

		String idUser = "";
		
		if(funcBean.getUser_id_cd_usuario().equals("")) {
			idUser = getIdByUser(funcBean.getUser_ds_login());
			
		}else {
			idUser = funcBean.getUser_id_cd_usuario();
		}
		
		Connection conn = DAO.getConexao();
		PreparedStatement pst = null;
		
		for (int i = 0; i < funcBean.getLstRegiaoCanal().size() ; i++) {
		
			if(funcBean.getLstRegiaoCanal().get(i).getUrcs_id_cd_canal().equals("9999")) {
				
				deleteRegiaoUsuario(idUser, funcBean.getLstRegiaoCanal().get(i).getUrcs_id_regi_cd_regiao());
				
				CsCdtbCanalCanaDao canaDao = new CsCdtbCanalCanaDao();
				List<CsCdtbCanalCanaBean> lstCana = canaDao.getListCanal("N");
				
				for (CsCdtbCanalCanaBean canaBean : lstCana) {
					
					pst = conn.prepareStatement(""
							+ " INSERT INTO CS_ASTB_USERREGIAOCANAL_URCS "
							+ " VALUES (?, ?, ?) "
							+ "");
					pst.setString(1, idUser);
					pst.setString(2, funcBean.getLstRegiaoCanal().get(i).getUrcs_id_regi_cd_regiao());
					pst.setString(3, canaBean.getCana_id_cd_canal());
					
					pst.execute();
				}
				
			}else {
				
				if(existeUrcs(idUser, funcBean.getLstRegiaoCanal().get(i).getUrcs_id_regi_cd_regiao(), funcBean.getLstRegiaoCanal().get(i).getUrcs_id_cd_canal())) {
					continue;
					
				}else {
					
					pst = conn.prepareStatement(""
							+ " INSERT INTO CS_ASTB_USERREGIAOCANAL_URCS "
							+ " VALUES (?, ?, ?) "
							+ "");
					pst.setString(1, idUser);
					pst.setString(2, funcBean.getLstRegiaoCanal().get(i).getUrcs_id_regi_cd_regiao());
					pst.setString(3, funcBean.getLstRegiaoCanal().get(i).getUrcs_id_cd_canal());
					
					pst.execute();
				}
			}
		}
		
		if(pst != null) {
			pst.close();
		}
	}
	
	public boolean existeUrcs(String user_id_cd_usuario, String urcs_id_regi_cd_regiao, String urcs_id_cd_canal) throws SQLException {
		
		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;

			
		pst = conn.prepareStatement(""
				+ " SELECT "
				+ "		URCS_ID_CD_USUARIO, "
				+ "		URCS_ID_CD_REGIAO, "
				+ "		URCS_ID_CD_CANAL "
				+ "	FROM "
				+ "		CS_ASTB_USERREGIAOCANAL_URCS "
				+ " WHERE "
				+ "		URCS_ID_CD_USUARIO = ? "
				+ " 	AND URCS_ID_CD_REGIAO = ? "
				+ "		AND URCS_ID_CD_CANAL = ? ");
		
		pst.setString(1, user_id_cd_usuario);
		pst.setString(2, urcs_id_regi_cd_regiao);
		pst.setString(3, urcs_id_cd_canal);
		
		ResultSet rs =pst.executeQuery();
		
		if(rs.next()) {
			return true;
		}else {
			return false;	
		}
		
	}
	
	private void deleteRegiaoUsuario(String user_id_cd_usuario, String urcs_id_regi_cd_regiao) throws SQLException {

		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;

			
		pst = conn.prepareStatement(""
				+ " DELETE "
				+ "		CS_ASTB_USERREGIAOCANAL_URCS "
				+ " WHERE "
				+ "		URCS_ID_CD_USUARIO = ? "
				+ " 	AND URCS_ID_CD_REGIAO = ? ");
		
		pst.setString(1, user_id_cd_usuario);
		pst.setString(2, urcs_id_regi_cd_regiao);
		
		pst.executeUpdate();
		pst.close();
		
	}

	public RetornoBean getUpdateUser(CsCdtbUsuarioUserBean funcBean) {

		RetornoBean retBean = new RetornoBean();
		retBean.setProcesso("update");
		// csCdtbUsuarioUserBean funcBeanRet = getLogin(funcBean);

		// if(funcBeanRet.getUser_id_cd_usuario() == null){

		DAO dao = new DAO();
		Connection conn = dao.getConexao();

		try {
			PreparedStatement pst;

			pst = conn.prepareStatement("" 
			+ " UPDATE CS_CDTB_USUARIO_USER SET " 
					+ " 	USER_DS_NOMEUSUARIO = ?, "
					+ " 	USER_DS_LOGIN = ?, " 
					+ " 	USER_DS_SENHA = ?, " 
					+ " 	USER_DS_EMAIL = ?, " 
					+ " 	USER_DS_PERMISSIONAMENTO = ?, " 
					+ " 	USER_IN_INATIVO = ?, "
					+ " 	USER_ID_CD_USUARIOATUALIZACAO = ? " 
					+ " WHERE " 
					+ "		USER_ID_CD_USUARIO = ? " 
					+ "");

			String inativo = "";
			if (funcBean.getUser_in_inativo().trim().equalsIgnoreCase("Não")) {
				inativo = "N";
			} else if (funcBean.getUser_in_inativo().trim().equalsIgnoreCase("Sim")) {
				inativo = "S";
			} else {
				inativo = funcBean.getUser_in_inativo().trim();
			}

			pst.setString(1, funcBean.getUser_ds_nomeusuario().trim().toUpperCase());
			pst.setString(2, funcBean.getUser_ds_login().trim().toUpperCase());
			pst.setString(3, funcBean.getUser_ds_senha().trim().toUpperCase());
			pst.setString(4, funcBean.getUser_ds_email().trim().toUpperCase());
			pst.setString(5, funcBean.getUser_ds_permissionamento().trim().toUpperCase());
			pst.setString(6, inativo.toUpperCase());
			pst.setLong(7, funcBean.getUser_id_cd_usuarioatualizacao() == "" ? 0L
					: Long.parseLong(funcBean.getUser_id_cd_usuarioatualizacao()));
			pst.setString(8, funcBean.getUser_id_cd_usuario());

			pst.executeUpdate();

			// funcBean.setUser_id_cd_usuario(getIdByUser(funcBean.getUser_ds_login()));

			//conn.close();
			pst.close();
			
			updateUsuarioRegiaoContato(funcBean);

			retBean.setSucesso(true);
			retBean.setMsg("Usuário atualizado");

		} catch (Exception e) {
			e.printStackTrace();
			retBean.setSucesso(false);
			retBean.setMsg(e.getMessage());
		}

		return retBean;
	}

	private void updateUsuarioRegiaoContato(CsCdtbUsuarioUserBean funcBean) throws SQLException {

		Connection conn = DAO.getConexao();
		
		PreparedStatement pst = null;

			
		pst = conn.prepareStatement(""
				+ " DELETE "
				+ "		CS_ASTB_USERREGIAOCANAL_URCS "
				+ " WHERE "
				+ "		URCS_ID_CD_USUARIO = ? ");
		
		pst.setString(1, funcBean.getUser_id_cd_usuario());
		
		pst.executeUpdate();
		pst.close();
		
		criaUsuarioRegiaoCanal(funcBean);
		
	}

	private String getIdByUser(String user) throws SQLException {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		PreparedStatement pst;
		ResultSet ret = null;

		String id = "0";

		pst = conn.prepareStatement("" + " SELECT " + "		USER_ID_CD_USUARIO " + " FROM "
				+ "		CS_CDTB_USUARIO_USER (NOLOCK)" + " 	WHERE " + " 		USER_DS_LOGIN = ? " + "");

		pst.setString(1, user);
		ret = pst.executeQuery();

		if (ret.next()) {

			id = ret.getString("USER_ID_CD_USUARIO");
		}

		//conn.close();
		pst.close();

		return id;
	}

	public void inativaUsuario(long id) throws SQLException {

		DAO dao = new DAO();
		Connection conn = dao.getConexao();
		PreparedStatement pst;

		pst = conn.prepareStatement("" + " UPDATE CS_CDTB_USUARIO_USER " + " SET " + "		USER_IN_INATIVO = ?"
				+ " WHERE" + "		USER_ID_CD_USUARIO = ?" + "");

		pst.setString(1, "N");
		pst.setLong(2, id);
		pst.execute();

		//conn.close();
		pst.close();
	}
}
