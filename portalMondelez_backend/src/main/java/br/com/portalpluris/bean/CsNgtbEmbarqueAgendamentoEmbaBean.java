package br.com.portalpluris.bean;

import java.util.Date;

public class CsNgtbEmbarqueAgendamentoEmbaBean {
	
	private String emba_id_cd_embarque;
	private String emba_id_cd_shipment;
	private Date emba_dh_registro;
	private String emba_ds_cod_transportadora;
	private String emba_id_cd_codcliente;
	private Date emba_dh_leadtime;
	private String emba_ds_planta;
	private Date emba_dh_shipment;
	private String emba_ds_veiculo;
	private String emba_ds_tipoveiculo;
	private String emba_in_transportadoraconfirmada;
	private Date emba_dh_atualizacao;
	private String emba_id_cd_usuarioatualizacao;
	private String emba_ds_periodo;
	private String emba_in_cutoff;
	private String emba_ds_motivocutoff;
	private String emba_ds_responsavelcutoff;
	private String emba_in_uso;
	private String emba_id_cd_status;
	private String emba_ds_tipocarga;
	private String emba_ds_tipoagendamento;
	private String emba_ds_salesdocument;
	private String emba_ds_destcity;
	private String updt_dh_leadtime;
	
	public String getUpdt_dh_leadtime() {
		return updt_dh_leadtime;
	}
	public void setUpdt_dh_leadtime(String updt_dh_leadtime) {
		this.updt_dh_leadtime = updt_dh_leadtime;
	}
	public String getEmba_id_cd_embarque() {
		return emba_id_cd_embarque;
	}
	public void setEmba_id_cd_embarque(String emba_id_cd_embarque) {
		this.emba_id_cd_embarque = emba_id_cd_embarque;
	}
	public String getEmba_id_cd_shipment() {
		return emba_id_cd_shipment;
	}
	public void setEmba_id_cd_shipment(String emba_id_cd_shipment) {
		this.emba_id_cd_shipment = emba_id_cd_shipment;
	}
	public Date getEmba_dh_registro() {
		return emba_dh_registro;
	}
	public void setEmba_dh_registro(Date emba_dh_registro) {
		this.emba_dh_registro = emba_dh_registro;
	}
	public String getEmba_ds_cod_transportadora() {
		return emba_ds_cod_transportadora;
	}
	public void setEmba_ds_cod_transportadora(String emba_ds_cod_transportadora) {
		this.emba_ds_cod_transportadora = emba_ds_cod_transportadora;
	}
	public String getEmba_id_cd_codcliente() {
		return emba_id_cd_codcliente;
	}
	public void setEmba_id_cd_codcliente(String emba_id_cd_codcliente) {
		this.emba_id_cd_codcliente = emba_id_cd_codcliente;
	}
	public Date getEmba_dh_leadtime() {
		return emba_dh_leadtime;
	}
	public void setEmba_dh_leadtime(Date emba_dh_leadtime) {
		this.emba_dh_leadtime = emba_dh_leadtime;
	}
	public String getEmba_ds_planta() {
		return emba_ds_planta;
	}
	public void setEmba_ds_planta(String emba_ds_planta) {
		this.emba_ds_planta = emba_ds_planta;
	}
	public Date getEmba_dh_shipment() {
		return emba_dh_shipment;
	}
	public void setEmba_dh_shipment(Date emba_dh_shipment) {
		this.emba_dh_shipment = emba_dh_shipment;
	}
	public String getEmba_ds_veiculo() {
		return emba_ds_veiculo;
	}
	public void setEmba_ds_veiculo(String emba_ds_veiculo) {
		this.emba_ds_veiculo = emba_ds_veiculo;
	}
	public String getEmba_ds_tipoveiculo() {
		return emba_ds_tipoveiculo;
	}
	public void setEmba_ds_tipoveiculo(String emba_ds_tipoveiculo) {
		this.emba_ds_tipoveiculo = emba_ds_tipoveiculo;
	}
	public String getEmba_in_transportadoraconfirmada() {
		return emba_in_transportadoraconfirmada;
	}
	public void setEmba_in_transportadoraconfirmada(
			String emba_in_transportadoraconfirmada) {
		this.emba_in_transportadoraconfirmada = emba_in_transportadoraconfirmada;
	}
	public Date getEmba_dh_atualizacao() {
		return emba_dh_atualizacao;
	}
	public void setEmba_dh_atualizacao(Date emba_dh_atualizacao) {
		this.emba_dh_atualizacao = emba_dh_atualizacao;
	}
	public String getEmba_id_cd_usuarioatualizacao() {
		return emba_id_cd_usuarioatualizacao;
	}
	public void setEmba_id_cd_usuarioatualizacao(
			String emba_id_cd_usuarioatualizacao) {
		this.emba_id_cd_usuarioatualizacao = emba_id_cd_usuarioatualizacao;
	}
	public String getEmba_ds_periodo() {
		return emba_ds_periodo;
	}
	public void setEmba_ds_periodo(String emba_ds_periodo) {
		this.emba_ds_periodo = emba_ds_periodo;
	}
	public String getEmba_in_cutoff() {
		return emba_in_cutoff;
	}
	public void setEmba_in_cutoff(String emba_in_cutoff) {
		this.emba_in_cutoff = emba_in_cutoff;
	}
	public String getEmba_ds_motivocutoff() {
		return emba_ds_motivocutoff;
	}
	public void setEmba_ds_motivocutoff(String emba_ds_motivocutoff) {
		this.emba_ds_motivocutoff = emba_ds_motivocutoff;
	}
	public String getEmba_ds_responsavelcutoff() {
		return emba_ds_responsavelcutoff;
	}
	public void setEmba_ds_responsavelcutoff(String emba_ds_responsavelcutoff) {
		this.emba_ds_responsavelcutoff = emba_ds_responsavelcutoff;
	}
	public String getEmba_in_uso() {
		return emba_in_uso;
	}
	public void setEmba_in_uso(String emba_in_uso) {
		this.emba_in_uso = emba_in_uso;
	}
	public String getEmba_id_cd_status() {
		return emba_id_cd_status;
	}
	public void setEmba_id_cd_status(String emba_id_cd_status) {
		this.emba_id_cd_status = emba_id_cd_status;
	}
	public String getEmba_ds_tipocarga() {
		return emba_ds_tipocarga;
	}
	public void setEmba_ds_tipocarga(String emba_ds_tipocarga) {
		this.emba_ds_tipocarga = emba_ds_tipocarga;
	}
	public String getEmba_ds_tipoagendamento() {
		return emba_ds_tipoagendamento;
	}
	public void setEmba_ds_tipoagendamento(String emba_ds_tipoagendamento) {
		this.emba_ds_tipoagendamento = emba_ds_tipoagendamento;
	}
	public String getEmba_ds_salesdocument() {
		return emba_ds_salesdocument;
	}
	public void setEmba_ds_salesdocument(String emba_ds_salesdocument) {
		this.emba_ds_salesdocument = emba_ds_salesdocument;
	}
	public String getEmba_ds_destcity() {
		return emba_ds_destcity;
	}
	public void setEmba_ds_destcity(String emba_ds_destcity) {
		this.emba_ds_destcity = emba_ds_destcity;
	}

}
