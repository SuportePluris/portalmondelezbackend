package br.com.portalpluris.bean;

import java.util.logging.Logger;

import br.com.portalpluris.business.CsLgbtPedidosdeliveryPediBusiness;
import br.com.portalpluris.business.CsLgtbShipmentShipBusiness;
import br.com.portalpluris.business.CsLgtbSkudeliverySkudBusiness;

public class ImportacaoCargaBean {
	
	
	/**
	 * Metodo responsavel por iniciar a importacao dos arquivos de carga de agendamento
	 * @author Caio Fernandes
	 * */
	public void iniciarImportacao() {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("[iniciarImportacao] - INICIO IMPORTACAO CARGA");
		
		try {
			
			// 1. *** IMPORTACAO SHIPMENT / RESUMO DE DNS (Planejamento de Transportes)
			log.info("[iniciarImportacao] - IMPORTACAO SHIPMENT");
			CsLgtbShipmentShipBusiness csLgtbShipmentShipBusiness = new CsLgtbShipmentShipBusiness();
			csLgtbShipmentShipBusiness.gravarShipment();
			
			// 2. *** IMPORTACAO SKUDELIVERY (DNS Alocadas)
			log.info("[iniciarImportacao] - IMPORTACAO SKU DELIVERY");
			CsLgtbSkudeliverySkudBusiness csLgtbSkudeliverySkudBusiness = new CsLgtbSkudeliverySkudBusiness();
			csLgtbSkudeliverySkudBusiness.gravarSkuDelivery();
			
			// 3. *** IMPORTACAO PEDIDOS DELIVERY (Base de Pedidos)
			log.info("[iniciarImportacao] - IMPORTACAO PEDIDOS DELIVERY");
			CsLgbtPedidosdeliveryPediBusiness csLgbtPedidosdeliveryPediBusiness = new CsLgbtPedidosdeliveryPediBusiness();
			csLgbtPedidosdeliveryPediBusiness.gravarPedidosDelivery();
			
		} catch (Exception e) {
			log.info("[iniciarImportacao] - ERRO IMPORTACAO CARGA " + e.getMessage());
		}
		
		log.info("[iniciarImportacao] - FIM IMPORTACAO CARGA");
		
	}
	
	public static void main(String[] args) {
		//iniciarImportacao();
	}

}
