package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsLgtbShipmentShipBean;
import br.com.portalpluris.bean.RetornoCargaBean;
import br.com.portalpluris.bean.RetornoQuantidadeImportadosBean;
import br.com.portalpluris.bean.RetornoValidaCargaBean;

public class CsLgtbShipmentShipDao {
	
	/**
	 * Metodo responsavel por realizar o insert das informacoes da planilha de shipment na tabela CS_LGTB_SHIPMENT_SHIP
	 * @author Caio Fernandes
	 * @param List<CsLgtbShipmentShipBusiness> lstSkuDeliveryBean
	 * */
	public void insertShipment(List<CsLgtbShipmentShipBean> lstShipmentBean, String nmArquivo) throws SQLException {
		
		Logger log = Logger.getLogger(this.getClass().getName());	
		Connection conn = DAO.getConexao();
		UtilDate util = new UtilDate();
		
		log.info("[insertShipment] - INICIO ");
		
		if(conn != null) {
			
			// FOR PELA LISTA DE PEDIDOS DELIVERYS
			for(int i = 0; i < lstShipmentBean.size(); i++) {
				
				// REALIZAR O INSERT
				log.info("[insertSkuDelivery] - REALIZANDO INSERT ");
				
				PreparedStatement pst;	
				pst = conn.prepareStatement(""
						+" INSERT CS_LGTB_SHIPMENT_SHIP ( "
						+ "		SHIP_CD_SHIPMENTID, " 	
						+ "		SHIP_DS_STOPTYPE, 	" 	
						+ "		SHIP_DS_STOPNUMBER, 	"				
						+ "		SHIP_DS_TOTALSTOPS, 	"					
						+ "		SHIP_CD_SERVICEPROVIDERID, 	"
						+ "		SHIP_DS_SERVICEPROVIDERID, 	"
						+ "		SHIP_DS_EQUIPMENTGROUP, 	"
						+ "		SHIP_DS_SOURCELOCATIONID, 	"
						+ "		SHIP_DS_SOURCELOCATIONNAME, 	"
						+ "		SHIP_DS_SOURCECITY, 	"
						+ "		SHIP_DS_SOURCEPROVINCECODE, 	"
						+ "		SHIP_DS_SOURCEZONE1, 	"
						+ "		SHIP_DS_DESTLOCATIONID, 	"
						+ "		SHIP_DS_DESTLOCATIONNAME, 	"
						+ "		SHIP_DS_DESTCITY, 	"
						+ "		SHIP_DS_PROVINCECODE, 	"
						+ "		SHIP_DS_DESTZONE1, 	"
						+ "		SHIP_DS_STOPDISTANCE, 	"
						+ "		SHIP_DS_STOPDISTANCEUOM, 	"
						+ "		SHIP_DS_STARTTIME, 	"
						+ "		SHIP_DS_WEEK, 	"
						+ "		SHIP_DS_MONTH, 	"
						+ "		SHIP_DS_DECLAREDVALUE, 	"
						+ "		SHIP_DS_NETFREIGHTAMOUNT, 	"
						+ "		SHIP_DS_NUMBERORDERRELEASES, 	"
						+ "		SHIP_DS_GROSSWEIGHT, 	"
						+ "		SHIP_DS_TOTALGROSSWEIGHT, 	"
						+ "		SHIP_DS_GROSSVOLUME, 	"
						+ "		SHIP_DS_TOTALGROSSVOLUME, 	"
						+ "		SHIP_DS_CUBEDWEIGHT, 	"
						+ "		SHIP_DS_CASES, 	"
						+ "		SHIP_DS_VOLUMEVEHICLECAPACITY, 	"
						+ "		SHIP_DS_VOLUMEVEHICLECAPCTYUOMCODE, 	"
						+ "		SHIP_DS_WEIGHTVEHICLECAPACITY, 	"
						+ "		SHIP_DS_WEIGHTVEHICLECAPCTYWOMCODE, 	"
						+ "		SHIP_DS_VOLUMEVEHICLEOCCUPANCY, 	"
						+ "		SHIP_DS_WEIGHTVEHICLEOCCUPANCY, 	"
						+ "		SHIP_DS_CORECARRIER, 	"
						+ "		SHIP_DS_PLANNEDSCAC, 	"
						+ "		SHIP_DS_PLANNEDSERVICEPROVIDERNAME, 	" 	//
						+ "		SHIP_DS_BULKPLANGID, 	"
						+ "		SHIP_DS_STATUSOFERTA, 	"
						+ "		SHIP_DS_BUYSHIPMENT, 	"
						+ "		SHIP_DS_TRUCKOUCARRETA, 	"
						+ "		SHIP_DS_CLIENTEAGENDADO, 	"
						+ "		SHIP_DS_AGENDAMENTOCOMNF, 	"
						+ "		SHIP_DS_TIPO, 	"
						+ "		SHIP_DS_TRANSPORTADORA, 	"
						+ "		SHIP_DS_CONTANDESHIPMENTS, 	"
						+ "		SHIP_DS_FAIXA, 	"
						+ "		SHIP_DS_CHAVESHIPMENT, 	"
						+ "		SHIP_DS_SHIPMENTS, 	"
						+ "		SHIP_DS_PLANTA, 	"
						+ "		SHIP_CD_CODCLIENTE, 	"
						+ "		SHIP_DS_CLIENTE, 	"
						+ "		SHIP_DS_PESO, 	"
						+ "		SHIP_DS_LIBERARCROSSDOCKING, 	"
						+ " 	SHIP_DS_NOMEARQUIVO )"
						+ "VALUES "
						+ "		(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)	"
						+ "");
				
				pst.setString(1, lstShipmentBean.get(i).getShip_cd_shipmentid());
				pst.setString(2, lstShipmentBean.get(i).getShip_ds_stoptype());
				pst.setString(3, lstShipmentBean.get(i).getShip_ds_stopnumber());
				pst.setString(4, lstShipmentBean.get(i).getShip_ds_totalstops());
				pst.setString(5, lstShipmentBean.get(i).getShip_cd_serviceproviderid());
				pst.setString(6, lstShipmentBean.get(i).getShip_ds_serviceproviderid());
				pst.setString(7, lstShipmentBean.get(i).getShip_ds_equipmentgroup());
				pst.setString(8, lstShipmentBean.get(i).getShip_ds_sourcelocationid());
				pst.setString(9, lstShipmentBean.get(i).getShip_ds_sourcelocationname());
				pst.setString(10, lstShipmentBean.get(i).getShip_ds_sourcecity());
				pst.setString(11, lstShipmentBean.get(i).getShip_ds_sourceprovincecode());
				pst.setString(12, lstShipmentBean.get(i).getShip_ds_sourcezone1());
				pst.setString(13, lstShipmentBean.get(i).getShip_ds_destlocationid());
				pst.setString(14, lstShipmentBean.get(i).getShip_ds_destlocationname());
				pst.setString(15, lstShipmentBean.get(i).getShip_ds_destcity());
				pst.setString(16, lstShipmentBean.get(i).getShip_ds_provincecode());
				pst.setString(17, lstShipmentBean.get(i).getShip_ds_destzone1());
				pst.setString(18, lstShipmentBean.get(i).getShip_ds_stopdistance());
				pst.setString(19, lstShipmentBean.get(i).getShip_ds_stopdistanceuom());
				pst.setString(20, util.convertDateToYYYY_MM_dd_HH_MM_SS(lstShipmentBean.get(i).getShip_ds_starttime()));
				pst.setString(21, lstShipmentBean.get(i).getShip_ds_week());
				pst.setString(22, lstShipmentBean.get(i).getShip_ds_month());
				pst.setString(23, lstShipmentBean.get(i).getShip_ds_declaredvalue());
				pst.setString(24, lstShipmentBean.get(i).getShip_ds_netfreightamount());
				pst.setString(25, lstShipmentBean.get(i).getShip_ds_numberorderreleases());
				pst.setString(26, lstShipmentBean.get(i).getShip_ds_grossweight());
				pst.setString(27, lstShipmentBean.get(i).getShip_ds_totalgrossweight());
				pst.setString(28, lstShipmentBean.get(i).getShip_ds_grossvolume());
				pst.setString(29, lstShipmentBean.get(i).getShip_ds_totalgrossvolume());
				pst.setString(30, lstShipmentBean.get(i).getShip_ds_cubedweight());
				pst.setString(31, lstShipmentBean.get(i).getShip_ds_cases());
				pst.setString(32, lstShipmentBean.get(i).getShip_ds_volumevehiclecapacity());
				pst.setString(33, lstShipmentBean.get(i).getShip_ds_volumevehiclecapctyuomcode());
				pst.setString(34, lstShipmentBean.get(i).getShip_ds_weightvehiclecapacity());
				pst.setString(35, lstShipmentBean.get(i).getShip_ds_weightvehiclecapctywomcode());
				pst.setString(36, lstShipmentBean.get(i).getShip_ds_volumevehicleoccupancy());
				pst.setString(37, lstShipmentBean.get(i).getShip_ds_weightvehicleoccupancy());
				pst.setString(38, lstShipmentBean.get(i).getShip_ds_corecarrier());
				pst.setString(39, lstShipmentBean.get(i).getShip_ds_plannedscac());
				pst.setString(40, lstShipmentBean.get(i).getShip_ds_plannedserviceprovidername());
				pst.setString(41, lstShipmentBean.get(i).getShip_ds_bulkplangid());
				pst.setString(42, lstShipmentBean.get(i).getShip_ds_statusoferta());
				pst.setString(43, lstShipmentBean.get(i).getShip_ds_buyshipment());				
				pst.setString(44, lstShipmentBean.get(i).getShip_ds_truckoucarreta());
				pst.setString(45, lstShipmentBean.get(i).getShip_ds_clienteagendado());
				pst.setString(46, lstShipmentBean.get(i).getShip_ds_agendamentocomnf());
				pst.setString(47, lstShipmentBean.get(i).getShip_ds_tipo());
				pst.setString(48, lstShipmentBean.get(i).getShip_ds_transportadora());
				pst.setString(49, lstShipmentBean.get(i).getShip_ds_contandeshipments());
				pst.setString(50, lstShipmentBean.get(i).getShip_ds_faixa());
				pst.setString(51, lstShipmentBean.get(i).getShip_ds_chaveshipment());
				pst.setString(52, lstShipmentBean.get(i).getShip_ds_shipments());
				pst.setString(53, lstShipmentBean.get(i).getShip_ds_planta());
				pst.setString(54, lstShipmentBean.get(i).getShip_cd_codcliente());
				pst.setString(55, lstShipmentBean.get(i).getShip_ds_cliente());
				pst.setString(56, lstShipmentBean.get(i).getShip_ds_peso());
				pst.setString(57, lstShipmentBean.get(i).getShip_ds_liberarcrossdocking());
				pst.setString(58, nmArquivo);
				
				pst.execute();
				pst.close();
				
				log.info("[insertShipment] - INSERT REALIZADO COM SUCESSO ");
				
			}
			
		} else {
			log.info("[insertShipment] - NAO FOI POSSIVEL OBTER CONEXAO COM O BANCO DE DADOS ");
		}
		
		//conn.close();
		
		log.info("[insertShipment] - FIM ");
		
	}




	public RetornoValidaCargaBean validaCarga(RetornoValidaCargaBean retCarga) {		
	
		ArrayList<RetornoCargaBean> validacaoCargaList = new ArrayList<RetornoCargaBean>();
		RetornoCargaBean validacaoCargaBean = new RetornoCargaBean();

		
		try {
			
			Connection conn = DAO.getConexao();
			ResultSet rs = null;

			PreparedStatement pst;
			
			String cSQL = 
					" SELECT "+
					" ISNULL(SHIP.SHIP_ID_CD_SHIPMENT, 'FALTA_ARQUIVO_SHIPMENT') AS 'ID_SHIPMENT', " +
					" ISNULL(SHIP.SHIP_ID_CD_DELIVERY,  'FALTA_ARQUIVO_ORDE_DELIVERY') AS 'SHIPMENT_DELIVERY', " +
					" ISNULL(NOFI.NOFI_DS_NOTAFISCALNUMBER,'FALTA_ARQUIVO_NOTA_FISCAL') AS 'NUMERO_NOTA_FISCAL', " +
					" SHIP.SHIP_DS_PEDIDOMONDELEZ AS 'PEDIDO_MONDELEZ', " +
					" ISNULL(ORDE.ORDE_DS_DELIVERY, 'FALTA_ARQUIVO_ORDE_DELIVERY') as  'ORDE_DELIVERY_NOTE',	 " +	
					" SHIP.SHIP_DH_REGISTRO AS 'SHIPMENT_DATA_REGISTRO', " +
					" ORDE_DH_REGISTRO AS 'ORDEM_DATA_REGISTRO', " +
					" NOFI.NOFI_DH_REGISTRO AS 'NOTA_FISCAL_DATA_REGISTRO'		 " +
					" FROM  CS_LGTB_SHIPMENTXML_SHIP SHIP " +
					" 	LEFT JOIN  CS_LGTB_ORDEM_ORDE ORDE " +
					" 		ON ORDE.ORDE_DS_SALESDOCUMENTNUMBER = SHIP.SHIP_DS_PEDIDOMONDELEZ  " +
					" 	LEFT JOIN CS_LGTB_NOTAFISCAL_NOFI NOFI " +
					" 			ON NOFI.NOFI_DS_SALESORDERNUMBER2 = SHIP.SHIP_DS_PEDIDOMONDELEZ ";
			
			pst = conn.prepareStatement(cSQL);		
			
			rs = pst.executeQuery();
	
			while (rs.next()) {
				
				 validacaoCargaBean = new RetornoCargaBean();
				 
				 validacaoCargaBean.setIdShipment(rs.getString("ID_SHIPMENT"));
				 validacaoCargaBean.setShipmentDelivery(rs.getString("SHIPMENT_DELIVERY"));
				 validacaoCargaBean.setNumeroNotaFiscal(rs.getString("NUMERO_NOTA_FISCAL"));
				 validacaoCargaBean.setPedidoMond(rs.getString("PEDIDO_MONDELEZ"));
				 validacaoCargaBean.setOrdeDeliveryNote(rs.getString("ORDE_DELIVERY_NOTE"));
				 validacaoCargaBean.setDhRegistroShipment(rs.getString("SHIPMENT_DATA_REGISTRO"));
				 validacaoCargaBean.setDhRegistroOrde(rs.getString("ORDEM_DATA_REGISTRO"));
				 validacaoCargaBean.setDhRegistroNotaFiscal(rs.getString("NOTA_FISCAL_DATA_REGISTRO"));
				 validacaoCargaList.add(validacaoCargaBean);

			}
			
			retCarga.setValidacaoCarga(validacaoCargaList);
		
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	
		return retCarga;
		
	}


	public RetornoValidaCargaBean qntdEmba() {			
			
			RetornoQuantidadeImportadosBean qntdBean = new RetornoQuantidadeImportadosBean();	
			RetornoValidaCargaBean retCarga = new RetornoValidaCargaBean();
			
			try {
				
			Connection conn = DAO.getConexao();
			ResultSet rs = null;

			PreparedStatement pst;
			
			String cSQL = " SELECT COUNT(EMBA_ID_CD_EMBARQUE) AS 'QUANTIDADE_DE_ARQUIVOS_AMBARQUE_IMPORTADOS' FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA  ( NOLOCK)  WHERE  EMBA_DH_REGISTRO >= convert(varchar(10),GETDATE(),120) ";
			
			pst = conn.prepareStatement(cSQL);			

			rs = pst.executeQuery();

			while (rs.next()) {
				
				qntdBean.setQntdEmba(rs.getString("QUANTIDADE_DE_ARQUIVOS_AMBARQUE_IMPORTADOS"));

			}
			
			PreparedStatement pst2;
			ResultSet rs2 = null;
			String cSQL2 = " SELECT COUNT(SKUD_ID_CD_SHIPMENT) AS 'QUANTIDADE_DE_ARQUIVOS_SKUD_IMPORTADOS' FROM CS_CDTB_SKUDELIVERY_SKUD  ( NOLOCK)  WHERE SKUD_DH_REGISTRO >= convert(varchar(10),GETDATE(),120) ";
			
			pst2 = conn.prepareStatement(cSQL2);			

			rs2 = pst2.executeQuery();

			while (rs2.next()) {
				
				qntdBean.setQtdSkud(rs2.getString("QUANTIDADE_DE_ARQUIVOS_SKUD_IMPORTADOS"));

			}
			PreparedStatement pst3;
			ResultSet rs3 = null;
			
			String cSQL3 = "SELECT COUNT(NOFI_DS_NOTAFISCALNUMBER) AS 'QUANTIDADE_DE_ARQUIVOS_NOTA_FISCAL_IMPORTADOS' FROM CS_LGTB_NOTAFISCAL_NOFI NOFI ( NOLOCK) WHERE NOFI_DH_PROCESSAMENTO >= convert(varchar(10),GETDATE(),120) ";
			
			pst3 = conn.prepareStatement(cSQL3);			

			rs3 = pst3.executeQuery();

			while (rs3.next()) {
				
				qntdBean.setQntdNotaFiscal(rs3.getString("QUANTIDADE_DE_ARQUIVOS_NOTA_FISCAL_IMPORTADOS"));

			}
			
			PreparedStatement pst4;
			
			ResultSet rs4 = null;
			String cSQL4 = "SELECT COUNT(ORDE_DS_MATERIALNUMBER) AS 'QUANTIDADE_DE_ARQUIVOS_DE_ORDEM_IMPORTADOS' FROM CS_LGTB_ORDEM_ORDE  ( NOLOCK)  WHERE ORDE_DH_PROCESSAMENTO >= convert(varchar(10),GETDATE(),120) ";
			
			pst4 = conn.prepareStatement(cSQL4);			

			rs4 = pst4.executeQuery();

			while (rs4.next()) {
				
				qntdBean.setQtndOrde(rs4.getString("QUANTIDADE_DE_ARQUIVOS_DE_ORDEM_IMPORTADOS"));

			}
			
			PreparedStatement pst5;
			
			ResultSet rs5 = null;
			String cSQL5 = "SELECT  COUNT(SHIP_ID_CD_SHIPMENT) AS 'QUANTIDADE_DE_ARQUIVOS_SHIPMENT_IMPORTADOS' FROM CS_LGTB_SHIPMENTXML_SHIP ( NOLOCK)  WHERE SHIP_DH_PROCESSAMENTO  >= convert(varchar(10),GETDATE(),120) ";
			
			pst5 = conn.prepareStatement(cSQL5);			

			rs5 = pst5.executeQuery();

			while (rs5.next()) {
				
				qntdBean.setQntdShipment(rs5.getString("QUANTIDADE_DE_ARQUIVOS_SHIPMENT_IMPORTADOS"));

			}
			
			} catch (SQLException e) {
			
				e.printStackTrace();
			}
			
			retCarga.setQtdProcessados(qntdBean);
			
			
			return retCarga;
		}

}
