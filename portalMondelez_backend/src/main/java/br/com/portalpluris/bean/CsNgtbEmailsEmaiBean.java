package br.com.portalpluris.bean;

public class CsNgtbEmailsEmaiBean {

	int emaiIdCdEmail;
	int userIdCdUsuario;
	int embaIdCdEmbarque;
	String emaiTxConteudo;
	String emaiDsAssunto;
	String emaiDsDestinatario;
	String emaiInEnviado;
	String emaiDhEnvio;
	String emaiDsErro;

	public int getEmaiIdCdEmail() {
		return emaiIdCdEmail;
	}

	public void setEmaiIdCdEmail(int emaiIdCdEmail) {
		this.emaiIdCdEmail = emaiIdCdEmail;
	}

	public int getUserIdCdUsuario() {
		return userIdCdUsuario;
	}

	public void setUserIdCdUsuario(int userIdCdUsuario) {
		this.userIdCdUsuario = userIdCdUsuario;
	}

	public int getEmbaIdCdEmbarque() {
		return embaIdCdEmbarque;
	}

	public void setEmbaIdCdEmbarque(int embaIdCdEmbarque) {
		this.embaIdCdEmbarque = embaIdCdEmbarque;
	}

	public String getEmaiTxConteudo() {
		return emaiTxConteudo;
	}

	public void setEmaiTxConteudo(String emaiTxConteudo) {
		this.emaiTxConteudo = emaiTxConteudo;
	}

	public String getEmaiDsAssunto() {
		return emaiDsAssunto;
	}

	public void setEmaiDsAssunto(String emaiDsAssunto) {
		this.emaiDsAssunto = emaiDsAssunto;
	}

	public String getEmaiDsDestinatario() {
		return emaiDsDestinatario;
	}

	public void setEmaiDsDestinatario(String emaiDsDestinatario) {
		this.emaiDsDestinatario = emaiDsDestinatario;
	}

	public String getEmaiInEnviado() {
		return emaiInEnviado;
	}

	public void setEmaiInEnviado(String emaiInEnviado) {
		this.emaiInEnviado = emaiInEnviado;
	}

	public String getEmaiDhEnvio() {
		return emaiDhEnvio;
	}

	public void setEmaiDhEnvio(String emaiDhEnvio) {
		this.emaiDhEnvio = emaiDhEnvio;
	}

	public String getEmaiDsErro() {
		return emaiDsErro;
	}

	public void setEmaiDsErro(String emaiDsErro) {
		this.emaiDsErro = emaiDsErro;
	}
}
