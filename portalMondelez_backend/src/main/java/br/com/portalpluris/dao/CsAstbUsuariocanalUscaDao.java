package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.CsAstbUsuariocanalUscaBean;
import br.com.portalpluris.bean.CsCdtbCanalCanaBean;
import br.com.portalpluris.bean.CsCdtbTransportadoraTranBean;
import br.com.portalpluris.bean.CsCdtbUsuarioUserBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsAstbUsuariocanalUscaDao {

	public RetornoBean createUsuarioCanal(CsAstbUsuariocanalUscaBean uscaBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		pst = conn.prepareStatement("" 
				+ " INSERT INTO CS_ASTB_USUARIOCANAL_USCA ( "
				+ " 		CANA_ID_CD_CANAL, " 
				+ "			USER_ID_CD_USUARIO "
				+ " ) VALUES ( ?, ? )");

		pst.setString(1, uscaBean.getCsCdtbCanalCanaBean().getCana_id_cd_canal());
		pst.setString(2, uscaBean.getCsCdtbUsuarioUserBean().getUser_id_cd_usuario());

		pst.execute();

		retBean.setSucesso(true);
		retBean.setMsg("Vinculo efetuado com sucesso!");

		return retBean;
	}
	

	private RetornoBean deleteUsuarioCanal(CsAstbUsuariocanalUscaBean uscaBean) throws SQLException {

		RetornoBean retBean = new RetornoBean();

		Connection conn = DAO.getConexao();

		PreparedStatement pst;

		pst = conn.prepareStatement("  	DELETE "
								   + "		CS_ASTB_USUARIOCANAL_USCA "
								   + "	WHERE "
								   + "		CANA_ID_CD_CANAL = ? "
								   + "		AND USER_ID_CD_USUARIO ? ");

		pst.setString(1, uscaBean.getCsCdtbCanalCanaBean().getCana_id_cd_canal());
		pst.setString(2, uscaBean.getCsCdtbUsuarioUserBean().getUser_id_cd_usuario());

		//pst = conn.prepareStatement(queryUpdate);
		pst.executeUpdate();

		retBean.setSucesso(true);
		retBean.setMsg("Vinculo excluído com sucesso!");

		return retBean;
	}

	
	
	
	public List<CsAstbUsuariocanalUscaBean> getListUsuariosPorCanal(String idUsuario) throws SQLException {

		List<CsAstbUsuariocanalUscaBean> listUsuarioBean = new ArrayList<CsAstbUsuariocanalUscaBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query =    "  " 
							+ "	SELECT "
							+ "		USCA.USER_ID_CD_USUARIO, "
							+ "		USUA.USER_DS_NOMEUSUARIO, "
							+ "		USCA.CANA_ID_CD_CANAL, "
							+ "		CANA.CANA_DS_CANAL "
							+ "	FROM  "
							+ "		CS_ASTB_USUARIOCANAL_USCA USCA (NOLOCK) INNER JOIN CS_CDTB_CANAL_CANA CANA  (NOLOCK)"
							+ "			ON CANA.CANA_ID_CD_CANAL = USCA.CANA_ID_CD_CANAL "
							+ "		INNER JOIN CS_CDTB_USUARIO_USER USUA (NOLOCK)"
							+ "			ON USUA.USER_ID_CD_USUARIO = USCA.USER_ID_CD_USUARIO "
							+ "	WHERE  "
							+ "		CANA_IN_INATIVO = 'N' " 		//SÓ ATIVOS
							//+ "		AND USER_IN_INATIVO = 'N' "		//SÓ ATIVOS
							+ "		AND USCA.USER_ID_CD_USUARIO  = "+idUsuario
							+ "	ORDER BY  "
							+ "		USUA.USER_DS_NOMEUSUARIO  ";

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			CsAstbUsuariocanalUscaBean vinculoUsuarioCanalBean = setUsuarioCanalBean(rs);

			listUsuarioBean.add(vinculoUsuarioCanalBean);
		}

		return listUsuarioBean;
	}

	public CsCdtbTransportadoraTranBean getTran(String tranIdCdTransportadora) throws SQLException {

		CsCdtbTransportadoraTranBean tranBean = new CsCdtbTransportadoraTranBean();
		tranBean.setTran_nm_nometransportadora("null");

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " + "		TRAN_ID_CD_TRANSPORTADORA, " + "		TRAN_NM_NOMETRANSPORTADORA, "
				+ "		TRAN_DS_CODIGOTRANSPORTADORA, " + "		TRAN_DH_REGISTRO, " + "		TRAN_IN_INATIVO, "
				+ "		TRAN_ID_CD_USUARIOATUALIZACAO, " + "		TRAN_DH_ATUALIZACAO " + " FROM "
				+ "		CS_CDTB_TRANSPORTADORA_TRAN (NOLOCK) " + " WHERE " + "		TRAN_DS_CODIGOTRANSPORTADORA = ?";

		pst = conn.prepareStatement(query);
		pst.setString(1, tranIdCdTransportadora);

		rs = pst.executeQuery();

		while (rs.next()) {

			tranBean = setTranBean(rs);
		}

		return tranBean;
	}

	private CsCdtbTransportadoraTranBean setTranBean(ResultSet rs) throws SQLException {

		CsCdtbTransportadoraTranBean tranBean = new CsCdtbTransportadoraTranBean();

		tranBean.setTran_id_cd_transportadora(rs.getInt("TRAN_ID_CD_TRANSPORTADORA"));
		tranBean.setTran_nm_nometransportadora(rs.getString("TRAN_NM_NOMETRANSPORTADORA"));
		tranBean.setTran_ds_codigotransportadora(rs.getString("TRAN_DS_CODIGOTRANSPORTADORA"));
		tranBean.setTran_dh_registro(rs.getString("TRAN_DH_REGISTRO"));
		// tranBean.setTranDsTipocargatransportadora(rs.getString("TRAN_DS_TIPOCARGATRANSPORTADORA"));
		tranBean.setTran_in_inativo(rs.getString("TRAN_IN_INATIVO"));
		tranBean.setTran_id_cd_usuarioatualizacao(rs.getInt("TRAN_ID_CD_USUARIOATUALIZACAO"));
		tranBean.setTran_dh_atualizacao(rs.getString("TRAN_DH_ATUALIZACAO"));

		return tranBean;
	}
	
	
	
	private CsAstbUsuariocanalUscaBean setUsuarioCanalBean(ResultSet rs) throws SQLException {

		// CANAL 
		CsCdtbCanalCanaBean canalBean = new CsCdtbCanalCanaBean();
			canalBean.setCana_id_cd_canal(rs.getString("CANA_ID_CD_CANAL"));
			canalBean.setCana_ds_canal(rs.getString("CANA_DS_CANAL"));

		// USUARIO
		CsCdtbUsuarioUserBean usuarioBean = new CsCdtbUsuarioUserBean();
			usuarioBean.setUser_id_cd_usuario(rs.getString("USER_ID_CD_USUARIO"));
			usuarioBean.setUser_ds_nomeusuario(rs.getString("USER_DS_NOMEUSUARIO"));
			
		// VINCULO
		CsAstbUsuariocanalUscaBean vinculoBean = new CsAstbUsuariocanalUscaBean();
			vinculoBean.setCsCdtbUsuarioUserBean(usuarioBean);
			vinculoBean.setCsCdtbCanalCanaBean(canalBean);
				
		return vinculoBean;
	}

	public boolean slcTran(String tranDsCodigotransportadora) throws SQLException {

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " + "		" + "TRAN_ID_CD_TRANSPORTADORA, "
				+ "		TRAN_NM_NOMETRANSPORTADORA, " + "		TRAN_DS_CODIGOTRANSPORTADORA, "
				+ "		TRAN_DH_REGISTRO, " + "		TRAN_IN_INATIVO, " + "		TRAN_ID_CD_USUARIOATUALIZACAO, "
				+ "		TRAN_DH_ATUALIZACAO " + " FROM " + "		CS_CDTB_TRANSPORTADORA_TRAN (NOLOCK) " + " WHERE "
				+ "		TRAN_DS_CODIGOTRANSPORTADORA = ?";

		pst = conn.prepareStatement(query);
		pst.setString(1, tranDsCodigotransportadora);

		rs = pst.executeQuery();

		while (rs.next()) {

			System.out.println("Já existe uma transportadora");

			return false;

		}

		return true;
	}

	public void updateTrans(CsCdtbTransportadoraTranBean transBean, String dhImportacao, String nmArquivo) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraTranDao");
		logsBean.setLogsDsMetodo("updateTrans");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Insert na base TRANSPORTADORA *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst;

			pst = con.prepareStatement("" + "UPDATE CS_CDTB_TRANSPORTADORA_TRAN SET"
					+ "  TRAN_NM_NOMETRANSPORTADORA = ? ," + "  TRAN_DH_ATUALIZACAO = ? "
					+ "	TRAN_NM_ARQUIVOPROCESSADO = ? " + " WHERE TRAN_DS_CODIGOTRANSPORTADORA = ?");

			pst.setString(1, transBean.getTran_nm_nometransportadora());
			pst.setString(2, dhImportacao);
			pst.setString(3, nmArquivo);
			pst.setString(4, transBean.getTran_ds_codigotransportadora());

			pst.executeUpdate();

		} catch (Exception e) {

			log.info("***** Erro no UPDATE na base do TRANSPORTADORA " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  UPDATE na base do TRANSPORTADORA  *****");
			daoLog.createLog(logsBean);

		}

	}

	public void insertTrans(CsCdtbTransportadoraTranBean transBean, String dhImportacao, String nmArquivo) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbTransportadoraTranDao");
		logsBean.setLogsDsMetodo("insertTrans");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Insert na base TRANSPORTADORA *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst = con.prepareStatement("");

			pst = con.prepareStatement("" + " 	INSERT INTO CS_CDTB_TRANSPORTADORA_TRAN ("
					+ "  TRAN_NM_NOMETRANSPORTADORA ," + "  TRAN_DS_CODIGOTRANSPORTADORA ," + "  TRAN_DH_REGISTRO  ,"
					+ "  TRAN_IN_INATIVO ," + "  TRAN_ID_CD_USUARIOATUALIZACAO," + " TRAN_NM_ARQUIVOPROCESSADO "
					+ ") VALUES ( ?, ?, ?, ?, ?, ?)");

			pst.setString(1, transBean.getTran_nm_nometransportadora());
			pst.setString(2, transBean.getTran_ds_codigotransportadora());
			pst.setString(3, dhImportacao);
			pst.setString(4, "N");
			pst.setString(5, nmArquivo);
			pst.setString(6, idUser);

			pst.execute();

		} catch (Exception e) {

			log.info("***** Erro no INSERT na base do TRANSPORTADORA " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  INSERT na base do TRANSPORTADORA  *****");
			daoLog.createLog(logsBean);

		}

	}

}
