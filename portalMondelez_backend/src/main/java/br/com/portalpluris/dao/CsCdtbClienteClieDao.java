package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilParseToInt;
import br.com.portalpluris.bean.BaseClienteConsolidadaBean;
import br.com.portalpluris.bean.CsCdtbContatoclienteCoclBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.RetornoBean;

public class CsCdtbClienteClieDao {

	public RetornoBean updateCliente(BaseClienteConsolidadaBean baseCliente) {

		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		RetornoBean retBean = new RetornoBean();
		DAOLog daoLog = new DAOLog();

		UtilParseToInt utilParse = new UtilParseToInt();

		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("***** Inicio Update na base de Clientes *****");
		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbClienteClieDao");
		logsBean.setLogsDsMetodo("updateCliente");
		logsBean.setLogsInErro("N");

		int user = utilParse.toInt(idUser);

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst;
			
			String query = ""
					+ "UPDATE CS_CDTB_CLIENTE_CLIE SET" + " CLIE_DS_NOMECLIENTE = ? , "
					+ " CLIE_DS_CNPJ = ? , " + " CLIE_DH_ATUALIZACAO = getdate() , " + " CLIE_IN_INATIVO = ? , "
					+ " CLIE_ID_CD_USUARIOATUALIZACAO = ? , " + " CLIE_DS_CIDADE = ? , " + " CLIE_DS_UF = ? , "
					+ " CLIE_DS_REGIAO = ? , " + " CLIE_DS_AGRUPAMENTO = ? , " + " CLIE_DS_ENDERECO = ? , "
					+ " CLIE_DS_CEP = ? , " + " CLIE_DS_TEMAGENDAMENTO = ? , " + " CLIE_DS_NFOBRIGATORIA = ? , "
					+ " CLIE_DS_AGENDAMENTOOBRIGATORIO = ? , " + " CLIE_DS_TIPOAGENDAMENTO = ? , "
					+ " CLIE_DS_DIASRECEBIMENTO = ? , " + " CLIE_DS_HORARIORECEBIMENTO = ? , "
					+ " CLIE_DS_ESPERACLIENTE = ? , " + " CLIE_DS_VEICULODEDICADO = ? , "
					+ " CLIE_DS_PAGATAXAVEICULODEDICADO = ? , " + " CLIE_DS_TIPODESCARGA = ? , "
					+ " CLIE_DS_TAXADESCARGA = ? , " + " CLIE_DS_FEFOESPECIAL = ? , "
					+ " CLIE_DS_PALETIZACAOESPECIAL = ? , " + " CLIE_DS_CLIENTEEXIGEPALETE = ? , "
					+ " CLIE_DS_PALETIZACAOMONDELEZ = ? , " + " CLIE_DS_TIPOPALETIZACAO = ? , "
					+ " CLIE_DS_ALTURPALETE = ? , " + " CLIE_DS_CODVEICULOMAX = ? , " + " CLIE_DS_TIPOVEICULOMAX = ? , "
					+ " CLIE_DS_PESOVEICULO = ? , " + " CLIE_DS_DATAREVISAO = ? , " + " CLIE_DS_RESPREVISAO = ? , "
					+ " CLIE_DS_OBSERVACAO = ? , " + " CLI_DS_RESTRICAOCIRCULACAO = ? , "
					+ " CLIE_DS_TIPORESTRICAO = ? , " + " CLIE_DS_HORARIORESTRICAO = ?, "
					+ " CLIE_NM_ARQUIVOPROCESSADO = ? "
					+ " WHERE CLIE_ID_CD_CODIGOCLIENTE = ?";
			
			//nem sempre no update haverá o nome de arquivo pois tem solicitações da tela
			//if(nmArquivo != null && !nmArquivo.equalsIgnoreCase("")){
			//	query +=   " + nmArquivo;
			//}

			//query += " WHERE CLIE_ID_CD_CODIGOCLIENTE = ?";
			
			pst = con.prepareStatement(query);
					
					pst.setString(1, baseCliente.getClie_ds_nomecliente());
					pst.setString(2, baseCliente.getClie_ds_cnpj());
					//pst.setString(3, dhImportacao);
					
					if(baseCliente.getClie_in_inativo() != null && 
							(baseCliente.getClie_in_inativo().equalsIgnoreCase("S") 
									|| baseCliente.getClie_in_inativo().equalsIgnoreCase("SIM") 
									|| baseCliente.getClie_in_inativo().equalsIgnoreCase("true"))){
						pst.setString(3, "S");
					}else{
						pst.setString(3, "N");
					}
					
					pst.setInt(4, user);
					pst.setString(5, baseCliente.getClie_ds_cidade());
					pst.setString(6, baseCliente.getClie_ds_uf());
					pst.setString(7, baseCliente.getClie_ds_regiao());
					pst.setString(8, baseCliente.getClie_ds_agrupamento());
					pst.setString(9, baseCliente.getClie_ds_endereco());
					pst.setString(10, baseCliente.getClie_ds_cep());
					pst.setString(11, baseCliente.getClie_ds_temagendamento());
					pst.setString(12, baseCliente.getClie_ds_nfobrigatoria());
					pst.setString(13, baseCliente.getClie_ds_agendamentoobrigatorio());
					pst.setString(14, baseCliente.getClie_ds_tipoagendamento());
					pst.setString(15, baseCliente.getClie_ds_diasrecebimento());
					pst.setString(16, baseCliente.getClie_ds_horariorecebimento());
					pst.setString(17, baseCliente.getClie_ds_esperacliente());
					pst.setString(18, baseCliente.getClie_ds_veiculodedicado());
					pst.setString(19, baseCliente.getClie_ds_pagataxaveiculodedicado());
					pst.setString(20, baseCliente.getClie_ds_tipodescarga());
					pst.setString(21, baseCliente.getClie_ds_taxadescarga());
					pst.setString(22, baseCliente.getClie_ds_fefoespecial());
					pst.setString(23, baseCliente.getClie_ds_paletizacaoespecial());
					pst.setString(24, baseCliente.getClie_ds_clienteexigepalete());
					pst.setString(25, baseCliente.getClie_ds_paletizacaomondelez());
					pst.setString(26, baseCliente.getClie_ds_tipopaletizacao());
					pst.setString(27, baseCliente.getClie_ds_alturpalete());
					pst.setString(28, baseCliente.getClie_ds_codveiculomax());
					pst.setString(29, baseCliente.getClie_ds_tipoveiculomax());
					pst.setString(30, baseCliente.getClie_ds_pesoveiculo());
					pst.setString(31, baseCliente.getClie_ds_datarevisao());
					pst.setString(32, baseCliente.getClie_ds_resprevisao());
					pst.setString(33, baseCliente.getClie_ds_observacao());
					pst.setString(34, baseCliente.getCli_ds_restricaocirculacao());
					pst.setString(35, baseCliente.getCli_ds_restricaocirculacao());
					pst.setString(36, baseCliente.getClie_ds_horariorestricao());
					pst.setString(37, baseCliente.getClie_nm_arquivoprocessado());
					pst.setString(38, baseCliente.getClie_id_cd_cliente());
				
								
			pst.executeUpdate();

			retBean.setSucesso(true);
			retBean.setProcesso("update");
			retBean.setMsg("Cliente atualizado com sucesso");

		} catch (Exception e) {
			log.info("*****  Erro no UPDATE:" + e + "*****");
			retBean.setSucesso(false);
			retBean.setProcesso("update");
			retBean.setMsg("Cliente não atualizado : "+e.getMessage() );

//			logsBean.setLogsInErro("S");
//			logsBean.setLogsTxErro(e.getMessage());

		}
//		finally {
//
//			daoLog.createLog(logsBean);
//
//		}

		return retBean;

	}

	public RetornoBean insertCliente(BaseClienteConsolidadaBean baseCliente) {

		String cliAtivo = "S";
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dhImportacao = sdf.format(date);
		
		
		Logger log = Logger.getLogger(this.getClass().getName());
		// DADOS DE LOG
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		RetornoBean retBean = new RetornoBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbClienteClieDao");
		logsBean.setLogsDsMetodo("insertCliente");
		logsBean.setLogsInErro("N");
		log.info("***** Inicio Insert na base de Clientes *****");

		try {

			int cdCliente = Integer.parseInt(baseCliente.getClie_id_cd_codigocliente());

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst = con.prepareStatement("");

			pst = con.prepareStatement("" + " 	INSERT INTO CS_CDTB_CLIENTE_CLIE (" 
					+ " CLIE_ID_CD_CODIGOCLIENTE,"
					+ " CLIE_DS_NOMECLIENTE," 
					+ " CLIE_DS_CNPJ," 
					+ " CLIE_IN_INATIVO,"
					+ " CLIE_ID_CD_USUARIOATUALIZACAO," 
					+ " CLIE_DS_CIDADE," 
					+ " CLIE_DS_UF," 
					+ " CLIE_DS_REGIAO,"
					+ " CLIE_DS_AGRUPAMENTO,"
					+ " CLIE_DS_ENDERECO," 
					+ " CLIE_DS_CEP," 
					+ " CLIE_DS_TEMAGENDAMENTO,"
					+ " CLIE_DS_NFOBRIGATORIA,"
					+ " CLIE_DS_AGENDAMENTOOBRIGATORIO," 
					+ " CLIE_DS_TIPOAGENDAMENTO,"
					+ " CLIE_DS_DIASRECEBIMENTO," 
					+ " CLIE_DS_HORARIORECEBIMENTO," 
					+ " CLIE_DS_ESPERACLIENTE,"
					+ " CLIE_DS_VEICULODEDICADO,"
					+ " CLIE_DS_PAGATAXAVEICULODEDICADO," 
					+ " CLIE_DS_TIPODESCARGA,"
					+ " CLIE_DS_TAXADESCARGA," 
					+ " CLIE_DS_FEFOESPECIAL,"
					+ " CLIE_DS_PALETIZACAOESPECIAL,"
					+ " CLIE_DS_CLIENTEEXIGEPALETE,"
					+ " CLIE_DS_PALETIZACAOMONDELEZ," 
					+ " CLIE_DS_TIPOPALETIZACAO,"
					+ " CLIE_DS_ALTURPALETE," 
					+ " CLIE_DS_CODVEICULOMAX," 
					+ " CLIE_DS_TIPOVEICULOMAX,"
					+ " CLIE_DS_PESOVEICULO," 
					+ " CLIE_DS_DATAREVISAO,"
					+ " CLIE_DS_RESPREVISAO,"
					+ " CLIE_DS_OBSERVACAO," 
					+ " CLI_DS_RESTRICAOCIRCULACAO,"
					+ " CLIE_DS_TIPORESTRICAO,"
					+ " CLIE_DS_HORARIORESTRICAO,"
					+ " CLIE_NM_ARQUIVOPROCESSADO,"
					+ " CLIE_DH_REGISTRO"
					+ "		) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?) ");

			pst.setString(1, baseCliente.getClie_id_cd_codigocliente());
			pst.setString(2, baseCliente.getClie_ds_nomecliente());
			pst.setString(3, baseCliente.getClie_ds_cnpj());
			pst.setString(4, "N");
			pst.setString(5, baseCliente.getClie_id_cd_usuarioatualizacao());
			pst.setString(6, baseCliente.getClie_ds_cidade());
			pst.setString(7, baseCliente.getClie_ds_uf());
			pst.setString(8, baseCliente.getClie_ds_regiao());
			pst.setString(9, baseCliente.getClie_ds_agrupamento());
			pst.setString(10, baseCliente.getClie_ds_endereco());
			pst.setString(11, baseCliente.getClie_ds_cep());
			pst.setString(12, baseCliente.getClie_ds_temagendamento());
			pst.setString(13, baseCliente.getClie_ds_nfobrigatoria());
			pst.setString(14, baseCliente.getClie_ds_agendamentoobrigatorio());
			pst.setString(15, baseCliente.getClie_ds_tipoagendamento());
			pst.setString(16, baseCliente.getClie_ds_diasrecebimento());
			pst.setString(17, baseCliente.getClie_ds_horariorecebimento());
			pst.setString(18, baseCliente.getClie_ds_esperacliente());
			pst.setString(19, baseCliente.getClie_ds_veiculodedicado());
			pst.setString(20, baseCliente.getClie_ds_pagataxaveiculodedicado());
			pst.setString(21, baseCliente.getClie_ds_tipodescarga());
			pst.setString(22, baseCliente.getClie_ds_taxadescarga());
			pst.setString(23, baseCliente.getClie_ds_fefoespecial());
			pst.setString(24, baseCliente.getClie_ds_paletizacaoespecial());
			pst.setString(25, baseCliente.getClie_ds_clienteexigepalete());
			pst.setString(26, baseCliente.getClie_ds_paletizacaomondelez());
			pst.setString(27, baseCliente.getClie_ds_tipopaletizacao());
			pst.setString(28, baseCliente.getClie_ds_alturpalete());
			pst.setString(29, baseCliente.getClie_ds_codveiculomax());
			pst.setString(30, baseCliente.getClie_ds_tipoveiculomax());
			pst.setString(31, baseCliente.getClie_ds_pesoveiculo());
			pst.setString(32, baseCliente.getClie_ds_datarevisao());
			pst.setString(33, baseCliente.getClie_ds_resprevisao());
			pst.setString(34, baseCliente.getClie_ds_observacao());
			pst.setString(35, baseCliente.getCli_ds_restricaocirculacao());
			pst.setString(36, baseCliente.getClie_ds_tiporestricao());
			pst.setString(37, baseCliente.getClie_ds_horariorestricao());
			pst.setString(38, baseCliente.getClie_nm_arquivoprocessado());
			pst.setString(39,dhImportacao);

			pst.execute();

		} catch (Exception e) {
			log.info("***** Erro no Insert:" + e + "*****");
//			logsBean.setLogsInErro("S");
//			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			log.info("***** FOI INSERIDO UM NOVO CLIENTE COM SUCESSO *****");
			// daoLog.createLog(logsBean);

		}

		return retBean;
	}

	public List<BaseClienteConsolidadaBean> getListClie(String clieInAtivo) throws SQLException {

		List<BaseClienteConsolidadaBean> listClieBean = new ArrayList<BaseClienteConsolidadaBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;

		String query = "" + " SELECT " 
				 + " CLIE_ID_CD_CLIENTE, "
				 + " CLIE_DS_NOMECLIENTE, "
				 + " CLIE_ID_CD_CODIGOCLIENTE, "
				 + " CLIE_DS_CNPJ, "
				 + " CLIE_DH_REGISTRO, "
				 + " CASE WHEN CLIE_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS CLIE_IN_INATIVO,"
				 + " CLIE_ID_CD_USUARIOATUALIZACAO, "
				 + " CLIE_DH_ATUALIZACAO, "
				 + " CLIE_DS_PA, "
				 + " CLIE_DS_CIDADE, "
				 + " CLIE_DS_UF, "
				 + " CLIE_DS_REGIAO, "
				 + " CLIE_DS_AGRUPAMENTO, "
				 + " CLIE_DS_ENDERECO, "
				 + " CLIE_DS_CEP, "
				 + " CLIE_DS_NFOBRIGATORIA, "
				 + " CLIE_DS_AGENDAMENTOOBRIGATORIO, "
				 + " CLIE_DS_DIASRECEBIMENTO, "
				 + " CLIE_DS_HORARIORECEBIMENTO, "
				 + " CLIE_DS_VEICULODEDICADO, "
				 + " CLIE_DS_TAXADESCARGA, "
				 + " CLIE_DS_FEFOESPECIAL, "
				 + " CLIE_DS_PALETIZACAOESPECIAL, "
				 + " CLIE_DS_PALETIZACAOMONDELEZ, "
				 + " CLIE_DS_TIPOPALETIZACAO, "
				 + " CLIE_DS_CODVEICULOMAX, "
				 + " CLIE_DS_TIPOVEICULOMAX, "
				 + " CLIE_DS_PESOVEICULO, "
				 + " CLIE_DS_TEMAGENDAMENTO, "
				 + " CLIE_DS_TIPOAGENDAMENTO, "
				 + " CLIE_DS_ESPERACLIENTE, "
				 + " CLIE_DS_PAGATAXAVEICULODEDICADO, "
				 + " CLIE_DS_TIPODESCARGA, "
				 + " CLIE_DS_CLIENTEEXIGEPALETE, "
				 + " CLIE_DS_ALTURPALETE, "
				 + " CLIE_CD_VEICULOMAX, "
				 + " CLIE_DS_DATAREVISAO, "
				 + " CLIE_DS_RESPREVISAO, "
				 + " CLIE_DS_OBSERVACAO, "
				 + " CLI_DS_RESTRICAOCIRCULACAO, "
				 + " CLIE_DS_TIPORESTRICAO, "
				 + " CLIE_DS_HORARIORESTRICAO, "
				 + " CLIE_NM_ARQUIVOPROCESSADO "

				+ " FROM "
				+ "		CS_CDTB_CLIENTE_CLIE (NOLOCK) " + "";

		if (clieInAtivo.equalsIgnoreCase("S") || clieInAtivo.equalsIgnoreCase("N")) {
			query += " WHERE CLIE_IN_INATIVO = '" + clieInAtivo + "'";
		}

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			BaseClienteConsolidadaBean clieBean = setClieBean(rs);

			listClieBean.add(clieBean);
		}

		return listClieBean;
	}
	
	public List<BaseClienteConsolidadaBean> getListClieFiltro(String criterio, String valor) throws SQLException {

		List<BaseClienteConsolidadaBean> listClieBean = new ArrayList<BaseClienteConsolidadaBean>();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;

		String query = "";
			
		if (criterio.equalsIgnoreCase("nome")) {
			query = " SELECT TOP 10 ";
		
		}else {
			query = " SELECT ";
		}
		
			query +=" CLIE_ID_CD_CLIENTE, "
				  + " CLIE_DS_NOMECLIENTE, "
				  + " CLIE_ID_CD_CODIGOCLIENTE, "
				  + " CLIE_DS_CNPJ, "
				  + " CLIE_DH_REGISTRO, "
				  + " CASE WHEN CLIE_IN_INATIVO = 'S' THEN 'SIM' ELSE 'NÃO' END AS CLIE_IN_INATIVO,"
				  + " CLIE_ID_CD_USUARIOATUALIZACAO, "
				  + " CLIE_DH_ATUALIZACAO, "
				  + " CLIE_DS_PA, "
				  + " CLIE_DS_CIDADE, "
				  + " CLIE_DS_UF, "
				  + " CLIE_DS_REGIAO, "
				  + " CLIE_DS_AGRUPAMENTO, "
				  + " CLIE_DS_ENDERECO, "
				  + " CLIE_DS_CEP, "
				  + " CLIE_DS_NFOBRIGATORIA, "
				  + " CLIE_DS_AGENDAMENTOOBRIGATORIO, "
				  + " CLIE_DS_DIASRECEBIMENTO, "
				  + " CLIE_DS_HORARIORECEBIMENTO, "
				  + " CLIE_DS_VEICULODEDICADO, "
				  + " CLIE_DS_TAXADESCARGA, "
				  + " CLIE_DS_FEFOESPECIAL, "
				  + " CLIE_DS_PALETIZACAOESPECIAL, "
				  + " CLIE_DS_PALETIZACAOMONDELEZ, "
				  + " CLIE_DS_TIPOPALETIZACAO, "
				  + " CLIE_DS_CODVEICULOMAX, "
				  + " CLIE_DS_TIPOVEICULOMAX, "
				  + " CLIE_DS_PESOVEICULO, "
				  + " CLIE_DS_TEMAGENDAMENTO, "
				  + " CLIE_DS_TIPOAGENDAMENTO, "
				  + " CLIE_DS_ESPERACLIENTE, "
				  + " CLIE_DS_PAGATAXAVEICULODEDICADO, "
				  + " CLIE_DS_TIPODESCARGA, "
				  + " CLIE_DS_CLIENTEEXIGEPALETE, "
				  + " CLIE_DS_ALTURPALETE, "
				  + " CLIE_CD_VEICULOMAX, "
				  + " CLIE_DS_DATAREVISAO, "
				  + " CLIE_DS_RESPREVISAO, "
				  + " CLIE_DS_OBSERVACAO, "
				  + " CLI_DS_RESTRICAOCIRCULACAO, "
				  + " CLIE_DS_TIPORESTRICAO, "
				  + " CLIE_DS_HORARIORESTRICAO, "
				  + " CLIE_NM_ARQUIVOPROCESSADO "
				  + " FROM "
				  + "		CS_CDTB_CLIENTE_CLIE (NOLOCK) " 
				  + " WHERE ";

		if (criterio.equalsIgnoreCase("nome")) {
			query += " CLIE_DS_NOMECLIENTE LIKE '%" + valor.trim() + "%'";
		
		}else if (criterio.equalsIgnoreCase("codigo")) {
			query += " CLIE_ID_CD_CODIGOCLIENTE = '" + valor.trim() + "'";
			
		}else if (criterio.equalsIgnoreCase("cnpj")) {
			query += " CLIE_DS_CNPJ = '" + valor.replace(".", "").replace("/", "").trim()+ "'";
		}

		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();

		while (rs.next()) {

			BaseClienteConsolidadaBean clieBean = setClieBean(rs);

			listClieBean.add(clieBean);
		}

		return listClieBean;
	}

	public BaseClienteConsolidadaBean getClie(String codCliente) throws SQLException {

		BaseClienteConsolidadaBean clieBean = new BaseClienteConsolidadaBean();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String query = "" + " SELECT " + " CLIE_ID_CD_CODIGOCLIENTE," + " CLIE_DS_NOMECLIENTE," + " CLIE_DS_CNPJ,"
				+ " CLIE_DH_REGISTRO," + " CLIE_IN_INATIVO," + " CLIE_ID_CD_USUARIOATUALIZACAO," + " CLIE_DS_CIDADE,"
				+ " CLIE_DS_UF," + " CLIE_DS_REGIAO," + " CLIE_DS_AGRUPAMENTO," + " CLIE_DS_ENDERECO," + " CLIE_DS_CEP,"
				+ " CLIE_DS_TEMAGENDAMENTO," + " CLIE_DS_NFOBRIGATORIA," + " CLIE_DS_AGENDAMENTOOBRIGATORIO,"
				+ " CLIE_DS_TIPOAGENDAMENTO," + " CLIE_DS_DIASRECEBIMENTO," + " CLIE_DS_HORARIORECEBIMENTO,"
				+ " CLIE_DS_ESPERACLIENTE," + " CLIE_DS_VEICULODEDICADO," + " CLIE_DS_PAGATAXAVEICULODEDICADO,"
				+ " CLIE_DS_TIPODESCARGA," + " CLIE_DS_TAXADESCARGA," + " CLIE_DS_FEFOESPECIAL,"
				+ " CLIE_DS_PALETIZACAOESPECIAL," + " CLIE_DS_CLIENTEEXIGEPALETE," + " CLIE_DS_PALETIZACAOMONDELEZ,"
				+ " CLIE_DS_TIPOPALETIZACAO," + " CLIE_DS_ALTURPALETE," + " CLIE_DS_CODVEICULOMAX,"
				+ " CLIE_DS_TIPOVEICULOMAX," + " CLIE_DS_PESOVEICULO," + " CLIE_DS_DATAREVISAO,"
				+ " CLIE_DS_RESPREVISAO," + " CLIE_DS_OBSERVACAO," + " CLI_DS_RESTRICAOCIRCULACAO,"
				+ " CLIE_DS_TIPORESTRICAO," + " CLIE_DS_HORARIORESTRICAO" + " FROM "
				+ "		CS_CDTB_CLIENTE_CLIE (NOLOCK) " + " WHERE " + "		CLIE_ID_CD_CODIGOCLIENTE = ?";

		pst = conn.prepareStatement(query);
		pst.setString(1, codCliente);

		rs = pst.executeQuery();

		while (rs.next()) {

			clieBean = setClieBean(rs);
		}

		return clieBean;
	}
	
	
	public BaseClienteConsolidadaBean getClieByEmbarque(String idEmbarque) throws SQLException {

		BaseClienteConsolidadaBean clieBean = new BaseClienteConsolidadaBean();

		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		String cSQL = "";
		cSQL += " SELECT CLIE_ID_CD_CLIENTE,   ";
		cSQL += " CLIE_ID_CD_CODIGOCLIENTE,  ";
		cSQL += "  CLIE_DS_NOMECLIENTE,  ";
		cSQL += "  CLIE_DS_CNPJ, ";
		cSQL += "  CLIE_DH_REGISTRO,  ";
		cSQL += "  CLIE_CD_VEICULOMAX, ";
		cSQL += "  CLIE_DS_TIPODESCARGA,  ";
		cSQL += "  case when CLIE_IN_INATIVO = 'N' THEN 'NÃO' ";
		cSQL += " 		 when CLIE_IN_INATIVO = 'S' THEN 'SIM' ";
		cSQL += " 		ELSE '' END AS CLIE_IN_INATIVO	,  ";
		cSQL += "  CLIE_ID_CD_USUARIOATUALIZACAO,   ";
		cSQL += "  CLIE_DS_CIDADE, ";
		cSQL += "  CLIE_DS_UF,  ";
		cSQL += "  CLIE_DS_REGIAO,   ";
		cSQL += "  CLIE_DS_AGRUPAMENTO,   ";
		cSQL += "  CLIE_DS_ENDERECO,   ";
		cSQL += "  CLIE_DS_CEP, ";
		cSQL += "  CLIE_DS_TEMAGENDAMENTO,   ";
		cSQL += "  CLIE_DS_NFOBRIGATORIA,   ";
		cSQL += "  CLIE_DS_AGENDAMENTOOBRIGATORIO, ";
		cSQL += "  CLIE_DS_TIPOAGENDAMENTO,   ";
		cSQL += "  CLIE_DS_DIASRECEBIMENTO,   ";
		cSQL += "  CLIE_DS_HORARIORECEBIMENTO, ";
		cSQL += "  CLIE_DS_ESPERACLIENTE,   ";
		cSQL += "  CLIE_DS_VEICULODEDICADO,   ";
		cSQL += "  CLIE_DS_PAGATAXAVEICULODEDICADO, ";
		cSQL += "  CLIE_DS_TIPODESCARGA,   ";
		cSQL += "  CLIE_DS_TAXADESCARGA,   ";
		cSQL += "  CLIE_DS_FEFOESPECIAL, ";
		cSQL += "  CLIE_DS_PALETIZACAOESPECIAL,   ";
		cSQL += "  CLIE_DS_CLIENTEEXIGEPALETE,   ";
		cSQL += "  CLIE_DS_PALETIZACAOMONDELEZ, ";
		cSQL += "  CLIE_DS_TIPOPALETIZACAO,   ";
		cSQL += "  CLIE_DS_ALTURPALETE,   ";
		cSQL += "  CLIE_DS_CODVEICULOMAX, ";
		cSQL += "  CLIE_DS_TIPOVEICULOMAX,   ";
		cSQL += "  CLIE_DS_PESOVEICULO,   ";
		cSQL += "  CLIE_DS_DATAREVISAO, ";
		cSQL += "  CLIE_DS_RESPREVISAO,   ";
		cSQL += "  CLIE_DS_OBSERVACAO,   ";
		cSQL += "  CLI_DS_RESTRICAOCIRCULACAO, ";
		cSQL += "  CLIE_DS_TIPORESTRICAO,   ";
		cSQL += "  CLIE_DS_HORARIORESTRICAO,  ";
		cSQL += "  CLIE_NM_ARQUIVOPROCESSADO ";
		cSQL += "  FROM CS_NGTB_EMBARQUEAGENDAMENTO_EMBA EMBA (NOLOCK) INNER JOIN  CS_CDTB_CLIENTE_CLIE CLIE (NOLOCK) ";
		cSQL += "  	ON EMBA.EMBA_ID_CD_CODCLIENTE = CLIE.CLIE_ID_CD_CODIGOCLIENTE  ";
		cSQL += "  WHERE  ";
		cSQL += " 		EMBA.EMBA_ID_CD_EMBARQUE = ? ";


		pst = conn.prepareStatement(cSQL);
		pst.setString(1, idEmbarque);

		rs = pst.executeQuery();

		while (rs.next()) {
			
			clieBean = setClieBean(rs);
			
			List<CsCdtbContatoclienteCoclBean> contatos = new ArrayList<CsCdtbContatoclienteCoclBean>();
			contatos = getClienteContatos(rs.getString("CLIE_ID_CD_CODIGOCLIENTE"));
			
			clieBean.setCsCdtbContatoclienteCoclBean(contatos);
			
		}

		return clieBean;
	}
	
	public List<CsCdtbContatoclienteCoclBean> getClienteContatos(String clie_id_cd_codigocliente) throws SQLException {
		
		List<CsCdtbContatoclienteCoclBean> lstContatos = new ArrayList<CsCdtbContatoclienteCoclBean>();
		String cSQL = "SELECT * FROM CS_CDTB_CONTATOCLIENTE_COCL (NOLOCK) WHERE COCL_ID_CD_CODIGOCLIENTE = ?";
		Connection conn = DAO.getConexao();
		ResultSet rs = null;

		PreparedStatement pst;
		pst = conn.prepareStatement(cSQL);
		pst.setString(1, clie_id_cd_codigocliente);
		
		rs = pst.executeQuery();
		
		while(rs.next()) {
			CsCdtbContatoclienteCoclBean contatos = new CsCdtbContatoclienteCoclBean();
			contatos.setCocl_id_cd_contatocliente(rs.getString("COCL_ID_CD_CONTATOCLIENTE"));
			contatos.setCocl_id_cd_codigocliente(rs.getString("COCL_ID_CD_CODIGOCLIENTE"));
			contatos.setCocl_ds_emailcontatocliente(rs.getString("COCL_DS_EMAILCONTATOCLIENTE"));
			contatos.setCocl_id_cd_usuarioatualizacao(rs.getString("COCL_ID_CD_USUARIOATUALIZACAO"));
			contatos.setCocl_id_cd_usuariocriacao(rs.getString("COCL_ID_CD_USUARIOCRIACAO"));
			contatos.setCocl_dh_registro(rs.getString("COCL_DH_REGISTRO"));
			contatos.setCocl_dh_atualizacao(rs.getString("COCL_DH_ATUALIZACAO"));
			contatos.setCocl_in_inativo(rs.getString("COCL_IN_INATIVO"));
			contatos.setCocl_ds_nomecliente(rs.getString("COCL_DS_NOMECLIENTE"));
			contatos.setCocl_ds_telefonecliente(rs.getString("COCl_DS_TELEFONECLIENTE"));
			contatos.setCocl_ds_cepcliente(rs.getString("COCl_DS_CEPCLIENTE"));
			contatos.setCocl_ds_bairrocliente(rs.getString("COCl_DS_BAIRROCLIENTE"));
			contatos.setCocl_ds_cidadecliente(rs.getString("COCl_DS_CIDADECLIENTE"));
			contatos.setCocl_ds_enderecocliente(rs.getString("COCl_DS_ENDERECOCLIENTE"));
			contatos.setCocl_ds_cnjcliente(rs.getString("COCl_DS_CNPJCLIENTE"));
			contatos.setCocl_ds_grupocliente(rs.getString("COCl_DS_CNPJCLIENTE"));
			contatos.setCocl_ds_ufcliente(rs.getString("COCL_DS_UFCLIENTE"));
			
			lstContatos.add(contatos);
		}
		
		
		
		
		return lstContatos;
	}

	public BaseClienteConsolidadaBean slcIdCliente(String idCliente) {

		BaseClienteConsolidadaBean cliBean = new BaseClienteConsolidadaBean();

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("ArnoMethods");
		logsBean.setLogsDsMetodo("importarArquivoCSV");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Consulta na base de Clientes *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += " SELECT  ";
			cSQL += "          CLIE_ID_CD_CLIENTE, ";
			cSQL += "          CLIE_DS_NOMECLIENTE, ";
			cSQL += "          CLIE_ID_CD_CODIGOCLIENTE, ";
			cSQL += "          CLIE_DS_CNPJ, ";
			cSQL += "          CLIE_DH_REGISTRO, ";
			cSQL += "          CLIE_IN_INATIVO, ";
			cSQL += "          CLIE_ID_CD_USUARIOATUALIZACAO, ";
			cSQL += "          CLIE_DH_ATUALIZACAO, ";
			cSQL += "          CLIE_DS_PA, ";
			cSQL += "          CLIE_DS_CIDADE, ";
			cSQL += "          CLIE_DS_UF, ";
			cSQL += "          CLIE_DS_REGIAO, ";
			cSQL += "          CLIE_DS_AGRUPAMENTO, ";
			cSQL += "          CLIE_DS_ENDERECO, ";
			cSQL += "          CLIE_DS_CEP, ";
			cSQL += "          CLIE_DS_NFOBRIGATORIA, ";
			cSQL += "          CLIE_DS_AGENDAMENTOOBRIGATORIO, ";
			cSQL += "          CLIE_DS_DIASRECEBIMENTO, ";
			cSQL += "          CLIE_DS_HORARIORECEBIMENTO, ";
			cSQL += "          CLIE_DS_VEICULODEDICADO, ";
			cSQL += "          CLIE_DS_TAXADESCARGA, ";
			cSQL += "          CLIE_DS_FEFOESPECIAL, ";
			cSQL += "          CLIE_DS_PALETIZACAOESPECIAL, ";
			cSQL += "          CLIE_DS_PALETIZACAOMONDELEZ, ";
			cSQL += "          CLIE_DS_TIPOPALETIZACAO, ";
			cSQL += "          CLIE_DS_CODVEICULOMAX, ";
			cSQL += "          CLIE_DS_TIPOVEICULOMAX, ";
			cSQL += "          CLIE_DS_PESOVEICULO, ";
			cSQL += "          CLIE_DS_TEMAGENDAMENTO, ";
			cSQL += "          CLIE_DS_TIPOAGENDAMENTO, ";
			cSQL += "          CLIE_DS_ESPERACLIENTE, ";
			cSQL += "          CLIE_DS_PAGATAXAVEICULODEDICADO, ";
			cSQL += "          CLIE_DS_TIPODESCARGA, ";
			cSQL += "          CLIE_DS_CLIENTEEXIGEPALETE, ";
			cSQL += "          CLIE_DS_ALTURPALETE, ";
			cSQL += "          CLIE_CD_VEICULOMAX, ";
			cSQL += "          CLIE_DS_DATAREVISAO, ";
			cSQL += "          CLIE_DS_RESPREVISAO, ";
			cSQL += "          CLIE_DS_OBSERVACAO, ";
			cSQL += "          CLI_DS_RESTRICAOCIRCULACAO, ";
			cSQL += "          CLIE_DS_TIPORESTRICAO, ";
			cSQL += "          CLIE_DS_HORARIORESTRICAO, ";
			cSQL += "          CLIE_NM_ARQUIVOPROCESSADO ";
			cSQL += " 	FROM CS_CDTB_CLIENTE_CLIE (NOLOCK)";
			cSQL += " 	WHERE  CLIE_ID_CD_CODIGOCLIENTE = ? ";

			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, idCliente);

			rs = pst.executeQuery();

			if (rs.next()) {

				log.info(" ******** O CLIENTE:" + idCliente + " Já existe na base *******");

				cliBean.setClie_id_cd_cliente("CLIE_ID_CD_CLIENTE");
				cliBean.setClie_ds_nomecliente("CLIE_DS_NOMECLIENTE");
				cliBean.setClie_id_cd_codigocliente("CLIE_ID_CD_CODIGOCLIENTE");
				cliBean.setClie_ds_cnpj("CLIE_DS_CNPJ");
				cliBean.setClie_dh_registro("CLIE_DH_REGISTRO");
				cliBean.setClie_in_inativo("CLIE_IN_INATIVO");
				cliBean.setClie_id_cd_usuarioatualizacao("CLIE_ID_CD_USUARIOATUALIZACAO");
				cliBean.setClie_dh_atualizacao("CLIE_DH_ATUALIZACAO");
				cliBean.setClie_ds_pa("CLIE_DS_PA");
				cliBean.setClie_ds_cidade("CLIE_DS_CIDADE");
				cliBean.setClie_ds_uf("CLIE_DS_UF");
				cliBean.setClie_ds_regiao("CLIE_DS_REGIAO");
				cliBean.setClie_ds_agrupamento("CLIE_DS_AGRUPAMENTO");
				cliBean.setClie_ds_endereco("CLIE_DS_ENDERECO");
				cliBean.setClie_ds_cep("CLIE_DS_CEP");
				cliBean.setClie_ds_nfobrigatoria("CLIE_DS_NFOBRIGATORIA");
				cliBean.setClie_ds_agendamentoobrigatorio("CLIE_DS_AGENDAMENTOOBRIGATORIO");
				cliBean.setClie_ds_diasrecebimento("CLIE_DS_DIASRECEBIMENTO");
				cliBean.setClie_ds_horariorecebimento("CLIE_DS_HORARIORECEBIMENTO");
				cliBean.setClie_ds_veiculodedicado("CLIE_DS_VEICULODEDICADO");
				cliBean.setClie_ds_taxadescarga("CLIE_DS_TAXADESCARGA");
				cliBean.setClie_ds_fefoespecial("CLIE_DS_FEFOESPECIAL");
				cliBean.setClie_ds_paletizacaoespecial("CLIE_DS_PALETIZACAOESPECIAL");
				cliBean.setClie_ds_paletizacaomondelez("CLIE_DS_PALETIZACAOMONDELEZ");
				cliBean.setClie_ds_tipopaletizacao("CLIE_DS_TIPOPALETIZACAO");
				cliBean.setClie_ds_codveiculomax("CLIE_DS_CODVEICULOMAX");
				cliBean.setClie_ds_tipoveiculomax("CLIE_DS_TIPOVEICULOMAX");
				cliBean.setClie_ds_pesoveiculo("CLIE_DS_PESOVEICULO");
				cliBean.setClie_ds_temagendamento("CLIE_DS_TEMAGENDAMENTO");
				cliBean.setClie_ds_tipoagendamento("CLIE_DS_TIPOAGENDAMENTO");
				cliBean.setClie_ds_esperacliente("CLIE_DS_ESPERACLIENTE");
				cliBean.setClie_ds_pagataxaveiculodedicado("CLIE_DS_PAGATAXAVEICULODEDICADO");
				cliBean.setClie_ds_tipodescarga("CLIE_DS_TIPODESCARGA");
				cliBean.setClie_ds_clienteexigepalete("CLIE_DS_CLIENTEEXIGEPALETE");
				cliBean.setClie_ds_alturpalete("CLIE_DS_ALTURPALETE");
				cliBean.setClie_cd_veiculomax("CLIE_CD_VEICULOMAX");
				cliBean.setClie_ds_datarevisao("CLIE_DS_DATAREVISAO");
				cliBean.setClie_ds_resprevisao("CLIE_DS_RESPREVISAO");
				cliBean.setClie_ds_observacao("CLIE_DS_OBSERVACAO");
				cliBean.setCli_ds_restricaocirculacao("CLI_DS_RESTRICAOCIRCULACAO");
				cliBean.setClie_ds_tiporestricao("CLIE_DS_TIPORESTRICAO");
				cliBean.setClie_ds_horariorestricao("CLIE_DS_HORARIORESTRICAO");
				cliBean.setClie_nm_arquivoprocessado("CLIE_NM_ARQUIVOPROCESSADO");

				return cliBean;
			}
		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			daoLog.createLog(logsBean);

		}
		return cliBean;
	}

	private BaseClienteConsolidadaBean setClieBean(ResultSet rs) throws SQLException {

		BaseClienteConsolidadaBean clieBean = new BaseClienteConsolidadaBean();
		
		clieBean.setClie_id_cd_cliente(rs.getString("CLIE_ID_CD_CLIENTE"));
		clieBean.setClie_id_cd_codigocliente(rs.getString("CLIE_ID_CD_CODIGOCLIENTE"));
		clieBean.setClie_ds_nomecliente(rs.getString("CLIE_DS_NOMECLIENTE"));
		clieBean.setClie_in_inativo(rs.getString("CLIE_IN_INATIVO"));
		clieBean.setClie_ds_cnpj(rs.getString("CLIE_DS_CNPJ"));
		clieBean.setClie_ds_cidade(rs.getString("CLIE_DS_CIDADE"));
		clieBean.setClie_ds_uf(rs.getString("CLIE_DS_UF"));
		clieBean.setClie_ds_regiao(rs.getString("CLIE_DS_REGIAO"));
		clieBean.setClie_ds_agrupamento(rs.getString("CLIE_DS_AGRUPAMENTO"));
		clieBean.setClie_ds_endereco(rs.getString("CLIE_DS_ENDERECO"));
		clieBean.setClie_ds_cep(rs.getString("CLIE_DS_CEP"));
		clieBean.setClie_ds_temagendamento(rs.getString("CLIE_DS_TEMAGENDAMENTO"));
		clieBean.setClie_ds_nfobrigatoria(rs.getString("CLIE_DS_NFOBRIGATORIA"));
		clieBean.setClie_ds_agendamentoobrigatorio(rs.getString("CLIE_DS_AGENDAMENTOOBRIGATORIO"));
		clieBean.setClie_ds_tipoagendamento(rs.getString("CLIE_DS_TIPOAGENDAMENTO"));
		clieBean.setClie_ds_diasrecebimento(rs.getString("CLIE_DS_DIASRECEBIMENTO"));
		clieBean.setClie_ds_horariorecebimento(rs.getString("CLIE_DS_HORARIORECEBIMENTO"));
		clieBean.setClie_ds_esperacliente(rs.getString("CLIE_DS_ESPERACLIENTE"));
		clieBean.setClie_ds_veiculodedicado(rs.getString("CLIE_DS_VEICULODEDICADO"));
		clieBean.setClie_ds_pagataxaveiculodedicado(rs.getString("CLIE_DS_PAGATAXAVEICULODEDICADO"));
		clieBean.setClie_ds_tipodescarga(rs.getString("CLIE_DS_TIPODESCARGA"));
		clieBean.setClie_ds_taxadescarga(rs.getString("CLIE_DS_TAXADESCARGA"));
		clieBean.setClie_cd_veiculomax(rs.getString("CLIE_CD_VEICULOMAX"));		
		clieBean.setClie_ds_fefoespecial(rs.getString("CLIE_DS_FEFOESPECIAL"));
		clieBean.setClie_ds_paletizacaoespecial(rs.getString("CLIE_DS_PALETIZACAOESPECIAL"));
		clieBean.setClie_ds_clienteexigepalete(rs.getString("CLIE_DS_CLIENTEEXIGEPALETE"));
		clieBean.setClie_ds_paletizacaomondelez(rs.getString("CLIE_DS_PALETIZACAOMONDELEZ"));
		clieBean.setClie_ds_tipopaletizacao(rs.getString("CLIE_DS_TIPOPALETIZACAO"));
		clieBean.setClie_ds_alturpalete(rs.getString("CLIE_DS_ALTURPALETE"));
		clieBean.setClie_ds_codveiculomax(rs.getString("CLIE_DS_CODVEICULOMAX"));
		clieBean.setClie_ds_tipoveiculomax(rs.getString("CLIE_DS_TIPOVEICULOMAX"));
		clieBean.setClie_ds_pesoveiculo(rs.getString("CLIE_DS_PESOVEICULO"));
		clieBean.setClie_ds_datarevisao(rs.getString("CLIE_DS_DATAREVISAO"));
		clieBean.setClie_ds_resprevisao(rs.getString("CLIE_DS_RESPREVISAO"));
		clieBean.setClie_ds_observacao(rs.getString("CLIE_DS_OBSERVACAO"));
		clieBean.setCli_ds_restricaocirculacao(rs.getString("CLI_DS_RESTRICAOCIRCULACAO"));
		clieBean.setClie_ds_tiporestricao(rs.getString("CLIE_DS_TIPORESTRICAO"));
		clieBean.setClie_ds_horariorestricao(rs.getString("CLIE_DS_HORARIORESTRICAO"));
		clieBean.setClie_nm_arquivoprocessado(rs.getString("CLIE_NM_ARQUIVOPROCESSADO"));
		
		//CONTATOS 
//		CsCdtbContatoclienteCoclBean contatoBean  = new CsCdtbContatoclienteCoclBean();
//		contatoBean.setCocl_ds_emailcontatocliente(rs.getString("COCL_DS_EMAILCONTATOCLIENTE"));
//		
//		clieBean.setCsCdtbContatoclienteCoclBean(contatoBean);
		
		
		return clieBean;
	}

}
