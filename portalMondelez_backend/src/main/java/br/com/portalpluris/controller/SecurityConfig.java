package br.com.portalpluris.controller;

import net.bytebuddy.agent.builder.AgentBuilder.InitializationStrategy.NoOp;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import br.com.portalpluris.dao.csCdtbUsuarioUserDao;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	
	@Autowired
	private UsuarioService usuarioService;
	
	
	@Override
	public void configure(AuthenticationManagerBuilder auth)throws Exception{
		
	//AUTENTICAÇÃO EM MEMORIA (PARA TESTE)
		/*auth.inMemoryAuthentication()
			.withUser("teste")
			.password("123")
			.roles("USER");*/
		
	//AUTENTICAÇÃO VIA SERVIÇO
		auth
			.userDetailsService(usuarioService)
			.passwordEncoder(passwordEncoder());
				
	}
	   
	@Bean
	public AuthenticationManager authenticationManager() throws Exception{
		return super.authenticationManager();
	}
	
	@Bean 
	public PasswordEncoder passwordEncoder(){
		//USAR CRIPTOGRAFIA 
		return NoOpPasswordEncoder.getInstance();	
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
			.csrf().disable()//NÃO USAR EM APIS
		    .cors()
		.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); // não guardar sessão e deixar expirar 
		
		
	}
}
