package br.com.portalpluris.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.ArquivoLeadTimeBean;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;

public class CsCdtbLeadTimeLdtmDao {

	public boolean slcNomePlanta(String plantaCidade) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbLeadTimeLdtmDao");
		logsBean.setLogsDsMetodo("slcNomePlanta");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Consulta na base LEAD TIME *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "	SELECT   ";
			cSQL += "	LDTM_DS_PLANTACIDADE";
			cSQL += " 		FROM CS_CDTB_LEADTIME_LDTM (NOLOCK)";
			cSQL += " 	WHERE  LDTM_DS_PLANTACIDADE = ? ";

			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, plantaCidade);

			rs = pst.executeQuery();

			if (rs.next()) {

				log.info(" ******** O Nome da planta :" + plantaCidade + " Já existe na base *******");

				return false;
			}

		} catch (Exception e) {
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			daoLog.createLog(logsBean);

		}

		return true;

	}

	public void insertLeadTime(ArquivoLeadTimeBean leadTime, String dhImportacao, int sourceLocation, int ltl, int tl) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbLeadTimeLdtmDao");
		logsBean.setLogsDsMetodo("slcNomePlanta");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Insert na base LEAD TIME *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst = con.prepareStatement("");

			pst = con.prepareStatement("" + " 	INSERT INTO CS_CDTB_LEADTIME_LDTM (" 
					+ "  LDTM_DS_PLANTACIDADE ,"
					+ "  LDTM_CD_SOURCELOCATION ," 
					+ "  LDTM_DS_SOURCELOCATION_NAME  ," 
					+ "  LDTM_DS_SOURCEPROVINCE ,"
					+ "  LDTM_DS_DESTINATIONCITY ," 
					+ "  LDTM_DS_CODPROVINCEDESTINATION  ,"
					+ "  LDTM_DS_NEWZONE ,"
					+ "  LDTM_DS_LTL ," 
					+ "  LDTM_DS_TL ," 
					+ "  LDTM_DH_DATAIMPORTACAO ,"
					+ "  LDTM_ID_USUARIOATUALIZACAO " 
					+ ") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");

			pst.setString(1, leadTime.getPlantaCidade());
			pst.setInt(2, sourceLocation);
			pst.setString(3, leadTime.getSourceLocationName());
			pst.setString(4, leadTime.getSourceProvince());
			pst.setString(5, leadTime.getDestinationCity());
			pst.setString(6, leadTime.getCodProvinceDestination());
			pst.setString(7, leadTime.getNewZone());
			pst.setInt(8, ltl);
			pst.setInt(9, tl);
			pst.setString(10, dhImportacao);
			pst.setString(11, idUser);

			pst.execute();

		} catch (Exception e) {

			log.info("***** Erro no INSERT na base do LEAD TIME " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  INSERT na base do LEAD TIME  *****");
			daoLog.createLog(logsBean);

		}

	}

	public void updateLeadTime(ArquivoLeadTimeBean leadTime, String dhImportacao, int sourceLocation, int ltl, int tl) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbLeadTimeLdtmDao");
		logsBean.setLogsDsMetodo("updateLeadTime");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio update na base LEAD TIME *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst;

			pst = con.prepareStatement("" + "UPDATE CS_CDTB_LEADTIME_LDTM SET" 
			+ "  LDTM_CD_SOURCELOCATION  = ?, "
					+ "  LDTM_DS_SOURCELOCATION_NAME   = ?, "
			+ "  LDTM_DS_SOURCEPROVINCE  = ?, "
					+ "  LDTM_DS_DESTINATIONCITY  = ?, " 
			+ "  LDTM_DS_CODPROVINCEDESTINATION   = ?, "
					+ "  LDTM_DS_NEWZONE  = ?, "
			+ "  LDTM_DS_LTL  = ?, " 
					+ "  LDTM_DS_TL  = ?, "
					+ "  LDTM_DH_ATUALIZACAO  = ?, " + "  LDTM_ID_USUARIOATUALIZACAO = ? "
					+ " WHERE LDTM_DS_PLANTACIDADE = ? ");

			pst.setInt(1, sourceLocation);
			pst.setString(2, leadTime.getSourceLocationName());
			pst.setString(3, leadTime.getSourceProvince());
			pst.setString(4, leadTime.getDestinationCity());
			pst.setString(5, leadTime.getCodProvinceDestination());
			pst.setString(6, leadTime.getNewZone());
			pst.setInt(7, ltl);
			pst.setInt(8, tl);
			pst.setString(9, dhImportacao);
			pst.setString(10, idUser);
			pst.setString(11, leadTime.getPlantaCidade());

			pst.executeUpdate();

		} catch (Exception e) {

			log.info("***** Erro no UPDATE na base do LEAD TIME " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  UPDATE na base do LEAD TIME  *****");
			daoLog.createLog(logsBean);

		}

	}

	public ArquivoLeadTimeBean slcLeadTime(String id) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		ArquivoLeadTimeBean leadTime = new ArquivoLeadTimeBean();

		leadTime.setPlantaCidade("null");

		log.info("***** Inicio Consulta na base LEAD TIME *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "	SELECT   ";
			cSQL += "	LDTM_DS_PLANTACIDADE,";
			cSQL += "	LDTM_CD_SOURCELOCATION,";
			cSQL += "	LDTM_DS_SOURCELOCATION_NAME,";
			cSQL += "	LDTM_DS_SOURCEPROVINCE,";
			cSQL += "	LDTM_DS_DESTINATIONCITY,";
			cSQL += "	LDTM_DS_CODPROVINCEDESTINATION,";
			cSQL += "	LDTM_DS_NEWZONE,";
			cSQL += "	LDTM_DS_LTL,";
			cSQL += "	LDTM_DS_TL";
			cSQL += " 		FROM CS_CDTB_LEADTIME_LDTM (NOLOCK)";
			cSQL += " 	WHERE  LDTM_DS_PLANTACIDADE = ? ";

			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, id);

			rs = pst.executeQuery();

			if (rs.next()) {

				leadTime.setSourceLocationName(rs.getString("LDTM_DS_SOURCELOCATION_NAME"));
				leadTime.setCodProvinceDestination(rs.getString("LDTM_DS_CODPROVINCEDESTINATION"));
				leadTime.setDestinationCity(rs.getString("LDTM_DS_DESTINATIONCITY"));
				leadTime.setLtl(rs.getInt("LDTM_DS_LTL"));
				leadTime.setNewZone(rs.getString("LDTM_DS_NEWZONE"));
				leadTime.setPlantaCidade(rs.getString("LDTM_DS_PLANTACIDADE"));
				leadTime.setSourceLocationID(rs.getInt("LDTM_CD_SOURCELOCATION"));
				leadTime.setSourceProvince(rs.getString("LDTM_DS_SOURCEPROVINCE"));
				leadTime.setTl(rs.getInt("LDTM_DS_TL"));

			}

		} catch (Exception e) {

			log.info("***** Erro na consulta do LEAD TIME " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim da consulta do LEAD TIME  *****");
			daoLog.createLog(logsBean);

		}

		return leadTime;
	}

}
