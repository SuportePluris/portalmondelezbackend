package br.com.portalpluris.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.catapi.util.UtilDate;
import br.com.catapi.util.UtilMoveArquivo;
import br.com.portalpluris.bean.CsLgtbCustomerCustBean;
import br.com.portalpluris.bean.CsLgtbNotaFiscalNofiBean;
import br.com.portalpluris.bean.CsLgtbOrdemOrdeBean;
import br.com.portalpluris.bean.CsLgtbShipmentShipBean;
import br.com.portalpluris.bean.RetornoValidaCargaBean;
import br.com.portalpluris.dao.CsDmtbConfiguracaoConfDao;
import br.com.portalpluris.dao.CsLgtbCustomerCustDao;
import br.com.portalpluris.dao.CsLgtbNotaFiscalNofiDao;
import br.com.portalpluris.dao.CsLgtbOrdemOrdeDao;
import br.com.portalpluris.dao.CsLgtbShipmentShipDao;

public class CsLgtbNotaFiscalNofiBusiness {

	/**
	 * Metodo responsavel por realizar leitura e gravacao das informacoes de nota fiscal disponibilizadas via arquivo TXT no banco de dados
	 * @author Caio Fernandes
	 * */
	public void gravarNotaFiscal() {

		Logger log = Logger.getLogger(this.getClass().getName());	
		log.info("[gravarNotaFiscal] - INICIO ");

		try {

			List<CsLgtbNotaFiscalNofiBean> lstNf = new ArrayList<CsLgtbNotaFiscalNofiBean>();
			CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();
			UtilMoveArquivo utilMove = new UtilMoveArquivo();

			// CONFIGURACOES DE DIRETORIOS
			//String dirOrdem = "C:\\Users\\caraujo\\Desktop\\MONDELEZ_IMP\\SHIPMENT\\";
			String dirNf = confDao.slctConf("conf.importacao.agendamento.diretorio.nf");
			String dirNfBkp = confDao.slctConf("conf.importacao.agendamento.diretorio.nf.bkp");
			String dirNfErro = confDao.slctConf("conf.importacao.agendamento.diretorio.nf.erro");

			// LEITURA DOS ARQUIVOS DO DIRETORIO
			File arquivo = new File(dirNf);
			String[] arq = arquivo.list();

			if(arq != null){

				CsLgtbCustomerCustBean customerBean = new CsLgtbCustomerCustBean();
				log.info("[gravarNotaFiscal] - CARREGANDO ARQUIVOS TXT ");
				log.info( " ******** "+ arq.length + " ARQUIVOS(S) CARREGADOS(S). ******* ");

				for (int i = 0; i < arq.length; i++) {
					
					// ARQUIVO A SER PROCESSADO
					File arquivoProcessado = new File(dirNf + "\\" +  arq[i]);
					
					try {

						if(arquivoProcessado.getName().toUpperCase().endsWith(".TXT")){
							
							String nmArquivo = arquivoProcessado.getName();
							log.info("INICIANDO LEITURA DO ARQUIVO TXT: " + nmArquivo);	
							
							// INSTANCIAR ARQUIVO
							FileInputStream fileInputStream = new FileInputStream(arquivoProcessado);
							BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
							
							// CONTROLES DO ARQUIVO
							String conteudo = "";
							int linha = 0;

							// FOR DAS LINHAS DO ARQUIVO
							while (((conteudo = bufferedReader.readLine()) != null)) {

								try{
									
									linha++;
									log.info("LENDO CONTEUDO DO ARQUIVO ");
									log.info("CONTEUDO DA LINHA: " + linha + "" + conteudo);	

									// SETAR VALORES DO ARQUIVO NO BEAN
									lstNf = setListaNotaFiscalBean(lstNf, conteudo);
									
								} catch (Exception e) {
									log.info("ERRO NA LEITURA DA LINHA: "+ linha);
									log.info("Erro:"+e);
									e.printStackTrace();				

								}
							}		
							
							// REALIZAR O INSERT DA LISTA NO BANCO
							CsLgtbNotaFiscalNofiDao notaFiscalNofiDao = new CsLgtbNotaFiscalNofiDao();
							notaFiscalNofiDao.insertNotaFiscal(lstNf, nmArquivo);
							
							// MOVER ARQUIVO PARA PASTA BKP
							File arquivoProcessadoBkp = new File(dirNfBkp + "\\" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoBkp);
						}
						
					} catch (Exception e) {
						log.info("[gravarNotaFiscal] - ERRO AO LER AQUIVO CUSTOMER ");
						
						// MOVER ARQUIVO PARA PASTA DE ERRO
						if(arq != null) {
							File arquivoProcessadoErro = new File(dirNfErro + "\\" +  arq[i]);
							utilMove.moveArquivo(arquivoProcessado, arquivoProcessadoErro);
						}
						
					}

				}
			}

		} catch (Exception e) {
			log.info("[gravarNotaFiscal] - ERRO AO GRAVAR CUSTOMER ");
		}

	}

	/**
	 * Metodo responsavel por obter os valores do arquivo e retornar a lista de CsLgtbOrdemOrdeBean preenchida.
	 * @author Caio Fernandes
	 * @param List<CsLgtbNotaFiscalNofiBean>, String, String
	 * @return List<CsLgtbNotaFiscalNofiBean>
	 * @throws ParseException 
	 * */
	private List<CsLgtbNotaFiscalNofiBean> setListaNotaFiscalBean(List<CsLgtbNotaFiscalNofiBean> lstNf, String conteudo) throws ParseException {
			
		// LEITURA DOS CAMPOS DO ARQUIVO
		CsLgtbNotaFiscalNofiBean nfBean = new CsLgtbNotaFiscalNofiBean();
		UtilDate util = new UtilDate();
		
		// IDENTIFICAR FINAL DO ARQUIVO
		if(conteudo.substring(0,10).indexOf("TOTAL") > -1) {
			// FIM DO ARQUIVO IDENTIFICADO
			return lstNf;
		}
		
		nfBean.setNofi_ds_company(conteudo.substring(0,4).trim());
		nfBean.setNofi_ds_noname(conteudo.substring(4,8).trim());
		nfBean.setNofi_ds_notafiscalserie(conteudo.substring(8,11).trim());
		nfBean.setNofi_ds_notafiscalnumber(conteudo.substring(11,20).trim());
		nfBean.setNofi_ds_salesordernumber(conteudo.substring(20,32).trim());
		nfBean.setNofi_ds_salesordernumber2(conteudo.substring(32,42).trim());
		nfBean.setNofi_ds_salesordernumber3(conteudo.substring(42,77).trim());
		nfBean.setNofi_ds_salesordernumber4(conteudo.substring(77,112).trim());
		nfBean.setNofi_dh_notafiscaldate(util.convertStringToDateYYYY_MM_dd_HH_MM_SS(conteudo.substring(112,120).trim()));
		nfBean.setNofi_dh_notafiscaldate(util.convertStringToDateYYYY_MM_dd_HH_MM_SS(conteudo.substring(120,128).trim()));
		nfBean.setNofi_cd_customercode(conteudo.substring(128,138).trim());
		nfBean.setNofi_ds_totalcostnotafiscal(conteudo.substring(138,153).trim());
		nfBean.setNofi_ds_quantity(conteudo.substring(153,158).trim());
		nfBean.setNofi_ds_netweight(conteudo.substring(158,173).trim());
		nfBean.setNofi_ds_grossweight(conteudo.substring(173,188).trim());
		nfBean.setNofi_ds_catcod6(conteudo.substring(188,198).trim());
		nfBean.setNofi_ds_catcod7(conteudo.substring(198,208).trim());
		nfBean.setNofi_ds_catcod8(conteudo.substring(208,218).trim());
		nfBean.setNofi_ds_catcod10(conteudo.substring(218,228).trim());
		nfBean.setNofi_ds_distrchannel(conteudo.substring(228,236).trim());
		nfBean.setNofi_ds_vendorcode(conteudo.substring(236,244).trim());
		nfBean.setNofi_ds_cfopcode(conteudo.substring(244,250).trim());
		nfBean.setNofi_ds_description(conteudo.substring(250,300).trim());
		nfBean.setNofi_ds_carriername(conteudo.substring(300,330).trim());
		nfBean.setNofi_ds_codelocation(conteudo.substring(330,334).trim());
		nfBean.setNofi_ds_doctype(conteudo.substring(334,336).trim());
		nfBean.setNofi_ds_nextnumber(conteudo.substring(336,346).trim());
		nfBean.setNofi_ds_volumenota(conteudo.substring(346,361).trim());
		nfBean.setNofi_ds_netvalue(conteudo.substring(361,376).trim());
		nfBean.setNofi_ds_invoicenumber(conteudo.substring(376,386).trim());
		nfBean.setNofi_dh_dateinvoice(util.convertStringToDateYYYY_MM_dd_HH_MM_SS(conteudo.substring(387,395).trim())); // 387, 395
		
		lstNf.add(nfBean);
		
		return lstNf;
	}

	public String getNotaFiscal(String idpedidomond) throws JsonProcessingException {


		String retorno = null;
		
		ArrayList<CsLgtbNotaFiscalNofiBean> retNofi = new ArrayList<CsLgtbNotaFiscalNofiBean>();
		CsLgtbNotaFiscalNofiDao notaFiscalNofiDao = new CsLgtbNotaFiscalNofiDao();	
		
		retNofi = 	notaFiscalNofiDao.slctNotaFiscalByPedidoMond(idpedidomond);
		
		ObjectMapper mapper = new ObjectMapper();
		retorno = mapper.writeValueAsString(retNofi);
		
		
		
		
		return retorno;
	}

}
