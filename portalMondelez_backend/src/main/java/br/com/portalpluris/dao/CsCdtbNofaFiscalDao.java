package br.com.portalpluris.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import br.com.catapi.util.UtilDate;
import br.com.portalpluris.bean.EsLgtbLogsLogsBean;
import br.com.portalpluris.bean.NotaFiscalBean;

public class CsCdtbNofaFiscalDao {

	public boolean slcNofi(NotaFiscalBean nofiBean) {

		NotaFiscalBean retNofi = new NotaFiscalBean();
		retNofi.setNrNotaFiscal("0");

		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("CsCdtbLeadTimeLdtmDao");
		logsBean.setLogsDsMetodo("slcNomePlanta");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio Consulta na base Nota Fiscal *****");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			ResultSet rs = null;

			String cSQL = "";
			cSQL += "	SELECT   ";
			cSQL += "	NOFI_NR_NOTAFISCAL,   ";
			cSQL += "	NOFI_DS_CODIGOEMPRESA,   ";
			cSQL += "	NOFI_NR_NUMPEDIDO,   ";
			cSQL += "	NOFI_NR_NUMPEDIDOCLIENTE,   ";
			cSQL += "	NOFI_NR_DELIVERYDOCUMENT,   ";
			cSQL += "	NOFI_DH_DELIVERY,   ";
			cSQL += "	NOFI_DS_SERIENOTA,   ";
			cSQL += "	NOFI_CD_ID_CLIENTE,   ";
			cSQL += "	NOFI_DS_RAZAOSOCIAL,   ";
			cSQL += "	NOFI_DS_CIDADECLIENTE,   ";
			cSQL += "	NOFI_DS_UFCLIENTE,   ";
			cSQL += "	NOFI_DH_EMISSAONOTA,   ";
			cSQL += "	NOFI_DS_VOLUMENETSALESKG,   ";
			cSQL += "	NOFI_DS_VALORNOTA,   ";
			cSQL += "	NOFI_DS_VALORVENDALIQUIDA,   ";
			cSQL += "	NOFI_DS_LTHALF,   ";
			cSQL += "	NOFI_DS_LTFULL,   ";
			cSQL += "	NOFI_NM_CANALVENDAS,   ";
			cSQL += "	NOFI_DS_CANAL,   ";
			cSQL += "	NOFI_DS_VOLUMENETSALESCX,   ";
			cSQL += "	NOFI_NM_VENDEDOR,   ";
			cSQL += "	NOFI_NM_FANTASIA,   ";
			cSQL += "	NOFI_DS_PLANTA,   ";
			cSQL += "	NOFI_CD_TIPOPEDIDO,   ";
			cSQL += "	NOFI_DS_INATIVO,   ";
			cSQL += "	NOFI_DH_IMPORTACAO,   ";
			cSQL += "	NOFI_DH_ATUALIZACAO,   ";
			cSQL += "	NOFI_CD_USUARIOATUALIZACAO   ";
			cSQL += "	FROM CS_CDTB_NOTAFISCAL_NOFI  (NOLOCK) ";
			cSQL += "	WHERE NOFI_NR_NOTAFISCAL = ?    ";

			PreparedStatement pst = con.prepareStatement(cSQL);

			pst.setString(1, nofiBean.getNrNotaFiscal());

			rs = pst.executeQuery();

			if (rs.next()) {

				log.info(" ******** A nota fiscal:" + nofiBean.getNrNotaFiscal() + " Já existe na base *******");

//				retNofi.setNrNotaFiscal(rs.getString("NOFI_NR_NOTAFISCAL"));
//				retNofi.setCodEmpresa(rs.getString("NOFI_DS_CODIGOEMPRESA"));
//				retNofi.setNrPedido(rs.getString("NOFI_NR_NUMPEDIDO"));
//				retNofi.setNrPedidoCliente(rs.getString("NOFI_NR_NUMPEDIDOCLIENTE"));
//				retNofi.setNrDeliveryDocument(rs.getString("NOFI_NR_DELIVERYDOCUMENT"));
//				retNofi.setDhDelivery(rs.getString("NOFI_DH_DELIVERY"));
//				retNofi.setSerieNota(rs.getString("NOFI_DS_SERIENOTA"));
//				retNofi.setCodCliente(rs.getString("NOFI_CD_ID_CLIENTE"));
//				retNofi.setRazaoSocial(rs.getString("NOFI_DS_RAZAOSOCIAL"));
//				retNofi.setCidadeCliente(rs.getString("NOFI_DS_CIDADECLIENTE"));
//				retNofi.setUfCliente(rs.getString("NOFI_DS_UFCLIENTE"));
//				retNofi.setDhEmissaoNota(rs.getString("NOFI_DH_EMISSAONOTA"));
//				retNofi.setVolumeNetSalesKg(rs.getString("NOFI_DS_VOLUMENETSALESKG"));
//				retNofi.setValorNota(rs.getString("NOFI_DS_VALORNOTA"));
//				retNofi.setValorVendaLiquida(rs.getString("NOFI_DS_VALORVENDALIQUIDA"));
//				retNofi.setLtHalf(rs.getString("NOFI_DS_LTHALF"));
//				retNofi.setLtFull(rs.getString("NOFI_DS_LTFULL"));
//				retNofi.setCanalVendas(rs.getString("NOFI_NM_CANALVENDAS"));
//				retNofi.setCanal(rs.getString("NOFI_DS_CANAL"));
//				retNofi.setVolumeNetSalesCx(rs.getString("NOFI_DS_VOLUMENETSALESCX"));
//				retNofi.setNomeVendedor(rs.getString("NOFI_NM_VENDEDOR"));
//				retNofi.setNomeFantasia(rs.getString("NOFI_NM_FANTASIA"));
//				retNofi.setPlanta(rs.getString("NOFI_DS_PLANTA"));
//				retNofi.setTipoPedido(rs.getString("NOFI_CD_TIPOPEDIDO"));
//				retNofi.setInativo(rs.getString("NOFI_DS_INATIVO"));
//				retNofi.setDhImportacao(rs.getString("NOFI_DH_IMPORTACAO"));
//				retNofi.setDhAtualizacao(rs.getString("NOFI_DH_ATUALIZACAO"));
//				retNofi.setUserAtualizacacao(rs.getString("NOFI_CD_USUARIOATUALIZACAO"));

				return true;
			}

		} catch (Exception e) {
			log.info("*** ERRO NA CONSULTA DE NOTA FISCAL  ******");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {
			daoLog.createLog(logsBean);

		}

		return false;

	}

	public void updateNofi(NotaFiscalBean nofiBean, String dhImportacao, String nmArquivo) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("insertLeadTime");
		logsBean.setLogsDsMetodo("updateNofi");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio update na NOTA FISCAL *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst;

			pst = con.prepareStatement("" + "UPDATE CS_CDTB_NOTAFISCAL_NOFI SET" + "	NOFI_DS_CODIGOEMPRESA = ? ,   "
					+ "	NOFI_NR_NUMPEDIDO = ? ,   " + "	NOFI_NR_NUMPEDIDOCLIENTE = ? ,   "
					+ "	NOFI_NR_DELIVERYDOCUMENT = ? ,   " + "	NOFI_DH_DELIVERY = ? ,   "
					+ "	NOFI_DS_SERIENOTA = ? ,   " + "	NOFI_CD_ID_CLIENTE = ? ,   "
					+ "	NOFI_DS_RAZAOSOCIAL = ? ,   " + "	NOFI_DS_CIDADECLIENTE = ? ,   "
					+ "	NOFI_DS_UFCLIENTE = ? ,   " + "	NOFI_DH_EMISSAONOTA = ? ,   "
					+ "	NOFI_DS_VOLUMENETSALESKG = ? ,   " + "	NOFI_DS_VALORNOTA = ? ,   "
					+ "	NOFI_DS_VALORVENDALIQUIDA = ? ,   " + "	NOFI_DS_LTHALF = ? ,   " + "	NOFI_DS_LTFULL = ? ,   "
					+ "	NOFI_NM_CANALVENDAS = ? ,   " + "	NOFI_DS_CANAL = ? ,   "
					+ "	NOFI_DS_VOLUMENETSALESCX = ? ,   " + "	NOFI_NM_VENDEDOR = ? ,   "
					+ "	NOFI_NM_FANTASIA = ? ,   " + "	NOFI_DS_PLANTA = ? ,   " + "	NOFI_CD_TIPOPEDIDO = ? ,   "
					+ "	NOFI_DH_ATUALIZACAO = ? ,   " + "	NOFI_CD_USUARIOATUALIZACAO = ?,  "
					+ "    NOFI_NM_ARQUIVOPROCESSADO = ? " + " WHERE	NOFI_NR_NOTAFISCAL = ?    ");

			pst.setString(1, nofiBean.getCodEmpresa());
			pst.setString(2, nofiBean.getNrPedido());
			pst.setString(3, nofiBean.getNrPedidoCliente());
			pst.setString(4, nofiBean.getNrDeliveryDocument());
			pst.setString(5, nofiBean.getDhDelivery());
			pst.setString(6, nofiBean.getSerieNota());
			pst.setString(7, nofiBean.getCodCliente());
			pst.setString(8, nofiBean.getRazaoSocial());
			pst.setString(9, nofiBean.getCidadeCliente());
			pst.setString(10, nofiBean.getUfCliente());
			pst.setString(11, nofiBean.getDhEmissaoNota());
			pst.setString(12, nofiBean.getVolumeNetSalesKg());
			pst.setString(13, nofiBean.getValorNota());
			pst.setString(14, nofiBean.getValorVendaLiquida());
			pst.setString(15, nofiBean.getLtHalf());
			pst.setString(16, nofiBean.getLtFull());
			pst.setString(17, nofiBean.getCanalVendas());
			pst.setString(18, nofiBean.getCanal());
			pst.setString(19, nofiBean.getVolumeNetSalesCx());
			pst.setString(20, nofiBean.getNomeVendedor());
			pst.setString(21, nofiBean.getNomeFantasia());
			pst.setString(22, nofiBean.getPlanta());
			pst.setString(23, nofiBean.getTipoPedido());
			pst.setString(24, dhImportacao);
			pst.setString(25, idUser);
			pst.setString(26, nmArquivo);
			pst.setString(27, nofiBean.getNrNotaFiscal());

			pst.executeUpdate();

		} catch (Exception e) {

			log.info("***** Erro no UPDATE na base NOTA FISCAL " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  UPDATE na base do NOTA FISCAL  *****");
			daoLog.createLog(logsBean);

		}

	}

	public void insertNofi(NotaFiscalBean nofiBean, String dhImportacao, String nmArquivo) {

		// DADOS DE LOG
		Logger log = Logger.getLogger(this.getClass().getName());
		EsLgtbLogsLogsBean logsBean = new EsLgtbLogsLogsBean();
		DAOLog daoLog = new DAOLog();
		CsDmtbConfiguracaoConfDao confDao = new CsDmtbConfiguracaoConfDao();

		logsBean.setLogsDhInicio(UtilDate.getSqlDateTimeAtual());
		logsBean.setLogsDsClasse("insertLeadTime");
		logsBean.setLogsDsMetodo("insertNofi");
		logsBean.setLogsInErro("N");

		log.info("***** Inicio INSERT na NOTA FISCAL *****");

		String idUser = confDao.slctConf("conf.importacao.idUsuario@1");

		try {

			DAO dao = new DAO();
			Connection con = dao.getConexao();

			PreparedStatement pst = con.prepareStatement("");

			pst = con.prepareStatement("" + " INSERT INTO CS_CDTB_NOTAFISCAL_NOFI  (" + "	NOFI_NR_NOTAFISCAL,   "
					+ "	NOFI_DS_CODIGOEMPRESA,   " + "	NOFI_NR_NUMPEDIDO,   " + "	NOFI_NR_NUMPEDIDOCLIENTE,   "
					+ "	NOFI_NR_DELIVERYDOCUMENT,   " + "	NOFI_DH_DELIVERY,   " + "	NOFI_DS_SERIENOTA,   "
					+ "	NOFI_CD_ID_CLIENTE,   " + "	NOFI_DS_RAZAOSOCIAL,   " + "	NOFI_DS_CIDADECLIENTE,   "
					+ "	NOFI_DS_UFCLIENTE,   " + "	NOFI_DH_EMISSAONOTA,   " + "	NOFI_DS_VOLUMENETSALESKG,   "
					+ "	NOFI_DS_VALORNOTA,   " + "	NOFI_DS_VALORVENDALIQUIDA,   " + "	NOFI_DS_LTHALF,   "
					+ "	NOFI_DS_LTFULL,   " + "	NOFI_NM_CANALVENDAS,   " + "	NOFI_DS_CANAL,   "
					+ "	NOFI_DS_VOLUMENETSALESCX,   " + "	NOFI_NM_VENDEDOR,   " + "	NOFI_NM_FANTASIA,   "
					+ "	NOFI_DS_PLANTA,   " + "	NOFI_CD_TIPOPEDIDO,   " + "	NOFI_DS_INATIVO,   "
					+ "		NOFI_DH_IMPORTACAO,   " + "	NOFI_CD_USUARIOATUALIZACAO,   "
					+ "    NOFI_NM_ARQUIVOPROCESSADO "
					+ " )VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?)");

			pst.setString(1, nofiBean.getNrNotaFiscal());
			pst.setString(2, nofiBean.getCodEmpresa());
			pst.setString(3, nofiBean.getNrPedido());
			pst.setString(4, nofiBean.getNrPedidoCliente());
			pst.setString(5, nofiBean.getNrDeliveryDocument());
			pst.setString(6, nofiBean.getDhDelivery());
			pst.setString(7, nofiBean.getSerieNota());
			pst.setString(8, nofiBean.getCodCliente());
			pst.setString(9, nofiBean.getRazaoSocial());
			pst.setString(10, nofiBean.getCidadeCliente());
			pst.setString(11, nofiBean.getUfCliente());
			pst.setString(12, nofiBean.getDhEmissaoNota());
			pst.setString(13, nofiBean.getVolumeNetSalesKg());
			pst.setString(14, nofiBean.getValorNota());
			pst.setString(15, nofiBean.getValorVendaLiquida());
			pst.setString(16, nofiBean.getLtHalf());
			pst.setString(17, nofiBean.getLtFull());
			pst.setString(18, nofiBean.getCanalVendas());
			pst.setString(19, nofiBean.getCanal());
			pst.setString(20, nofiBean.getVolumeNetSalesCx());
			pst.setString(21, nofiBean.getNomeVendedor());
			pst.setString(22, nofiBean.getNomeFantasia());
			pst.setString(23, nofiBean.getPlanta());
			pst.setString(24, nofiBean.getTipoPedido());
			pst.setString(25, "N");
			pst.setString(26, dhImportacao);
			pst.setString(27, idUser);
			pst.setString(28, nmArquivo);

			pst.execute();

		} catch (Exception e) {

			log.info("***** Erro no INSERT na base NOTA FISCAL " + e + " *****");
			logsBean.setLogsInErro("S");
			logsBean.setLogsTxErro(e.getMessage());

		} finally {

			log.info("***** Fim do  INSERT na base do NOTA FISCAL  *****");
			daoLog.createLog(logsBean);

		}

	}

}
