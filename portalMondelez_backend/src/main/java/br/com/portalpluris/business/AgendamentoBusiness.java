package br.com.portalpluris.business;

import java.util.ArrayList;
import java.util.List;

import br.com.portalpluris.bean.RetornoDeliveryBean;
import br.com.portalpluris.bean.RetornoShipmentBean;
import br.com.portalpluris.bean.RetornoShipmentDeliveryBean;
import br.com.portalpluris.bean.RetornoSkudBean;
import br.com.portalpluris.dao.CsCdtbSkudDeliveryDao;
import br.com.portalpluris.dao.CsNgtbEmbarqueEmbaDao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AgendamentoBusiness {

	public String getShipmentDelivery(String embarque) throws JsonProcessingException {
		
		String jsonRetorno = "";
			
		
		ArrayList<RetornoDeliveryBean> retDelivery = new ArrayList<RetornoDeliveryBean>();
		RetornoShipmentBean retShip = new RetornoShipmentBean();
		
		CsNgtbEmbarqueEmbaDao embaDao = new CsNgtbEmbarqueEmbaDao();
		System.out.println("  ");
		//retShip = embaDao.slcEmbarqueShipment(embarque);// COMO ESTAVA ANTES 05/07
		retShip = embaDao.slcEmbarqueShipmentAgrupado(embarque); // DANILLO APÓS 05/07 
		
		retDelivery = embaDao.slcDelivery(retShip.getEmba_id_cd_shipment(), embarque, "tela");
		
		retShip.setRetDelivery(retDelivery);
		
		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retShip);
		
		return jsonRetorno;
		
	}

	public String getSkus(String idDelivery) throws JsonProcessingException {

	String jsonRetorno = "";		
		
		CsCdtbSkudDeliveryDao skudDao = new CsCdtbSkudDeliveryDao();
		List<RetornoSkudBean> skudBean = new ArrayList<RetornoSkudBean>();
		
		skudBean = skudDao.slcSkudByDelivery(idDelivery);
		
		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(skudBean);
		

		return jsonRetorno;
	}

	public String getDeliveryByStatus(String condicao, String id, String status) throws JsonProcessingException {
	
		String jsonRetorno = "";	
		
		ArrayList<RetornoShipmentBean> retShip = new ArrayList<RetornoShipmentBean>();
		CsNgtbEmbarqueEmbaDao embaDao = new CsNgtbEmbarqueEmbaDao();
		
		retShip = embaDao.slctByCriteria(condicao,id, status);
		
		ObjectMapper mapper = new ObjectMapper();
		jsonRetorno = mapper.writeValueAsString(retShip);		

		return jsonRetorno;

	}
	
	
	

}
