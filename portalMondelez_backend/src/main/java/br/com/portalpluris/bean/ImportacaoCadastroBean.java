package br.com.portalpluris.bean;

import java.util.logging.Logger;

import br.com.portalpluris.business.CsLgbtPedidosdeliveryPediBusiness;
import br.com.portalpluris.business.CsLgtbCustomerCustBusiness;
import br.com.portalpluris.business.CsLgtbShipmentShipBusiness;
import br.com.portalpluris.business.CsLgtbSkudeliverySkudBusiness;

public class ImportacaoCadastroBean {
	
	
	/**
	 * Metodo responsavel por iniciar a importacao dos arquivos de carga de cadastro
	 * @author Caio Fernandes
	 * */
	public void iniciarImportacao() {
		
		Logger log = Logger.getLogger(this.getClass().getName());
		log.info("[iniciarImportacao] - INICIO IMPORTACAO CARGA");
		
		try {
			
			// 1. *** IMPORTACAO CUSTOMER ***
			log.info("[iniciarImportacao] - IMPORTACAO CUSTOMER");
			CsLgtbCustomerCustBusiness csLgtbCustomerCustBusiness = new CsLgtbCustomerCustBusiness();
			csLgtbCustomerCustBusiness.gravarCustomer();
			
			// CHAMADA HIER
			
		} catch (Exception e) {
			log.info("[iniciarImportacao] - ERRO IMPORTACAO CARGA " + e.getMessage());
		}
		
		log.info("[iniciarImportacao] - FIM IMPORTACAO CARGA");
		
	}
	
	public static void main(String[] args) {
		//iniciarImportacao();
	}

}
