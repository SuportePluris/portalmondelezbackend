package br.com.portalpluris.bean;

import java.util.Date;

public class CsLgtbSkudeliverySkudBean {
	
	// SKUD_CD_DELIVERY 
	private String skud_cd_delivery;
	
	// SKUD_CD_REFERENCEDOCUMENT 
	private String skud_cd_referencedocument;
	
	// SKUD_DS_ITEM
	private String skud_ds_item;
	
	// SKUD_DS_SHIPTOPARTY
	private String skud_ds_shiptoparty;
	
	// SKUD_DS_NAMESHIPTOPARTY
	private String skud_ds_nameshiptoparty;
	
	// SKUD_DS_MATERIAL
	private String skud_ds_material;
	
	// SKUD_DS_DESCRIPTION 
	private String skud_ds_description;
	
	// SKUD_DS_DELIVERYQUANTITY
	private String skud_ds_deliveryquantity;
	
	// SKUD_DS_SALESUNIT 
	private String skud_ds_salesunit;
	
	// SKUD_DH_MATERIALAVAILDATE
	private Date skud_dh_materialavaildate;
	
	// SKUD_DS_NETWEIGHT 
	private String skud_ds_netweight;
	
	// SKUD_DS_TOTALWEIGHT 
	private String skud_ds_totalweight;
	
	// SKUD_DS_WEIGHTUNIT 
	private String skud_ds_weightunit;
	
	// SKUD_DS_VOLUME 
	private String skud_ds_volume;
	
	// SKUD_DS_VOLUMEUNIT
	private String skud_ds_volumeunit;
	
	// SKUD_DS_STORAGELOCATION 
	private String skud_ds_storagelocation;
	
	// SKUD_DS_BATCH 
	private String skud_ds_batch;
	
	// SKUD_DS_PLANT 
	private String skud_ds_plant;
	
	// SKUD_DH_ACTGDSMVMNTDATE
	private Date skud_dh_actgdsmvmntdate;
	
	// SKUD_DH_DELIVERYDATE 
	private Date skud_dh_deliverydate;
	
	// SKUD_DS_ROUTE
	private String skud_ds_route;
	
	// SKUD_DS_ACTUALDELIVERYQTY
	private String skud_ds_actualdeliveryqty;
	
	// SKUD_DS_VENDORLOCATION 
	private String skud_ds_vendorlocation;
	
	// SKUD_DS_LOCATIONSHIPTOPARTY 
	private String skud_ds_locationshiptoparty;
	
	// SKUD_DH_LOADINGDATE 
	private Date skud_dh_loadingdate;
	
	// SKUD_DS_DELIVERYTYPE
	private String skud_ds_deliverytype;
	
	// SKUD_DS_SHIPPINPOINT 
	private String skud_ds_shippinpoint;
	
	// SKUD_DS_DELIVERYPRIORITY 
	private String skud_ds_deliverypriority;
	
	// SKUD_DH_GOODSISSUEDATE 
	private Date skud_dh_goodsissuedate;
	
	// SKUD_DH_TRANSPTNPLANGDATE 
	private Date skud_dh_transptnplangdate;
	
	// SKUD_DH_PICKINGDATE 
	private Date skud_dh_pickingdate;
	
	// SKUD_DS_LOCATIONSOLDTOPARTY 
	private String skud_ds_locationsoldtoparty;
	
	// SKUD_DS_VENDOR 
	private String skud_ds_vendor;
	
	// SKUD_DS_DISTRIBUTIONCHANNEL
	private String skud_ds_distributionchannel;
	
	// SKUD_DS_SALESORGANIZATION 
	private String skud_ds_salesorganization;
	
	// SKUD_DS_WAREHOUSENUMBER
	private String skud_ds_warehousenumber;
	
	// SKUD_CD_SHIPMENT 
	private String skud_cd_shipment;
	
	// SKUD_DH_REGISTRO
	private Date skud_dh_registro;
	
	// SKUD_DS_STATUSPROCESSAMENTO
	private String skud_ds_statusprocessamento;

	public String getSkud_cd_delivery() {
		return skud_cd_delivery;
	}

	public void setSkud_cd_delivery(String skud_cd_delivery) {
		this.skud_cd_delivery = skud_cd_delivery;
	}

	public String getSkud_cd_referencedocument() {
		return skud_cd_referencedocument;
	}

	public void setSkud_cd_referencedocument(String skud_cd_referencedocument) {
		this.skud_cd_referencedocument = skud_cd_referencedocument;
	}

	public String getSkud_ds_item() {
		return skud_ds_item;
	}

	public void setSkud_ds_item(String skud_ds_item) {
		this.skud_ds_item = skud_ds_item;
	}

	public String getSkud_ds_shiptoparty() {
		return skud_ds_shiptoparty;
	}

	public void setSkud_ds_shiptoparty(String skud_ds_shiptoparty) {
		this.skud_ds_shiptoparty = skud_ds_shiptoparty;
	}

	public String getSkud_ds_nameshiptoparty() {
		return skud_ds_nameshiptoparty;
	}

	public void setSkud_ds_nameshiptoparty(String skud_ds_nameshiptoparty) {
		this.skud_ds_nameshiptoparty = skud_ds_nameshiptoparty;
	}

	public String getSkud_ds_material() {
		return skud_ds_material;
	}

	public void setSkud_ds_material(String skud_ds_material) {
		this.skud_ds_material = skud_ds_material;
	}

	public String getSkud_ds_description() {
		return skud_ds_description;
	}

	public void setSkud_ds_description(String skud_ds_description) {
		this.skud_ds_description = skud_ds_description;
	}

	public String getSkud_ds_deliveryquantity() {
		return skud_ds_deliveryquantity;
	}

	public void setSkud_ds_deliveryquantity(String skud_ds_deliveryquantity) {
		this.skud_ds_deliveryquantity = skud_ds_deliveryquantity;
	}

	public String getSkud_ds_salesunit() {
		return skud_ds_salesunit;
	}

	public void setSkud_ds_salesunit(String skud_ds_salesunit) {
		this.skud_ds_salesunit = skud_ds_salesunit;
	}

	public Date getSkud_dh_materialavaildate() {
		return skud_dh_materialavaildate;
	}

	public void setSkud_dh_materialavaildate(Date skud_dh_materialavaildate) {
		this.skud_dh_materialavaildate = skud_dh_materialavaildate;
	}

	public String getSkud_ds_netweight() {
		return skud_ds_netweight;
	}

	public void setSkud_ds_netweight(String skud_ds_netweight) {
		this.skud_ds_netweight = skud_ds_netweight;
	}

	public String getSkud_ds_totalweight() {
		return skud_ds_totalweight;
	}

	public void setSkud_ds_totalweight(String skud_ds_totalweight) {
		this.skud_ds_totalweight = skud_ds_totalweight;
	}

	public String getSkud_ds_weightunit() {
		return skud_ds_weightunit;
	}

	public void setSkud_ds_weightunit(String skud_ds_weightunit) {
		this.skud_ds_weightunit = skud_ds_weightunit;
	}

	public String getSkud_ds_volume() {
		return skud_ds_volume;
	}

	public void setSkud_ds_volume(String skud_ds_volume) {
		this.skud_ds_volume = skud_ds_volume;
	}

	public String getSkud_ds_volumeunit() {
		return skud_ds_volumeunit;
	}

	public void setSkud_ds_volumeunit(String skud_ds_volumeunit) {
		this.skud_ds_volumeunit = skud_ds_volumeunit;
	}

	public String getSkud_ds_storagelocation() {
		return skud_ds_storagelocation;
	}

	public void setSkud_ds_storagelocation(String skud_ds_storagelocation) {
		this.skud_ds_storagelocation = skud_ds_storagelocation;
	}

	public String getSkud_ds_batch() {
		return skud_ds_batch;
	}

	public void setSkud_ds_batch(String skud_ds_batch) {
		this.skud_ds_batch = skud_ds_batch;
	}

	public String getSkud_ds_plant() {
		return skud_ds_plant;
	}

	public void setSkud_ds_plant(String skud_ds_plant) {
		this.skud_ds_plant = skud_ds_plant;
	}

	public Date getSkud_dh_actgdsmvmntdate() {
		return skud_dh_actgdsmvmntdate;
	}

	public void setSkud_dh_actgdsmvmntdate(Date skud_dh_actgdsmvmntdate) {
		this.skud_dh_actgdsmvmntdate = skud_dh_actgdsmvmntdate;
	}

	public Date getSkud_dh_deliverydate() {
		return skud_dh_deliverydate;
	}

	public void setSkud_dh_deliverydate(Date skud_dh_deliverydate) {
		this.skud_dh_deliverydate = skud_dh_deliverydate;
	}

	public String getSkud_ds_route() {
		return skud_ds_route;
	}

	public void setSkud_ds_route(String skud_ds_route) {
		this.skud_ds_route = skud_ds_route;
	}

	public String getSkud_ds_actualdeliveryqty() {
		return skud_ds_actualdeliveryqty;
	}

	public void setSkud_ds_actualdeliveryqty(String skud_ds_actualdeliveryqty) {
		this.skud_ds_actualdeliveryqty = skud_ds_actualdeliveryqty;
	}

	public String getSkud_ds_vendorlocation() {
		return skud_ds_vendorlocation;
	}

	public void setSkud_ds_vendorlocation(String skud_ds_vendorlocation) {
		this.skud_ds_vendorlocation = skud_ds_vendorlocation;
	}

	public String getSkud_ds_locationshiptoparty() {
		return skud_ds_locationshiptoparty;
	}

	public void setSkud_ds_locationshiptoparty(String skud_ds_locationshiptoparty) {
		this.skud_ds_locationshiptoparty = skud_ds_locationshiptoparty;
	}

	public Date getSkud_dh_loadingdate() {
		return skud_dh_loadingdate;
	}

	public void setSkud_dh_loadingdate(Date skud_dh_loadingdate) {
		this.skud_dh_loadingdate = skud_dh_loadingdate;
	}

	public String getSkud_ds_deliverytype() {
		return skud_ds_deliverytype;
	}

	public void setSkud_ds_deliverytype(String skud_ds_deliverytype) {
		this.skud_ds_deliverytype = skud_ds_deliverytype;
	}

	public String getSkud_ds_shippinpoint() {
		return skud_ds_shippinpoint;
	}

	public void setSkud_ds_shippinpoint(String skud_ds_shippinpoint) {
		this.skud_ds_shippinpoint = skud_ds_shippinpoint;
	}

	public String getSkud_ds_deliverypriority() {
		return skud_ds_deliverypriority;
	}

	public void setSkud_ds_deliverypriority(String skud_ds_deliverypriority) {
		this.skud_ds_deliverypriority = skud_ds_deliverypriority;
	}

	public Date getSkud_dh_goodsissuedate() {
		return skud_dh_goodsissuedate;
	}

	public void setSkud_dh_goodsissuedate(Date skud_dh_goodsissuedate) {
		this.skud_dh_goodsissuedate = skud_dh_goodsissuedate;
	}

	public Date getSkud_dh_transptnplangdate() {
		return skud_dh_transptnplangdate;
	}

	public void setSkud_dh_transptnplangdate(Date skud_dh_transptnplangdate) {
		this.skud_dh_transptnplangdate = skud_dh_transptnplangdate;
	}

	public Date getSkud_dh_pickingdate() {
		return skud_dh_pickingdate;
	}

	public void setSkud_dh_pickingdate(Date skud_dh_pickingdate) {
		this.skud_dh_pickingdate = skud_dh_pickingdate;
	}

	public String getSkud_ds_locationsoldtoparty() {
		return skud_ds_locationsoldtoparty;
	}

	public void setSkud_ds_locationsoldtoparty(String skud_ds_locationsoldtoparty) {
		this.skud_ds_locationsoldtoparty = skud_ds_locationsoldtoparty;
	}

	public String getSkud_ds_vendor() {
		return skud_ds_vendor;
	}

	public void setSkud_ds_vendor(String skud_ds_vendor) {
		this.skud_ds_vendor = skud_ds_vendor;
	}

	public String getSkud_ds_distributionchannel() {
		return skud_ds_distributionchannel;
	}

	public void setSkud_ds_distributionchannel(String skud_ds_distributionchannel) {
		this.skud_ds_distributionchannel = skud_ds_distributionchannel;
	}

	public String getSkud_ds_salesorganization() {
		return skud_ds_salesorganization;
	}

	public void setSkud_ds_salesorganization(String skud_ds_salesorganization) {
		this.skud_ds_salesorganization = skud_ds_salesorganization;
	}

	public String getSkud_ds_warehousenumber() {
		return skud_ds_warehousenumber;
	}

	public void setSkud_ds_warehousenumber(String skud_ds_warehousenumber) {
		this.skud_ds_warehousenumber = skud_ds_warehousenumber;
	}

	public String getSkud_cd_shipment() {
		return skud_cd_shipment;
	}

	public void setSkud_cd_shipment(String skud_cd_shipment) {
		this.skud_cd_shipment = skud_cd_shipment;
	}

	public Date getSkud_dh_registro() {
		return skud_dh_registro;
	}

	public void setSkud_dh_registro(Date skud_dh_registro) {
		this.skud_dh_registro = skud_dh_registro;
	}

	public String getSkud_ds_statusprocessamento() {
		return skud_ds_statusprocessamento;
	}

	public void setSkud_ds_statusprocessamento(String skud_ds_statusprocessamento) {
		this.skud_ds_statusprocessamento = skud_ds_statusprocessamento;
	}
	
}
