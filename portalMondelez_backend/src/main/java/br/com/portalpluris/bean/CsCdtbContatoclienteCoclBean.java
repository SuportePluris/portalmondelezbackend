package br.com.portalpluris.bean;

import java.sql.Timestamp;

public class CsCdtbContatoclienteCoclBean {

	String cocl_id_cd_contatocliente;
	String cocl_id_cd_codigocliente;
	String cocl_ds_emailcontatocliente;
	String cocl_id_cd_usuarioatualizacao;
	String cocl_id_cd_usuariocriacao;
	String cocl_dh_registro;
	String cocl_dh_atualizacao;
	String cocl_in_inativo;
	String cocl_ds_nomecliente;
	String cocl_ds_telefonecliente;
	String cocl_ds_cepcliente;
	String cocl_ds_bairrocliente;
	String cocl_ds_cidadecliente;
	String cocl_ds_enderecocliente;
	String cocl_ds_cnjcliente;
	String cocl_ds_grupocliente;
	String cocl_ds_ufcliente;
	
	
	
	
	
	public String getCocl_ds_ufcliente() {
		return cocl_ds_ufcliente;
	}
	public void setCocl_ds_ufcliente(String cocl_ds_ufcliente) {
		this.cocl_ds_ufcliente = cocl_ds_ufcliente;
	}
	public String getCocl_ds_grupocliente() {
		return cocl_ds_grupocliente;
	}
	public void setCocl_ds_grupocliente(String cocl_ds_grupocliente) {
		this.cocl_ds_grupocliente = cocl_ds_grupocliente;
	}
	public String getCocl_id_cd_contatocliente() {
		return cocl_id_cd_contatocliente;
	}
	public void setCocl_id_cd_contatocliente(String cocl_id_cd_contatocliente) {
		this.cocl_id_cd_contatocliente = cocl_id_cd_contatocliente;
	}
	public String getCocl_id_cd_codigocliente() {
		return cocl_id_cd_codigocliente;
	}
	public void setCocl_id_cd_codigocliente(String cocl_id_cd_codigocliente) {
		this.cocl_id_cd_codigocliente = cocl_id_cd_codigocliente;
	}
	public String getCocl_ds_emailcontatocliente() {
		return cocl_ds_emailcontatocliente;
	}
	public void setCocl_ds_emailcontatocliente(String cocl_ds_emailcontatocliente) {
		this.cocl_ds_emailcontatocliente = cocl_ds_emailcontatocliente;
	}
	public String getCocl_id_cd_usuarioatualizacao() {
		return cocl_id_cd_usuarioatualizacao;
	}
	public void setCocl_id_cd_usuarioatualizacao(
			String cocl_id_cd_usuarioatualizacao) {
		this.cocl_id_cd_usuarioatualizacao = cocl_id_cd_usuarioatualizacao;
	}
	public String getCocl_id_cd_usuariocriacao() {
		return cocl_id_cd_usuariocriacao;
	}
	public void setCocl_id_cd_usuariocriacao(String cocl_id_cd_usuariocriacao) {
		this.cocl_id_cd_usuariocriacao = cocl_id_cd_usuariocriacao;
	}
	public String getCocl_dh_registro() {
		return cocl_dh_registro;
	}
	public void setCocl_dh_registro(String cocl_dh_registro) {
		this.cocl_dh_registro = cocl_dh_registro;
	}
	public String getCocl_dh_atualizacao() {
		return cocl_dh_atualizacao;
	}
	public void setCocl_dh_atualizacao(String cocl_dh_atualizacao) {
		this.cocl_dh_atualizacao = cocl_dh_atualizacao;
	}
	public String getCocl_in_inativo() {
		return cocl_in_inativo;
	}
	public void setCocl_in_inativo(String cocl_in_inativo) {
		this.cocl_in_inativo = cocl_in_inativo;
	}
	public String getCocl_ds_nomecliente() {
		return cocl_ds_nomecliente;
	}
	public void setCocl_ds_nomecliente(String cocl_ds_nomecliente) {
		this.cocl_ds_nomecliente = cocl_ds_nomecliente;
	}
	public String getCocl_ds_telefonecliente() {
		return cocl_ds_telefonecliente;
	}
	public void setCocl_ds_telefonecliente(String cocl_ds_telefonecliente) {
		this.cocl_ds_telefonecliente = cocl_ds_telefonecliente;
	}
	public String getCocl_ds_cepcliente() {
		return cocl_ds_cepcliente;
	}
	public void setCocl_ds_cepcliente(String cocl_ds_cepcliente) {
		this.cocl_ds_cepcliente = cocl_ds_cepcliente;
	}
	public String getCocl_ds_bairrocliente() {
		return cocl_ds_bairrocliente;
	}
	public void setCocl_ds_bairrocliente(String cocl_ds_bairrocliente) {
		this.cocl_ds_bairrocliente = cocl_ds_bairrocliente;
	}
	public String getCocl_ds_cidadecliente() {
		return cocl_ds_cidadecliente;
	}
	public void setCocl_ds_cidadecliente(String cocl_ds_cidadecliente) {
		this.cocl_ds_cidadecliente = cocl_ds_cidadecliente;
	}
	public String getCocl_ds_enderecocliente() {
		return cocl_ds_enderecocliente;
	}
	public void setCocl_ds_enderecocliente(String cocl_ds_enderecocliente) {
		this.cocl_ds_enderecocliente = cocl_ds_enderecocliente;
	}
	public String getCocl_ds_cnjcliente() {
		return cocl_ds_cnjcliente;
	}
	public void setCocl_ds_cnjcliente(String cocl_ds_cnjcliente) {
		this.cocl_ds_cnjcliente = cocl_ds_cnjcliente;
	}
	
	
	
	
	
	
}

